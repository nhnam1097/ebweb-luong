import './vendor.ts';

import { NgModule, Injector } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';
import { Ng2Webstorage, LocalStorageService, SessionStorageService } from 'ngx-webstorage';
import { JhiEventManager } from 'ng-jhipster';

import { AuthInterceptor } from './blocks/interceptor/auth.interceptor';
import { AuthExpiredInterceptor } from './blocks/interceptor/auth-expired.interceptor';
import { ErrorHandlerInterceptor } from './blocks/interceptor/errorhandler.interceptor';
import { NotificationInterceptor } from './blocks/interceptor/notification.interceptor';
import { TestluongSharedModule } from 'app/shared';
import { TestluongCoreModule } from 'app/core';
import { TestluongAppRoutingModule } from './app-routing.module';
import { TestluongHomeModule } from './home/home.module';
import { TestluongAccountModule } from './account/account.module';
import { TestluongEntityModule } from './entities/entity.module';
import * as moment from 'moment';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { NavbarComponent, FooterComponent, ActiveMenuDirective, ErrorComponent } from './layouts';
import { LoginComponent } from 'app/layouts/login/login.component';
import { EbMainComponent } from './layouts';
import { NgxMaskModule } from 'ngx-mask';
import { SidebarComponent } from 'app/layouts/sidebar/sidebar.component';
import { CookieService } from 'ngx-cookie';
import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EbContextMenuModule } from 'app/shared/context-menu/contex-menu.module';

@NgModule({
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        TestluongAppRoutingModule,
        Ng2Webstorage.forRoot({ prefix: '', separator: '' }),
        NgxMaskModule.forRoot(),
        TestluongSharedModule,
        TestluongCoreModule,
        TestluongHomeModule,
        TestluongAccountModule,
        TestluongEntityModule,
        LoadingBarHttpClientModule,
        EbContextMenuModule
        // jhipster-needle-angular-add-module JHipster will add new module here
    ],
    declarations: [
        EbMainComponent,
        NavbarComponent,
        ErrorComponent,
        ActiveMenuDirective,
        FooterComponent,
        LoginComponent,
        SidebarComponent
    ],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthInterceptor,
            multi: true,
            deps: [LocalStorageService, SessionStorageService, CookieService]
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthExpiredInterceptor,
            multi: true,
            deps: [Injector]
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: ErrorHandlerInterceptor,
            multi: true,
            deps: [JhiEventManager]
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: NotificationInterceptor,
            multi: true,
            deps: [Injector]
        }
    ],
    bootstrap: [EbMainComponent]
})
export class TestluongAppModule {
    constructor(private dpConfig: NgbDatepickerConfig) {
        this.dpConfig.minDate = { year: moment().year() - 100, month: 1, day: 1 };
    }
}
