import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { JhiAlertService, JhiEventManager, JhiParseLinks } from 'ng-jhipster';

import { Principal } from 'app/core';

import { DATE_FORMAT, ITEMS_PER_PAGE } from 'app/shared';
import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ROLE } from 'app/role.constants';
import { DatePipe } from '@angular/common';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { BaseComponent } from 'app/shared/base-component/base.component';
import { ebAuth } from 'app/shared/decorator/ebAuth.decorator';
import { PSTimeSheetService } from 'app/luong/cham_cong';
import { BangLuongService } from 'app/luong/bang_luong/bang-luong.service';
import { PSTimeSheetSummaryService } from 'app/luong/tong_hop_cham_cong';
import { PSSalarySheetDetailsService } from 'app/luong/bang_luong/ps-salary-sheet-details/ps-salary-sheet-details.service';
import { DDSo_TienVND, DECIMAL, THOUSANDS, TYPEGROUP, TypeMultiPrintTemplate } from 'app/app.constants';
import * as moment from 'moment';
import { CurrencyMaskConfig } from 'app/shared/directive/ng2-currency-mask/currency-mask.config';
import { IPSSalarySheet } from 'app/shared/models/ps-salary-sheet.model';
import { ISearchVoucher } from 'app/shared/models/SearchVoucher';
import { IType } from 'app/shared/models/type.model';
import { IPSSalarySheetDetails } from 'app/shared/models/ps-salary-sheet-details.model';
import { IOrganizationUnit } from 'app/shared/models/organization-unit.model';
import { IPSTimeSheet } from 'app/shared/models/ps-time-sheet.model';
import { ISessionPSTimeSheet } from 'app/shared/models/session-p-s-time-sheet';
import { UtilsService } from 'app/shared/UtilsService/Utils.service';
import { RefModalService } from 'app/shared/services/ref-modal.service';
import { OrganizationUnitService } from 'app/shared/services/organization-unit.service';
import { NotificationService } from 'app/shared/notification';
import { InChungTuHangLoatService } from 'app/shared/in-chung-tu-hang-loat/in-chung-tu-hang-loat.service';
import { IPSTimeSheetSummary } from 'app/shared/models/ps-time-sheet-summary.model';
import { HandlingResultComponent } from 'app/shared/handling-result/handling-result.component';
import { EbReportPdfPopupComponent } from 'app/shared/show-pdf/eb-report-pdf-popup.component';
import { IDataSessionStorage } from 'app/shared/models/DataSessionStorage';

@Component({
    selector: 'eb-ps-salary-sheet',
    templateUrl: './bang-luong.component.html',
    styleUrls: ['./bang-luong.component.css']
})
export class PSSalarySheetComponent extends BaseComponent implements OnInit {
    @ViewChild('popUpMultiDelete') popUpMultiDelete: TemplateRef<any>;
    @ViewChild('contentUnRecord') public modalContentUnRecord: NgbModalRef;
    @ViewChild('contentAddNew') public popUpAddNew: NgbModalRef;
    @ViewChild('popUpDelete') popUpDelete: TemplateRef<any>;
    currentAccount: any;
    pSSalarySheet: IPSSalarySheet[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    modalRef: NgbModalRef;
    countTotalVouchers: number;
    countSuccessVouchers: number;
    countFailVouchers: number;
    optionsDirective = {
        decimal: DECIMAL,
        thousands: THOUSANDS,
        allowNegative: false,
        precision: 2
    };

    formatCurrency = {
        decimal: DECIMAL,
        thousands: THOUSANDS,
        negative: '(',
        precision: 3
    };

    formatSoLuong = {
        decimal: DECIMAL,
        thousands: THOUSANDS,
        negative: '(',
        precision: 3
    };

    CustomCurrencyMaskConfigTienViet: CurrencyMaskConfig = {
        align: 'right',
        allowNegative: false,
        decimal: ',',
        precision: 2,
        prefix: '',
        suffix: '',
        thousands: '.',
        negative: '-',
        autocomplete: 'off'
    };

    CustomCurrencyMaskConfigTienVietNotAllowNegative: CurrencyMaskConfig = {
        align: 'right',
        allowNegative: false,
        decimal: ',',
        precision: 2,
        prefix: '',
        suffix: '',
        thousands: '.',
        negative: '-',
        autocomplete: 'off'
    };

    CustomCurrencyMaskConfigSoLuong: CurrencyMaskConfig = {
        align: 'right',
        allowNegative: false,
        decimal: ',',
        precision: 2,
        prefix: '',
        suffix: '',
        thousands: '.',
        negative: '-',
        autocomplete: 'off'
    };

    isSaving: boolean;
    typeID: number;
    fromDate: any;
    toDate: any;
    recorded: boolean;
    accountingObjectCode: string;
    objSearch: ISearchVoucher;
    pSSalarySheetDetails: IPSSalarySheetDetails[];
    types: IType[];
    typeName: string;
    date: any;
    items: any;
    selectedRow: any;
    rowNum: number;
    pageCount: any;
    accountingObjectName: string;
    isShowSearch: boolean;
    searchValue: string;
    options: any;
    organizationUnitss: IOrganizationUnit[];
    index: any;
    currentRow: any;
    listTypePSSalarySheet: any[];
    listMonth: any[];
    listYear: any[];
    month: number;
    year: number;
    typeTimeSheet: number;
    typeCreate: number;
    autoAddNewEmployee: boolean;
    getEmployeeNotActive: boolean;
    lastTimeSheetID: string;
    listLastPSTimeSheetSummaryNotChange: IPSSalarySheet[];
    listLastPSTimeSheetSummary: IPSSalarySheet[];
    listLastPSTimeSheetNotChange: IPSTimeSheet[];
    listLastPSTimeSheet: IPSTimeSheet[];
    listLastPSSalarySheetNotChange: IPSSalarySheet[];
    listLastPSSalarySheet: IPSSalarySheet[];
    pSSalarySheetName: string;
    pSTimeSheetName: string;
    TYPE_BANG_LUONG_THEO_BUOI = 770;
    TYPE_BANG_LUONG_THEO_GIO = 771;
    TYPE_BANG_LUONG_CB_CO_DINH = 772;
    TYPE_BANG_LUONG_TAM_UNG = 774;
    TYPE_TH_CHAM_CONG_THEO_NGAY = 760;
    TYPE_TH_CHAM_CONG_THEO_GIO = 761;
    TYPE_CHAM_CONG_THEO_NGAY = 750;
    TYPE_CHAM_CONG_THEO_GIO = 751;
    TAO_MOI_HOAN_TOAN = 0;
    DUA_TREN_BANG_THCC = 2;
    DUA_TREN_BANG_CC = 3;
    DUA_TREN_BANG_LUONG = 1;
    pPTinhLuong: any;
    isSingleClick: any;

    buttonDeleteTranslate = 'ebwebApp.mBDeposit.toolTip.delete';
    buttonAddTranslate = 'ebwebApp.mBDeposit.toolTip.add';
    buttonEditTranslate = 'ebwebApp.mBDeposit.toolTip.edit';
    buttonRecordTranslate = 'ebwebApp.mBDeposit.toolTip.record';
    buttonUnRecordTranslate = 'ebwebApp.mBDeposit.toolTip.unrecord';
    buttonExportTranslate = 'ebwebApp.mBDeposit.toolTip.export';
    buttonSearchTranslate = 'ebwebApp.mBDeposit.toolTip.search';

    // navigate update form
    dataSession: IDataSessionStorage = new class implements IDataSessionStorage {
        accountingObjectName: string;
        itemsPerPage: number;
        page: number;
        pageCount: number;
        predicate: number;
        reverse: number;
        rowNum: number;
        totalItems: number;
    }();

    sessionPSTimeSheet: ISessionPSTimeSheet = new class implements ISessionPSTimeSheet {
        month: number;
        year: number;
        pSTimeSheetName?: string;
        pSSalarySheetName?: string;
        pSTimeSalarySheetName?: string;
        typeCreate?: number;
        lastPSTimeSheetID?: string;
        lastPSTimeSheetSummaryID?: string;
        autoAddNewEmployee?: boolean;
        getEmployeeNotActive?: boolean;
        listDepartmentID?: any[];
        dayWorks: number;
    }();

    ROLE_XEM = ROLE.BangLuong_XEM;
    ROLE_THEM = ROLE.BangLuong_Them;
    ROLE_SUA = ROLE.BangLuong_Sua;
    ROLE_XOA = ROLE.BangLuong_Xoa;
    ROLE_IN = ROLE.BangLuong_In;
    ROLE_KETXUAT = ROLE.BangLuong_KetXuat;
    ngayHachToan: any;
    TypeMultiPrintTemplate = TypeMultiPrintTemplate;

    constructor(
        private pSSalarySheetService: BangLuongService,
        private parseLinks: JhiParseLinks,
        private jhiAlertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        public utilsService: UtilsService,
        private datepipe: DatePipe,
        private toastr: ToastrService,
        public translate: TranslateService,
        private organizationUnitService: OrganizationUnitService,
        private refModalService: RefModalService,
        private modalService: NgbModal,
        public activeModal: NgbActiveModal,
        private psSalarySheetService: BangLuongService,
        private pSSalarySheetDetailService: PSSalarySheetDetailsService,
        private psTimeSheetService: PSTimeSheetService,
        private pSTimeSheetSummaryService: PSTimeSheetSummaryService,
        private notificationService: NotificationService,
        private inChungTuHangLoatService: InChungTuHangLoatService
    ) {
        super();
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe(data => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
        });
        // this.accService.getAllDTO().subscribe((res: HttpResponse<IAccountingObject[]>) => {
        //     this.accounting = res.body;
        //     this.accountingObjects = res.body;
        // });
        this.objSearch = new class implements ISearchVoucher {
            accountingObjectID: string;
            currencyID: string;
            fromDate: moment.Moment;
            statusRecorded: boolean;
            textSearch: string;
            toDate: moment.Moment;
            typeID: number;
        }();
        this.principal.identity().then(account => {
            this.currentAccount = account;
            this.isUseToolIn = this.utilsService.checkUseToolIn(account);
            this.ngayHachToan = this.utilsService.getPostedDateDefault(this.currentAccount);
            const dateSearch = this.utilsService.getDataYearWork(this.currentAccount);
            if (dateSearch.fromDate !== '') {
                this.objSearch.fromDate = dateSearch.fromDate.format(DATE_FORMAT);
            }
            if (dateSearch.toDate !== '') {
                this.objSearch.toDate = dateSearch.toDate.format(DATE_FORMAT);
            }
            const formatTienViet = this.currentAccount.systemOption.find(a => a.code === 'DDSo_TienVND');
            const formatSoLuong = this.currentAccount.systemOption.find(a => a.code === 'DDSo_SoLuong');
            this.formatCurrency.precision = formatTienViet ? parseInt(formatTienViet.data, 0) : 0;
            this.formatSoLuong.precision = formatSoLuong ? parseInt(formatSoLuong.data, 0) : 0;
            this.pPTinhLuong = this.currentAccount.systemOption.find(a => a.code === 'TCKHAC_TinhLuong').data;
            this.CustomCurrencyMaskConfigTienViet = this.utilsService.getConfigCurrencyWithAcc(account, DDSo_TienVND, false);
            this.CustomCurrencyMaskConfigTienVietNotAllowNegative = this.utilsService.getConfigCurrencyWithAcc(account, DDSo_TienVND, true);
            this.CustomCurrencyMaskConfigSoLuong = this.utilsService.getConfigCurrencyWithAcc(account, DDSo_TienVND, false);
            this.formatCurrency = this.utilsService.getConfigCurrencyWithAccWithPrec(account, false, 3);
            this.formatSoLuong = this.utilsService.getConfigCurrencyWithAccWithPrec(account, false, 3);
            this.optionsDirective = this.utilsService.getConfigCurrencyWithAccWithPrec(account, false, 2);
        });
        this.translate
            .get([
                'ebwebApp.pSTimeSheet.month',
                'ebwebApp.pSSalarySheet.salarySheetByDays',
                'ebwebApp.pSSalarySheet.salarySheetByHours',
                'ebwebApp.pSSalarySheet.salarySheetDefault',
                'ebwebApp.pSSalarySheet.salarySheetPayroll'
            ])
            .subscribe(res => {
                this.listTypePSSalarySheet = [
                    { value: this.TYPE_BANG_LUONG_THEO_BUOI, name: res['ebwebApp.pSSalarySheet.salarySheetByDays'] },
                    { value: this.TYPE_BANG_LUONG_THEO_GIO, name: res['ebwebApp.pSSalarySheet.salarySheetByHours'] },
                    { value: this.TYPE_BANG_LUONG_CB_CO_DINH, name: res['ebwebApp.pSSalarySheet.salarySheetDefault'] },
                    { value: this.TYPE_BANG_LUONG_TAM_UNG, name: res['ebwebApp.pSSalarySheet.salarySheetPayroll'] }
                ];
                this.listMonth = [
                    { value: 1, name: res['ebwebApp.pSTimeSheet.month'] + ' 1' },
                    { value: 2, name: res['ebwebApp.pSTimeSheet.month'] + ' 2' },
                    { value: 3, name: res['ebwebApp.pSTimeSheet.month'] + ' 3' },
                    { value: 4, name: res['ebwebApp.pSTimeSheet.month'] + ' 4' },
                    { value: 5, name: res['ebwebApp.pSTimeSheet.month'] + ' 5' },
                    { value: 6, name: res['ebwebApp.pSTimeSheet.month'] + ' 6' },
                    { value: 7, name: res['ebwebApp.pSTimeSheet.month'] + ' 7' },
                    { value: 8, name: res['ebwebApp.pSTimeSheet.month'] + ' 8' },
                    { value: 9, name: res['ebwebApp.pSTimeSheet.month'] + ' 9' },
                    { value: 10, name: res['ebwebApp.pSTimeSheet.month'] + ' 10' },
                    { value: 11, name: res['ebwebApp.pSTimeSheet.month'] + ' 11' },
                    { value: 12, name: res['ebwebApp.pSTimeSheet.month'] + ' 12' }
                ];
                this.listYear = [];
                for (let i = 1950; i <= 2050; i++) {
                    this.listYear.push({ value: i });
                }
                const currentDate = new Date();
                this.month = currentDate.getMonth() + 1;
                this.year = currentDate.getFullYear();
                this.typeID = this.TYPE_BANG_LUONG_THEO_BUOI;
                this.typeCreate = this.TAO_MOI_HOAN_TOAN;
                this.pSSalarySheetName = '';
                if (this.typeID === this.TYPE_BANG_LUONG_THEO_BUOI) {
                    this.pSSalarySheetName += this.listTypePSSalarySheet[0].name;
                } else if (this.typeID === this.TYPE_BANG_LUONG_THEO_GIO) {
                    this.pSSalarySheetName += this.listTypePSSalarySheet[1].name;
                } else if (this.typeID === this.TYPE_BANG_LUONG_CB_CO_DINH) {
                    this.pSSalarySheetName += this.listTypePSSalarySheet[2].name;
                } else {
                    this.pSSalarySheetName += this.listTypePSSalarySheet[3].name;
                }
                this.pSSalarySheetName +=
                    ' ' +
                    this.translate.instant('ebwebApp.pSTimeSheet.month') +
                    ' ' +
                    (this.month ? this.month : '') +
                    ' ' +
                    this.translate.instant('ebwebApp.pSTimeSheet.year') +
                    ' ' +
                    (this.year ? this.year : '');
            });
    }

    loadAll() {
        if (sessionStorage.getItem('dataSession')) {
            this.dataSession = JSON.parse(sessionStorage.getItem('dataSession'));
            this.page = this.dataSession.page;
            this.itemsPerPage = this.dataSession.itemsPerPage;
            this.isShowSearch = this.dataSession.isShowSearch;
            this.predicate = this.dataSession.predicate;
            this.reverse = this.dataSession.reverse;
            this.previousPage = this.dataSession.page;
            sessionStorage.removeItem('dataSession');
        }
        if (sessionStorage.getItem('dataSearchPSTimeSheet')) {
            this.objSearch = JSON.parse(sessionStorage.getItem('dataSearchPSTimeSheet'));
            sessionStorage.removeItem('dataSearchPSTimeSheet');
        }
        if (this.activatedRoute.snapshot.paramMap.has('isSearch')) {
            this.pSSalarySheetService
                .searchAll({
                    page: this.page - 1,
                    size: this.itemsPerPage,
                    searchVoucher: JSON.stringify(this.saveSearchVoucher())
                })
                .subscribe(
                    (res: HttpResponse<IPSSalarySheet[]>) => this.paginatePSTimeSheetSummaries(res.body, res.headers),
                    (res: HttpErrorResponse) => this.onError(res.message)
                );
            return;
        } else {
            this.router.navigate(['/bang-luong']);
        }
        this.pSSalarySheetService
            .searchAll({
                page: this.page - 1,
                size: this.itemsPerPage,
                searchVoucher: JSON.stringify(this.saveSearchVoucher())
            })
            .subscribe(
                (res: HttpResponse<IPSSalarySheet[]>) => this.paginatePSTimeSheetSummaries(res.body, res.headers),
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    loadPage(page: number) {
        this.selectedRows = [];
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    getRowNumberOfRecord(page: number, index: number): number {
        if (page > 0 && index !== -1) {
            return (page - 1) * this.itemsPerPage + index + 1;
        }
    }

    transition() {
        this.dataSession.rowNum = 0;
        this.router.navigate(['/bang-luong']);
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.router.navigate(['/bang-luong']);
        this.loadAll();
    }

    ngOnInit() {
        this.listLastPSTimeSheet = [];
        this.listLastPSTimeSheetSummary = [];
        this.listLastPSSalarySheet = [];
        this.loadAll();
        if (sessionStorage.getItem('checkNewBangLuong')) {
            this.addNew(false);
            sessionStorage.removeItem('checkNewBangLuong');
        }
        this.pSTimeSheetSummaryService.getPSTimeSheetsSummaries().subscribe((res: HttpResponse<IPSTimeSheetSummary[]>) => {
            this.listLastPSTimeSheetSummary = res.body.filter(a => a.typeID === this.TYPE_TH_CHAM_CONG_THEO_NGAY);
            // this.listLastPSTimeSheetSummary = res.body.filter(a => a.typeID === this.TYPE_TH_CHAM_CONG_THEO_NGAY);
            this.listLastPSTimeSheetSummaryNotChange = res.body;
        });
        this.psTimeSheetService.getPSTimeSheets().subscribe((res: HttpResponse<IPSTimeSheet[]>) => {
            // this.listLastPSTimeSheet = res.body.filter(a => a.typeID === this.TYPE_CHAM_CONG_THEO_NGAY);
            this.listLastPSTimeSheet = res.body.filter(a => a.typeID === this.TYPE_CHAM_CONG_THEO_NGAY);
            this.listLastPSTimeSheetNotChange = res.body;
        });
        this.psSalarySheetService.getPSSalarySheets().subscribe((res: HttpResponse<IPSSalarySheet[]>) => {
            // this.listLastPSSalarySheet = res.body.filter(a => a.typeID === this.TYPE_CHAM_CONG_THEO_NGAY);
            this.listLastPSSalarySheet = res.body.filter(a => a.typeID === this.TYPE_BANG_LUONG_THEO_BUOI);
            this.listLastPSSalarySheetNotChange = res.body;
        });
        this.registerChangeInPSTimeSheetSummaries();
        this.registerExport();
    }

    registerChangeInPSTimeSheetSummaries() {
        this.eventSubscriber = this.eventManager.subscribe('pSSalarySheetListModification', response => this.loadAll());
    }

    selectedItemPerPage() {
        this.selectedRows = [];
        this.loadAll();
    }

    // ngOnDestroy() {
    //     this.eventManager.destroy(this.eventSubscriber);
    // }

    trackId(index: number, item: IPSSalarySheet) {
        return item.id;
    }

    sort() {
        const result = ['date' + ',' + (this.reverse ? 'desc' : 'asc')];
        result.push('postedDate' + ',' + (this.reverse ? 'desc' : 'asc'));
        return result;
    }

    private paginatePSTimeSheetSummaries(data: IPSSalarySheet[], headers: HttpHeaders) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = parseInt(headers.get('X-Total-Count'), 10);
        this.queryCount = this.totalItems;
        this.pSSalarySheet = data;
        this.objects = data;
        if (this.page > 1 && this.pSSalarySheet && this.pSSalarySheet.length === 0) {
            this.page = this.page - 1;
            this.loadAll();
            return;
        }

        if (this.objects && this.objects.length > 0) {
            if (this.dataSession && this.dataSession.rowNum) {
                this.selectedRows.push(this.pSSalarySheet[this.dataSession.rowNum]);
                this.selectedRow = this.pSSalarySheet[this.dataSession.rowNum];
            } else {
                this.selectedRows.push(this.pSSalarySheet[0]);
                this.selectedRow = this.pSSalarySheet[0];
            }
        } else {
            this.selectedRow = null;
            this.pSSalarySheetDetails = null;
        }
        this.onSelect(this.selectedRow);
        //  display pageCount
        this.pageCount = Math.ceil(this.totalItems / this.itemsPerPage);
    }

    private onError(errorMessage: string) {
        this.toastr.error(this.translate.instant('ebwebApp.mBDeposit.error'), this.translate.instant('ebwebApp.mBDeposit.message'));
    }

    onSelect(select: IPSSalarySheet, data = null) {
        this.isSingleClick = true;
        setTimeout(() => {
            if (this.isSingleClick) {
                if (select) {
                    this.selectedRow = select;
                    this.psSalarySheetService.find(select.id).subscribe((res: HttpResponse<IPSSalarySheet>) => {
                        this.pSSalarySheetDetails =
                            res.body.pSSalarySheetDetails === undefined
                                ? []
                                : res.body.pSSalarySheetDetails.sort((a, b) => a.orderPriority - b.orderPriority);
                        for (let i = 0; i < this.pSSalarySheetDetails.length; i++) {
                            this.pSSalarySheetDetails[i].stt = i + 1;
                        }
                        this.lstDataForSetDefault = [];
                        this.lstDataForSetDefault.push(this.pSSalarySheetDetails);
                        this.loadDataDefaultForOpenVoucher(this.lstDataForSetDefault);
                    });
                }
            }
        }, 250);
    }

    get pSTimeSheet() {
        return this.selectedRow;
    }

    set pSTimeSheet(pSTimeSheet: IPSSalarySheet) {
        this.selectedRow = pSTimeSheet;
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IPSSalarySheet>>) {
        result.subscribe((res: HttpResponse<IPSSalarySheet>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
    }

    private onSaveError() {
        this.isSaving = false;
    }

    doubleClickRow(any?) {
        this.isSingleClick = false;
        this.edit();
    }

    @ebAuth(['ROLE_ADMIN', ROLE.BangLuong_XEM])
    edit() {
        event.preventDefault();
        this.dataSession.page = this.page;
        this.dataSession.itemsPerPage = this.itemsPerPage;
        this.dataSession.pageCount = this.pageCount;
        this.dataSession.totalItems = this.totalItems;
        this.dataSession.rowNum = this.pSSalarySheet.indexOf(this.selectedRow);
        this.dataSession.isShowSearch = this.isShowSearch;
        // sort
        this.dataSession.predicate = this.predicate;
        this.dataSession.accountingObjectName = this.accountingObjectName;
        this.dataSession.reverse = this.reverse;
        this.sessionPSTimeSheet.month = this.month;
        this.sessionPSTimeSheet.year = this.year;
        this.sessionPSTimeSheet.typeID = this.typeID;
        this.sessionPSTimeSheet.pSTimeSheetName = this.pSTimeSheetName;
        this.sessionPSTimeSheet.pSSalarySheetName = this.pSSalarySheetName;
        this.sessionPSTimeSheet.typeCreate = this.typeCreate;
        this.sessionPSTimeSheet.lastPSTimeSheetID = this.lastTimeSheetID;
        this.sessionPSTimeSheet.autoAddNewEmployee = this.autoAddNewEmployee;
        this.sessionPSTimeSheet.getEmployeeNotActive = this.getEmployeeNotActive;
        sessionStorage.setItem('dataSession', JSON.stringify(this.dataSession));
        sessionStorage.setItem('sessionPSSalarySheet', JSON.stringify(this.sessionPSTimeSheet));
        sessionStorage.setItem('dataSearchPSTimeSheet', JSON.stringify(this.saveSearchVoucher()));
        this.router.navigate(['./bang-luong', this.selectedRow.id, 'edit']);
        // }
    }

    showPopupAddNew() {
        event.preventDefault();
        this.dataSession.page = this.page;
        this.dataSession.itemsPerPage = this.itemsPerPage;
        this.dataSession.pageCount = this.pageCount;
        this.dataSession.totalItems = this.totalItems;
        this.dataSession.rowNum = this.rowNum;
        // sort
        this.dataSession.predicate = this.predicate;
        this.dataSession.accountingObjectName = this.accountingObjectName;
        this.dataSession.reverse = this.reverse;
        this.sessionPSTimeSheet.month = this.month;
        this.sessionPSTimeSheet.year = this.year;
        this.sessionPSTimeSheet.typeID = this.typeID;
        this.sessionPSTimeSheet.pSTimeSheetName = this.pSTimeSheetName;
        this.sessionPSTimeSheet.pSSalarySheetName = this.pSSalarySheetName;
        this.sessionPSTimeSheet.typeCreate = this.typeCreate;
        this.sessionPSTimeSheet.autoAddNewEmployee = this.autoAddNewEmployee ? this.autoAddNewEmployee : false;
        this.sessionPSTimeSheet.getEmployeeNotActive = this.getEmployeeNotActive ? this.getEmployeeNotActive : false;
        this.sessionPSTimeSheet.listDepartmentID = this.organizationUnitss.filter(n => n.checked).map(n => n.id);
        if (!this.sessionPSTimeSheet.daysWork) {
            this.toastr.error(
                this.translate.instant('ebwebApp.pSSalarySheet.daysWorkMustBeGreaterThan0'),
                this.translate.instant('ebwebApp.pSTimeSheet.message')
            );
            return;
        }
        if (!this.sessionPSTimeSheet.pSSalarySheetName) {
            this.toastr.error(
                this.translate.instant('ebwebApp.pSSalarySheet.pSSalarySheetNameNotBeBlank'),
                this.translate.instant('ebwebApp.pSTimeSheet.message')
            );
            return;
        }
        if (!this.sessionPSTimeSheet.month) {
            this.toastr.error(
                this.translate.instant('ebwebApp.pSTimeSheet.monthNotBeBlank'),
                this.translate.instant('ebwebApp.pSTimeSheet.message')
            );
            return;
        }
        if (!this.sessionPSTimeSheet.year) {
            this.toastr.error(
                this.translate.instant('ebwebApp.pSTimeSheet.yearNotBeBlank'),
                this.translate.instant('ebwebApp.pSTimeSheet.message')
            );
            return;
        }
        if (this.typeCreate !== this.TAO_MOI_HOAN_TOAN) {
            if (!this.sessionPSTimeSheet.lastPSTimeSheetID) {
                if (this.typeCreate === this.DUA_TREN_BANG_THCC) {
                    this.toastr.error(
                        this.translate.instant('ebwebApp.pSTimeSheetSummary.lastPSTimeSheetSummaryIDNotBeBlank'),
                        this.translate.instant('ebwebApp.pSTimeSheet.message')
                    );
                    return;
                } else if (this.typeCreate === this.DUA_TREN_BANG_CC) {
                    this.toastr.error(
                        this.translate.instant('ebwebApp.pSTimeSheet.lastPSTimeSheetIDNotBeBlank'),
                        this.translate.instant('ebwebApp.pSTimeSheet.message')
                    );
                    return;
                } else if (this.typeCreate === this.DUA_TREN_BANG_LUONG) {
                    this.toastr.error(
                        this.translate.instant('ebwebApp.pSSalarySheet.lastPSSalarySheetIDNotBeBlank'),
                        this.translate.instant('ebwebApp.pSTimeSheet.message')
                    );
                    return;
                }
            }
        }
        // let lastDay = new Date(this.sessionPSTimeSheet.year, this.sessionPSTimeSheet.month + 1, 0);
        // if (this.checkCloseBook(this.currentAccount, lastDay)) {
        //     this.toastr.error(
        //         this.translate.instant('ebwebApp.pSTimeSheet.isHasCloseBook'),
        //         this.translate.instant('ebwebApp.pSTimeSheet.message')
        //     );
        //     return;
        // }
        // if (this.sessionPSTimeSheet.listDepartmentID === null || this.sessionPSTimeSheet.listDepartmentID.length === 0) {
        //     this.toastr.error(
        //         this.translate.instant('ebwebApp.pSTimeSheet.departmentIDNotBeBlank'),
        //         this.translate.instant('ebwebApp.pSTimeSheet.message')
        //     );
        //     return;
        // }
        if (this.modalRef) {
            this.modalRef.close();
            this.isShowPopup = false;
        }
        this.psSalarySheetService
            .checkExceedPSSalarySheet({
                month: this.month,
                year: this.year,
                typeID: this.typeID
            })
            .subscribe(
                (res: HttpResponse<any>) => {
                    sessionStorage.setItem('sessionPSSalarySheet', JSON.stringify(this.sessionPSTimeSheet));
                    this.router.navigate(['bang-luong/', 'new']);
                },
                (res: HttpErrorResponse) => {
                    if (res.error.errorKey === 'exceedPSSalarySheet') {
                        if (this.typeID === this.TYPE_BANG_LUONG_TAM_UNG) {
                            this.toastr.error(
                                this.translate.instant('ebwebApp.pSSalarySheet.exceedAnotherPSSalarySheet'),
                                this.translate.instant('ebwebApp.mBDeposit.message')
                            );
                        } else {
                            this.toastr.error(
                                this.translate.instant('ebwebApp.pSSalarySheet.exceedPSSalarySheetTU'),
                                this.translate.instant('ebwebApp.mBDeposit.message')
                            );
                        }
                    }
                }
            );
    }

    @ebAuth(['ROLE_ADMIN', ROLE.BangLuong_Xoa])
    delete() {
        event.preventDefault();
        if (this.selectedRow) {
            if (this.selectedRows.length > 1) {
                this.modalRef = this.modalService.open(this.popUpMultiDelete, { backdrop: 'static' });
            } else if (this.selectedRows && this.selectedRows.length === 1) {
                if (this.selectedRow.gOtherVoucherID) {
                    this.toastr.error(
                        this.translate.instant('ebwebApp.pSSalarySheet.existGOtherVoucherID'),
                        this.translate.instant('ebwebApp.mBDeposit.message')
                    );
                } else {
                    this.modalRef = this.modalService.open(this.popUpDelete, { backdrop: 'static' });
                }
            }
        }
    }

    continueDelete() {
        this.pSSalarySheetService.multiDelete(this.selectedRows).subscribe(
            (res: HttpResponse<any>) => {
                if (this.modalRef) {
                    this.modalRef.close();
                }
                this.selectedRows = [];
                this.loadAll();
                if (res.body.countTotalVouchers !== res.body.countSuccessVouchers) {
                    this.modalRef = this.refModalService.open(
                        res.body,
                        HandlingResultComponent,
                        null,
                        false,
                        770,
                        null,
                        null,
                        null,
                        null,
                        true
                    );
                } else {
                    this.toastr.success(this.translate.instant('ebwebApp.pSSalarySheet.deleteSuccessful'));
                }
                this.activeModal.close();
                // }
            },
            (res: HttpErrorResponse) => {
                if (res.error.errorKey === 'errorDeleteList') {
                    this.toastr.error(
                        this.translate.instant('ebwebApp.pSSalarySheet.errorDeleteVoucherNo'),
                        this.translate.instant('ebwebApp.mBDeposit.message')
                    );
                }
                if (this.modalRef) {
                    this.modalRef.close();
                }
            }
        );
    }

    closePopUpDelete() {
        if (this.modalRef) {
            this.modalRef.close();
            this.inPopup = false;
        }
    }

    getAccountingObjectbyID(id) {
        if (this.accountingObjects) {
            const acc = this.accountingObjects.find(n => n.id === id);
            if (acc) {
                return acc.accountingObjectCode;
            }
        }
    }

    getOrganizationUnitbyID(id) {
        if (this.organizationUnits) {
            const organizationUnit = this.organizationUnits.find(n => n.id === id);
            if (organizationUnit) {
                return organizationUnit.organizationUnitCode;
            }
        }
    }

    registerExport() {
        this.eventSubscriber = this.eventManager.subscribe(`export-excel-${this.TYPE_BANG_LUONG_THEO_BUOI}`, () => {
            this.exportExcel();
        });
    }

    exportExcel() {
        this.pSSalarySheetService.exportExcel().subscribe(
            res => {
                const blob = new Blob([res.body], { type: 'application/vnd.ms-excel' });
                const fileURL = URL.createObjectURL(blob);

                const link = document.createElement('a');
                document.body.appendChild(link);
                link.download = fileURL;
                link.setAttribute('style', 'display: none');
                const name = 'DS_BangLuong.xls';
                link.setAttribute('download', name);
                link.href = fileURL;
                link.click();
            },
            () => {}
        );
    }

    getTypeName(typeID) {
        if (typeID && this.types) {
            const currentType = this.types.find(type => type.id === typeID);
            if (currentType) {
                return currentType.typeName;
            } else {
                return '';
            }
        }
    }

    getEmployeeByID(id) {
        if (this.employees) {
            const epl = this.employees.find(n => n.id === id);
            if (epl) {
                return epl.accountingObjectCode;
            } else {
                return '';
            }
        }
    }

    getEmployeeName(id) {
        if (this.employees) {
            const epl = this.employees.find(n => n.id === id);
            if (epl) {
                return epl.accountingObjectName;
            } else {
                return '';
            }
        }
    }

    getAccountingObjectTitle(id) {
        if (this.accountingObjects) {
            const epl = this.accountingObjects.find(n => n.id === id);
            if (epl) {
                return epl.contactTitle;
            } else {
                return '';
            }
        }
    }

    newArr(lenght: number): any[] {
        if (lenght > 0) {
            return new Array(lenght);
        } else {
            return new Array(0);
        }
    }

    @ebAuth(['ROLE_ADMIN', ROLE.BangLuong_Them])
    addNew(isNew = false) {
        event.preventDefault();
        if (this.modalRef) {
            this.modalRef.close();
        }
        this.organizationUnitService.getOrgForSalary().subscribe((resGetOrg: HttpResponse<IOrganizationUnit[]>) => {
            this.organizationUnitss = resGetOrg.body.sort((a, b) => a.organizationUnitCode.localeCompare(b.organizationUnitCode));
            this.unCheckAll();
            this.pSTimeSheetSummaryService.getPSTimeSheetsSummaries().subscribe((res: HttpResponse<IPSTimeSheetSummary[]>) => {
                this.listLastPSTimeSheetSummary = res.body;
                // this.listLastPSTimeSheetSummary = resGetOrg.body.filter(a => a.typeID === this.TYPE_TH_CHAM_CONG_THEO_NGAY);
                this.listLastPSTimeSheetSummaryNotChange = res.body;
            });
            this.psTimeSheetService.getPSTimeSheets().subscribe((res: HttpResponse<IPSTimeSheet[]>) => {
                // this.listLastPSTimeSheet = resGetOrg.body.filter(a => a.typeID === this.TYPE_CHAM_CONG_THEO_NGAY);
                this.listLastPSTimeSheet = res.body;
                this.listLastPSTimeSheetNotChange = res.body;
            });
            this.psSalarySheetService.getPSSalarySheets().subscribe((res: HttpResponse<IPSSalarySheet[]>) => {
                // this.listLastPSSalarySheet = resGetOrg.body.filter(a => a.typeID === this.TYPE_CHAM_CONG_THEO_NGAY);
                this.listLastPSSalarySheet = res.body;
                this.listLastPSSalarySheetNotChange = res.body;
            });
            this.typeCreate = this.TAO_MOI_HOAN_TOAN;
            this.sessionPSTimeSheet.daysWork = 0;
            this.month = this.ngayHachToan.month() + 1;
            this.year = this.ngayHachToan.year();
            this.selectChangePSSheetName();
            this.modalRef = this.modalService.open(this.popUpAddNew, { backdrop: 'static', size: 'lg' });
            document.getElementById('field_typeID').focus();
            this.inPopup = true;
        });
        // this.router.navigate(['/bang-luong/new']);
    }

    unCheckAll() {
        if (this.organizationUnitss) {
            this.organizationUnits.forEach(item => (item.checked = false));
        }
    }

    isCheckAll() {
        if (this.organizationUnitss) {
            return this.organizationUnitss.every(item => item.checked) && this.organizationUnitss.length;
        } else {
            return false;
        }
    }

    checkAll() {
        const isCheck = this.organizationUnitss.every(item => item.checked) && this.organizationUnitss.length;
        this.organizationUnitss.forEach(item => (item.checked = !isCheck));
    }

    check(organizationUnit: IOrganizationUnit) {
        organizationUnit.checked = !organizationUnit.checked;
    }

    selectChangePSSheetName() {
        this.sessionPSTimeSheet.lastPSTimeSheetID = null;
        this.typeCreate = 0;
        if (this.listLastPSSalarySheetNotChange && this.listLastPSSalarySheetNotChange.length > 0) {
            this.listLastPSSalarySheet = this.listLastPSSalarySheetNotChange.filter(a => a.typeID === this.typeID);
        }
        if (
            this.typeID === this.TYPE_BANG_LUONG_THEO_BUOI &&
            this.listLastPSTimeSheetNotChange &&
            this.listLastPSTimeSheetNotChange.length > 0
        ) {
            this.listLastPSTimeSheet = this.listLastPSTimeSheetNotChange.filter(a => a.typeID === this.TYPE_CHAM_CONG_THEO_NGAY);
            this.listLastPSTimeSheetSummary = this.listLastPSTimeSheetSummaryNotChange.filter(
                a => a.typeID === this.TYPE_TH_CHAM_CONG_THEO_NGAY
            );
        } else if (
            this.typeID === this.TYPE_BANG_LUONG_THEO_GIO &&
            this.listLastPSTimeSheetNotChange &&
            this.listLastPSTimeSheetNotChange.length > 0
        ) {
            this.listLastPSTimeSheet = this.listLastPSTimeSheetNotChange.filter(a => a.typeID === this.TYPE_CHAM_CONG_THEO_GIO);
            this.listLastPSTimeSheetSummary = this.listLastPSTimeSheetSummaryNotChange.filter(
                a => a.typeID === this.TYPE_TH_CHAM_CONG_THEO_GIO
            );
        }
        if (this.typeID === this.TYPE_BANG_LUONG_THEO_BUOI) {
            this.pSSalarySheetName = this.listTypePSSalarySheet[0].name;
        } else if (this.typeID === this.TYPE_BANG_LUONG_THEO_GIO) {
            this.pSSalarySheetName = this.listTypePSSalarySheet[1].name;
        } else if (this.typeID === this.TYPE_BANG_LUONG_CB_CO_DINH) {
            this.pSSalarySheetName = this.listTypePSSalarySheet[2].name;
        } else {
            this.pSSalarySheetName = this.listTypePSSalarySheet[3].name;
        }
        this.pSSalarySheetName +=
            ' ' +
            this.translate.instant('ebwebApp.pSTimeSheet.month') +
            ' ' +
            (this.month ? this.month : '') +
            ' ' +
            this.translate.instant('ebwebApp.pSTimeSheet.year') +
            ' ' +
            (this.year ? this.year : '');
    }

    getTypeTimeSheet(typeID) {
        if (typeID && typeID === this.TYPE_BANG_LUONG_THEO_BUOI) {
            return this.listTypePSSalarySheet ? this.listTypePSSalarySheet[0].name : null;
        } else if (typeID && typeID === this.TYPE_BANG_LUONG_THEO_GIO) {
            return this.listTypePSSalarySheet ? this.listTypePSSalarySheet[1].name : null;
        } else if (typeID && typeID === this.TYPE_BANG_LUONG_CB_CO_DINH) {
            return this.listTypePSSalarySheet ? this.listTypePSSalarySheet[2].name : null;
        } else if (typeID && typeID === this.TYPE_BANG_LUONG_TAM_UNG) {
            return this.listTypePSSalarySheet ? this.listTypePSSalarySheet[3].name : null;
        }
    }

    getDepartmentID(id) {
        if (this.organizationUnits && this.organizationUnits.length > 0) {
            const department = this.organizationUnits.find(a => a.id === id);
            if (department) {
                return department.organizationUnitCode;
            }
        }
    }

    changeTypeCreate() {
        if (this.typeCreate === this.TAO_MOI_HOAN_TOAN) {
            this.autoAddNewEmployee = false;
            this.getEmployeeNotActive = false;
            this.lastTimeSheetID = null;
        }
        if (this.typeCreate === this.DUA_TREN_BANG_LUONG) {
            this.listLastPSSalarySheet = this.listLastPSSalarySheetNotChange.filter(a => a.typeID === this.typeID);
        } else if (this.typeCreate === this.DUA_TREN_BANG_CC) {
            if (this.typeID === this.TYPE_BANG_LUONG_THEO_BUOI) {
                this.listLastPSTimeSheet = this.listLastPSTimeSheetNotChange.filter(a => a.typeID === this.TYPE_CHAM_CONG_THEO_NGAY);
            } else {
                this.listLastPSTimeSheet = this.listLastPSTimeSheetNotChange.filter(a => a.typeID === this.TYPE_CHAM_CONG_THEO_GIO);
            }
            for (let i = 0; i < this.listLastPSTimeSheet.length; i++) {
                if (this.listLastPSTimeSheet[i].typeLedger === 0) {
                    this.listLastPSTimeSheet[i].typeLedgerStr = 'Sổ tài chính';
                } else if (this.listLastPSTimeSheet[i].typeLedger === 1) {
                    this.listLastPSTimeSheet[i].typeLedgerStr = 'Sổ quản trị';
                }
            }
        } else if (this.typeCreate === this.DUA_TREN_BANG_THCC) {
            if (this.typeID === this.TYPE_BANG_LUONG_THEO_BUOI) {
                this.listLastPSTimeSheetSummary = this.listLastPSTimeSheetSummaryNotChange.filter(
                    a => a.typeID === this.TYPE_TH_CHAM_CONG_THEO_NGAY
                );
            } else {
                this.listLastPSTimeSheetSummary = this.listLastPSTimeSheetSummaryNotChange.filter(
                    a => a.typeID === this.TYPE_TH_CHAM_CONG_THEO_GIO
                );
            }
        }
    }

    @ebAuth(['ROLE_ADMIN', ROLE.BangLuong_KetXuat])
    export() {
        event.preventDefault();
        if (this.pSSalarySheet.length > 0) {
            this.psSalarySheetService.exportPDF({ searchVoucher: JSON.stringify(this.saveSearchVoucher()) }).subscribe(res => {
                this.refModalService.open(null, EbReportPdfPopupComponent, res, false, this.TYPE_BANG_LUONG_THEO_BUOI);
            });
        }
    }

    checkNameLabel() {
        if (this.typeCreate === this.DUA_TREN_BANG_CC) {
            return 'ebwebApp.pSTimeSheet.duaTrenBCC';
        } else if (this.typeCreate === this.DUA_TREN_BANG_THCC) {
            return 'ebwebApp.pSTimeSheetSummary.duaTrenBCC';
        } else if (this.typeCreate === this.DUA_TREN_BANG_LUONG) {
            return 'ebwebApp.pSSalarySheet.duaTrenBCC';
        }
    }

    checkList() {
        if (this.typeCreate === this.DUA_TREN_BANG_CC) {
            return this.listLastPSTimeSheet;
        } else if (this.typeCreate === this.DUA_TREN_BANG_THCC) {
            return this.listLastPSTimeSheetSummary;
        } else if (this.typeCreate === this.DUA_TREN_BANG_LUONG) {
            return this.listLastPSSalarySheet;
        }
    }

    checkListColumns() {
        if (this.typeCreate === this.DUA_TREN_BANG_CC) {
            return ['pSTimeSheetName', 'typeLedgerStr'];
        } else if (this.typeCreate === this.DUA_TREN_BANG_THCC) {
            return ['pSTimeSheetSummaryName'];
        } else if (this.typeCreate === this.DUA_TREN_BANG_LUONG) {
            return ['pSSalarySheetName'];
        }
    }

    checkListDisplayMember() {
        if (this.typeCreate === this.DUA_TREN_BANG_CC) {
            return 'pSTimeSheetName';
        } else if (this.typeCreate === this.DUA_TREN_BANG_THCC) {
            return 'pSTimeSheetSummaryName';
        } else if (this.typeCreate === this.DUA_TREN_BANG_LUONG) {
            return 'pSSalarySheetName';
        }
    }

    checkHeaderColumns() {
        if (this.typeCreate === this.DUA_TREN_BANG_CC) {
            return ['Kỳ chấm công', 'Sổ làm việc'];
        } else if (this.typeCreate === this.DUA_TREN_BANG_THCC) {
            return ['Bảng tổng hợp chấm công'];
        } else if (this.typeCreate === this.DUA_TREN_BANG_LUONG) {
            return ['Bảng lương'];
        }
    }

    sumFooter(item) {
        if (this.selectedRow && this.pSSalarySheetDetails && this.pSSalarySheetDetails.length > 0) {
            let total = 0;
            for (let i = 0; i < this.pSSalarySheetDetails.length; i++) {
                total += this.pSSalarySheetDetails[i][item];
            }
            return total;
        }
        return '';
    }

    continueDeletePSSalarySheet() {
        this.pSSalarySheetService.delete(this.selectedRow.id).subscribe(response => {
            this.modalRef.close();
            this.toastr.success(this.translate.instant('ebwebApp.pSSalarySheet.deleteSuccessful'));
            this.loadAll();
        });
    }

    clearSearch() {
        this.selectedRows = [];
        this.objSearch = {};
        const dateSearch = this.utilsService.getDataYearWork(this.currentAccount);
        if (dateSearch.fromDate !== '') {
            this.objSearch.fromDate = dateSearch.fromDate.format(DATE_FORMAT);
        }
        if (dateSearch.toDate !== '') {
            this.objSearch.toDate = dateSearch.toDate.format(DATE_FORMAT);
        }
        this.pSSalarySheetService
            .searchAll({
                page: this.page - 1,
                size: this.itemsPerPage,
                sort: this.sort(),
                searchVoucher: JSON.stringify(this.saveSearchVoucher())
            })
            .subscribe(
                (res: HttpResponse<any[]>) => this.paginatePSTimeSheetSummaries(res.body, res.headers),
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    saveSearchVoucher(): ISearchVoucher {
        const searchObj: ISearchVoucher = {};
        searchObj.typeID = this.objSearch.typeID !== undefined ? this.objSearch.typeID : null;
        // searchObj.statusRecorded = this.objSearch.statusRecorded !== undefined ? this.objSearch.statusRecorded : null;
        // searchObj.currencyID = this.objSearch.currencyID !== undefined ? this.objSearch.currencyID : null;
        searchObj.fromDate = this.objSearch.fromDate !== undefined ? this.objSearch.fromDate : null;
        searchObj.toDate = this.objSearch.toDate !== undefined ? this.objSearch.toDate : null;
        // searchObj.accountingObjectID = this.objSearch.accountingObjectID !== undefined ? this.objSearch.accountingObjectID : null;
        searchObj.textSearch = this.objSearch.textSearch !== undefined ? this.objSearch.textSearch : null;
        return searchObj;
    }

    toggleSearch($event) {
        $event.preventDefault();
        this.isShowSearch = !this.isShowSearch;
    }

    search() {
        this.selectedRows = [];
        this.dataSession.rowNum = 0;
        if (this.objSearch.fromDate > this.objSearch.toDate) {
            this.toastr.error(
                this.translate.instant('ebwebApp.mBDeposit.fromDateMustBeLessThanToDate'),
                this.translate.instant('ebwebApp.mBDeposit.message')
            );
            return;
        }
        sessionStorage.setItem('dataSearchPSTimeSheet', JSON.stringify(this.saveSearchVoucher()));
        this.pSSalarySheetService
            .searchAll({
                page: this.page - 1,
                size: this.itemsPerPage,
                searchVoucher: JSON.stringify(this.saveSearchVoucher())
            })
            .subscribe(
                (res: HttpResponse<any[]>) => this.paginatePSTimeSheetSummaries(res.body, res.headers),
                (res: HttpErrorResponse) => this.onError(res.message)
            );
        this.page = 1;
        this.loadPage(this.page);
    }

    navigateHelp() {
        this.notificationService.findHelpLink({ id: 'BL' }).subscribe((res: HttpResponse<any>) => {
            window.open(res.body, '_blank');
        });
    }

    exportPDFWebService(valueVoucher?) {
        const listID = [];
        if (this.selectedRows.length === 0) {
            listID.push(this.selectedRow.id);
        } else {
            for (let i = 0; i < this.selectedRows.length; i++) {
                listID.push(this.selectedRows[i].id);
            }
        }
        this.inChungTuHangLoatService.inChungTuHangLoat({
            typeGroupID: TYPEGROUP.BANG_LUONG,
            typeReport: valueVoucher,
            listID,
            fromDate: null,
            toDate: null,
            paramCheckAll: false,
            isDesc: true
        });
    }

    is770(selectedRows: any[]) {
        if (selectedRows.length === 0) {
            if (!this.selectedRow) {
                return false;
            } else {
                if (this.selectedRow.typeID === this.TYPE_BANG_LUONG_THEO_BUOI) {
                    return true;
                }
            }
        } else {
            if (selectedRows.filter(s => s.typeID !== this.TYPE_BANG_LUONG_THEO_BUOI).length === 0) {
                return true;
            }
        }
        return false;
    }

    is771(selectedRows: any[]) {
        if (selectedRows.length === 0) {
            if (!this.selectedRow) {
                return false;
            } else {
                if (this.selectedRow.typeID === this.TYPE_BANG_LUONG_THEO_GIO) {
                    return true;
                }
            }
        } else {
            if (selectedRows.filter(s => s.typeID !== this.TYPE_BANG_LUONG_THEO_GIO).length === 0) {
                return true;
            }
        }
        return false;
    }

    is772(selectedRows: any[]) {
        if (selectedRows.length === 0) {
            if (!this.selectedRow) {
                return false;
            } else {
                if (this.selectedRow.typeID === this.TYPE_BANG_LUONG_CB_CO_DINH) {
                    return true;
                }
            }
        } else {
            if (selectedRows.filter(s => s.typeID !== this.TYPE_BANG_LUONG_CB_CO_DINH).length === 0) {
                return true;
            }
        }
        return false;
    }

    isBTBH(selectedRows: any[]) {
        if (selectedRows.length === 0) {
            if (!this.selectedRow) {
                return false;
            } else {
                if (this.selectedRow.typeID !== this.TYPE_BANG_LUONG_TAM_UNG) {
                    return true;
                }
            }
        } else {
            if (selectedRows.filter(s => s.typeID === this.TYPE_BANG_LUONG_TAM_UNG).length === 0) {
                return true;
            }
        }
        return false;
    }

    checkSameOrg() {
        return this.selectedRows.every(n => n.companyID === this.currentAccount.organizationUnit.id);
    }
}
