import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { BangLuongService } from './bang-luong.service';
import { PSSalarySheetComponent } from './bang-luong.component';
import { PSSalarySheetDeletePopupComponent } from './bang-luong-delete-dialog.component';
import { PSSalarySheetUpdateComponent } from 'app/luong/bang_luong/bang-luong-update.component';
import { ROLE } from 'app/role.constants';
import { IPSSalarySheet, PSSalarySheet } from 'app/shared/models/ps-salary-sheet.model';

@Injectable({ providedIn: 'root' })
export class PSSalarySheetResolve implements Resolve<IPSSalarySheet> {
    constructor(private service: BangLuongService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((pSSalarySheet: HttpResponse<PSSalarySheet>) => pSSalarySheet.body));
        }
        return of(new PSSalarySheet());
    }
}

export const pSSalarySheetRoute: Routes = [
    {
        path: '',
        component: PSSalarySheetComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER', ROLE.BangLuong_XEM],
            defaultSort: 'id,asc',
            pageTitle: 'ebwebApp.pSSalarySheet.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: PSSalarySheetUpdateComponent,
        resolve: {
            pSSalarySheet: PSSalarySheetResolve
        },
        data: {
            authorities: ['ROLE_USER', ROLE.BangLuong_Them],
            pageTitle: 'ebwebApp.pSSalarySheet.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: PSSalarySheetUpdateComponent,
        resolve: {
            pSSalarySheet: PSSalarySheetResolve
        },
        data: {
            authorities: ['ROLE_USER', ROLE.BangLuong_Sua],
            pageTitle: 'ebwebApp.pSSalarySheet.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const pSSalarySheetPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: PSSalarySheetDeletePopupComponent,
        resolve: {
            pSSalarySheet: PSSalarySheetResolve
        },
        data: {
            authorities: ['ROLE_USER', ROLE.BangLuong_Xoa],
            pageTitle: 'ebwebApp.pSSalarySheet.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
