import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IPSSalarySheetDetails } from 'app/shared/models/ps-salary-sheet-details.model';

type EntityResponseType = HttpResponse<IPSSalarySheetDetails>;
type EntityArrayResponseType = HttpResponse<IPSSalarySheetDetails[]>;

@Injectable({ providedIn: 'root' })
export class PSSalarySheetDetailsService {
    private resourceUrl = SERVER_API_URL + 'api/p-s-salary-sheet-details';

    constructor(private http: HttpClient) {}

    create(pSSalarySheetDetails: IPSSalarySheetDetails): Observable<EntityResponseType> {
        return this.http.post<IPSSalarySheetDetails>(this.resourceUrl, pSSalarySheetDetails, { observe: 'response' });
    }

    update(pSSalarySheetDetails: IPSSalarySheetDetails): Observable<EntityResponseType> {
        return this.http.put<IPSSalarySheetDetails>(this.resourceUrl, pSSalarySheetDetails, { observe: 'response' });
    }

    find(id: string): Observable<EntityResponseType> {
        return this.http.get<IPSSalarySheetDetails>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IPSSalarySheetDetails[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: string): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    getNewPSSalarySheet(req?: any): Observable<EntityArrayResponseType> {
        return this.http.put<IPSSalarySheetDetails[]>(this.resourceUrl + '/get-new-ps-salary-sheet', req, {
            observe: 'response'
        });
    }

    exportExcel(req: any, type: any): Observable<any> {
        let headers = new HttpHeaders();
        const options = createRequestOption(type);
        headers = headers.set('Accept', 'application/x-excel');
        return this.http.post(this.resourceUrl + '/export-excel', req, {
            params: options,
            observe: 'response',
            headers,
            responseType: 'blob'
        });
    }
}
