import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import {
    PSSalarySheetComponent,
    PSSalarySheetUpdateComponent,
    PSSalarySheetDeletePopupComponent,
    PSSalarySheetDeleteDialogComponent,
    pSSalarySheetRoute,
    pSSalarySheetPopupRoute
} from './index';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';
import { CurrencyMaskModule } from 'app/shared/directive/ng2-currency-mask/currency-mask.module';
import { TestluongSharedModule } from 'app/shared';
import { EbVirtualScrollerModule } from 'app/shared/virtual-scroller/virtual-scroller';

const ENTITY_STATES = [...pSSalarySheetRoute, ...pSSalarySheetPopupRoute];

@NgModule({
    imports: [TestluongSharedModule, RouterModule.forChild(ENTITY_STATES), EbVirtualScrollerModule, CurrencyMaskModule],
    declarations: [
        PSSalarySheetComponent,
        PSSalarySheetUpdateComponent,
        PSSalarySheetDeleteDialogComponent,
        PSSalarySheetDeletePopupComponent
    ],
    entryComponents: [
        PSSalarySheetComponent,
        PSSalarySheetUpdateComponent,
        PSSalarySheetDeleteDialogComponent,
        PSSalarySheetDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class EbwebPSSalarySheetModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
