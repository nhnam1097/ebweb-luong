import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { BangLuongService } from './bang-luong.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { IPSSalarySheet } from 'app/shared/models/ps-salary-sheet.model';

@Component({
    selector: 'eb-ps-salary-sheet-delete-dialog',
    templateUrl: './bang-luong-delete-dialog.component.html'
})
export class PSSalarySheetDeleteDialogComponent {
    pSSalarySheet: IPSSalarySheet;

    constructor(
        private pSSalarySheetService: BangLuongService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager,
        private toastr: ToastrService,
        private translate: TranslateService,
        private router: Router
    ) {}

    clear() {
        this.router.navigate(['/bang-luong']);
        // window.history.back();
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: string) {
        this.pSSalarySheetService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'pSSalarySheetListModification',
                content: 'Deleted an pSSalarySheet'
            });
            this.activeModal.dismiss(true);
        });
        this.toastr.success(this.translate.instant('ebwebApp.pSSalarySheet.deleteSuccessful'));
        this.router.navigate(['/bang-luong']);
    }
}

@Component({
    selector: 'eb-ps-salary-sheet-delete-popup',
    template: ''
})
export class PSSalarySheetDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ pSSalarySheet }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(PSSalarySheetDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.pSSalarySheet = pSSalarySheet;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
