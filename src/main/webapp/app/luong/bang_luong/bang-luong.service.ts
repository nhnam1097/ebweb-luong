import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IPSSalarySheet } from 'app/shared/models/ps-salary-sheet.model';
import { IPSTimeSheetSummary } from 'app/shared/models/ps-time-sheet-summary.model';
import { IPSTimeSheet } from 'app/shared/models/ps-time-sheet.model';

type EntityResponseType = HttpResponse<IPSSalarySheet>;
type EntityArrayResponseType = HttpResponse<IPSSalarySheet[]>;

@Injectable({ providedIn: 'root' })
export class BangLuongService {
    private resourceUrl = SERVER_API_URL + 'api/p-s-salary-sheets';

    constructor(private http: HttpClient) {}

    create(pSSalarySheet: IPSSalarySheet): Observable<EntityResponseType> {
        return this.http.post<IPSSalarySheet>(this.resourceUrl, pSSalarySheet, { observe: 'response' });
    }

    update(pSSalarySheet: IPSSalarySheet): Observable<EntityResponseType> {
        return this.http.put<IPSSalarySheet>(this.resourceUrl, pSSalarySheet, { observe: 'response' });
    }

    find(id: string): Observable<EntityResponseType> {
        return this.http.get<IPSSalarySheet>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IPSSalarySheet[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: string): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    getPSSalarySheets(): Observable<EntityArrayResponseType> {
        return this.http.get<IPSTimeSheetSummary[]>(this.resourceUrl + '/find-all-ps-salary-sheets', {
            observe: 'response'
        });
    }

    multiDelete(obj: IPSTimeSheet[]): Observable<HttpResponse<any>> {
        return this.http.post<any>(`${this.resourceUrl}/multi-delete-ps-salary-sheets`, obj, { observe: 'response' });
    }

    exportPDF(req?: any): Observable<any> {
        const options = createRequestOption(req);
        let headers = new HttpHeaders();
        headers = headers.set('Accept', 'application/x-excel');
        return this.http.get(this.resourceUrl + '/export/pdf', { params: options, observe: 'response', headers, responseType: 'blob' });
    }

    getAllPSSalarySheets(): Observable<EntityArrayResponseType> {
        return this.http.get<IPSTimeSheetSummary[]>(`${this.resourceUrl}/get-all-ps-salary-sheets`, {
            observe: 'response'
        });
    }

    exportExcel(req?: any): Observable<any> {
        const options = createRequestOption(req);
        let headers = new HttpHeaders();
        headers = headers.set('Accept', 'application/x-excel');
        return this.http.get(this.resourceUrl + '/export/excel', {
            params: options,
            observe: 'response',
            headers,
            responseType: 'blob'
        });
    }

    getCustomerReport(req?: any): Observable<HttpResponse<any>> {
        const options = createRequestOption(req);
        let headers = new HttpHeaders();
        headers = headers.set('Accept', 'application/pdf');
        return this.http.get(SERVER_API_URL + 'api/report/pdf', {
            params: options,
            observe: 'response',
            headers,
            responseType: 'blob'
        });
    }

    searchAll(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IPSSalarySheet[]>(`${this.resourceUrl}/search-all`, {
            params: options,
            observe: 'response'
        });
    }

    checkExceedPSSalarySheet(req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.get<any[]>(`${this.resourceUrl}/check-exceed-ps-salary-sheet`, {
            params: options,
            observe: 'response'
        });
    }

    getIndexRow(req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.get<object>(this.resourceUrl + '/getIndexVoucher', {
            params: options,
            observe: 'response'
        });
    }

    findByRowNum(req?: any): Observable<EntityResponseType> {
        const options = createRequestOption(req);
        return this.http.get<object>(this.resourceUrl + '/findByRowNum', {
            params: options,
            observe: 'response'
        });
    }
}
