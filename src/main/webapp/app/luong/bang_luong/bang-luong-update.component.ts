import { AfterViewChecked, AfterViewInit, Component, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Observable, Subscription } from 'rxjs';

import { TranslateService } from '@ngx-translate/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ROLE } from 'app/role.constants';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';
import { ToastrService } from 'ngx-toastr';
import { DatePipe } from '@angular/common';
import { Principal } from 'app/core';
import { BaseComponent } from 'app/shared/base-component/base.component';
import { ebAuth } from 'app/shared/decorator/ebAuth.decorator';
import { BangLuongService } from 'app/luong/bang_luong/bang-luong.service';
import { PSSalarySheetDetailsService } from 'app/luong/bang_luong/ps-salary-sheet-details/ps-salary-sheet-details.service';
import { DDSo_TienVND, TYPEGROUP, TypeMultiPrintTemplate } from 'app/app.constants';
import { CurrencyMaskConfig } from 'app/shared/directive/ng2-currency-mask/currency-mask.config';
import { IPSSalarySheet } from 'app/shared/models/ps-salary-sheet.model';
import { BangLuongExportExcel, IPSSalarySheetDetails } from 'app/shared/models/ps-salary-sheet-details.model';
import { IDataSessionStorage } from 'app/shared/models/DataSessionStorage';
import { ContextMenu, IContextMenu } from 'app/shared/models/context-menu.model';
import { ISessionPSTimeSheet } from 'app/shared/models/session-p-s-time-sheet';
import { IPSSalaryTaxInsuranceRegulation } from 'app/shared/models/ps-salary-tax-insurance-regulation.model';
import { IAccountingObject } from 'app/shared/models/accounting-object.model';
import { AccountingObjectService } from 'app/shared/services/accounting-object.service';
import { RefModalService } from 'app/shared/services/ref-modal.service';
import { QuyDinhLuongThueBaoHiemService } from 'app/shared/services/quy-dinh-luong-thue-bao-hiem.service';
import { UtilsService } from 'app/shared/UtilsService/Utils.service';
import { NotificationService } from 'app/shared/notification';
import { InChungTuHangLoatService } from 'app/shared/in-chung-tu-hang-loat/in-chung-tu-hang-loat.service';
import { SearchVoucher } from 'app/shared/models/SearchVoucher';
import { IPersonalSalaryTax } from 'app/shared/models/personal-salary-tax.model';
import { PersonalSalaryTaxService } from 'app/shared/services/personal-salary-tax.service';

@Component({
    selector: 'eb-ps-salary-sheet-update',
    templateUrl: './bang-luong-update.component.html',
    styleUrls: ['./bang-luong-update.component.css']
})
export class PSSalarySheetUpdateComponent extends BaseComponent implements OnInit, AfterViewInit, AfterViewChecked, OnDestroy {
    private _pSSalarySheet: IPSSalarySheet;
    TYPE_BANG_LUONG_THEO_BUOI = 770;
    TYPE_BANG_LUONG_THEO_GIO = 771;
    TYPE_BANG_LUONG_CB_CO_DINH = 772;
    TYPE_BANG_LUONG_TAM_UNG = 774;
    TYPE_TH_CHAM_CONG_THEO_NGAY = 760;
    TYPE_TH_CHAM_CONG_THEO_GIO = 761;
    TYPE_CHAM_CONG_THEO_NGAY = 750;
    TYPE_CHAM_CONG_THEO_GIO = 751;
    TAO_MOI_HOAN_TOAN = 0;
    DUA_TREN_BANG_THCC = 2;
    DUA_TREN_BANG_CC = 3;
    DUA_TREN_BANG_LUONG = 1;
    isSaving: boolean;
    periodPSTimeSheet: string;
    newRow: any;
    month: number;
    year: number;
    select: any;
    pSSalarySheetDetails: IPSSalarySheetDetails[];
    @ViewChild('content') content: TemplateRef<any>;
    @ViewChild('popUpDelete') popUpDelete: TemplateRef<any>;
    @ViewChild('popupDeleteGOtherVoucherID') popupDeleteGOtherVoucherID: TemplateRef<any>;
    count: number;
    items: any;
    //  data storage provider
    warningMessage: string;
    page: number;
    itemsPerPage: number;
    pageCount: number;
    totalItems: number;
    rowNum: number;
    // sort
    predicate: any;
    isCreateUrl: boolean;
    isEditUrl: boolean;
    disableAddButton: boolean;
    disableEditButton: boolean;
    dataSession: IDataSessionStorage;
    eventSubscriber: Subscription;
    modalRef: NgbModalRef;
    currentAccount: any;
    options: any;
    isHideTypeLedger: Boolean;
    pSSalarySheetCopy: IPSSalarySheet;
    pSSalarySheetDetailsCopy: IPSSalarySheetDetails[];
    isReadOnly: boolean;
    contextMenu: IContextMenu;
    sessionPSTimeSheet: ISessionPSTimeSheet = new class implements ISessionPSTimeSheet {
        month: number;
        year: number;
        pSSalarySheetName?: string;
        typeCreate?: number;
        lastPSTimeSheetID?: string;
        lastPSTimeSheetSummaryID?: string;
        lastPSSalarySheetID?: string;
        autoAddNewEmployee?: boolean;
        getEmployeeNotActive?: boolean;
        listDepartmentID?: any[];
    }();
    formatCurrency = {
        align: 'right',
        allowNegative: false,
        decimal: ',',
        precision: 2,
        prefix: '',
        suffix: '',
        thousands: '.',
        negative: '-',
        autocomplete: 'off'
    };
    formatSoLuong = {
        align: 'right',
        allowNegative: false,
        decimal: ',',
        precision: 3,
        prefix: '',
        suffix: '',
        thousands: '.',
        negative: '-',
        autocomplete: 'off'
    };
    optionsDirective = {
        align: 'right',
        allowNegative: false,
        decimal: ',',
        precision: 2,
        prefix: '',
        suffix: '',
        thousands: '.',
        negative: '-',
        autocomplete: 'off'
    };
    optionsDirective3: any;
    index: number;
    pSSalaryTaxInsuranceRegulation: IPSSalaryTaxInsuranceRegulation;
    personalSalaryTaxes: IPersonalSalaryTax[];
    pPTinhLuong: any;
    employeess: IAccountingObject[];
    ROLE_XEM = ROLE.BangLuong_XEM;
    ROLE_THEM = ROLE.BangLuong_Them;
    ROLE_SUA = ROLE.BangLuong_Sua;
    ROLE_XOA = ROLE.BangLuong_Xoa;
    ROLE_IN = ROLE.BangLuong_In;
    ROLE_KETXUAT = ROLE.BangLuong_KetXuat;

    buttonDeleteTranslate = 'ebwebApp.mBDeposit.toolTip.delete';
    buttonAddTranslate = 'ebwebApp.mBDeposit.toolTip.add';
    buttonEditTranslate = 'ebwebApp.mBDeposit.toolTip.edit';
    buttonSaveTranslate = 'ebwebApp.mBDeposit.toolTip.save';
    buttonPrintTranslate = 'ebwebApp.mBDeposit.toolTip.print';
    buttonCloseFormTranslate = 'ebwebApp.mBDeposit.toolTip.closeForm';

    CustomCurrencyMaskConfigTienViet: CurrencyMaskConfig = {
        align: 'right',
        allowNegative: false,
        decimal: ',',
        precision: 2,
        prefix: '',
        suffix: '',
        thousands: '.',
        negative: '-',
        autocomplete: 'off'
    };

    CustomCurrencyMaskConfigTienVietNotAllowNegative: CurrencyMaskConfig = {
        align: 'right',
        allowNegative: false,
        decimal: ',',
        precision: 2,
        prefix: '',
        suffix: '',
        thousands: '.',
        negative: '-',
        autocomplete: 'off'
    };

    CustomCurrencyMaskConfigSoLuong: CurrencyMaskConfig = {
        align: 'right',
        allowNegative: false,
        decimal: ',',
        precision: 2,
        prefix: '',
        suffix: '',
        thousands: '.',
        negative: '-',
        autocomplete: 'off'
    };
    TypeMultiPrintTemplate = TypeMultiPrintTemplate;
    isUseToolIn: boolean;
    searchVoucher: string;

    constructor(
        private pSSalarySheetService: BangLuongService,
        private pSSalarySheetDetailsService: PSSalarySheetDetailsService,
        private activatedRoute: ActivatedRoute,
        private accService: AccountingObjectService,
        private router: Router,
        private jhiAlertService: JhiAlertService,
        private toastr: ToastrService,
        private translate: TranslateService,
        private datepipe: DatePipe,
        private eventManager: JhiEventManager,
        private modalService: NgbModal,
        private refModalService: RefModalService,
        private principal: Principal,
        private pSSalaryTaxInsuranceRegulationService: QuyDinhLuongThueBaoHiemService,
        private personalSalaryTaxService: PersonalSalaryTaxService,
        public utilsService: UtilsService,
        private notificationService: NotificationService,
        private inChungTuHangLoatService: InChungTuHangLoatService
    ) {
        super();
        this.contextMenu = new ContextMenu();
        this.principal.identity().then(account => {
            this.currentAccount = account;
            this.isUseToolIn = this.utilsService.checkUseToolIn(account);
            this.isEnter = this.currentAccount.systemOption.filter(n => n.code === 'TCKHAC_Enter')[0].data === '1';
            this.optionsDirective3 = this.utilsService.getConfigCurrencyWithAccWithPrec(account, false, 3);
            if (this.isEnter) {
                this.noUseEnter = true;
            } else {
                this.noUseEnter = false;
            }
            /*const formatTienViet = this.currentAccount.systemOption.find(a => a.code === 'DDSo_TienVND');
            const formatSoLuong = this.currentAccount.systemOption.find(a => a.code === 'DDSo_SoLuong');
            this.formatCurrency.precision = formatTienViet ? parseInt(formatTienViet.data) : 0;
            this.formatSoLuong.precision = formatSoLuong ? parseInt(formatSoLuong.data) : 0;*/
            this.pPTinhLuong = this.currentAccount.systemOption.find(a => a.code === 'TCKHAC_TinhLuong').data;
            this.CustomCurrencyMaskConfigTienViet = this.utilsService.getConfigCurrencyWithAcc(account, DDSo_TienVND, false);
            this.CustomCurrencyMaskConfigTienVietNotAllowNegative = this.utilsService.getConfigCurrencyWithAcc(account, DDSo_TienVND, true);
            this.CustomCurrencyMaskConfigSoLuong = this.utilsService.getConfigCurrencyWithAcc(account, DDSo_TienVND, false);

            this.formatCurrency = this.utilsService.getConfigCurrencyWithAccWithPrec(account, false, 3);
            this.formatSoLuong = this.utilsService.getConfigCurrencyWithAccWithPrec(account, false, 3);
            this.optionsDirective = this.utilsService.getConfigCurrencyWithAccWithPrec(account, false, 2);
        });
    }

    ngOnInit() {
        this.pSSalarySheetDetails = [];
        this.isSaving = false;
        this.isHideTypeLedger = false;
        this.searchVoucher = JSON.parse(sessionStorage.getItem('dataSearchPSTimeSheet'));
        this.isEditUrl = window.location.href.includes('/edit') || window.location.href.includes('/view');
        this.activatedRoute.data.subscribe(({ pSSalarySheet }) => {
            this.pSSalarySheet = pSSalarySheet;
            if (sessionStorage.getItem('arr')) {
                const arr = JSON.parse(sessionStorage.getItem('arr'));
                arr.forEach((n, i) => {
                    Object.keys(n).forEach(m => {
                        this.pSSalarySheet.pSSalarySheetDetails[i][m] = n[m];
                    });
                });
                sessionStorage.removeItem('arr');
            }
            if (sessionStorage.getItem('sessionPSSalarySheet')) {
                this.sessionPSTimeSheet = JSON.parse(sessionStorage.getItem('sessionPSSalarySheet'));
                this.month = this.sessionPSTimeSheet.month;
                this.year = this.sessionPSTimeSheet.year;
            } else {
                this.month = this.pSSalarySheet.month;
                this.year = this.pSSalarySheet.year;
            }
            this.pSSalaryTaxInsuranceRegulationService
                .getDataByCompanyID({ month: this.month, year: this.year })
                .subscribe((res: HttpResponse<IPSSalaryTaxInsuranceRegulation>) => {
                    this.pSSalaryTaxInsuranceRegulation = res.body;
                });
            this.personalSalaryTaxService.getDataByCompanyID().subscribe((res: HttpResponse<IPersonalSalaryTax[]>) => {
                this.personalSalaryTaxes = res.body;
                for (let i = 0; i < this.personalSalaryTaxes.length; i++) {
                    this.personalSalaryTaxes[i].taxAmountInPersonalSalary =
                        (this.personalSalaryTaxes[i].toAmount - this.personalSalaryTaxes[i].fromAmount) *
                        (this.personalSalaryTaxes[i].taxRate / 100);
                }
            });
            if (!this.pSSalarySheet.id) {
                if (sessionStorage.getItem('sessionPSSalarySheet')) {
                    this.sessionPSTimeSheet = JSON.parse(sessionStorage.getItem('sessionPSSalarySheet'));
                    sessionStorage.removeItem('sessionPSSalarySheet');
                } else {
                    this.router.navigate(['/bang-luong']);
                }
                this.disableEditButton = true;
                this.pSSalarySheet.pSSalarySheetName = this.sessionPSTimeSheet.pSSalarySheetName;
                this.pSSalarySheet.dayWorks = this.sessionPSTimeSheet.daysWork;
                this.pSSalarySheet.typeID = this.sessionPSTimeSheet.typeID;
                this.pSSalarySheet.pSTimeSheetID = this.sessionPSTimeSheet.lastPSTimeSheetID;
                if (this.sessionPSTimeSheet) {
                    const req = {
                        listDepartmentID: this.sessionPSTimeSheet.listDepartmentID,
                        typeID: this.sessionPSTimeSheet.typeID,
                        typeCreate: this.sessionPSTimeSheet.typeCreate,
                        lastPSSalarySheetID: this.sessionPSTimeSheet.lastPSTimeSheetID,
                        dayWorks: this.sessionPSTimeSheet.daysWork,
                        month: this.sessionPSTimeSheet.month,
                        year: this.sessionPSTimeSheet.year
                    };
                    this.pSSalarySheetDetailsService
                        .getNewPSSalarySheet(req)
                        .subscribe(async (res: HttpResponse<IPSSalarySheetDetails[]>) => {
                            this.pSSalarySheetDetails = res.body;
                            for (let i = 0; i < this.pSSalarySheetDetails.length; i++) {
                                this.pSSalarySheetDetails[i].stt = i + 1;
                            }
                            this.lstDataForSetDefault = [];
                            this.lstDataForSetDefault.push(this.pSSalarySheetDetails);
                            await this.loadDataDefaultForOpenVoucherSync(this.lstDataForSetDefault);
                            if (this.pSSalarySheetDetails && this.pSSalarySheetDetails.length > 0) {
                                for (let i = 0; i < this.pSSalarySheetDetails.length; i++) {
                                    if (this.employees && this.employees.length > 0) {
                                        const listAcc = this.employees.filter(
                                            a => a.departmentId && a.departmentId === this.pSSalarySheetDetails[i].departmentID
                                        );
                                        if (listAcc && listAcc.length > 0) {
                                            this.pSSalarySheetDetails[i].listEmployeeID = listAcc.map(object => ({ ...object }));
                                        }
                                        const acc = this.pSSalarySheetDetails[i].listEmployeeID.find(
                                            a => a.id && a.id === this.pSSalarySheetDetails[i].employeeID
                                        );
                                        if (!acc) {
                                            const employee = this.employees.find(
                                                a => a.id && a.id === this.pSSalarySheetDetails[i].employeeID
                                            );
                                            if (employee) {
                                                this.pSSalarySheetDetails[i].listEmployeeID.push(employee);
                                            }
                                        }
                                    }
                                    this.getAccountingObjectTitle(i);
                                    if (
                                        this.pSSalarySheet.typeID !== this.TYPE_BANG_LUONG_CB_CO_DINH &&
                                        this.pSSalarySheet.typeID !== this.TYPE_BANG_LUONG_TAM_UNG
                                    ) {
                                        if (this.pPTinhLuong === '1') {
                                            this.pSSalarySheetDetails[i].agreementSalary =
                                                (this.pSSalarySheetDetails[i].salaryCoefficient
                                                    ? this.pSSalarySheetDetails[i].salaryCoefficient
                                                    : 0) *
                                                (this.pSSalarySheetDetails[i].basicWage ? this.pSSalarySheetDetails[i].basicWage : 0);
                                        }
                                        if (this.pSSalarySheet.typeID === this.TYPE_BANG_LUONG_THEO_BUOI) {
                                            this.pSSalarySheetDetails[i].workingDayUnitPrice = this.utilsService.round(
                                                (this.pSSalarySheetDetails[i].agreementSalary
                                                    ? this.pSSalarySheetDetails[i].agreementSalary
                                                    : 0) / (this.sessionPSTimeSheet.daysWork ? this.sessionPSTimeSheet.daysWork : 0),
                                                this.currentAccount.systemOption,
                                                7
                                            );
                                        } else {
                                            this.pSSalarySheetDetails[i].workingHourUnitPrice = this.utilsService.round(
                                                (this.pSSalarySheetDetails[i].agreementSalary
                                                    ? this.pSSalarySheetDetails[i].agreementSalary
                                                    : 0) /
                                                    (this.sessionPSTimeSheet.daysWork *
                                                        (this.pSSalaryTaxInsuranceRegulation &&
                                                        this.pSSalaryTaxInsuranceRegulation.workHoursInDay
                                                            ? this.pSSalaryTaxInsuranceRegulation.workHoursInDay
                                                            : 0)),
                                                this.currentAccount.systemOption,
                                                7
                                            );
                                        }
                                        if (this.pSSalarySheet.dayWorks !== this.pSSalarySheetDetails[i].numberOfPaidWorkingDayTimeSheet) {
                                            this.pSSalarySheetDetails[i].paidWorkingDayAmount = this.utilsService.round(
                                                (this.pSSalarySheet.typeID === this.TYPE_BANG_LUONG_THEO_BUOI
                                                    ? this.pSSalarySheetDetails[i].workingDayUnitPrice
                                                    : this.pSSalarySheetDetails[i].workingHourUnitPrice) *
                                                    this.pSSalarySheetDetails[i].numberOfPaidWorkingDayTimeSheet,
                                                this.currentAccount.systemOption,
                                                7
                                            );
                                        } else {
                                            this.pSSalarySheetDetails[i].paidWorkingDayAmount = this.pSSalarySheetDetails[
                                                i
                                            ].agreementSalary;
                                        }
                                    }
                                    if (this.pSSalarySheet.typeID !== 774) {
                                        if (this.pSSalaryTaxInsuranceRegulation) {
                                            this.pSSalarySheetDetails[i].employeeSocityInsuranceAmount = this.utilsService.round(
                                                (this.pSSalaryTaxInsuranceRegulation.employeeSocityInsurancePercent
                                                    ? this.pSSalaryTaxInsuranceRegulation.employeeSocityInsurancePercent
                                                    : 0) *
                                                    (this.pSSalarySheetDetails[i].insuranceSalary
                                                        ? this.pSSalarySheetDetails[i].insuranceSalary
                                                        : 0) /
                                                    100,
                                                this.currentAccount.systemOption,
                                                7
                                            );
                                            this.pSSalarySheetDetails[i].employeeAccidentInsuranceAmount = this.utilsService.round(
                                                (this.pSSalaryTaxInsuranceRegulation.employeeAccidentInsurancePercent
                                                    ? this.pSSalaryTaxInsuranceRegulation.employeeAccidentInsurancePercent
                                                    : 0) *
                                                    (this.pSSalarySheetDetails[i].insuranceSalary
                                                        ? this.pSSalarySheetDetails[i].insuranceSalary
                                                        : 0) /
                                                    100,
                                                this.currentAccount.systemOption,
                                                7
                                            );
                                            this.pSSalarySheetDetails[i].employeeMedicalInsuranceAmount = this.utilsService.round(
                                                (this.pSSalaryTaxInsuranceRegulation.employeeMedicalInsurancePercent
                                                    ? this.pSSalaryTaxInsuranceRegulation.employeeMedicalInsurancePercent
                                                    : 0) *
                                                    (this.pSSalarySheetDetails[i].insuranceSalary
                                                        ? this.pSSalarySheetDetails[i].insuranceSalary
                                                        : 0) /
                                                    100,
                                                this.currentAccount.systemOption,
                                                7
                                            );
                                            this.pSSalarySheetDetails[i].employeeUnEmployeeInsuranceAmount = this.utilsService.round(
                                                (this.pSSalaryTaxInsuranceRegulation.employeeUnEmployeeInsurancePercent
                                                    ? this.pSSalaryTaxInsuranceRegulation.employeeUnEmployeeInsurancePercent
                                                    : 0) *
                                                    (this.pSSalarySheetDetails[i].insuranceSalary
                                                        ? this.pSSalarySheetDetails[i].insuranceSalary
                                                        : 0) /
                                                    100,
                                                this.currentAccount.systemOption,
                                                7
                                            );
                                            this.pSSalarySheetDetails[i].employeeTradeUnionInsuranceAmount = this.utilsService.round(
                                                (this.pSSalaryTaxInsuranceRegulation.employeeTradeUnionInsurancePercent
                                                    ? this.pSSalaryTaxInsuranceRegulation.employeeTradeUnionInsurancePercent
                                                    : 0) *
                                                    (this.pSSalarySheetDetails[i].insuranceSalary
                                                        ? this.pSSalarySheetDetails[i].insuranceSalary
                                                        : 0) /
                                                    100,
                                                this.currentAccount.systemOption,
                                                7
                                            );
                                            this.pSSalarySheetDetails[i].companySocityInsuranceAmount = this.utilsService.round(
                                                (this.pSSalaryTaxInsuranceRegulation.companySocityInsurancePercent
                                                    ? this.pSSalaryTaxInsuranceRegulation.companySocityInsurancePercent
                                                    : 0) *
                                                    (this.pSSalarySheetDetails[i].insuranceSalary
                                                        ? this.pSSalarySheetDetails[i].insuranceSalary
                                                        : 0) /
                                                    100,
                                                this.currentAccount.systemOption,
                                                7
                                            );
                                            this.pSSalarySheetDetails[i].companyAccidentInsuranceAmount = this.utilsService.round(
                                                (this.pSSalaryTaxInsuranceRegulation.companytAccidentInsurancePercent
                                                    ? this.pSSalaryTaxInsuranceRegulation.companytAccidentInsurancePercent
                                                    : 0) *
                                                    (this.pSSalarySheetDetails[i].insuranceSalary
                                                        ? this.pSSalarySheetDetails[i].insuranceSalary
                                                        : 0) /
                                                    100,
                                                this.currentAccount.systemOption,
                                                7
                                            );
                                            this.pSSalarySheetDetails[i].companyMedicalInsuranceAmount = this.utilsService.round(
                                                (this.pSSalaryTaxInsuranceRegulation.companyMedicalInsurancePercent
                                                    ? this.pSSalaryTaxInsuranceRegulation.companyMedicalInsurancePercent
                                                    : 0) *
                                                    (this.pSSalarySheetDetails[i].insuranceSalary
                                                        ? this.pSSalarySheetDetails[i].insuranceSalary
                                                        : 0) /
                                                    100,
                                                this.currentAccount.systemOption,
                                                7
                                            );
                                            this.pSSalarySheetDetails[i].companyUnEmployeeInsuranceAmount = this.utilsService.round(
                                                (this.pSSalaryTaxInsuranceRegulation.companyUnEmployeeInsurancePercent
                                                    ? this.pSSalaryTaxInsuranceRegulation.companyUnEmployeeInsurancePercent
                                                    : 0) *
                                                    (this.pSSalarySheetDetails[i].insuranceSalary
                                                        ? this.pSSalarySheetDetails[i].insuranceSalary
                                                        : 0) /
                                                    100,
                                                this.currentAccount.systemOption,
                                                7
                                            );
                                            this.pSSalarySheetDetails[i].companyTradeUnionInsuranceAmount = this.utilsService.round(
                                                (this.pSSalaryTaxInsuranceRegulation.companyTradeUnionInsurancePercent
                                                    ? this.pSSalaryTaxInsuranceRegulation.companyTradeUnionInsurancePercent
                                                    : 0) *
                                                    (this.pSSalarySheetDetails[i].insuranceSalary
                                                        ? this.pSSalarySheetDetails[i].insuranceSalary
                                                        : 0) /
                                                    100,
                                                this.currentAccount.systemOption,
                                                7
                                            );
                                        }
                                        this.selectChangeSumOfDeductionAmount(this.pSSalarySheetDetails[i]);
                                        this.pSSalarySheetDetails[i].totalPersonalTaxIncomeAmount = this.utilsService.round(
                                            (this.pSSalarySheetDetails[i].payrollFundAllowance
                                                ? this.pSSalarySheetDetails[i].payrollFundAllowance
                                                : 0) +
                                                (this.pSSalarySheetDetails[i].otherAllowance
                                                    ? this.pSSalarySheetDetails[i].otherAllowance
                                                    : 0),
                                            this.currentAccount.systemOption,
                                            7
                                        );
                                        if (this.sessionPSTimeSheet && this.sessionPSTimeSheet.typeCreate === 1) {
                                        } else if (this.pSSalaryTaxInsuranceRegulation) {
                                            this.pSSalarySheetDetails[
                                                i
                                            ].reduceSelfTaxAmount = this.pSSalaryTaxInsuranceRegulation.reduceSelfTaxAmount;
                                        }
                                        if (
                                            this.pSSalarySheetDetails[i].employeeID &&
                                            this.pSSalarySheetDetails[i].listEmployeeID &&
                                            this.pSSalarySheetDetails[i].listEmployeeID.length > 0 &&
                                            this.pSSalaryTaxInsuranceRegulation
                                        ) {
                                            const numberOfDependent = this.pSSalarySheetDetails[i].listEmployeeID.find(
                                                a => a.id && a.id === this.pSSalarySheetDetails[i].employeeID
                                            );
                                            if (this.sessionPSTimeSheet && this.sessionPSTimeSheet.typeCreate === 1) {
                                            } else if (numberOfDependent) {
                                                this.pSSalarySheetDetails[i].reduceDependTaxAmount =
                                                    (this.pSSalaryTaxInsuranceRegulation &&
                                                    this.pSSalaryTaxInsuranceRegulation.reduceDependTaxAmount
                                                        ? this.pSSalaryTaxInsuranceRegulation.reduceDependTaxAmount
                                                        : 0) *
                                                    (numberOfDependent.numberOfDependent ? numberOfDependent.numberOfDependent : 0);
                                            } else {
                                                this.pSSalarySheetDetails[i].reduceDependTaxAmount = 0;
                                            }
                                        } else {
                                            this.pSSalarySheetDetails[i].reduceDependTaxAmount = 0;
                                        }
                                        const incomeForTaxCalculation =
                                            (this.pSSalarySheetDetails[i].totalPersonalTaxIncomeAmount
                                                ? this.pSSalarySheetDetails[i].totalPersonalTaxIncomeAmount
                                                : 0) -
                                            (this.pSSalarySheetDetails[i].employeeSocityInsuranceAmount
                                                ? this.pSSalarySheetDetails[i].employeeSocityInsuranceAmount
                                                : 0) -
                                            (this.pSSalarySheetDetails[i].employeeAccidentInsuranceAmount
                                                ? this.pSSalarySheetDetails[i].employeeAccidentInsuranceAmount
                                                : 0) -
                                            (this.pSSalarySheetDetails[i].employeeUnEmployeeInsuranceAmount
                                                ? this.pSSalarySheetDetails[i].employeeUnEmployeeInsuranceAmount
                                                : 0) -
                                            (this.pSSalarySheetDetails[i].employeeMedicalInsuranceAmount
                                                ? this.pSSalarySheetDetails[i].employeeMedicalInsuranceAmount
                                                : 0) -
                                            (this.pSSalarySheetDetails[i].employeeTradeUnionInsuranceAmount
                                                ? this.pSSalarySheetDetails[i].employeeTradeUnionInsuranceAmount
                                                : 0) -
                                            (this.pSSalarySheetDetails[i].reduceSelfTaxAmount
                                                ? this.pSSalarySheetDetails[i].reduceSelfTaxAmount
                                                : 0) -
                                            (this.pSSalarySheetDetails[i].reduceDependTaxAmount
                                                ? this.pSSalarySheetDetails[i].reduceDependTaxAmount
                                                : 0);
                                        this.pSSalarySheetDetails[i].incomeForTaxCalculation =
                                            incomeForTaxCalculation > 0 ? incomeForTaxCalculation : 0;
                                        this.selectChangeTotalAmount(this.pSSalarySheetDetails[i]);
                                        this.checkCalIncomeTaxAmount(this.pSSalarySheetDetails[i]);
                                        this.selectChangeNetAmount(this.pSSalarySheetDetails[i]);
                                    }
                                    this.selectChangeTotalAmount(this.pSSalarySheetDetails[i]);
                                    this.pSSalarySheetDetails[i].totalPersonalTaxIncomeAmount =
                                        this.pSSalarySheetDetails[i].totalAmount -
                                        (this.pSSalarySheetDetails[i].notIncomeTaxAllowance
                                            ? this.pSSalarySheetDetails[i].notIncomeTaxAllowance
                                            : 0);
                                }
                            }
                            // this.pSSalarySheetDetails.forEach(item => {
                            //     item.workAllDay = 0;
                            //     item.workHalfAday = 0;
                            //     item.totalOverTime = 0;
                            //     item.total = 0;
                            // });
                        });
                }
            } else {
                // region Tiến lùi chứng từ
                this.pSSalarySheetService
                    .getIndexRow({
                        id: this.pSSalarySheet.id,
                        isNext: true,
                        typeID: this.TYPE_CHAM_CONG_THEO_NGAY,
                        searchVoucher: this.searchVoucher === undefined ? null : JSON.stringify(this.searchVoucher)
                    })
                    .subscribe(
                        (res: HttpResponse<any[]>) => {
                            this.rowNum = res.body[0];
                            if (res.body.length === 1) {
                                this.count = 1;
                            } else {
                                this.count = res.body[1];
                            }
                        },
                        (res: HttpErrorResponse) => this.onError(res.message)
                    );
                this.pSSalarySheetDetails = this.pSSalarySheet.pSSalarySheetDetails.sort((a, b) => a.orderPriority - b.orderPriority);
                for (let i = 0; i < this.pSSalarySheetDetails.length; i++) {
                    this.pSSalarySheetDetails[i].stt = i + 1;
                }
                this.utilsService
                    .getDataDefaultForOpenVoucher({ lstIDEmployee: this.pSSalarySheetDetails.map(n => n.employeeID) })
                    .subscribe(async (res: HttpResponse<any>) => {
                        this.lstDataForSetDefault = [];
                        this.lstDataForSetDefault.push(this.pSSalarySheetDetails);
                        await this.loadDataDefaultForOpenVoucherSync(this.lstDataForSetDefault);
                        this.employees = res.body.listEmployees;
                        if (this.pSSalarySheetDetails && this.pSSalarySheetDetails.length > 0) {
                            for (let i = 0; i < this.pSSalarySheetDetails.length; i++) {
                                if (this.employees && this.employees.length > 0) {
                                    const listAcc = this.employees.filter(
                                        a => a.departmentId === this.pSSalarySheetDetails[i].departmentID
                                    );
                                    if (listAcc && listAcc.length > 0) {
                                        this.pSSalarySheetDetails[i].listEmployeeID = listAcc.map(object => ({ ...object }));
                                    }
                                    const acc = this.pSSalarySheetDetails[i].listEmployeeID.find(
                                        a => a.id === this.pSSalarySheetDetails[i].employeeID
                                    );
                                    if (!acc) {
                                        this.pSSalarySheetDetails[i].listEmployeeID.push(
                                            this.employees.find(a => a.id === this.pSSalarySheetDetails[i].employeeID)
                                        );
                                    }
                                }
                            }
                        }
                    });
            }
        });
        // this.organizationUnitService.getOrganizationUnits().subscribe((res: HttpResponse<IOrganizationUnit[]>) => {
        //     this.organizationUnits = res.body
        //         .filter(a => a.unitType === 4)
        //         .sort((a, b) => a.organizationUnitCode.localeCompare(b.organizationUnitCode));
        // });
        this.accService.getAllDTO().subscribe(res => {
            this.employeess = res.body.filter(a => a.isEmployee);
        });
        this.isCreateUrl = window.location.href.includes('/bang-luong/new');
        this.isEdit = this.isCreateUrl;
        this.disableAddButton = true;
        this.sumAfterDeleteByContextMenu();
        this.afterAddRow();
        this.registerCopyRow();
        this.eventSubscriber = this.eventManager.subscribe('saveSuccess', response => {
            this.isCbbSaveAndNew = false;
            this.registerComboboxSave(response);
            this.selectChangeAccountingObject();
            this.utilsService.setShowPopup(false);
        });
        this.eventSubscriber = this.eventManager.subscribe('saveAndNewSuccess', response => {
            this.isCbbSaveAndNew = false;
            this.registerComboboxSave(response);
            this.selectChangeAccountingObject();
            this.utilsService.setShowPopup(false);
        });
        this.eventSubscriber = this.eventManager.subscribe('closePopup', response => {
            this.utilsService.setShowPopup(response.content);
        });
        this.eventSubscribers.push(this.eventSubscriber);
        this.checkSameOrg();
    }

    closeForm() {
        event.preventDefault();
        this.closeAll();
    }

    closeAll() {
        this.router.navigate(['/bang-luong']);
    }

    copy() {
        if (this.pSSalarySheet.id) {
            this.pSSalarySheetDetails = this.pSSalarySheet.pSSalarySheetDetails ? this.pSSalarySheet.pSSalarySheetDetails : [];
        }
        this.pSSalarySheetCopy = Object.assign({}, this.pSSalarySheet);
        if (this.pSSalarySheetDetails) {
            this.pSSalarySheetDetailsCopy = this.pSSalarySheetDetails.map(object => ({ ...object }));
        }
    }

    @ebAuth(['ROLE_ADMIN', ROLE.BangLuong_Sua])
    edit() {
        event.preventDefault();
        if (!this.isCreateUrl) {
            if (this.pSSalarySheet.gOtherVoucherID) {
                if (this.modalRef) {
                    this.modalRef.close();
                }
                return (this.modalRef = this.modalService.open(this.popupDeleteGOtherVoucherID, { backdrop: 'static' }));
            } else {
                this.acceptEdit();
            }
        }
    }

    acceptEdit() {
        if (!this.isCreateUrl) {
            if (this.modalRef) {
                this.modalRef.close();
            }
            this.isCreateUrl = !this.isCreateUrl;
            this.isEdit = this.isCreateUrl;
            this.disableAddButton = false;
            this.disableEditButton = true;
            this.isReadOnly = false;
            this.copy();
            this.focusFirstInput();
        }
    }

    @ebAuth(['ROLE_ADMIN', ROLE.BangLuong_Them])
    addNew($event) {
        event.preventDefault();
        if (!this.isCreateUrl) {
            // && !this.utilsService.isShowPopup
            sessionStorage.setItem('checkNewBangLuong', JSON.stringify(true));
            this.router.navigate(['bang-luong']);
        }
    }

    @ebAuth(['ROLE_ADMIN', ROLE.BangLuong_Them, ROLE.BangLuong_Sua])
    save(isNew = false) {
        event.preventDefault();
        if (this.isCreateUrl) {
            this.pSSalarySheet.typeID = this.pSSalarySheet.typeID ? this.pSSalarySheet.typeID : this.sessionPSTimeSheet.typeID;
            this.pSSalarySheet.month = this.pSSalarySheet.month ? this.pSSalarySheet.month : this.sessionPSTimeSheet.month;
            this.pSSalarySheet.year = this.pSSalarySheet.year ? this.pSSalarySheet.year : this.sessionPSTimeSheet.year;
            this.pSSalarySheet.pSSalarySheetName = this.pSSalarySheet.pSSalarySheetName
                ? this.pSSalarySheet.pSSalarySheetName
                : this.sessionPSTimeSheet.pSSalarySheetName;
            this.pSSalarySheet.pSSalarySheetDetails = this.pSSalarySheetDetails;
            if (this.checkError()) {
                this.isSaving = true;
                // check is url new
                if (this.pSSalarySheet.id && this.isEditUrl) {
                    this.isCreateUrl = this.isEdit = false;
                }
                if (this.isCreateUrl && this.pSSalarySheet.id !== undefined) {
                    this.pSSalarySheet.id = undefined;
                    for (let i = 0; i < this.pSSalarySheet.pSSalarySheetDetails.length; i++) {
                        this.pSSalarySheet.pSSalarySheetDetails[i].id = undefined;
                    }
                }
                let sum = 0;
                for (let i = 0; i < this.pSSalarySheet.pSSalarySheetDetails.length; i++) {
                    if (this.pSSalarySheet.typeID !== this.TYPE_BANG_LUONG_TAM_UNG) {
                        sum += this.pSSalarySheet.pSSalarySheetDetails[i].netAmount
                            ? this.pSSalarySheet.pSSalarySheetDetails[i].netAmount
                            : 0;
                    } else {
                        sum += this.pSSalarySheet.pSSalarySheetDetails[i].temporaryAmount
                            ? this.pSSalarySheet.pSSalarySheetDetails[i].temporaryAmount
                            : 0;
                    }
                    this.pSSalarySheet.pSSalarySheetDetails[i].orderPriority = this.pSSalarySheet.pSSalarySheetDetails[i].stt;
                }
                this.pSSalarySheet.totalNetIncomeAmount = sum;
                if (!this.isCreateUrl && !this.isEditUrl) {
                    this.pSSalarySheet.id = undefined;
                }
                if (this.pSSalarySheet.id !== undefined) {
                    this.subscribeToSaveResponse(this.pSSalarySheetService.update(this.pSSalarySheet));
                } else {
                    this.subscribeToSaveResponse(this.pSSalarySheetService.create(this.pSSalarySheet));
                }
            } else {
            }
        }
    }

    // continueSave() {
    //     this.pSSalarySheet.typeID = this.pSSalarySheet.typeID ? this.pSSalarySheet.typeID : this.sessionPSTimeSheet.typeID;
    //     this.pSSalarySheet.month = this.pSSalarySheet.month ? this.pSSalarySheet.month : this.sessionPSTimeSheet.month;
    //     this.pSSalarySheet.year = this.pSSalarySheet.year ? this.pSSalarySheet.year : this.sessionPSTimeSheet.year;
    //     this.pSSalarySheet.pSSalarySheetName = this.pSSalarySheet.pSSalarySheetName
    //         ? this.pSSalarySheet.pSSalarySheetName
    //         : this.sessionPSTimeSheet.pSSalarySheetName;
    //     this.pSSalarySheet.pSSalarySheetDetails = this.pSSalarySheetDetails;
    //     if (this.checkError()) {
    //         this.isSaving = true;
    //         // check is url new
    //         if (this.pSSalarySheet.id && this.isEditUrl) {
    //             this.isCreateUrl = this.isEdit = false;
    //         }
    //         if (this.isCreateUrl && this.pSSalarySheet.id !== undefined) {
    //             this.pSSalarySheet.id = undefined;
    //             for (let i = 0; i < this.pSSalarySheet.pSSalarySheetDetails.length; i++) {
    //                 this.pSSalarySheet.pSSalarySheetDetails[i].id = undefined;
    //             }
    //         }
    //         let sum = 0;
    //         for (let i = 0; i < this.pSSalarySheet.pSSalarySheetDetails.length; i++) {
    //             sum += this.pSSalarySheet.pSSalarySheetDetails[i].netAmount;
    //         }
    //         this.pSSalarySheet.totalNetIncomeAmount = sum;
    //         if (!this.isCreateUrl && !this.isEditUrl) {
    //             this.pSSalarySheet.id = undefined;
    //         }
    //         if (this.pSSalarySheet.id !== undefined) {
    //             this.subscribeToSaveResponse(this.pSSalarySheetService.update(this.pSSalarySheet));
    //         } else {
    //             this.subscribeToSaveResponse(this.pSSalarySheetService.create(this.pSSalarySheet));
    //         }
    //     }
    // }

    checkError() {
        if (this.pSSalarySheetDetails.length === 0) {
            this.toastr.error(
                this.translate.instant('ebwebApp.pSSalarySheet.detailsIsNotNull'),
                this.translate.instant('ebwebApp.mBDeposit.message')
            );
            return false;
        }
        for (let i = 0; i < this.pSSalarySheetDetails.length; i++) {
            if (!this.pSSalarySheetDetails[i].departmentID) {
                this.toastr.error(
                    this.translate.instant('ebwebApp.pSSalarySheet.departmentIDIsNotNull'),
                    this.translate.instant('ebwebApp.mBDeposit.message')
                );
                return false;
            }
            if (!this.pSSalarySheetDetails[i].employeeID) {
                this.toastr.error(
                    this.translate.instant('ebwebApp.pSSalarySheet.employeeIDIsNotNull'),
                    this.translate.instant('ebwebApp.mBDeposit.message')
                );
                return false;
            }
        }
        return true;
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<any>>) {
        result.subscribe(
            (res: HttpResponse<any>) => {
                if (this.modalRef) {
                    this.modalRef.close();
                }
                const arr = [];
                this.pSSalarySheet.pSSalarySheetDetails.forEach(n => {
                    arr.push({
                        workingDay: n.workingDay,
                        weekendDay: n.weekendDay,
                        holiday: n.holiday,
                        workingDayNight: n.workingDayNight,
                        weekendDayNight: n.weekendDayNight,
                        holidayNight: n.holidayNight,
                        numberOfPaidWorkingHourTimeSheet: n.numberOfPaidWorkingDayTimeSheet
                    });
                });
                if (sessionStorage.getItem('arr')) {
                    sessionStorage.removeItem('arr');
                } else {
                    sessionStorage.setItem('arr', JSON.stringify(arr));
                }
                this.pSSalarySheet = res.body;
                // this.pSSalarySheet.id = res.body.id;
                // this.pSSalarySheet.month = res.body.month;
                // this.pSSalarySheet.year = res.body.year;
                // this.pSSalarySheet.pSSalarySheetName = res.body.pSSalarySheetName;
                // this.pSSalarySheet.typeID = res.body.typeID;
                // this.pSSalarySheet.dayWorks = res.body.dayWorks;
                // this.pSSalarySheet.pSSalarySheetDetails = res.body.pSSalarySheetDetails;
                this.pSSalarySheetDetails = res.body.pSSalarySheetDetails;
                arr.forEach((n, i) => {
                    Object.keys(n).forEach(m => {
                        this.pSSalarySheet.pSSalarySheetDetails[i][m] = n[m];
                        this.pSSalarySheetDetails[i][m] = n[m];
                    });
                });
                this.onSaveSuccess();
                if (this.isEditUrl) {
                    this.toastr.success(
                        this.translate.instant('ebwebApp.pSTimeSheet.editSuccess'),
                        this.translate.instant('ebwebApp.pSTimeSheet.message')
                    );
                } else {
                    this.toastr.success(
                        this.translate.instant('ebwebApp.pSTimeSheet.insertSuccess'),
                        this.translate.instant('ebwebApp.pSTimeSheet.message')
                    );
                }
                this.router.navigate(['./bang-luong', this.pSSalarySheet.id, 'edit']);
                if (this.pSSalarySheetDetails && this.pSSalarySheetDetails.length > 0) {
                    for (let i = 0; i < this.pSSalarySheetDetails.length; i++) {
                        this.pSSalarySheetDetails[i].stt = i + 1;
                        if (this.employees && this.employees.length > 0) {
                            const listAcc = this.employees.filter(
                                a => a.departmentId && a.departmentId === this.pSSalarySheetDetails[i].departmentID
                            );
                            if (listAcc && listAcc.length > 0) {
                                this.pSSalarySheetDetails[i].listEmployeeID = listAcc.map(object => ({ ...object }));
                            }
                            const acc = this.employees.find(a => a.id && a.id === this.pSSalarySheetDetails[i].employeeID);
                            if (!acc) {
                                const employee = this.accountingObjects.find(a => a.id && a.id === this.pSSalarySheetDetails[i].employeeID);
                                if (employee) {
                                    this.pSSalarySheetDetails[i].listEmployeeID.push(employee);
                                }
                            }
                        }
                    }
                }
            },
            (res: HttpErrorResponse) => {
                if (res.error.errorKey !== 'noVoucherLimited') {
                    this.toastr.error(
                        this.translate.instant('ebwebApp.pSTimeSheet.updateError'),
                        this.translate.instant('ebwebApp.mBDeposit.message')
                    );
                }
                this.onSaveError();
                this.isCreateUrl = this.isEdit = true;
            }
        );
    }

    noVoucherExist() {
        this.toastr.error(
            this.translate.instant('global.data.noVocherAlreadyExist'),
            this.translate.instant('ebwebApp.mCReceipt.home.message')
        );
        this.isCreateUrl = this.isEdit = true;
    }

    private onSaveSuccess() {
        this.copy();
        this.isSaving = false;
        this.disableAddButton = true;
        this.disableEditButton = false;
    }

    private onSaveError() {
        this.isSaving = false;
        this.isCreateUrl = !this.isCreateUrl;
        this.isCreateUrl = this.isEdit;
    }

    get pSSalarySheet() {
        return this._pSSalarySheet;
    }

    set pSSalarySheet(pSSalarySheet: IPSSalarySheet) {
        this._pSSalarySheet = pSSalarySheet;
    }

    selectChangeAccountingObject() {}

    addNewRow(eventData: any, select: number) {
        if (this.isCreateUrl) {
            if (select === 0) {
                const ob = Object.assign({}, this.pSSalarySheetDetails[this.pSSalarySheetDetails.length - 1]);
                ob.id = undefined;
                ob.employeeID = null;
                ob.accountingObjectName = null;
                ob.stt = this.pSSalarySheetDetails.length + 1;
                this.pSSalarySheetDetails.push(ob);
                // this.pSTimeSheetDetails[this.pSTimeSheetDetails.length - 1].orderPriority = this.pSTimeSheetDetails.length - 1;
                // const nameTag: string = event.srcElement.id;
                const index: number = this.pSSalarySheetDetails.length;
                const nameTag = 'field_departmentID';
                const nameTagIndex: string = nameTag + String(index);
                setTimeout(function() {
                    const element: HTMLElement = document.getElementById(nameTagIndex);
                    if (element) {
                        element.focus();
                    }
                }, 0);
            } else {
            }
        }
    }

    keyPress(value: number, select: number) {
        if (select === 0) {
            this.pSSalarySheetDetails.splice(value, 1);
        }
    }

    CTKTExportPDF(isDownload, typeReports: number) {
        if (!this.isCreateUrl) {
            this.pSSalarySheetService
                .getCustomerReport({
                    id: this.pSSalarySheet.id,
                    typeID: 790,
                    typeReport: typeReports
                })
                .subscribe(response => {
                    // this.showReport(response);
                    const file = new Blob([response.body], { type: 'application/pdf' });
                    const fileURL = window.URL.createObjectURL(file);
                    if (isDownload) {
                        const link = document.createElement('a');
                        document.body.appendChild(link);
                        link.download = fileURL;
                        link.setAttribute('style', 'display: none');
                        const name = 'Bao_co.pdf';
                        link.setAttribute('download', name);
                        link.href = fileURL;
                        link.click();
                    } else {
                        const contentDispositionHeader = response.headers.get('Content-Disposition');
                        const result = contentDispositionHeader
                            .split(';')[1]
                            .trim()
                            .split('=')[1];
                        const newWin = window.open(fileURL, '_blank');

                        // add a load listener to the window so that the title gets changed on page load
                        newWin.addEventListener('load', function() {
                            newWin.document.title = result.replace(/"/g, '');
                            // this.router.navigate(['/report/buy']);
                        });
                    }
                });
            if (typeReports === 2) {
                this.toastr.success(
                    this.translate.instant('ebwebApp.pSTimeSheet.printing') +
                        ' ' +
                        this.translate.instant('ebwebApp.pSTimeSheet.insuranceSpreadsheets') +
                        '...',
                    this.translate.instant('ebwebApp.pSTimeSheet.message')
                );
            }
        }
    }

    ExportPDF(isDownload, typeReports: number) {
        if (!this.isCreateUrl) {
            this.pSSalarySheetService
                .getCustomerReport({
                    id: this.pSSalarySheet.id,
                    typeID: this.pSSalarySheet.typeID,
                    typeReport: typeReports
                })
                .subscribe(response => {
                    // this.showReport(response);
                    const file = new Blob([response.body], { type: 'application/pdf' });
                    const fileURL = window.URL.createObjectURL(file);
                    if (isDownload) {
                        const link = document.createElement('a');
                        document.body.appendChild(link);
                        link.download = fileURL;
                        link.setAttribute('style', 'display: none');
                        const name = 'Bao_co.pdf';
                        link.setAttribute('download', name);
                        link.href = fileURL;
                        link.click();
                    } else {
                        const contentDispositionHeader = response.headers.get('Content-Disposition');
                        const result = contentDispositionHeader
                            .split(';')[1]
                            .trim()
                            .split('=')[1];
                        const newWin = window.open(fileURL, '_blank');

                        // add a load listener to the window so that the title gets changed on page load
                        newWin.addEventListener('load', function() {
                            newWin.document.title = result.replace(/"/g, '');
                            // this.router.navigate(['/report/buy']);
                        });
                    }
                });
            if (typeReports === 1) {
                this.toastr.success(
                    this.translate.instant('ebwebApp.pSTimeSheet.printing') +
                        ' ' +
                        this.translate.instant('ebwebApp.pSSalarySheet.salarySheetByDays') +
                        '...',
                    this.translate.instant('ebwebApp.pSTimeSheet.message')
                );
            } else if (typeReports === 2) {
                this.toastr.success(
                    this.translate.instant('ebwebApp.pSTimeSheet.printing') +
                        ' ' +
                        this.translate.instant('ebwebApp.pSSalarySheet.salarySheetByHours') +
                        '...',
                    this.translate.instant('ebwebApp.pSTimeSheet.message')
                );
            } else if (typeReports === 3) {
                this.toastr.success(
                    this.translate.instant('ebwebApp.pSTimeSheet.printing') +
                        ' ' +
                        this.translate.instant('ebwebApp.pSSalarySheet.salarySheetDefault') +
                        '...',
                    this.translate.instant('ebwebApp.pSTimeSheet.message')
                );
            }
        }
    }

    // region Tiến lùi chứng từ
    // ham lui, tien
    previousEdit() {
        // goi service get by row num
        if (this.rowNum !== this.count) {
            this.pSSalarySheetService
                .findByRowNum({
                    id: this.pSSalarySheet.id,
                    isNext: false,
                    typeID: this.TYPE_CHAM_CONG_THEO_NGAY,
                    searchVoucher: JSON.stringify(new SearchVoucher())
                })
                .subscribe(
                    (res: HttpResponse<IPSSalarySheet>) => {
                        this.navigate(res.body);
                    },
                    (res: HttpErrorResponse) => this.onError(res.message)
                );
        }
    }

    nextEdit() {
        // goi service get by row num
        if (this.rowNum !== 1) {
            this.pSSalarySheetService
                .findByRowNum({
                    id: this.pSSalarySheet.id,
                    isNext: true,
                    typeID: this.TYPE_CHAM_CONG_THEO_NGAY,
                    searchVoucher: JSON.stringify(new SearchVoucher())
                })
                .subscribe(
                    (res: HttpResponse<IPSSalarySheet>) => {
                        this.navigate(res.body);
                    },
                    (res: HttpErrorResponse) => this.onError(res.message)
                );
        }
    }

    // end of lui tien

    navigate(ipsTimeSheet: IPSSalarySheet) {
        this.router.navigate(['/bang-luong', ipsTimeSheet.id, 'edit']);
    }

    private onError(errorMessage: string) {
        this.toastr.error(this.translate.instant('ebwebApp.pSTimeSheet.error'), this.translate.instant('ebwebApp.pSTimeSheet.message'));
    }

    @ebAuth(['ROLE_ADMIN', ROLE.BangLuong_Xoa])
    delete() {
        if (this.pSSalarySheet.id && !this.isCreateUrl) {
            if (this.pSSalarySheet.gOtherVoucherID) {
                this.toastr.error(
                    this.translate.instant('ebwebApp.pSSalarySheet.existGOtherVoucherID'),
                    this.translate.instant('ebwebApp.mBDeposit.message')
                );
            } else {
                this.copy();
                this.modalRef = this.modalService.open(this.popUpDelete, { backdrop: 'static' });
            }
        }
    }

    onRightClick($event, data, selectedData, isNew, isDelete, select, currentRow, isCopy) {
        this.contextMenu.isNew = isNew;
        this.contextMenu.isDelete = isDelete;
        this.contextMenu.isShow = true;
        this.contextMenu.event = $event;
        this.contextMenu.data = data;
        this.contextMenu.selectedData = selectedData;
        this.contextMenu.isCopy = isCopy;
        this.select = select;
        this.currentRow = currentRow;
    }

    closeContextMenu() {
        this.contextMenu.isShow = false;
    }

    sumAfterDeleteByContextMenu() {
        this.eventSubscriber = this.eventManager.subscribe('afterDeleteRow', response => {
            if (this.isEdit) {
                if (this.select === 0) {
                    this.pSSalarySheetDetails.splice(this.currentRow, 1);
                    for (let i = 0; i < this.pSSalarySheetDetails.length; i++) {
                        this.pSSalarySheetDetails[i].stt = i + 1;
                    }
                }
            }
        });
        this.eventSubscribers.push(this.eventSubscriber);
    }

    keyDownAddRow(value: number) {
        if (this.isCreateUrl && !this.getSelectionText()) {
            if (value !== null && value !== undefined) {
                const ob: IPSSalarySheetDetails = Object.assign({}, this.pSSalarySheetDetails[value]);
                ob.id = undefined;
                ob.orderPriority = undefined;
                ob.stt = this.pSSalarySheetDetails.length + 1;
                this.pSSalarySheetDetails.push(ob);
            } else {
                this.pSSalarySheetDetails.push({});
            }
        }
    }

    afterAddRow() {
        this.eventSubscriber = this.eventManager.subscribe('afterAddNewRow', response => {
            this.pSSalarySheetDetails.push({ stt: this.pSSalarySheetDetails.length + 1 });
        });
        this.eventSubscribers.push(this.eventSubscriber);
    }

    registerCopyRow() {
        this.eventSubscriber = this.eventManager.subscribe('afterCopyRow', response => {
            const ob: IPSSalarySheetDetails = Object.assign({ stt: this.pSSalarySheetDetails.length + 1 }, this.contextMenu.selectedData);
            ob.id = undefined;
            ob.orderPriority = undefined;
            this.pSSalarySheetDetails.push(ob);
        });
        this.eventSubscribers.push(this.eventSubscriber);
    }

    saveContent() {
        if (this.modalRef) {
            this.modalRef.close();
        }
        this.save(false);
    }

    close() {
        this.copy();
        if (this.modalRef) {
            this.modalRef.close();
        }
        this.closeAll();
    }

    exit() {
        if (this.modalRef) {
            this.modalRef.close();
            return;
        }
    }

    /*
  * hàm ss du lieu 2 object và 2 mảng object
  * return true: neu tat ca giong nhau
  * return fale: neu 1 trong cac object ko giong nhau
  * */
    canDeactive(): boolean {
        if (this.isReadOnly) {
            return true;
        } else {
            return (
                this.utilsService.isEquivalent(this.pSSalarySheet, this.pSSalarySheetCopy) &&
                this.utilsService.isEquivalentArray(this.pSSalarySheetDetails, this.pSSalarySheetDetailsCopy)
            );
        }
    }

    ngAfterViewInit(): void {
        if (this.isCreateUrl) {
            this.focusFirstInput();
        }
    }

    ngAfterViewChecked(): void {
        this.disableInput();
    }

    @ebAuth(['ROLE_ADMIN', ROLE.BangLuong_In])
    print($event?) {
        event.preventDefault();
    }

    saveDetails(i) {
        this.currentRow = i;
        this.details = this.pSSalarySheetDetails;
    }

    saveParent() {
        this.currentRow = null;
        this.parent = this.pSSalarySheet;
    }

    addDataToDetail() {
        this.pSSalarySheetDetails = this.details ? this.details : this.pSSalarySheetDetails;
        this.pSSalarySheet = this.parent ? this.parent : this.pSSalarySheet;
    }

    continueDelete() {
        this.pSSalarySheetService.delete(this.pSSalarySheet.id).subscribe(response => {
            this.modalRef.close();
            this.toastr.success(this.translate.instant('ebwebApp.pSSalarySheet.deleteSuccessful'));
            this.router.navigate(['bang-luong']);
        });
    }

    closePopUpDelete() {
        if (this.modalRef) {
            this.modalRef.close();
        }
    }

    newArr(lenght: number): any[] {
        if (lenght > 0) {
            return new Array(lenght);
        } else {
            return new Array(0);
        }
    }

    selectChangeEmployeeID(detail, id) {
        if (id) {
            const existDepartment = this.pSSalarySheetDetails.filter(a => a.departmentID === detail.departmentID);
            if (existDepartment) {
                const existEmployee = existDepartment.filter(a => a.employeeID === id);
                if (existEmployee && existEmployee.length > 1) {
                    detail.employeeID = '';
                    detail.accountingObjectName = null;
                    detail.accountingObjectTitle = null;
                    return this.toastr.error(this.translate.instant('ebwebApp.pSSalarySheet.existEmployee'));
                }
            }
            if (this.pPTinhLuong === '1') {
                detail.agreementSalary =
                    (detail.salaryCoefficient ? detail.salaryCoefficient : 0) * (detail.basicWage ? detail.basicWage : 0);
            }
            if (this.employeess && this.pSSalarySheetDetails && this.pSSalarySheetDetails.length > 0) {
                const employee = this.employeess.find(a => a.id === id);
                if (employee) {
                    detail.accountingObjectName = employee.accountingObjectName;
                    detail.accountingObjectTitle = employee.contactTitle;
                    detail.agreementSalary = employee.agreementSalary;
                    if (this.pSSalarySheet.typeID === this.TYPE_BANG_LUONG_THEO_BUOI) {
                        detail.workingDayUnitPrice = this.utilsService.round(
                            employee.agreementSalary / this.pSSalarySheet.dayWorks,
                            this.currentAccount.systemOption,
                            7
                        );
                    } else if (this.pSSalarySheet.typeID === this.TYPE_BANG_LUONG_THEO_GIO) {
                        detail.workingHourUnitPrice = this.utilsService.round(
                            employee.agreementSalary /
                                (this.pSSalarySheet.dayWorks *
                                    (this.pSSalaryTaxInsuranceRegulation && this.pSSalaryTaxInsuranceRegulation.workHoursInDay
                                        ? this.pSSalaryTaxInsuranceRegulation.workHoursInDay
                                        : 0)),
                            this.currentAccount.systemOption,
                            7
                        );
                    }
                    if (this.pSSalarySheet.typeID !== this.TYPE_BANG_LUONG_TAM_UNG) {
                        if (this.pSSalarySheet.typeID !== this.TYPE_BANG_LUONG_CB_CO_DINH) {
                            detail.numberOfPaidWorkingDayTimeSheet = 0;
                            detail.paidWorkingDayAmount = 0;
                            detail.numberOfNonWorkingDayTimeSheet = 0;
                            detail.nonWorkingDayAmount = 0;
                            detail.totalOverTime = 0;
                            detail.overTimeAmount = 0;
                        }
                        detail.temporaryAmount = 0;
                        detail.employeeSocityInsuranceAmount = this.utilsService.round(
                            (this.pSSalaryTaxInsuranceRegulation.employeeSocityInsurancePercent
                                ? this.pSSalaryTaxInsuranceRegulation.employeeSocityInsurancePercent
                                : 0) * (detail.insuranceSalary ? detail.insuranceSalary : 0),
                            this.currentAccount.systemOption,
                            7
                        );
                        detail.employeeAccidentInsuranceAmount = this.utilsService.round(
                            (this.pSSalaryTaxInsuranceRegulation.employeeAccidentInsurancePercent
                                ? this.pSSalaryTaxInsuranceRegulation.employeeAccidentInsurancePercent
                                : 0) * (detail.insuranceSalary ? detail.insuranceSalary : 0),
                            this.currentAccount.systemOption,
                            7
                        );
                        detail.employeeUnEmployeeInsuranceAmount = this.utilsService.round(
                            (this.pSSalaryTaxInsuranceRegulation.employeeUnEmployeeInsurancePercent
                                ? this.pSSalaryTaxInsuranceRegulation.employeeUnEmployeeInsurancePercent
                                : 0) * (detail.insuranceSalary ? detail.insuranceSalary : 0),
                            this.currentAccount.systemOption,
                            7
                        );
                        detail.employeeTradeUnionInsuranceAmount = this.utilsService.round(
                            (this.pSSalaryTaxInsuranceRegulation.employeeTradeUnionInsurancePercent
                                ? this.pSSalaryTaxInsuranceRegulation.employeeTradeUnionInsurancePercent
                                : 0) * (detail.insuranceSalary ? detail.insuranceSalary : 0),
                            this.currentAccount.systemOption,
                            7
                        );
                    }
                    this.selectChangeSumOfDeductionAmount(detail);
                    this.selectChangeTotalAmount(detail);
                    this.selectChangeIncomeForTaxCalculation(detail);
                    this.selectChangeNetAmount(detail);
                }
            }
        }
    }

    // selectChangeTotal(i) {
    //     this.pSSalarySheetDetails[i].total =
    //         (this.pSSalarySheetDetails[i].workAllDay ? this.pSSalarySheetDetails[i].workAllDay : 0) +
    //         (this.pSSalarySheetDetails[i].workHalfAday ? this.pSSalarySheetDetails[i].workHalfAday : 0) +
    //         (this.pSSalarySheetDetails[i].totalOverTime ? this.pSSalarySheetDetails[i].totalOverTime : 0);
    // }

    selectChangeDepartmentID(detail) {
        if (detail.departmentID) {
            const listAcc = this.employeess.filter(a => a.departmentId === detail.departmentID);
            detail.listEmployeeID = [];
            detail.employeeID = null;
            detail.accountingObjectName = null;
            detail.accountingObjectTitle = null;
            if (listAcc && listAcc.length > 0) {
                detail.listEmployeeID = listAcc.map(object => ({ ...object }));
            }
        } else {
            detail.listEmployeeID = [];
        }
    }

    selectChangeTotalAmount(detail) {
        if (this.pSSalarySheet.typeID !== this.TYPE_BANG_LUONG_TAM_UNG) {
            detail.totalAmount = 0;
            if (this.pSSalarySheet.typeID !== this.TYPE_BANG_LUONG_CB_CO_DINH) {
                detail.totalAmount +=
                    (detail.paidWorkingDayAmount ? detail.paidWorkingDayAmount : 0) +
                    (detail.nonWorkingDayAmount ? detail.nonWorkingDayAmount : 0) +
                    (detail.overTimeAmount ? detail.overTimeAmount : 0);
            } else {
                detail.totalAmount += detail.agreementSalary ? detail.agreementSalary : 0;
            }
            detail.totalAmount +=
                // (detail.agreementSalary ? detail.agreementSalary : 0) +
                (detail.payrollFundAllowance ? detail.payrollFundAllowance : 0) +
                (detail.otherAllowance ? detail.otherAllowance : 0) +
                (detail.notIncomeTaxAllowance ? detail.notIncomeTaxAllowance : 0);
            // detail.totalPersonalTaxIncomeAmount =
            //     (detail.payrollFundAllowance ? detail.payrollFundAllowance : 0) +
            //     (detail.otherAllowance ? detail.otherAllowance : 0);
            detail.totalPersonalTaxIncomeAmount =
                (detail.totalAmount ? detail.totalAmount : 0) - (detail.notIncomeTaxAllowance ? detail.notIncomeTaxAllowance : 0);
            this.selectChangeTotalPersonalTaxIncomeAmount(detail);
            this.selectChangeIncomeForTaxCalculation(detail);
            this.selectChangeNetAmount(detail);
        }
    }

    selectChangeTotalPersonalTaxIncomeAmount(detail) {
        detail.totalPersonalTaxIncomeAmount =
            (detail.totalAmount ? detail.totalAmount : 0) - (detail.notIncomeTaxAllowance ? detail.notIncomeTaxAllowance : 0);
    }

    checkCalIncomeTaxAmount(detail) {
        if (!detail.isUnOfficialStaff) {
            this.findGradePersonalSalaryTax(detail);
        } else {
            detail.incomeTaxAmount = this.utilsService.round(
                (detail.incomeForTaxCalculation ? detail.incomeForTaxCalculation : 0) * 0.1,
                this.currentAccount.systemOption,
                7
            );
            detail.sumOfDeductionAmount =
                (detail.employeeSocityInsuranceAmount ? detail.employeeSocityInsuranceAmount : 0) +
                (detail.employeeAccidentInsuranceAmount ? detail.employeeAccidentInsuranceAmount : 0) +
                (detail.employeeMedicalInsuranceAmount ? detail.employeeMedicalInsuranceAmount : 0) +
                (detail.employeeUnEmployeeInsuranceAmount ? detail.employeeUnEmployeeInsuranceAmount : 0) +
                (detail.employeeTradeUnionInsuranceAmount ? detail.employeeTradeUnionInsuranceAmount : 0) +
                (detail.incomeTaxAmount ? detail.incomeTaxAmount : 0);
            this.selectChangeNetAmount(detail);
        }
    }

    findGradePersonalSalaryTax(detail) {
        if (this.personalSalaryTaxes && this.personalSalaryTaxes.length > 0 && this.pSSalarySheet.typeID !== this.TYPE_BANG_LUONG_TAM_UNG) {
            for (let j = 0; j < this.personalSalaryTaxes.length; j++) {
                if (
                    detail.incomeForTaxCalculation &&
                    detail.incomeForTaxCalculation > this.personalSalaryTaxes[j].fromAmount &&
                    detail.incomeForTaxCalculation <= this.personalSalaryTaxes[j].toAmount
                ) {
                    return this.calIncomeTaxAmount(detail, this.personalSalaryTaxes[j].personalSalaryTaxGrade);
                }
            }
        }
        detail.incomeTaxAmount = 0;
        if (this.pSSalarySheet.typeID !== this.TYPE_BANG_LUONG_TAM_UNG) {
            detail.sumOfDeductionAmount =
                (detail.employeeSocityInsuranceAmount ? detail.employeeSocityInsuranceAmount : 0) +
                (detail.employeeAccidentInsuranceAmount ? detail.employeeAccidentInsuranceAmount : 0) +
                (detail.employeeMedicalInsuranceAmount ? detail.employeeMedicalInsuranceAmount : 0) +
                (detail.employeeUnEmployeeInsuranceAmount ? detail.employeeUnEmployeeInsuranceAmount : 0) +
                (detail.employeeTradeUnionInsuranceAmount ? detail.employeeTradeUnionInsuranceAmount : 0) +
                (detail.incomeTaxAmount ? detail.incomeTaxAmount : 0);
        }
        this.selectChangeNetAmount(detail);
    }

    calIncomeTaxAmount(detail, grade) {
        if (this.personalSalaryTaxes && this.personalSalaryTaxes.length > 0 && this.pSSalarySheet.typeID !== this.TYPE_BANG_LUONG_TAM_UNG) {
            detail.incomeTaxAmount = 0;
            for (let j = 0; j < this.personalSalaryTaxes.length; j++) {
                if (
                    this.personalSalaryTaxes[j].personalSalaryTaxGrade != null &&
                    this.personalSalaryTaxes[j].personalSalaryTaxGrade <= grade
                ) {
                    if (detail.incomeForTaxCalculation > this.personalSalaryTaxes[j].toAmount) {
                        detail.incomeTaxAmount += this.personalSalaryTaxes[j].taxAmountInPersonalSalary;
                    } else {
                        detail.incomeTaxAmount += this.utilsService.round(
                            (detail.incomeForTaxCalculation - this.personalSalaryTaxes[j].fromAmount) *
                                (this.personalSalaryTaxes[j].taxRate / 100),
                            this.currentAccount,
                            7
                        );
                    }
                }
            }
            // const personalSalaryTax = this.personalSalaryTaxes.find(a => a.personalSalaryTaxGrade === grade);
            // detail.incomeTaxAmount =
            //     ((detail.incomeForTaxCalculation ? detail.incomeForTaxCalculation : 0) * (personalSalaryTax.taxRate / 100)) - sum;
            if (this.pSSalarySheet.typeID !== this.TYPE_BANG_LUONG_TAM_UNG) {
                detail.sumOfDeductionAmount =
                    (detail.employeeSocityInsuranceAmount ? detail.employeeSocityInsuranceAmount : 0) +
                    (detail.employeeAccidentInsuranceAmount ? detail.employeeAccidentInsuranceAmount : 0) +
                    (detail.employeeMedicalInsuranceAmount ? detail.employeeMedicalInsuranceAmount : 0) +
                    (detail.employeeUnEmployeeInsuranceAmount ? detail.employeeUnEmployeeInsuranceAmount : 0) +
                    (detail.employeeTradeUnionInsuranceAmount ? detail.employeeTradeUnionInsuranceAmount : 0) +
                    (detail.incomeTaxAmount ? detail.incomeTaxAmount : 0);
            }
            this.selectChangeNetAmount(detail);
        }
    }

    selectChangeSumOfDeductionAmount(detail) {
        if (this.pSSalarySheet.typeID !== this.TYPE_BANG_LUONG_TAM_UNG) {
            detail.sumOfDeductionAmount =
                (detail.employeeSocityInsuranceAmount ? detail.employeeSocityInsuranceAmount : 0) +
                (detail.employeeAccidentInsuranceAmount ? detail.employeeAccidentInsuranceAmount : 0) +
                (detail.employeeMedicalInsuranceAmount ? detail.employeeMedicalInsuranceAmount : 0) +
                (detail.employeeUnEmployeeInsuranceAmount ? detail.employeeUnEmployeeInsuranceAmount : 0) +
                (detail.employeeTradeUnionInsuranceAmount ? detail.employeeTradeUnionInsuranceAmount : 0) +
                (detail.incomeTaxAmount ? detail.incomeTaxAmount : 0);
            this.selectChangeIncomeForTaxCalculation(detail);
        }
    }

    selectChangeIncomeForTaxCalculation(detail) {
        if (this.pSSalarySheet.typeID !== this.TYPE_BANG_LUONG_TAM_UNG) {
            const incomeForTaxCalculation =
                (detail.totalPersonalTaxIncomeAmount ? detail.totalPersonalTaxIncomeAmount : 0) -
                (detail.employeeSocityInsuranceAmount ? detail.employeeSocityInsuranceAmount : 0) -
                (detail.employeeAccidentInsuranceAmount ? detail.employeeAccidentInsuranceAmount : 0) -
                (detail.employeeUnEmployeeInsuranceAmount ? detail.employeeUnEmployeeInsuranceAmount : 0) -
                (detail.employeeMedicalInsuranceAmount ? detail.employeeMedicalInsuranceAmount : 0) -
                (detail.employeeTradeUnionInsuranceAmount ? detail.employeeTradeUnionInsuranceAmount : 0) -
                (detail.reduceSelfTaxAmount ? detail.reduceSelfTaxAmount : 0) -
                (detail.reduceDependTaxAmount ? detail.reduceDependTaxAmount : 0);
            detail.incomeForTaxCalculation = incomeForTaxCalculation > 0 ? incomeForTaxCalculation : 0;
            this.checkCalIncomeTaxAmount(detail);
        }
    }

    selectChangeIncomeTaxAmount(detail) {
        if (this.pSSalarySheet.typeID !== this.TYPE_BANG_LUONG_TAM_UNG) {
            detail.sumOfDeductionAmount =
                (detail.employeeSocityInsuranceAmount ? detail.employeeSocityInsuranceAmount : 0) +
                (detail.employeeAccidentInsuranceAmount ? detail.employeeAccidentInsuranceAmount : 0) +
                (detail.employeeMedicalInsuranceAmount ? detail.employeeMedicalInsuranceAmount : 0) +
                (detail.employeeUnEmployeeInsuranceAmount ? detail.employeeUnEmployeeInsuranceAmount : 0) +
                (detail.employeeTradeUnionInsuranceAmount ? detail.employeeTradeUnionInsuranceAmount : 0) +
                (detail.incomeTaxAmount ? detail.incomeTaxAmount : 0);
            const incomeForTaxCalculation =
                (detail.totalPersonalTaxIncomeAmount ? detail.totalPersonalTaxIncomeAmount : 0) -
                (detail.employeeSocityInsuranceAmount ? detail.employeeSocityInsuranceAmount : 0) -
                (detail.employeeAccidentInsuranceAmount ? detail.employeeAccidentInsuranceAmount : 0) -
                (detail.employeeUnEmployeeInsuranceAmount ? detail.employeeUnEmployeeInsuranceAmount : 0) -
                (detail.employeeMedicalInsuranceAmount ? detail.employeeMedicalInsuranceAmount : 0) -
                (detail.employeeTradeUnionInsuranceAmount ? detail.employeeTradeUnionInsuranceAmount : 0) -
                (detail.reduceSelfTaxAmount ? detail.reduceSelfTaxAmount : 0) -
                (detail.reduceDependTaxAmount ? detail.reduceDependTaxAmount : 0);
            detail.incomeForTaxCalculation = incomeForTaxCalculation > 0 ? incomeForTaxCalculation : 0;
            this.selectChangeNetAmount(detail);
        }
    }

    selectChangeNetAmount(detail) {
        if (this.pSSalarySheet.typeID !== this.TYPE_BANG_LUONG_TAM_UNG) {
            detail.netAmount =
                (detail.totalPersonalTaxIncomeAmount ? detail.totalPersonalTaxIncomeAmount : 0) +
                (detail.notIncomeTaxAllowance ? detail.notIncomeTaxAllowance : 0) -
                (detail.sumOfDeductionAmount ? detail.sumOfDeductionAmount : 0) -
                (detail.temporaryAmount ? detail.temporaryAmount : 0);
        }
    }

    selectChangeAgreementSalary(detail) {
        if (this.pSSalarySheet.typeID !== this.TYPE_BANG_LUONG_TAM_UNG) {
            if (!detail.agreementSalary) {
                detail.totalAmount = 0;
            }
            if (this.pPTinhLuong === '1') {
                detail.agreementSalary =
                    (detail.salaryCoefficient ? detail.salaryCoefficient : 0) * (detail.basicWage ? detail.basicWage : 0);
            }
            if (this.pSSalarySheet.typeID === this.TYPE_BANG_LUONG_THEO_BUOI) {
                detail.workingDayUnitPrice =
                    (detail.agreementSalary ? detail.agreementSalary : 0) /
                    (this.pSSalarySheet.dayWorks ? this.pSSalarySheet.dayWorks : this.sessionPSTimeSheet.daysWork);
                this.calculateOverTimeAmount(detail, detail.workingDayUnitPrice);
            } else {
                detail.workingHourUnitPrice = this.utilsService.round(
                    (detail.agreementSalary ? detail.agreementSalary : 0) /
                        ((this.pSSalarySheet.dayWorks ? this.pSSalarySheet.dayWorks : this.sessionPSTimeSheet.daysWork) *
                            (this.pSSalaryTaxInsuranceRegulation && this.pSSalaryTaxInsuranceRegulation.workHoursInDay
                                ? this.pSSalaryTaxInsuranceRegulation.workHoursInDay
                                : 0)),
                    this.currentAccount.systemOption,
                    7
                );
                this.calculateOverTimeAmount(detail, detail.workingHourUnitPrice);
            }
            if (this.pSSalarySheet.dayWorks !== detail.numberOfPaidWorkingDayTimeSheet) {
                detail.paidWorkingDayAmount = this.utilsService.round(
                    (this.pSSalarySheet.typeID === this.TYPE_BANG_LUONG_THEO_BUOI
                        ? detail.workingDayUnitPrice
                        : detail.workingHourUnitPrice) * detail.numberOfPaidWorkingDayTimeSheet,
                    this.currentAccount.systemOption,
                    7
                );
            } else {
                detail.paidWorkingDayAmount = detail.agreementSalary;
            }
            this.selectChangeTotalAmount(detail);
            this.selectNumberOfPaidWorkingDayTimeSheet(detail);
            // detail.overTimeAmount = 0;
        }
    }

    selectNumberOfPaidWorkingDayTimeSheet(detail) {
        if (this.pSSalarySheet.typeID !== this.TYPE_BANG_LUONG_CB_CO_DINH && this.pSSalarySheet.typeID !== this.TYPE_BANG_LUONG_TAM_UNG) {
            if (this.pSSalarySheet.typeID === this.TYPE_BANG_LUONG_THEO_BUOI) {
                if (this.pSSalarySheet.dayWorks !== detail.numberOfPaidWorkingDayTimeSheet) {
                    detail.paidWorkingDayAmount = this.utilsService.round(
                        detail.workingDayUnitPrice * detail.numberOfPaidWorkingDayTimeSheet,
                        this.currentAccount.systemOption,
                        7
                    );
                    this.calculateOverTimeAmount(detail, detail.workingDayUnitPrice);
                } else {
                    detail.paidWorkingDayAmount = detail.agreementSalary;
                }
            } else {
                // if (
                //     this.pSSalaryTaxInsuranceRegulation &&
                //     this.pSSalaryTaxInsuranceRegulation.workHoursInDay &&
                //     this.pSSalarySheet.dayWorks * this.pSSalaryTaxInsuranceRegulation.workHoursInDay !==
                //         detail.numberOfPaidWorkingDayTimeSheet
                // ) {
                //     detail.paidWorkingDayAmount = this.utilsService.round(
                //         detail.workingHourUnitPrice * detail.numberOfPaidWorkingHourTimeSheet,
                //         this.currentAccount.systemOption,
                //         7
                //     );
                //     this.calculateOverTimeAmount(detail, detail.workingHourUnitPrice);
                // } else {
                //     detail.paidWorkingDayAmount = detail.agreementSalary;
                // }
                detail.paidWorkingDayAmount = this.utilsService.round(
                    detail.workingHourUnitPrice * detail.numberOfPaidWorkingHourTimeSheet,
                    this.currentAccount.systemOption,
                    7
                );
                this.calculateOverTimeAmount(detail, detail.workingHourUnitPrice);
            }
            this.selectChangeTotalAmount(detail);
        }
    }

    calculateOverTimeAmount(detail, unitPrice) {
        detail.overTimeAmount = this.utilsService.round(
            detail.workingDay *
                (this.pSSalaryTaxInsuranceRegulation && this.pSSalaryTaxInsuranceRegulation.overtimeDailyPercent
                    ? this.pSSalaryTaxInsuranceRegulation.overtimeDailyPercent / 100
                    : 1) *
                unitPrice +
                detail.weekendDay *
                    (this.pSSalaryTaxInsuranceRegulation && this.pSSalaryTaxInsuranceRegulation.overtimeWeekendPercent
                        ? this.pSSalaryTaxInsuranceRegulation.overtimeWeekendPercent / 100
                        : 1) *
                    unitPrice +
                detail.holiday *
                    (this.pSSalaryTaxInsuranceRegulation && this.pSSalaryTaxInsuranceRegulation.overtimeHolidayPercent
                        ? this.pSSalaryTaxInsuranceRegulation.overtimeHolidayPercent / 100
                        : 1) *
                    unitPrice +
                detail.workingDayNight *
                    (this.pSSalaryTaxInsuranceRegulation && this.pSSalaryTaxInsuranceRegulation.overtimeWorkingDayNightPercent
                        ? this.pSSalaryTaxInsuranceRegulation.overtimeWorkingDayNightPercent / 100
                        : 1) *
                    unitPrice +
                detail.weekendDayNight *
                    (this.pSSalaryTaxInsuranceRegulation && this.pSSalaryTaxInsuranceRegulation.overtimeWeekendDayNightPercent
                        ? this.pSSalaryTaxInsuranceRegulation.overtimeWeekendDayNightPercent / 100
                        : 1) *
                    unitPrice +
                detail.holidayNight *
                    (this.pSSalaryTaxInsuranceRegulation && this.pSSalaryTaxInsuranceRegulation.overtimeHolidayNightPercent
                        ? this.pSSalaryTaxInsuranceRegulation.overtimeHolidayNightPercent / 100
                        : 1) *
                    unitPrice,
            this.currentAccount.systemOption,
            7
        );
    }

    getAccountingObjectTitle(i) {
        if (
            this.pSSalarySheetDetails &&
            this.pSSalarySheetDetails[i].listEmployeeID &&
            this.pSSalarySheetDetails[i].listEmployeeID.length > 0
        ) {
            const epl = this.pSSalarySheetDetails[i].listEmployeeID.find(a => a.id === this.pSSalarySheetDetails[i].employeeID);
            if (epl) {
                this.pSSalarySheetDetails[i].accountingObjectTitle = epl.contactTitle;
            }
        }
    }

    sumFooter(item) {
        if (this.pSSalarySheetDetails && this.pSSalarySheetDetails.length > 0) {
            let total = 0;
            for (let i = 0; i < this.pSSalarySheetDetails.length; i++) {
                total += this.pSSalarySheetDetails[i][item] ? this.pSSalarySheetDetails[i][item] : 0;
            }
            return total;
        }
        return '';
    }

    selectChangeInsuranceSalary(detail) {
        if (this.pSSalaryTaxInsuranceRegulation) {
            detail.employeeSocityInsuranceAmount = this.utilsService.round(
                (this.pSSalaryTaxInsuranceRegulation.employeeSocityInsurancePercent
                    ? this.pSSalaryTaxInsuranceRegulation.employeeSocityInsurancePercent
                    : 0) *
                    (detail.insuranceSalary ? detail.insuranceSalary : 0) /
                    100,
                this.currentAccount.systemOption,
                7
            );
            detail.employeeAccidentInsuranceAmount = this.utilsService.round(
                (this.pSSalaryTaxInsuranceRegulation.employeeAccidentInsurancePercent
                    ? this.pSSalaryTaxInsuranceRegulation.employeeAccidentInsurancePercent
                    : 0) *
                    (detail.insuranceSalary ? detail.insuranceSalary : 0) /
                    100,
                this.currentAccount.systemOption,
                7
            );
            detail.employeeMedicalInsuranceAmount = this.utilsService.round(
                (this.pSSalaryTaxInsuranceRegulation.employeeMedicalInsurancePercent
                    ? this.pSSalaryTaxInsuranceRegulation.employeeMedicalInsurancePercent
                    : 0) *
                    (detail.insuranceSalary ? detail.insuranceSalary : 0) /
                    100,
                this.currentAccount.systemOption,
                7
            );
            detail.employeeUnEmployeeInsuranceAmount = this.utilsService.round(
                (this.pSSalaryTaxInsuranceRegulation.employeeUnEmployeeInsurancePercent
                    ? this.pSSalaryTaxInsuranceRegulation.employeeUnEmployeeInsurancePercent
                    : 0) *
                    (detail.insuranceSalary ? detail.insuranceSalary : 0) /
                    100,
                this.currentAccount.systemOption,
                7
            );
            detail.employeeTradeUnionInsuranceAmount = this.utilsService.round(
                (this.pSSalaryTaxInsuranceRegulation.employeeTradeUnionInsurancePercent
                    ? this.pSSalaryTaxInsuranceRegulation.employeeTradeUnionInsurancePercent
                    : 0) *
                    (detail.insuranceSalary ? detail.insuranceSalary : 0) /
                    100,
                this.currentAccount.systemOption,
                7
            );
            detail.companySocityInsuranceAmount = this.utilsService.round(
                (this.pSSalaryTaxInsuranceRegulation.companySocityInsurancePercent
                    ? this.pSSalaryTaxInsuranceRegulation.companySocityInsurancePercent
                    : 0) *
                    (detail.insuranceSalary ? detail.insuranceSalary : 0) /
                    100,
                this.currentAccount.systemOption,
                7
            );
            detail.companyAccidentInsuranceAmount = this.utilsService.round(
                (this.pSSalaryTaxInsuranceRegulation.companytAccidentInsurancePercent
                    ? this.pSSalaryTaxInsuranceRegulation.companytAccidentInsurancePercent
                    : 0) *
                    (detail.insuranceSalary ? detail.insuranceSalary : 0) /
                    100,
                this.currentAccount.systemOption,
                7
            );
            detail.companyMedicalInsuranceAmount = this.utilsService.round(
                (this.pSSalaryTaxInsuranceRegulation.companyMedicalInsurancePercent
                    ? this.pSSalaryTaxInsuranceRegulation.companyMedicalInsurancePercent
                    : 0) *
                    (detail.insuranceSalary ? detail.insuranceSalary : 0) /
                    100,
                this.currentAccount.systemOption,
                7
            );
            detail.companyUnEmployeeInsuranceAmount = this.utilsService.round(
                (this.pSSalaryTaxInsuranceRegulation.companyUnEmployeeInsurancePercent
                    ? this.pSSalaryTaxInsuranceRegulation.companyUnEmployeeInsurancePercent
                    : 0) *
                    (detail.insuranceSalary ? detail.insuranceSalary : 0) /
                    100,
                this.currentAccount.systemOption,
                7
            );
            detail.companyTradeUnionInsuranceAmount = this.utilsService.round(
                (this.pSSalaryTaxInsuranceRegulation.companyTradeUnionInsurancePercent
                    ? this.pSSalaryTaxInsuranceRegulation.companyTradeUnionInsurancePercent
                    : 0) *
                    (detail.insuranceSalary ? detail.insuranceSalary : 0) /
                    100,
                this.currentAccount.systemOption,
                7
            );
            this.selectChangeSumOfDeductionAmount(detail);
            this.selectChangeNetAmount(detail);
        }
    }

    selectChangeOverTimeAmount(detail) {
        detail.overTimeAmount = 0;
        this.selectChangeTotalAmount(detail);
    }

    addIfLastInput(event, stt, currentRow?) {
        if (
            stt === this.pSSalarySheetDetails.length &&
            ((currentRow === 39 && this.pSSalarySheet.typeID !== this.TYPE_BANG_LUONG_TAM_UNG) ||
                (currentRow === 6 && this.pSSalarySheet.typeID === this.TYPE_BANG_LUONG_TAM_UNG))
        ) {
            this.addNewRow(event, 0);
        }
    }

    clickEnter(e, detail) {
        let newRow = false;
        const cellCurrent = +document.activeElement.getAttribute('tabIndex');
        if (detail.stt - 1 === this.pSSalarySheetDetails.length - 1) {
            // có 39 cell được đánh index
            if (
                (this.pSSalarySheet.typeID !== this.TYPE_BANG_LUONG_TAM_UNG ? 39 : 6) * (detail.stt - 1) +
                    (this.pSSalarySheet.typeID !== this.TYPE_BANG_LUONG_TAM_UNG ? 39 : 6) ===
                cellCurrent
            ) {
                newRow = true;
            }
        }
        if ((this.isEnter && detail.stt - 1 === this.pSSalarySheetDetails.length - 1) || newRow) {
            this.addNewRow(e, 0);
            return;
        }
        const index = (this.pSSalarySheet.typeID !== this.TYPE_BANG_LUONG_TAM_UNG ? 39 : 6) * (detail.stt - 1 + 1) + 2;
        if (this.isEnter && detail.stt - 1 < this.pSSalarySheetDetails.length - 1) {
            const inputs = document.getElementsByTagName('input');
            for (let i = 0; i < inputs.length; i++) {
                if (inputs[i].tabIndex === index) {
                    setTimeout(() => {
                        inputs[i].focus();
                        inputs[i].select();
                    }, 0);
                    break;
                }
            }
        }
    }

    navigateHelp() {
        this.notificationService.findHelpLink({ id: 'BL' }).subscribe((res: HttpResponse<any>) => {
            window.open(res.body, '_blank');
        });
    }

    exportFileExcel() {
        const bangLuongs = [];
        for (let i = 0; i < this.pSSalarySheetDetails.length; i++) {
            const bangLuong = new BangLuongExportExcel();
            bangLuong.setAllFields(this.pSSalarySheetDetails[i], this.organizationUnits);
            bangLuongs.push(bangLuong);
        }
        this.pSSalarySheetDetailsService
            .exportExcel(bangLuongs, { typeID: this.pSSalarySheet.typeID, tinhLuong: this.pPTinhLuong })
            .subscribe(res => {
                const blob = new Blob([res.body], { type: 'application/vnd.ms-excel' });
                const fileURL = URL.createObjectURL(blob);
                const link = document.createElement('a');
                document.body.appendChild(link);
                link.download = fileURL;
                link.setAttribute('style', 'display: none');
                const name = this.pSSalarySheet.pSSalarySheetName
                    ? this.pSSalarySheet.pSSalarySheetName
                    : this.sessionPSTimeSheet.pSTimeSheetName + '.xlsx';
                link.setAttribute('download', name);
                link.href = fileURL;
                link.click();
            });
    }

    exportPDFWebService(valueVoucher?) {
        if (!this.isUseToolIn) {
            if (valueVoucher === TypeMultiPrintTemplate.BANG_LUONG_THEO_BUOI) {
                this.ExportPDF(false, 1);
            } else if (valueVoucher === TypeMultiPrintTemplate.BANG_LUONG_THEO_GIO) {
                this.ExportPDF(false, 2);
            } else if (valueVoucher === TypeMultiPrintTemplate.BANG_LUONG_CO_BAN_CO_DINH) {
                this.ExportPDF(false, 3);
            } else if (valueVoucher === TypeMultiPrintTemplate.BANG_TINH_BAO_HIEM) {
                this.CTKTExportPDF(false, 2);
            }
        } else {
            const listID = [];
            listID.push(this.pSSalarySheet.id);
            this.inChungTuHangLoatService.inChungTuHangLoat({
                typeGroupID: TYPEGROUP.BANG_LUONG,
                typeReport: valueVoucher,
                listID,
                fromDate: null,
                toDate: null,
                paramCheckAll: false
            });
        }
    }

    checkSameOrg() {
        if (
            this.pSSalarySheet.id !== null &&
            this.pSSalarySheet.id !== undefined &&
            this.pSSalarySheet.companyID !== null &&
            this.pSSalarySheet.companyID !== undefined
        ) {
            return this.pSSalarySheet.companyID === this.currentAccount.organizationUnit.id;
        } else {
            return true;
        }
    }
}
