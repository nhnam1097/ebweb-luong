import { Component, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiAlertService, JhiEventManager, JhiParseLinks } from 'ng-jhipster';

import { Principal } from 'app/core';

import { DATE_FORMAT, ITEMS_PER_PAGE } from 'app/shared';
import { DatePipe } from '@angular/common';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
import { MSGERROR, SO_LAM_VIEC, TCKHAC_MauCTuChuaGS, TYPEGROUP, TypeID } from 'app/app.constants';
import { BaseComponent } from 'app/shared/base-component/base.component';
import { ROLE } from 'app/role.constants';
import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ebAuth } from 'app/shared/decorator/ebAuth.decorator';
import { BangLuongService } from 'app/luong/bang_luong';
import { CurrencyMaskModel } from 'app/shared/directive/ng2-currency-mask/currency-mask.model';
import { IGOtherVoucher } from 'app/shared/models/g-other-voucher.model';
import { IGOtherVoucherDetails } from 'app/shared/models/g-other-voucher-details.model';
import { ISearchVoucher } from 'app/shared/models/SearchVoucher';
import { IPSSalarySheet } from 'app/shared/models/ps-salary-sheet.model';
import { IDataSessionStorage } from 'app/shared/models/DataSessionStorage';
import { ISessionPSTimeSheet } from 'app/shared/models/session-p-s-time-sheet';
import { AccountingObjectService } from 'app/shared/services/accounting-object.service';
import { TypeService } from 'app/shared/services/type.service';
import { UtilsService } from 'app/shared/UtilsService/Utils.service';
import { RefModalService } from 'app/shared/services/ref-modal.service';
import { NotificationService } from 'app/shared/notification';
import { InChungTuHangLoatService } from 'app/shared/in-chung-tu-hang-loat/in-chung-tu-hang-loat.service';
import { EbReportPdfPopupComponent } from 'app/shared/show-pdf/eb-report-pdf-popup.component';
import { HandlingResultComponent } from 'app/shared/handling-result/handling-result.component';
import { ICurrency } from 'app/shared/models/currency.model';
import { IPrepaidExpense } from 'app/shared/models/prepaid-expense.model';
import { IGoodsServicePurchase } from 'app/shared/models/goods-service-purchase.model';
import { Irecord } from 'app/shared/models/record';
import { GeneralLedgerService } from 'app/shared/services/general-ledger.service';
import { RequestRecordListDtoModel } from 'app/shared/models/request-record-list-dto.model';
import { CurrencyService } from 'app/shared/services/currency.service';
import { GoodsServicePurchaseService } from 'app/shared/services/goods-service-purchase.service';
import { PrepaidExpenseService } from 'app/shared/services/prepaid-expense.service';
import { ChiPhiTraTruocService } from 'app/shared/services/chi-phi-tra-truoc.service';
import { GOtherVoucherService } from 'app/shared/services/chung-tu-nghiep-vu-khac.service';

@Component({
    selector: 'eb-hach-toan-chi-phi-luong',
    templateUrl: './hach-toan-chi-phi-luong.component.html',
    styleUrls: ['./hach-toan-chi-phi-luong.component.css']
})
export class HachToanChiPhiLuongComponent extends BaseComponent implements OnInit, OnDestroy {
    @ViewChild('popUpMultiDelete') popUpMultiDelete: TemplateRef<any>;
    @ViewChild('contentAddNew') public popUpAddNew: NgbModalRef;
    currentAccount: any;
    gOtherVouchers: IGOtherVoucher[];
    isSaving: boolean;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    typeID: string;
    fromDate: any;
    toDate: any;
    currencyID: string;
    recorded: boolean;
    accountingObjectCode: string;
    currencies: ICurrency[];
    gOtherVoucherDetails: IGOtherVoucherDetails[];
    prepaidExpenses: IPrepaidExpense[];
    objSearch: ISearchVoucher;
    typeName: string;
    date: any;
    isRecord: Boolean;
    items: any;
    record_: any;
    selectedRow: IGOtherVoucher;
    rowNum: number;
    pageCount: any;
    accountingObjectName: string;
    translateTypeGOtherVoucher: string;
    listRecord: any;
    listColumnsRecord: string[] = ['name'];
    listHeaderColumnsRecord: string[] = ['Trạng thái'];
    statusRecordAfterFind: Boolean;
    mutipleRowSelected: IGOtherVoucher[];
    isKeyPressed: boolean;
    viewVouchers: any;
    options: any;
    isRecorded: boolean;
    isUnRecorded: boolean;
    goodsServicePurchases: IGoodsServicePurchase[];
    prepaidExpenseCodeList: any[];
    isSoTaiChinh: boolean;
    isFocusCurrentVoucher: boolean;
    index: any;
    statusRecord: any;
    currentRow: any;
    TYPE_HACH_TOAN_CHI_PHI_LUONG = 708;
    color: string;
    currency: ICurrency;
    isNavigateForm: boolean;
    modalRef: NgbModalRef;
    typeMultiAction: number;
    isShowGoodsServicePurchase: boolean;
    ROLE_KET_CHUYEN_LAI_LO = ROLE.KetChuyenLaiLo_Xem;
    ROLE_PHAN_BO_CHI_PHI_TRA_TRUOC = ROLE.PhanBoChiPhiTRaTruoc_Xem;
    isRoleKetChuyenLaiLo: boolean;
    isRolePhanBoChiPhiTraTruoc: boolean;
    pSSalarySheetID: string;
    pSSalarySheetList: IPSSalarySheet[];
    isShowSearch: boolean;
    reSearchWhenRecord: string;
    typeCreate: number;
    isSingleClick: any;

    ROLE_HTCPL_XEM = ROLE.HachToanChiPhiLuong_XEM;
    ROLE_HTCPL_THEM = ROLE.HachToanChiPhiLuong_Them;
    ROLE_HTCPL_SUA = ROLE.HachToanChiPhiLuong_Sua;
    ROLE_HTCPL_XOA = ROLE.HachToanChiPhiLuong_Xoa;
    ROLE_HTCPL_GHISO = ROLE.HachToanChiPhiLuong_GhiSo;
    ROLE_HTCPL_IN = ROLE.HachToanChiPhiLuong_In;
    ROLE_HTCPL_KETXUAT = ROLE.HachToanChiPhiLuong_KetXuat;

    buttonDeleteTranslate = 'ebwebApp.mBDeposit.toolTip.delete';
    buttonAddTranslate = 'ebwebApp.mBDeposit.toolTip.add';
    buttonEditTranslate = 'ebwebApp.mBDeposit.toolTip.edit';
    buttonRecordTranslate = 'ebwebApp.mBDeposit.toolTip.record';
    buttonUnRecordTranslate = 'ebwebApp.mBDeposit.toolTip.unrecord';
    buttonExportTranslate = 'ebwebApp.mBDeposit.toolTip.export';
    buttonSearchTranslate = 'ebwebApp.mBDeposit.toolTip.search';

    // navigate update form
    dataSession: IDataSessionStorage = new class implements IDataSessionStorage {
        accountingObjectName: string;
        itemsPerPage: number;
        page: number;
        pageCount: number;
        predicate: number;
        reverse: number;
        rowNum: number;
        totalItems: number;
    }();
    customCurrencyMaskConfig: CurrencyMaskModel;

    sessionPSTimeSheet: ISessionPSTimeSheet = new class implements ISessionPSTimeSheet {
        month: number;
        year: number;
        pSSalarySheetName?: string;
        typeCreate?: number;
        lastPSTimeSheetID?: string;
        lastPSTimeSheetSummaryID?: string;
        lastPSSalarySheetID?: string;
        autoAddNewEmployee?: boolean;
        getEmployeeNotActive?: boolean;
        listDepartmentID?: any[];
    }();

    constructor(
        private gOtherVoucherService: GOtherVoucherService,
        private parseLinks: JhiParseLinks,
        private jhiAlertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private currencyService: CurrencyService,
        public utilsService: UtilsService,
        private datepipe: DatePipe,
        private toastr: ToastrService,
        public translate: TranslateService,
        private goodsServicePurchaseService: GoodsServicePurchaseService,
        private refModalService: RefModalService,
        private prepaidExpenseService: PrepaidExpenseService,
        private chiPhiTraTruocService: ChiPhiTraTruocService,
        private modalService: NgbModal,
        public activeModal: NgbActiveModal,
        private pSSalarySheetService: BangLuongService,
        private notificationService: NotificationService,
        private inChungTuHangLoatService: InChungTuHangLoatService,
        private gLService: GeneralLedgerService
    ) {
        super();
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe(data => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
        });
        if (this.isSoTaiChinh) {
            this.predicate = 'date desc,postedDate desc,noFBook';
        } else {
            this.predicate = 'date desc,postedDate desc,noMBook';
        }
        this.translate.get(['ebwebApp.mBCreditCard.home.recorded', 'ebwebApp.mBCreditCard.home.unrecorded']).subscribe(res => {
            this.listRecord = [
                { value: 1, name: res['ebwebApp.mBCreditCard.home.recorded'] },
                { value: 0, name: res['ebwebApp.mBCreditCard.home.unrecorded'] }
            ];
        });
        this.isRecorded = true;
        this.isUnRecorded = true;
        this.translateTypeGOtherVoucher = this.translate.instant('ebwebApp.gOtherVoucher.home.title');
    }

    loadAll() {
        this.selectedRows = [];
        if (sessionStorage.getItem('dataSession')) {
            this.dataSession = JSON.parse(sessionStorage.getItem('dataSession'));
            this.page = this.dataSession.page;
            this.itemsPerPage = this.dataSession.itemsPerPage;
            this.isShowSearch = this.dataSession.isShowSearch;
            this.predicate = this.dataSession.predicate;
            this.reverse = this.dataSession.reverse;
            this.previousPage = this.dataSession.page;
            this.objSearch = this.dataSession.searchVoucher != null ? JSON.parse(this.dataSession.searchVoucher) : {};
            this.statusRecord = this.objSearch.statusRecorded;
            sessionStorage.removeItem('dataSession');
        }
        if (this.activatedRoute.snapshot.paramMap.has('isSearch')) {
            this.objSearch.statusRecorded = this.statusRecord;
            this.gOtherVoucherService
                .searchAll({
                    page: this.page - 1,
                    size: this.itemsPerPage,
                    sort: this.sort(),
                    searchVoucher: this.dataSession.searchVoucher
                })
                .subscribe(
                    (res: HttpResponse<IGOtherVoucher[]>) => this.paginateGOtherVouchers(res.body, res.headers),
                    (res: HttpErrorResponse) => this.onError(res.message)
                );
            this.isShowSearch = true;
            return;
        } else {
            // this.router.navigate(['/g-other-voucher'], {
            //     queryParams: {
            //         page: this.page,
            //         size: this.itemsPerPage,
            //         sort: this.predicate + ',' + (this.reverse ? 'desc' : 'asc')
            //     }
            // });
            this.router.navigate(['/hach-toan-chi-phi-luong']);
        }
        this.gOtherVoucherService
            .searchAll({
                page: this.page - 1,
                size: this.itemsPerPage,
                sort: this.sort(),
                searchVoucher: JSON.stringify(this.saveSearchVoucher())
            })
            .subscribe(
                (res: HttpResponse<IGOtherVoucher[]>) => this.paginateGOtherVouchers(res.body, res.headers),
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.dataSession.rowNum = 0;
        this.router.navigate(['/hach-toan-chi-phi-luong']);
        // this.router.navigate(['/hach-toan-chi-phi-luong'], {
        //     queryParams: {
        //         page: this.page,
        //         size: this.itemsPerPage,
        //         sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        //     }
        // });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.router.navigate(['/hach-toan-chi-phi-luong']);
        this.loadAll();
    }

    ngOnInit() {
        this.objSearch = new class implements ISearchVoucher {
            accountingObjectID: string;
            currencyID: string;
            fromDate: moment.Moment;
            statusRecorded: boolean;
            textSearch: string;
            toDate: moment.Moment;
            typeID: number;
        }();
        this.dataSession = new class implements IDataSessionStorage {
            creditCardType: string;
            itemsPerPage: number;
            ownerCard: string;
            page: number;
            pageCount: number;
            predicate: number;
            reverse: number;
            rowNum: number;
            searchVoucher: string;
            totalItems: number;
        }();
        this.reSearchWhenRecord = '{}';
        if (sessionStorage.getItem('checkNewHTCPL')) {
            if (this.modalRef) {
                this.modalRef.close();
            }
            this.pSSalarySheetService.getPSSalarySheets().subscribe((res: HttpResponse<IPSSalarySheet[]>) => {
                this.pSSalarySheetList = res.body.filter(a => !a.gOtherVoucherID && a.typeID !== 774);
            });
            this.modalRef = this.modalService.open(this.popUpAddNew, { backdrop: 'static' });
            sessionStorage.removeItem('checkNewHTCPL');
        }
        this.isNavigateForm = true;
        this.index = 0;
        this.isRecorded = false;
        this.isUnRecorded = false;
        this.statusRecordAfterFind = null;
        this.isFocusCurrentVoucher = true;
        this.typeCreate = 0;
        this.recorded = null;
        this.mutipleRowSelected = [];
        this.prepaidExpenseService.getPrepaidExpenses().subscribe((res: HttpResponse<IPrepaidExpense[]>) => {
            this.prepaidExpenses = res.body;
        });
        this.chiPhiTraTruocService.getPrepaidExpenseCode().subscribe(ref => {
            this.prepaidExpenseCodeList = ref.body;
        });
        // this.accountingObjectService.getAllDTO().subscribe((res: HttpResponse<IAccountingObject[]>) => {
        //     this.accountingObject = res.body.sort((a, b) => a.accountingObjectCode.localeCompare(b.accountingObjectCode));
        // });
        // this.statisticsCodeService.getAllStatisticsCodes().subscribe((res: HttpResponse<IStatisticsCode[]>) => {
        //     this.statisticsCodes = res.body;
        // });
        // this.bankAccountDetailsService.getBankAccountDetails().subscribe((res: HttpResponse<IBankAccountDetails[]>) => {
        //     this.bankAccountDetails = res.body;
        // });
        // this.costSetService.getAllCostSets().subscribe((res: HttpResponse<ICostSet[]>) => {
        //     this.costSets = res.body;
        // });
        // this.expenseItemsService.getExpenseItems().subscribe((res: HttpResponse<IExpenseItem[]>) => {
        //     this.expenseItems = res.body;
        // });
        // this.eMContractService.getEMContracts().subscribe((res: HttpResponse<IEMContract[]>) => {
        //     this.eMContracts = res.body;
        // });
        // this.organizationUnitService.getOrganizationUnits().subscribe((res: HttpResponse<IOrganizationUnit[]>) => {
        //     this.organizationUnits = res.body;
        // });
        // this.budgetItemService.getBudgetItems().subscribe((res: HttpResponse<IBudgetItem[]>) => {
        //     this.budgetItem = res.body;
        // });
        this.goodsServicePurchaseService.getGoodServicePurchases().subscribe((res: HttpResponse<IGoodsServicePurchase[]>) => {
            this.goodsServicePurchases = res.body;
        });
        this.pSSalarySheetService.getPSSalarySheets().subscribe((res: HttpResponse<IPSSalarySheet[]>) => {
            // this.listLastPSSalarySheet = res.body.filter(a => a.typeID === this.TYPE_CHAM_CONG_THEO_NGAY);
            this.pSSalarySheetList = res.body.filter(a => !a.gOtherVoucherID && a.typeID !== 774);
        });
        this.principal.identity().then(account => {
            this.currentAccount = account;
            this.isUseToolIn = this.utilsService.checkUseToolIn(account);
            const dateSearch = this.utilsService.getDataYearWork(this.currentAccount);
            if (dateSearch.fromDate !== '') {
                this.objSearch.fromDate = dateSearch.fromDate.format(DATE_FORMAT);
            }
            if (dateSearch.toDate !== '') {
                this.objSearch.toDate = dateSearch.toDate.format(DATE_FORMAT);
            }
            if (account) {
                this.isRolePhanBoChiPhiTraTruoc = this.currentAccount.authorities.includes(this.ROLE_PHAN_BO_CHI_PHI_TRA_TRUOC);
                this.isRoleKetChuyenLaiLo = this.currentAccount.authorities.includes(this.ROLE_KET_CHUYEN_LAI_LO);
                if (account.organizationUnit.taxCalculationMethod === 0) {
                    this.isShowGoodsServicePurchase = true;
                } else {
                    this.isShowGoodsServicePurchase = false;
                }
                this.isSoTaiChinh = this.currentAccount.systemOption.some(x => x.code === SO_LAM_VIEC && x.data === '0');
                this.color = this.currentAccount.systemOption.find(x => x.code === TCKHAC_MauCTuChuaGS && x.data).data;
            }
            this.currencyService.findAllActive().subscribe(res => {
                this.currencies = res.body.sort((a, b) => a.currencyCode.localeCompare(b.currencyCode));
                this.currency = this.currencies.find(cur => cur.currencyCode === this.currentAccount.organizationUnit.currencyID);
            });
            this.loadAll();
        });
        this.isKeyPressed = false;
        this.registerExport();
        this.registerLockSuccess();
        this.registerUnlockSuccess();
        this.registerChangeInHachToanChiPhiLuongs();
    }

    //
    // ngOnDestroy() {
    //     this.eventManager.destroy(this.eventSubscriber);
    // }

    trackId(index: number, item: IGOtherVoucher) {
        return item.id;
    }

    registerChangeInHachToanChiPhiLuongs() {
        this.eventSubscriber = this.eventManager.subscribe('hachToanChiPhiLuongListModification', response => {
            this.selectedRows = [];
            this.searchAfterChangeRecord();
        });
        this.eventSubscribers.push(this.eventSubscriber);
    }

    sort() {
        const result = ['date' + ',' + (this.reverse ? 'desc' : 'asc')];
        // result.push('postedDate' + ',' + (this.reverse ? 'asc' : 'desc'));
        result.push('postedDate' + ',' + (this.reverse ? 'desc' : 'asc'));
        if (this.isSoTaiChinh) {
            result.push('noFBook' + ',' + (this.reverse ? 'desc' : 'asc'));
        } else {
            result.push('noMBook' + ',' + (this.reverse ? 'desc' : 'asc'));
        }
        // const result = ['date, desc', 'postedDate, desc'];
        return result;
    }

    private paginateGOtherVouchers(data: IGOtherVoucher[], headers: HttpHeaders) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = parseInt(headers.get('X-Total-Count'), 10);
        this.queryCount = this.totalItems;
        this.gOtherVouchers = data;
        this.objects = data;
        if (data && data.length === 0 && this.page > 1) {
            this.page = this.page - 1;
            this.loadAll();
            // this.router.navigate(['/g-other-voucher'], {
            //     queryParams: {
            //         page: this.page,
            //         size: this.itemsPerPage,
            //         sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            //     }
            // });
        }
        //  load first element
        if (this.objects && this.objects.length > 0) {
            if (this.dataSession && this.dataSession.rowNum) {
                this.selectedRows.push(this.gOtherVouchers[this.dataSession.rowNum]);
                this.selectedRow = this.gOtherVouchers[this.dataSession.rowNum];
            } else {
                this.selectedRows.push(this.gOtherVouchers[0]);
                this.selectedRow = this.gOtherVouchers[0];
            }
        } else {
            this.selectedRow = null;
            this.gOtherVoucherDetails = null;
            this.viewVouchers = null;
        }
        this.onSelect(this.selectedRow);
        this.checkMultiButton(false);
        //  display pageCount
        sessionStorage.removeItem('dataSearchGOtherVoucher');
        this.pageCount = Math.ceil(this.totalItems / this.itemsPerPage);
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    getRowNumberOfRecord(page: number, index: number): number {
        if (page > 0 && index !== -1) {
            return (page - 1) * this.itemsPerPage + index + 1;
        }
    }

    selectedItemPerPage() {
        this.loadAll();
    }

    onSelect(select: IGOtherVoucher) {
        event.preventDefault();
        this.isSingleClick = true;
        setTimeout(() => {
            if (this.isSingleClick) {
                this.selectedRow = select;
                if (this.selectedRow) {
                    this.index = this.gOtherVouchers.indexOf(this.selectedRow);
                    this.rowNum = this.getRowNumberOfRecord(this.page, this.index);
                    switch (this.selectedRow.typeID) {
                        case this.TYPE_HACH_TOAN_CHI_PHI_LUONG:
                            this.gOtherVoucherService.find(this.selectedRow.id).subscribe((res: HttpResponse<IGOtherVoucher>) => {
                                this.gOtherVoucherDetails =
                                    res.body.gOtherVoucherDetails === undefined
                                        ? []
                                        : res.body.gOtherVoucherDetails.sort((a, b) => a.orderPriority - b.orderPriority);
                                this.viewVouchers = res.body.viewVouchers;
                                this.lstDataForSetDefault = [];
                                this.lstDataForSetDefault.push(this.gOtherVoucherDetails);
                                this.loadDataDefaultForOpenVoucher(this.lstDataForSetDefault);
                            });
                            break;
                    }
                }
            }
        }, 250);
    }

    @ebAuth(['ROLE_ADMIN', ROLE.HachToanChiPhiLuong_KetXuat])
    export() {
        event.preventDefault();
        if (this.gOtherVouchers && this.gOtherVouchers.length > 0) {
            this.gOtherVoucherService
                .exportPDF({
                    searchVoucher: JSON.stringify(this.saveSearchVoucher())
                })
                .subscribe(res => {
                    this.refModalService.open(null, EbReportPdfPopupComponent, res, false, this.TYPE_HACH_TOAN_CHI_PHI_LUONG);
                });
        }
    }

    @ebAuth(['ROLE_ADMIN', ROLE.HachToanChiPhiLuong_GhiSo])
    record() {
        event.preventDefault();
        if (this.selectedRows.length > 1) {
            this.typeMultiAction = 2;
            this.modalRef = this.modalService.open(this.popUpMultiDelete, { backdrop: 'static' });
        } else {
            if (this.selectedRow && !this.selectedRow.recorded && !this.checkCloseBook(this.currentAccount, this.selectedRow.postedDate)) {
                this.dataSession.rowNum = this.gOtherVouchers.indexOf(this.selectedRow);
                this.isUnRecorded = true;
                this.isRecorded = false;
                this._record();
            }
        }
    }

    @ebAuth(['ROLE_ADMIN', ROLE.HachToanChiPhiLuong_GhiSo])
    unrecord() {
        event.preventDefault();
        if (this.selectedRows.length > 1) {
            this.typeMultiAction = 1;
            this.modalRef = this.modalService.open(this.popUpMultiDelete, { backdrop: 'static' });
        } else {
            if (this.selectedRow && this.selectedRow.recorded && !this.checkCloseBook(this.currentAccount, this.selectedRow.postedDate)) {
                this.dataSession.rowNum = this.gOtherVouchers.indexOf(this.selectedRow);
                this.isRecorded = true;
                this.isUnRecorded = false;
                this._record();
            }
        }
    }

    _record() {
        this.record_ = {};
        this.record_.id = this.selectedRow.id;
        this.record_.typeID = this.selectedRow.typeID;
        if (this.selectedRow.recorded !== true) {
            this.gLService.record(this.record_).subscribe((res: HttpResponse<Irecord>) => {
                if (res.body.success) {
                    this.selectedRow.recorded = true;
                    this.toastr.success(
                        this.translate.instant('ebwebApp.mBDeposit.recordSuccess'),
                        this.translate.instant('ebwebApp.mBDeposit.message')
                    );
                } else if (res.body.msg === MSGERROR.XUAT_QUA_TON_QUY_QT) {
                    this.toastr.error(
                        this.translate.instant('global.messages.error.checkTonQuyQT'),
                        this.translate.instant('ebwebApp.mCReceipt.error.error')
                    );
                } else if (res.body.msg === MSGERROR.XUAT_QUA_TON_QUY_TC) {
                    this.toastr.error(
                        this.translate.instant('global.messages.error.checkTonQuyTC'),
                        this.translate.instant('ebwebApp.mCReceipt.error.error')
                    );
                } else if (res.body.msg === MSGERROR.XUAT_QUA_TON_QUY) {
                    this.toastr.error(
                        this.translate.instant('global.messages.error.checkTonQuy'),
                        this.translate.instant('ebwebApp.mCReceipt.error.error')
                    );
                }
                this.searchAfterChangeRecord();
            });
            if (this.statusRecordAfterFind === null || this.statusRecordAfterFind === undefined) {
                this.isRecorded = false;
                this.isUnRecorded = true;
            } else {
                this.isRecorded = !this.statusRecordAfterFind;
            }
        } else if (this.selectedRow.recorded) {
            this.gLService.unrecord(this.record_).subscribe((res: HttpResponse<Irecord>) => {
                if (res.body.success) {
                    this.selectedRow.recorded = false;
                    this.toastr.success(
                        this.translate.instant('ebwebApp.mBDeposit.unrecordSuccess'),
                        this.translate.instant('ebwebApp.mBDeposit.message')
                    );
                    this.searchAfterChangeRecord();
                }
            });
            if (this.statusRecordAfterFind === null || this.statusRecordAfterFind === undefined) {
                this.isUnRecorded = true;
                this.isRecorded = false;
            } else {
                this.isUnRecorded = !this.statusRecordAfterFind;
            }
        } else {
            this.toastr.error('ebwebApp.mBDeposit.error', 'ebwebApp.mBDeposit.message');
        }
        this.selectedRows = [];
    }

    doubleClickRow(any?) {
        this.isSingleClick = false;
        this.edit();
    }

    @ebAuth(['ROLE_ADMIN', ROLE.HachToanChiPhiLuong_Sua])
    edit() {
        event.preventDefault();
        this.dataSession.page = this.page;
        if (this.selectedRow) {
            this.isShowSearch = false;
        }
        this.dataSession.itemsPerPage = this.itemsPerPage;
        this.dataSession.pageCount = this.pageCount;
        this.dataSession.totalItems = this.totalItems;
        this.dataSession.rowNum = this.gOtherVouchers.indexOf(this.selectedRow);
        this.dataSession.isShowSearch = this.isShowSearch;
        this.dataSession.searchVoucher = JSON.stringify(this.saveSearchVoucher());
        this.dataSession.predicate = this.predicate;
        this.dataSession.accountingObjectName = this.accountingObjectName;
        this.dataSession.reverse = this.reverse;
        sessionStorage.setItem('dataSession', JSON.stringify(this.dataSession));
        if (this.isNavigateForm) {
            switch (this.selectedRow.typeID) {
                case this.TYPE_HACH_TOAN_CHI_PHI_LUONG:
                    this.router.navigate(['./hach-toan-chi-phi-luong', this.selectedRow.id, 'edit']);
                    break;
            }
        }
    }

    getEMContractsbyID(id) {
        if (this.eMContracts) {
            const eMC = this.eMContracts.find(n => n.id === id);
            if (eMC) {
                return eMC.noMBook;
            }
        }
    }

    getAccountingObjectbyID(id) {
        if (this.accountingObjects) {
            const acc = this.accountingObjects.find(n => n.id === id);
            if (acc) {
                return acc.accountingObjectCode;
            }
        }
    }

    getBankAccountDetailbyID(id) {
        if (this.bankAccountDetails) {
            const ibank = this.bankAccountDetails.find(n => n.id === id);
            if (ibank) {
                return ibank.bankAccount;
            }
        }
    }

    getEmployeeByID(id) {
        if (this.employees) {
            const acc = this.employees.find(n => n.id === id);
            if (acc) {
                return acc.accountingObjectCode;
            }
        }
    }

    getStatisticsCodebyID(id) {
        if (this.statisticCodes) {
            const statisticsCode = this.statisticCodes.find(n => n.id === id);
            if (statisticsCode) {
                return statisticsCode.statisticsCode;
            }
        }
    }

    getCostSetbyID(id) {
        if (this.costSets) {
            const costSet = this.costSets.find(n => n.id === id);
            if (costSet) {
                return costSet.costSetCode;
            }
        }
    }

    getExpenseItembyID(id) {
        if (this.expenseItems) {
            const expenseItem = this.expenseItems.find(n => n.id === id);
            if (expenseItem) {
                return expenseItem.expenseItemCode;
            }
        }
    }

    getOrganizationUnitbyID(id) {
        if (this.organizationUnits) {
            const organizationUnit = this.organizationUnits.find(n => n.id === id);
            if (organizationUnit) {
                return organizationUnit.organizationUnitCode;
            }
        }
    }

    getBudgetItembyID(id) {
        if (this.budgetItems) {
            const iBudgetItem = this.budgetItems.find(n => n.id === id);
            if (iBudgetItem) {
                return iBudgetItem.budgetItemCode;
            }
        }
    }

    registerExport() {
        this.eventSubscriber = this.eventManager.subscribe(`export-excel-${this.TYPE_HACH_TOAN_CHI_PHI_LUONG}`, () => {
            this.exportExcel();
        });
        this.eventSubscribers.push(this.eventSubscriber);
    }

    exportExcel() {
        this.gOtherVoucherService.exportExcel({ searchVoucher: JSON.stringify(this.saveSearchVoucher()) }).subscribe(
            res => {
                const blob = new Blob([res.body], { type: 'application/vnd.ms-excel' });
                const fileURL = URL.createObjectURL(blob);

                const link = document.createElement('a');
                document.body.appendChild(link);
                link.download = fileURL;
                link.setAttribute('style', 'display: none');
                const name = 'DS_HachToanChiPhiLuong.xls';
                link.setAttribute('download', name);
                link.href = fileURL;
                link.click();
            },
            () => {}
        );
    }

    newArr(lenght: number): any[] {
        if (lenght > 0) {
            return new Array(lenght);
        } else {
            return new Array(0);
        }
    }

    @ebAuth(['ROLE_ADMIN', ROLE.HachToanChiPhiLuong_Them])
    addNew(isNew = false) {
        event.preventDefault();
        if (this.modalRef) {
            this.modalRef.close();
        }
        this.pSSalarySheetService.getPSSalarySheets().subscribe((res: HttpResponse<IPSSalarySheet[]>) => {
            this.pSSalarySheetList = res.body.filter(a => !a.gOtherVoucherID && a.typeID !== 774);
        });
        this.inPopup = true;
        this.modalRef = this.modalService.open(this.popUpAddNew, { backdrop: 'static' });
        setTimeout(() => {
            document.getElementById('index1').focus();
        }, 0);
    }

    showPopupAddNew() {
        event.preventDefault();
        if (!this.pSSalarySheetID) {
            this.toastr.error(
                this.translate.instant('ebwebApp.gOtherVoucher.pSSalarySheetIDNotBeBlank'),
                this.translate.instant('ebwebApp.mBDeposit.message')
            );
            return;
        }
        if (this.modalRef) {
            this.modalRef.close();
        }
        const pSSalarySheet = this.pSSalarySheetList.find(a => a.id === this.pSSalarySheetID);
        if (pSSalarySheet) {
            this.sessionPSTimeSheet.month = pSSalarySheet.month;
            this.sessionPSTimeSheet.year = pSSalarySheet.year;
            this.sessionPSTimeSheet.lastPSSalarySheetID = this.pSSalarySheetID;
            this.sessionPSTimeSheet.typeCreate = this.typeCreate;
            sessionStorage.setItem('sessionHachToanChiPhiLuong', JSON.stringify(this.sessionPSTimeSheet));
        }
        // sessionStorage.setItem('dataSession', JSON.stringify(this.dataSession));
        this.router.navigate(['hach-toan-chi-phi-luong/', 'new']);
    }

    isForeignCurrency() {
        if (this.selectedRow) {
            return this.currentAccount && this.selectedRow.currencyID !== this.currentAccount.organizationUnit.currencyID;
        }
    }

    getAmountOriginalType() {
        if (this.isForeignCurrency()) {
            return 8;
        }
        return 7;
    }

    getSumDetails(prop) {
        let sum = 0;
        if (this.gOtherVoucherDetails) {
            for (let i = 0; i < this.gOtherVoucherDetails.length; i++) {
                sum += this.gOtherVoucherDetails[i][prop];
            }
        }
        return sum;
    }

    getVATRate(vAT) {
        if (vAT === 0) {
            return '0%';
        } else if (vAT === 1) {
            return '5%';
        } else if (vAT === 2) {
            return '10%';
        } else if (vAT === -1) {
            return 'Không chịu thuế';
        } else if (vAT === -2) {
            return 'Không tính thuế';
        } else {
            return '';
        }
    }

    getPrepaidExpenseCode(id) {
        if (id && this.prepaidExpenses) {
            const prepaidExpense = this.prepaidExpenses.find(a => a.id === id);
            if (prepaidExpense) {
                return prepaidExpense.prepaidExpenseCode;
            }
        } else {
            return '';
        }
    }

    getPrepaidExpenseName(id) {
        if (id && this.prepaidExpenses) {
            const prepaidExpense = this.prepaidExpenses.find(a => a.id === id);
            if (prepaidExpense) {
                return prepaidExpense.prepaidExpenseName;
            }
        } else {
            return '';
        }
    }

    getObjectCode(id) {
        if (id && this.prepaidExpenseCodeList) {
            const prepaidExpense = this.prepaidExpenseCodeList.find(a => a.id === id);
            if (prepaidExpense) {
                return prepaidExpense.code;
            }
        } else {
            return '';
        }
    }

    registerLockSuccess() {
        this.eventSubscriber = this.eventManager.subscribe('lockSuccess', response => {
            this.principal.identity().then(account => {
                this.currentAccount = account;
            });
            this.loadAll();
        });
        this.eventSubscribers.push(this.eventSubscriber);
    }

    registerUnlockSuccess() {
        this.eventSubscriber = this.eventManager.subscribe('unlockSuccess', response => {
            this.principal.identity().then(account => {
                this.currentAccount = account;
            });
            this.loadAll();
        });
        this.eventSubscribers.push(this.eventSubscriber);
    }

    checkMultiButton(isRecord: boolean) {
        if (this.selectedRows.length > 0) {
            for (let i = 0; i < this.selectedRows.length; i++) {
                if (this.selectedRows && this.selectedRows[i] && this.selectedRows[i].recorded === isRecord) {
                    return true;
                }
            }
        }
        return false;
    }

    delete() {
        event.preventDefault();
        if (this.selectedRows.length > 1 && !this.checkCloseBook(this.currentAccount, this.selectedRow.postedDate)) {
            this.typeMultiAction = 0;
            this.modalRef = this.modalService.open(this.popUpMultiDelete, { backdrop: 'static' });
        } else {
            if (this.selectedRow && !this.selectedRow.recorded) {
                this.dataSession.rowNum = this.gOtherVouchers.indexOf(this.selectedRow);
                this.router.navigate(['/hach-toan-chi-phi-luong', { outlets: { popup: this.selectedRow.id + '/delete' } }]);
            }
        }
    }

    continueDelete() {
        if (this.typeMultiAction === 0) {
            this.gOtherVoucherService.multiDelete(this.selectedRows).subscribe(
                (res: HttpResponse<any>) => {
                    if (this.modalRef) {
                        this.modalRef.close();
                    }
                    this.selectedRows = [];
                    this.searchAfterChangeRecord();
                    if (res.body.countTotalVouchers !== res.body.countSuccessVouchers) {
                        this.modalRef = this.refModalService.open(
                            res.body,
                            HandlingResultComponent,
                            null,
                            false,
                            null,
                            null,
                            null,
                            null,
                            null,
                            true
                        );
                    } else {
                        this.toastr.success(this.translate.instant('ebwebApp.mBDeposit.deleteSuccessful'));
                    }
                    this.activeModal.close();
                    this.loadAll();
                    // }
                },
                (res: HttpErrorResponse) => {
                    if (res.error.errorKey === 'errorDeleteList') {
                        this.toastr.error(
                            this.translate.instant('ebwebApp.mBDeposit.errorDeleteVoucherNo'),
                            this.translate.instant('ebwebApp.mBDeposit.message')
                        );
                    }
                    if (this.modalRef) {
                        this.modalRef.close();
                    }
                }
            );
        } else if (this.typeMultiAction === 1) {
            this.gOtherVoucherService.multiUnRecord(this.selectedRows).subscribe(
                (res: HttpResponse<any>) => {
                    if (this.modalRef) {
                        this.modalRef.close();
                    }
                    this.selectedRows = [];
                    if (res.body.listFail && res.body.listFail.length > 0) {
                        this.modalRef = this.refModalService.open(
                            res.body,
                            HandlingResultComponent,
                            null,
                            false,
                            null,
                            null,
                            null,
                            null,
                            null,
                            true
                        );
                    } else {
                        this.toastr.success(
                            this.translate.instant('ebwebApp.mBCreditCard.unrecordSuccess'),
                            this.translate.instant('ebwebApp.mBDeposit.message')
                        );
                    }
                    this.activeModal.close();
                    this.loadAll();
                    // }
                },
                (res: HttpErrorResponse) => {
                    this.toastr.error(
                        this.translate.instant('global.data.unRecordFailed'),
                        this.translate.instant('ebwebApp.mBDeposit.message')
                    );
                    if (this.modalRef) {
                        this.modalRef.close();
                    }
                }
            );
        } else if (this.typeMultiAction === 2) {
            const listRecord: RequestRecordListDtoModel = {};
            listRecord.typeIDMain = TypeID.CHUNG_TU_NGHIEP_VU_KHAC;
            listRecord.records = [];
            this.selectedRows.forEach(item => {
                listRecord.records.push({
                    id: item.id,
                    typeID: item.typeID,
                    companyID: item.companyID
                });
            });
            this.gLService.recordList(listRecord).subscribe(
                (res: HttpResponse<any>) => {
                    if (this.modalRef) {
                        this.modalRef.close();
                    }
                    this.selectedRows = [];
                    if (res.body.listFail && res.body.listFail.length > 0) {
                        this.modalRef = this.refModalService.open(
                            res.body,
                            HandlingResultComponent,
                            null,
                            false,
                            null,
                            null,
                            null,
                            null,
                            null,
                            true
                        );
                    } else {
                        this.toastr.success(
                            this.translate.instant('ebwebApp.mBCreditCard.recordSuccess'),
                            this.translate.instant('ebwebApp.mBDeposit.message')
                        );
                    }
                    this.activeModal.close();
                    this.loadAll();
                    // }
                },
                (res: HttpErrorResponse) => {
                    this.toastr.error(
                        this.translate.instant('global.data.recordFailed'),
                        this.translate.instant('ebwebApp.mBDeposit.message')
                    );
                    if (this.modalRef) {
                        this.modalRef.close();
                    }
                }
            );
        }
    }

    closePopUpDelete() {
        if (this.modalRef) {
            this.modalRef.close();
        }
    }

    toggleSearch($event) {
        $event.preventDefault();
        this.isShowSearch = !this.isShowSearch;
        if (this.isShowSearch) {
            this.inPopup = true;
            this.isSearching = true;
            setTimeout(() => {
                document.getElementById('inputFirst').focus();
            }, 0);
        } else {
            this.inPopup = false;
            this.isSearching = false;
        }
    }

    search() {
        this.selectedRows = [];
        if (this.objSearch.fromDate > this.objSearch.toDate) {
            this.toastr.error(
                this.translate.instant('ebwebApp.mBDeposit.fromDateMustBeLessThanToDate'),
                this.translate.instant('ebwebApp.mBDeposit.message')
            );
            return;
        }
        this.statusRecordAfterFind = this.objSearch.statusRecorded;
        sessionStorage.setItem('dataSearchGOtherVoucher', JSON.stringify(this.saveSearchVoucher()));
        this.gOtherVoucherService
            .searchAll({
                page: this.page - 1,
                size: this.itemsPerPage,
                searchVoucher: JSON.stringify(this.saveSearchVoucher())
            })
            .subscribe(
                (res: HttpResponse<IGOtherVoucher[]>) => this.paginateGOtherVouchers(res.body, res.headers),
                (res: HttpErrorResponse) => this.onError(res.message)
            );
        if (this.objSearch.statusRecorded) {
            this.isRecorded = true;
        } else {
            this.isUnRecorded = true;
        }
        this.reSearchWhenRecord = JSON.stringify(this.saveSearchVoucher());
        this.page = 1;
        this.loadPage(this.page);
    }

    clearSearch() {
        this.selectedRows = [];
        this.objSearch = {};
        this.statusRecord = null;
        const dateSearch = this.utilsService.getDataYearWork(this.currentAccount);
        if (dateSearch.fromDate !== '') {
            this.objSearch.fromDate = dateSearch.fromDate.format(DATE_FORMAT);
        }
        if (dateSearch.toDate !== '') {
            this.objSearch.toDate = dateSearch.toDate.format(DATE_FORMAT);
        }
        this.gOtherVoucherService
            .searchAll({
                page: this.page - 1,
                size: this.itemsPerPage,
                sort: this.sort(),
                searchVoucher: JSON.stringify(this.saveSearchVoucher())
            })
            .subscribe(
                (res: HttpResponse<IGOtherVoucher[]>) => this.paginateGOtherVouchers(res.body, res.headers),
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    saveSearchVoucher(): ISearchVoucher {
        const searchObj: ISearchVoucher = {};
        searchObj.typeID = this.TYPE_HACH_TOAN_CHI_PHI_LUONG;
        searchObj.statusRecorded = this.objSearch.statusRecorded !== undefined ? this.objSearch.statusRecorded : null;
        searchObj.currencyID = this.objSearch.currencyID !== undefined ? this.objSearch.currencyID : null;
        searchObj.fromDate = this.objSearch.fromDate !== undefined ? this.objSearch.fromDate : null;
        searchObj.toDate = this.objSearch.toDate !== undefined ? this.objSearch.toDate : null;
        searchObj.accountingObjectID = this.objSearch.accountingObjectID !== undefined ? this.objSearch.accountingObjectID : null;
        searchObj.textSearch = this.objSearch.textSearch !== undefined ? this.objSearch.textSearch : null;
        return searchObj;
    }

    searchAfterChangeRecord() {
        if (this.reSearchWhenRecord) {
            this.gOtherVoucherService
                .searchAll({
                    page: this.page - 1,
                    size: this.itemsPerPage,
                    sort: this.sort(),
                    searchVoucher: JSON.stringify(this.saveSearchVoucher())
                })
                .subscribe(
                    (res: HttpResponse<IGOtherVoucher[]>) => this.paginateGOtherVouchers(res.body, res.headers),
                    (res: HttpErrorResponse) => this.onError(res.message)
                );
        }
    }

    closePopup() {
        this.inPopup = false;
    }

    checkSameOrg() {
        if (this.selectedRow) {
            if (this.selectedRows.length > 1) {
                return true;
            } else {
                return this.selectedRow.companyID === this.currentAccount.organizationUnit.id;
            }
        } else {
            return true;
        }
    }

    navigateHelp() {
        this.notificationService.findHelpLink({ id: 'HTCPL' }).subscribe((res: HttpResponse<any>) => {
            window.open(res.body, '_blank');
        });
    }

    exportPDFWebService(number: number) {
        const listID = this.selectedRows.map(x => x.id);
        this.inChungTuHangLoatService.inChungTuHangLoat({
            typeGroupID: TYPEGROUP.CHUNG_TU_NGHIEP_VU_KHAC,
            typeReport: number,
            listID,
            fromDate: null,
            toDate: null,
            paramCheckAll: false,
            isDesc: true
        });
    }
}
