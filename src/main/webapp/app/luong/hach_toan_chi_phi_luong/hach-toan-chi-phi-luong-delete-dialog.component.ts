import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { HachToanChiPhiLuongService } from './hach-toan-chi-phi-luong.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { BaseComponent } from 'app/shared/base-component/base.component';
import { IGOtherVoucher } from 'app/shared/models/g-other-voucher.model';

@Component({
    selector: 'eb-hach-toan-chi-phi-luong-delete-dialog',
    templateUrl: './hach-toan-chi-phi-luong-delete-dialog.component.html'
})
export class HachToanChiPhiLuongDeleteDialogComponent extends BaseComponent {
    gOtherVoucher: IGOtherVoucher;

    constructor(
        private gOtherVoucherService: HachToanChiPhiLuongService,
        public activeModal: NgbActiveModal,
        private router: Router,
        private eventManager: JhiEventManager,
        private toastr: ToastrService,
        private translate: TranslateService
    ) {
        super();
    }

    clear() {
        window.history.back();
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: string) {
        this.gOtherVoucherService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'hachToanChiPhiLuongListModification',
                content: 'Deleted an hachToanChiPhiLuong'
            });
            this.activeModal.dismiss(true);
        });
        this.toastr.success(
            this.translate.instant('ebwebApp.mBDeposit.deleteSuccessful'),
            this.translate.instant('ebwebApp.mBDeposit.message')
        );
        this.router.navigate(['hach-toan-chi-phi-luong']);
    }
}

@Component({
    selector: 'eb-hach-toan-chi-phi-luong-delete-popup',
    template: ''
})
export class HachToanChiPhiLuongDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ gOtherVoucher }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(HachToanChiPhiLuongDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.gOtherVoucher = gOtherVoucher;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], {
                            replaceUrl: true,
                            queryParamsHandling: 'merge'
                        });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], {
                            replaceUrl: true,
                            queryParamsHandling: 'merge'
                        });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
