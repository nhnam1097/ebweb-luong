import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { HachToanChiPhiLuongService } from './hach-toan-chi-phi-luong.service';
import { HachToanChiPhiLuongComponent } from './hach-toan-chi-phi-luong.component';
import { HachToanChiPhiLuongUpdateComponent } from './hach-toan-chi-phi-luong-update.component';
import { HachToanChiPhiLuongDeletePopupComponent } from './hach-toan-chi-phi-luong-delete-dialog.component';
import { ROLE } from 'app/role.constants';
import { GOtherVoucher, IGOtherVoucher } from 'app/shared/models/g-other-voucher.model';
import { CanDeactiveGuardService } from 'app/shared/can-deactive-guard/can-deactive-guard.service';

@Injectable({ providedIn: 'root' })
export class HachToanChiPhiLuongResolve implements Resolve<IGOtherVoucher> {
    constructor(private service: HachToanChiPhiLuongService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((gOtherVoucher: HttpResponse<GOtherVoucher>) => gOtherVoucher.body));
        }
        return of(new GOtherVoucher());
    }
}

export const hachToanChiPhiLuongRoute: Routes = [
    {
        path: '',
        component: HachToanChiPhiLuongComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER', ROLE.HachToanChiPhiLuong_XEM],
            defaultSort: 'id,asc',
            pageTitle: 'ebwebApp.gOtherVoucher.home.titleHachToanChiPhiLuong'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: HachToanChiPhiLuongUpdateComponent,
        resolve: {
            gOtherVoucher: HachToanChiPhiLuongResolve
        },
        data: {
            authorities: ['ROLE_USER', ROLE.HachToanChiPhiLuong_Them],
            pageTitle: 'ebwebApp.gOtherVoucher.home.titleHachToanChiPhiLuong'
        },
        canActivate: [UserRouteAccessService],
        canDeactivate: [CanDeactiveGuardService]
    },
    {
        path: 'hasSearch/:isSearch',
        component: HachToanChiPhiLuongComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER', ROLE.HachToanChiPhiLuong_XEM],
            defaultSort: 'id,asc',
            pageTitle: 'ebwebApp.gOtherVoucher.home.titleHachToanChiPhiLuong'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit/:rowNum',
        component: HachToanChiPhiLuongUpdateComponent,
        resolve: {
            gOtherVoucher: HachToanChiPhiLuongResolve
        },
        data: {
            authorities: ['ROLE_USER', ROLE.HachToanChiPhiLuong_Sua],
            pageTitle: 'ebwebApp.gOtherVoucher.home.titleHachToanChiPhiLuong'
        },
        canActivate: [UserRouteAccessService],
        canDeactivate: [CanDeactiveGuardService]
    },
    {
        path: ':id/edit',
        component: HachToanChiPhiLuongUpdateComponent,
        resolve: {
            gOtherVoucher: HachToanChiPhiLuongResolve
        },
        data: {
            authorities: ['ROLE_USER', ROLE.HachToanChiPhiLuong_Sua],
            pageTitle: 'ebwebApp.gOtherVoucher.home.titleHachToanChiPhiLuong'
        },
        canActivate: [UserRouteAccessService],
        canDeactivate: [CanDeactiveGuardService]
    },
    {
        path: ':id/edit/from-ref',
        component: HachToanChiPhiLuongUpdateComponent,
        resolve: {
            gOtherVoucher: HachToanChiPhiLuongResolve
        },
        data: {
            authorities: ['ROLE_USER', ROLE.HachToanChiPhiLuong_Sua],
            pageTitle: 'ebwebApp.gOtherVoucher.home.titleHachToanChiPhiLuong'
        },
        canActivate: [UserRouteAccessService],
        canDeactivate: [CanDeactiveGuardService]
    },
    {
        path: ':id/edit/from-g-other-voucher',
        component: HachToanChiPhiLuongUpdateComponent,
        resolve: {
            mCPayment: HachToanChiPhiLuongResolve
        },
        data: {
            authorities: ['ROLE_USER', ROLE.HachToanChiPhiLuong_Sua],
            pageTitle: 'ebwebApp.gOtherVoucher.home.titleHachToanChiPhiLuong'
        },
        canActivate: [UserRouteAccessService],
        canDeactivate: [CanDeactiveGuardService]
    }
];

export const hachToanChiPhiLuongPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: HachToanChiPhiLuongDeletePopupComponent,
        resolve: {
            gOtherVoucher: HachToanChiPhiLuongResolve
        },
        data: {
            authorities: ['ROLE_USER', ROLE.HachToanChiPhiLuong_Xoa],
            pageTitle: 'ebwebApp.gOtherVoucher.home.titleHachToanChiPhiLuong'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
