import { AfterViewChecked, AfterViewInit, Component, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Observable, Subscription } from 'rxjs';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { NgbModal, NgbModalRef, NgbTabChangeEvent } from '@ng-bootstrap/ng-bootstrap';
import { AccountType, DDSo_TienVND, MSGERROR, SO_LAM_VIEC, TCKHAC_GhiSo, TCKHAC_SDSoQuanTri, TYPEGROUP } from 'app/app.constants';
import { Principal } from 'app/core';
import { BaseComponent } from 'app/shared/base-component/base.component';
import * as moment from 'moment';
import { ROLE } from 'app/role.constants';
import { ebAuth } from 'app/shared/decorator/ebAuth.decorator';
import { DATE_FORMAT } from 'app/shared';
import { CurrencyMaskModel } from 'app/shared/directive/ng2-currency-mask/currency-mask.model';
import { CurrencyMaskConfig } from 'app/shared/directive/ng2-currency-mask/currency-mask.config';
import { IGOtherVoucher } from 'app/shared/models/g-other-voucher.model';
import { Currency, ICurrency } from 'app/shared/models/currency.model';
import { IGOtherVoucherDetails } from 'app/shared/models/g-other-voucher-details.model';
import { IAccountingObjectBankAccount } from 'app/shared/models/accounting-object-bank-account.model';
import { IAccountingObject } from 'app/shared/models/accounting-object.model';
import { IDataSessionStorage } from 'app/shared/models/DataSessionStorage';
import { ContextMenu } from 'app/shared/models/context-menu.model';
import { IGoodsServicePurchase } from 'app/shared/models/goods-service-purchase.model';
import { ISessionPSTimeSheet } from 'app/shared/models/session-p-s-time-sheet';
import { AccountingObjectService } from 'app/shared/services/accounting-object.service';
import { UtilsService } from 'app/shared/UtilsService/Utils.service';
import { GeneralLedgerService } from 'app/shared/services/general-ledger.service';
import { NotificationService } from 'app/shared/notification';
import { InChungTuHangLoatService } from 'app/shared/in-chung-tu-hang-loat/in-chung-tu-hang-loat.service';
import { GoodsServicePurchaseService } from 'app/shared/services/goods-service-purchase.service';
import { RefModalService } from 'app/shared/services/ref-modal.service';
import { CurrencyService } from 'app/shared/services/currency.service';
import { Irecord } from 'app/shared/models/record';
import { IAccountList } from 'app/shared/models/account-list.model';
import { IAutoPrinciple } from 'app/shared/models/auto-principle.model';
import { IBank } from 'app/shared/models/bank.model';
import { GOtherVoucherService } from 'app/shared/services/chung-tu-nghiep-vu-khac.service';
import { BankService } from 'app/shared/services/bank.service';
import { AccountListService } from 'app/shared/services/account-list.service';
import { IAccountAllList } from 'app/shared/models/account-all-list.model';
import { AutoPrincipleService } from 'app/shared/services/auto-principle.service';

@Component({
    selector: 'eb-g-other-voucher-update',
    templateUrl: './hach-toan-chi-phi-luong-update.component.html',
    styleUrls: ['./hach-toan-chi-phi-luong-update.component.css']
})
export class HachToanChiPhiLuongUpdateComponent extends BaseComponent implements OnInit, OnDestroy, AfterViewInit, AfterViewChecked {
    @ViewChild('test') public modalComponent: NgbModalRef;
    @ViewChild('content') content: TemplateRef<any>;
    @ViewChild('contentAmount') public modalComponentAmount: NgbModalRef;
    private _gOtherVoucher: IGOtherVoucher;
    isSaving: boolean;
    dateDp: any;
    postedDateDp: any;
    accountDefault: { value?: string };
    currencies: ICurrency[];
    gOtherVoucherDetails: IGOtherVoucherDetails[];
    // eMContracts: IEMContract[];
    isRecord: Boolean;
    totalAmount: number;
    totalAmountOriginal: number;
    temp: Number;
    totalVatAmount: any;
    totalVatAmountOriginal: any;
    banks: IBank[];
    accountList: IAccountList[];
    autoPrinciple: IAutoPrinciple[];
    accountingObjectBankAccount: IAccountingObjectBankAccount[];
    employeeName: string;
    autoPrincipleName: string;
    items: any;
    id: string;
    i: number;
    recorded: boolean;
    //  data storage provider
    page: number;
    itemsPerPage: number;
    pageCount: number;
    totalItems: number;
    rowNum: number;
    // sort
    predicate: any;
    accountingObjectName: any;
    reverse: any;
    record_: any;
    isCreateUrl: boolean;
    isEditUrl: boolean;
    disableAddButton: boolean;
    disableEditButton: boolean;
    accounting: IAccountingObject[];
    dataSession: IDataSessionStorage;
    creditCardType: string;
    ownerCard: string;
    warningMessage: any;
    viewVouchersSelected: any;
    contextMenu: ContextMenu;
    select: number;
    eventSubscriber: Subscription;
    modalRef: NgbModalRef;
    currentAccount: any;
    isSoTaiChinh: boolean;
    currency: Currency;
    ngayHachToan: any;
    sysRecord: any;
    sysTypeLedger: any;
    options: any;
    isHideTypeLedger: Boolean;
    TCKHAC_SDSoQuanTri: any;
    creditAccountList: IAccountList[];
    creditAccountListItem: IAccountList;
    debitAccountList: IAccountList[];
    debitAccountItem: IAccountList;
    vatAccountList: IAccountList[];
    iAutoPrinciple: IAutoPrinciple;
    gOtherVoucherCopy: IGOtherVoucher;
    gOtherVoucherDetailsCopy: IGOtherVoucherDetails[];
    statusVoucher: number;
    viewVouchersSelectedCopy: any;
    no: any;
    goodsServicePurchases: IGoodsServicePurchase[];
    goodsServicePurchaseID: any;
    isReadOnly: boolean;
    CustomCurrencyMaskConfigTienViet: CurrencyMaskConfig = {
        align: 'right',
        allowNegative: false,
        decimal: ',',
        precision: 2,
        prefix: '',
        suffix: '',
        thousands: '.',
        negative: '-',
        autocomplete: 'off'
    };
    columnList = [
        { column: AccountType.TK_CO, ppType: false },
        { column: AccountType.TK_NO, ppType: false },
        { column: AccountType.TK_THUE_GTGT, ppType: false },
        { column: AccountType.TKDU_THUE_GTGT, ppType: false }
    ];
    sessionPSTimeSheet: ISessionPSTimeSheet = new class implements ISessionPSTimeSheet {
        month: number;
        year: number;
        pSSalarySheetName?: string;
        typeCreate?: number;
        lastPSTimeSheetID?: string;
        lastPSTimeSheetSummaryID?: string;
        lastPSSalarySheetID?: string;
        autoAddNewEmployee?: boolean;
        getEmployeeNotActive?: boolean;
        listDepartmentID?: any[];
    }();
    currentRow: number;
    isRefVoucher: boolean;
    TYPE_HACH_TOAN_CHI_PHI_LUONG = 708;
    TYPE_CHUNG_TU_NGHIEP_VU_KHAC = 700;
    TYPE_KET_CHUYEN_LAI_LO = 702;
    TYPE_PHAN_BO_CHI_PHI_TRA_TRUOC = 709;
    TYPE_NGHIEM_THU_CONG_TRINH_DON_HANG_HOP_DONG = 703;
    count: number;
    listVAT: any[];
    isShowGoodsServicePurchase: boolean;

    ROLE_HTCPL_XEM = ROLE.HachToanChiPhiLuong_XEM;
    ROLE_HTCPL_THEM = ROLE.HachToanChiPhiLuong_Them;
    ROLE_HTCPL_SUA = ROLE.HachToanChiPhiLuong_Sua;
    ROLE_HTCPL_XOA = ROLE.HachToanChiPhiLuong_Xoa;
    ROLE_HTCPL_GHISO = ROLE.HachToanChiPhiLuong_GhiSo;
    ROLE_HTCPL_IN = ROLE.HachToanChiPhiLuong_In;
    ROLE_HTCPL_KETXUAT = ROLE.HachToanChiPhiLuong_KetXuat;

    buttonDeleteTranslate = 'ebwebApp.mBDeposit.toolTip.delete';
    buttonAddTranslate = 'ebwebApp.mBDeposit.toolTip.add';
    buttonEditTranslate = 'ebwebApp.mBDeposit.toolTip.edit';
    buttonSaveTranslate = 'ebwebApp.mBDeposit.toolTip.save';
    buttonRecordTranslate = 'ebwebApp.mBDeposit.toolTip.record';
    buttonUnRecordTranslate = 'ebwebApp.mBDeposit.toolTip.unrecord';
    buttonPrintTranslate = 'ebwebApp.mBDeposit.toolTip.print';
    buttonSaveAndNewTranslate = 'ebwebApp.mBDeposit.toolTip.saveAndNew';
    buttonCopyAndNewTranslate = 'ebwebApp.mBDeposit.toolTip.copyAndNew';
    buttonCloseFormTranslate = 'ebwebApp.mBDeposit.toolTip.closeForm';
    private tabShow: number;
    customCurrencyMaskConfig: CurrencyMaskModel;
    searchVoucher: string;
    isUseToolIn: boolean;

    constructor(
        private router: Router,
        private gOtherVoucherService: GOtherVoucherService,
        private activatedRoute: ActivatedRoute,
        private accService: AccountingObjectService,
        public utilService: UtilsService,
        private jhiAlertService: JhiAlertService,
        private gLService: GeneralLedgerService,
        private currencyService: CurrencyService,
        private bankService: BankService,
        private accountListService: AccountListService,
        private autoPrincipleService: AutoPrincipleService,
        private translate: TranslateService,
        private toastr: ToastrService,
        private eventManager: JhiEventManager,
        private refModalService: RefModalService,
        private principal: Principal,
        private modalService: NgbModal,
        private goodsServicePurchaseService: GoodsServicePurchaseService,
        public utilsService: UtilsService,
        private notificationService: NotificationService,
        private inChungTuHangLoatService: InChungTuHangLoatService
    ) {
        super();
        this.dataSession = JSON.parse(sessionStorage.getItem('dataSession'));
        this.searchVoucher = this.dataSession ? this.dataSession.searchVoucher : null;
        if (this.dataSession) {
            this.page = this.dataSession.page;
            this.itemsPerPage = this.dataSession.itemsPerPage;
            this.pageCount = this.dataSession.pageCount;
            this.predicate = this.dataSession.predicate;
            this.reverse = this.dataSession.reverse;
        } else {
            this.dataSession = null;
        }
        this.contextMenu = new ContextMenu();
        this.principal.identity().then(account => {
            this.currentAccount = account;
            this.isUseToolIn = this.utilsService.checkUseToolIn(account);
            this.CustomCurrencyMaskConfigTienViet = this.utilsService.getConfigCurrencyWithAcc(account, DDSo_TienVND, false);
            this.customCurrencyMaskConfig = this.utilsService.getConfigCurrencyAll(account, false);
            if (account.organizationUnit.taxCalculationMethod === 0) {
                this.isShowGoodsServicePurchase = true;
                this.goodsServicePurchaseID = this.currentAccount.organizationUnit.goodsServicePurchaseID;
            } else {
                this.isShowGoodsServicePurchase = false;
            }
            this.sysRecord = this.currentAccount.systemOption.find(x => x.code === TCKHAC_GhiSo && x.data);
            this.sysTypeLedger = this.currentAccount.systemOption.find(x => x.code === SO_LAM_VIEC).data;
            this.TCKHAC_SDSoQuanTri = this.currentAccount.systemOption.find(x => x.code === TCKHAC_SDSoQuanTri).data;
            this.currencyService.findAllActive().subscribe(res => {
                this.currencies = res.body;
                this.currency = this.currencies.find(cur => cur.currencyCode === this.currentAccount.organizationUnit.currencyID);
            });
            this.accountListService
                .getAccountTypeThird({
                    typeID: 700,
                    columnName: JSON.stringify(this.columnList)
                })
                .subscribe(res => {
                    const dataAccount: IAccountAllList = res.body;
                    this.creditAccountList = dataAccount.creditAccount;
                    this.debitAccountList = dataAccount.debitAccount;
                    this.vatAccountList = dataAccount.vatAccount;
                });
            this.accService.getAllDTO().subscribe((res: HttpResponse<IAccountingObject[]>) => {
                this.accounting = res.body;
                this.accountingObjects = res.body
                    .filter(n => n.isActive)
                    .sort((a, b) => a.accountingObjectCode.localeCompare(b.accountingObjectCode));
                this.employees = res.body.filter(e => e.isEmployee && e.isActive);
            });
            this.isEnter = this.currentAccount.systemOption.filter(n => n.code === 'TCKHAC_Enter')[0].data === '1';
        });
    }

    ngOnInit() {
        this.isSaving = false;
        this.tabShow = 1;
        this.viewVouchersSelected = [];
        this.listVAT = [
            { name: '0%', data: 0 },
            { name: '5%', data: 1 },
            { name: '10%', data: 2 },
            { name: 'KCT', data: 3 },
            { name: 'KTT', data: 4 }
        ];
        this.isEditUrl = window.location.href.includes('/edit');
        this.isHideTypeLedger = false;
        this.gOtherVoucher = {};
        this.activatedRoute.data.subscribe(({ gOtherVoucher }) => {
            this.gOtherVoucher = gOtherVoucher;
            this.isSoTaiChinh = this.currentAccount.systemOption.some(x => x.code === SO_LAM_VIEC && x.data === '0');
            if (sessionStorage.getItem('sessionHachToanChiPhiLuong')) {
                if (!this.isEditUrl) {
                    this.sessionPSTimeSheet = JSON.parse(sessionStorage.getItem('sessionHachToanChiPhiLuong'));
                    sessionStorage.removeItem('sessionHachToanChiPhiLuong');
                } else {
                    this.copy();
                    this.router.navigate(['/hach-toan-chi-phi-luong']);
                }
                this.statusVoucher = 0;
                const lastDay = moment(new Date(this.sessionPSTimeSheet.year, this.sessionPSTimeSheet.month, 0));
                this.gOtherVoucher.date = lastDay;
                this.gOtherVoucher.postedDate = lastDay;
                if (this.isSoTaiChinh) {
                    this.gOtherVoucher.typeLedger = 0;
                } else {
                    this.gOtherVoucher.typeLedger = 1;
                }
                this.utilService
                    .getGenCodeVoucher({
                        typeGroupID: 70, // typeGroupID loại chứng từ
                        companyID: '', // ID công ty
                        branchID: '', // ID chi nhánh
                        displayOnBook: this.sysTypeLedger // 0 - sổ tài chính, 1 - sổ quản trị, 2-cả hai sổ
                    })
                    .subscribe((resGenCode: HttpResponse<string>) => {
                        // this.mCReceipt.noFBook = (resGenCode.body.toString());
                        this.no = resGenCode.body;
                        this.gOtherVoucherService
                            .getDetailsHTCPL({
                                pSSalarySheetID: this.sessionPSTimeSheet.lastPSSalarySheetID,
                                typeCreate: this.sessionPSTimeSheet.typeCreate
                            })
                            .subscribe((resGetDetails: HttpResponse<IGOtherVoucherDetails[]>) => {
                                this.gOtherVoucherDetails =
                                    resGetDetails && resGetDetails.body
                                        ? resGetDetails.body.sort((a, b) => a.orderPriority - b.orderPriority)
                                        : [];
                                for (let i = 0; i < this.gOtherVoucherDetails.length; i++) {
                                    this.gOtherVoucherDetails[i].amount = this.utilsService.round(
                                        this.gOtherVoucherDetails[i].amount,
                                        this.currentAccount.systemOption,
                                        7
                                    );
                                }
                                this.gOtherVoucherDetails.forEach((v, i) => {
                                    v.stt = i + 1;
                                });
                                this.lstDataForSetDefault = [];
                                this.lstDataForSetDefault.push(this.gOtherVoucherDetails);
                                this.loadDataDefaultForOpenVoucher(this.lstDataForSetDefault);
                                this.selectChangeTotalAmount();
                                // this.organizationUnitService.getOrganizationUnits().subscribe((res: HttpResponse<IOrganizationUnit[]>) => {
                                //     this.organizationUnits = res.body.sort((a, b) =>
                                //         a.organizationUnitCode.localeCompare(b.organizationUnitCode)
                                //     );
                                //     for (let i = 0; i < this.gOtherVoucherDetails.length; i++) {
                                //         const organizationUnit = this.organizationUnits.find(
                                //             a => a.id === this.gOtherVoucherDetails[i].departmentID
                                //         );
                                //         if (organizationUnit) {
                                //             this.gOtherVoucherDetails[i].lastOrganizationUnitCode = organizationUnit.organizationUnitCode;
                                //         }
                                //     }
                                // });
                            });
                        this.translate.get(['ebwebApp.gOtherVoucher.defaultReasonHTCPL']).subscribe(res2 => {
                            this.gOtherVoucher.reason =
                                res2['ebwebApp.gOtherVoucher.defaultReasonHTCPL'] +
                                ' tháng ' +
                                this.sessionPSTimeSheet.month +
                                ' năm ' +
                                this.sessionPSTimeSheet.year;
                        });
                        this.copy();
                    });
                this.isReadOnly = false;
            } else {
                if (!this.gOtherVoucher.id) {
                    this.router.navigate(['/hach-toan-chi-phi-luong']);
                } else {
                    this.utilsService
                        .getIndexRow({
                            id: this.gOtherVoucher.id,
                            isNext: true,
                            typeID: this.TYPE_HACH_TOAN_CHI_PHI_LUONG,
                            searchVoucher: this.searchVoucher === undefined ? null : this.searchVoucher
                        })
                        .subscribe(
                            (res: HttpResponse<any[]>) => {
                                this.rowNum = res.body[0];
                                if (res.body.length === 1) {
                                    this.count = 1;
                                } else {
                                    this.count = res.body[1];
                                }
                            },
                            (res: HttpErrorResponse) => this.onError(res.message)
                        );
                    this.viewVouchersSelected = gOtherVoucher.viewVouchers ? gOtherVoucher.viewVouchers : [];
                    this.gOtherVoucherDetails = gOtherVoucher.gOtherVoucherDetails
                        ? gOtherVoucher.gOtherVoucherDetails.sort((a, b) => a.orderPriority - b.orderPriority)
                        : [];
                    this.gOtherVoucherDetails.forEach((v, i) => {
                        v.stt = i + 1;
                    });
                    this.lstDataForSetDefault = [];
                    this.lstDataForSetDefault.push(this.gOtherVoucherDetails);
                    this.loadDataDefaultForOpenVoucher(this.lstDataForSetDefault);
                    this.no = this.isSoTaiChinh ? gOtherVoucher.noFBook : gOtherVoucher.noMBook;
                    if (this.TCKHAC_SDSoQuanTri === '0') {
                        this.isHideTypeLedger = true;
                    }
                    this.statusVoucher = 1;
                    this.isReadOnly = true;
                    this.copy();
                }
            }
        });
        this.bankService.getBanks().subscribe((res: HttpResponse<IBank[]>) => {
            this.banks = res.body;
        });
        this.accountListService.findByGOtherVoucher().subscribe((res: HttpResponse<IAccountList[]>) => {
            this.accountList = res.body;
        });
        this.autoPrincipleService.getAutoPrinciples().subscribe((res: HttpResponse<IAutoPrinciple[]>) => {
            this.autoPrincipleName = res.body.find(a => a.typeId === 0).autoPrincipleName;
            this.autoPrinciple = res.body.filter(aPrinciple => aPrinciple.typeId === 708 || aPrinciple.typeId === 0).sort((n1, n2) => {
                if (n1.typeId > n2.typeId) {
                    return 1;
                }
                if (n1.typeId < n2.typeId) {
                    return -1;
                }
                return 0;
            });
        });
        this.goodsServicePurchaseService.getGoodServicePurchases().subscribe((res: HttpResponse<IGoodsServicePurchase[]>) => {
            this.goodsServicePurchases = res.body;
        });
        // Check ghi sổ, đếm số dòng
        if (this.gOtherVoucher.id) {
            this.gOtherVoucherDetails =
                this.gOtherVoucher.gOtherVoucherDetails === undefined
                    ? []
                    : this.gOtherVoucher.gOtherVoucherDetails.sort((a, b) => a.orderPriority - b.orderPriority);
            this.gOtherVoucherDetails.forEach((v, i) => {
                v.stt = i + 1;
            });
        } else {
            this.gOtherVoucherDetails = [];
            this.viewVouchersSelected = [];
            this.isRecord = false;
        }
        if (this.gOtherVoucher.id) {
            this.id = this.gOtherVoucher.id;
        }
        this.isCreateUrl = window.location.href.includes('/hach-toan-chi-phi-luong/new');
        if (this.gOtherVoucher.id && !this.isCreateUrl) {
            this.isEdit = this.isCreateUrl = false;
        } else {
            this.isEdit = this.isCreateUrl = true;
        }
        this.disableAddButton = true;
        if (this.gOtherVoucher.id) {
            if (!this.gOtherVoucher.recorded && this.isEditUrl) {
                this.disableEditButton = false;
            } else {
                this.disableEditButton = true;
            }
        } else {
            this.disableEditButton = true;
        }
        this.isRefVoucher = window.location.href.includes('/edit/from-ref');
        this.registerRef();
        this.copy();
        this.afterCopyRow();
        this.afterAddRow();
        this.eventSubscriber = this.eventManager.subscribe('saveSuccess', response => {
            this.isCbbSaveAndNew = false;
            this.registerComboboxSave(response);
            this.utilsService.setShowPopup(false);
        });
        this.eventSubscribers.push(this.eventSubscriber);
        this.eventSubscriber = this.eventManager.subscribe('saveAndNewSuccess', response => {
            this.isCbbSaveAndNew = false;
            this.registerComboboxSave(response);
            this.utilsService.setShowPopup(false);
        });
        this.eventSubscribers.push(this.eventSubscriber);
        this.eventSubscriber = this.eventManager.subscribe('closePopup', response => {
            this.utilService.setShowPopup(response.content);
        });
        this.eventSubscribers.push(this.eventSubscriber);
        this.sumAfterDeleteByContextMenu();
        this.checkSameOrg();
    }

    closeForm() {
        event.preventDefault();
        if (this.gOtherVoucherCopy && (this.statusVoucher === 0 || this.statusVoucher === 1) && !this.utilsService.isShowPopup) {
            if (
                !this.utilsService.isEquivalent(this.gOtherVoucher, this.gOtherVoucherCopy) ||
                !this.utilsService.isEquivalentArray(this.gOtherVoucherDetails, this.gOtherVoucherDetailsCopy) ||
                !this.utilsService.isEquivalentArray(this.viewVouchersSelected, this.viewVouchersSelectedCopy)
            ) {
                if (this.isReadOnly) {
                    this.closeAll();
                    return;
                }
                // this.copy();
                if (this.modalRef) {
                    this.modalRef.close();
                }
                this.modalRef = this.modalService.open(this.content, { backdrop: 'static' });
                return;
            } else {
                this.closeAll();
                return;
            }
        } else if (!this.utilsService.isShowPopup) {
            this.copy();
            this.closeAll();
            return;
        }
    }

    @ebAuth(['ROLE_ADMIN', ROLE.HachToanChiPhiLuong_Sua])
    edit() {
        event.preventDefault();
        // && this.isRecord
        if (
            !this.isCreateUrl &&
            !this.checkCloseBook(this.currentAccount, this.gOtherVoucher.postedDate) &&
            !this.utilsService.isShowPopup
        ) {
            this.isCreateUrl = !this.isCreateUrl;
            this.isEdit = this.isCreateUrl;
            this.disableAddButton = false;
            this.disableEditButton = true;
            this.statusVoucher = 0;
            this.isReadOnly = false;
            this.focusFirstInput();
        }
    }

    @ebAuth(['ROLE_ADMIN', ROLE.HachToanChiPhiLuong_Them])
    addNew($event) {
        event.preventDefault();
        if (!this.isCreateUrl && !this.utilsService.isShowPopup) {
            if (window.location.href.includes('/edit/from-ref')) {
                this.router.navigate(['chung-tu-nghiep-vu-khac/new']);
            } else {
                sessionStorage.setItem('checkNewHTCPL', JSON.stringify(true));
                this.router.navigate(['hach-toan-chi-phi-luong']);
            }
        }
    }

    @ebAuth(['ROLE_ADMIN', ROLE.HachToanChiPhiLuong_Them, ROLE.HachToanChiPhiLuong_Sua])
    save(isNew = false) {
        event.preventDefault();
        this.warningMessage = '';
        this.gOtherVoucher.gOtherVoucherDetails = this.gOtherVoucherDetails;
        this.gOtherVoucher.viewVouchers = this.viewVouchersSelected;
        if (this.isCreateUrl && !this.isShowPopup) {
            if (this.checkError()) {
                let sum = 0;
                if (this.gOtherVoucher && this.gOtherVoucher.gOtherVoucherDetails && this.gOtherVoucherDetails.length > 0) {
                    for (let i = 0; i < this.gOtherVoucher.gOtherVoucherDetails.length; i++) {
                        this.gOtherVoucher.gOtherVoucherDetails[i].orderPriority = i + 1;
                        this.gOtherVoucher.gOtherVoucherDetails[i].amountOriginal = this.gOtherVoucher.gOtherVoucherDetails[i].amount;
                        sum += this.gOtherVoucher.gOtherVoucherDetails[i].amount;
                    }
                }
                this.gOtherVoucher.totalAmount = sum;
                this.gOtherVoucher.totalAmountOriginal = sum;
                this.isCreateUrl = !this.isCreateUrl;
                this.isEdit = this.isCreateUrl;
                this.disableAddButton = true;
                if (this.gOtherVoucher.typeLedger === 0) {
                    this.gOtherVoucher.noFBook = this.no;
                    this.gOtherVoucher.noMBook = null;
                } else if (this.gOtherVoucher.typeLedger === 1) {
                    this.gOtherVoucher.noFBook = null;
                    this.gOtherVoucher.noMBook = this.no;
                } else {
                    if (this.isSoTaiChinh) {
                        this.gOtherVoucher.noFBook = this.no;
                    } else {
                        this.gOtherVoucher.noMBook = this.no;
                    }
                }
                this.gOtherVoucher.typeID = 708;
                if (this.sessionPSTimeSheet.lastPSSalarySheetID) {
                    this.gOtherVoucher.pSSalarySheetID = this.sessionPSTimeSheet.lastPSSalarySheetID;
                }
                this.isSaving = true;

                // check is url new
                if (this.isCreateUrl && this.gOtherVoucher.id) {
                    this.gOtherVoucher.id = undefined;
                }
                if (!this.isCreateUrl && !this.isEditUrl) {
                    this.gOtherVoucher.id = undefined;
                    for (let i = 0; i < this.gOtherVoucher.gOtherVoucherDetails.length; i++) {
                        this.gOtherVoucher.gOtherVoucherDetails[i].id = undefined;
                    }
                }
                if (!this.isCreateUrl && !this.isEditUrl) {
                    this.gOtherVoucher.id = undefined;
                }
                if (this.gOtherVoucher.id) {
                    this.subscribeToSaveResponse(this.gOtherVoucherService.update(this.gOtherVoucher));
                } else {
                    this.subscribeToSaveResponse(this.gOtherVoucherService.create(this.gOtherVoucher));
                }
            } else {
            }
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<any>>) {
        result.subscribe(
            (res: HttpResponse<any>) => {
                if (res.body.status === 0) {
                    if (this.isEditUrl) {
                        this.toastr.success(
                            this.translate.instant('ebwebApp.mBDeposit.editSuccess'),
                            this.translate.instant('ebwebApp.mBDeposit.message')
                        );
                    } else {
                        this.toastr.success(
                            this.translate.instant('ebwebApp.mBDeposit.insertSuccess'),
                            this.translate.instant('ebwebApp.mBDeposit.message')
                        );
                    }
                    this.isEdit = this.isCreateUrl = false;
                    if (res.body.gOtherVoucher.recorded) {
                        this.disableAddButton = false;
                    } else {
                        this.disableAddButton = true;
                    }
                    this.gOtherVoucher.id = res.body.gOtherVoucher.id;
                    this.gOtherVoucher.recorded = res.body.gOtherVoucher.recorded;
                    this.onSaveSuccess();
                    this.isReadOnly = true;
                    this.router.navigate(['./hach-toan-chi-phi-luong', this.gOtherVoucher.id, 'edit']);
                    if (this.modalRef) {
                        this.modalRef.close();
                    }
                } else if (res.body.status === 1) {
                    this.noVoucherExist();
                    this.isEdit = this.isCreateUrl = true;
                    if (this.modalRef) {
                        this.modalRef.close();
                    }
                    return;
                } else if (res.body.status === 2) {
                    if (res.body.msg === MSGERROR.XUAT_QUA_TON_QUY_QT) {
                        this.isSaving = false;
                        this.statusVoucher = 1;
                        this.isEdit = false;
                        this.disableEditButton = false;
                        this.disableAddButton = true;
                        this.gOtherVoucher.id = res.body.gOtherVoucher.id;
                        this.copy();
                        this.router.navigate(['./hach-toan-chi-phi-luong', this.gOtherVoucher.id, 'edit']);
                        this.toastr.error(
                            this.translate.instant('global.messages.error.checkTonQuyQT'),
                            this.translate.instant('ebwebApp.mCReceipt.error.error')
                        );
                    } else if (res.body.msg === MSGERROR.XUAT_QUA_TON_QUY_TC) {
                        this.isSaving = false;
                        this.statusVoucher = 1;
                        this.isEdit = false;
                        this.disableEditButton = false;
                        this.disableAddButton = true;
                        this.gOtherVoucher.id = res.body.gOtherVoucher.id;
                        this.copy();
                        this.router.navigate(['./hach-toan-chi-phi-luong', this.gOtherVoucher.id, 'edit']);
                        this.toastr.error(
                            this.translate.instant('global.messages.error.checkTonQuyTC'),
                            this.translate.instant('ebwebApp.mCReceipt.error.error')
                        );
                    } else if (res.body.msg === MSGERROR.XUAT_QUA_TON_QUY) {
                        this.isSaving = false;
                        this.statusVoucher = 1;
                        this.isEdit = false;
                        this.disableEditButton = false;
                        this.disableAddButton = true;
                        this.gOtherVoucher.id = res.body.gOtherVoucher.id;
                        this.copy();
                        this.router.navigate(['./hach-toan-chi-phi-luong', this.gOtherVoucher.id, 'edit']);
                        this.toastr.error(
                            this.translate.instant('global.messages.error.checkTonQuy'),
                            this.translate.instant('ebwebApp.mCReceipt.error.error')
                        );
                    } else {
                        this.recordFailed();
                    }
                }
            },
            (res: HttpErrorResponse) => {
                this.onSaveError();
                this.isEdit = this.isCreateUrl = true;
            }
        );
    }

    private recordFailed() {
        this.toastr.error(this.translate.instant('global.data.recordFailed'), this.translate.instant('ebwebApp.mCReceipt.home.message'));
    }

    private onSaveSuccess() {
        this.copy();
        this.statusVoucher = 1;
        this.isSaving = false;
        this.disableAddButton = true;
        this.disableEditButton = true;
        this.isRecord = false;
    }

    private onSaveError() {
        this.isSaving = false;
        this.isCreateUrl = !this.isCreateUrl;
        this.isEdit = this.isCreateUrl;
    }

    get gOtherVoucher() {
        return this._gOtherVoucher;
    }

    set gOtherVoucher(gOtherVoucher: IGOtherVoucher) {
        this._gOtherVoucher = gOtherVoucher;
    }

    // selectChangeAccountingObject() {
    //     const iAccountingObject = this.accountingObjects.find(
    //         accountingObject => accountingObject.id === this.gOtherVoucher.accountingObjectID
    //     );
    //     if (iAccountingObject) {
    //         this.gOtherVoucher.accountingObjectName = iAccountingObject.accountingObjectName;
    //         this.gOtherVoucher.accountingObjectAddress = iAccountingObject.accountingObjectAddress;
    //     } else {
    //         this.gOtherVoucher.accountingObjectName = '';
    //         this.gOtherVoucher.accountingObjectAddress = '';
    //     }
    //     for (const dt of this.gOtherVoucherDetails) {
    //         dt.accountingObjectID = this.gOtherVoucher.accountingObjectID;
    //     }
    //     for (const dtt of this.gOtherVoucherDetailTax) {
    //         dtt.accountingObjectID = this.gOtherVoucher.accountingObjectID;
    //     }
    // }

    AddnewRow(eventData: any, select: number) {
        if (this.isCreateUrl) {
            if (select === 0) {
                this.gOtherVoucherDetails.push(Object.assign({}, this.gOtherVoucherDetails[this.gOtherVoucherDetails.length - 1]));
                if (this.gOtherVoucherDetails.length === 1) {
                    this.gOtherVoucher.totalAmountOriginal = 0;
                    this.gOtherVoucher.totalAmount = 0;
                    // if (this.gOtherVoucher.accountingObjectID) {
                    //     this.gOtherVoucherDetails[0].accountingObjectID = this.gOtherVoucher.accountingObjectID;
                    // }
                    if (this.gOtherVoucher.reason) {
                        this.gOtherVoucherDetails[0].description = this.gOtherVoucher.reason;
                    }
                }
                this.gOtherVoucherDetails[this.gOtherVoucherDetails.length - 1].id = undefined;
                if (this.iAutoPrinciple) {
                    this.gOtherVoucherDetails[this.gOtherVoucherDetails.length - 1].debitAccount = this.iAutoPrinciple.debitAccount;
                    this.gOtherVoucherDetails[this.gOtherVoucherDetails.length - 1].creditAccount = this.iAutoPrinciple.creditAccount;
                }
                this.gOtherVoucherDetails[this.gOtherVoucherDetails.length - 1].amount = 0;
                this.gOtherVoucherDetails[this.gOtherVoucherDetails.length - 1].amountOriginal = 0;
                // this.gOtherVoucherDetails[this.gOtherVoucherDetails.length - 1].orderPriority = this.gOtherVoucherDetails.length - 1;
                const nameTag = 'diengiai';
                const index: number = this.gOtherVoucherDetails.length - 1;
                const nameTagIndex = nameTag + String(index);
                setTimeout(function() {
                    const element: HTMLElement = document.getElementById(nameTagIndex);
                    if (element) {
                        element.focus();
                    }
                }, 0);
            } else {
            }
        }
    }

    KeyPress(value: number, select: number) {
        if (select === 0) {
            this.gOtherVoucherDetails.splice(value, 1);
        }
    }

    @ebAuth(['ROLE_ADMIN', ROLE.HachToanChiPhiLuong_GhiSo])
    record() {
        event.preventDefault();
        if (
            !this.gOtherVoucher.recorded &&
            !this.isCreateUrl &&
            !this.checkCloseBook(this.currentAccount, this.gOtherVoucher.postedDate) &&
            !this.utilsService.isShowPopup
        ) {
            if (this.gOtherVoucher.id) {
                this.record_ = {};
                this.record_.id = this.gOtherVoucher.id;
                this.record_.typeID = this.gOtherVoucher.typeID;
                if (!this.gOtherVoucher.recorded) {
                    this.gLService.record(this.record_).subscribe((res: HttpResponse<Irecord>) => {
                        if (res.body.success) {
                            this.gOtherVoucher.recorded = true;
                            this.toastr.success(
                                this.translate.instant('ebwebApp.mBCreditCard.recordSuccess'),
                                this.translate.instant('ebwebApp.mBCreditCard.message')
                            );
                            this.disableEditButton = true;
                            this.isRecord = false;
                        } else if (res.body.msg === MSGERROR.XUAT_QUA_TON_QUY_QT) {
                            this.toastr.error(
                                this.translate.instant('global.messages.error.checkTonQuyQT'),
                                this.translate.instant('ebwebApp.mCReceipt.error.error')
                            );
                        } else if (res.body.msg === MSGERROR.XUAT_QUA_TON_QUY_TC) {
                            this.toastr.error(
                                this.translate.instant('global.messages.error.checkTonQuyTC'),
                                this.translate.instant('ebwebApp.mCReceipt.error.error')
                            );
                        } else if (res.body.msg === MSGERROR.XUAT_QUA_TON_QUY) {
                            this.toastr.error(
                                this.translate.instant('global.messages.error.checkTonQuy'),
                                this.translate.instant('ebwebApp.mCReceipt.error.error')
                            );
                        }
                    });
                }
            }
        }
    }

    @ebAuth(['ROLE_ADMIN', ROLE.HachToanChiPhiLuong_GhiSo])
    unrecord() {
        event.preventDefault();
        if (
            this.gOtherVoucher.recorded &&
            !this.isCreateUrl &&
            !this.checkCloseBook(this.currentAccount, this.gOtherVoucher.postedDate) &&
            !this.utilsService.isShowPopup
        ) {
            if (this.gOtherVoucher.id) {
                // if (this.gOtherVoucher.recorded) {
                //     this.isRecord = false;
                // } else {
                //     this.isRecord = true;
                // }
                this.record_ = {};
                this.record_.id = this.gOtherVoucher.id;
                this.record_.typeID = this.gOtherVoucher.typeID;
                if (this.gOtherVoucher.recorded) {
                    this.gLService.unrecord(this.record_).subscribe((res: HttpResponse<Irecord>) => {
                        if (res.body.success) {
                            this.gOtherVoucher.recorded = false;
                            this.toastr.success(
                                this.translate.instant('ebwebApp.mBCreditCard.unrecordSuccess'),
                                this.translate.instant('ebwebApp.mBCreditCard.message')
                            );
                        }
                    });
                    this.disableEditButton = false;
                    this.isRecord = true;
                }
            }
        }
    }

    CTKTExportPDF(isDownload, typeReports: number) {
        if (!this.isCreateUrl) {
            this.gOtherVoucherService
                .getCustomerReport({
                    id: this.gOtherVoucher.id,
                    typeID: 708,
                    typeReport: typeReports
                })
                .subscribe(response => {
                    // this.showReport(response);
                    const file = new Blob([response.body], { type: 'application/pdf' });
                    const fileURL = window.URL.createObjectURL(file);

                    if (isDownload) {
                        const link = document.createElement('a');
                        document.body.appendChild(link);
                        link.download = fileURL;
                        link.setAttribute('style', 'display: none');
                        const name = 'Bao_co.pdf';
                        link.setAttribute('download', name);
                        link.href = fileURL;
                        link.click();
                    } else {
                        const contentDispositionHeader = response.headers.get('Content-Disposition');
                        const result = contentDispositionHeader
                            .split(';')[1]
                            .trim()
                            .split('=')[1];
                        const newWin = window.open(fileURL, '_blank');

                        // add a load listener to the window so that the title gets changed on page load
                        newWin.addEventListener('load', function() {
                            newWin.document.title = result.replace(/"/g, '');
                            // this.router.navigate(['/report/buy']);
                        });
                    }
                });
            this.toastr.success(
                this.translate.instant('ebwebApp.mBCreditCard.printing') + this.translate.instant('ebwebApp.mBCreditCard.financialPaper'),
                this.translate.instant('ebwebApp.mBCreditCard.message')
            );
        }
    }

    // ham lui, tien
    previousEdit() {
        // goi service get by row num
        if (this.rowNum !== this.count) {
            this.utilsService
                .findByRowNum({
                    id: this.gOtherVoucher.id,
                    isNext: false,
                    typeID: this.TYPE_HACH_TOAN_CHI_PHI_LUONG,
                    searchVoucher: this.searchVoucher === undefined ? null : this.searchVoucher
                })
                .subscribe(
                    (res: HttpResponse<IGOtherVoucher>) => {
                        this.navigate(res.body);
                    },
                    (res: HttpErrorResponse) => this.onError(res.message)
                );
        }
    }

    nextEdit() {
        // goi service get by row num
        if (this.rowNum !== 1) {
            this.utilsService
                .findByRowNum({
                    id: this.gOtherVoucher.id,
                    isNext: true,
                    typeID: this.TYPE_HACH_TOAN_CHI_PHI_LUONG,
                    searchVoucher: this.searchVoucher === undefined ? null : this.searchVoucher
                })
                .subscribe(
                    (res: HttpResponse<IGOtherVoucher>) => {
                        // this.router.navigate(['/phieu-chi', res.body.id, 'edit']);
                        this.navigate(res.body);
                    },
                    (res: HttpErrorResponse) => this.onError(res.message)
                );
        }
    }

    navigate(iGOtherVoucher: IGOtherVoucher) {
        switch (iGOtherVoucher.typeID) {
            case this.TYPE_CHUNG_TU_NGHIEP_VU_KHAC:
                this.router.navigate(['./chung-tu-nghiep-vu-khac', iGOtherVoucher.id, 'edit']);
                break;
            case this.TYPE_KET_CHUYEN_LAI_LO:
                this.router.navigate(['./ket-chuyen-lai-lo', iGOtherVoucher.id, 'edit', 'from-g-other-voucher']);
                break;
            case this.TYPE_PHAN_BO_CHI_PHI_TRA_TRUOC:
                this.router.navigate(['./phan-bo-chi-phi-tra-truoc', iGOtherVoucher.id, 'edit', 'from-g-other-voucher']);
                break;
            case this.TYPE_HACH_TOAN_CHI_PHI_LUONG:
                this.router.navigate(['./hach-toan-chi-phi-luong', iGOtherVoucher.id, 'edit', 'from-g-other-voucher']);
                break;
        }
    }

    // end of lui tien
    onError(errorMessage: string) {
        this.toastr.error(this.translate.instant('ebwebApp.mBCreditCard.error'), this.translate.instant('ebwebApp.mBCreditCard.message'));
    }

    onRightClick($event, data, selectedData, isNew, isDelete, select, currentRow) {
        if (this.isCreateUrl) {
            this.contextMenu.isNew = isNew;
            this.contextMenu.isDelete = isDelete;
            this.contextMenu.isShow = true;
            this.contextMenu.event = $event;
            this.contextMenu.data = data;
            this.contextMenu.selectedData = selectedData;
            if (select === 0 || select === 1 || select === 2) {
                this.contextMenu.isCopy = true;
            } else {
                this.contextMenu.isCopy = false;
            }
            this.select = select;
            this.currentRow = currentRow;
        }
    }

    closeContextMenu() {
        this.contextMenu.isShow = false;
    }

    @ebAuth(['ROLE_ADMIN', ROLE.HachToanChiPhiLuong_Xoa])
    delete() {
        event.preventDefault();
        if (
            !this.isCreateUrl &&
            !this.checkCloseBook(this.currentAccount, this.gOtherVoucher.postedDate) &&
            !this.utilsService.isShowPopup
        ) {
            if (this.gOtherVoucher.recorded) {
                // this.toastr.error(
                //     this.translate.instant('ebwebApp.mBCreditCard.errorDeleteVoucherNo'),
                //     this.translate.instant('ebwebApp.mBCreditCard.message')
                // );
                return;
            } else {
                this.router.navigate(['/hach-toan-chi-phi-luong', { outlets: { popup: this.gOtherVoucher.id + '/delete' } }]);
            }
        }
    }

    newArr(lenght: number): any[] {
        if (lenght > 0) {
            return new Array(lenght);
        } else {
            return new Array(0);
        }
    }

    subscribeToSaveResponseAndContinue(result: Observable<HttpResponse<any>>) {
        result.subscribe(
            (resResult: HttpResponse<any>) => {
                if (resResult.body.status === 0) {
                    if (this.isEditUrl) {
                        this.toastr.success(
                            this.translate.instant('ebwebApp.mBDeposit.editSuccess'),
                            this.translate.instant('ebwebApp.mBDeposit.message')
                        );
                    } else {
                        this.toastr.success(
                            this.translate.instant('ebwebApp.mBDeposit.insertSuccess'),
                            this.translate.instant('ebwebApp.mBDeposit.message')
                        );
                    }
                    this.onSaveSuccess();
                    this.gOtherVoucher = {};
                    this.gOtherVoucher.recorded = false;
                    this.gOtherVoucherDetails = [];
                    this.viewVouchersSelected = [];
                    this.copy();
                    this.router.navigate(['hach-toan-chi-phi-luong/new']);
                    this.isEdit = this.isCreateUrl = true;
                    this.gOtherVoucher.date = this.utilService.ngayHachToan(this.currentAccount);
                    this.gOtherVoucher.postedDate = this.gOtherVoucher.date;
                    if (this.isSoTaiChinh) {
                        this.gOtherVoucher.typeLedger = 0;
                    } else {
                        this.gOtherVoucher.typeLedger = 1;
                    }
                    this.utilService
                        .getGenCodeVoucher({
                            typeGroupID: 70, // typeGroupID loại chứng từ
                            companyID: '', // ID công ty
                            branchID: '', // ID chi nhánh
                            displayOnBook: this.sysTypeLedger // 0 - sổ tài chính, 1 - sổ quản trị, 2-cả hai sổ
                        })
                        .subscribe((res2: HttpResponse<string>) => {
                            // this.mCReceipt.noFBook = (resResult.body.toString());
                            this.no = res2.body;
                            this.gOtherVoucher.currencyID = this.currency.currencyCode;
                            this.translate.get(['ebwebApp.gOtherVoucher.defaultReason']).subscribe(res => {
                                this.gOtherVoucher.reason = res['ebwebApp.gOtherVoucher.defaultReason'];
                            });
                            this.copy();
                        });
                    this.copy();
                    this.isReadOnly = true;
                } else if (resResult.body.status === 1) {
                    this.noVoucherExist();
                    return;
                } else if (resResult.body.status === 2) {
                    if (resResult.body.msg === MSGERROR.XUAT_QUA_TON_QUY_QT) {
                        this.isSaving = false;
                        this.statusVoucher = 1;
                        this.isEdit = false;
                        this.gOtherVoucher.id = resResult.body.gOtherVoucher.id;
                        this.toastr.error(
                            this.translate.instant('global.messages.error.checkTonQuyQT'),
                            this.translate.instant('ebwebApp.mCReceipt.error.error')
                        );
                        this.copy();
                        this.router.navigate(['/hach-toan-chi-phi-luong', resResult.body.gOtherVoucher.id, 'edit']).then(() => {
                            this.isReadOnly = true;
                            this.router.navigate(['/hach-toan-chi-phi-luong', 'new']);
                        });
                    } else if (resResult.body.msg === MSGERROR.XUAT_QUA_TON_QUY_TC) {
                        this.isSaving = false;
                        this.statusVoucher = 1;
                        this.isEdit = false;
                        this.gOtherVoucher.id = resResult.body.gOtherVoucher.id;
                        this.toastr.error(
                            this.translate.instant('global.messages.error.checkTonQuyTC'),
                            this.translate.instant('ebwebApp.mCReceipt.error.error')
                        );
                        this.copy();
                        this.router.navigate(['/hach-toan-chi-phi-luong', resResult.body.gOtherVoucher.id, 'edit']).then(() => {
                            this.isReadOnly = true;
                            this.router.navigate(['/hach-toan-chi-phi-luong', 'new']);
                        });
                    } else if (resResult.body.msg === MSGERROR.XUAT_QUA_TON_QUY) {
                        this.isSaving = false;
                        this.statusVoucher = 1;
                        this.isEdit = false;
                        this.gOtherVoucher.id = resResult.body.gOtherVoucher.id;
                        this.toastr.error(
                            this.translate.instant('global.messages.error.checkTonQuy'),
                            this.translate.instant('ebwebApp.mCReceipt.error.error')
                        );
                        this.copy();
                        this.router.navigate(['/hach-toan-chi-phi-luong', resResult.body.gOtherVoucher.id, 'edit']).then(() => {
                            this.isReadOnly = true;
                            this.router.navigate(['/hach-toan-chi-phi-luong', 'new']);
                        });
                    } else {
                        this.recordFailed();
                    }
                }
            },
            (res: HttpErrorResponse) => this.onSaveError()
        );
    }

    registerRef() {
        this.eventSubscriber = this.eventManager.subscribe('selectViewVoucher', response => {
            if (this.isCreateUrl) {
                this.viewVouchersSelected = response.content;
            }
        });
        this.eventSubscribers.push(this.eventSubscriber);
    }

    beforeChange($event: NgbTabChangeEvent) {
        if ($event.nextId === 'reference') {
            $event.preventDefault();
            if (this.isCreateUrl) {
                this.modalRef = this.refModalService.open(this.viewVouchersSelected);
            }
        }
        if ($event.nextId === 'tab1') {
            this.tabShow = 1;
        } else if ($event.nextId === 'tab2') {
            this.tabShow = 2;
        }
    }

    getAmountOriginal() {
        if (this.gOtherVoucherDetails && this.gOtherVoucherDetails.length > 0) {
            return this.gOtherVoucherDetails.map(n => n.amountOriginal).reduce((a, b) => a + b);
        } else {
            return 0;
        }
    }

    getAmount() {
        if (this.gOtherVoucherDetails && this.gOtherVoucherDetails.length > 0) {
            return this.gOtherVoucherDetails.map(n => n.amount).reduce((a, b) => a + b);
        } else {
            return 0;
        }
    }

    closeAll() {
        this.copy();
        if (sessionStorage.getItem('dataSearchGOtherVoucher')) {
            if (window.location.href.includes('/edit/from-g-other-voucher')) {
                this.router.navigate(['/chung-tu-nghiep-vu-khac', 'hasSearch', '1']);
            } else {
                this.router.navigate(['/hach-toan-chi-phi-luong', 'hasSearch', '1']);
            }
        } else {
            if (window.location.href.includes('/edit/from-g-other-voucher')) {
                this.router.navigate(['/chung-tu-nghiep-vu-khac']);
            } else {
                this.router.navigate(['/hach-toan-chi-phi-luong']);
            }
        }
    }

    saveContent() {
        if (this.modalRef) {
            this.modalRef.close();
        }
        this.save();
    }

    close() {
        this.copy();
        if (this.modalRef) {
            this.modalRef.close();
        }
        this.closeAll();
    }

    copy() {
        this.gOtherVoucherCopy = Object.assign({}, this.gOtherVoucher);
        this.gOtherVoucherDetailsCopy = this.gOtherVoucherDetails.map(object => ({ ...object }));
        this.viewVouchersSelectedCopy = this.viewVouchersSelected.map(object => ({ ...object }));
    }

    noVoucherExist() {
        this.toastr.error(
            this.translate.instant('global.data.noVocherAlreadyExist'),
            this.translate.instant('ebwebApp.mCReceipt.home.message')
        );
        this.isCreateUrl = !this.isCreateUrl;
        this.isEdit = this.isCreateUrl;
    }

    exit() {
        if (this.modalRef) {
            this.modalRef.close();
        }
    }

    /*
    * hàm ss du lieu 2 object và 2 mảng object
    * return true: neu tat ca giong nhau
    * return fale: neu 1 trong cac object ko giong nshau
    * */
    canDeactive(): boolean {
        if (this.isReadOnly || this.isReadOnly === undefined) {
            return true;
        } else {
            return (
                this.utilService.isEquivalent(this.gOtherVoucher, this.gOtherVoucherCopy) &&
                this.utilService.isEquivalentArray(this.gOtherVoucherDetails, this.gOtherVoucherDetailsCopy) &&
                this.utilsService.isEquivalentArray(this.viewVouchersSelected, this.viewVouchersSelectedCopy)
            );
        }
    }

    checkError(): boolean {
        if (!this.no) {
            this.warningMessage =
                this.translate.instant('ebwebApp.mBCreditCard.voucherNumber') +
                ' ' +
                this.translate.instant('ebwebApp.mBCreditCard.isNotNull');
            this.toastr.error(this.warningMessage.toString(), this.translate.instant('ebwebApp.mBCreditCard.message'));
            return false;
        }
        if (this.checkCloseBook(this.currentAccount, this.gOtherVoucher.postedDate)) {
            this.toastr.error(this.translate.instant('ebwebApp.mBCreditCard.checkCloseBook'));
            return false;
        }
        if (!this.gOtherVoucher.date) {
            this.warningMessage =
                this.translate.instant('ebwebApp.mBCreditCard.date') + ' ' + this.translate.instant('ebwebApp.mBCreditCard.isNotNull');
            this.toastr.error(this.warningMessage.toString(), this.translate.instant('ebwebApp.mBCreditCard.message'));
            return false;
        }
        if (!this.gOtherVoucher.postedDate) {
            this.warningMessage =
                this.translate.instant('ebwebApp.mBCreditCard.postedDate') +
                ' ' +
                this.translate.instant('ebwebApp.mBCreditCard.isNotNull');
            this.toastr.error(this.warningMessage.toString(), this.translate.instant('ebwebApp.mBCreditCard.message'));
            return false;
        }
        if (this.gOtherVoucher.postedDate.format(DATE_FORMAT) < this.gOtherVoucher.date.format(DATE_FORMAT)) {
            this.toastr.error(
                this.translate.instant('ebwebApp.mBCreditCard.errorPostedDateAndDate'),
                this.translate.instant('ebwebApp.mBCreditCard.message')
            );
            return false;
        }
        if (this.gOtherVoucher.typeLedger === undefined || this.gOtherVoucher.typeLedger === null) {
            this.warningMessage =
                this.translate.instant('ebwebApp.mBCreditCard.typeLedger') +
                ' ' +
                this.translate.instant('ebwebApp.mBCreditCard.isNotNull');
            this.toastr.error(this.warningMessage.toString(), this.translate.instant('ebwebApp.mBCreditCard.message'));
            return false;
        }
        if (!this.utilService.checkNoBook(this.no ? this.no : null, this.currentAccount)) {
            return false;
        }
        if (this.gOtherVoucher.gOtherVoucherDetails.length === 0) {
            this.warningMessage =
                this.translate.instant('ebwebApp.mBCreditCard.home.details') +
                ' ' +
                this.translate.instant('ebwebApp.mBCreditCard.isNotNull');
            this.toastr.error(this.warningMessage.toString(), this.translate.instant('ebwebApp.mBCreditCard.message'));
            return false;
        }
        for (this.i = 0; this.i < this.gOtherVoucher.gOtherVoucherDetails.length; this.i++) {
            if (
                !this.gOtherVoucher.gOtherVoucherDetails[this.i].debitAccount ||
                this.gOtherVoucher.gOtherVoucherDetails[this.i].debitAccount === ''
            ) {
                this.warningMessage =
                    this.translate.instant('ebwebApp.mBCreditCard.debitAccount') +
                    ' ' +
                    this.translate.instant('ebwebApp.mBCreditCard.isNotNull');
                this.toastr.error(this.warningMessage.toString(), this.translate.instant('ebwebApp.mBCreditCard.message'));
                return false;
            }
            if (
                !this.gOtherVoucher.gOtherVoucherDetails[this.i].creditAccount ||
                this.gOtherVoucher.gOtherVoucherDetails[this.i].creditAccount === ''
            ) {
                this.warningMessage =
                    this.translate.instant('ebwebApp.mBCreditCard.creditAccount') +
                    ' ' +
                    this.translate.instant('ebwebApp.mBCreditCard.isNotNull');
                this.toastr.error(this.warningMessage.toString(), this.translate.instant('ebwebApp.mBCreditCard.message'));
                return false;
            }
        }
        const checkAcc = this.utilsService.checkAccoutWithDetailType(
            this.debitAccountList,
            this.creditAccountList,
            this.gOtherVoucherDetails,
            this.accountingObjects,
            this.costSets,
            this.eMContracts,
            null,
            this.bankAccountDetails,
            this.organizationUnits,
            this.expenseItems,
            this.budgetItems,
            this.statisticCodes,
            null,
            true
        );
        if (checkAcc) {
            this.toastr.error(checkAcc, this.translate.instant('ebwebApp.mCReceipt.error.error'));
            return false;
        }
        return true;
    }

    continueModalSaveComponent() {
        this.isCreateUrl = !this.isCreateUrl;
        this.isEdit = this.isCreateUrl;
        this.disableAddButton = true;
        if (this.gOtherVoucher.typeLedger === 0) {
            this.gOtherVoucher.noFBook = this.no;
            this.gOtherVoucher.noMBook = null;
        } else if (this.gOtherVoucher.typeLedger === 1) {
            this.gOtherVoucher.noFBook = null;
            this.gOtherVoucher.noMBook = this.no;
        } else {
            if (this.isSoTaiChinh) {
                this.gOtherVoucher.noFBook = this.no;
            } else {
                this.gOtherVoucher.noMBook = this.no;
            }
        }
        this.gOtherVoucher.typeID = 708;
        this.isSaving = true;
        // check is url new
        if (this.isCreateUrl && this.gOtherVoucher.id) {
            this.gOtherVoucher.id = undefined;
        }
        if (!this.isCreateUrl && !this.isEditUrl) {
            this.gOtherVoucher.id = undefined;
            for (let i = 0; i < this.gOtherVoucher.gOtherVoucherDetails.length; i++) {
                this.gOtherVoucher.gOtherVoucherDetails[i].id = undefined;
            }
        }
        if (!this.isCreateUrl && !this.isEditUrl) {
            this.gOtherVoucher.id = undefined;
        }
        if (this.gOtherVoucher.id) {
            this.subscribeToSaveResponse(this.gOtherVoucherService.update(this.gOtherVoucher));
        } else {
            this.subscribeToSaveResponse(this.gOtherVoucherService.create(this.gOtherVoucher));
        }
    }

    closeModalSaveComponent() {
        if (this.modalRef) {
            this.modalRef.close();
        }
    }

    keyDownAddRow(value: number, select: number) {
        if (!this.getSelectionText()) {
            if (select === 0) {
                if (value !== null && value !== undefined) {
                    const ob: IGOtherVoucherDetails = Object.assign({}, this.gOtherVoucherDetails[value]);
                    ob.id = undefined;
                    ob.orderPriority = undefined;
                    this.gOtherVoucherDetails.push(ob);
                } else {
                    this.gOtherVoucherDetails.push({});
                }
            }
            this.selectChangeTotalAmount();
        }
    }

    afterCopyRow() {
        this.eventSubscriber = this.eventManager.subscribe('afterCopyRow', response => {
            if (this.select === 0 || this.select === 3) {
                const ob: IGOtherVoucherDetails = Object.assign({}, this.contextMenu.selectedData);
                ob.id = undefined;
                ob.orderPriority = undefined;
                this.gOtherVoucherDetails.push(ob);
                this.selectChangeTotalAmount();
            }
        });
        this.eventSubscribers.push(this.eventSubscriber);
    }

    afterAddRow() {
        this.eventSubscriber = this.eventManager.subscribe('afterAddNewRow', response => {
            if (this.select === 0 || this.select === 3) {
                this.gOtherVoucherDetails.push({ description: this.gOtherVoucher.reason, amount: 0, amountOriginal: 0 });
            }
        });
        this.eventSubscribers.push(this.eventSubscriber);
    }

    selectChangeTotalAmount() {
        let totalAmount = 0;
        let totalAmountOriginal = 0;
        for (let i = 0; i < this.gOtherVoucherDetails.length; i++) {
            totalAmountOriginal += this.gOtherVoucherDetails[i].amountOriginal;
            this.gOtherVoucherDetails[i].amountOriginal = this.gOtherVoucherDetails[i].amount;
            totalAmount += this.gOtherVoucherDetails[i].amount;
        }
        this.gOtherVoucher.totalAmountOriginal = totalAmountOriginal;
        this.gOtherVoucher.totalAmount = totalAmount;
    }

    changeAmount() {
        if (this.isCreateUrl) {
            let totalAmount = 0;
            let totalAmountOriginal = 0;
            for (let i = 0; i < this.gOtherVoucherDetails.length; i++) {
                totalAmount += this.gOtherVoucherDetails[i].amount;
                totalAmountOriginal += this.gOtherVoucherDetails[i].amountOriginal;
            }
            this.gOtherVoucher.totalAmount = totalAmount;
            this.gOtherVoucher.totalAmountOriginal = totalAmountOriginal;
        }
    }

    print($event) {
        this.CTKTExportPDF(false, 0);
    }

    ngAfterViewInit(): void {
        if (this.isCreateUrl) {
            this.focusFirstInput();
        }
    }

    getAmountOriginalType() {
        if (this.isForeignCurrency()) {
            return 8;
        }
        return 7;
    }

    round(value, type) {
        if (type === 8) {
            if (this.isForeignCurrency()) {
                return this.utilService.round(value, this.currentAccount.systemOption, type);
            } else {
                return this.utilService.round(value, this.currentAccount.systemOption, 7);
            }
        } else if (type === 2) {
            if (this.isForeignCurrency()) {
                return this.utilService.round(value, this.currentAccount.systemOption, type);
            } else {
                return this.utilService.round(value, this.currentAccount.systemOption, 1);
            }
        } else {
            return this.utilService.round(value, this.currentAccount.systemOption, type);
        }
    }

    isForeignCurrency() {
        return this.currentAccount && this.gOtherVoucher.currencyID !== this.currentAccount.organizationUnit.currencyID;
    }

    ngAfterViewChecked(): void {
        this.disableInput();
    }

    selectChangeDate() {
        if (this.isEdit) {
            if (this.gOtherVoucher.date) {
                this.gOtherVoucher.postedDate = this.gOtherVoucher.date;
            }
        }
    }

    saveDetails(i) {
        this.currentRow = i;
        this.details = this.gOtherVoucherDetails;
    }

    saveParent() {
        this.currentRow = null;
        this.parent = this.gOtherVoucher;
    }

    addDataToDetail() {
        this.gOtherVoucherDetails = this.details ? this.details : this.gOtherVoucherDetails;
        this.gOtherVoucher = this.parent ? this.parent : this.gOtherVoucher;
    }

    sumAfterDeleteByContextMenu() {
        this.eventSubscriber = this.eventManager.subscribe('afterDeleteRow', response => {
            if (this.isCreateUrl) {
                if (this.select === 0 || this.select === 3) {
                    if (this.gOtherVoucherDetails.length > 1) {
                    }
                    this.gOtherVoucherDetails.splice(this.currentRow, 1);
                } else {
                    this.viewVouchersSelected.splice(this.currentRow, 1);
                    this.select = null;
                }
            }
            this.selectChangeTotalAmount();
        });
        this.eventSubscribers.push(this.eventSubscriber);
    }

    selectChangeDepartmentID(i) {
        if (this.gOtherVoucherDetails[i].lastOrganizationUnitCode) {
            const organizationUnit = this.organizationUnits.find(a => a.id === this.gOtherVoucherDetails[i].departmentID);
            const index = this.gOtherVoucherDetails[i].description.indexOf(this.gOtherVoucherDetails[i].lastOrganizationUnitCode);
            if (index !== null && index !== undefined) {
                this.gOtherVoucherDetails[i].description = this.gOtherVoucherDetails[i].description.substring(0, index);
                this.gOtherVoucherDetails[i].description = this.gOtherVoucherDetails[i].description + organizationUnit.organizationUnitCode;
            }
            this.gOtherVoucherDetails[i].lastOrganizationUnitCode = organizationUnit.organizationUnitCode;
        }
    }

    clickEnter(e, index) {
        let newRow = false;
        const cellCurrent = +document.activeElement.getAttribute('tabIndex');
        if (index === this.gOtherVoucherDetails.length - 1) {
            if (4 * index + 9 === cellCurrent) {
                newRow = true;
            }
        }
        if ((this.isEnter && index === this.gOtherVoucherDetails.length - 1) || newRow) {
            this.AddnewRow(e, 0);
            return;
        }
        if (this.isEnter && index < this.gOtherVoucherDetails.length - 1) {
            const inputs = document.getElementsByTagName('input');
            for (let i = 0; i < inputs.length; i++) {
                if (inputs[i].tabIndex === 4 * (index + 1) + 6) {
                    setTimeout(() => {
                        inputs[i].focus();
                    }, 0);
                    break;
                }
            }
        }
    }

    clickEnter1(e, index) {
        let newRow = false;
        const cellCurrent = +document.activeElement.getAttribute('tabIndex');
        if (index === this.gOtherVoucherDetails.length - 1) {
            if (112 + 13 * index === cellCurrent) {
                newRow = true;
            }
        }
        if ((this.isEnter && index === this.gOtherVoucherDetails.length - 1) || newRow) {
            this.AddnewRow(e, 0);
            return;
        }
        if (this.isEnter && index < this.gOtherVoucherDetails.length - 1) {
            const inputs = document.getElementsByTagName('input');
            for (let i = 0; i < inputs.length; i++) {
                if (inputs[i].tabIndex === 100 + 13 * (index + 1)) {
                    setTimeout(() => {
                        inputs[i].focus();
                    }, 0);
                    break;
                }
            }
        }
    }

    checkFocus() {
        const inputs = document.getElementsByTagName('input');
        if (this.tabShow === 1) {
            for (let i = 0; i < inputs.length; i++) {
                if (inputs[i].tabIndex === 6) {
                    setTimeout(() => {
                        inputs[i].focus();
                    }, 0);
                    break;
                }
            }
        } else if (this.tabShow === 2) {
            for (let i = 0; i < inputs.length; i++) {
                if (inputs[i].tabIndex === 100) {
                    setTimeout(() => {
                        inputs[i].focus();
                    }, 0);
                    break;
                }
            }
        }
    }

    checkSameOrg() {
        if (
            this.gOtherVoucher.id !== null &&
            this.gOtherVoucher.id !== undefined &&
            this.gOtherVoucher.companyID !== null &&
            this.gOtherVoucher.companyID !== undefined
        ) {
            return this.gOtherVoucher.companyID === this.currentAccount.organizationUnit.id;
        } else {
            return true;
        }
    }

    navigateHelp() {
        this.notificationService.findHelpLink({ id: 'HTCPL' }).subscribe((res: HttpResponse<any>) => {
            window.open(res.body, '_blank');
        });
    }

    exportPDFWebService(valueVoucher?) {
        if (!this.isUseToolIn) {
            this.CTKTExportPDF(false, 1);
        } else {
            const listID = [];
            listID.push(this.gOtherVoucher.id);
            this.inChungTuHangLoatService.inChungTuHangLoat({
                typeGroupID: TYPEGROUP.CHUNG_TU_NGHIEP_VU_KHAC,
                typeReport: valueVoucher,
                listID,
                fromDate: null,
                toDate: null,
                paramCheckAll: false
            });
        }
    }
}
