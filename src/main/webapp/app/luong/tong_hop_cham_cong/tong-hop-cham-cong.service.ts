import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IPSTimeSheetSummary } from 'app/shared/models/ps-time-sheet-summary.model';
import { IPSTimeSheet } from 'app/shared/models/ps-time-sheet.model';

type EntityResponseType = HttpResponse<IPSTimeSheetSummary>;
type EntityArrayResponseType = HttpResponse<IPSTimeSheetSummary[]>;

@Injectable({ providedIn: 'root' })
export class PSTimeSheetSummaryService {
    private resourceUrl = SERVER_API_URL + 'api/p-s-time-sheet-summaries';

    constructor(private http: HttpClient) {}

    create(pSTimeSheetSummary: IPSTimeSheetSummary): Observable<EntityResponseType> {
        return this.http.post<IPSTimeSheetSummary>(this.resourceUrl, pSTimeSheetSummary, { observe: 'response' });
    }

    update(pSTimeSheetSummary: IPSTimeSheetSummary): Observable<EntityResponseType> {
        return this.http.put<IPSTimeSheetSummary>(this.resourceUrl, pSTimeSheetSummary, { observe: 'response' });
    }

    find(id: string): Observable<EntityResponseType> {
        return this.http.get<IPSTimeSheetSummary>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IPSTimeSheetSummary[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: string): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    getPSTimeSheetsSummaries(): Observable<EntityArrayResponseType> {
        return this.http.get<IPSTimeSheetSummary[]>(this.resourceUrl + '/find-all-ps-time-sheets-summaries', {
            observe: 'response'
        });
    }

    multiDelete(obj: IPSTimeSheet[]): Observable<HttpResponse<any>> {
        return this.http.post<any>(`${this.resourceUrl}/multi-delete-ps-time-sheet-summaries`, obj, { observe: 'response' });
    }

    exportPDF(req?: any): Observable<any> {
        let headers = new HttpHeaders();
        const options = createRequestOption(req);
        headers = headers.set('Accept', 'application/x-excel');
        return this.http.get(this.resourceUrl + '/export/pdf', { params: options, observe: 'response', headers, responseType: 'blob' });
    }

    getAllPSTimeSheetsSummaries(): Observable<EntityArrayResponseType> {
        return this.http.get<IPSTimeSheetSummary[]>(`${this.resourceUrl}/get-all-ps-time-sheets-summaries`, {
            observe: 'response'
        });
    }

    exportExcel(req?: any): Observable<any> {
        const options = createRequestOption(req);
        let headers = new HttpHeaders();
        headers = headers.set('Accept', 'application/x-excel');
        return this.http.get(this.resourceUrl + '/export/excel', { params: options, observe: 'response', headers, responseType: 'blob' });
    }

    searchAll(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IPSTimeSheetSummary[]>(`${this.resourceUrl}/search-all`, {
            params: options,
            observe: 'response'
        });
    }

    getIndexRow(req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.get<object>(this.resourceUrl + '/getIndexVoucher', {
            params: options,
            observe: 'response'
        });
    }

    findByRowNum(req?: any): Observable<EntityResponseType> {
        const options = createRequestOption(req);
        return this.http.get<object>(this.resourceUrl + '/findByRowNum', {
            params: options,
            observe: 'response'
        });
    }
}
