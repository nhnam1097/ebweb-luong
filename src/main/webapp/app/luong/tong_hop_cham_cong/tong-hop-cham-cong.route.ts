import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { PSTimeSheetSummaryService } from './tong-hop-cham-cong.service';
import { PSTimeSheetSummaryComponent } from './tong-hop-cham-cong.component';
import { PSTimeSheetSummaryUpdateComponent } from './tong-hop-cham-cong-update.component';
import { PSTimeSheetSummaryDeletePopupComponent } from './tong-hop-cham-cong-delete-dialog.component';
import { ROLE } from 'app/role.constants';
import { IPSTimeSheetSummary, PSTimeSheetSummary } from 'app/shared/models/ps-time-sheet-summary.model';

@Injectable({ providedIn: 'root' })
export class PSTimeSheetSummaryResolve implements Resolve<IPSTimeSheetSummary> {
    constructor(private service: PSTimeSheetSummaryService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((pSTimeSheetSummary: HttpResponse<PSTimeSheetSummary>) => pSTimeSheetSummary.body));
        }
        return of(new PSTimeSheetSummary());
    }
}

export const pSTimeSheetSummaryRoute: Routes = [
    {
        path: '',
        component: PSTimeSheetSummaryComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER', ROLE.TongHopChamCong_XEM],
            defaultSort: 'id,asc',
            pageTitle: 'ebwebApp.pSTimeSheetSummary.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: PSTimeSheetSummaryUpdateComponent,
        resolve: {
            pSTimeSheetSummary: PSTimeSheetSummaryResolve
        },
        data: {
            authorities: ['ROLE_USER', ROLE.TongHopChamCong_Them],
            pageTitle: 'ebwebApp.pSTimeSheetSummary.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: PSTimeSheetSummaryUpdateComponent,
        resolve: {
            pSTimeSheetSummary: PSTimeSheetSummaryResolve
        },
        data: {
            authorities: ['ROLE_USER', ROLE.TongHopChamCong_Sua],
            pageTitle: 'ebwebApp.pSTimeSheetSummary.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const pSTimeSheetSummaryPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: PSTimeSheetSummaryDeletePopupComponent,
        resolve: {
            pSTimeSheetSummary: PSTimeSheetSummaryResolve
        },
        data: {
            authorities: ['ROLE_USER', ROLE.TongHopChamCong_Xoa],
            pageTitle: 'ebwebApp.pSTimeSheetSummary.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
