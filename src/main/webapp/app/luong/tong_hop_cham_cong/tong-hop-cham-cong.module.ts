import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import {
    PSTimeSheetSummaryComponent,
    PSTimeSheetSummaryUpdateComponent,
    PSTimeSheetSummaryDeletePopupComponent,
    PSTimeSheetSummaryDeleteDialogComponent,
    pSTimeSheetSummaryRoute,
    pSTimeSheetSummaryPopupRoute
} from './index';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';
import { CurrencyMaskModule } from 'app/shared/directive/ng2-currency-mask/currency-mask.module';
import { EbVirtualScrollerModule } from 'app/shared/virtual-scroller/virtual-scroller';
import { TestluongSharedModule } from 'app/shared';

const ENTITY_STATES = [...pSTimeSheetSummaryRoute, ...pSTimeSheetSummaryPopupRoute];

@NgModule({
    imports: [TestluongSharedModule, RouterModule.forChild(ENTITY_STATES), EbVirtualScrollerModule, CurrencyMaskModule],
    declarations: [
        PSTimeSheetSummaryComponent,
        PSTimeSheetSummaryUpdateComponent,
        PSTimeSheetSummaryDeleteDialogComponent,
        PSTimeSheetSummaryDeletePopupComponent
    ],
    entryComponents: [
        PSTimeSheetSummaryComponent,
        PSTimeSheetSummaryUpdateComponent,
        PSTimeSheetSummaryDeleteDialogComponent,
        PSTimeSheetSummaryDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class EbwebPSTimeSheetSummaryModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
