import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { PSTimeSheetSummaryService } from './tong-hop-cham-cong.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { IPSTimeSheetSummary } from 'app/shared/models/ps-time-sheet-summary.model';

@Component({
    selector: 'eb-ps-time-sheet-summary-delete-dialog',
    templateUrl: './tong-hop-cham-cong-delete-dialog.component.html'
})
export class PSTimeSheetSummaryDeleteDialogComponent {
    pSTimeSheetSummary: IPSTimeSheetSummary;

    constructor(
        private pSTimeSheetSummaryService: PSTimeSheetSummaryService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager,
        private toastr: ToastrService,
        private translate: TranslateService,
        private router: Router
    ) {}

    clear() {
        window.history.back();
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: string) {
        this.pSTimeSheetSummaryService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'pSTimeSheetSummaryListModification',
                content: 'Deleted an pSTimeSheetSummary'
            });
            this.activeModal.dismiss(true);
        });
        this.toastr.success(this.translate.instant('ebwebApp.pSTimeSheetSummary.deleteSuccessful'));
        this.router.navigate(['ps-time-sheet-summary']);
    }
}

@Component({
    selector: 'eb-ps-time-sheet-summary-delete-popup',
    template: ''
})
export class PSTimeSheetSummaryDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ pSTimeSheetSummary }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(PSTimeSheetSummaryDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.pSTimeSheetSummary = pSTimeSheetSummary;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
