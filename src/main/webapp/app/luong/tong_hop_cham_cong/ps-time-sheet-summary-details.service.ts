import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IPSTimeSheetSummaryDetails } from 'app/shared/models/ps-time-sheet-summary-details.model';
import { IPSTimeSheetSummary } from 'app/shared/models/ps-time-sheet-summary.model';

type EntityResponseType = HttpResponse<IPSTimeSheetSummaryDetails>;
type EntityArrayResponseType = HttpResponse<IPSTimeSheetSummaryDetails[]>;

@Injectable({ providedIn: 'root' })
export class PSTimeSheetSummaryDetailsService {
    private resourceUrl = SERVER_API_URL + 'api/p-s-time-sheet-summary-details';

    constructor(private http: HttpClient) {}

    create(pSTimeSheetSummaryDetails: IPSTimeSheetSummaryDetails): Observable<EntityResponseType> {
        return this.http.post<IPSTimeSheetSummaryDetails>(this.resourceUrl, pSTimeSheetSummaryDetails, { observe: 'response' });
    }

    update(pSTimeSheetSummaryDetails: IPSTimeSheetSummaryDetails): Observable<EntityResponseType> {
        return this.http.put<IPSTimeSheetSummaryDetails>(this.resourceUrl, pSTimeSheetSummaryDetails, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IPSTimeSheetSummaryDetails>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IPSTimeSheetSummaryDetails[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: string): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    getNewPSTimeSheetSummary(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IPSTimeSheetSummary[]>(this.resourceUrl + '/get-new-ps-time-sheet-summary', {
            params: options,
            observe: 'response'
        });
    }

    getNewPSTimeSheetSummaryType2(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IPSTimeSheetSummary[]>(this.resourceUrl + '/get-new-ps-time-sheet-summary-type-2', {
            params: options,
            observe: 'response'
        });
    }

    getNewPSTimeSheetSummaryType1(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IPSTimeSheetSummary[]>(this.resourceUrl + '/get-new-ps-time-sheet-summary-type-1', {
            params: options,
            observe: 'response'
        });
    }

    exportExcel(req: any, typeID: any): Observable<any> {
        let headers = new HttpHeaders();
        const options = createRequestOption(typeID);
        headers = headers.set('Accept', 'application/x-excel');
        return this.http.post(this.resourceUrl + '/export-excel', req, {
            params: options,
            observe: 'response',
            headers,
            responseType: 'blob'
        });
    }
}
