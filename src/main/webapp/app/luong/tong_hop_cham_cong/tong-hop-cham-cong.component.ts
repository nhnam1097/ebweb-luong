import { Component, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { JhiAlertService, JhiEventManager, JhiParseLinks } from 'ng-jhipster';

import { Principal } from 'app/core';

import { DATE_FORMAT, ITEMS_PER_PAGE } from 'app/shared';
import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ROLE } from 'app/role.constants';
import { DatePipe } from '@angular/common';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { BaseComponent } from 'app/shared/base-component/base.component';
import { ebAuth } from 'app/shared/decorator/ebAuth.decorator';
import { PSTimeSheetSummaryService } from 'app/luong/tong_hop_cham_cong/tong-hop-cham-cong.service';
import { PSTimeSheetService } from 'app/luong/cham_cong';
import { DECIMAL, THOUSANDS, TYPEGROUP } from 'app/app.constants';
import * as moment from 'moment';
import { ISearchVoucher } from 'app/shared/models/SearchVoucher';
import { IType } from 'app/shared/models/type.model';
import { IOrganizationUnit } from 'app/shared/models/organization-unit.model';
import { IPSTimeSheet } from 'app/shared/models/ps-time-sheet.model';
import { DataSessionStorage, IDataSessionStorage } from 'app/shared/models/DataSessionStorage';
import { ISessionPSTimeSheet, SessionPSTimeSheet } from 'app/shared/models/session-p-s-time-sheet';
import { OrganizationUnitService } from 'app/shared/services/organization-unit.service';
import { UtilsService } from 'app/shared/UtilsService/Utils.service';
import { RefModalService } from 'app/shared/services/ref-modal.service';
import { TimeSheetSymbolsService } from 'app/shared/services/time-sheet-symbols.service';
import { AccountingObjectService } from 'app/shared/services/accounting-object.service';
import { NotificationService } from 'app/shared/notification';
import { InChungTuHangLoatService } from 'app/shared/in-chung-tu-hang-loat/in-chung-tu-hang-loat.service';
import { EbReportPdfPopupComponent } from 'app/shared/show-pdf/eb-report-pdf-popup.component';
import { IPSTimeSheetSummary } from 'app/shared/models/ps-time-sheet-summary.model';
import { IPSTimeSheetSummaryDetails } from 'app/shared/models/ps-time-sheet-summary-details.model';
import { HandlingResultComponent } from 'app/shared/handling-result/handling-result.component';

@Component({
    selector: 'eb-ps-time-sheet-summary',
    templateUrl: './tong-hop-cham-cong.component.html',
    styleUrls: ['./tong-hop-cham-cong.component.css']
})
export class PSTimeSheetSummaryComponent extends BaseComponent implements OnInit, OnDestroy {
    @ViewChild('popUpMultiDelete') popUpMultiDelete: TemplateRef<any>;
    @ViewChild('contentUnRecord') public modalContentUnRecord: NgbModalRef;
    @ViewChild('contentAddNew') public popUpAddNew: NgbModalRef;
    @ViewChild('downFile') public downFile: NgbModalRef;
    @ViewChild('popUpDelete') popUpDelete: TemplateRef<any>;
    currentAccount: any;
    pSTimeSheetsSummary: IPSTimeSheetSummary[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    modalRef: NgbModalRef;

    isSaving: boolean;
    typeID: number;
    fromDate: any;
    toDate: any;
    recorded: boolean;
    accountingObjectCode: string;
    objSearch: ISearchVoucher;
    pSTimeSheetSummaryDetails: IPSTimeSheetSummaryDetails[];
    types: IType[];
    typeName: string;
    date: any;
    items: any;
    selectedRow: any;
    rowNum: number;
    pageCount: any;
    accountingObjectName: string;
    isShowSearch: boolean;
    searchValue: string;
    options: any;
    organizationUnitss: IOrganizationUnit[];
    index: any;
    currentRow: any;
    listTypePSTimeSheet: any[];
    listMonth: any[];
    listYear: any[];
    month: number;
    year: number;
    typeTimeSheet: number;
    typeCreate: number;
    autoAddNewEmployee: boolean;
    getEmployeeNotActive: boolean;
    lastTimeSheetID: string;
    listLastPSTimeSheetSummaryNotChange: IPSTimeSheetSummary[];
    listLastPSTimeSheetSummary: IPSTimeSheetSummary[];
    listLastPSTimeSheetNotChange: IPSTimeSheet[];
    listLastPSTimeSheet: IPSTimeSheet[];
    pSTimeSheetSummaryName: string;
    pSTimeSheetName: string;
    TYPE_TH_CHAM_CONG_THEO_NGAY = 760;
    TYPE_TH_CHAM_CONG_THEO_GIO = 761;
    TYPE_CHAM_CONG_THEO_NGAY = 750;
    TYPE_CHAM_CONG_THEO_GIO = 751;
    TAO_MOI_HOAN_TOAN = 0;
    DUA_TREN_BANG_THCC = 2;
    DUA_TREN_BANG_CC = 1;
    NHAP_TU_EXCEL = 3;
    isSingleClick: any;
    file: any;
    isLoading: boolean;
    link: any;

    optionsDirective = {
        decimal: DECIMAL,
        thousands: THOUSANDS,
        allowNegative: false,
        precision: 2
    };

    buttonDeleteTranslate = 'ebwebApp.mBDeposit.toolTip.delete';
    buttonAddTranslate = 'ebwebApp.mBDeposit.toolTip.add';
    buttonEditTranslate = 'ebwebApp.mBDeposit.toolTip.edit';
    buttonRecordTranslate = 'ebwebApp.mBDeposit.toolTip.record';
    buttonUnRecordTranslate = 'ebwebApp.mBDeposit.toolTip.unrecord';
    buttonExportTranslate = 'ebwebApp.mBDeposit.toolTip.export';
    buttonSearchTranslate = 'ebwebApp.mBDeposit.toolTip.search';

    // navigate update form
    dataSession: IDataSessionStorage = new DataSessionStorage();

    sessionPSTimeSheet: ISessionPSTimeSheet = new SessionPSTimeSheet();
    ROLE_XEM = ROLE.TongHopChamCong_XEM;
    ROLE_THEM = ROLE.TongHopChamCong_Them;
    ROLE_SUA = ROLE.TongHopChamCong_Sua;
    ROLE_XOA = ROLE.TongHopChamCong_Xoa;
    ROLE_IN = ROLE.TongHopChamCong_In;
    ROLE_KETXUAT = ROLE.TongHopChamCong_KetXuat;
    ngayHachToan: any;

    constructor(
        private pSTimeSheetSummaryService: PSTimeSheetSummaryService,
        private parseLinks: JhiParseLinks,
        private jhiAlertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        public utilsService: UtilsService,
        private toastr: ToastrService,
        public translate: TranslateService,
        private organizationUnitService: OrganizationUnitService,
        private refModalService: RefModalService,
        private modalService: NgbModal,
        public activeModal: NgbActiveModal,
        private psTimeSheetSummaryService: PSTimeSheetSummaryService,
        private accService: AccountingObjectService,
        private psTimeSheetService: PSTimeSheetService,
        private notificationService: NotificationService,
        private inChungTuHangLoatService: InChungTuHangLoatService,
        private toastrService: ToastrService,
        private translateService: TranslateService
    ) {
        super();
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe(data => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
        });
        // this.accService.getAllDTO().subscribe((res: HttpResponse<IAccountingObject[]>) => {
        //     this.accounting = res.body;
        //     this.accountingObjects = res.body;
        // });
        this.translate
            .get([
                'ebwebApp.pSTimeSheetSummary.dayTimeSheet',
                'ebwebApp.pSTimeSheetSummary.hourTimeSheet',
                'ebwebApp.pSTimeSheet.month',
                'ebwebApp.pSTimeSheetSummary.dayTimeSheetText',
                'ebwebApp.pSTimeSheetSummary.hourTimeSheetText'
            ])
            .subscribe(res => {
                this.listTypePSTimeSheet = [
                    {
                        value: this.TYPE_TH_CHAM_CONG_THEO_NGAY,
                        name: res['ebwebApp.pSTimeSheetSummary.dayTimeSheet'],
                        text: res['ebwebApp.pSTimeSheetSummary.dayTimeSheetText']
                    },
                    {
                        value: this.TYPE_TH_CHAM_CONG_THEO_GIO,
                        name: res['ebwebApp.pSTimeSheetSummary.hourTimeSheet'],
                        text: res['ebwebApp.pSTimeSheetSummary.hourTimeSheetText']
                    }
                ];
                this.listMonth = [
                    { value: 1, name: res['ebwebApp.pSTimeSheet.month'] + ' 1' },
                    { value: 2, name: res['ebwebApp.pSTimeSheet.month'] + ' 2' },
                    { value: 3, name: res['ebwebApp.pSTimeSheet.month'] + ' 3' },
                    { value: 4, name: res['ebwebApp.pSTimeSheet.month'] + ' 4' },
                    { value: 5, name: res['ebwebApp.pSTimeSheet.month'] + ' 5' },
                    { value: 6, name: res['ebwebApp.pSTimeSheet.month'] + ' 6' },
                    { value: 7, name: res['ebwebApp.pSTimeSheet.month'] + ' 7' },
                    { value: 8, name: res['ebwebApp.pSTimeSheet.month'] + ' 8' },
                    { value: 9, name: res['ebwebApp.pSTimeSheet.month'] + ' 9' },
                    { value: 10, name: res['ebwebApp.pSTimeSheet.month'] + ' 10' },
                    { value: 11, name: res['ebwebApp.pSTimeSheet.month'] + ' 11' },
                    { value: 12, name: res['ebwebApp.pSTimeSheet.month'] + ' 12' }
                ];
                this.listYear = [];
                for (let i = 1950; i <= 2050; i++) {
                    this.listYear.push({ value: i });
                }
                const currentDate = new Date();
                this.month = currentDate.getMonth() + 1;
                this.year = currentDate.getFullYear();
                this.typeID = this.TYPE_TH_CHAM_CONG_THEO_NGAY;
                this.typeCreate = this.TAO_MOI_HOAN_TOAN;
                this.pSTimeSheetSummaryName =
                    'Bảng ' +
                    (this.typeID === this.TYPE_TH_CHAM_CONG_THEO_NGAY
                        ? this.listTypePSTimeSheet[0].text
                        : this.listTypePSTimeSheet[1].text) +
                    ' ' +
                    this.translate.instant('ebwebApp.pSTimeSheet.month') +
                    ' ' +
                    this.month +
                    ' ' +
                    this.translate.instant('ebwebApp.pSTimeSheet.year') +
                    ' ' +
                    this.year;
            });
    }

    unCheckAll() {
        this.organizationUnitss.forEach(item => (item.checked = false));
    }

    loadAll() {
        if (sessionStorage.getItem('dataSession')) {
            this.dataSession = JSON.parse(sessionStorage.getItem('dataSession'));
            this.page = this.dataSession.page;
            this.itemsPerPage = this.dataSession.itemsPerPage;
            this.isShowSearch = this.dataSession.isShowSearch;
            this.predicate = this.dataSession.predicate;
            this.reverse = this.dataSession.reverse;
            this.previousPage = this.dataSession.page;
            sessionStorage.removeItem('dataSession');
        }
        if (this.activatedRoute.snapshot.paramMap.has('isSearch')) {
            this.pSTimeSheetSummaryService
                .searchAll({
                    page: this.page - 1,
                    size: this.itemsPerPage,
                    sort: this.sort(),
                    searchVoucher: this.dataSession.searchVoucher
                })
                .subscribe(
                    (res: HttpResponse<IPSTimeSheetSummary[]>) => this.paginatePSTimeSheetSummaries(res.body, res.headers),
                    (res: HttpErrorResponse) => this.onError(res.message)
                );
            return;
        } else {
            this.router.navigate(['/tong-hop-cham-cong']);
        }
        this.pSTimeSheetSummaryService
            .searchAll({
                page: this.page - 1,
                size: this.itemsPerPage,
                sort: this.sort(),
                searchVoucher: JSON.stringify(this.saveSearchVoucher())
            })
            .subscribe(
                (res: HttpResponse<IPSTimeSheetSummary[]>) => this.paginatePSTimeSheetSummaries(res.body, res.headers),
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    loadPage(page: number) {
        this.selectedRows = [];
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    getRowNumberOfRecord(page: number, index: number): number {
        if (page > 0 && index !== -1) {
            return (page - 1) * this.itemsPerPage + index + 1;
        }
    }

    transition() {
        this.dataSession.rowNum = 0;
        this.router.navigate(['/tong-hop-cham-cong']);
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.router.navigate(['/tong-hop-cham-cong']);
        this.loadAll();
    }

    ngOnInit() {
        this.listLastPSTimeSheet = [];
        this.listLastPSTimeSheetSummary = [];
        this.objSearch = new class implements ISearchVoucher {
            accountingObjectID: string;
            currencyID: string;
            fromDate: moment.Moment;
            statusRecorded: boolean;
            textSearch: string;
            toDate: moment.Moment;
            typeID: number;
        }();
        if (sessionStorage.getItem('checkNewTongHopChamCong')) {
            if (this.modalRef) {
                this.modalRef.close();
            }
            this.organizationUnitService.getOrgForSalary().subscribe((resOrgForSalary: HttpResponse<IOrganizationUnit[]>) => {
                this.organizationUnitss = resOrgForSalary.body.sort((a, b) => a.organizationUnitCode.localeCompare(b.organizationUnitCode));
                this.unCheckAll();
                this.psTimeSheetSummaryService.getPSTimeSheetsSummaries().subscribe((res: HttpResponse<IPSTimeSheetSummary[]>) => {
                    this.listLastPSTimeSheetSummary = res.body.filter(a => a.typeID === this.TYPE_TH_CHAM_CONG_THEO_NGAY);
                    for (let i = 0; i < this.listLastPSTimeSheetSummary.length; i++) {
                        if (this.listLastPSTimeSheetSummary[i].typeLedger === 0) {
                            this.listLastPSTimeSheetSummary[i].typeLedgerStr = 'Sổ tài chính';
                        } else if (this.listLastPSTimeSheetSummary[i].typeLedger === 1) {
                            this.listLastPSTimeSheetSummary[i].typeLedgerStr = 'Sổ quản trị';
                        }
                    }
                    this.listLastPSTimeSheetSummaryNotChange = res.body;
                    for (let i = 0; i < this.listLastPSTimeSheetSummaryNotChange.length; i++) {
                        if (this.listLastPSTimeSheetSummaryNotChange[i].typeLedger === 0) {
                            this.listLastPSTimeSheetSummaryNotChange[i].typeLedgerStr = 'Sổ tài chính';
                        } else if (this.listLastPSTimeSheetSummaryNotChange[i].typeLedger === 1) {
                            this.listLastPSTimeSheetSummaryNotChange[i].typeLedgerStr = 'Sổ quản trị';
                        }
                    }
                });
                this.psTimeSheetService.getPSTimeSheets().subscribe((res: HttpResponse<IPSTimeSheet[]>) => {
                    this.listLastPSTimeSheet = res.body.filter(a => a.typeID === this.TYPE_CHAM_CONG_THEO_NGAY);
                    for (let i = 0; i < this.listLastPSTimeSheet.length; i++) {
                        if (this.listLastPSTimeSheet[i].typeLedger === 0) {
                            this.listLastPSTimeSheet[i].typeLedgerStr = 'Sổ tài chính';
                        } else if (this.listLastPSTimeSheet[i].typeLedger === 1) {
                            this.listLastPSTimeSheet[i].typeLedgerStr = 'Sổ quản trị';
                        }
                    }
                    this.listLastPSTimeSheetNotChange = res.body;
                    for (let i = 0; i < this.listLastPSTimeSheetNotChange.length; i++) {
                        if (this.listLastPSTimeSheetNotChange[i].typeLedger === 0) {
                            this.listLastPSTimeSheetNotChange[i].typeLedgerStr = 'Sổ tài chính';
                        } else if (this.listLastPSTimeSheetNotChange[i].typeLedger === 1) {
                            this.listLastPSTimeSheetNotChange[i].typeLedgerStr = 'Sổ quản trị';
                        }
                    }
                });
                this.typeCreate = this.TAO_MOI_HOAN_TOAN;
                this.modalRef = this.modalService.open(this.popUpAddNew, { backdrop: 'static', size: 'lg' });
                sessionStorage.removeItem('checkNewTongHopChamCong');
            });
        }
        this.principal.identity().then(account => {
            this.isUseToolIn = this.utilsService.checkUseToolIn(account);
            this.currentAccount = account;
            this.ngayHachToan = this.utilsService.getPostedDateDefault(this.currentAccount);
            const dateSearch = this.utilsService.getDataYearWork(this.currentAccount);
            if (dateSearch.fromDate !== '') {
                this.objSearch.fromDate = dateSearch.fromDate.format(DATE_FORMAT);
            }
            if (dateSearch.toDate !== '') {
                this.objSearch.toDate = dateSearch.toDate.format(DATE_FORMAT);
            }
            this.loadAll();
        });
        this.psTimeSheetSummaryService.getPSTimeSheetsSummaries().subscribe((res: HttpResponse<IPSTimeSheetSummary[]>) => {
            this.listLastPSTimeSheetSummary = res.body.filter(a => a.typeID === this.TYPE_TH_CHAM_CONG_THEO_NGAY);
            for (let i = 0; i < this.listLastPSTimeSheetSummary.length; i++) {
                if (this.listLastPSTimeSheetSummary[i].typeLedger === 0) {
                    this.listLastPSTimeSheetSummary[i].typeLedgerStr = 'Sổ tài chính';
                } else if (this.listLastPSTimeSheetSummary[i].typeLedger === 1) {
                    this.listLastPSTimeSheetSummary[i].typeLedgerStr = 'Sổ quản trị';
                }
            }
            this.listLastPSTimeSheetSummaryNotChange = res.body;
            for (let i = 0; i < this.listLastPSTimeSheetSummaryNotChange.length; i++) {
                if (this.listLastPSTimeSheetSummaryNotChange[i].typeLedger === 0) {
                    this.listLastPSTimeSheetSummaryNotChange[i].typeLedgerStr = 'Sổ tài chính';
                } else if (this.listLastPSTimeSheetSummaryNotChange[i].typeLedger === 1) {
                    this.listLastPSTimeSheetSummaryNotChange[i].typeLedgerStr = 'Sổ quản trị';
                }
            }
        });
        this.psTimeSheetService.getPSTimeSheets().subscribe((res: HttpResponse<IPSTimeSheet[]>) => {
            this.listLastPSTimeSheet = res.body.filter(a => a.typeID === this.TYPE_CHAM_CONG_THEO_NGAY);
            for (let i = 0; i < this.listLastPSTimeSheet.length; i++) {
                if (this.listLastPSTimeSheet[i].typeLedger === 0) {
                    this.listLastPSTimeSheet[i].typeLedgerStr = 'Sổ tài chính';
                } else if (this.listLastPSTimeSheet[i].typeLedger === 1) {
                    this.listLastPSTimeSheet[i].typeLedgerStr = 'Sổ quản trị';
                }
            }
            this.listLastPSTimeSheetNotChange = res.body;
            for (let i = 0; i < this.listLastPSTimeSheetNotChange.length; i++) {
                if (this.listLastPSTimeSheetNotChange[i].typeLedger === 0) {
                    this.listLastPSTimeSheetNotChange[i].typeLedgerStr = 'Sổ tài chính';
                } else if (this.listLastPSTimeSheetNotChange[i].typeLedger === 1) {
                    this.listLastPSTimeSheetNotChange[i].typeLedgerStr = 'Sổ quản trị';
                }
            }
        });
        this.eventSubscriber = this.eventManager.subscribe('inpopup_luong', res => {
            this.inPopup = res.content;
        });
        this.eventSubscribers.push(this.eventSubscriber);
        this.eventSubscriber = this.eventManager.subscribe('closepopup', res => {
            this.inPopup = res.content;
        });
        this.eventSubscribers.push(this.eventSubscriber);
        this.registerChangeInPSTimeSheetSummaries();
        this.registerExport();
    }

    registerChangeInPSTimeSheetSummaries() {
        this.eventSubscriber = this.eventManager.subscribe('pSTimeSheetSummaryListModification', response => this.loadAll());
    }

    selectedItemPerPage() {
        this.loadAll();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IPSTimeSheetSummary) {
        return item.id;
    }

    sort() {
        const result = ['date' + ',' + (this.reverse ? 'desc' : 'asc')];
        result.push('postedDate' + ',' + (this.reverse ? 'desc' : 'asc'));
        return result;
    }

    private paginatePSTimeSheetSummaries(data: IPSTimeSheetSummary[], headers: HttpHeaders) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = parseInt(headers.get('X-Total-Count'), 10);
        this.queryCount = this.totalItems;
        this.pSTimeSheetsSummary = data;
        this.objects = data;
        if (this.page > 1 && this.pSTimeSheetsSummary && this.pSTimeSheetsSummary.length === 0) {
            this.page = this.page - 1;
            this.loadAll();
            return;
        }

        if (this.objects && this.objects.length > 0) {
            if (this.dataSession && this.dataSession.rowNum) {
                this.selectedRows.push(this.pSTimeSheetsSummary[this.dataSession.rowNum]);
                this.selectedRow = this.pSTimeSheetsSummary[this.dataSession.rowNum];
            } else {
                this.selectedRows.push(this.pSTimeSheetsSummary[0]);
                this.selectedRow = this.pSTimeSheetsSummary[0];
            }
        } else {
            this.selectedRow = null;
            this.pSTimeSheetSummaryDetails = null;
        }
        this.onSelect(this.selectedRow);
        //  display pageCount
        this.pageCount = Math.ceil(this.totalItems / this.itemsPerPage);
    }

    private onError(errorMessage: string) {
        this.toastr.error(this.translate.instant('ebwebApp.mBDeposit.error'), this.translate.instant('ebwebApp.mBDeposit.message'));
    }

    onSelect(select: IPSTimeSheetSummary, data = null) {
        // this.checkSameOrg();
        this.isSingleClick = true;
        setTimeout(() => {
            if (this.isSingleClick) {
                if (select) {
                    this.selectedRow = select;
                    this.psTimeSheetSummaryService.find(select.id).subscribe((res: HttpResponse<IPSTimeSheetSummary>) => {
                        this.pSTimeSheetSummaryDetails =
                            res.body.pSTimeSheetSummaryDetails === undefined
                                ? []
                                : res.body.pSTimeSheetSummaryDetails.sort((a, b) => a.orderPriority - b.orderPriority);
                        this.lstDataForSetDefault = [];
                        this.lstDataForSetDefault.push(this.pSTimeSheetSummaryDetails);
                        this.loadDataDefaultForOpenVoucher(this.lstDataForSetDefault);
                    });
                }
            }
        }, 250);
    }

    get pSTimeSheet() {
        return this.selectedRow;
    }

    set pSTimeSheet(pSTimeSheet: IPSTimeSheetSummary) {
        this.selectedRow = pSTimeSheet;
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IPSTimeSheetSummary>>) {
        result.subscribe((res: HttpResponse<IPSTimeSheetSummary>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
    }

    private onSaveError() {
        this.isSaving = false;
    }

    doubleClickRow(any?) {
        this.isSingleClick = false;
        this.edit();
    }

    @ebAuth(['ROLE_ADMIN', ROLE.TongHopChamCong_XEM])
    edit() {
        event.preventDefault();
        this.dataSession.page = this.page;
        this.dataSession.itemsPerPage = this.itemsPerPage;
        this.dataSession.pageCount = this.pageCount;
        this.dataSession.totalItems = this.totalItems;
        this.dataSession.rowNum = this.pSTimeSheetsSummary.indexOf(this.selectedRow);
        this.dataSession.isShowSearch = this.isShowSearch;
        // sort
        this.dataSession.predicate = this.predicate;
        this.dataSession.accountingObjectName = this.accountingObjectName;
        this.dataSession.reverse = this.reverse;
        this.sessionPSTimeSheet.month = this.month;
        this.sessionPSTimeSheet.year = this.year;
        this.sessionPSTimeSheet.typeID = this.typeID;
        this.sessionPSTimeSheet.pSTimeSheetName = this.pSTimeSheetName;
        this.sessionPSTimeSheet.pSTimeSheetSummaryName = this.pSTimeSheetSummaryName;
        this.sessionPSTimeSheet.typeCreate = this.typeCreate;
        this.sessionPSTimeSheet.lastPSTimeSheetID = this.lastTimeSheetID;
        this.sessionPSTimeSheet.autoAddNewEmployee = this.autoAddNewEmployee;
        this.sessionPSTimeSheet.getEmployeeNotActive = this.getEmployeeNotActive;
        sessionStorage.setItem('dataSession', JSON.stringify(this.dataSession));
        sessionStorage.setItem('sessionPSTimeSheetSummary', JSON.stringify(this.sessionPSTimeSheet));
        this.router.navigate(['./tong-hop-cham-cong', this.selectedRow.id, 'edit']);
        // }
    }

    showPopupAddNew() {
        event.preventDefault();
        if (this.typeCreate === this.NHAP_TU_EXCEL) {
            this.upload(this.downFile);
            return;
        }
        this.dataSession.page = this.page;
        this.dataSession.itemsPerPage = this.itemsPerPage;
        this.dataSession.pageCount = this.pageCount;
        this.dataSession.totalItems = this.totalItems;
        this.dataSession.rowNum = this.rowNum;
        // sort
        this.dataSession.predicate = this.predicate;
        this.dataSession.accountingObjectName = this.accountingObjectName;
        this.dataSession.reverse = this.reverse;
        this.sessionPSTimeSheet.month = this.month;
        this.sessionPSTimeSheet.year = this.year;
        this.sessionPSTimeSheet.typeID = this.typeID;
        this.sessionPSTimeSheet.pSTimeSheetName = this.pSTimeSheetName;
        this.sessionPSTimeSheet.pSTimeSheetSummaryName = this.pSTimeSheetSummaryName;
        this.sessionPSTimeSheet.typeCreate = this.typeCreate;
        this.sessionPSTimeSheet.lastPSTimeSheetID = this.lastTimeSheetID;
        this.sessionPSTimeSheet.lastPSTimeSheetSummaryID = this.lastTimeSheetID;
        this.sessionPSTimeSheet.autoAddNewEmployee = this.autoAddNewEmployee ? this.autoAddNewEmployee : false;
        this.sessionPSTimeSheet.getEmployeeNotActive = this.getEmployeeNotActive ? this.getEmployeeNotActive : false;
        this.sessionPSTimeSheet.listDepartmentID = this.organizationUnitss.filter(n => n.checked).map(n => n.id);
        if (!this.sessionPSTimeSheet.pSTimeSheetSummaryName) {
            this.toastr.error(
                this.translate.instant('ebwebApp.pSTimeSheetSummary.pSTimeSheetSummaryNameNotBeBlank'),
                this.translate.instant('ebwebApp.pSTimeSheet.message')
            );
            return;
        }
        if (!this.sessionPSTimeSheet.month) {
            this.toastr.error(
                this.translate.instant('ebwebApp.pSTimeSheet.monthNotBeBlank'),
                this.translate.instant('ebwebApp.pSTimeSheet.message')
            );
            return;
        }
        if (!this.sessionPSTimeSheet.year) {
            this.toastr.error(
                this.translate.instant('ebwebApp.pSTimeSheet.yearNotBeBlank'),
                this.translate.instant('ebwebApp.pSTimeSheet.message')
            );
            return;
        }
        if (this.typeCreate === this.DUA_TREN_BANG_THCC || this.typeCreate === this.DUA_TREN_BANG_CC) {
            if (!this.sessionPSTimeSheet.lastPSTimeSheetID) {
                if (this.typeCreate === this.DUA_TREN_BANG_THCC) {
                    this.toastr.error(
                        this.translate.instant('ebwebApp.pSTimeSheetSummary.lastPSTimeSheetSummaryIDNotBeBlank'),
                        this.translate.instant('ebwebApp.pSTimeSheet.message')
                    );
                } else {
                    this.toastr.error(
                        this.translate.instant('ebwebApp.pSTimeSheet.lastPSTimeSheetIDNotBeBlank'),
                        this.translate.instant('ebwebApp.pSTimeSheet.message')
                    );
                }
                return;
            }
        }
        // let lastDay = new Date(this.sessionPSTimeSheet.year, this.sessionPSTimeSheet.month + 1, 0);
        // if (this.checkCloseBook(this.currentAccount, lastDay)) {
        //     this.toastr.error(
        //         this.translate.instant('ebwebApp.pSTimeSheetSummary.isHasCloseBook'),
        //         this.translate.instant('ebwebApp.pSTimeSheet.message')
        //     );
        //     return;
        // }
        // if (this.sessionPSTimeSheet.listDepartmentID === null || this.sessionPSTimeSheet.listDepartmentID.length === 0) {
        //     this.toastr.error(
        //         this.translate.instant('ebwebApp.pSTimeSheet.departmentIDNotBeBlank'),
        //         this.translate.instant('ebwebApp.pSTimeSheet.message')
        //     );
        //     return;
        // }
        if (this.modalRef) {
            this.modalRef.close();
        }
        sessionStorage.setItem('sessionPSTimeSheetSummary', JSON.stringify(this.sessionPSTimeSheet));
        this.router.navigate(['tong-hop-cham-cong/', 'new']);
    }

    @ebAuth(['ROLE_ADMIN', ROLE.TongHopChamCong_Xoa])
    delete() {
        event.preventDefault();
        if (this.selectedRow) {
            if (this.selectedRows.length > 1) {
                this.modalRef = this.modalService.open(this.popUpMultiDelete, { backdrop: 'static' });
            } else if (this.selectedRows && this.selectedRows.length === 1) {
                this.modalRef = this.modalService.open(this.popUpDelete, { backdrop: 'static' });
            }
        }
    }

    continueDelete() {
        this.pSTimeSheetSummaryService.multiDelete(this.selectedRows).subscribe(
            (res: HttpResponse<any>) => {
                if (this.modalRef) {
                    this.modalRef.close();
                }
                this.selectedRows = [];
                this.loadAll();
                if (res.body.countTotalVouchers !== res.body.countSuccessVouchers) {
                    this.modalRef = this.refModalService.open(
                        res.body,
                        HandlingResultComponent,
                        null,
                        false,
                        null,
                        null,
                        null,
                        null,
                        null,
                        true
                    );
                } else {
                    this.toastr.success(this.translate.instant('ebwebApp.pSTimeSheetSummary.deleteSuccessful'));
                }
                this.activeModal.close();
                // }
            },
            (res: HttpErrorResponse) => {
                if (res.error.errorKey === 'errorDeleteList') {
                    this.toastr.error(
                        this.translate.instant('ebwebApp.mBDeposit.errorDeleteVoucherNo'),
                        this.translate.instant('ebwebApp.mBDeposit.message')
                    );
                }
                if (this.modalRef) {
                    this.modalRef.close();
                }
            }
        );
    }

    closePopUpDelete() {
        if (this.modalRef) {
            this.modalRef.close();
        }
    }

    getAccountingObjectbyID(id) {
        if (this.accountingObjects) {
            const acc = this.accountingObjects.find(n => n.id === id);
            if (acc) {
                return acc.accountingObjectCode;
            }
        }
    }

    getOrganizationUnitbyID(id) {
        if (this.organizationUnits) {
            const organizationUnit = this.organizationUnits.find(n => n.id === id);
            if (organizationUnit) {
                return organizationUnit.organizationUnitCode;
            }
        }
    }

    registerExport() {
        this.eventSubscriber = this.eventManager.subscribe(`export-excel-${this.TYPE_TH_CHAM_CONG_THEO_NGAY}`, () => {
            this.exportExcel();
        });
    }

    exportExcel() {
        this.pSTimeSheetSummaryService.exportExcel({ searchVoucher: JSON.stringify(this.saveSearchVoucher()) }).subscribe(
            res => {
                const blob = new Blob([res.body], { type: 'application/vnd.ms-excel' });
                const fileURL = URL.createObjectURL(blob);

                const link = document.createElement('a');
                document.body.appendChild(link);
                link.download = fileURL;
                link.setAttribute('style', 'display: none');
                const name = 'DS_TongHopChamCong.xls';
                link.setAttribute('download', name);
                link.href = fileURL;
                link.click();
            },
            () => {}
        );
    }

    getTypeName(typeID) {
        if (typeID && this.types) {
            const currentType = this.types.find(type => type.id === typeID);
            if (currentType) {
                return currentType.typeName;
            } else {
                return '';
            }
        }
    }

    getEmployeeByID(id) {
        if (this.employees) {
            const epl = this.employees.find(n => n.id === id);
            if (epl) {
                return epl.accountingObjectCode;
            } else {
                return '';
            }
        }
    }

    getEmployeeName(id) {
        if (this.employees) {
            const epl = this.employees.find(n => n.id === id);
            if (epl) {
                return epl.accountingObjectName;
            } else {
                return '';
            }
        }
    }

    newArr(lenght: number): any[] {
        if (lenght > 0) {
            return new Array(lenght);
        } else {
            return new Array(0);
        }
    }

    @ebAuth(['ROLE_ADMIN', ROLE.TongHopChamCong_Them])
    addNew(isNew = false) {
        if (this.modalRef) {
            this.modalRef.close();
        }
        this.organizationUnitService.getOrgForSalary().subscribe((resOrgForSalary: HttpResponse<IOrganizationUnit[]>) => {
            this.organizationUnitss = resOrgForSalary.body.sort((a, b) => a.organizationUnitCode.localeCompare(b.organizationUnitCode));
            this.unCheckAll();
            this.psTimeSheetSummaryService.getPSTimeSheetsSummaries().subscribe((res: HttpResponse<IPSTimeSheetSummary[]>) => {
                this.listLastPSTimeSheetSummary = res.body.filter(a => a.typeID === this.TYPE_TH_CHAM_CONG_THEO_NGAY);
                for (let i = 0; i < this.listLastPSTimeSheetSummary.length; i++) {
                    if (this.listLastPSTimeSheetSummary[i].typeLedger === 0) {
                        this.listLastPSTimeSheetSummary[i].typeLedgerStr = 'Sổ tài chính';
                    } else if (this.listLastPSTimeSheetSummary[i].typeLedger === 1) {
                        this.listLastPSTimeSheetSummary[i].typeLedgerStr = 'Sổ quản trị';
                    }
                }
                this.listLastPSTimeSheetSummaryNotChange = res.body;
                for (let i = 0; i < this.listLastPSTimeSheetSummaryNotChange.length; i++) {
                    if (this.listLastPSTimeSheetSummaryNotChange[i].typeLedger === 0) {
                        this.listLastPSTimeSheetSummaryNotChange[i].typeLedgerStr = 'Sổ tài chính';
                    } else if (this.listLastPSTimeSheetSummaryNotChange[i].typeLedger === 1) {
                        this.listLastPSTimeSheetSummaryNotChange[i].typeLedgerStr = 'Sổ quản trị';
                    }
                }
            });
            this.psTimeSheetService.getPSTimeSheets().subscribe((res: HttpResponse<IPSTimeSheet[]>) => {
                this.listLastPSTimeSheet = res.body.filter(a => a.typeID === this.TYPE_CHAM_CONG_THEO_NGAY);
                for (let i = 0; i < this.listLastPSTimeSheet.length; i++) {
                    if (this.listLastPSTimeSheet[i].typeLedger === 0) {
                        this.listLastPSTimeSheet[i].typeLedgerStr = 'Sổ tài chính';
                    } else if (this.listLastPSTimeSheet[i].typeLedger === 1) {
                        this.listLastPSTimeSheet[i].typeLedgerStr = 'Sổ quản trị';
                    }
                }
                this.listLastPSTimeSheetNotChange = res.body;
                for (let i = 0; i < this.listLastPSTimeSheetNotChange.length; i++) {
                    if (this.listLastPSTimeSheetNotChange[i].typeLedger === 0) {
                        this.listLastPSTimeSheetNotChange[i].typeLedgerStr = 'Sổ tài chính';
                    } else if (this.listLastPSTimeSheetNotChange[i].typeLedger === 1) {
                        this.listLastPSTimeSheetNotChange[i].typeLedgerStr = 'Sổ quản trị';
                    }
                }
            });
            this.typeCreate = this.TAO_MOI_HOAN_TOAN;
            this.inPopup = true;
            this.month = this.ngayHachToan.month() + 1;
            this.year = this.ngayHachToan.year();
            this.selectChangePSSheetName();
            this.modalRef = this.modalService.open(this.popUpAddNew, { backdrop: 'static', size: 'lg' });
            document.getElementById('field_accountingObjectType').focus();
        });
        // this.router.navigate(['/tong-hop-cham-cong/new']);
    }

    isCheckAll() {
        if (this.organizationUnitss) {
            return this.organizationUnitss.every(item => item.checked) && this.organizationUnitss.length;
        } else {
            return false;
        }
    }

    checkAll() {
        const isCheck = this.organizationUnitss.every(item => item.checked) && this.organizationUnitss.length;
        this.organizationUnitss.forEach(item => (item.checked = !isCheck));
    }

    check(organizationUnit: IOrganizationUnit) {
        organizationUnit.checked = !organizationUnit.checked;
    }

    changeCheckBoxAddNewEmployee() {
        this.autoAddNewEmployee = !this.autoAddNewEmployee;
    }

    changeCheckBoxGetEmployeeNotActive() {
        this.getEmployeeNotActive = !this.getEmployeeNotActive;
    }

    selectChangePSSheetName() {
        this.listLastPSTimeSheetSummary = this.listLastPSTimeSheetSummaryNotChange.filter(a => a.typeID === this.typeID);
        if (this.typeID === this.TYPE_TH_CHAM_CONG_THEO_NGAY) {
            this.listLastPSTimeSheet = this.listLastPSTimeSheetNotChange.filter(a => a.typeID === this.TYPE_CHAM_CONG_THEO_NGAY);
        } else {
            this.listLastPSTimeSheet = this.listLastPSTimeSheetNotChange.filter(a => a.typeID === this.TYPE_CHAM_CONG_THEO_GIO);
        }
        this.pSTimeSheetSummaryName =
            'Bảng ' +
            (this.typeID === this.TYPE_TH_CHAM_CONG_THEO_NGAY ? this.listTypePSTimeSheet[0].text : this.listTypePSTimeSheet[1].text) +
            ' ' +
            this.translate.instant('ebwebApp.pSTimeSheet.month') +
            ' ' +
            (this.month ? this.month : '') +
            ' ' +
            this.translate.instant('ebwebApp.pSTimeSheet.year') +
            ' ' +
            (this.year ? this.year : '');
    }

    getTypeTimeSheet(typeID) {
        if (typeID && typeID === this.TYPE_TH_CHAM_CONG_THEO_NGAY) {
            return this.listTypePSTimeSheet ? this.listTypePSTimeSheet[0].name : null;
        } else {
            return this.listTypePSTimeSheet ? this.listTypePSTimeSheet[1].name : null;
        }
    }

    getDepartmentID(id) {
        if (this.organizationUnits && this.organizationUnits.length > 0) {
            const department = this.organizationUnits.find(a => a.id === id);
            if (department) {
                return department.organizationUnitCode;
            }
        }
    }

    checkIsSunday(day) {
        if (this.selectedRow) {
            const today = new Date(this.selectedRow.year, this.selectedRow.month - 1, day);
            if (today.getDay() === 0 || today.getDay() === 6) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    changeTypeCreate() {
        if (this.typeCreate === this.TAO_MOI_HOAN_TOAN) {
            this.autoAddNewEmployee = false;
            this.getEmployeeNotActive = false;
            this.lastTimeSheetID = null;
        }
    }

    @ebAuth(['ROLE_ADMIN', ROLE.TongHopChamCong_KetXuat])
    export() {
        event.preventDefault();
        if (this.pSTimeSheetsSummary.length > 0) {
            this.psTimeSheetSummaryService.exportPDF({ searchVoucher: JSON.stringify(this.saveSearchVoucher()) }).subscribe(res => {
                this.refModalService.open(null, EbReportPdfPopupComponent, res, false, this.TYPE_TH_CHAM_CONG_THEO_NGAY);
            });
        }
    }

    sumFooter(item) {
        if (this.selectedRow && this.pSTimeSheetSummaryDetails && this.pSTimeSheetSummaryDetails.length > 0) {
            let total = 0;
            for (let i = 0; i < this.pSTimeSheetSummaryDetails.length; i++) {
                total += this.pSTimeSheetSummaryDetails[i][item];
            }
            return total;
        }
        return '';
    }

    continueDeletePSTimeSheetSummary() {
        this.pSTimeSheetSummaryService.delete(this.selectedRow.id).subscribe(response => {
            this.modalRef.close();
            this.toastr.success(this.translate.instant('ebwebApp.pSTimeSheetSummary.deleteSuccessful'));
            this.loadAll();
        });
    }

    toggleSearch($event) {
        $event.preventDefault();
        this.isShowSearch = !this.isShowSearch;
        if (this.isShowSearch) {
            this.inPopup = true;
            this.isSearching = true;
            setTimeout(() => {
                document.getElementById('inputFirst').focus();
            }, 0);
        } else {
            this.inPopup = false;
            this.isSearching = false;
        }
    }

    search() {
        this.selectedRows = [];
        if (this.objSearch.fromDate > this.objSearch.toDate) {
            this.toastr.error(
                this.translate.instant('ebwebApp.mBDeposit.fromDateMustBeLessThanToDate'),
                this.translate.instant('ebwebApp.mBDeposit.message')
            );
            return;
        }
        sessionStorage.setItem('dataSearchPSTimeSheet', JSON.stringify(this.saveSearchVoucher()));
        this.pSTimeSheetSummaryService
            .searchAll({
                page: this.page - 1,
                size: this.itemsPerPage,
                searchVoucher: JSON.stringify(this.saveSearchVoucher())
            })
            .subscribe(
                (res: HttpResponse<IPSTimeSheetSummary[]>) => this.paginatePSTimeSheetSummaries(res.body, res.headers),
                (res: HttpErrorResponse) => this.onError(res.message)
            );
        this.page = 1;
        this.loadPage(this.page);
    }

    saveSearchVoucher(): ISearchVoucher {
        const searchObj: ISearchVoucher = {};
        searchObj.typeID = this.objSearch.typeID !== undefined ? this.objSearch.typeID : null;
        searchObj.statusRecorded = this.objSearch.statusRecorded !== undefined ? this.objSearch.statusRecorded : null;
        searchObj.currencyID = this.objSearch.currencyID !== undefined ? this.objSearch.currencyID : null;
        searchObj.fromDate = this.objSearch.fromDate !== undefined ? this.objSearch.fromDate : null;
        searchObj.toDate = this.objSearch.toDate !== undefined ? this.objSearch.toDate : null;
        searchObj.accountingObjectID = this.objSearch.accountingObjectID !== undefined ? this.objSearch.accountingObjectID : null;
        searchObj.textSearch = this.objSearch.textSearch !== undefined ? this.objSearch.textSearch : null;
        return searchObj;
    }

    clearSearch() {
        this.selectedRows = [];
        this.objSearch = {};
        const dateSearch = this.utilsService.getDataYearWork(this.currentAccount);
        if (dateSearch.fromDate !== '') {
            this.objSearch.fromDate = dateSearch.fromDate.format(DATE_FORMAT);
        }
        if (dateSearch.toDate !== '') {
            this.objSearch.toDate = dateSearch.toDate.format(DATE_FORMAT);
        }
        this.psTimeSheetSummaryService
            .searchAll({
                page: this.page - 1,
                size: this.itemsPerPage,
                sort: this.sort(),
                searchVoucher: JSON.stringify(this.saveSearchVoucher())
            })
            .subscribe(
                (res: HttpResponse<any[]>) => this.paginatePSTimeSheetSummaries(res.body, res.headers),
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    closePopup() {
        this.inPopup = false;
        if (this.file) {
            this.file = null;
        }
    }

    navigateHelp() {
        this.notificationService.findHelpLink({ id: 'THCC' }).subscribe((res: HttpResponse<any>) => {
            window.open(res.body, '_blank');
        });
    }

    exportPDFWebService(number: number) {
        const listID = this.selectedRows.map(x => x.id);
        this.inChungTuHangLoatService.inChungTuHangLoat({
            typeGroupID: TYPEGROUP.BANG_TONG_HOP_CHAM_CONG,
            typeReport: number,
            listID,
            fromDate: null,
            toDate: null,
            paramCheckAll: false,
            isDesc: true
        });
    }

    checkPSTimeSheetSummary(number: number) {
        switch (number) {
            case 27:
                return this.selectedRows.some(n => n.typeID === 761);
            case 28:
                return this.selectedRows.some(n => n.typeID === 760);
            default:
                return false;
        }
    }

    checkDisable() {
        if (this.selectedRows.length > 1) {
            return this.selectedRows.some(n => n.typeID === 761) && this.selectedRows.some(n => n.typeID === 760);
        }
    }

    checkSameOrg() {
        return this.selectedRows.every(n => n.companyID === this.currentAccount.organizationUnit.id);
    }

    changeFile(event) {
        const file = event.target.files;
        this.file = null;
        if (file && file.length) {
            if (file[0].name.endsWith('xlsx') || file[0].name.endsWith('xls')) {
                if (file[0].size < 2097152) {
                    this.file = file[0];
                } else {
                    this.toastrService.error(this.translateService.instant('ebwebApp.saBill.error.maxSize'));
                }
            } else {
                this.toastrService.error(this.translateService.instant('ebwebApp.saBill.error.notFormat'));
            }
        }
    }

    upload(download) {
        this.isLoading = true;
        if (!this.file) {
            this.toastrService.error(this.translateService.instant('ebwebApp.saBill.error.noFile'));
            this.isLoading = false;
            return;
        }
        this.toastrService.info(this.translateService.instant('ebwebApp.saBill.announce'));
        this.psTimeSheetService.upload(this.file).subscribe(res => {
            if (res.headers.get('isError') === '1') {
                if (res.headers.get('message') !== 'FAIL') {
                    this.toastrService.error(this.translateService.instant('ebwebApp.saBill.upload.' + res.headers.get('message')));
                } else {
                    // this.refModel.close();
                    this.modalService.open(download, { size: 'lg' });
                    const blob = new Blob([res.body], { type: 'application/pdf' });
                    const fileURL = URL.createObjectURL(blob);

                    this.link = document.createElement('a');
                    document.body.appendChild(this.link);
                    this.link.download = fileURL;
                    this.link.setAttribute('style', 'display: none');
                    const name = 'Import_TongHopChamCong_Loi.xlsx';
                    this.link.setAttribute('download', name);
                    this.link.href = fileURL;
                }
            } else {
                const blob = new Blob([res.body], { type: 'application/json' });
                const fileReader = new FileReader();
                const that = this;
                fileReader.onload = function() {
                    sessionStorage.setItem('DataFromImport', this.result);
                    that.dataSession.page = that.page;
                    that.dataSession.itemsPerPage = that.itemsPerPage;
                    that.dataSession.pageCount = that.pageCount;
                    that.dataSession.totalItems = that.totalItems;
                    that.dataSession.rowNum = that.rowNum;
                    // sort
                    that.dataSession.predicate = that.predicate;
                    that.dataSession.accountingObjectName = that.accountingObjectName;
                    that.dataSession.reverse = that.reverse;
                    that.sessionPSTimeSheet.month = that.month;
                    that.sessionPSTimeSheet.year = that.year;
                    that.sessionPSTimeSheet.typeID = that.typeID;
                    that.sessionPSTimeSheet.pSTimeSheetName = that.pSTimeSheetName;
                    that.sessionPSTimeSheet.pSTimeSheetSummaryName = that.pSTimeSheetSummaryName;
                    that.sessionPSTimeSheet.typeCreate = that.typeCreate;
                    that.sessionPSTimeSheet.lastPSTimeSheetID = that.lastTimeSheetID;
                    that.sessionPSTimeSheet.lastPSTimeSheetSummaryID = that.lastTimeSheetID;
                    that.sessionPSTimeSheet.autoAddNewEmployee = that.autoAddNewEmployee ? that.autoAddNewEmployee : false;
                    that.sessionPSTimeSheet.getEmployeeNotActive = that.getEmployeeNotActive ? that.getEmployeeNotActive : false;
                    that.sessionPSTimeSheet.listDepartmentID = that.organizationUnitss.filter(n => n.checked).map(n => n.id);
                    if (that.modalRef) {
                        that.modalRef.close();
                    }
                    sessionStorage.setItem('sessionPSTimeSheetSummary', JSON.stringify(that.sessionPSTimeSheet));
                    that.router.navigate(['tong-hop-cham-cong/', 'new']);
                };
                fileReader.readAsText(blob);
            }
        });
    }

    download() {
        this.link.click();
    }

    downloadTem() {
        this.isLoading = true;
        this.psTimeSheetService.downloadTem().subscribe(
            res => {
                const blob = new Blob([res.body], { type: 'application/vnd.ms-excel' });
                const fileURL = URL.createObjectURL(blob);

                const link = document.createElement('a');
                document.body.appendChild(link);
                link.download = fileURL;
                link.setAttribute('style', 'display: none');
                const name = 'Import_TongHopChamCong.xlsx';
                link.setAttribute('download', name);
                link.href = fileURL;
                link.click();
                this.isLoading = false;
            },
            () => {
                this.isLoading = false;
            }
        );
    }
}
