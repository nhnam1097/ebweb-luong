import { AfterViewChecked, AfterViewInit, Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Observable, Subscription } from 'rxjs';

import { TranslateService } from '@ngx-translate/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ROLE } from 'app/role.constants';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';
import { ToastrService } from 'ngx-toastr';
import { DatePipe } from '@angular/common';
import { Principal } from 'app/core';
import { BaseComponent } from 'app/shared/base-component/base.component';
import { ebAuth } from 'app/shared/decorator/ebAuth.decorator';
import { PSTimeSheetSummaryService } from 'app/luong/tong_hop_cham_cong/tong-hop-cham-cong.service';
import { DECIMAL, THOUSANDS, TypeMultiPrintTemplate } from 'app/app.constants';
import { IPSTimeSheetSummary } from 'app/shared/models/ps-time-sheet-summary.model';
import { IAccountingObject } from 'app/shared/models/accounting-object.model';
import {
    IPSTimeSheetSummaryDetails,
    PSTimeSheetSummaryDetails,
    TongHopChamCongExportExcel
} from 'app/shared/models/ps-time-sheet-summary-details.model';
import { IDataSessionStorage } from 'app/shared/models/DataSessionStorage';
import { IPSTimeSheet } from 'app/shared/models/ps-time-sheet.model';
import { ContextMenu, IContextMenu } from 'app/shared/models/context-menu.model';
import { ISessionPSTimeSheet } from 'app/shared/models/session-p-s-time-sheet';
import { IPSSalaryTaxInsuranceRegulation } from 'app/shared/models/ps-salary-tax-insurance-regulation.model';
import { AccountingObjectService } from 'app/shared/services/accounting-object.service';
import { OrganizationUnitService } from 'app/shared/services/organization-unit.service';
import { UtilsService } from 'app/shared/UtilsService/Utils.service';
import { RefModalService } from 'app/shared/services/ref-modal.service';
import { QuyDinhLuongThueBaoHiemService } from 'app/shared/services/quy-dinh-luong-thue-bao-hiem.service';
import { NotificationService } from 'app/shared/notification';
import { InChungTuHangLoatService } from 'app/shared/in-chung-tu-hang-loat/in-chung-tu-hang-loat.service';
import { SearchVoucher } from 'app/shared/models/SearchVoucher';
import { PSTimeSheetSummaryDetailsService } from 'app/luong/tong_hop_cham_cong/ps-time-sheet-summary-details.service';

@Component({
    selector: 'eb-ps-time-sheet-summary-update',
    templateUrl: './tong-hop-cham-cong-update.component.html',
    styleUrls: ['./tong-hop-cham-cong-update.component.css']
})
export class PSTimeSheetSummaryUpdateComponent extends BaseComponent implements OnInit, AfterViewInit, AfterViewChecked {
    private _pSTimeSheetSummary: IPSTimeSheetSummary;
    TYPE_CHAM_CONG_THEO_NGAY = 760;
    TYPE_CHAM_CONG_THEO_GIO = 761;
    isSaving: boolean;
    periodPSTimeSheet: string;
    month: number;
    year: number;
    select: any;
    employeess: IAccountingObject[];
    pSTimeSheetSummaryDetails: IPSTimeSheetSummaryDetails[];
    @ViewChild('content') content: TemplateRef<any>;
    @ViewChild('popUpMultiDelete') popUpMultiDelete: TemplateRef<any>;
    @ViewChild('popUpDelete') popUpDelete: TemplateRef<any>;
    count: number;
    items: any;
    //  data storage provider
    warningMessage: string;
    page: number;
    itemsPerPage: number;
    pageCount: number;
    totalItems: number;
    rowNum: number;
    // sort
    predicate: any;
    isCreateUrl: boolean;
    isEditUrl: boolean;
    disableAddButton: boolean;
    disableEditButton: boolean;
    dataSession: IDataSessionStorage;
    eventSubscriber: Subscription;
    modalRef: NgbModalRef;
    currentAccount: any;
    options: any;
    isHideTypeLedger: Boolean;
    pSTimeSheetSummaryCopy: IPSTimeSheet;
    pSTimeSheetSummaryDetailsCopy: IPSTimeSheetSummaryDetails[];
    isReadOnly: boolean;
    contextMenu: IContextMenu;
    sessionPSTimeSheet: ISessionPSTimeSheet = new class implements ISessionPSTimeSheet {
        month: number;
        year: number;
        pSTimeSheetSummaryName?: string;
        typeCreate?: number;
        lastPSTimeSheetID?: string;
        lastPSTimeSheetSummaryID?: string;
        autoAddNewEmployee?: boolean;
        getEmployeeNotActive?: boolean;
        listDepartmentID?: any[];
    }();
    index: number;
    currentDay: string;
    daysOfMonth: number;
    pSSalaryTaxInsuranceRegulation: IPSSalaryTaxInsuranceRegulation;
    isSunday: boolean;
    defaultAllDayTimeSheetCode: string;
    defaultHalfDayTimeSheetCode: string;

    optionsDirective = {
        decimal: DECIMAL,
        thousands: THOUSANDS,
        allowNegative: false,
        precision: 2
    };

    ROLE_XEM = ROLE.ChamCong_XEM;
    ROLE_THEM = ROLE.ChamCong_Them;
    ROLE_SUA = ROLE.ChamCong_Sua;
    ROLE_XOA = ROLE.ChamCong_Xoa;
    ROLE_IN = ROLE.ChamCong_In;
    ROLE_KETXUAT = ROLE.ChamCong_KetXuat;

    buttonDeleteTranslate = 'ebwebApp.mBDeposit.toolTip.delete';
    buttonAddTranslate = 'ebwebApp.mBDeposit.toolTip.add';
    buttonEditTranslate = 'ebwebApp.mBDeposit.toolTip.edit';
    buttonSaveTranslate = 'ebwebApp.mBDeposit.toolTip.save';
    buttonPrintTranslate = 'ebwebApp.mBDeposit.toolTip.print';
    buttonCloseFormTranslate = 'ebwebApp.mBDeposit.toolTip.closeForm';
    TypeMultiPrintTemplate = TypeMultiPrintTemplate;
    isUseToolIn: boolean;
    searchVoucher: string;
    constructor(
        private pSTimeSheetSummaryService: PSTimeSheetSummaryService,
        private pSTimeSheetSummaryDetailsService: PSTimeSheetSummaryDetailsService,
        private activatedRoute: ActivatedRoute,
        private accService: AccountingObjectService,
        private router: Router,
        private jhiAlertService: JhiAlertService,
        private organizationUnitService: OrganizationUnitService,
        private toastr: ToastrService,
        private translate: TranslateService,
        private datepipe: DatePipe,
        private eventManager: JhiEventManager,
        private modalService: NgbModal,
        private utilsServiceSecond: UtilsService,
        public utilsService: UtilsService,
        public translateService: TranslateService,
        private refModalService: RefModalService,
        private principal: Principal,
        private pSSalaryTaxInsuranceRegulationService: QuyDinhLuongThueBaoHiemService,
        private notificationService: NotificationService,
        private inChungTuHangLoatService: InChungTuHangLoatService,
        private accountingObjectService: AccountingObjectService
    ) {
        super();
        this.contextMenu = new ContextMenu();
        this.principal.identity().then(account => {
            this.currentAccount = account;
            this.isEnter = this.currentAccount.systemOption.filter(n => n.code === 'TCKHAC_Enter')[0].data === '1';
        });
    }

    ngOnInit() {
        this.pSTimeSheetSummaryDetails = [];
        this.isSaving = false;
        this.isHideTypeLedger = false;
        this.searchVoucher = JSON.parse(sessionStorage.getItem('dataSearchPSTimeSheet'));
        this.isEditUrl = window.location.href.includes('/edit') || window.location.href.includes('/view');
        this.activatedRoute.data.subscribe(async ({ pSTimeSheetSummary }) => {
            this.pSTimeSheetSummary = pSTimeSheetSummary;
            if (sessionStorage.getItem('sessionPSTimeSheetSummary')) {
                this.sessionPSTimeSheet = JSON.parse(sessionStorage.getItem('sessionPSTimeSheetSummary'));
                this.month = this.sessionPSTimeSheet.month;
                this.year = this.sessionPSTimeSheet.year;
            } else {
                this.month = this.pSTimeSheetSummary.month;
                this.year = this.pSTimeSheetSummary.year;
            }
            this.pSSalaryTaxInsuranceRegulationService
                .getDataByCompanyID({ month: this.month, year: this.year })
                .subscribe((res: HttpResponse<IPSSalaryTaxInsuranceRegulation>) => {
                    this.pSSalaryTaxInsuranceRegulation = res.body;
                });
            if (!this.pSTimeSheetSummary.id) {
                this.disableEditButton = true;
                if (sessionStorage.getItem('sessionPSTimeSheetSummary')) {
                    this.sessionPSTimeSheet = JSON.parse(sessionStorage.getItem('sessionPSTimeSheetSummary'));
                    sessionStorage.removeItem('sessionPSTimeSheetSummary');
                } else {
                    this.router.navigate(['/tong-hop-cham-cong']);
                }
                this.pSTimeSheetSummary.typeID =
                    this.sessionPSTimeSheet && this.sessionPSTimeSheet.typeID
                        ? this.sessionPSTimeSheet.typeID
                        : this.TYPE_CHAM_CONG_THEO_NGAY;
                if (this.sessionPSTimeSheet && this.sessionPSTimeSheet.typeCreate === 0) {
                    if (this.sessionPSTimeSheet.listDepartmentID) {
                        this.pSTimeSheetSummaryDetailsService
                            .getNewPSTimeSheetSummary({ listDepartmentID: this.sessionPSTimeSheet.listDepartmentID })
                            .subscribe(async (res: HttpResponse<IPSTimeSheetSummaryDetails[]>) => {
                                this.pSTimeSheetSummaryDetails = res.body;
                                this.pSTimeSheetSummaryDetails.forEach((i, v) => {
                                    i.stt = v;
                                });
                                this.lstDataForSetDefault = [];
                                this.lstDataForSetDefault.push(this.pSTimeSheetSummaryDetails);
                                await this.loadDataDefaultForOpenVoucherSync(this.lstDataForSetDefault);
                                if (this.pSTimeSheetSummaryDetails && this.pSTimeSheetSummaryDetails.length > 0) {
                                    for (let i = 0; i < this.pSTimeSheetSummaryDetails.length; i++) {
                                        if (this.employees && this.employees.length > 0) {
                                            const listAcc = this.employees.filter(
                                                a => a.departmentId === this.pSTimeSheetSummaryDetails[i].departmentID
                                            );
                                            if (listAcc && listAcc.length > 0) {
                                                this.pSTimeSheetSummaryDetails[i].listEmployeeID = listAcc.map(object => ({
                                                    ...object
                                                }));
                                            }
                                            const acc = this.pSTimeSheetSummaryDetails[i].listEmployeeID.find(
                                                a => a.id === this.pSTimeSheetSummaryDetails[i].employeeID
                                            );
                                            if (!acc) {
                                                this.pSTimeSheetSummaryDetails[i].listEmployeeID.push(
                                                    this.employees.find(a => a.id === this.pSTimeSheetSummaryDetails[i].employeeID)
                                                );
                                            }
                                        }
                                    }
                                }
                                this.pSTimeSheetSummaryDetails.forEach(item => {
                                    item.workAllDay = 0;
                                    item.workHalfAday = 0;
                                    item.totalOverTime = 0;
                                    item.total = 0;
                                });
                            });
                    } else {
                        this.pSTimeSheetSummaryDetails = [];
                    }
                } else if (this.sessionPSTimeSheet && this.sessionPSTimeSheet.typeCreate === 1) {
                    if (this.sessionPSTimeSheet.listDepartmentID) {
                        this.pSTimeSheetSummaryDetailsService
                            .getNewPSTimeSheetSummaryType2({
                                lastPSTimeSheetSummaryID: this.sessionPSTimeSheet.lastPSTimeSheetSummaryID,
                                listDepartmentID: this.sessionPSTimeSheet.listDepartmentID
                            })
                            .subscribe(async (res: HttpResponse<IPSTimeSheetSummaryDetails[]>) => {
                                this.pSTimeSheetSummaryDetails = res.body;
                                this.pSTimeSheetSummaryDetails.forEach((i, v) => {
                                    i.stt = v;
                                });
                                this.lstDataForSetDefault = [];
                                this.lstDataForSetDefault.push(this.pSTimeSheetSummaryDetails);
                                await this.loadDataDefaultForOpenVoucherSync(this.lstDataForSetDefault);
                                if (this.pSTimeSheetSummaryDetails && this.pSTimeSheetSummaryDetails.length > 0) {
                                    for (let i = 0; i < this.pSTimeSheetSummaryDetails.length; i++) {
                                        if (this.employees && this.employees.length > 0) {
                                            const listAcc = this.employees.filter(
                                                a => a.departmentId === this.pSTimeSheetSummaryDetails[i].departmentID
                                            );
                                            if (listAcc && listAcc.length > 0) {
                                                this.pSTimeSheetSummaryDetails[i].listEmployeeID = listAcc.map(object => ({
                                                    ...object
                                                }));
                                            }
                                            const acc = this.pSTimeSheetSummaryDetails[i].listEmployeeID.find(
                                                a => a.id === this.pSTimeSheetSummaryDetails[i].employeeID
                                            );
                                            if (!acc) {
                                                this.pSTimeSheetSummaryDetails[i].listEmployeeID.push(
                                                    this.employees.find(a => a.id === this.pSTimeSheetSummaryDetails[i].employeeID)
                                                );
                                            }
                                        }
                                    }
                                }
                            });
                    } else {
                        this.pSTimeSheetSummaryDetails = [];
                    }
                } else if (this.sessionPSTimeSheet && this.sessionPSTimeSheet.typeCreate === 2) {
                    if (this.sessionPSTimeSheet.listDepartmentID) {
                        this.pSTimeSheetSummaryDetailsService
                            .getNewPSTimeSheetSummaryType1({
                                lastPSTimeSheetSummaryID: this.sessionPSTimeSheet.lastPSTimeSheetID,
                                listDepartmentID: this.sessionPSTimeSheet.listDepartmentID
                            })
                            .subscribe(async (res: HttpResponse<IPSTimeSheetSummaryDetails[]>) => {
                                this.pSTimeSheetSummaryDetails = res.body;
                                this.pSTimeSheetSummaryDetails.forEach((i, v) => {
                                    i.stt = v;
                                });
                                for (let i = 0; i < this.pSTimeSheetSummaryDetails.length; i++) {
                                    this.selectChangeTotal(i);
                                }
                                this.lstDataForSetDefault = [];
                                this.lstDataForSetDefault.push(this.pSTimeSheetSummaryDetails);
                                await this.loadDataDefaultForOpenVoucherSync(this.lstDataForSetDefault);
                                if (this.pSTimeSheetSummaryDetails && this.pSTimeSheetSummaryDetails.length > 0) {
                                    for (let i = 0; i < this.pSTimeSheetSummaryDetails.length; i++) {
                                        if (this.employees && this.employees.length > 0) {
                                            const listAcc = this.employees.filter(
                                                a => a.departmentId === this.pSTimeSheetSummaryDetails[i].departmentID
                                            );
                                            if (listAcc && listAcc.length > 0) {
                                                this.pSTimeSheetSummaryDetails[i].listEmployeeID = listAcc.map(object => ({
                                                    ...object
                                                }));
                                            }
                                            const acc = this.pSTimeSheetSummaryDetails[i].listEmployeeID.find(
                                                a => a.id === this.pSTimeSheetSummaryDetails[i].employeeID
                                            );
                                            if (!acc) {
                                                this.pSTimeSheetSummaryDetails[i].listEmployeeID.push(
                                                    this.employees.find(a => a.id === this.pSTimeSheetSummaryDetails[i].employeeID)
                                                );
                                            }
                                        }
                                    }
                                }
                            });
                    } else {
                        this.pSTimeSheetSummaryDetails = [];
                    }
                } else if (this.sessionPSTimeSheet && this.sessionPSTimeSheet.typeCreate === 3) {
                    await this.loadDepartment();
                    await this.loadEmployee();
                    const objs = JSON.parse(sessionStorage.getItem('DataFromImport'));
                    sessionStorage.removeItem('DataFromImport');
                    for (let j = 0; j < objs.length; j++) {
                        const item = new PSTimeSheetSummaryDetails();
                        item.employeeID = objs[j].employeeID;
                        item.accountingObjectName = objs[j].accountingObjectName;
                        item.departmentID = objs[j].departmentID;
                        item.workAllDay = objs[j].workAllDay;
                        item.workHalfAday = objs[j].workHalfAday;
                        item.totalOverTime = objs[j].totalOverTime;
                        item.total = objs[j].total;
                        this.pSTimeSheetSummaryDetails.push(item);
                    }
                    this.pSTimeSheetSummaryDetails.forEach((i, v) => {
                        i.stt = v;
                    });
                    for (let i = 0; i < this.pSTimeSheetSummaryDetails.length; i++) {
                        if (this.employees && this.employees.length > 0) {
                            const listAcc = this.employees.filter(a => a.departmentId === this.pSTimeSheetSummaryDetails[i].departmentID);
                            if (listAcc && listAcc.length > 0) {
                                this.pSTimeSheetSummaryDetails[i].listEmployeeID = listAcc.map(object => ({
                                    ...object
                                }));
                            }
                            const acc = this.pSTimeSheetSummaryDetails[i].listEmployeeID.find(
                                a => a.id === this.pSTimeSheetSummaryDetails[i].employeeID
                            );
                            if (!acc) {
                                this.pSTimeSheetSummaryDetails[i].listEmployeeID.push(
                                    this.employees.find(a => a.id === this.pSTimeSheetSummaryDetails[i].employeeID)
                                );
                            }
                        }
                    }
                }
                this.isReadOnly = false;
            } else {
                this.pSTimeSheetSummaryService
                    .getIndexRow({
                        id: this.pSTimeSheetSummary.id,
                        isNext: true,
                        typeID: this.TYPE_CHAM_CONG_THEO_NGAY,
                        searchVoucher: this.searchVoucher === undefined ? null : JSON.stringify(this.searchVoucher)
                    })
                    .subscribe(
                        (res: HttpResponse<any[]>) => {
                            this.rowNum = res.body[0];
                            if (res.body.length === 1) {
                                this.count = 1;
                            } else {
                                this.count = res.body[1];
                            }
                        },
                        (res: HttpErrorResponse) => this.onError(res.message)
                    );
                this.pSTimeSheetSummaryDetails = this.pSTimeSheetSummary.pSTimeSheetSummaryDetails.sort(
                    (a, b) => a.orderPriority - b.orderPriority
                );
                this.pSTimeSheetSummaryDetails.forEach((i, v) => {
                    i.stt = v;
                });
                this.utilsService
                    .getDataDefaultForOpenVoucher({ lstIDEmployee: this.pSTimeSheetSummaryDetails.map(n => n.employeeID) })
                    .subscribe(async (res: HttpResponse<any>) => {
                        this.lstDataForSetDefault = [];
                        this.lstDataForSetDefault.push(this.pSTimeSheetSummaryDetails);
                        await this.loadDataDefaultForOpenVoucherSync(this.lstDataForSetDefault);
                        this.employees = res.body.listEmployees;
                        if (this.pSTimeSheetSummaryDetails && this.pSTimeSheetSummaryDetails.length > 0) {
                            for (let i = 0; i < this.pSTimeSheetSummaryDetails.length; i++) {
                                if (this.employees && this.employees.length > 0) {
                                    const listAcc = this.employees.filter(
                                        a => a.departmentId === this.pSTimeSheetSummaryDetails[i].departmentID
                                    );
                                    if (listAcc && listAcc.length > 0) {
                                        this.pSTimeSheetSummaryDetails[i].listEmployeeID = listAcc.map(object => ({ ...object }));
                                    }
                                    const acc = this.pSTimeSheetSummaryDetails[i].listEmployeeID.find(
                                        a => a.id === this.pSTimeSheetSummaryDetails[i].employeeID
                                    );
                                    if (!acc) {
                                        this.pSTimeSheetSummaryDetails[i].listEmployeeID.push(
                                            this.employees.find(a => a.id === this.pSTimeSheetSummaryDetails[i].employeeID)
                                        );
                                    }
                                }
                            }
                        }
                    });
            }
        });
        // this.organizationUnitService.getOrganizationUnits().subscribe((res: HttpResponse<IOrganizationUnit[]>) => {
        //     this.organizationUnits = res.body
        //         .filter(a => a.unitType === 4)
        //         .sort((a, b) => a.organizationUnitCode.localeCompare(b.organizationUnitCode));
        // });
        this.accService.getAllDTO().subscribe(res => {
            this.employeess = res.body.filter(a => a.isEmployee);
        });
        this.isCreateUrl = window.location.href.includes('/tong-hop-cham-cong/new');
        this.isEdit = this.isCreateUrl;
        this.disableAddButton = true;
        this.sumAfterDeleteByContextMenu();
        this.afterAddRow();
        this.registerCopyRow();
        this.eventSubscriber = this.eventManager.subscribe('saveSuccess', response => {
            this.isCbbSaveAndNew = false;
            this.registerComboboxSave(response);
            this.selectChangeAccountingObject();
            this.utilsService.setShowPopup(false);
        });
        this.eventSubscriber = this.eventManager.subscribe('saveAndNewSuccess', response => {
            this.isCbbSaveAndNew = false;
            this.registerComboboxSave(response);
            this.selectChangeAccountingObject();
            this.utilsService.setShowPopup(false);
        });
        this.eventSubscriber = this.eventManager.subscribe('closePopup', response => {
            this.utilsService.setShowPopup(response.content);
        });
        this.eventSubscribers.push(this.eventSubscriber);
        this.checkSameOrg();
    }

    closeForm() {
        event.preventDefault();
        this.closeAll();
    }

    closeAll() {
        this.router.navigate(['/tong-hop-cham-cong']);
    }

    copy() {
        if (this.pSTimeSheetSummary.id) {
            this.pSTimeSheetSummaryDetails = this.pSTimeSheetSummary.pSTimeSheetSummaryDetails
                ? this.pSTimeSheetSummary.pSTimeSheetSummaryDetails
                : [];
        }
        this.pSTimeSheetSummaryCopy = Object.assign({}, this.pSTimeSheetSummary);
        if (this.pSTimeSheetSummaryDetails) {
            this.pSTimeSheetSummaryDetailsCopy = this.pSTimeSheetSummaryDetails.map(object => ({ ...object }));
        }
    }

    @ebAuth(['ROLE_ADMIN', ROLE.ChamCong_Sua])
    edit() {
        event.preventDefault();
        if (!this.isCreateUrl) {
            // && !this.utilsService.isShowPopup
            this.isCreateUrl = !this.isCreateUrl;
            this.isEdit = this.isCreateUrl;
            this.disableAddButton = false;
            this.disableEditButton = true;
            this.isReadOnly = false;
            this.copy();
            this.focusFirstInput();
        }
    }

    @ebAuth(['ROLE_ADMIN', ROLE.ChamCong_Them])
    addNew($event) {
        event.preventDefault();
        if (!this.isCreateUrl) {
            // && !this.utilsService.isShowPopup
            sessionStorage.setItem('checkNewTongHopChamCong', JSON.stringify(true));
            this.router.navigate(['tong-hop-cham-cong']);
        }
    }

    @ebAuth(['ROLE_ADMIN', ROLE.ChamCong_Them, ROLE.ChamCong_Sua])
    save(isNew = false) {
        event.preventDefault();
        if (this.isCreateUrl) {
            this.pSTimeSheetSummary.typeID = this.pSTimeSheetSummary.typeID
                ? this.pSTimeSheetSummary.typeID
                : this.sessionPSTimeSheet.typeID;
            this.pSTimeSheetSummary.month = this.pSTimeSheetSummary.month ? this.pSTimeSheetSummary.month : this.sessionPSTimeSheet.month;
            this.pSTimeSheetSummary.year = this.pSTimeSheetSummary.year ? this.pSTimeSheetSummary.year : this.sessionPSTimeSheet.year;
            this.pSTimeSheetSummary.pSTimeSheetSummaryName = this.pSTimeSheetSummary.pSTimeSheetSummaryName
                ? this.pSTimeSheetSummary.pSTimeSheetSummaryName
                : this.sessionPSTimeSheet.pSTimeSheetSummaryName;
            this.pSTimeSheetSummary.pSTimeSheetSummaryDetails = this.pSTimeSheetSummaryDetails;
            if (this.checkError()) {
                this.isSaving = true;
                // check is url new
                if (this.pSTimeSheetSummary.id && this.isEditUrl) {
                    this.isCreateUrl = this.isEdit = false;
                }
                if (this.isCreateUrl && this.pSTimeSheetSummary.id !== undefined) {
                    this.pSTimeSheetSummary.id = undefined;
                    for (let i = 0; i < this.pSTimeSheetSummary.pSTimeSheetSummaryDetails.length; i++) {
                        this.pSTimeSheetSummary.pSTimeSheetSummaryDetails[i].id = undefined;
                    }
                }
                for (let i = 0; i < this.pSTimeSheetSummary.pSTimeSheetSummaryDetails.length; i++) {
                    const contactTitle = this.employees.find(a => a.id === this.pSTimeSheetSummary.pSTimeSheetSummaryDetails[i].employeeID);
                    if (contactTitle) {
                        this.pSTimeSheetSummary.pSTimeSheetSummaryDetails[i].accountingObjectTitle = contactTitle.contactTitle;
                    }
                }
                if (!this.isCreateUrl && !this.isEditUrl) {
                    this.pSTimeSheetSummary.id = undefined;
                }
                if (this.pSTimeSheetSummary.id !== undefined) {
                    this.subscribeToSaveResponse(this.pSTimeSheetSummaryService.update(this.pSTimeSheetSummary));
                } else {
                    this.subscribeToSaveResponse(this.pSTimeSheetSummaryService.create(this.pSTimeSheetSummary));
                }
            } else {
            }
        }
    }

    checkError() {
        if (this.pSTimeSheetSummaryDetails.length === 0) {
            this.warningMessage =
                this.translate.instant('ebwebApp.mBDeposit.home.chiTiet') + ' ' + this.translate.instant('ebwebApp.mBDeposit.isNotNull');
            this.toastr.error(this.warningMessage.toString(), this.translate.instant('ebwebApp.pSTimeSheet.message'));
            return false;
        }
        for (let i = 0; i < this.pSTimeSheetSummaryDetails.length; i++) {
            if (!this.pSTimeSheetSummaryDetails[i].departmentID) {
                this.warningMessage =
                    this.translate.instant('ebwebApp.pSTimeSheetDetails.departmentID') +
                    ' ' +
                    this.translate.instant('ebwebApp.mBDeposit.isNotNull');
                this.toastr.error(this.warningMessage.toString(), this.translate.instant('ebwebApp.mBDeposit.message'));
                return false;
            }
            if (!this.pSTimeSheetSummaryDetails[i].employeeID) {
                this.warningMessage =
                    this.translate.instant('ebwebApp.pSTimeSheetDetails.employeeID') +
                    ' ' +
                    this.translate.instant('ebwebApp.mBDeposit.isNotNull');
                this.toastr.error(this.warningMessage.toString(), this.translate.instant('ebwebApp.mBDeposit.message'));
                return false;
            }
        }
        return true;
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<any>>) {
        result.subscribe(
            (res: HttpResponse<any>) => {
                this.pSTimeSheetSummary.id = res.body.id;
                this.pSTimeSheetSummary.month = res.body.month;
                this.pSTimeSheetSummary.year = res.body.year;
                this.pSTimeSheetSummary.pSTimeSheetSummaryName = res.body.pSTimeSheetSummaryName;
                this.pSTimeSheetSummary.typeID = res.body.typeID;
                this.pSTimeSheetSummary.pSTimeSheetSummaryDetails = res.body.pSTimeSheetSummaryDetails;
                this.pSTimeSheetSummaryDetails = res.body.pSTimeSheetSummaryDetails;
                this.onSaveSuccess();
                if (this.isEditUrl) {
                    this.toastr.success(
                        this.translate.instant('ebwebApp.pSTimeSheet.editSuccess'),
                        this.translate.instant('ebwebApp.pSTimeSheet.message')
                    );
                } else {
                    this.toastr.success(
                        this.translate.instant('ebwebApp.pSTimeSheet.insertSuccess'),
                        this.translate.instant('ebwebApp.pSTimeSheet.message')
                    );
                }
                if (this.pSTimeSheetSummaryDetails && this.pSTimeSheetSummaryDetails.length > 0) {
                    for (let i = 0; i < this.pSTimeSheetSummaryDetails.length; i++) {
                        if (this.employees && this.employees.length > 0) {
                            const listAcc = this.employees.filter(a => a.departmentId === this.pSTimeSheetSummaryDetails[i].departmentID);
                            if (listAcc && listAcc.length > 0) {
                                this.pSTimeSheetSummaryDetails[i].listEmployeeID = listAcc.map(object => ({
                                    ...object
                                }));
                            }
                            const acc = this.employees.find(a => a.id === this.pSTimeSheetSummaryDetails[i].employeeID);
                            if (!acc) {
                                if (!this.pSTimeSheetSummaryDetails[i].listEmployeeID) {
                                    this.pSTimeSheetSummaryDetails[i].listEmployeeID = [];
                                }
                                this.pSTimeSheetSummaryDetails[i].listEmployeeID.push(
                                    this.accountingObjects.find(a => a.id === this.pSTimeSheetSummaryDetails[i].employeeID)
                                );
                            }
                        }
                    }
                }
                this.router.navigate(['./tong-hop-cham-cong', this.pSTimeSheetSummary.id, 'edit']);
            },
            (res: HttpErrorResponse) => {
                if (res.error.errorKey !== 'noVoucherLimited') {
                    this.toastr.error(
                        this.translate.instant('ebwebApp.pSTimeSheet.updateError'),
                        this.translate.instant('ebwebApp.mBDeposit.message')
                    );
                }
                this.onSaveError();
                this.isCreateUrl = this.isEdit = true;
            }
        );
    }

    noVoucherExist() {
        this.toastr.error(
            this.translate.instant('global.data.noVocherAlreadyExist'),
            this.translate.instant('ebwebApp.mCReceipt.home.message')
        );
        this.isCreateUrl = this.isEdit = true;
    }

    private onSaveSuccess() {
        this.copy();
        this.isSaving = false;
        this.disableAddButton = true;
        this.disableEditButton = false;
    }

    private onSaveError() {
        this.isSaving = false;
        this.isCreateUrl = !this.isCreateUrl;
        this.isCreateUrl = this.isEdit;
    }

    get pSTimeSheetSummary() {
        return this._pSTimeSheetSummary;
    }

    set pSTimeSheetSummary(pSTimeSheetSummary: IPSTimeSheetSummary) {
        this._pSTimeSheetSummary = pSTimeSheetSummary;
    }

    selectChangeAccountingObject() {}

    addNewRow(eventData: any, select: number) {
        if (this.isCreateUrl) {
            if (select === 0) {
                const ob = Object.assign({}, this.pSTimeSheetSummaryDetails[this.pSTimeSheetSummaryDetails.length - 1]);
                ob.id = undefined;
                ob.employeeID = null;
                ob.accountingObjectName = null;
                if (ob.stt !== undefined && ob.stt !== null) {
                    ob.stt++;
                } else {
                    ob.stt = 0;
                }
                this.pSTimeSheetSummaryDetails.push(ob);
                // this.pSTimeSheetSummaryDetails[this.pSTimeSheetSummaryDetails.length - 1].orderPriority = this.pSTimeSheetSummaryDetails.length - 1;
                // const nameTag: string = event.srcElement.id;
                // const index: number = this.pSTimeSheetSummaryDetails.length - 1;
                // const nameTagIndex: string = nameTag + String(index);
                const index: number = this.pSTimeSheetSummaryDetails.length - 1;
                const nameTag = 'field_departmentID';
                const nameTagIndex: string = nameTag + String(index);
                setTimeout(function() {
                    const element: HTMLElement = document.getElementById(nameTagIndex);
                    if (element) {
                        element.focus();
                    }
                }, 0);
            } else {
            }
        }
    }

    keyPress(value: number, select: number) {
        if (select === 0) {
            this.pSTimeSheetSummaryDetails.splice(value, 1);
        }
    }

    CTKTExportPDF(isDownload, typeReports: number) {
        // if (!this.isCreateUrl) {
        //     this.pSTimeSheetSummaryService
        //         .getCustomerReport({
        //             id: this.pSTimeSheetSummary.id,
        //             typeID: ,
        //             typeReport: typeReports
        //         })
        //         .subscribe(response => {
        //             // this.showReport(response);
        //             const file = new Blob([response.body], {type: 'application/pdf'});
        //             const fileURL = window.URL.createObjectURL(file);
        //             if (isDownload) {
        //                 const link = document.createElement('a');
        //                 document.body.appendChild(link);
        //                 link.download = fileURL;
        //                 link.setAttribute('style', 'display: none');
        //                 const name = 'Bao_co.pdf';
        //                 link.setAttribute('download', name);
        //                 link.href = fileURL;
        //                 link.click();
        //             } else {
        //                 const contentDispositionHeader = response.headers.get('Content-Disposition');
        //                 const result = contentDispositionHeader
        //                     .split(';')[1]
        //                     .trim()
        //                     .split('=')[1];
        //                 const newWin = window.open(fileURL, '_blank');
        //
        //                 // add a load listener to the window so that the title gets changed on page load
        //                 newWin.addEventListener('load', function () {
        //                     newWin.document.title = result.replace(/"/g, '');
        //                     // this.router.navigate(['/report/buy']);
        //                 });
        //             }
        //         });
        //     if (typeReports === 1) {
        //         this.toastr.success(
        //             this.translate.instant('ebwebApp.pSTimeSheet.printing') +
        //             this.translate.instant('ebwebApp.pSTimeSheet.financialPaper') +
        //             '...',
        //             this.translate.instant('ebwebApp.pSTimeSheet.message')
        //         );
        //     } else if (typeReports === 2) {
        //         this.toastr.success(
        //             this.translate.instant('ebwebApp.pSTimeSheet.printing') + this.translate.instant('ebwebApp.pSTimeSheet.creditNote') + '...',
        //             this.translate.instant('ebwebApp.pSTimeSheet.message')
        //         );
        //     }
        // }
    }

    exportPdf(isDownload: boolean, typeReports: number) {
        this.utilsServiceSecond.getCustomerReportPDF(
            {
                id: this.pSTimeSheetSummary.id,
                typeID: this.pSTimeSheetSummary.typeID,
                typeReport: typeReports
            },
            isDownload
        );
        if (typeReports === 4) {
            this.toastr.success(
                this.translateService.instant('ebwebApp.mBDeposit.printing') +
                    this.translateService.instant('ebwebApp.pSTimeSheetSummary.boardPSTimeSheetSummary') +
                    '...',
                this.translateService.instant('ebwebApp.mBDeposit.message')
            );
        }
    }

    // region Tiến lùi chứng từ
    // ham lui, tien
    previousEdit() {
        // goi service get by row num
        if (this.rowNum !== this.count) {
            this.pSTimeSheetSummaryService
                .findByRowNum({
                    id: this.pSTimeSheetSummary.id,
                    isNext: false,
                    typeID: this.TYPE_CHAM_CONG_THEO_NGAY,
                    searchVoucher: JSON.stringify(new SearchVoucher())
                })
                .subscribe(
                    (res: HttpResponse<IPSTimeSheet>) => {
                        this.navigate(res.body);
                    },
                    (res: HttpErrorResponse) => this.onError(res.message)
                );
        }
    }

    nextEdit() {
        // goi service get by row num
        if (this.rowNum !== 1) {
            this.pSTimeSheetSummaryService
                .findByRowNum({
                    id: this.pSTimeSheetSummary.id,
                    isNext: true,
                    typeID: this.TYPE_CHAM_CONG_THEO_NGAY,
                    searchVoucher: JSON.stringify(new SearchVoucher())
                })
                .subscribe(
                    (res: HttpResponse<IPSTimeSheet>) => {
                        this.navigate(res.body);
                    },
                    (res: HttpErrorResponse) => this.onError(res.message)
                );
        }
    }

    // end of lui tien

    navigate(ipsTimeSheet: IPSTimeSheet) {
        this.router.navigate(['/tong-hop-cham-cong', ipsTimeSheet.id, 'edit']);
    }

    private onError(errorMessage: string) {
        this.toastr.error(this.translate.instant('ebwebApp.pSTimeSheet.error'), this.translate.instant('ebwebApp.pSTimeSheet.message'));
    }

    @ebAuth(['ROLE_ADMIN', ROLE.ChamCong_Xoa])
    delete() {
        if (this.pSTimeSheetSummary.id && !this.isCreateUrl) {
            //     if (this.selectedRows.length > 1) {
            //         this.typeMultiAction = 0;
            //         this.modalRef = this.modalService.open(this.popUpMultiDelete, {backdrop: 'static'});
            //     } else if (
            //         this.selectedRows &&
            //         this.selectedRows.length === 1 &&
            //         !this.checkCloseBook(this.currentAccount, this.selectedRow.postedDate)
            //     ) {
            //         if (this.selectedRow && !this.selectedRow.recorded) {
            //             this.typeMultiAction = undefined;
            this.modalRef = this.modalService.open(this.popUpDelete, { backdrop: 'static' });
        }
    }

    onRightClick($event, data, selectedData, isNew, isDelete, select, currentRow, isCopy) {
        this.contextMenu.isNew = isNew;
        this.contextMenu.isDelete = isDelete;
        this.contextMenu.isShow = true;
        this.contextMenu.event = $event;
        this.contextMenu.data = data;
        this.contextMenu.selectedData = selectedData;
        this.contextMenu.isCopy = isCopy;
        this.select = select;
        this.currentRow = currentRow;
    }

    closeContextMenu() {
        this.contextMenu.isShow = false;
    }

    sumAfterDeleteByContextMenu() {
        this.eventSubscriber = this.eventManager.subscribe('afterDeleteRow', response => {
            if (this.isEdit) {
                if (this.select === 0) {
                    for (let i = this.currentRow + 1; i < this.pSTimeSheetSummaryDetails.length; i++) {
                        if (this.currentRow === i - 1) {
                            this.pSTimeSheetSummaryDetails[i].stt = this.pSTimeSheetSummaryDetails[i - 1].stt;
                        } else {
                            this.pSTimeSheetSummaryDetails[i].stt = this.pSTimeSheetSummaryDetails[i - 1].stt + 1;
                        }
                    }
                    this.pSTimeSheetSummaryDetails.splice(this.currentRow, 1);
                }
            }
        });
        this.eventSubscribers.push(this.eventSubscriber);
    }

    keyDownAddRow(value: number) {
        if (this.isCreateUrl && !this.getSelectionText()) {
            if (value !== null && value !== undefined) {
                const ob: IPSTimeSheetSummaryDetails = Object.assign({}, this.pSTimeSheetSummaryDetails[value]);
                ob.id = undefined;
                ob.orderPriority = undefined;
                this.pSTimeSheetSummaryDetails.push(ob);
            } else {
                this.pSTimeSheetSummaryDetails.push({});
            }
        }
    }

    afterAddRow() {
        this.eventSubscriber = this.eventManager.subscribe('afterAddNewRow', response => {
            const item = new PSTimeSheetSummaryDetails();
            item.stt = this.pSTimeSheetSummaryDetails.length > 0 ? this.pSTimeSheetSummaryDetails.length : 0;
            this.pSTimeSheetSummaryDetails.push(item);
        });
        this.eventSubscribers.push(this.eventSubscriber);
    }

    registerCopyRow() {
        this.eventSubscriber = this.eventManager.subscribe('afterCopyRow', response => {
            const ob: IPSTimeSheetSummaryDetails = Object.assign({}, this.contextMenu.selectedData);
            ob.id = undefined;
            ob.orderPriority = undefined;
            this.pSTimeSheetSummaryDetails.push(ob);
        });
        this.eventSubscribers.push(this.eventSubscriber);
    }

    saveContent() {
        if (this.modalRef) {
            this.modalRef.close();
        }
        this.save(false);
    }

    close() {
        this.copy();
        if (this.modalRef) {
            this.modalRef.close();
        }
        this.closeAll();
    }

    exit() {
        if (this.modalRef) {
            this.modalRef.close();
            return;
        }
    }

    /*
  * hàm ss du lieu 2 object và 2 mảng object
  * return true: neu tat ca giong nhau
  * return fale: neu 1 trong cac object ko giong nhau
  * */
    canDeactive(): boolean {
        if (this.isReadOnly) {
            return true;
        } else {
            return (
                this.utilsService.isEquivalent(this.pSTimeSheetSummary, this.pSTimeSheetSummaryCopy) &&
                this.utilsService.isEquivalentArray(this.pSTimeSheetSummaryDetails, this.pSTimeSheetSummaryDetailsCopy)
            );
        }
    }

    ngAfterViewInit(): void {
        if (this.isCreateUrl) {
            this.focusFirstInput();
        }
    }

    ngAfterViewChecked(): void {
        this.disableInput();
    }

    @ebAuth(['ROLE_ADMIN', ROLE.ChamCong_In])
    print($event?) {
        event.preventDefault();
    }

    saveDetails(i) {
        this.currentRow = i;
        this.details = this.pSTimeSheetSummaryDetails;
    }

    saveParent() {
        this.currentRow = null;
        this.parent = this.pSTimeSheetSummary;
    }

    addDataToDetail() {
        this.pSTimeSheetSummaryDetails = this.details ? this.details : this.pSTimeSheetSummaryDetails;
        this.pSTimeSheetSummary = this.parent ? this.parent : this.pSTimeSheetSummary;
    }

    continueDelete() {
        this.pSTimeSheetSummaryService.delete(this.pSTimeSheetSummary.id).subscribe(response => {
            this.modalRef.close();
            this.toastr.success(this.translate.instant('ebwebApp.pSTimeSheetSummary.deleteSuccessful'));
            this.router.navigate(['tong-hop-cham-cong']);
            // this.eventManager.broadcast({
            //     name: 'pSTimeSheetSummaryListModification',
            //     content: 'Deleted an pSTimeSheetSummary'
            // });
        });
    }

    closePopUpDelete() {
        if (this.modalRef) {
            this.modalRef.close();
        }
    }

    newArr(lenght: number): any[] {
        if (lenght > 0) {
            return new Array(lenght);
        } else {
            return new Array(0);
        }
    }

    selectChangeEmployeeID(detail) {
        if (detail.employeeID) {
            const existDepartment = this.pSTimeSheetSummaryDetails.filter(a => a.departmentID === detail.departmentID);
            if (existDepartment) {
                const existEmployee = existDepartment.filter(a => a.employeeID === detail.employeeID);
                if (existEmployee && existEmployee.length > 1) {
                    detail.employeeID = null;
                    return this.toastr.error(this.translate.instant('ebwebApp.pSTimeSheetSummary.existEmployee'));
                }
            }
            if (this.employeess && this.pSTimeSheetSummaryDetails && this.pSTimeSheetSummaryDetails.length > 0) {
                const employee = this.employeess.find(a => a.id === detail.employeeID);
                if (employee) {
                    detail.accountingObjectName = employee.accountingObjectName;
                    detail.accountingObjectTitle = employee.contactTitle;
                }
            }
        } else {
            detail.listEmployeeID = this.employeess.filter(a => a.departmentId === detail.departmentID);
        }
    }

    selectChangeTotal(i) {
        this.pSTimeSheetSummaryDetails[i].total =
            (this.pSTimeSheetSummaryDetails[i].workAllDay ? this.pSTimeSheetSummaryDetails[i].workAllDay : 0) +
            (this.pSTimeSheetSummaryDetails[i].workHalfAday ? this.pSTimeSheetSummaryDetails[i].workHalfAday : 0) +
            (this.pSTimeSheetSummaryDetails[i].totalOverTime ? this.pSTimeSheetSummaryDetails[i].totalOverTime : 0);
    }

    async selectChangeDepartmentID(i) {
        this.lstDataForSetDefault = [];
        this.lstDataForSetDefault.push(this.pSTimeSheetSummaryDetails);
        const listAcc = this.employeess.filter(a => a.departmentId === this.pSTimeSheetSummaryDetails[i].departmentID);
        this.pSTimeSheetSummaryDetails[i].listEmployeeID = [];
        this.pSTimeSheetSummaryDetails[i].employeeID = null;
        this.pSTimeSheetSummaryDetails[i].accountingObjectName = null;
        this.pSTimeSheetSummaryDetails[i].accountingObjectTitle = null;
        if (listAcc && listAcc.length > 0) {
            this.pSTimeSheetSummaryDetails[i].listEmployeeID = listAcc.map(object => ({ ...object }));
        }
    }

    sumFooter(item) {
        if (this.pSTimeSheetSummary && this.pSTimeSheetSummaryDetails && this.pSTimeSheetSummaryDetails.length > 0) {
            let total = 0;
            for (let i = 0; i < this.pSTimeSheetSummaryDetails.length; i++) {
                total += this.pSTimeSheetSummaryDetails[i][item];
            }
            return total;
        }
        return '';
    }

    clickEnter(e, detail) {
        let newRow = false;
        // const cellCurrent = e.path['0'].closest('td').cellIndex;
        const cellCurrent = +document.activeElement.getAttribute('tabIndex');
        if (detail.stt === this.pSTimeSheetSummaryDetails.length - 1) {
            // có 7 cell được đánh index
            if (6 * detail.stt + 6 === cellCurrent) {
                newRow = true;
            }
        }
        if ((this.isEnter && detail.stt === this.pSTimeSheetSummaryDetails.length - 1) || newRow) {
            this.addNewRow(e, 0);
            return;
        }
        if (this.isEnter && detail.stt < this.pSTimeSheetSummaryDetails.length - 1) {
            const inputs = document.getElementsByTagName('input');
            for (let i = 0; i < inputs.length; i++) {
                if (inputs[i].tabIndex === 6 * (detail.stt + 1) + 1) {
                    setTimeout(() => {
                        inputs[i].focus();
                    }, 0);
                    break;
                }
            }
        }
    }

    navigateHelp() {
        this.notificationService.findHelpLink({ id: 'THCC' }).subscribe((res: HttpResponse<any>) => {
            window.open(res.body, '_blank');
        });
    }

    exportExcel() {
        const tongHopChamCongExportExcels = [];
        for (let i = 0; i < this.pSTimeSheetSummaryDetails.length; i++) {
            const tongHopChamCongExportExcel = new TongHopChamCongExportExcel();
            tongHopChamCongExportExcel.setAllFieldsFromPSTimeSummarySheetDetails(this.pSTimeSheetSummaryDetails[i], this.organizationUnits);
            tongHopChamCongExportExcels.push(tongHopChamCongExportExcel);
        }
        this.pSTimeSheetSummaryDetailsService
            .exportExcel(tongHopChamCongExportExcels, { typeID: this.pSTimeSheetSummary.typeID })
            .subscribe(res => {
                const blob = new Blob([res.body], { type: 'application/vnd.ms-excel' });
                const fileURL = URL.createObjectURL(blob);
                const link = document.createElement('a');
                document.body.appendChild(link);
                link.download = fileURL;
                link.setAttribute('style', 'display: none');
                const name = this.pSTimeSheetSummary.pSTimeSheetSummaryName
                    ? this.pSTimeSheetSummary.pSTimeSheetSummaryName
                    : this.sessionPSTimeSheet.pSTimeSheetSummaryName + '.xlsx';
                link.setAttribute('download', name);
                link.href = fileURL;
                link.click();
            });
    }

    exportPDFWebService(valueVoucher?) {
        if (!this.isUseToolIn) {
            this.exportPdf(false, 4);
        } else {
            const listID = [];
            listID.push(this.pSTimeSheetSummary.id);
            this.inChungTuHangLoatService.inChungTuHangLoat({
                typeGroupID: 31,
                typeReport: valueVoucher,
                listID,
                fromDate: null,
                toDate: null,
                paramCheckAll: false
            });
        }
    }
    checkSameOrg() {
        if (
            this.pSTimeSheetSummary.id !== null &&
            this.pSTimeSheetSummary.id !== undefined &&
            this.pSTimeSheetSummary.companyID !== null &&
            this.pSTimeSheetSummary.companyID !== undefined
        ) {
            return this.pSTimeSheetSummary.companyID === this.currentAccount.organizationUnit.id;
        } else {
            return true;
        }
    }

    loadEmployee() {
        return new Promise(resolve => {
            this.accountingObjectService.loadEmployee().subscribe(res => {
                this.employees = res.body;
                resolve(res.body);
            });
        });
    }

    loadDepartment() {
        return new Promise(resolve => {
            this.organizationUnitService.getOrgForSalary().subscribe(res => {
                this.organizationUnits = res.body;
                resolve(res.body);
            });
        });
    }
}
