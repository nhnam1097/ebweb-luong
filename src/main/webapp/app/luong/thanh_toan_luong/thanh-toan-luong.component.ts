import { AfterViewInit, Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Principal } from 'app/core';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { JhiEventManager, JhiParseLinks } from 'ng-jhipster';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { DATE_FORMAT, ITEMS_PER_PAGE } from 'app/shared';
import * as moment from 'moment';
import { SO_LAM_VIEC } from 'app/app.constants';
import { Router } from '@angular/router';
import { BaseComponent } from 'app/shared/base-component/base.component';
import { ROLE } from 'app/role.constants';
import { IBankAccountDetails } from 'app/shared/models/bank-account-details.model';
import { ISalaryPayment } from 'app/shared/models/salaryPayment.model';
import { IHistorySalaryPayment } from 'app/shared/models/historySalaryPayment.model';
import { IAccountingObject } from 'app/shared/models/accounting-object.model';
import { IOrganizationUnit } from 'app/shared/models/organization-unit.model';
import { OrganizationUnitService } from 'app/shared/services/organization-unit.service';
import { NotificationService } from 'app/shared/notification';
import { AccountingObjectService } from 'app/shared/services/accounting-object.service';
import { BankAccountDetailsService } from 'app/shared/services/bank-account-details.service';
import { UtilsService } from 'app/shared/UtilsService/Utils.service';
import { GOtherVoucherService } from 'app/shared/services/chung-tu-nghiep-vu-khac.service';

@Component({
    selector: 'eb-thanh-toan-luong',
    templateUrl: 'thanh-toan-luong.component.html',
    styleUrls: ['thanh-toan-luong.component.css']
})
export class ThanhToanLuongComponent extends BaseComponent implements OnInit, AfterViewInit {
    @ViewChild('showVoucher') showVoucher: TemplateRef<any>;
    @ViewChild('historySalaryPayment') historySalaryPayment: TemplateRef<any>;
    @ViewChild('warningAmountPayableThisTime') warningAmountPayableThisTime: TemplateRef<any>;
    warningAmountPayableThisTimeMessage: string;
    datePayment: any;
    account: any;
    paymentAccount: string;
    methodPayment: number;
    methodList: any[];
    bankAccountDetails: IBankAccountDetails[];
    salaryPayments: ISalaryPayment[];
    modalRef: NgbModalRef;
    mCPayment: any;
    mBTellerPaper: any;
    isSoTaiChinh: boolean;
    res: any;
    bankAccountDetailID: string;
    historySalaryPayments: IHistorySalaryPayment[];
    employeeID: string;
    employees: IAccountingObject[];
    organizationUnits: IOrganizationUnit[];
    fromDate: any;
    toDate: any;
    listTimeLine: any[];
    timeLineVoucher: any;
    objTimeLine: { dtBeginDate?: string; dtEndDate?: string };

    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    pageCount: any;

    linksHistory: any;
    totalItemsHistory: any;
    queryCountHistory: any;
    itemsPerPageHistory: any;
    pageHistory: any;
    predicateHistory: any;
    previousPageHistory: any;
    pageCountHistory: any;

    searchEmployeeCode: string;
    searchEmployeeName: string;
    searchDepartmentCode: string;
    listOnSelect: any[];
    listOnSelectSave: any[];
    listEdit: any[];
    paramCheckAll: boolean;
    selectedItems: number;
    listSalaryPayments: ISalaryPayment[];

    ROLE_XEM = ROLE.ThanhToanLuong_XEM;
    ROLE_THEM = ROLE.ThanhToanLuong_Them;

    constructor(
        public activeModal: NgbActiveModal,
        private principal: Principal,
        private toastr: ToastrService,
        private router: Router,
        public translate: TranslateService,
        public eventManager: JhiEventManager,
        public utilsService: UtilsService,
        public gOtherVoucherService: GOtherVoucherService,
        private modalService: NgbModal,
        private parseLinks: JhiParseLinks,
        private bankAccountDetailsService: BankAccountDetailsService,
        private accountingObjectService: AccountingObjectService,
        private organizationUnitService: OrganizationUnitService,
        private notificationService: NotificationService
    ) {
        super();
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.itemsPerPageHistory = ITEMS_PER_PAGE;
        this.page = 1;
        this.pageHistory = 1;
        this.organizationUnitService.getOrganizationUnits().subscribe((res: HttpResponse<IOrganizationUnit[]>) => {
            this.organizationUnits = res.body.sort((a, b) => a.organizationUnitCode.localeCompare(b.organizationUnitCode));
        });
        this.translate.get(['insurancePayment.cash', 'insurancePayment.accreditative']).subscribe(res => {
            this.methodList = [
                { value: 111, name: res['insurancePayment.cash'] },
                { value: 121, name: res['insurancePayment.accreditative'] }
            ];
        });
    }

    ngOnInit(): void {
        this.listSalaryPayments = [];
        this.isEdit = true;
        this.listOnSelect = [];
        this.listEdit = [];
        // this.employees = [];
        this.paramCheckAll = false;
        this.principal.identity().then(account => {
            this.account = account;
            this.datePayment = this.utilsService.ngayHachToan(this.account);
            this.historySalaryPayments = [];
            this.isSoTaiChinh = this.account.systemOption.some(x => x.code === SO_LAM_VIEC && x.data === '0');
            if (account) {
                this.salaryPayments = [];
                this.methodPayment = 111;
                this.listTimeLine = this.utilsService.getCbbTimeLine();
                this.timeLineVoucher = this.listTimeLine[4].value;
                this.selectChangeBeginDateAndEndDate(this.timeLineVoucher, account);
                this.loadAll();
            }
        });
    }

    checkErr() {
        if (!this.datePayment) {
            this.toastr.error(
                this.translate.instant('salaryPayment.datePaymentNotBeBlank'),
                this.translate.instant('ebwebApp.mCReceipt.home.message')
            );
            return false;
        }
        if ((this.listOnSelect.length > 0 && !this.paramCheckAll) || this.paramCheckAll) {
            for (let i = 0; i < this.listOnSelect.length; i++) {
                if (
                    ((this.listOnSelect[i].accumAmount <= 0 || this.listOnSelect[i].payAmount === 0) &&
                        this.listOnSelect.length > 0 &&
                        !this.paramCheckAll) ||
                    (this.paramCheckAll && this.listOnSelect.length === this.totalItems)
                ) {
                    this.toastr.error(
                        this.translate.instant('salaryPayment.errorAccept'),
                        this.translate.instant('ebwebApp.mBDeposit.message')
                    );
                    return false;
                }
            }
        } else {
            this.toastr.error(this.translate.instant('salaryPayment.errorAccept'), this.translate.instant('ebwebApp.mBDeposit.message'));
            return false;
        }
        if (this.methodPayment === 121) {
            if (!this.bankAccountDetailID) {
                this.toastr.error(
                    this.translate.instant('insurancePayment.bankAccountDetailIDNotBeBlank'),
                    this.translate.instant('ebwebApp.mCReceipt.home.message')
                );
                return false;
            }
        }
        return true;
    }

    accept() {
        if (this.checkErr()) {
            this.warningAmountPayableThisTimeMessage = '';
            if (this.paramCheckAll && this.listOnSelect.length === 0) {
                this.listSalaryPayments.forEach(item => {
                    item.check = true;
                });
            } else if (this.paramCheckAll && this.listOnSelect.length > 0) {
                this.listSalaryPayments.forEach(item => {
                    const salaryPayment = this.listOnSelect.filter(a => a.employeeID === item.employeeID);
                    if (salaryPayment && salaryPayment.length > 0) {
                        item.check = false;
                    } else {
                        item.check = true;
                    }
                });
            } else {
                this.listSalaryPayments.forEach(item => {
                    const salaryPayment = this.listOnSelect.filter(a => a.employeeID === item.employeeID);
                    if (salaryPayment && salaryPayment.length > 0) {
                        item.check = true;
                    } else {
                        item.check = false;
                    }
                });
            }
            const listCheck = this.listSalaryPayments.filter(a => a.check);
            // check những thằng check đc thay đổi số tiền thanh toán lần này
            for (let i = 0; i < listCheck.length; i++) {
                const findChangePayAmount = this.listEdit.find(a => a.employeeID === listCheck[i].employeeID);
                if (findChangePayAmount) {
                    listCheck[i].payAmount = findChangePayAmount.payAmount;
                }
            }
            let total = 0;
            // check những thằng < 0 và âm để đưa ra thông báo
            for (let i = 0; i < listCheck.length; i++) {
                if (listCheck[i].payAmount > listCheck[i].accumAmount) {
                    this.toastr.error(
                        this.translate.instant('salaryPayment.payAmountBiggerThanAccumAmount'),
                        this.translate.instant('ebwebApp.mBDeposit.message')
                    );
                    return;
                }
                if (listCheck[i].payAmount <= 0) {
                    const employee = this.employees.find(a => a.id === listCheck[i].employeeID);
                    if (employee) {
                        this.warningAmountPayableThisTimeMessage += employee.accountingObjectCode + ', ';
                    }
                }
                if (listCheck[i].payAmount > 0) {
                    total += listCheck[i].payAmount;
                }
            }
            if (total <= 0) {
                this.toastr.error(
                    this.translate.instant('salaryPayment.errorAccept'),
                    this.translate.instant('ebwebApp.mBDeposit.message')
                );
                return;
            }
            if (this.warningAmountPayableThisTimeMessage) {
                this.warningAmountPayableThisTimeMessage = this.warningAmountPayableThisTimeMessage.substring(
                    0,
                    this.warningAmountPayableThisTimeMessage.length - 2
                );
                this.modalRef = this.modalService.open(this.warningAmountPayableThisTime, { backdrop: 'static' });
                return;
            }
            if (this.checkCloseBook(this.account, this.datePayment)) {
                this.toastr.error(
                    this.translate.instant('ebwebApp.lockBook.err.checkForSave'),
                    this.translate.instant('ebwebApp.mCReceipt.error.error')
                );
                return;
            }
            const listInsurancePayments = {
                datePayment: moment(this.datePayment).format(DATE_FORMAT),
                typeVoucher: this.methodPayment,
                bankAccountDetailID: this.bankAccountDetailID ? this.bankAccountDetailID : null,
                salaryPaymentDTO: this.listOnSelect,
                listEdit: this.listEdit,
                paramCheckAll: this.paramCheckAll,
                searchEmployeeCode: this.searchEmployeeCode,
                searchEmployeeName: this.searchEmployeeName,
                searchDepartmentCode: this.searchDepartmentCode
            };
            this.gOtherVoucherService.createSalaryPayment(listInsurancePayments).subscribe(
                (res: HttpResponse<any>) => {
                    if (res && res.body.status !== 0) {
                        this.res = res.body;
                        if (res.body.status === 2) {
                            this.toastr.error(
                                this.translate.instant('insurancePayment.errorRecord'),
                                this.translate.instant('ebwebApp.mCReceipt.home.message')
                            );
                        }
                        this.listOnSelect = [];
                        this.listEdit = [];
                        this.paramCheckAll = false;
                        if (this.datePayment) {
                            this.loadAll();
                        }
                        this.modalRef = this.modalService.open(this.showVoucher, { backdrop: 'static' });
                    } else {
                        this.noVoucherExist();
                    }
                },
                (res: HttpErrorResponse) => {
                    if (res.error.errorKey !== 'noVoucherLimited') {
                        if (res.error.errorKey === 'xuatQuaTonQuy') {
                            if (this.isSoTaiChinh) {
                                this.toastr.error(
                                    this.translate.instant('insurancePayment.xuatQuaTonQuyTC'),
                                    this.translate.instant('ebwebApp.mBDeposit.message')
                                );
                            } else {
                                this.toastr.error(
                                    this.translate.instant('insurancePayment.xuatQuaTonQuyQT'),
                                    this.translate.instant('ebwebApp.mBDeposit.message')
                                );
                            }
                        } else {
                            this.toastr.error(
                                this.translate.instant('salaryPayment.errorAccept'),
                                this.translate.instant('ebwebApp.mBDeposit.message')
                            );
                        }
                    }
                }
            );
        }
    }

    continueAccept() {
        if (this.modalRef) {
            this.modalRef.close();
        }
        const listInsurancePayments = {
            datePayment: moment(this.datePayment).format(DATE_FORMAT),
            typeVoucher: this.methodPayment,
            bankAccountDetailID: this.bankAccountDetailID ? this.bankAccountDetailID : null,
            salaryPaymentDTO: this.listOnSelect,
            listEdit: this.listEdit,
            paramCheckAll: this.paramCheckAll,
            searchEmployeeCode: this.searchEmployeeCode,
            searchEmployeeName: this.searchEmployeeName,
            searchDepartmentCode: this.searchDepartmentCode
        };
        this.gOtherVoucherService.createSalaryPayment(listInsurancePayments).subscribe(
            (res: HttpResponse<any>) => {
                if (res && res.body.status !== 0) {
                    this.res = res.body;
                    if (res.body.status === 2) {
                        this.toastr.error(
                            this.translate.instant('insurancePayment.errorRecord'),
                            this.translate.instant('ebwebApp.mCReceipt.home.message')
                        );
                    }
                    if (this.datePayment) {
                        this.loadAll();
                    }
                    this.modalRef = this.modalService.open(this.showVoucher, { backdrop: 'static' });
                } else {
                    this.noVoucherExist();
                }
            },
            (res: HttpErrorResponse) => {
                if (res.error.errorKey !== 'noVoucherLimited') {
                    if (res.error.errorKey === 'xuatQuaTonQuy') {
                        if (this.isSoTaiChinh) {
                            this.toastr.error(
                                this.translate.instant('insurancePayment.xuatQuaTonQuyTC'),
                                this.translate.instant('ebwebApp.mBDeposit.message')
                            );
                        } else {
                            this.toastr.error(
                                this.translate.instant('insurancePayment.xuatQuaTonQuyQT'),
                                this.translate.instant('ebwebApp.mBDeposit.message')
                            );
                        }
                    } else {
                        this.toastr.error(
                            this.translate.instant('salaryPayment.errorAccept'),
                            this.translate.instant('ebwebApp.mBDeposit.message')
                        );
                    }
                    if (this.datePayment) {
                        this.loadAll();
                    }
                }
            }
        );
    }

    private noVoucherExist() {
        this.toastr.error(
            this.translate.instant('global.data.noVocherAlreadyExist'),
            this.translate.instant('ebwebApp.mCReceipt.home.message')
        );
    }

    newArr(length: number): any[] {
        if (length > 0) {
            return new Array(length);
        } else {
            return new Array(0);
        }
    }

    selectChangeDatePayment() {
        if (this.datePayment) {
            this.loadAll();
        } else {
            this.salaryPayments = [];
        }
    }

    close() {
        if (this.modalRef) {
            this.modalRef.close();
            const e = document.getElementById('button1');
            if (e) {
                e.focus();
            }
        }
    }

    continueViewVoucher() {
        if (this.modalRef) {
            this.modalRef.close();
        }
        if (this.activeModal) {
            this.activeModal.close();
        }
        if (this.res && this.res.typeVoucher === 111) {
            this.router.navigate(['./phieu-chi', this.res.id, 'edit']);
        } else {
            this.router.navigate(['./bao-no', this.res.id, 'edit']);
        }
    }

    selectChangeMethodPayment() {
        if (this.methodPayment === 113) {
            this.bankAccountDetailID = null;
        }
    }

    showHistory() {
        this.employeeID = null;
        this.historySalaryPayments = [];
        this.modalRef = this.modalService.open(this.historySalaryPayment, {
            backdrop: 'static',
            windowClass: 'width-80'
        });
        setTimeout(() => {
            document.getElementById('index2').focus();
        }, 0);
    }

    getEmployeeCode(i) {
        if (this.employees && this.employees.length > 0) {
            const employee = this.employees.find(a => a.id === this.salaryPayments[i].employeeID);
            if (employee) {
                return employee.accountingObjectCode;
            } else {
                return null;
            }
        }
    }

    getEmployeeName(i) {
        if (this.employees && this.employees.length > 0) {
            const employee = this.employees.find(a => a.id === this.salaryPayments[i].employeeID);
            if (employee) {
                return employee.accountingObjectName;
            } else {
                return null;
            }
        }
    }

    getEmployeeNameByID(id) {
        if (this.employees && this.employees.length > 0) {
            const employee = this.employees.find(a => a.id === id);
            if (employee) {
                return employee.accountingObjectName;
            } else {
                return null;
            }
        }
    }

    getOrganizationUnitCode(i) {
        if (this.organizationUnits && this.organizationUnits.length > 0) {
            const department = this.organizationUnits.find(a => a.id === this.salaryPayments[i].departmentID);
            if (department) {
                return department.organizationUnitCode;
            } else {
                return null;
            }
        }
    }

    selectChangeAccountingObjectBankAccount(i, id) {
        const accountingObjectBankAccount = this.salaryPayments[i].accountingObjectBankAccounts.find(a => a.id === id);
        if (accountingObjectBankAccount) {
            this.salaryPayments[i].bankName = accountingObjectBankAccount.bankName;
        } else {
            this.salaryPayments[i].bankName = null;
        }
    }

    selectChangeHistorySalaries() {
        if (moment(this.fromDate).format(DATE_FORMAT) <= moment(this.toDate).format(DATE_FORMAT)) {
            if (this.employeeID) {
                const fromDateData = this.fromDate ? moment(this.fromDate).format(DATE_FORMAT) : '';
                const toDateData = this.toDate ? moment(this.toDate).format(DATE_FORMAT) : '';
                this.gOtherVoucherService
                    .getHistorySalaries({
                        page: this.pageHistory - 1,
                        size: this.itemsPerPageHistory,
                        employeeID: this.employeeID,
                        fromDate: fromDateData,
                        toDate: toDateData
                    })
                    .subscribe((res: HttpResponse<IHistorySalaryPayment[]>) => {
                        this.linksHistory = this.parseLinks.parse(res.headers.get('link'));
                        this.totalItemsHistory = parseInt(res.headers.get('X-Total-Count'), 10);
                        this.queryCountHistory = this.totalItemsHistory;
                        this.pageCountHistory = Math.ceil(this.totalItemsHistory / this.itemsPerPageHistory);
                        this.historySalaryPayments = res.body;
                    });
            } else {
                this.toastr.error(
                    this.translate.instant('ebwebApp.baoCao.soTongHopLuongNhanVien.accountingObjectsNotBeBlank'),
                    this.translate.instant('ebwebApp.mBDeposit.message')
                );
                return;
            }
        } else {
            return this.toastr.error(
                this.translate.instant('ebwebApp.mBDeposit.fromDateMustBeLessThanToDate'),
                this.translate.instant('ebwebApp.mBDeposit.message')
            );
        }
    }

    sumFooterSalaryPayments(item) {
        if (this.selectedRow && this.salaryPayments && this.salaryPayments.length > 0) {
            let total = 0;
            for (let i = 0; i < this.salaryPayments.length; i++) {
                total += this.salaryPayments[i][item];
            }
            return total;
        }
        return '';
    }

    sumFooterHistoryPayments(item) {
        if (this.selectedRow && this.historySalaryPayments && this.historySalaryPayments.length > 0) {
            let total = 0;
            for (let i = 0; i < this.historySalaryPayments.length; i++) {
                total += this.historySalaryPayments[i][item];
            }
            return total;
        }
        return '';
    }

    selectChangeBeginDateAndEndDate(intTimeLine: String, ngayHachToan?: any) {
        if (intTimeLine) {
            this.objTimeLine = this.utilsService.getTimeLine(intTimeLine, ngayHachToan);
            this.fromDate = moment(this.objTimeLine.dtBeginDate);
            this.toDate = moment(this.objTimeLine.dtEndDate);
            // this.toDate = ngayHachToan;
        }
    }

    loadPage(page: number) {
        this.selectedRows = [];
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    loadPageHistory(page: number) {
        if (page !== this.previousPageHistory) {
            this.previousPageHistory = page;
            this.transitionHistory();
        }
    }

    selectedItemPerPage() {
        this.loadAll();
    }

    selectedItemPerPageHistory() {
        this.loadAll();
    }

    transition() {
        this.loadAll();
    }

    transitionHistory() {
        this.selectChangeHistorySalaries();
    }

    loadAll() {
        this.gOtherVoucherService
            .getSalaryPayments({
                page: this.page - 1,
                size: this.itemsPerPage,
                datePayment: moment(this.datePayment).format(DATE_FORMAT),
                searchEmployeeCode: this.searchEmployeeCode ? this.searchEmployeeCode : '',
                searchEmployeeName: this.searchEmployeeName ? this.searchEmployeeName : '',
                searchDepartmentCode: this.searchDepartmentCode ? this.searchDepartmentCode : ''
            })
            .subscribe((resGetSalaryPayments: HttpResponse<ISalaryPayment[]>) => {
                this.salaryPayments = resGetSalaryPayments.body;
                this.links = this.parseLinks.parse(resGetSalaryPayments.headers.get('link'));
                this.totalItems = parseInt(resGetSalaryPayments.headers.get('X-Total-Count'), 10);
                this.queryCount = this.totalItems;
                this.pageCount = Math.ceil(this.totalItems / this.itemsPerPage);
                if (this.paramCheckAll) {
                    this.salaryPayments.forEach(n => {
                        if (this.listOnSelect.some(m => m.employeeID === n.employeeID)) {
                            n.check = false;
                        } else {
                            n.check = true;
                        }
                        const payAmountEdit = this.listEdit.find(m => m.employeeID === n.employeeID);
                        if (payAmountEdit) {
                            n.payAmount = payAmountEdit.payAmount;
                        }
                    });
                } else {
                    this.salaryPayments.forEach(n => {
                        if (this.listOnSelect.some(m => m.employeeID === n.employeeID)) {
                            n.check = true;
                        } else {
                            n.check = false;
                        }
                        const payAmountEdit = this.listEdit.find(m => m.employeeID === n.employeeID);
                        if (payAmountEdit) {
                            n.payAmount = payAmountEdit.payAmount;
                        }
                    });
                }
                this.gOtherVoucherService
                    .findAllPaymentSalaries({
                        datePayment: moment(this.datePayment).format(DATE_FORMAT),
                        searchEmployeeCode: this.searchEmployeeCode ? this.searchEmployeeCode : '',
                        searchEmployeeName: this.searchEmployeeName ? this.searchEmployeeName : '',
                        searchDepartmentCode: this.searchDepartmentCode ? this.searchDepartmentCode : ''
                    })
                    .subscribe(async (resFindPaymentSalaries: HttpResponse<any>) => {
                        this.listSalaryPayments = resFindPaymentSalaries.body;
                        this.lstDataForSetDefault = [];
                        this.lstDataForSetDefault.push(this.listSalaryPayments);
                        await this.loadDataDefaultForOpenVoucherSync(this.lstDataForSetDefault);
                        this.bankAccountDetailsService
                            .getBankAccountDetailsNotCreditCard()
                            .subscribe((res: HttpResponse<IBankAccountDetails[]>) => {
                                this.bankAccountDetails = res.body.filter(a => a.isActive);
                            });
                    });
                for (let i = 0; i < this.salaryPayments.length; i++) {
                    if (
                        this.salaryPayments[i].accountingObjectBankAccounts &&
                        this.salaryPayments[i].accountingObjectBankAccounts.length > 0
                    ) {
                        this.salaryPayments[i].accountingObjectBankAccountID = this.salaryPayments[i].accountingObjectBankAccounts[0].id;
                        this.selectChangeAccountingObjectBankAccount(i, this.salaryPayments[i].accountingObjectBankAccountID);
                    }
                }
            });
    }

    check(salaryPayment: ISalaryPayment) {
        salaryPayment.check = !salaryPayment.check;
        if (this.paramCheckAll) {
            if (!salaryPayment.check) {
                this.listOnSelect.push(salaryPayment);
            } else {
                for (let i = 0; i < this.listOnSelect.length; i++) {
                    if (this.listOnSelect[i].employeeID === salaryPayment.employeeID) {
                        this.listOnSelect.splice(i, 1);
                        i--;
                    }
                }
            }
        } else {
            if (salaryPayment.check) {
                this.listOnSelect.push(salaryPayment);
            } else {
                for (let i = 0; i < this.listOnSelect.length; i++) {
                    if (this.listOnSelect[i].employeeID === salaryPayment.employeeID) {
                        this.listOnSelect.splice(i, 1);
                        i--;
                    }
                }
            }
        }
        if (this.totalItems === this.listOnSelect.length && salaryPayment.check) {
            this.paramCheckAll = true;
            this.listOnSelectSave = this.listOnSelect.map(object => ({ ...object }));
            this.listOnSelect = [];
            console.log(this.listOnSelectSave);
        } else {
            this.paramCheckAll = false;
            this.listOnSelect = this.listOnSelectSave.filter(a => a.employeeID !== salaryPayment.employeeID).map(object => ({ ...object }));
            console.log(this.listOnSelect);
        }
    }

    getNumberSelect() {
        if (this.paramCheckAll) {
            return this.totalItems - this.listOnSelect.length;
        } else {
            return this.listOnSelect.length;
        }
    }

    checkAll() {
        if (this.paramCheckAll && this.listOnSelect.length > 0) {
            this.paramCheckAll = true;
        } else {
            this.paramCheckAll = !this.paramCheckAll;
        }
        if (this.paramCheckAll) {
            // this.totalSelect = this.totalItems;
            this.salaryPayments.forEach(n => (n.check = true));
            this.listOnSelect = [];
        } else {
            // this.totalSelect = 0;
            this.salaryPayments.forEach(n => (n.check = false));
            this.listOnSelect = [];
        }
    }

    isCheckAll() {
        if (this.salaryPayments) {
            return this.paramCheckAll && this.totalItems - this.listOnSelect.length === this.totalItems;
        } else {
            return false;
        }
    }

    changeSearch() {
        this.page = 1;
        this.loadAll();
    }

    ngAfterViewInit(): void {
        this.inPopup = true;
        this.focusFirstInput();
        this.eventManager.broadcast({
            name: 'inpopup_luong',
            content: true
        });
    }

    closePopup() {
        this.inPopup = false;
        this.eventManager.broadcast({
            name: 'closepopup',
            content: false
        });
    }

    changeListEdit(item) {
        const exist = this.listEdit.find(a => a.employeeID === item.employeeID);
        if (!exist) {
            this.listEdit.push(item);
        } else {
            const index = this.listEdit.indexOf(item);
            this.listEdit.splice(index, 1);
            this.listEdit.push(item);
        }
    }

    moveNext($event) {
        const inputs = document.getElementsByTagName('input');
        const buttons = document.getElementsByTagName('button');
        const selects = document.getElementsByTagName('select');
        const is = document.getElementsByTagName('i');
        const curIndex = $event.path['0'].tabIndex;
        let nextIndex = curIndex;
        let j = 0;
        let isFound = false;
        // for (let i = 0; i < inputs.length; i++) {
        //     // loop over the tabs.
        //     if (inputs[i].tabIndex === curIndex && !inputs[i].disabled) {
        //
        //     }
        // }

        while (j < 15) {
            j++;
            nextIndex = curIndex + j;
            for (let i = 0; i < inputs.length; i++) {
                // loop over the tabs.
                if (inputs[i].tabIndex === nextIndex && !inputs[i].disabled) {
                    // is this our tab?
                    inputs[i].focus(); // Focus and leave.
                    inputs[i].setSelectionRange(0, inputs[i].value.length); // Focus and leave.
                    // if ($event.path['0'].type !== 'checkbox' && inputs[i].type !== 'number') {
                    //     inputs[i].selectionEnd = 0;
                    // }
                    isFound = true;
                    break;
                }
            }
            for (let i = 0; i < buttons.length; i++) {
                // loop over the tabs.
                if (buttons[i].tabIndex === nextIndex && !buttons[i].disabled) {
                    // is this our tab?
                    buttons[i].focus(); // Focus and leave.
                    isFound = true;
                    break;
                }
            }
            for (let i = 0; i < selects.length; i++) {
                // loop over the tabs.
                if (selects[i].tabIndex === nextIndex && !selects[i].disabled) {
                    // is this our tab?
                    selects[i].focus(); // Focus and leave.
                    isFound = true;
                    break;
                }
            }
            for (let k = 0; k < is.length; k++) {
                // loop over the tabs.
                if (is[k].tabIndex === nextIndex) {
                    // is this our tab?
                    is[k].focus(); // Focus and leave.
                    isFound = true;
                    break;
                }
            }
            if (isFound) {
                break;
            }
        }
    }

    movePrevious($event) {
        const inputs = document.getElementsByTagName('input');
        const selects = document.getElementsByTagName('select');
        const is = document.getElementsByTagName('i');
        const curIndex = $event.path['0'].tabIndex;
        let nextIndex = curIndex;
        let j = 0;
        let isFound = false;
        const currentPosition = $event.path['0'].selectionEnd ? $event.path['0'].selectionEnd : 0;
        if (currentPosition === 0) {
            while (j < 15) {
                j++;
                nextIndex = curIndex - j;
                for (let i = 0; i < inputs.length; i++) {
                    // loop over the tabs.
                    if (inputs[i].tabIndex === nextIndex && !inputs[i].disabled) {
                        // is this our tab?
                        inputs[i].focus(); // Focus and leave.
                        inputs[i].setSelectionRange(0, inputs[i].value.length); // Focus and leave.
                        // if (inputs[i].type !== 'number' && inputs[i].type !== 'checkbox') {
                        //     inputs[i].selectionEnd = 0;
                        // }

                        isFound = true;
                        break;
                    }
                }
                for (let i = 0; i < selects.length; i++) {
                    // loop over the tabs.
                    if (selects[i].tabIndex === nextIndex && !selects[i].disabled) {
                        // is this our tab?
                        selects[i].focus(); // Focus and leave.
                        selects[i].setSelectionRange(0, selects[i].length); // Focus and leave.
                        isFound = true;
                        break;
                    }
                }
                for (let k = 0; k < is.length; k++) {
                    // loop over the tabs.
                    if (is[k].tabIndex === nextIndex) {
                        // is this our tab?
                        is[k].focus(); // Focus and leave.
                        isFound = true;
                        break;
                    }
                }
                if (isFound) {
                    break;
                }
            }
        }
    }

    stopEvent(e) {
        e.preventDefault();
    }

    navigateHelp() {
        this.notificationService.findHelpLink({ id: 'TTL' }).subscribe((res: HttpResponse<any>) => {
            window.open(res.body, '_blank');
        });
    }
}
