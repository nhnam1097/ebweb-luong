import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { PSTimeSheetService } from './cham-cong.service';
import { PSTimeSheetComponent } from './cham-cong.component';
import { PSTimeSheetUpdateComponent } from './cham-cong-update.component';
import { PSTimeSheetDeletePopupComponent } from './cham-cong-delete-dialog.component';
import { ROLE } from 'app/role.constants';
import {IPSTimeSheet, PSTimeSheet} from 'app/shared/models/ps-time-sheet.model';

@Injectable({ providedIn: 'root' })
export class PSTimeSheetResolve implements Resolve<IPSTimeSheet> {
    constructor(private service: PSTimeSheetService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((pSTimeSheet: HttpResponse<PSTimeSheet>) => pSTimeSheet.body));
        }
        return of(new PSTimeSheet());
    }
}

export const pSTimeSheetRoute: Routes = [
    {
        path: '',
        component: PSTimeSheetComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER', ROLE.ChamCong_XEM],
            defaultSort: 'id,asc',
            pageTitle: 'ebwebApp.pSTimeSheet.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: PSTimeSheetUpdateComponent,
        resolve: {
            pSTimeSheet: PSTimeSheetResolve
        },
        data: {
            authorities: ['ROLE_USER', ROLE.ChamCong_Them],
            pageTitle: 'ebwebApp.pSTimeSheet.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: PSTimeSheetUpdateComponent,
        resolve: {
            pSTimeSheet: PSTimeSheetResolve
        },
        data: {
            authorities: ['ROLE_USER', ROLE.ChamCong_Sua],
            pageTitle: 'ebwebApp.pSTimeSheet.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const pSTimeSheetPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: PSTimeSheetDeletePopupComponent,
        resolve: {
            pSTimeSheet: PSTimeSheetResolve
        },
        data: {
            authorities: ['ROLE_USER', ROLE.ChamCong_Xoa],
            pageTitle: 'ebwebApp.pSTimeSheet.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
