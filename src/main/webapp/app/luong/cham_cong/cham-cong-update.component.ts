import {AfterViewChecked, AfterViewInit, Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {Observable, Subscription} from 'rxjs';

import {PSTimeSheetService} from './cham-cong.service';
import {TranslateService} from '@ngx-translate/core';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {ROLE} from 'app/role.constants';
import {JhiAlertService, JhiEventManager} from 'ng-jhipster';
import {ToastrService} from 'ngx-toastr';
import {DatePipe} from '@angular/common';
import {Principal} from 'app/core';
import {BaseComponent} from 'app/shared/base-component/base.component';
import {ebAuth} from 'app/shared/decorator/ebAuth.decorator';
import {DECIMAL, THOUSANDS, TYPEGROUP, TypeMultiPrintTemplate} from 'app/app.constants';
import {IPSTimeSheet} from 'app/shared/models/ps-time-sheet.model';
import {IAccountingObject} from 'app/shared/models/accounting-object.model';
import {IDataSessionStorage} from 'app/shared/models/DataSessionStorage';
import {ISessionPSTimeSheet} from 'app/shared/models/session-p-s-time-sheet';
import {OrganizationUnitService} from 'app/shared/services/organization-unit.service';
import {UtilsService} from 'app/shared/UtilsService/Utils.service';
import {InChungTuHangLoatService} from 'app/shared/in-chung-tu-hang-loat/in-chung-tu-hang-loat.service';
import {NotificationService} from 'app/shared/notification';
import {BangChamCongExportExcelDTO, ChamCongExportExcelDTO, IPSTimeSheetDetails} from 'app/shared/models/ps-time-sheet-details.model';
import {ContextMenu, IContextMenu} from 'app/shared/models/context-menu.model';
import {ITimeSheetSymbols} from 'app/shared/models/time-sheet-symbols.model';
import {IPSSalaryTaxInsuranceRegulation} from 'app/shared/models/ps-salary-tax-insurance-regulation.model';
import {PSTimeSheetDetailsService} from 'app/shared/services/ps-time-sheet-details.service';
import {AccountingObjectService} from 'app/shared/services/accounting-object.service';
import {TimeSheetSymbolsService} from 'app/shared/services/time-sheet-symbols.service';
import {QuyDinhLuongThueBaoHiemService} from 'app/shared/services/quy-dinh-luong-thue-bao-hiem.service';
import {SearchVoucher} from 'app/shared/models/SearchVoucher';

@Component({
    selector: 'eb-ps-time-sheet-update',
    templateUrl: './cham-cong-update.component.html',
    styleUrls: ['./cham-cong-update.component.css']
})
export class PSTimeSheetUpdateComponent extends BaseComponent implements OnInit, AfterViewInit, AfterViewChecked {
    private _pSTimeSheet: IPSTimeSheet;
    @ViewChild('popUpDelete') popUpDelete: TemplateRef<any>;
    TYPE_CHAM_CONG_THEO_NGAY = 750;
    TYPE_CHAM_CONG_THEO_GIO = 751;
    isSaving: boolean;
    periodPSTimeSheet: string;
    month: number;
    year: number;
    select: any;
    pSTimeSheetDetails: IPSTimeSheetDetails[];
    @ViewChild('content') content: TemplateRef<any>;
    @ViewChild('popUpMultiDelete') popUpMultiDelete: TemplateRef<any>;
    count: number;
    employeess: IAccountingObject[];
    // accountingObjects: IAccountingObject[];
    items: any;
    //  data storage provider
    warningMessage: string;
    page: number;
    itemsPerPage: number;
    pageCount: number;
    totalItems: number;
    rowNum: number;
    // sort
    predicate: any;
    searchVoucher: string;
    isCreateUrl: boolean;
    isEditUrl: boolean;
    disableAddButton: boolean;
    disableEditButton: boolean;
    dataSession: IDataSessionStorage;
    eventSubscriber: Subscription;
    modalRef: NgbModalRef;
    currentAccount: any;
    options: any;
    isHideTypeLedger: Boolean;
    pSTimeSheetCopy: IPSTimeSheet;
    pSTimeSheetDetailsCopy: IPSTimeSheetDetails[];
    isReadOnly: boolean;
    contextMenu: IContextMenu;
    totalDays: number;
    sessionPSTimeSheet: ISessionPSTimeSheet = new class implements ISessionPSTimeSheet {
        month: number;
        year: number;
        pSTimeSheetName?: string;
        typeCreate?: number;
        lastPSTimeSheetID?: string;
        autoAddNewEmployee?: boolean;
        getEmployeeNotActive?: boolean;
        listDepartmentID?: any[];
    }();
    timeSheetSymbols: ITimeSheetSymbols[];
    allTimeSheetSymbols: ITimeSheetSymbols[];
    index: number;
    currentDay: string;
    currentDayNumber: number;
    daysOfMonth: number;
    pSSalaryTaxInsuranceRegulation: IPSSalaryTaxInsuranceRegulation;
    isSunday: boolean;
    defaultAllDayTimeSheetCode: string;
    defaultHalfDayTimeSheetCode: string;
    searchTimeSheetSymbolCode: string;
    searchTimeSheetSymbolName: string;
    indexPause: any;

    optionsDirective = {
        decimal: DECIMAL,
        thousands: THOUSANDS,
        allowNegative: false,
        precision: 2
    };

    optionsDirective3 = {
        decimal: DECIMAL,
        thousands: THOUSANDS,
        allowNegative: false,
        precision: 3
    };

    ROLE_XEM = ROLE.ChamCong_XEM;
    ROLE_THEM = ROLE.ChamCong_Them;
    ROLE_SUA = ROLE.ChamCong_Sua;
    ROLE_XOA = ROLE.ChamCong_Xoa;
    ROLE_IN = ROLE.ChamCong_In;
    ROLE_KETXUAT = ROLE.ChamCong_KetXuat;

    buttonDeleteTranslate = 'ebwebApp.mBDeposit.toolTip.delete';
    buttonAddTranslate = 'ebwebApp.mBDeposit.toolTip.add';
    buttonEditTranslate = 'ebwebApp.mBDeposit.toolTip.edit';
    buttonSaveTranslate = 'ebwebApp.mBDeposit.toolTip.save';
    buttonPrintTranslate = 'ebwebApp.mBDeposit.toolTip.print';
    buttonCloseFormTranslate = 'ebwebApp.mBDeposit.toolTip.closeForm';
    TypeMultiPrintTemplate = TypeMultiPrintTemplate;
    isUseToolIn: boolean;
    day: any;

    constructor(
        private pSTimeSheetService: PSTimeSheetService,
        private pSTimeSheetDetailsService: PSTimeSheetDetailsService,
        private activatedRoute: ActivatedRoute,
        private accService: AccountingObjectService,
        private router: Router,
        private jhiAlertService: JhiAlertService,
        private organizationUnitService: OrganizationUnitService,
        private toastr: ToastrService,
        private translate: TranslateService,
        private datepipe: DatePipe,
        private eventManager: JhiEventManager,
        private modalService: NgbModal,
        private principal: Principal,
        private timeSheetSymbolsService: TimeSheetSymbolsService,
        private pSSalaryTaxInsuranceRegulationService: QuyDinhLuongThueBaoHiemService,
        private utilsServiceSecond: UtilsService,
        public utilsService: UtilsService,
        public translateService: TranslateService,
        private notificationService: NotificationService,
        private inChungTuHangLoatService: InChungTuHangLoatService
    ) {
        super();
        this.contextMenu = new ContextMenu();
        this.searchVoucher = JSON.parse(sessionStorage.getItem('dataSearchPSTimeSheet'));
        this.dataSession = JSON.parse(sessionStorage.getItem('dataSession'));
        if (this.dataSession) {
            this.page = this.dataSession.page;
            this.itemsPerPage = this.dataSession.itemsPerPage;
            this.pageCount = this.dataSession.pageCount;
            this.predicate = this.dataSession.predicate;
        } else {
            this.dataSession = null;
        }
        this.principal.identity().then(account => {
            this.currentAccount = account;
            this.isUseToolIn = this.utilsService.checkUseToolIn(account);
            this.optionsDirective3 = this.utilsService.getConfigCurrencyWithAccWithPrec(account, false, 3);
            this.optionsDirective = this.utilsService.getConfigCurrencyWithAccWithPrec(account, false, 2);
            this.timeSheetSymbolsService.getTimeSymbols().subscribe((res: HttpResponse<ITimeSheetSymbols[]>) => {
                this.timeSheetSymbols = res.body.sort((a, b) => a.timeSheetSymbolsCode.localeCompare(b.timeSheetSymbolsCode));
                this.allTimeSheetSymbols = res.body.sort((a, b) => a.timeSheetSymbolsCode.localeCompare(b.timeSheetSymbolsCode));
                res.body.forEach(item => {
                    if (item.isHalfDayDefault) {
                        this.defaultHalfDayTimeSheetCode = item.timeSheetSymbolsCode;
                    }
                    if (item.isDefault) {
                        this.defaultAllDayTimeSheetCode = item.timeSheetSymbolsCode;
                    }
                });
            });
            this.isEnter = this.currentAccount.systemOption.filter(n => n.code === 'TCKHAC_Enter')[0].data === '1';
        });
    }

    ngOnInit() {
        this.pSTimeSheetDetails = [];
        this.isSaving = false;
        this.isHideTypeLedger = false;
        this.isEditUrl = window.location.href.includes('/edit') || window.location.href.includes('/view');
        this.activatedRoute.data.subscribe(({ pSTimeSheet }) => {
            this.pSTimeSheet = pSTimeSheet;
            if (sessionStorage.getItem('sessionPSTimeSheet') && !this.pSTimeSheet.id) {
                this.sessionPSTimeSheet = JSON.parse(sessionStorage.getItem('sessionPSTimeSheet'));
                this.month = this.sessionPSTimeSheet.month;
                this.year = this.sessionPSTimeSheet.year;
            } else {
                this.month = this.pSTimeSheet.month;
                this.year = this.pSTimeSheet.year;
            }
            this.pSSalaryTaxInsuranceRegulationService
                .getDataByCompanyID({ month: this.month, year: this.year })
                .subscribe((res: HttpResponse<IPSSalaryTaxInsuranceRegulation>) => {
                    this.pSSalaryTaxInsuranceRegulation = res.body;
                });
            if (!this.pSTimeSheet.id) {
                this.disableEditButton = true;
                this.disableAddButton = false;
                if (sessionStorage.getItem('sessionPSTimeSheet')) {
                    this.sessionPSTimeSheet = JSON.parse(sessionStorage.getItem('sessionPSTimeSheet'));
                    sessionStorage.removeItem('sessionPSTimeSheet');
                } else {
                    this.router.navigate(['/cham-cong']);
                }
                if (this.sessionPSTimeSheet.typeCreate === 0) {
                    if (this.sessionPSTimeSheet.listDepartmentID !== null && this.sessionPSTimeSheet.listDepartmentID.length > 0) {
                        this.pSTimeSheetDetailsService
                            .getNewPSTimeSheets({ listDepartmentID: this.sessionPSTimeSheet.listDepartmentID })
                            .subscribe(async (res: HttpResponse<IPSTimeSheetDetails[]>) => {
                                this.pSTimeSheetDetails = res.body;
                                for (let i = 0; i < this.pSTimeSheetDetails.length; i++) {
                                    this.pSTimeSheetDetails[i].stt = i + 1;
                                }
                                this.lstDataForSetDefault = [];
                                this.lstDataForSetDefault.push(this.pSTimeSheetDetails);
                                await this.loadDataDefaultForOpenVoucherSync(this.lstDataForSetDefault);
                                for (let i = 0; i < this.pSTimeSheetDetails.length; i++) {
                                    if (this.employees && this.employees.length > 0) {
                                        const listAcc = this.employees.filter(
                                            a =>
                                                a.departmentId === this.pSTimeSheetDetails[i].departmentID ||
                                                a.id === this.pSTimeSheetDetails[i].employeeID
                                        );
                                        if (listAcc && listAcc.length > 0) {
                                            this.pSTimeSheetDetails[i].listEmployeeID = listAcc.map(object => ({ ...object }));
                                        }
                                        const acc = this.employees.find(a => a.id === this.pSTimeSheetDetails[i].employeeID);
                                        if (!acc) {
                                            this.pSTimeSheetDetails[i].listEmployeeID.push(
                                                this.accountingObjects.find(a => a.id === this.pSTimeSheetDetails[i].employeeID)
                                            );
                                        }
                                    }
                                }
                                this.daysOfMonth = new Date(this.sessionPSTimeSheet.year, this.sessionPSTimeSheet.month, 0).getDate();
                                for (let i = 0; i < this.pSTimeSheetDetails.length; i++) {
                                    for (let j = 1; j <= this.daysOfMonth; j++) {
                                        const today = new Date(this.sessionPSTimeSheet.year, this.sessionPSTimeSheet.month - 1, j);
                                        if (this.pSSalaryTaxInsuranceRegulation) {
                                            if (
                                                (this.pSSalaryTaxInsuranceRegulation.isWorkingOnMSaturday ||
                                                    this.pSSalaryTaxInsuranceRegulation.isWorkingOnNSaturday) &&
                                                today.getDay() === 6
                                            ) {
                                                if (
                                                    this.pSSalaryTaxInsuranceRegulation.isWorkingOnMSaturday &&
                                                    this.pSSalaryTaxInsuranceRegulation.isWorkingOnNSaturday
                                                ) {
                                                    const day = 'day' + j.toString();
                                                    this.pSTimeSheetDetails[i][day] =
                                                        this.sessionPSTimeSheet.typeID === this.TYPE_CHAM_CONG_THEO_NGAY
                                                            ? this.defaultAllDayTimeSheetCode
                                                            : this.defaultAllDayTimeSheetCode +
                                                              (this.pSSalaryTaxInsuranceRegulation &&
                                                              this.pSSalaryTaxInsuranceRegulation.workHoursInDay
                                                                  ? this.pSSalaryTaxInsuranceRegulation.workHoursInDay
                                                                  : '');
                                                } else {
                                                    const day = 'day' + j.toString();
                                                    if (
                                                        this.pSTimeSheet.typeID === this.TYPE_CHAM_CONG_THEO_GIO &&
                                                        this.pSSalaryTaxInsuranceRegulation &&
                                                        this.pSSalaryTaxInsuranceRegulation.workHoursInDay
                                                    ) {
                                                        this.pSTimeSheetDetails[i][day] =
                                                            this.defaultHalfDayTimeSheetCode +
                                                            (this.pSSalaryTaxInsuranceRegulation.workHoursInDay / 2).toString();
                                                    } else {
                                                        this.pSTimeSheetDetails[i][day] = this.defaultHalfDayTimeSheetCode;
                                                    }
                                                }
                                            } else if (this.pSSalaryTaxInsuranceRegulation) {
                                                if (
                                                    (this.pSSalaryTaxInsuranceRegulation.isWorkingOnMSunday ||
                                                        this.pSSalaryTaxInsuranceRegulation.isWorkingOnNSunday) &&
                                                    today.getDay() === 0
                                                ) {
                                                    if (
                                                        this.pSSalaryTaxInsuranceRegulation.isWorkingOnMSunday &&
                                                        this.pSSalaryTaxInsuranceRegulation.isWorkingOnNSunday
                                                    ) {
                                                        const day = 'day' + j.toString();
                                                        this.pSTimeSheetDetails[i][day] =
                                                            this.sessionPSTimeSheet.typeID === this.TYPE_CHAM_CONG_THEO_NGAY
                                                                ? this.defaultAllDayTimeSheetCode
                                                                : this.defaultAllDayTimeSheetCode +
                                                                  (this.pSSalaryTaxInsuranceRegulation &&
                                                                  this.pSSalaryTaxInsuranceRegulation.workHoursInDay
                                                                      ? this.pSSalaryTaxInsuranceRegulation.workHoursInDay
                                                                      : '');
                                                    } else {
                                                        const day = 'day' + j.toString();
                                                        if (
                                                            this.pSTimeSheet.typeID === this.TYPE_CHAM_CONG_THEO_GIO &&
                                                            this.pSSalaryTaxInsuranceRegulation &&
                                                            this.pSSalaryTaxInsuranceRegulation.workHoursInDay
                                                        ) {
                                                            this.pSTimeSheetDetails[i][day] =
                                                                this.defaultHalfDayTimeSheetCode +
                                                                (this.pSSalaryTaxInsuranceRegulation.workHoursInDay / 2).toString();
                                                        } else {
                                                            this.pSTimeSheetDetails[i][day] = this.defaultHalfDayTimeSheetCode;
                                                        }
                                                    }
                                                } else {
                                                    if (this.pSSalaryTaxInsuranceRegulation.isWorkingOnNSaturday && today.getDay() === 6) {
                                                        const day = 'day' + j.toString();
                                                        if (
                                                            this.pSTimeSheet.typeID === this.TYPE_CHAM_CONG_THEO_GIO &&
                                                            this.pSSalaryTaxInsuranceRegulation &&
                                                            this.pSSalaryTaxInsuranceRegulation.workHoursInDay
                                                        ) {
                                                            this.pSTimeSheetDetails[i][day] =
                                                                this.defaultHalfDayTimeSheetCode +
                                                                (this.pSSalaryTaxInsuranceRegulation.workHoursInDay / 2).toString();
                                                        } else {
                                                            this.pSTimeSheetDetails[i][day] = this.defaultHalfDayTimeSheetCode;
                                                        }
                                                    } else if (
                                                        this.pSSalaryTaxInsuranceRegulation.isWorkingOnNSunday &&
                                                        today.getDay() === 0
                                                    ) {
                                                        const day = 'day' + j.toString();
                                                        if (
                                                            this.pSTimeSheet.typeID === this.TYPE_CHAM_CONG_THEO_GIO &&
                                                            this.pSSalaryTaxInsuranceRegulation &&
                                                            this.pSSalaryTaxInsuranceRegulation.workHoursInDay
                                                        ) {
                                                            this.pSTimeSheetDetails[i][day] =
                                                                this.defaultHalfDayTimeSheetCode +
                                                                (this.pSSalaryTaxInsuranceRegulation.workHoursInDay / 2).toString();
                                                        } else {
                                                            this.pSTimeSheetDetails[i][day] = this.defaultHalfDayTimeSheetCode;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        if (today.getDay() !== 6 && today.getDay() !== 0) {
                                            const day = 'day' + j.toString();
                                            this.pSTimeSheetDetails[i][day] =
                                                this.sessionPSTimeSheet.typeID === this.TYPE_CHAM_CONG_THEO_NGAY
                                                    ? this.defaultAllDayTimeSheetCode
                                                    : this.defaultAllDayTimeSheetCode +
                                                      (this.pSSalaryTaxInsuranceRegulation &&
                                                      this.pSSalaryTaxInsuranceRegulation.workHoursInDay
                                                          ? this.pSSalaryTaxInsuranceRegulation.workHoursInDay
                                                          : '');
                                        }
                                    }
                                }
                                this.countPaidWorkingDay();
                                this.countPaidNonWorkingDay();
                                this.countWorkingDay();
                                this.countWeekendDay();
                                this.countWorkingDayNight();
                                this.countWeekendDayNight();
                                this.countHoliday();
                                this.countHolidayNight();
                                this.countTotalOverTime();
                            });
                    }
                } else {
                    this.pSTimeSheetDetailsService
                        .getNewPSTimeSheetsByExistPSTS({
                            lastPSTimeSheetID: this.sessionPSTimeSheet.lastPSTimeSheetID,
                            autoAddNewEmployee: this.sessionPSTimeSheet.autoAddNewEmployee,
                            getEmployeeNotActive: this.sessionPSTimeSheet.getEmployeeNotActive,
                            listDepartmentID: this.sessionPSTimeSheet.listDepartmentID
                        })
                        .subscribe(async (resGetNew: HttpResponse<IPSTimeSheetDetails[]>) => {
                            if (resGetNew.body != null) {
                                this.pSTimeSheetDetails = resGetNew.body;
                                for (let i = 0; i < this.pSTimeSheetDetails.length; i++) {
                                    this.pSTimeSheetDetails[i].stt = i + 1;
                                }
                                this.lstDataForSetDefault = [];
                                this.lstDataForSetDefault.push(this.pSTimeSheetDetails);
                                await this.loadDataDefaultForOpenVoucherSync(this.lstDataForSetDefault);
                                for (let i = 0; i < this.pSTimeSheetDetails.length; i++) {
                                    if (this.employees && this.employees.length > 0) {
                                        const listAcc = this.employees.filter(
                                            a => a.departmentId === this.pSTimeSheetDetails[i].departmentID
                                        );
                                        if (listAcc && listAcc.length > 0) {
                                            this.pSTimeSheetDetails[i].listEmployeeID = listAcc.map(object => ({ ...object }));
                                        }
                                        const acc = this.employees.find(a => a.id === this.pSTimeSheetDetails[i].employeeID);
                                        if (!acc) {
                                            this.pSTimeSheetDetails[i].listEmployeeID.push(
                                                this.accountingObjects.find(a => a.id === this.pSTimeSheetDetails[i].employeeID)
                                            );
                                        }
                                    }
                                }
                                this.daysOfMonth = new Date(this.sessionPSTimeSheet.year, this.sessionPSTimeSheet.month, 0).getDate();
                                if (this.pSTimeSheetDetails) {
                                    for (let i = 0; i < this.pSTimeSheetDetails.length; i++) {
                                        if (!this.pSTimeSheetDetails[i].paidWorkingDay && !this.pSTimeSheetDetails[i].paidNonWorkingDay) {
                                            for (let j = 1; j <= this.daysOfMonth; j++) {
                                                const today = new Date(this.sessionPSTimeSheet.year, this.sessionPSTimeSheet.month - 1, j);
                                                if (this.pSSalaryTaxInsuranceRegulation) {
                                                    if (this.pSSalaryTaxInsuranceRegulation.isWorkingOnMSaturday) {
                                                        if (this.pSSalaryTaxInsuranceRegulation.isWorkingOnNSaturday) {
                                                            const day = 'day' + j.toString();
                                                            this.pSTimeSheetDetails[i][day] = this.defaultAllDayTimeSheetCode;
                                                        } else {
                                                            const day = 'day' + j.toString();
                                                            this.pSTimeSheetDetails[i][day] = this.defaultHalfDayTimeSheetCode;
                                                        }
                                                    } else {
                                                        if (this.pSSalaryTaxInsuranceRegulation.isWorkingOnNSaturday) {
                                                            const day = 'day' + j.toString();
                                                            this.pSTimeSheetDetails[i][day] = this.defaultHalfDayTimeSheetCode;
                                                        }
                                                    }
                                                }
                                                if (today.getDay() !== 6 && today.getDay() !== 0) {
                                                    const day = 'day' + j.toString();
                                                    this.pSTimeSheetDetails[i][day] = this.defaultAllDayTimeSheetCode;
                                                }
                                            }
                                        }
                                    }
                                }
                                this.timeSheetSymbolsService.getTimeSymbols().subscribe((res: HttpResponse<ITimeSheetSymbols[]>) => {
                                    this.timeSheetSymbols = res.body;
                                    this.countPaidWorkingDay();
                                    this.countPaidNonWorkingDay();
                                    this.countWorkingDay();
                                    this.countWeekendDay();
                                    this.countWorkingDayNight();
                                    this.countWeekendDayNight();
                                    this.countHoliday();
                                    this.countHolidayNight();
                                    this.countTotalOverTime();
                                });
                            }
                        });
                }
                if (!this.isEditUrl) {
                    this.pSTimeSheet.typeID =
                        this.sessionPSTimeSheet && this.sessionPSTimeSheet.typeID
                            ? this.sessionPSTimeSheet.typeID
                            : this.TYPE_CHAM_CONG_THEO_NGAY;
                    this.isReadOnly = false;
                } else {
                    this.isReadOnly = true;
                }
            } else {
                // region Tiến lùi chứng từ
                this.pSTimeSheetService
                    .getIndexRow({
                        id: this.pSTimeSheet.id,
                        isNext: true,
                        typeID: this.TYPE_CHAM_CONG_THEO_NGAY,
                        searchVoucher: this.searchVoucher === undefined ? null : JSON.stringify(this.searchVoucher)
                    })
                    .subscribe(
                        (res: HttpResponse<any[]>) => {
                            this.rowNum = res.body[0];
                            if (res.body.length === 1) {
                                this.count = 1;
                            } else {
                                this.count = res.body[1];
                            }
                        },
                        (res: HttpErrorResponse) => this.onError(res.message)
                    );
                // endregion
                this.disableAddButton = true;
                this.pSTimeSheetDetails = this.pSTimeSheet.pSTimeSheetDetails.sort((a, b) => a.orderPriority - b.orderPriority);
                for (let i = 0; i < this.pSTimeSheetDetails.length; i++) {
                    this.pSTimeSheetDetails[i].stt = i + 1;
                }
                this.utilsService
                    .getDataDefaultForOpenVoucher({ lstIDEmployee: this.pSTimeSheetDetails.map(n => n.employeeID) })
                    .subscribe(async (resGetDataDefault: HttpResponse<any>) => {
                        this.lstDataForSetDefault = [];
                        this.lstDataForSetDefault.push(this.pSTimeSheetDetails);
                        await this.loadDataDefaultForOpenVoucherSync(this.lstDataForSetDefault);
                        this.employees = resGetDataDefault.body.listEmployees;
                        this.accService.getAllDTO().subscribe((res: HttpResponse<IAccountingObject[]>) => {
                            this.accountingObjects = res.body;
                            for (let i = 0; i < this.pSTimeSheetDetails.length; i++) {
                                if (this.employees && this.employees.length > 0) {
                                    const listAcc = this.employees.filter(a => a.departmentId === this.pSTimeSheetDetails[i].departmentID);
                                    if (listAcc && listAcc.length > 0) {
                                        this.pSTimeSheetDetails[i].listEmployeeID = listAcc.map(object => ({ ...object }));
                                    }
                                    const acc = this.pSTimeSheetDetails[i].listEmployeeID.find(
                                        a => a.id === this.pSTimeSheetDetails[i].employeeID
                                    );
                                    if (!acc) {
                                        this.pSTimeSheetDetails[i].listEmployeeID.push(
                                            this.accountingObjects.find(a => a.id === this.pSTimeSheetDetails[i].employeeID)
                                        );
                                    }
                                }
                            }
                        });
                    });
            }
            this.daysOfMonth = new Date(
                this.sessionPSTimeSheet.year ? this.sessionPSTimeSheet.year : this.pSTimeSheet.year,
                this.sessionPSTimeSheet.month ? this.sessionPSTimeSheet.month : this.pSTimeSheet.month,
                0
            ).getDate();
        });
        // this.organizationUnitService.getOrganizationUnits().subscribe((res: HttpResponse<IOrganizationUnit[]>) => {
        //     this.organizationUnits = res.body
        //         .filter(a => a.unitType === 4)
        //         .sort((a, b) => a.organizationUnitCode.localeCompare(b.organizationUnitCode));
        // });
        this.accService.getAllDTO().subscribe(res => {
            this.employeess = res.body.filter(a => a.isEmployee);
        });
        this.isCreateUrl = window.location.href.includes('/cham-cong/new');
        this.isEdit = this.isCreateUrl;
        this.isEdit = this.isCreateUrl;
        this.sumAfterDeleteByContextMenu();
        this.afterAddRow();
        this.registerCopyRow();
        this.copyAnnotationVertical();
        this.copyAnnotationHorizontal();
        this.eventSubscriber = this.eventManager.subscribe('saveSuccess', response => {
            this.isCbbSaveAndNew = false;
            this.registerComboboxSave(response);
            this.selectChangeAccountingObject();
            this.utilsService.setShowPopup(false);
        });
        this.eventSubscriber = this.eventManager.subscribe('saveAndNewSuccess', response => {
            this.isCbbSaveAndNew = false;
            this.registerComboboxSave(response);
            this.selectChangeAccountingObject();
            this.utilsService.setShowPopup(false);
        });
        this.eventSubscriber = this.eventManager.subscribe('closePopup', response => {
            this.utilsService.setShowPopup(response.content);
        });
        this.eventSubscribers.push(this.eventSubscriber);
        this.checkSameOrg();
    }

    closeForm() {
        event.preventDefault();
        if (!this.inPopup) {
            this.closeAll();
        }
        // if (this.pSTimeSheetCopy && !this.utilsService.isShowPopup) {
        //     if (
        //         !this.utilsService.isEquivalent(this.pSTimeSheet, this.pSTimeSheetCopy) ||
        //         !this.utilsService.isEquivalentArray(this.pSTimeSheetDetails, this.pSTimeSheetDetailsCopy)
        //     ) {
        //         if (this.isReadOnly) {
        //             this.closeAll();
        //             return;
        //         }
        //         // this.copy();
        //         if (this.modalRef) {
        //             this.modalRef.close();
        //         }
        //         this.modalRef = this.modalService.open(this.content, {backdrop: 'static'});
        //         return;
        //     } else {
        //         this.closeAll();
        //         return;
        //     }
        // } else if (!this.utilsService.isShowPopup) {
        //     this.copy();
        //     this.closeAll();
        //     return;
        // }
    }

    closeAll() {
        this.router.navigate(['/cham-cong']);
    }

    copy() {
        if (this.pSTimeSheet.id) {
            this.pSTimeSheetDetails = this.pSTimeSheet.pSTimeSheetDetails ? this.pSTimeSheet.pSTimeSheetDetails : [];
        }
        this.pSTimeSheetCopy = Object.assign({}, this.pSTimeSheet);
        if (this.pSTimeSheetDetails) {
            this.pSTimeSheetDetailsCopy = this.pSTimeSheetDetails.map(object => ({ ...object }));
        }
    }

    @ebAuth(['ROLE_ADMIN', ROLE.ChamCong_Sua])
    edit() {
        event.preventDefault();
        if (!this.isCreateUrl && !this.inPopup) {
            // && !this.utilsService.isShowPopup
            for (let i = 0; i < this.pSTimeSheetDetails.length; i++) {
                if (this.employees && this.employees.length > 0) {
                    const listAcc = this.employees.filter(a => a.departmentId === this.pSTimeSheetDetails[i].departmentID);
                    if (listAcc && listAcc.length > 0) {
                        this.pSTimeSheetDetails[i].listEmployeeID = listAcc.map(object => ({ ...object }));
                    }
                    const acc = this.employees.find(a => a.id === this.pSTimeSheetDetails[i].employeeID);
                    if (!acc) {
                        this.pSTimeSheetDetails[i].listEmployeeID.push(
                            this.accountingObjects.find(a => a.id === this.pSTimeSheetDetails[i].employeeID)
                        );
                    }
                }
            }
            this.isCreateUrl = !this.isCreateUrl;
            this.isEdit = this.isCreateUrl;
            this.disableAddButton = false;
            this.disableEditButton = true;
            this.isReadOnly = false;
            this.copy();
            // this.focusFirstInput();
            document.getElementById('index1').focus();
        }
    }

    @ebAuth(['ROLE_ADMIN', ROLE.ChamCong_Them])
    addNew($event) {
        event.preventDefault();
        if (!this.isCreateUrl && !this.inPopup) {
            // && !this.utilsService.isShowPopup
            sessionStorage.setItem('checkNewChamCong', JSON.stringify(true));
            this.router.navigate(['cham-cong']);
        }
    }

    @ebAuth(['ROLE_ADMIN', ROLE.ChamCong_Them, ROLE.ChamCong_Sua])
    save(isNew = false) {
        event.preventDefault();
        if (this.isCreateUrl && !this.inPopup) {
            this.pSTimeSheet.typeID = this.pSTimeSheet.typeID ? this.pSTimeSheet.typeID : this.sessionPSTimeSheet.typeID;
            this.pSTimeSheet.month = this.pSTimeSheet.month ? this.pSTimeSheet.month : this.sessionPSTimeSheet.month;
            this.pSTimeSheet.year = this.pSTimeSheet.year ? this.pSTimeSheet.year : this.sessionPSTimeSheet.year;
            this.pSTimeSheet.pSTimeSheetName = this.pSTimeSheet.pSTimeSheetName
                ? this.pSTimeSheet.pSTimeSheetName
                : this.sessionPSTimeSheet.pSTimeSheetName;
            this.pSTimeSheet.pSTimeSheetDetails = this.pSTimeSheetDetails;
            if (this.checkError()) {
                this.isSaving = true;
                // check is url new
                if (this.pSTimeSheet.id && this.isEditUrl) {
                    this.isCreateUrl = this.isEdit = false;
                }
                if (this.isCreateUrl && this.pSTimeSheet.id !== undefined) {
                    this.pSTimeSheet.id = undefined;
                    for (let i = 0; i < this.pSTimeSheet.pSTimeSheetDetails.length; i++) {
                        this.pSTimeSheet.pSTimeSheetDetails[i].id = undefined;
                    }
                }
                for (let i = 0; i < this.pSTimeSheet.pSTimeSheetDetails.length; i++) {
                    const contactTitle = this.employees.find(a => a.id === this.pSTimeSheet.pSTimeSheetDetails[i].employeeID);
                    if (contactTitle) {
                        this.pSTimeSheet.pSTimeSheetDetails[i].accountingObjectTitle = contactTitle.contactTitle;
                    }
                }
                if (!this.isCreateUrl && !this.isEditUrl) {
                    this.pSTimeSheet.id = undefined;
                }
                if (this.pSTimeSheet.id !== undefined) {
                    this.subscribeToSaveResponse(this.pSTimeSheetService.update(this.pSTimeSheet));
                } else {
                    this.subscribeToSaveResponse(this.pSTimeSheetService.create(this.pSTimeSheet));
                }
            } else {
            }
        }
    }

    checkError() {
        if (this.pSTimeSheetDetails.length === 0) {
            this.warningMessage =
                this.translate.instant('ebwebApp.mBDeposit.home.chiTiet') + ' ' + this.translate.instant('ebwebApp.mBDeposit.isNotNull');
            this.toastr.error(this.warningMessage.toString(), this.translate.instant('ebwebApp.mBDeposit.message'));
            return false;
        }
        for (let i = 0; i < this.pSTimeSheetDetails.length; i++) {
            if (!this.pSTimeSheetDetails[i].departmentID) {
                this.warningMessage =
                    this.translate.instant('ebwebApp.pSTimeSheetDetails.departmentID') +
                    ' ' +
                    this.translate.instant('ebwebApp.mBDeposit.isNotNull');
                this.toastr.error(this.warningMessage.toString(), this.translate.instant('ebwebApp.mBDeposit.message'));
                return false;
            }
            if (!this.pSTimeSheetDetails[i].employeeID) {
                this.warningMessage =
                    this.translate.instant('ebwebApp.pSTimeSheetDetails.employeeID') +
                    ' ' +
                    this.translate.instant('ebwebApp.mBDeposit.isNotNull');
                this.toastr.error(this.warningMessage.toString(), this.translate.instant('ebwebApp.mBDeposit.message'));
                return false;
            }
        }
        return true;
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<any>>) {
        result.subscribe(
            (res: HttpResponse<any>) => {
                this.pSTimeSheet.id = res.body.id;
                this.pSTimeSheet.month = res.body.month;
                this.pSTimeSheet.year = res.body.year;
                this.pSTimeSheet.pSTimeSheetName = res.body.pSTimeSheetName;
                this.pSTimeSheet.typeID = res.body.typeID;
                this.pSTimeSheet.pSTimeSheetDetails = res.body.pSTimeSheetDetails;
                this.pSTimeSheetDetails = res.body.pSTimeSheetDetails;
                for (let i = 0; i < this.pSTimeSheetDetails.length; i++) {
                    this.pSTimeSheetDetails[i].stt = i + 1;
                }
                this.disableEditButton = false;
                this.disableAddButton = false;
                this.onSaveSuccess();
                if (this.isEditUrl) {
                    this.toastr.success(
                        this.translate.instant('ebwebApp.pSTimeSheet.editSuccess'),
                        this.translate.instant('ebwebApp.pSTimeSheet.message')
                    );
                } else {
                    this.toastr.success(
                        this.translate.instant('ebwebApp.pSTimeSheet.insertSuccess'),
                        this.translate.instant('ebwebApp.pSTimeSheet.message')
                    );
                }
                this.router.navigate(['./cham-cong', this.pSTimeSheet.id, 'edit']);
                for (let i = 0; i < this.pSTimeSheetDetails.length; i++) {
                    if (this.employees && this.employees.length > 0) {
                        const listAcc = this.employees.filter(a => a.departmentId === this.pSTimeSheetDetails[i].departmentID);
                        if (listAcc && listAcc.length > 0) {
                            this.pSTimeSheetDetails[i].listEmployeeID = listAcc.map(object => ({ ...object }));
                        }
                        const acc = this.employees.find(a => a.id === this.pSTimeSheetDetails[i].employeeID);
                        if (!acc) {
                            this.pSTimeSheetDetails[i].listEmployeeID.push(
                                this.accountingObjects.find(a => a.id === this.pSTimeSheetDetails[i].employeeID)
                            );
                        }
                    }
                }
            },
            (res: HttpErrorResponse) => {
                if (res.error.errorKey !== 'noVoucherLimited') {
                    this.toastr.error(
                        this.translate.instant('ebwebApp.pSTimeSheet.updateError'),
                        this.translate.instant('ebwebApp.mBDeposit.message')
                    );
                }
                this.onSaveError();
                this.isCreateUrl = this.isEdit = true;
            }
        );
    }

    noVoucherExist() {
        this.toastr.error(
            this.translate.instant('global.data.noVocherAlreadyExist'),
            this.translate.instant('ebwebApp.mCReceipt.home.message')
        );
        this.isCreateUrl = this.isEdit = true;
    }

    private onSaveSuccess() {
        this.copy();
        this.isSaving = false;
        this.disableAddButton = true;
        this.disableEditButton = false;
    }

    private onSaveError() {
        this.isSaving = false;
        this.isCreateUrl = !this.isCreateUrl;
        this.isCreateUrl = this.isEdit;
    }

    get pSTimeSheet() {
        return this._pSTimeSheet;
    }

    set pSTimeSheet(pSTimeSheet: IPSTimeSheet) {
        this._pSTimeSheet = pSTimeSheet;
    }

    selectChangeAccountingObject() {}

    addNewRow(eventData: any, select: number) {
        if (this.isCreateUrl) {
            if (select === 0) {
                const ob = Object.assign({}, this.pSTimeSheetDetails[this.pSTimeSheetDetails.length - 1]);
                ob.id = undefined;
                ob.employeeID = null;
                ob.accountingObjectName = null;
                ob.stt = this.pSTimeSheetDetails.length + 1;
                this.pSTimeSheetDetails.push(ob);
                // this.pSTimeSheetDetails[this.pSTimeSheetDetails.length - 1].orderPriority = this.pSTimeSheetDetails.length - 1;
                // const nameTag: string = event.srcElement.id;
                const index: number = this.pSTimeSheetDetails.length - 1;
                const nameTag = 'field_departmentID';
                const nameTagIndex = nameTag + String(index);
                setTimeout(function() {
                    const element: HTMLElement = document.getElementById(nameTagIndex);
                    if (element) {
                        element.focus();
                    }
                }, 0);
            } else {
            }
        }
    }

    keyPress(value: number, select: number) {
        if (select === 0) {
            this.pSTimeSheetDetails.splice(value, 1);
        }
    }

    CTKTExportPDF(isDownload, typeReports: number) {
        // if (!this.isCreateUrl) {
        //     this.pSTimeSheetService
        //         .getCustomerReport({
        //             id: this.pSTimeSheet.id,
        //             typeID: ,
        //             typeReport: typeReports
        //         })
        //         .subscribe(response => {
        //             // this.showReport(response);
        //             const file = new Blob([response.body], {type: 'application/pdf'});
        //             const fileURL = window.URL.createObjectURL(file);
        //             if (isDownload) {
        //                 const link = document.createElement('a');
        //                 document.body.appendChild(link);
        //                 link.download = fileURL;
        //                 link.setAttribute('style', 'display: none');
        //                 const name = 'Bao_co.pdf';
        //                 link.setAttribute('download', name);
        //                 link.href = fileURL;
        //                 link.click();
        //             } else {
        //                 const contentDispositionHeader = response.headers.get('Content-Disposition');
        //                 const result = contentDispositionHeader
        //                     .split(';')[1]
        //                     .trim()
        //                     .split('=')[1];
        //                 const newWin = window.open(fileURL, '_blank');
        //
        //                 // add a load listener to the window so that the title gets changed on page load
        //                 newWin.addEventListener('load', function () {
        //                     newWin.document.title = result.replace(/"/g, '');
        //                     // this.router.navigate(['/report/buy']);
        //                 });
        //             }
        //         });
        //     if (typeReports === 1) {
        //         this.toastr.success(
        //             this.translate.instant('ebwebApp.pSTimeSheet.printing') +
        //             this.translate.instant('ebwebApp.pSTimeSheet.financialPaper') +
        //             '...',
        //             this.translate.instant('ebwebApp.pSTimeSheet.message')
        //         );
        //     } else if (typeReports === 2) {
        //         this.toastr.success(
        //             this.translate.instant('ebwebApp.pSTimeSheet.printing') + this.translate.instant('ebwebApp.pSTimeSheet.creditNote') + '...',
        //             this.translate.instant('ebwebApp.pSTimeSheet.message')
        //         );
        //     }
        // }
    }

    exportPdf(isDownload: boolean, typeReports: number) {
        this.utilsServiceSecond.getCustomerReportPDF(
            {
                id: this.pSTimeSheet.id,
                typeID: this.pSTimeSheet.typeID,
                typeReport: typeReports
            },
            isDownload
        );
        if (typeReports === 2) {
            this.toastr.success(
                this.translateService.instant('ebwebApp.mBDeposit.printing') +
                    this.translateService.instant('ebwebApp.pSTimeSheet.boardPSTimeSheet') +
                    '...',
                this.translateService.instant('ebwebApp.mBDeposit.message')
            );
        }
    }

    // region Tiến lùi chứng từ
    // ham lui, tien
    previousEdit() {
        // goi service get by row num
        if (this.rowNum !== this.count) {
            this.pSTimeSheetService
                .findByRowNum({
                    id: this.pSTimeSheet.id,
                    isNext: false,
                    typeID: this.TYPE_CHAM_CONG_THEO_NGAY,
                    searchVoucher: JSON.stringify(new SearchVoucher())
                })
                .subscribe(
                    (res: HttpResponse<IPSTimeSheet>) => {
                        this.navigate(res.body);
                    },
                    (res: HttpErrorResponse) => this.onError(res.message)
                );
        }
    }

    nextEdit() {
        // goi service get by row num
        if (this.rowNum !== 1) {
            this.pSTimeSheetService
                .findByRowNum({
                    id: this.pSTimeSheet.id,
                    isNext: true,
                    typeID: this.TYPE_CHAM_CONG_THEO_NGAY,
                    searchVoucher: JSON.stringify(new SearchVoucher())
                })
                .subscribe(
                    (res: HttpResponse<IPSTimeSheet>) => {
                        this.navigate(res.body);
                    },
                    (res: HttpErrorResponse) => this.onError(res.message)
                );
        }
    }

    // end of lui tien

    navigate(ipsTimeSheet: IPSTimeSheet) {
        this.router.navigate(['/cham-cong', ipsTimeSheet.id, 'edit']);
    }

    private onError(errorMessage: string) {
        this.toastr.error(this.translate.instant('ebwebApp.pSTimeSheet.error'), this.translate.instant('ebwebApp.pSTimeSheet.message'));
    }

    @ebAuth(['ROLE_ADMIN', ROLE.ChamCong_Xoa])
    delete() {
        if (this.pSTimeSheet.id && !this.isCreateUrl && !this.inPopup) {
            //     if (this.selectedRows.length > 1) {
            //         this.typeMultiAction = 0;
            //         this.modalRef = this.modalService.open(this.popUpMultiDelete, {backdrop: 'static'});
            //     } else if (
            //         this.selectedRows &&
            //         this.selectedRows.length === 1 &&
            //         !this.checkCloseBook(this.currentAccount, this.selectedRow.postedDate)
            //     ) {
            //         if (this.selectedRow && !this.selectedRow.recorded) {
            //             this.typeMultiAction = undefined;
            this.modalRef = this.modalService.open(this.popUpDelete, { backdrop: 'static' });
        }
    }

    onRightClick($event, data, selectedData, isNew, isDelete, select, currentRow, isCopy, isCopyVertical, isCopyHorizontal) {
        this.contextMenu.isNew = isNew;
        this.contextMenu.isDelete = isDelete;
        this.contextMenu.isShow = true;
        this.contextMenu.event = $event;
        this.contextMenu.data = data;
        this.contextMenu.selectedData = selectedData;
        this.contextMenu.isCopy = isCopy;
        this.contextMenu.isCopyAnnotation = isCopyVertical;
        this.contextMenu.isCopyAnnotationHorizontal = isCopyHorizontal;
        this.select = select;
        this.currentRow = currentRow;
    }

    closeContextMenu() {
        this.contextMenu.isShow = false;
    }

    sumAfterDeleteByContextMenu() {
        this.eventSubscriber = this.eventManager.subscribe('afterDeleteRow', response => {
            if (this.isEdit) {
                if (this.select === 0) {
                    this.pSTimeSheetDetails.splice(this.currentRow, 1);
                }
            }
            for (let i = 0; i < this.pSTimeSheetDetails.length; i++) {
                this.pSTimeSheetDetails[i].stt = i + 1;
            }
        });
        this.eventSubscribers.push(this.eventSubscriber);
    }

    keyDownAddRow(value: number) {
        if (this.isCreateUrl && !this.getSelectionText()) {
            const ob: IPSTimeSheetDetails = Object.assign({}, this.pSTimeSheetDetails[value]);
            ob.id = undefined;
            ob.orderPriority = undefined;
            ob.stt = this.pSTimeSheetDetails.length + 1;
            this.pSTimeSheetDetails.push(ob);
        }
    }

    afterAddRow() {
        this.eventSubscriber = this.eventManager.subscribe('afterAddNewRow', response => {
            const ob: IPSTimeSheetDetails = Object.assign({});
            ob.id = undefined;
            ob.orderPriority = undefined;
            ob.stt = this.pSTimeSheetDetails.length + 1;
            this.pSTimeSheetDetails.push(ob);
        });
        this.eventSubscribers.push(this.eventSubscriber);
    }

    registerCopyRow() {
        this.eventSubscriber = this.eventManager.subscribe('afterCopyRow', response => {
            const ob: IPSTimeSheetDetails = Object.assign({}, this.contextMenu.selectedData);
            ob.id = undefined;
            ob.orderPriority = undefined;
            this.pSTimeSheetDetails.push(ob);
        });
        this.eventSubscribers.push(this.eventSubscriber);
    }

    copyAnnotationVertical() {
        this.eventSubscriber = this.eventManager.subscribe('afterCopyAnnotationVertical', () => {
            if (this.day) {
                const day = 'day' + this.day;
                const assignValue = this.pSTimeSheetDetails[this.currentRow][day];
                this.pSTimeSheetDetails.forEach((v, i) => {
                    if (i > this.currentRow) {
                        v[day] = assignValue;
                    }
                });
                this.day = null;
                this.countPaidWorkingDay();
                this.countPaidNonWorkingDay();
                this.countWorkingDay();
                this.countWeekendDay();
                this.countWorkingDayNight();
                this.countWeekendDayNight();
                this.countHoliday();
                this.countHolidayNight();
                this.countTotalOverTime();
            }
        });
        this.eventSubscribers.push(this.eventSubscriber);
    }

    copyAnnotationHorizontal() {
        this.eventSubscriber = this.eventManager.subscribe('afterCopyAnnotationHorizontal', () => {
            if (this.day) {
                const assignValue = this.pSTimeSheetDetails[this.currentRow]['day' + this.day];
                Array.from(Array(31).keys())
                    .map(n => n + 1)
                    .forEach(n => {
                        if (this.daysOfMonth >= n && !this.checkIsSunday(n)) {
                            const d = 'day' + n;
                            this.pSTimeSheetDetails[this.currentRow][d] = assignValue;
                        }
                    });
                this.day = null;
                this.countPaidWorkingDay();
                this.countPaidNonWorkingDay();
                this.countWorkingDay();
                this.countWeekendDay();
                this.countWorkingDayNight();
                this.countWeekendDayNight();
                this.countHoliday();
                this.countHolidayNight();
                this.countTotalOverTime();
            }
        });
        this.eventSubscribers.push(this.eventSubscriber);
    }

    saveContent() {
        if (this.modalRef) {
            this.modalRef.close();
        }
        this.save(false);
    }

    close() {
        this.copy();
        if (this.modalRef) {
            this.modalRef.close();
        }
        this.closeAll();
    }

    exit() {
        if (this.modalRef) {
            this.modalRef.close();
            return;
        }
    }

    /*
  * hàm ss du lieu 2 object và 2 mảng object
  * return true: neu tat ca giong nhau
  * return fale: neu 1 trong cac object ko giong nhau
  * */
    canDeactive(): boolean {
        if (this.isReadOnly) {
            return true;
        } else {
            return (
                this.utilsService.isEquivalent(this.pSTimeSheet, this.pSTimeSheetCopy) &&
                this.utilsService.isEquivalentArray(this.pSTimeSheetDetails, this.pSTimeSheetDetailsCopy)
            );
        }
    }

    ngAfterViewInit(): void {
        if (this.isCreateUrl && document.getElementById('index1')) {
            // this.focusFirstInput();
            document.getElementById('index1').focus();
        }
    }

    ngAfterViewChecked(): void {
        this.disableInput();
    }

    @ebAuth(['ROLE_ADMIN', ROLE.ChamCong_In])
    print($event?) {
        event.preventDefault();
    }

    // saveDetails(i) {
    //     this.currentRow = i;
    //     this.details = this.pSTimeSheetDetails;
    // }
    //
    // saveParent() {
    //     this.currentRow = null;
    //     this.parent = this.pSTimeSheet;
    // }
    //
    // addDataToDetail() {
    //     this.pSTimeSheetDetails = this.details ? this.details : this.pSTimeSheetDetails;
    //     this.pSTimeSheet = this.parent ? this.parent : this.pSTimeSheet;
    // }

    continueDelete() {
        this.pSTimeSheetService.delete(this.pSTimeSheet.id).subscribe(response => {
            this.modalRef.close();
            this.toastr.success(this.translate.instant('ebwebApp.pSTimeSheet.deleteSuccessful'));
            this.router.navigate(['cham-cong']);
        });
    }

    closePopUpDelete() {
        if (this.modalRef) {
            this.modalRef.close();
        }
    }

    newArr(lenght: number): any[] {
        if (lenght > 0) {
            return new Array(lenght);
        } else {
            return new Array(0);
        }
    }

    selectChangeEmployeeID(detail) {
        if (detail.employeeID) {
            const existDepartment = this.pSTimeSheetDetails.filter(a => a.departmentID === detail.departmentID);
            if (existDepartment) {
                const existEmployee = existDepartment.filter(a => a.employeeID === detail.employeeID);
                if (existEmployee && existEmployee.length > 1) {
                    detail.employeeID = null;
                    return this.toastr.error(this.translate.instant('ebwebApp.pSTimeSheet.existEmployee'));
                }
            }
            if (this.employeess && this.pSTimeSheetDetails && this.pSTimeSheetDetails.length > 0) {
                const employee = this.employeess.find(a => a.id === detail.employeeID);
                if (employee) {
                    detail.accountingObjectName = employee.accountingObjectName;
                    detail.accountingObjectTitle = employee.contactTitle;
                }
            }
        } else {
            detail.listEmployeeID = this.employeess.filter(a => a.departmentId === detail.departmentID);
        }
    }

    isCheckAll() {
        if (this.timeSheetSymbols) {
            return this.timeSheetSymbols.every(item => item.checked) && this.timeSheetSymbols.length;
        } else {
            return false;
        }
    }

    checkAll() {
        const isCheck = this.timeSheetSymbols.filter(a => !a.isReadOnly).every(item => item.checked) && this.timeSheetSymbols.length;
        this.timeSheetSymbols.filter(a => !a.isReadOnly).forEach(item => (item.checked = !isCheck));
    }

    check(timeSheetSymbols: ITimeSheetSymbols) {
        timeSheetSymbols.checked = !timeSheetSymbols.checked;
    }

    showPopupTimeSheetSymbols(popupTimeSheetSymbols, detail, day, dayInt, index) {
        const isSaturday = this.checkInputSaturday(dayInt);
        const isSunday = this.checkInputSunday(dayInt);
        this.currentDayNumber = dayInt;
        this.indexPause = index;
        this.inPopup = true;
        if (
            // ((this.pSSalaryTaxInsuranceRegulation &&
            //     // isSaturday &&
            //     (this.pSSalaryTaxInsuranceRegulation.isWorkingOnNSaturday || this.pSSalaryTaxInsuranceRegulation.isWorkingOnMSaturday)) ||
            //     (this.pSSalaryTaxInsuranceRegulation &&
            //         // isSunday &&
            //         (this.pSSalaryTaxInsuranceRegulation.isWorkingOnNSunday || this.pSSalaryTaxInsuranceRegulation.isWorkingOnMSunday)) ||
            //     (!isSaturday && !isSunday)) &&
            this.isCreateUrl
        ) {
            this.index = this.pSTimeSheetDetails.indexOf(detail);
            this.currentDay = day;
            this.timeSheetSymbols.forEach(item => {
                item.checked = false;
                item.isReadOnly = false;
            });
            this.modalRef = this.modalService.open(popupTimeSheetSymbols, { size: 'lg', backdrop: 'static' });
            setTimeout(() => {
                document.getElementById('index-popup').focus();
            }, 100);
            const currentDay = detail[day];
            if (currentDay) {
                const acc = currentDay.split(';');
                for (let j = 0; j < this.timeSheetSymbols.length; j++) {
                    for (let l = 0; l < acc.length; l++) {
                        const acc2 = acc[l].split(' ');
                        let acc3 = '';
                        if (acc2 && acc2.length > 1) {
                            for (let k = 0; k < acc2.length - 1; k++) {
                                if (k === acc2.length - 2) {
                                    acc3 += acc2[k];
                                } else {
                                    acc3 += acc2[k] + ' ';
                                }
                            }
                            if (acc3 === this.timeSheetSymbols[j].timeSheetSymbolsCode) {
                                this.timeSheetSymbols[j].checked = true;
                                const timeOverTime = Number(acc2[acc2.length - 1]);
                                this.timeSheetSymbols[j].timeOverTime = timeOverTime;
                            }
                        } else {
                            let temp;
                            if (
                                (this.sessionPSTimeSheet.typeID === this.TYPE_CHAM_CONG_THEO_GIO ||
                                    this.pSTimeSheet.typeID === this.TYPE_CHAM_CONG_THEO_GIO) &&
                                this.timeSheetSymbols[j] &&
                                (this.timeSheetSymbols[j].isHalfDayDefault || this.timeSheetSymbols[j].isDefault) &&
                                this.pSSalaryTaxInsuranceRegulation &&
                                this.pSSalaryTaxInsuranceRegulation.workHoursInDay
                            ) {
                                if (this.timeSheetSymbols[j].isDefault) {
                                    temp =
                                        this.timeSheetSymbols[j].timeSheetSymbolsCode +
                                        this.pSSalaryTaxInsuranceRegulation.workHoursInDay.toString();
                                } else {
                                    temp =
                                        this.timeSheetSymbols[j].timeSheetSymbolsCode +
                                        (this.pSSalaryTaxInsuranceRegulation.workHoursInDay / 2).toString();
                                }
                            } else {
                                temp = this.timeSheetSymbols[j].timeSheetSymbolsCode;
                            }
                            if (acc[l] === temp) {
                                this.timeSheetSymbols[j].checked = true;
                            } else if (acc[l] === this.timeSheetSymbols[j].timeSheetSymbolsCode) {
                                this.timeSheetSymbols[j].checked = true;
                            }
                        }
                    }
                }
            } else {
            }
            // for (let j = 0; j < this.timeSheetSymbols.length; j++) {
            //     if (
            //         this.pSSalaryTaxInsuranceRegulation &&
            //         !this.pSSalaryTaxInsuranceRegulation.isWorkingOnMSaturday &&
            //         !this.pSSalaryTaxInsuranceRegulation.isWorkingOnNSaturday &&
            //         isSaturday &&
            //         (this.timeSheetSymbols[j].isDefault || this.timeSheetSymbols[j].isHalfDayDefault) && !this.timeSheetSymbols[j].checked
            //     ) {
            //         this.timeSheetSymbols[j].isReadOnly = true;
            //     }
            //     if (
            //         this.pSSalaryTaxInsuranceRegulation &&
            //         !this.pSSalaryTaxInsuranceRegulation.isWorkingOnMSunday &&
            //         !this.pSSalaryTaxInsuranceRegulation.isWorkingOnMSunday &&
            //         isSunday &&
            //         (this.timeSheetSymbols[j].isDefault || this.timeSheetSymbols[j].isHalfDayDefault)  && !this.timeSheetSymbols[j].checked
            //     ) {
            //         this.timeSheetSymbols[j].isReadOnly = true;
            //     }
            // }
            this.countPaidWorkingDay();
            this.countPaidNonWorkingDay();
            this.countWorkingDay();
            this.countWeekendDay();
            this.countWorkingDayNight();
            this.countWeekendDayNight();
            this.countHoliday();
            this.countHolidayNight();
            this.countTotalOverTime();
        }
    }

    addTimeSheetSymbols() {
        let strTimeSheet = '';
        let count = 0;
        let isContinue = true;
        let isDefault = false;
        let isHalfDay = false;
        let isDefaultFullTime = false;
        let isFullTime = false;
        // check ko dc bo trong
        this.timeSheetSymbols.forEach(item => {
            if (item.checked) {
                count++;
            }
        });
        // if (count === 0) {
        //     return this.toastr.error(this.translate.instant('ebwebApp.pSTimeSheet.timeSheetSymbolCodeNotBeBlank'));
        // }
        if (this.timeSheetSymbols && this.timeSheetSymbols.length > 0) {
            this.timeSheetSymbols.forEach(item => {
                if (item.checked) {
                    if (item.isDefault) {
                        isDefault = true;
                        isDefaultFullTime = true;
                    }
                    if (
                        !item.isDefault &&
                        !item.isHalfDay &&
                        !item.isHalfDayDefault &&
                        (item.overTimeSymbol === null || item.overTimeSymbol === undefined)
                    ) {
                        isFullTime = true;
                    }
                    if (item.isHalfDay || item.isHalfDayDefault) {
                        isHalfDay = true;
                    }
                    if (isContinue) {
                        if (count > 1) {
                            if (item.overTimeSymbol !== null && item.overTimeSymbol !== undefined) {
                                if (!item.timeOverTime) {
                                    isContinue = false;
                                    return this.toastr.error(this.translate.instant('ebwebApp.pSTimeSheet.timeOverTimeNotBeBlank'));
                                } else {
                                    strTimeSheet += item.timeSheetSymbolsCode + ' ' + item.timeOverTime.toString() + ';';
                                }
                            } else {
                                if (item.isDefault && this.pSTimeSheet.typeID === this.TYPE_CHAM_CONG_THEO_GIO) {
                                    strTimeSheet +=
                                        item.timeSheetSymbolsCode +
                                        (this.pSSalaryTaxInsuranceRegulation && this.pSSalaryTaxInsuranceRegulation.workHoursInDay
                                            ? this.pSSalaryTaxInsuranceRegulation.workHoursInDay
                                            : '') +
                                        ';';
                                } else {
                                    if (
                                        this.pSTimeSheet.typeID === this.TYPE_CHAM_CONG_THEO_GIO &&
                                        this.pSSalaryTaxInsuranceRegulation &&
                                        this.pSSalaryTaxInsuranceRegulation.workHoursInDay &&
                                        item.isHalfDayDefault
                                    ) {
                                        strTimeSheet +=
                                            this.defaultHalfDayTimeSheetCode +
                                            (this.pSSalaryTaxInsuranceRegulation.workHoursInDay / 2).toString() +
                                            ';';
                                    } else {
                                        strTimeSheet += item.timeSheetSymbolsCode + ';';
                                    }
                                }
                            }
                        } else {
                            if (item.overTimeSymbol !== null && item.overTimeSymbol !== undefined) {
                                if (!item.timeOverTime) {
                                    isContinue = false;
                                    return this.toastr.error(this.translate.instant('ebwebApp.pSTimeSheet.timeOverTimeNotBeBlank'));
                                } else {
                                    strTimeSheet += item.timeSheetSymbolsCode + ' ' + item.timeOverTime.toString();
                                }
                            } else {
                                if (item.isDefault && this.pSTimeSheet.typeID === this.TYPE_CHAM_CONG_THEO_GIO) {
                                    strTimeSheet +=
                                        item.timeSheetSymbolsCode +
                                        (this.pSSalaryTaxInsuranceRegulation && this.pSSalaryTaxInsuranceRegulation.workHoursInDay
                                            ? this.pSSalaryTaxInsuranceRegulation.workHoursInDay
                                            : '');
                                } else {
                                    if (
                                        this.pSTimeSheet.typeID === this.TYPE_CHAM_CONG_THEO_GIO &&
                                        this.pSSalaryTaxInsuranceRegulation &&
                                        this.pSSalaryTaxInsuranceRegulation.workHoursInDay &&
                                        item.isHalfDayDefault
                                    ) {
                                        strTimeSheet +=
                                            this.defaultHalfDayTimeSheetCode +
                                            (this.pSSalaryTaxInsuranceRegulation.workHoursInDay / 2).toString();
                                    } else {
                                        strTimeSheet += item.timeSheetSymbolsCode;
                                    }
                                }
                            }
                        }
                    }
                }
            });
        }
        if (isDefaultFullTime && isFullTime && isContinue) {
            return this.toastr.error(this.translate.instant('ebwebApp.pSTimeSheet.notSelectMultiFullTime'));
        }
        if (isDefault && isHalfDay && isContinue) {
            return this.toastr.error(this.translate.instant('ebwebApp.pSTimeSheet.notCheckDefaultAndIsHalfDay'));
        }
        if (isContinue) {
            this.pSTimeSheetDetails[this.index][this.currentDay] = strTimeSheet;
            const is = document.getElementsByTagName('i');
            if (this.modalRef) {
                this.inPopup = false;
                this.modalRef.close();
                for (let i = 0; i < is.length; i++) {
                    if (is[i].tabIndex === this.indexPause) {
                        is[i].focus();
                        break;
                    }
                }
            }
            this.countPaidWorkingDay();
            this.countPaidNonWorkingDay();
            this.countWorkingDay();
            this.countWeekendDay();
            this.countWorkingDayNight();
            this.countWeekendDayNight();
            this.countHoliday();
            this.countHolidayNight();
            this.countTotalOverTime();
        }
    }

    countPaidWorkingDay() {
        if (this.pSTimeSheetDetails && this.pSTimeSheetDetails.length > 0) {
            for (let i = 0; i < this.pSTimeSheetDetails.length; i++) {
                let count = 0;
                for (let k = 1; k <= this.daysOfMonth; k++) {
                    const day = 'day' + k.toString();
                    if (this.pSTimeSheetDetails[i][day]) {
                        const acc = this.pSTimeSheetDetails[i][day].split(';');
                        // if (this.timeSheetSymbols && this.timeSheetSymbols.length > 0) {
                        //     for (let j = 0; j < this.timeSheetSymbols.length; j++) {
                        for (let l = 0; l < acc.length; l++) {
                            let timeSheetSymbol;
                            if (
                                (this.sessionPSTimeSheet.typeID === this.TYPE_CHAM_CONG_THEO_GIO ||
                                    this.pSTimeSheet.typeID === this.TYPE_CHAM_CONG_THEO_GIO) &&
                                this.pSSalaryTaxInsuranceRegulation &&
                                this.pSSalaryTaxInsuranceRegulation.workHoursInDay
                            ) {
                                timeSheetSymbol = this.timeSheetSymbols.find(
                                    a =>
                                        ((a.timeSheetSymbolsCode + this.pSSalaryTaxInsuranceRegulation.workHoursInDay === acc[l] ||
                                            a.timeSheetSymbolsCode + (this.pSSalaryTaxInsuranceRegulation.workHoursInDay / 2).toString() ===
                                                acc[l]) &&
                                            (a.isDefault || a.isHalfDayDefault || a.isHalfDay)) ||
                                        (a.timeSheetSymbolsCode === acc[l] && !a.isDefault && !a.isHalfDayDefault)
                                );
                            } else {
                                timeSheetSymbol = this.timeSheetSymbols.find(a => a.timeSheetSymbolsCode === acc[l]);
                            }

                            let temp;
                            if (timeSheetSymbol) {
                                if (
                                    (this.pSTimeSheet.typeID === this.TYPE_CHAM_CONG_THEO_GIO ||
                                        this.sessionPSTimeSheet.typeID === this.TYPE_CHAM_CONG_THEO_GIO) &&
                                    timeSheetSymbol &&
                                    this.pSSalaryTaxInsuranceRegulation &&
                                    this.pSSalaryTaxInsuranceRegulation.workHoursInDay &&
                                    (timeSheetSymbol.isDefault || timeSheetSymbol.isHalfDayDefault)
                                ) {
                                    if (timeSheetSymbol.isHalfDayDefault) {
                                        temp =
                                            timeSheetSymbol.timeSheetSymbolsCode +
                                            (this.pSSalaryTaxInsuranceRegulation.workHoursInDay / 2).toString();
                                    } else {
                                        temp =
                                            timeSheetSymbol.timeSheetSymbolsCode +
                                            this.pSSalaryTaxInsuranceRegulation.workHoursInDay.toString();
                                    }
                                } else {
                                    temp = timeSheetSymbol.timeSheetSymbolsCode;
                                }
                                if (acc[l] === temp && timeSheetSymbol.salaryRate === 100 && timeSheetSymbol.isDefault) {
                                    count++;
                                } else if (
                                    acc[l] === temp &&
                                    (timeSheetSymbol.isHalfDay || timeSheetSymbol.isHalfDayDefault) &&
                                    timeSheetSymbol.salaryRate === 100
                                ) {
                                    count = count + 0.5;
                                } else if (
                                    acc[l] === temp &&
                                    (!timeSheetSymbol.isHalfDay && !timeSheetSymbol.isHalfDayDefault) &&
                                    timeSheetSymbol.salaryRate === 100
                                ) {
                                    count++;
                                }
                            }
                        }
                        // }
                        // }
                    }
                }
                this.pSTimeSheetDetails[i].paidWorkingDay =
                    this.sessionPSTimeSheet.typeID === this.TYPE_CHAM_CONG_THEO_NGAY ||
                    this.pSTimeSheet.typeID === this.TYPE_CHAM_CONG_THEO_NGAY
                        ? count
                        : count *
                          (this.pSSalaryTaxInsuranceRegulation && this.pSSalaryTaxInsuranceRegulation.workHoursInDay
                              ? this.pSSalaryTaxInsuranceRegulation.workHoursInDay
                              : 0);
            }
        }
    }

    countPaidNonWorkingDay() {
        if (this.pSTimeSheetDetails && this.pSTimeSheetDetails.length > 0) {
            for (let i = 0; i < this.pSTimeSheetDetails.length; i++) {
                let count = 0;
                for (let k = 1; k <= this.daysOfMonth; k++) {
                    const day = 'day' + k.toString();
                    if (this.pSTimeSheetDetails[i][day]) {
                        const acc = this.pSTimeSheetDetails[i][day].split(';');
                        if (this.timeSheetSymbols && this.timeSheetSymbols.length > 0) {
                            for (let j = 0; j < this.timeSheetSymbols.length; j++) {
                                for (let l = 0; l < acc.length; l++) {
                                    if (
                                        acc[l] === this.timeSheetSymbols[j].timeSheetSymbolsCode &&
                                        this.timeSheetSymbols[j].salaryRate !== 100
                                    ) {
                                        if (this.timeSheetSymbols[j].isHalfDay) {
                                            count = count + 0.5;
                                        } else {
                                            count = count + 1;
                                        }
                                        // else {
                                        //     count = count + 1;
                                        // }
                                    }
                                }
                            }
                        }
                    }
                }
                this.pSTimeSheetDetails[i].paidNonWorkingDay =
                    this.sessionPSTimeSheet.typeID === this.TYPE_CHAM_CONG_THEO_NGAY ||
                    this.pSTimeSheet.typeID === this.TYPE_CHAM_CONG_THEO_NGAY
                        ? count
                        : count *
                          (this.pSSalaryTaxInsuranceRegulation && this.pSSalaryTaxInsuranceRegulation.workHoursInDay
                              ? this.pSSalaryTaxInsuranceRegulation.workHoursInDay
                              : 1);
            }
        }
    }

    countWorkingDay() {
        if (this.pSTimeSheetDetails && this.pSTimeSheetDetails.length > 0) {
            for (let i = 0; i < this.pSTimeSheetDetails.length; i++) {
                let count = 0;
                for (let k = 1; k <= this.daysOfMonth; k++) {
                    const day = 'day' + k.toString();
                    if (this.pSTimeSheetDetails[i][day]) {
                        const acc = this.pSTimeSheetDetails[i][day].split(';');
                        if (this.timeSheetSymbols && this.timeSheetSymbols.length > 0) {
                            for (let j = 0; j < this.timeSheetSymbols.length; j++) {
                                for (let l = 0; l < acc.length; l++) {
                                    const acc2 = acc[l].split(' ');
                                    let acc3 = '';
                                    if (acc2 && acc2.length > 1) {
                                        for (let n = 0; n < acc2.length - 1; n++) {
                                            if (n === acc2.length - 2) {
                                                acc3 += acc2[n];
                                            } else {
                                                acc3 += acc2[n] + ' ';
                                            }
                                        }
                                        if (
                                            acc3 === this.timeSheetSymbols[j].timeSheetSymbolsCode &&
                                            this.timeSheetSymbols[j].isOverTime &&
                                            this.timeSheetSymbols[j].overTimeSymbol === 0
                                        ) {
                                            count = count + parseFloat(acc2[acc2.length - 1]);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if (
                    this.pSTimeSheet.typeID === this.TYPE_CHAM_CONG_THEO_NGAY &&
                    this.pSSalaryTaxInsuranceRegulation &&
                    this.pSSalaryTaxInsuranceRegulation.workHoursInDay !== null &&
                    this.pSSalaryTaxInsuranceRegulation.workHoursInDay !== undefined
                ) {
                    this.pSTimeSheetDetails[i].workingDay = count / this.pSSalaryTaxInsuranceRegulation.workHoursInDay;
                } else {
                    this.pSTimeSheetDetails[i].workingDay = count;
                }
            }
        }
    }

    countWeekendDay() {
        if (this.pSTimeSheetDetails && this.pSTimeSheetDetails.length > 0) {
            for (let i = 0; i < this.pSTimeSheetDetails.length; i++) {
                let count = 0;
                for (let k = 1; k <= this.daysOfMonth; k++) {
                    const day = 'day' + k.toString();
                    if (this.pSTimeSheetDetails[i][day]) {
                        const acc = this.pSTimeSheetDetails[i][day].split(';');
                        if (this.timeSheetSymbols && this.timeSheetSymbols.length > 0) {
                            for (let j = 0; j < this.timeSheetSymbols.length; j++) {
                                for (let l = 0; l < acc.length; l++) {
                                    const acc2 = acc[l].split(' ');
                                    let acc3 = '';
                                    if (acc2 && acc2.length > 1) {
                                        for (let n = 0; n < acc2.length - 1; n++) {
                                            if (n === acc2.length - 2) {
                                                acc3 += acc2[n];
                                            } else {
                                                acc3 += acc2[n] + ' ';
                                            }
                                        }
                                        if (
                                            acc3 === this.timeSheetSymbols[j].timeSheetSymbolsCode &&
                                            this.timeSheetSymbols[j].isOverTime &&
                                            this.timeSheetSymbols[j].overTimeSymbol === 1
                                        ) {
                                            count = count + parseFloat(acc2[acc2.length - 1]);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if (
                    this.pSTimeSheet.typeID === this.TYPE_CHAM_CONG_THEO_NGAY &&
                    this.pSSalaryTaxInsuranceRegulation &&
                    this.pSSalaryTaxInsuranceRegulation.workHoursInDay !== null &&
                    this.pSSalaryTaxInsuranceRegulation.workHoursInDay !== undefined
                ) {
                    this.pSTimeSheetDetails[i].weekendDay = count / this.pSSalaryTaxInsuranceRegulation.workHoursInDay;
                } else {
                    this.pSTimeSheetDetails[i].weekendDay = count;
                }
            }
        }
    }

    countHoliday() {
        if (this.pSTimeSheetDetails && this.pSTimeSheetDetails.length > 0) {
            for (let i = 0; i < this.pSTimeSheetDetails.length; i++) {
                let count = 0;
                for (let k = 1; k <= this.daysOfMonth; k++) {
                    const day = 'day' + k.toString();
                    if (this.pSTimeSheetDetails[i][day]) {
                        const acc = this.pSTimeSheetDetails[i][day].split(';');
                        if (this.timeSheetSymbols && this.timeSheetSymbols.length > 0) {
                            for (let j = 0; j < this.timeSheetSymbols.length; j++) {
                                for (let l = 0; l < acc.length; l++) {
                                    const acc2 = acc[l].split(' ');
                                    let acc3 = '';
                                    if (acc2 && acc2.length > 1) {
                                        for (let n = 0; n < acc2.length - 1; n++) {
                                            if (n === acc2.length - 2) {
                                                acc3 += acc2[n];
                                            } else {
                                                acc3 += acc2[n] + ' ';
                                            }
                                        }
                                        if (
                                            acc3 === this.timeSheetSymbols[j].timeSheetSymbolsCode &&
                                            this.timeSheetSymbols[j].isOverTime &&
                                            this.timeSheetSymbols[j].overTimeSymbol === 2
                                        ) {
                                            count = count + parseFloat(acc2[acc2.length - 1]);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if (
                    this.pSTimeSheet.typeID === this.TYPE_CHAM_CONG_THEO_NGAY &&
                    this.pSSalaryTaxInsuranceRegulation &&
                    this.pSSalaryTaxInsuranceRegulation.workHoursInDay !== null &&
                    this.pSSalaryTaxInsuranceRegulation.workHoursInDay !== undefined
                ) {
                    this.pSTimeSheetDetails[i].holiday = count / this.pSSalaryTaxInsuranceRegulation.workHoursInDay;
                } else {
                    this.pSTimeSheetDetails[i].holiday = count;
                }
            }
        }
    }

    countWorkingDayNight() {
        if (this.pSTimeSheetDetails && this.pSTimeSheetDetails.length > 0) {
            for (let i = 0; i < this.pSTimeSheetDetails.length; i++) {
                let count = 0;
                for (let k = 1; k <= this.daysOfMonth; k++) {
                    const day = 'day' + k.toString();
                    if (this.pSTimeSheetDetails[i][day]) {
                        const acc = this.pSTimeSheetDetails[i][day].split(';');
                        if (this.timeSheetSymbols && this.timeSheetSymbols.length > 0) {
                            for (let j = 0; j < this.timeSheetSymbols.length; j++) {
                                for (let l = 0; l < acc.length; l++) {
                                    const acc2 = acc[l].split(' ');
                                    let acc3 = '';
                                    if (acc2 && acc2.length > 1) {
                                        for (let n = 0; n < acc2.length - 1; n++) {
                                            if (n === acc2.length - 2) {
                                                acc3 += acc2[n];
                                            } else {
                                                acc3 += acc2[n] + ' ';
                                            }
                                        }
                                        if (
                                            acc3 === this.timeSheetSymbols[j].timeSheetSymbolsCode &&
                                            this.timeSheetSymbols[j].isOverTime &&
                                            this.timeSheetSymbols[j].overTimeSymbol === 3
                                        ) {
                                            count = count + parseFloat(acc2[acc2.length - 1]);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if (
                    this.pSTimeSheet.typeID === this.TYPE_CHAM_CONG_THEO_NGAY &&
                    this.pSSalaryTaxInsuranceRegulation &&
                    this.pSSalaryTaxInsuranceRegulation.workHoursInDay !== null &&
                    this.pSSalaryTaxInsuranceRegulation.workHoursInDay !== undefined
                ) {
                    this.pSTimeSheetDetails[i].workingDayNight = count / this.pSSalaryTaxInsuranceRegulation.workHoursInDay;
                } else {
                    this.pSTimeSheetDetails[i].workingDayNight = count;
                }
            }
        }
    }

    countWeekendDayNight() {
        if (this.pSTimeSheetDetails && this.pSTimeSheetDetails.length > 0) {
            for (let i = 0; i < this.pSTimeSheetDetails.length; i++) {
                let count = 0;
                for (let k = 1; k <= this.daysOfMonth; k++) {
                    const day = 'day' + k.toString();
                    if (this.pSTimeSheetDetails[i][day]) {
                        const acc = this.pSTimeSheetDetails[i][day].split(';');
                        if (this.timeSheetSymbols && this.timeSheetSymbols.length > 0) {
                            for (let j = 0; j < this.timeSheetSymbols.length; j++) {
                                for (let l = 0; l < acc.length; l++) {
                                    const acc2 = acc[l].split(' ');
                                    let acc3 = '';
                                    if (acc2 && acc2.length > 1) {
                                        for (let n = 0; n < acc2.length - 1; n++) {
                                            if (n === acc2.length - 2) {
                                                acc3 += acc2[n];
                                            } else {
                                                acc3 += acc2[n] + ' ';
                                            }
                                        }
                                        if (
                                            acc3 === this.timeSheetSymbols[j].timeSheetSymbolsCode &&
                                            this.timeSheetSymbols[j].isOverTime &&
                                            this.timeSheetSymbols[j].overTimeSymbol === 4
                                        ) {
                                            count = count + parseFloat(acc2[acc2.length - 1]);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if (
                    this.pSTimeSheet.typeID === this.TYPE_CHAM_CONG_THEO_NGAY &&
                    this.pSSalaryTaxInsuranceRegulation &&
                    this.pSSalaryTaxInsuranceRegulation.workHoursInDay !== null &&
                    this.pSSalaryTaxInsuranceRegulation.workHoursInDay !== undefined
                ) {
                    this.pSTimeSheetDetails[i].weekendDayNight = count / this.pSSalaryTaxInsuranceRegulation.workHoursInDay;
                } else {
                    this.pSTimeSheetDetails[i].weekendDayNight = count;
                }
            }
        }
    }

    countHolidayNight() {
        if (this.pSTimeSheetDetails && this.pSTimeSheetDetails.length > 0) {
            for (let i = 0; i < this.pSTimeSheetDetails.length; i++) {
                let count = 0;
                for (let k = 1; k <= this.daysOfMonth; k++) {
                    const day = 'day' + k.toString();
                    if (this.pSTimeSheetDetails[i][day]) {
                        const acc = this.pSTimeSheetDetails[i][day].split(';');
                        if (this.timeSheetSymbols && this.timeSheetSymbols.length > 0) {
                            for (let j = 0; j < this.timeSheetSymbols.length; j++) {
                                for (let l = 0; l < acc.length; l++) {
                                    const acc2 = acc[l].split(' ');
                                    let acc3 = '';
                                    if (acc2 && acc2.length > 1) {
                                        for (let n = 0; n < acc2.length - 1; n++) {
                                            if (n === acc2.length - 2) {
                                                acc3 += acc2[n];
                                            } else {
                                                acc3 += acc2[n] + ' ';
                                            }
                                        }
                                        if (
                                            acc3 === this.timeSheetSymbols[j].timeSheetSymbolsCode &&
                                            this.timeSheetSymbols[j].isOverTime &&
                                            this.timeSheetSymbols[j].overTimeSymbol === 5
                                        ) {
                                            count = count + parseFloat(acc2[acc2.length - 1]);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if (
                    this.pSTimeSheet.typeID === this.TYPE_CHAM_CONG_THEO_NGAY &&
                    this.pSSalaryTaxInsuranceRegulation &&
                    this.pSSalaryTaxInsuranceRegulation.workHoursInDay !== null &&
                    this.pSSalaryTaxInsuranceRegulation.workHoursInDay !== undefined
                ) {
                    this.pSTimeSheetDetails[i].holidayNight = count / this.pSSalaryTaxInsuranceRegulation.workHoursInDay;
                } else {
                    this.pSTimeSheetDetails[i].holidayNight = count;
                }
            }
        }
    }

    countTotalOverTime() {
        if (this.pSTimeSheetDetails && this.pSTimeSheetDetails.length > 0) {
            for (let i = 0; i < this.pSTimeSheetDetails.length; i++) {
                let count = 0;
                count =
                    this.pSTimeSheetDetails[i].workingDay +
                    this.pSTimeSheetDetails[i].weekendDay +
                    this.pSTimeSheetDetails[i].holiday +
                    this.pSTimeSheetDetails[i].workingDayNight +
                    this.pSTimeSheetDetails[i].weekendDayNight +
                    this.pSTimeSheetDetails[i].holidayNight;
                this.pSTimeSheetDetails[i].totalOverTime = count;
            }
        }
    }

    checkIsSunday(day) {
        const today = new Date(
            this.pSTimeSheet.year ? this.pSTimeSheet.year : this.sessionPSTimeSheet.year,
            this.pSTimeSheet.month ? this.pSTimeSheet.month - 1 : this.sessionPSTimeSheet.month - 1,
            day
        );
        if (today.getDay() === 0 || today.getDay() === 6) {
            return true;
        } else {
            return false;
        }
    }

    checkInputSaturday(day) {
        const today = new Date(
            this.pSTimeSheet.year ? this.pSTimeSheet.year : this.sessionPSTimeSheet.year,
            this.pSTimeSheet.month ? this.pSTimeSheet.month - 1 : this.sessionPSTimeSheet.month - 1,
            day
        );
        if (today.getDay() === 6) {
            return true;
        } else {
            return false;
        }
    }

    checkInputSunday(day) {
        const today = new Date(
            this.pSTimeSheet.year ? this.pSTimeSheet.year : this.sessionPSTimeSheet.year,
            this.pSTimeSheet.month ? this.pSTimeSheet.month - 1 : this.sessionPSTimeSheet.month - 1,
            day
        );
        if (today.getDay() === 0) {
            return true;
        } else {
            return false;
        }
    }

    checkDisabledTimeOverTime(i) {
        if (!this.timeSheetSymbols[i].checked) {
            this.timeSheetSymbols[i].timeOverTime = null;
            return true;
        }
        if (this.timeSheetSymbols[i].overTimeSymbol != null && this.timeSheetSymbols[i].overTimeSymbol !== undefined) {
            return false;
        } else {
            this.timeSheetSymbols[i].timeOverTime = null;
            return true;
        }
    }

    selectChangeDepartmentID(detail) {
        const listAcc = this.employeess.filter(a => a.departmentId === detail.departmentID);
        detail.listEmployeeID = [];
        detail.employeeID = null;
        detail.accountingObjectName = null;
        detail.accountingObjectTitle = null;
        if (listAcc && listAcc.length > 0) {
            detail.listEmployeeID = listAcc.map(object => ({ ...object }));
        }
    }

    checkIsShowButton(day) {
        if (this.isCreateUrl) {
            return true;
            // const today = new Date(
            //     this.sessionPSTimeSheet.year ? this.sessionPSTimeSheet.year : this.pSTimeSheet.year,
            //     this.sessionPSTimeSheet.month ? this.sessionPSTimeSheet.month - 1 : this.pSTimeSheet.month - 1,
            //     day
            // );
            // if (today.getDay() === 0 || today.getDay() === 6) {
            //     if (
            //         this.pSSalaryTaxInsuranceRegulation &&
            //         today.getDay() === 0 &&
            //         (this.pSSalaryTaxInsuranceRegulation.isWorkingOnNSunday || this.pSSalaryTaxInsuranceRegulation.isWorkingOnMSunday)
            //     ) {
            //         return true;
            //     }
            //     if (
            //         this.pSSalaryTaxInsuranceRegulation &&
            //         today.getDay() === 6 &&
            //         (this.pSSalaryTaxInsuranceRegulation.isWorkingOnNSaturday || this.pSSalaryTaxInsuranceRegulation.isWorkingOnMSaturday)
            //     ) {
            //         return true;
            //     }
            //     return false;
            // } else {
            //     return true;
            // }
        } else {
            return false;
        }
    }

    changeSearch() {
        this.timeSheetSymbols = this.allTimeSheetSymbols.filter(
            a =>
                a.timeSheetSymbolsCode
                    .toUpperCase()
                    .includes(this.searchTimeSheetSymbolCode ? this.searchTimeSheetSymbolCode.toUpperCase() : '') &&
                a.timeSheetSymbolsName
                    .toUpperCase()
                    .includes(this.searchTimeSheetSymbolName ? this.searchTimeSheetSymbolName.toUpperCase() : '')
        );
    }

    checkDisabled(item?: ITimeSheetSymbols) {
        const today = new Date(
            this.sessionPSTimeSheet.year ? this.sessionPSTimeSheet.year : this.pSTimeSheet.year,
            this.sessionPSTimeSheet.month ? this.sessionPSTimeSheet.month - 1 : this.pSTimeSheet.month - 1,
            this.currentDayNumber
        );
        if (today.getDay() === 0 || today.getDay() === 6) {
            if (
                this.pSSalaryTaxInsuranceRegulation &&
                today.getDay() === 0 &&
                (!this.pSSalaryTaxInsuranceRegulation.isWorkingOnNSunday && !this.pSSalaryTaxInsuranceRegulation.isWorkingOnMSunday)
            ) {
                if (item.overTimeSymbol === null || item.overTimeSymbol === undefined) {
                    return true;
                } else {
                    return false;
                }
            }
            if (
                this.pSSalaryTaxInsuranceRegulation &&
                today.getDay() === 6 &&
                (!this.pSSalaryTaxInsuranceRegulation.isWorkingOnNSaturday && !this.pSSalaryTaxInsuranceRegulation.isWorkingOnMSaturday)
            ) {
                if (item.overTimeSymbol === null || item.overTimeSymbol === undefined) {
                    return true;
                } else {
                    return false;
                }
            }
            return false;
        } else {
            return true;
        }
    }

    clickEnter(e, index) {
        let newRow = false;
        const cellCurrent = +document.activeElement.getAttribute('tabIndex');
        if (index === this.pSTimeSheetDetails.length - 1) {
            // có 34 cell được đánh index
            if (this.daysOfMonth >= 31) {
                if (34 * index + 34 === cellCurrent) {
                    newRow = true;
                }
            } else if (this.daysOfMonth >= 30) {
                if (34 * index + 33 === cellCurrent) {
                    newRow = true;
                }
            } else if (this.daysOfMonth >= 29) {
                if (34 * index + 32 === cellCurrent) {
                    newRow = true;
                }
            } else if (this.daysOfMonth >= 28) {
                if (34 * index + 31 === cellCurrent) {
                    newRow = true;
                }
            }
        }
        if ((this.isEnter && index === this.pSTimeSheetDetails.length - 1) || newRow) {
            this.addNewRow(e, 0);
            return;
        }
        if (this.isEnter && index < this.pSTimeSheetDetails.length - 1) {
            const inputs = document.getElementsByTagName('input');
            for (let i = 0; i < inputs.length; i++) {
                if (inputs[i].tabIndex === 34 * (index + 1) + 1) {
                    setTimeout(() => {
                        inputs[i].focus();
                    }, 0);
                    break;
                }
            }
        }
    }

    clickEnterTab(e, index) {
        let newRow = false;
        const cellCurrent = +document.activeElement.getAttribute('tabIndex');
        if (index === this.pSTimeSheetDetails.length - 1) {
            // có 34 cell được đánh index
            if (this.daysOfMonth >= 31) {
                if (34 * index + 34 === cellCurrent) {
                    newRow = true;
                }
            } else if (this.daysOfMonth >= 30) {
                if (34 * index + 33 === cellCurrent) {
                    newRow = true;
                }
            } else if (this.daysOfMonth >= 29) {
                if (34 * index + 32 === cellCurrent) {
                    newRow = true;
                }
            } else if (this.daysOfMonth >= 28) {
                if (34 * index + 31 === cellCurrent) {
                    newRow = true;
                }
            }
        }
        if (newRow) {
            this.addNewRow(e, 0);
            return;
        }
    }

    closeFormPopup() {
        const is = document.getElementsByTagName('i');
        if (this.modalRef) {
            this.inPopup = false;
            this.modalRef.close();
            for (let i = 0; i < is.length; i++) {
                if (is[i].tabIndex === this.indexPause) {
                    is[i].focus();
                    break;
                }
            }
        }
    }

    navigateHelp() {
        this.notificationService.findHelpLink({ id: 'CC' }).subscribe((res: HttpResponse<any>) => {
            window.open(res.body, '_blank');
        });
    }
    checkSameOrg() {
        if (
            this.pSTimeSheet.companyID !== null &&
            this.pSTimeSheet.id !== null &&
            this.pSTimeSheet.companyID !== undefined &&
            this.pSTimeSheet.id !== undefined
        ) {
            return this.pSTimeSheet.companyID === this.currentAccount.organizationUnit.id;
        } else {
            return true;
        }
    }

    exportPDFWebService(valueVoucher?) {
        if (!this.isUseToolIn) {
            this.exportPdf(false, 2);
        } else {
            const listID = [];
            listID.push(this.pSTimeSheet.id);
            this.inChungTuHangLoatService.inChungTuHangLoat({
                typeGroupID: TYPEGROUP.BANG_CHAM_CONG,
                typeReport: valueVoucher,
                listID,
                fromDate: null,
                toDate: null,
                paramCheckAll: false
            });
            this.toastr.success(
                this.translate.instant('ebwebApp.mBDeposit.printing') + this.translate.instant('ebwebApp.toolPrint.psTimeSheet') + '...',
                this.translate.instant('ebwebApp.mBDeposit.message')
            );
        }
    }

    exportExcel() {
        const chamCongDTOs: ChamCongExportExcelDTO[] = [];
        for (let i = 0; i < this.pSTimeSheetDetails.length; i++) {
            const chamCong = new ChamCongExportExcelDTO();
            chamCong.setAllFieldsFromPSTimeSheetDetails(this.pSTimeSheetDetails[i], this.organizationUnits);
            chamCongDTOs.push(chamCong);
        }
        const bangChamCongExportExcelDTO: BangChamCongExportExcelDTO = new BangChamCongExportExcelDTO();
        bangChamCongExportExcelDTO.chamCongExportExcelDTOs = chamCongDTOs;
        bangChamCongExportExcelDTO.soNgayCuaThang = this.daysOfMonth;
        for (let i = 1; i <= 8; i++) {
            if (this.checkIsSunday(i)) {
                bangChamCongExportExcelDTO.chuNhatDauTien = i + 1;
                break;
            }
        }

        this.pSTimeSheetDetailsService.exportExcel(bangChamCongExportExcelDTO).subscribe(res => {
            const blob = new Blob([res.body], { type: 'application/vnd.ms-excel' });
            const fileURL = URL.createObjectURL(blob);
            const link = document.createElement('a');
            document.body.appendChild(link);
            link.download = fileURL;
            link.setAttribute('style', 'display: none');
            const name = this.pSTimeSheet.pSTimeSheetName
                ? this.pSTimeSheet.pSTimeSheetName
                : this.sessionPSTimeSheet.pSTimeSheetName + '.xlsx';
            link.setAttribute('download', name);
            link.href = fileURL;
            link.click();
        });
    }

    eventClickCell(number) {
        this.day = number;
    }
}
