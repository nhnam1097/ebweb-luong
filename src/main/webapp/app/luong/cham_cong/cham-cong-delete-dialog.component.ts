import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Route, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { PSTimeSheetService } from './cham-cong.service';
import { BaseComponent } from 'app/shared/base-component/base.component';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import {IPSTimeSheet} from 'app/shared/models/ps-time-sheet.model';

@Component({
    selector: 'eb-ps-time-sheet-delete-dialog',
    templateUrl: './cham-cong-delete-dialog.component.html'
})
export class PSTimeSheetDeleteDialogComponent extends BaseComponent {
    pSTimeSheet: IPSTimeSheet;

    constructor(
        private pSTimeSheetService: PSTimeSheetService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager,
        private toastr: ToastrService,
        private translate: TranslateService,
        private router: Router
    ) {
        super();
    }

    clear() {
        window.history.back();
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: string) {
        this.pSTimeSheetService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'pSTimeSheetListModification',
                content: 'Deleted an pSTimeSheet'
            });
            this.activeModal.dismiss(true);
        });
        this.toastr.success(this.translate.instant('ebwebApp.mBDeposit.deleteSuccessful'));
        window.history.back();
    }
}

@Component({
    selector: 'eb-ps-time-sheet-delete-popup',
    template: ''
})
export class PSTimeSheetDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ pSTimeSheet }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(PSTimeSheetDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.pSTimeSheet = pSTimeSheet;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], {
                            replaceUrl: true,
                            queryParamsHandling: 'merge'
                        });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], {
                            replaceUrl: true,
                            queryParamsHandling: 'merge'
                        });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
