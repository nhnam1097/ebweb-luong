import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import * as moment from 'moment';
import {IPSTimeSheet} from 'app/shared/models/ps-time-sheet.model';

type EntityResponseType = HttpResponse<IPSTimeSheet>;
type EntityResponseTypeObject = HttpResponse<object>;
type EntityArrayResponseType = HttpResponse<IPSTimeSheet[]>;

@Injectable({ providedIn: 'root' })
export class PSTimeSheetService {
    private resourceUrl = SERVER_API_URL + 'api/p-s-time-sheets';

    constructor(private http: HttpClient) {}

    create(pSTimeSheet: IPSTimeSheet): Observable<EntityResponseType> {
        return this.http.post<IPSTimeSheet>(this.resourceUrl, pSTimeSheet, { observe: 'response' });
    }

    update(pSTimeSheet: IPSTimeSheet): Observable<EntityResponseType> {
        return this.http.put<IPSTimeSheet>(this.resourceUrl, pSTimeSheet, { observe: 'response' });
    }

    find(id: string): Observable<EntityResponseType> {
        return this.http.get<IPSTimeSheet>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IPSTimeSheet[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: string): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    getPSTimeSheets(): Observable<EntityArrayResponseType> {
        return this.http.get<IPSTimeSheet[]>(this.resourceUrl + '/find-all-ps-time-sheets', {
            observe: 'response'
        });
    }

    multiDelete(obj: IPSTimeSheet[]): Observable<HttpResponse<any>> {
        return this.http.post<any>(`${this.resourceUrl}/multi-delete-ps-time-sheets`, obj, { observe: 'response' });
    }

    exportPDF(req?: any): Observable<any> {
        const options = createRequestOption(req);
        let headers = new HttpHeaders();
        headers = headers.set('Accept', 'application/x-excel');
        return this.http.get(this.resourceUrl + '/export/pdf', { params: options, observe: 'response', headers, responseType: 'blob' });
    }

    getAllPSTimeSheets(): Observable<EntityArrayResponseType> {
        return this.http.get<IPSTimeSheet[]>(`${this.resourceUrl}/get-all-ps-time-sheets`, {
            observe: 'response'
        });
    }

    exportExcel(req?: any): Observable<any> {
        const options = createRequestOption(req);
        let headers = new HttpHeaders();
        headers = headers.set('Accept', 'application/x-excel');
        return this.http.get(this.resourceUrl + '/export/excel', { params: options, observe: 'response', headers, responseType: 'blob' });
    }

    searchAll(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IPSTimeSheet[]>(`${this.resourceUrl}/search-all`, {
            params: options,
            observe: 'response'
        });
    }

    getIndexRow(req?: any): Observable<EntityResponseTypeObject> {
        const options = createRequestOption(req);
        return this.http.get<object>(this.resourceUrl + '/getIndexVoucher', {
            params: options,
            observe: 'response'
        });
    }

    findByRowNum(req?: any): Observable<EntityResponseType> {
        const options = createRequestOption(req);
        return this.http.get<object>(this.resourceUrl + '/findByRowNum', {
            params: options,
            observe: 'response'
        });
    }

    upload(file: any): Observable<any> {
        const formData = new FormData();
        if (file) {
            formData.append('file', file, file.name);
        }
        // @ts-ignore
        return this.http.post<any>(`${this.resourceUrl}/upload`, formData, { observe: 'response', responseType: 'blob' });
    }

    downloadTem(): Observable<any> {
        // @ts-ignore
        return this.http.post<any>(`${this.resourceUrl}/download`, null, { observe: 'response', responseType: 'blob' });
    }
}
