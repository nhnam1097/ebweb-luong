import { AfterViewInit, Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Principal } from 'app/core';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { JhiEventManager } from 'ng-jhipster';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { DATE_FORMAT } from 'app/shared';
import * as moment from 'moment';
import { SO_LAM_VIEC } from 'app/app.constants';
import { Router } from '@angular/router';
import { ROLE } from 'app/role.constants';
import { BaseComponent } from 'app/shared/base-component/base.component';
import { IBankAccountDetails } from 'app/shared/models/bank-account-details.model';
import { IInsurancePayment } from 'app/shared/models/insurancePayment.model';
import { UtilsService } from 'app/shared/UtilsService/Utils.service';
import { GOtherVoucherService } from 'app/shared/services/chung-tu-nghiep-vu-khac.service';
import { NotificationService } from 'app/shared/notification';
import { IAccountingObjectBankAccount } from 'app/shared/models/accounting-object-bank-account.model';
import { BankAccountDetailsService } from 'app/shared/services/bank-account-details.service';
import { AccountingObjectBankAccountService } from 'app/shared/services/accounting-object-bank-account.service';

@Component({
    selector: 'eb-nop-tien-bao-hiem',
    templateUrl: 'nop-tien-bao-hiem.component.html',
    styleUrls: ['nop-tien-bao-hiem.component.css']
})
export class NopTienBaoHiemComponent extends BaseComponent implements OnInit, AfterViewInit {
    @ViewChild('showVoucher') showVoucher: TemplateRef<any>;
    @ViewChild('warningAmountPayableThisTime') warningAmountPayableThisTime: TemplateRef<any>;
    warningAmountPayableThisTimeMessage: string;
    datePayment: any;
    account: any;
    paymentAccount: string;
    methodPayment: number;
    methodList: any[];
    bankAccountDetails: IBankAccountDetails[];
    insurancePayments: IInsurancePayment[];
    modalRef: NgbModalRef;
    mCPayment: any;
    mBTellerPaper: any;
    isSoTaiChinh: boolean;
    res: any;
    bankAccountDetailID: string;
    accountingObjectID: string;
    accountingObjectBankAccountID: string;

    ROLE_XEM = ROLE.NopTienBaoHiem_XEM;
    ROLE_THEM = ROLE.NopTienBaoHiem_Them;

    constructor(
        public activeModal: NgbActiveModal,
        private principal: Principal,
        private toastr: ToastrService,
        private router: Router,
        public translate: TranslateService,
        public eventManager: JhiEventManager,
        public utilsService: UtilsService,
        public gOtherVoucherService: GOtherVoucherService,
        private modalService: NgbModal,
        private bankAccountDetailsService: BankAccountDetailsService,
        private accountingObjectBankAccountService: AccountingObjectBankAccountService,
        private notificationService: NotificationService
    ) {
        super();
        this.translate.get(['insurancePayment.cash', 'insurancePayment.accreditative']).subscribe(res => {
            this.methodList = [
                { value: 113, name: res['insurancePayment.cash'] },
                { value: 123, name: res['insurancePayment.accreditative'] }
            ];
        });
        this.inPopup = true;
    }

    ngOnInit(): void {
        this.principal.identity().then(account => {
            this.account = account;
            this.isSoTaiChinh = this.account.systemOption.some(x => x.code === SO_LAM_VIEC && x.data === '0');
            this.datePayment = this.utilsService.ngayHachToan(this.account);
            if (account) {
                this.insurancePayments = [];
                this.bankAccountDetailsService
                    .getBankAccountDetailsNotCreditCard()
                    .subscribe((res: HttpResponse<IBankAccountDetails[]>) => {
                        this.bankAccountDetails = res.body.filter(a => a.isActive);
                    });
                // this.datePayment = moment(new Date());
                this.methodPayment = 113;
                this.gOtherVoucherService
                    .getInsurancePayments({ datePayment: this.datePayment.format(DATE_FORMAT) })
                    .subscribe((res: HttpResponse<IInsurancePayment[]>) => {
                        this.insurancePayments = res.body;
                    });
            }
        });
    }

    checkErr() {
        let strDescription = '';
        if (!this.datePayment) {
            this.toastr.error(
                this.translate.instant('insurancePayment.datePaymentNotBeBlank'),
                this.translate.instant('ebwebApp.mCReceipt.home.message')
            );
            return false;
        }
        const listInsurancePayments = this.insurancePayments.filter(a => a.check);
        if (listInsurancePayments.length > 0) {
            for (let i = 0; i < listInsurancePayments.length; i++) {
                if (listInsurancePayments[i].amountPayable <= 0 || listInsurancePayments[i].amountPayableThisTime === 0) {
                    this.toastr.error(
                        this.translate.instant('insurancePayment.errorAccept'),
                        this.translate.instant('ebwebApp.mCReceipt.home.message')
                    );
                    return false;
                }
                if (listInsurancePayments[i].amountPayableThisTime > listInsurancePayments[i].amountPayable) {
                    strDescription += listInsurancePayments[i].description + ', ';
                }
            }
            if (strDescription) {
                strDescription = strDescription.substring(0, strDescription.length - 2);
                this.toastr.error(
                    this.translate.instant('insurancePayment.payAbleThisTimeBiggerThanPayAbleStr1') +
                        strDescription +
                        this.translate.instant('insurancePayment.payAbleThisTimeBiggerThanPayAbleStr2'),
                    this.translate.instant('ebwebApp.mCReceipt.home.message')
                );
                return false;
            }
        } else {
            this.toastr.error(
                this.translate.instant('insurancePayment.errorAccept'),
                this.translate.instant('ebwebApp.mCReceipt.home.message')
            );
            return false;
        }
        if (this.methodPayment === 123) {
            if (!this.bankAccountDetailID) {
                this.toastr.error(
                    this.translate.instant('insurancePayment.bankAccountDetailIDNotBeBlank'),
                    this.translate.instant('ebwebApp.mCReceipt.home.message')
                );
                return false;
            }
        }
        return true;
    }

    accept() {
        if (this.checkErr()) {
            this.warningAmountPayableThisTimeMessage = '';
            const listCheck = this.insurancePayments.filter(a => a.check);
            for (let i = 0; i < listCheck.length; i++) {
                if (listCheck[i].amountPayableThisTime < 0) {
                    this.warningAmountPayableThisTimeMessage += listCheck[i].description + ', ';
                }
            }
            if (this.warningAmountPayableThisTimeMessage) {
                this.warningAmountPayableThisTimeMessage = this.warningAmountPayableThisTimeMessage.substring(
                    0,
                    this.warningAmountPayableThisTimeMessage.length - 2
                );
                this.modalRef = this.modalService.open(this.warningAmountPayableThisTime, { backdrop: 'static' });
                return;
            }
            if (this.checkCloseBook(this.account, this.datePayment)) {
                this.toastr.error(
                    this.translate.instant('ebwebApp.lockBook.err.checkForSave'),
                    this.translate.instant('ebwebApp.mCReceipt.error.error')
                );
                return;
            }
            const listInsurancePayments = {
                datePayment: moment(this.datePayment).format(DATE_FORMAT),
                typeVoucher: this.methodPayment,
                bankAccountDetailID: this.bankAccountDetailID ? this.bankAccountDetailID : null,
                insurancePaymentDTO: this.insurancePayments.filter(a => a.check),
                accountingObjectID: this.accountingObjectID,
                accountingObjectBankAccountID: this.accountingObjectBankAccountID
            };
            this.gOtherVoucherService.createInsurancePayment(listInsurancePayments).subscribe(
                (resCreateInsurancePayment: HttpResponse<any>) => {
                    this.gOtherVoucherService
                        .getInsurancePayments({ datePayment: moment(this.datePayment).format(DATE_FORMAT) })
                        .subscribe((res: HttpResponse<IInsurancePayment[]>) => {
                            this.insurancePayments = res.body;
                        });
                    if (resCreateInsurancePayment && resCreateInsurancePayment.body.status !== 0) {
                        this.res = resCreateInsurancePayment.body;
                        if (resCreateInsurancePayment.body.status === 2) {
                            this.toastr.error(
                                this.translate.instant('insurancePayment.errorRecord'),
                                this.translate.instant('ebwebApp.mCReceipt.home.message')
                            );
                        }
                        this.modalRef = this.modalService.open(this.showVoucher, { backdrop: 'static' });
                    } else {
                        this.noVoucherExist();
                    }
                },
                (res: HttpErrorResponse) => {
                    if (res.error.errorKey !== 'noVoucherLimited') {
                        if (res.error.errorKey === 'xuatQuaTonQuy') {
                            if (this.isSoTaiChinh) {
                                this.toastr.error(
                                    this.translate.instant('insurancePayment.xuatQuaTonQuyTC'),
                                    this.translate.instant('ebwebApp.mBDeposit.message')
                                );
                            } else {
                                this.toastr.error(
                                    this.translate.instant('insurancePayment.xuatQuaTonQuyQT'),
                                    this.translate.instant('ebwebApp.mBDeposit.message')
                                );
                            }
                        }
                    }
                }
            );
        }
    }

    continueAccept() {
        if (this.modalRef) {
            this.modalRef.close();
        }
        const listInsurancePayments = {
            datePayment: moment(this.datePayment).format(DATE_FORMAT),
            typeVoucher: this.methodPayment,
            bankAccountDetailID: this.bankAccountDetailID ? this.bankAccountDetailID : null,
            insurancePaymentDTO: this.insurancePayments.filter(a => a.check),
            accountingObjectID: this.accountingObjectID,
            accountingObjectBankAccountID: this.accountingObjectBankAccountID
        };
        this.gOtherVoucherService.createInsurancePayment(listInsurancePayments).subscribe(
            (resCreateInsurancePayment: HttpResponse<any>) => {
                this.gOtherVoucherService
                    .getInsurancePayments({ datePayment: moment(this.datePayment).format(DATE_FORMAT) })
                    .subscribe((res: HttpResponse<IInsurancePayment[]>) => {
                        this.insurancePayments = res.body;
                    });
                if (resCreateInsurancePayment && resCreateInsurancePayment.body.status !== 0) {
                    this.res = resCreateInsurancePayment.body;
                    if (resCreateInsurancePayment.body.status === 2) {
                        this.toastr.error(
                            this.translate.instant('insurancePayment.errorRecord'),
                            this.translate.instant('ebwebApp.mCReceipt.home.message')
                        );
                    }
                    this.modalRef = this.modalService.open(this.showVoucher, { backdrop: 'static' });
                } else {
                    this.noVoucherExist();
                }
            },
            (res: HttpErrorResponse) => {
                if (res.error.errorKey !== 'noVoucherLimited') {
                    if (res.error.errorKey === 'xuatQuaTonQuy') {
                        if (this.isSoTaiChinh) {
                            this.toastr.error(
                                this.translate.instant('insurancePayment.xuatQuaTonQuyTC'),
                                this.translate.instant('ebwebApp.mBDeposit.message')
                            );
                        } else {
                            this.toastr.error(
                                this.translate.instant('insurancePayment.xuatQuaTonQuyQT'),
                                this.translate.instant('ebwebApp.mBDeposit.message')
                            );
                        }
                    }
                }
            }
        );
    }

    private noVoucherExist() {
        this.toastr.error(
            this.translate.instant('global.data.noVocherAlreadyExist'),
            this.translate.instant('ebwebApp.mCReceipt.home.message')
        );
    }

    newArr(length: number): any[] {
        if (length > 0) {
            return new Array(length);
        } else {
            return new Array(0);
        }
    }

    isCheckAll() {
        if (this.insurancePayments) {
            return this.insurancePayments.every(item => item.check) && this.insurancePayments.length;
        } else {
            return false;
        }
    }

    checkAll() {
        const isCheck = this.insurancePayments.every(item => item.check) && this.insurancePayments.length;
        if (!isCheck) {
            this.insurancePayments.forEach(item => (item.amountPayableThisTime = item.amountPayable));
        } else {
            this.insurancePayments.forEach(item => (item.amountPayableThisTime = 0));
        }
        this.insurancePayments.forEach(item => (item.check = !isCheck));
    }

    check(insurancePayment: IInsurancePayment, i) {
        insurancePayment.check = !insurancePayment.check;
        if (insurancePayment.check) {
            this.insurancePayments[i].amountPayableThisTime = this.insurancePayments[i].amountPayable;
        } else {
            this.insurancePayments[i].amountPayableThisTime = 0;
        }
    }

    selectChangeDatePayment() {
        if (this.datePayment) {
            this.gOtherVoucherService
                .getInsurancePayments({ datePayment: this.datePayment ? moment(this.datePayment).format(DATE_FORMAT) : null })
                .subscribe((res: HttpResponse<IInsurancePayment[]>) => {
                    this.insurancePayments = res.body;
                });
        } else {
            this.insurancePayments = [];
        }
    }

    close() {
        if (this.modalRef) {
            this.modalRef.close();
        }
    }

    continueViewVoucher() {
        if (this.modalRef) {
            this.modalRef.close();
        }
        if (this.activeModal) {
            this.activeModal.close();
        }
        if (this.res && this.res.typeVoucher === 113) {
            this.router.navigate(['./phieu-chi', this.res.id, 'edit']);
        } else {
            this.router.navigate(['./bao-no', this.res.id, 'edit']);
        }
    }

    selectChangeMethodPayment() {
        if (this.methodPayment === 113) {
            this.bankAccountDetailID = null;
        } else {
            this.selectChangeAccountingObject();
        }
    }

    selectChangeAccountingObject() {
        if (this.methodPayment === 123) {
            if (this.accountingObjectID) {
                this.accountingObjectBankAccountService
                    .getByAccountingObjectById({ accountingObjectID: this.accountingObjectID })
                    .subscribe((res: HttpResponse<IAccountingObjectBankAccount[]>) => {
                        this.accountingObjectBankAccounts = res.body;
                        if (this.accountingObjectBankAccounts && this.accountingObjectBankAccounts.length > 0) {
                            this.accountingObjectBankAccountID = this.accountingObjectBankAccounts[0].id;
                        }
                    });
            } else {
                this.accountingObjectBankAccounts = [];
            }
        }
    }

    ngAfterViewInit(): void {
        this.focusFirstInput();
        this.eventManager.broadcast({
            name: 'inpopup_luong',
            content: true
        });
    }

    closePopup() {
        this.inPopup = false;
        this.eventManager.broadcast({
            name: 'closepopup',
            content: false
        });
    }

    navigateHelp() {
        this.notificationService.findHelpLink({ id: 'NBH' }).subscribe((res: HttpResponse<any>) => {
            window.open(res.body, '_blank');
        });
    }
}
