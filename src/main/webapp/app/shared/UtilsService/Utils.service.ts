import { EventEmitter, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { createRequestOption, DATE_FORMAT } from 'app/shared';
import {
    BaoCao,
    BaoCaoDong,
    CategoryName,
    CentimeterToPixel,
    DDSo_DonGia,
    DDSo_DonGiaNT,
    DDSo_NCachHangDVi,
    DDSo_NCachHangNghin,
    DDSo_NgoaiTe,
    DDSo_SoAm,
    DDSo_SoLuong,
    DDSo_TienVND,
    DDSo_TyGia,
    DDSo_TyLe,
    DDSo_TyLePBo,
    MinStandardPercent,
    NameReport,
    NGAY_HACH_TOAN,
    percent,
    percentLandScape,
    percentPortrait,
    PicaPointToPixel,
    SERVER_API_URL,
    SO_LAM_VIEC,
    SO_NGUYEN,
    StandardPercent,
    SU_DUNG_SO_QUAN_TRI,
    TangSoChungTu,
    TypeID,
    ViTri
} from 'app/app.constants';
import { DatePipe } from '@angular/common';
import { ToastrService } from 'ngx-toastr';
import { JhiEventManager } from 'ng-jhipster';
import { map } from 'rxjs/operators';
import * as moment from 'moment';
import { Moment } from 'moment';
import { IResponeSds } from 'app/shared/models/respone-sds';
import { ViewVoucherService } from 'app/shared/ref/view-voucher.service';
import { TranslateService } from '@ngx-translate/core';
import { ROLE } from 'app/role.constants';
import { CurrencyMaskModel } from 'app/shared/directive/ng2-currency-mask/currency-mask.model';
import { IAccountingObject } from 'app/shared/models/accounting-object.model';
import { IOrganizationUnit } from 'app/shared/models/organization-unit.model';
import { IAccountList } from 'app/shared/models/account-list.model';
import { IMaterialGoods } from 'app/shared/models/material-goods.model';
import { ICostSet } from 'app/shared/models/cost-set.model';
import { IEMContract } from 'app/shared/models/em-contract.model';
import { IBankAccountDetails } from 'app/shared/models/bank-account-details.model';
import { IExpenseItem } from 'app/shared/models/expense-item.model';
import { IBudgetItem } from 'app/shared/models/budget-item.model';
import { IStatisticsCode } from 'app/shared/models/statistics-code.model';

type EntityResponseType = HttpResponse<object>;
type EntityArrayResponseType = HttpResponse<object[]>;

@Injectable({
    providedIn: 'root'
})
/*Create by Hautv*/
export class UtilsService {
    isShowPopup: boolean;
    isPopupOverride: boolean;
    checkEvent = new EventEmitter();
    listColumnsTimeLine: string[] = ['display'];
    listHeaderColumnsTimeLine: string[] = ['Thời gian'];
    dtBeginTemp: any;
    dtEndTemp: any;
    dtBeginDate: any;
    dtEndDate: any;
    objTimeLine: { dtBeginDate?: string; dtEndDate?: string };
    DOI_TUONG: any = CategoryName.DOI_TUONG;
    KHACH_HANG: any = CategoryName.KHACH_HANG;
    NHA_CUNG_CAP: any = CategoryName.NHA_CUNG_CAP;
    KHAC: any = CategoryName.KHAC;
    NHAN_VIEN: any = CategoryName.NHAN_VIEN;
    TAI_KHOAN_NGAN_HANG: any = CategoryName.TAI_KHOAN_NGAN_HANG;
    NGAN_HANG: any = CategoryName.NGAN_HANG;
    MA_THONG_KE: any = CategoryName.MA_THONG_KE;
    HOP_DONG: any = CategoryName.HOP_DONG;
    MUC_THU_CHI: any = CategoryName.MUC_THU_CHI;
    VAT_TU_HANG_HOA: any = CategoryName.VAT_TU_HANG_HOA;
    VAT_TU_HANG_HOA_PP_SERVICE: any = CategoryName.VAT_TU_HANG_HOA_PP_SERVICE;
    THE_TIN_DUNG: any = CategoryName.THE_TIN_DUNG;
    LOAI_TIEN: any = CategoryName.LOAI_TIEN;
    DON_VI_TINH: any = CategoryName.DON_VI_TINH;
    KHOAN_MUC_CHI_PHI: any = CategoryName.KHOAN_MUC_CHI_PHI;
    TAI_SAN_CO_DINH: any = CategoryName.TAI_SAN_CO_DINH;
    KHO: any = CategoryName.KHO;
    LOAI: any = CategoryName.LOAI_VAT_TU_HANG_HOA;
    DOI_TUONG_TAP_HOP_CHI_PHI: any = CategoryName.DOI_TUONG_TAP_HOP_CHI_PHI;
    PHONG_BAN: any = CategoryName.PHONG_BAN;
    PHONG_BAN_DOI_TUONG_TAP_HOP_CHI_PHI: any = CategoryName.PHONG_BAN_DOI_TUONG_TAP_HOP_CHI_PHI;
    PHUONG_THUC_VAN_CHUYEN: any = CategoryName.PHUONG_THUC_VAN_CHUYEN;
    DINH_MUC_NVL: any = CategoryName.DINH_MUC_NVL;
    LOAI_TSCD: any = CategoryName.LOAI_TSCD;
    CBB_CCTC: any = CategoryName.CBB_CCTC;
    LENH_SAN_XUAT: any = CategoryName.LENH_SAN_XUAT;

    NHOM_GIA_BAN: any = CategoryName.NHOM_GIA_BAN;
    listColumnsAutoPrinciple: string[] = ['autoPrincipleName', 'debitAccount', 'creditAccount'];
    listHeaderColumnsAutoPrinciple: string[] = ['Tên định khoản', 'Tài khoản nợ', 'Tài khoản có'];
    listHeaderColumnsAccountingObject: string[] = ['Mã đối tượng', 'Tên đối tượng', 'Địa chỉ'];
    listColumnsAccountingObjectsSearch: string[] = ['accountingObjectCode', 'accountingObjectName'];
    listHeaderColumnsAccountingObjectsSearch: string[] = ['Mã đối tượng', 'Tên đối tượng'];
    listColumnsAccountingObjects: string[] = ['accountingObjectCode', 'accountingObjectName', 'accountingObjectAddress'];
    listColumnsBankAccountDetails: string[] = ['bankAccount', 'bankName'];
    listHeaderColumnsBankAccountDetails: string[] = ['Số tài khoản ngân hàng', 'Tên ngân hàng'];
    listColumnsAccountingObjectBankAccount: string[] = ['bankAccount', 'bankName'];
    listColumnsEmployees: string[] = ['accountingObjectCode', 'accountingObjectName'];
    listHeaderColumnsEmployees: string[] = ['Mã nhân viên', 'Tên nhân viên'];
    listColumnsAccountList: string[] = ['accountNumber', 'accountName'];
    listHeaderColumnsAccountList: string[] = ['Tài khoản', 'Tên tài khoản'];
    listColumnsAccountDefaults: string[] = ['accountNumber', 'accountName'];
    listColumnsAccountDefaults1: string[] = ['accountNumber'];
    listColumnsExpenseItem: string[] = ['expenseItemCode', 'expenseItemName'];
    listHeaderColumnsExpenseItem: string[] = ['Mã khoản mục CP', 'Tên khoản mục CP'];
    listColumnsCostSet: string[] = ['costSetCode', 'costSetName'];
    listHeaderColumnsCostSet: string[] = ['Mã ĐT THCP', 'Tên ĐT THCP'];
    listColumnsOrganizationUnit: string[] = ['organizationUnitCode', 'organizationUnitName'];
    listColumnsOrganizationUnitName = ['organizationUnitName'];
    listHeaderColumnsOrganizationUnit: string[] = ['Mã phòng ban', 'Tên phòng ban'];
    listColumnsStatisticCode: string[] = ['statisticsCode', 'statisticsCodeName'];
    listHeaderColumnsStatisticCode: string[] = ['Mã thống kê', 'Tên mã thống kê'];
    listHeaderColumnsAccountDefaults: string[] = ['Tài khoản', 'Tên tài khoản'];
    listColumnsBudgetItem: string[] = ['budgetItemCode', 'budgetItemName'];
    listHeaderColumnsBudgetItem: string[] = ['Mã mục thu/chi', 'Tên mục thu/chi'];

    constructor(
        private http: HttpClient,
        private translate: TranslateService,
        private datepipe: DatePipe,
        private toastr: ToastrService,
        private viewVoucherService: ViewVoucherService,
        public eventManager: JhiEventManager
    ) {}

    setShowPopup(data) {
        this.isShowPopup = data;
        this.checkEvent.emit(data);
    }

    getAllForComboboxWithpage(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<any[]>(SERVER_API_URL + 'api/combo-box/getAll-page', { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    private convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        res.body.forEach((obj: object) => {
            obj['date'] = obj['date'] != null ? moment(obj['date']) : null;
            obj['postedDate'] = obj['postedDate'] != null ? moment(obj['postedDate']) : null;
            obj['issuedate'] = obj['issuedate'] != null ? moment(obj['issuedate']) : null;
            obj['matchdate'] = obj['matchdate'] != null ? moment(obj['matchdate']) : null;
        });
        return res;
    }

    checkUseToolIn(account?: any): boolean {
        if (account) {
            const checkToolIn = account.systemOption.find(n => n.code === 'TCKHAC_SDInBangTool');
            if ((checkToolIn && checkToolIn.data === '1') || account.login === 'luatt' || account.login === '133sds_phuongct@gmail.com') {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    getPostedDateDefault(ac): Moment {
        let dateCheck;
        const currentDate = new Date();
        if (ac) {
            const defaultData = ac.systemOption.find(x => x.code === NGAY_HACH_TOAN).data;
            if (defaultData) {
                const ngayHachToanData = moment(ac.systemOption.find(x => x.code === NGAY_HACH_TOAN).data, DATE_FORMAT);
                return ngayHachToanData;
            } else {
                dateCheck = moment(new Date(ac.yearWork, currentDate.getMonth(), currentDate.getDate()), DATE_FORMAT);
                return dateCheck;
            }
        } else {
            dateCheck = moment(new Date(ac.yearWork, currentDate.getMonth(), currentDate.getDate()), DATE_FORMAT);
            return dateCheck;
        }
    }

    getDataYearWork(currentAccount?: any) {
        const yearWork = currentAccount.yearWork;
        let getFromDate;
        let getToDate;
        if (yearWork) {
            getFromDate = moment(new Date(yearWork, 0, 1));
            getToDate = moment(new Date(yearWork, 12, 0));
        } else {
            getFromDate = '';
            getToDate = '';
        }
        return { fromDate: getFromDate, toDate: getToDate, year: yearWork };
    }

    getDataDefaultForOpenVoucher(requestGetDataDefaultDTO) {
        return this.http.post<IResponeSds>(SERVER_API_URL + 'api/combo-box/get-data-default-for-open-voucher', requestGetDataDefaultDTO, {
            observe: 'response'
        });
    }

    getConfigCurrencyWithAccWithPrec(currentAccount, noUseAllowNegative?, prec?) {
        if (prec) {
            prec = Number(prec);
        } else {
            prec = 0;
        }
        const thousands = currentAccount.systemOption.find(x => x.code === DDSo_NCachHangNghin && x.data).data;
        const decimal = currentAccount.systemOption.find(x => x.code === DDSo_NCachHangDVi && x.data).data;
        const negative = currentAccount.systemOption.find(x => x.code === DDSo_SoAm && x.data).data === '0';
        if (noUseAllowNegative) {
            return this.getConfigCurrency(prec, thousands, decimal, false, negative);
        } else {
            return this.getConfigCurrency(prec, thousands, decimal, true, negative);
        }
    }

    getConfigCurrency(prec?, thousands?, decimal?, allowNegative?, negative?) {
        if (!prec) {
            prec = 0;
        }
        if (!thousands) {
            thousands = '.';
        }
        if (!decimal) {
            decimal = ',';
        }
        return {
            align: 'right',
            allowNegative,
            decimal,
            precision: prec,
            prefix: '',
            suffix: '',
            thousands,
            negative: negative ? '(' : '-',
            autocomplete: 'off'
        };
    }

    public getCustomerReportPDF(req?: any, isDownload?: boolean) {
        const options = createRequestOption(req);
        let headers = new HttpHeaders();
        headers = headers.set('Accept', 'application/pdf');
        const respone = this.http.get(SERVER_API_URL + 'api/report/pdf', {
            params: options,
            observe: 'response',
            headers,
            responseType: 'blob'
        });
        respone.subscribe(response => {
            // this.showReport(response);
            const file = new Blob([response.body], { type: 'application/pdf' });
            const fileURL = window.URL.createObjectURL(file);
            if (isDownload) {
                const link = document.createElement('a');
                document.body.appendChild(link);
                link.download = fileURL;
                link.setAttribute('style', 'display: none');
                const name = 'ten_bao_cao.pdf';
                link.setAttribute('download', name);
                link.href = fileURL;
                link.click();
            } else {
                const contentDispositionHeader = response.headers.get('Content-Disposition');
                const result = contentDispositionHeader
                    .split(';')[1]
                    .trim()
                    .split('=')[1];
                const newWin = window.open(fileURL, '_blank');
                // add a load listener to the window so that the title gets changed on page load

                newWin.addEventListener('load', () => {
                    newWin.document.title = result.replace(/"/g, '');
                    // this.router.navigate(['/report/buy']);
                });
            }
        });
    }

    isEquivalent(a, b) {
        // Create arrays of property names
        if (!a || !b) {
            return false;
        }
        const aProps = Object.getOwnPropertyNames(a);
        const bProps = Object.getOwnPropertyNames(b);

        // If number of properties is different,
        // objects are not equivalent
        if (aProps.length !== bProps.length) {
            return false;
        }

        for (let i = 0; i < aProps.length; i++) {
            const propName = aProps[i];

            // Trường hợp 2 trường cần so sánh là ngày tháng
            if (a[propName] instanceof moment && b[propName] instanceof moment) {
                if (!a[propName].format(DATE_FORMAT) === a[propName].format(DATE_FORMAT)) {
                    return false;
                }
                continue;
            }

            // Trường hợp 2 trường cần so sánh là object
            if (a[propName] instanceof Object && b[propName] instanceof Object) {
                if (!this.isEquivalent(a[propName], b[propName])) {
                    return false;
                }
                continue;
            }

            // If values of same property are not equal,
            // objects are not equivalent
            if ((a[propName] || b[propName]) && a[propName] !== b[propName]) {
                return false;
            }
        }

        // If we made it this far, objects
        // are considered equivalent
        return true;
    }

    isEquivalentArray(a, b) {
        if ((!a && b) || (a && !b)) {
            return false;
        }
        if (!a && !b) {
            return true;
        }
        if (a && b && a.length !== b.length) {
            return false;
        }

        for (let i = 0; i < a.length; i++) {
            if (!this.isEquivalent(a[i], b[i])) {
                return false;
            }
        }

        return true;
    }

    getCbbTimeLine() {
        let listTimeLine;
        this.translate
            .get([
                'ebwebApp.receiveBill.timeLine.today',
                'ebwebApp.receiveBill.timeLine.thisWeek',
                'ebwebApp.receiveBill.timeLine.earlyWeekToNow',
                'ebwebApp.receiveBill.timeLine.thisMonth',
                'ebwebApp.receiveBill.timeLine.earlyMonthToNow',
                'ebwebApp.receiveBill.timeLine.thisQuarter',
                'ebwebApp.receiveBill.timeLine.earlyQuarterToNow',
                'ebwebApp.receiveBill.timeLine.thisYear',
                'ebwebApp.receiveBill.timeLine.earlyYearToNow',
                'ebwebApp.receiveBill.timeLine.january',
                'ebwebApp.receiveBill.timeLine.february',
                'ebwebApp.receiveBill.timeLine.march',
                'ebwebApp.receiveBill.timeLine.april',
                'ebwebApp.receiveBill.timeLine.may',
                'ebwebApp.receiveBill.timeLine.june',
                'ebwebApp.receiveBill.timeLine.july',
                'ebwebApp.receiveBill.timeLine.august',
                'ebwebApp.receiveBill.timeLine.september',
                'ebwebApp.receiveBill.timeLine.october',
                'ebwebApp.receiveBill.timeLine.november',
                'ebwebApp.receiveBill.timeLine.december',
                'ebwebApp.receiveBill.timeLine.quarterOne',
                'ebwebApp.receiveBill.timeLine.quarterTwo',
                'ebwebApp.receiveBill.timeLine.quarterThree',
                'ebwebApp.receiveBill.timeLine.quarterFour',
                'ebwebApp.receiveBill.timeLine.lastWeek',
                'ebwebApp.receiveBill.timeLine.lastMonth',
                'ebwebApp.receiveBill.timeLine.lastQuarter',
                'ebwebApp.receiveBill.timeLine.lastYear',
                'ebwebApp.receiveBill.timeLine.nextWeek',
                'ebwebApp.receiveBill.timeLine.theNextFourWeeks',
                'ebwebApp.receiveBill.timeLine.nextMonth',
                'ebwebApp.receiveBill.timeLine.nextQuarter',
                'ebwebApp.receiveBill.timeLine.nextYear',
                'ebwebApp.receiveBill.timeLine.optional'
            ])
            .subscribe(res => {
                listTimeLine = [
                    { value: '0', display: res['ebwebApp.receiveBill.timeLine.today'] },
                    { value: '1', display: res['ebwebApp.receiveBill.timeLine.thisWeek'] },
                    { value: '2', display: res['ebwebApp.receiveBill.timeLine.earlyWeekToNow'] },
                    { value: '3', display: res['ebwebApp.receiveBill.timeLine.thisMonth'] },
                    { value: '4', display: res['ebwebApp.receiveBill.timeLine.earlyMonthToNow'] },
                    { value: '5', display: res['ebwebApp.receiveBill.timeLine.thisQuarter'] },
                    { value: '6', display: res['ebwebApp.receiveBill.timeLine.earlyQuarterToNow'] },
                    { value: '7', display: res['ebwebApp.receiveBill.timeLine.thisYear'] },
                    { value: '8', display: res['ebwebApp.receiveBill.timeLine.earlyYearToNow'] },
                    { value: '9', display: res['ebwebApp.receiveBill.timeLine.january'] },
                    { value: '10', display: res['ebwebApp.receiveBill.timeLine.february'] },
                    { value: '11', display: res['ebwebApp.receiveBill.timeLine.march'] },
                    { value: '12', display: res['ebwebApp.receiveBill.timeLine.april'] },
                    { value: '13', display: res['ebwebApp.receiveBill.timeLine.may'] },
                    { value: '14', display: res['ebwebApp.receiveBill.timeLine.june'] },
                    { value: '15', display: res['ebwebApp.receiveBill.timeLine.july'] },
                    { value: '16', display: res['ebwebApp.receiveBill.timeLine.august'] },
                    { value: '17', display: res['ebwebApp.receiveBill.timeLine.september'] },
                    { value: '18', display: res['ebwebApp.receiveBill.timeLine.october'] },
                    { value: '19', display: res['ebwebApp.receiveBill.timeLine.november'] },
                    { value: '20', display: res['ebwebApp.receiveBill.timeLine.december'] },
                    { value: '21', display: res['ebwebApp.receiveBill.timeLine.quarterOne'] },
                    { value: '22', display: res['ebwebApp.receiveBill.timeLine.quarterTwo'] },
                    { value: '23', display: res['ebwebApp.receiveBill.timeLine.quarterThree'] },
                    { value: '24', display: res['ebwebApp.receiveBill.timeLine.quarterFour'] },
                    { value: '25', display: res['ebwebApp.receiveBill.timeLine.lastWeek'] },
                    { value: '26', display: res['ebwebApp.receiveBill.timeLine.lastMonth'] },
                    { value: '27', display: res['ebwebApp.receiveBill.timeLine.lastQuarter'] },
                    { value: '28', display: res['ebwebApp.receiveBill.timeLine.lastYear'] },
                    { value: '29', display: res['ebwebApp.receiveBill.timeLine.nextWeek'] },
                    { value: '30', display: res['ebwebApp.receiveBill.timeLine.theNextFourWeeks'] },
                    { value: '31', display: res['ebwebApp.receiveBill.timeLine.nextMonth'] },
                    { value: '32', display: res['ebwebApp.receiveBill.timeLine.nextQuarter'] },
                    { value: '33', display: res['ebwebApp.receiveBill.timeLine.nextYear'] },
                    { value: '34', display: res['ebwebApp.receiveBill.timeLine.optional'] }
                ];
            });
        return listTimeLine;
    }

    getTimeLine(intTimeLine: String, account?: any) {
        let ngayHachToan;
        if (account) {
            ngayHachToan = this.ngayHachToan(account);
        }
        this.objTimeLine = {};
        let today = new Date();
        this.dtBeginTemp = new Date();
        this.dtEndTemp = new Date();
        let dayOfWeek = today.getDay();
        let month = today.getMonth() - 1;
        let year = today.getFullYear();
        if (ngayHachToan) {
            today = new Date(ngayHachToan.year(), ngayHachToan.month(), ngayHachToan.date());
            dayOfWeek = today.getDay();
            this.dtBeginTemp = new Date(ngayHachToan.year(), ngayHachToan.month(), ngayHachToan.date());
            this.dtEndTemp = new Date(ngayHachToan.year(), ngayHachToan.month(), ngayHachToan.date());
            year = ngayHachToan.year();
            month = ngayHachToan.month();
            this.dtBeginTemp = new Date(year, this.dtBeginTemp.getMonth(), this.dtBeginTemp.getDate());
            this.dtEndTemp = new Date(year, this.dtEndTemp.getMonth(), this.dtEndTemp.getDate());
        }
        // số ngày từ ngày đầu tuần tới ngày hiện tại
        // const alpha = getLocaleFirstDayOfWeek('');
        const alpha = 7 - dayOfWeek;
        switch (intTimeLine.toString()) {
            case '0': // hôm nay
                this.dtBeginDate = this.dtEndDate = this.datepipe.transform(today, 'yyyy-MM-dd');
                break;
            case '1': // tuần này
                this.dtBeginDate = this.datepipe.transform(
                    this.dtBeginTemp.setDate(this.dtBeginTemp.getDate() - (dayOfWeek - 1)),
                    'yyyy-MM-dd'
                );
                this.dtEndDate = this.datepipe.transform(this.dtEndTemp.setDate(this.dtEndTemp.getDate() + alpha), 'yyyy-MM-dd');
                break;
            case '2': // đầu tuần đến hiện tại
                this.dtBeginDate = this.datepipe.transform(today.setDate(today.getDate() - (dayOfWeek - 1)), 'yyyy-MM-dd');
                this.dtEndDate = this.datepipe.transform(this.dtEndTemp, 'yyyy-MM-dd');
                break;
            case '3': // tháng này
                this.dtBeginDate = this.datepipe.transform(new Date(year, month, 1), 'yyyy-MM-dd');
                this.dtEndDate = this.datepipe.transform(new Date(year, month + 1, 0), 'yyyy-MM-dd');
                break;
            case '4': // đầu tháng tới hiện tại
                this.dtBeginDate = this.datepipe.transform(new Date(year, month, 1), 'yyyy-MM-dd');
                this.dtEndDate = this.datepipe.transform(this.dtEndTemp, 'yyyy-MM-dd');
                break;
            case '5': // quý này
                // quý I
                if (month >= 0 && month <= 2) {
                    this.dtBeginDate = this.datepipe.transform(new Date(year, 0, 1), 'yyyy-MM-dd');
                    this.dtEndDate = this.datepipe.transform(new Date(year, 3, 0), 'yyyy-MM-dd');
                } else if (month >= 3 && month <= 5) {
                    // quý II
                    this.dtBeginDate = this.datepipe.transform(new Date(year, 3, 1), 'yyyy-MM-dd');
                    this.dtEndDate = this.datepipe.transform(new Date(year, 6, 0), 'yyyy-MM-dd');
                } else if (month >= 6 && month <= 8) {
                    // quý III
                    this.dtBeginDate = this.datepipe.transform(new Date(year, 6, 1), 'yyyy-MM-dd');
                    this.dtEndDate = this.datepipe.transform(new Date(year, 9, 0), 'yyyy-MM-dd');
                } else if (month >= 9 && month <= 11) {
                    // quý IV
                    this.dtBeginDate = this.datepipe.transform(new Date(year, 9, 1), 'yyyy-MM-dd');
                    this.dtEndDate = this.datepipe.transform(new Date(year, 12, 0), 'yyyy-MM-dd');
                } else {
                    this.toastr.error(
                        this.translate.instant('ebwebApp.receiveBill.errorWhenSelectTimeLine'),
                        this.translate.instant('ebwebApp.receiveBill.message')
                    );
                }
                break;
            case '6': // đầu quý đến hiện tại
                // quý I
                if (month >= 0 && month <= 2) {
                    this.dtBeginDate = this.datepipe.transform(new Date(year, 0, 1), 'yyyy-MM-dd');
                    this.dtEndDate = this.datepipe.transform(this.dtEndTemp, 'yyyy-MM-dd');
                } else if (month >= 3 && month <= 5) {
                    // quý II
                    this.dtBeginDate = this.datepipe.transform(new Date(year, 3, 1), 'yyyy-MM-dd');
                    this.dtEndDate = this.datepipe.transform(this.dtEndTemp, 'yyyy-MM-dd');
                } else if (month >= 6 && month <= 8) {
                    // quý III
                    this.dtBeginDate = this.datepipe.transform(new Date(year, 6, 1), 'yyyy-MM-dd');
                    this.dtEndDate = this.datepipe.transform(this.dtEndTemp, 'yyyy-MM-dd');
                } else if (month >= 9 && month <= 11) {
                    // quý IV
                    this.dtBeginDate = this.datepipe.transform(new Date(year, 9, 1), 'yyyy-MM-dd');
                    this.dtEndDate = this.datepipe.transform(this.dtEndTemp, 'yyyy-MM-dd');
                } else {
                    this.toastr.error(
                        this.translate.instant('ebwebApp.receiveBill.errorWhenSelectTimeLine'),
                        this.translate.instant('ebwebApp.receiveBill.message')
                    );
                }
                break;
            case '7': // năm nay
                this.dtBeginDate = this.datepipe.transform(new Date(year, 0, 1), 'yyyy-MM-dd');
                this.dtEndDate = this.datepipe.transform(new Date(year, 12, 0), 'yyyy-MM-dd');
                break;
            case '8': // đầu năm tới hiện tại
                this.dtBeginDate = this.datepipe.transform(new Date(year, 0, 1), 'yyyy-MM-dd');
                this.dtEndDate = this.datepipe.transform(this.dtEndTemp, 'yyyy-MM-dd');
                break;
            case '9': // tháng 1
                this.dtBeginDate = this.datepipe.transform(new Date(year, 0, 1), 'yyyy-MM-dd');
                this.dtEndDate = this.datepipe.transform(new Date(year, 1, 0), 'yyyy-MM-dd');
                break;
            case '10': // tháng 2
                this.dtBeginDate = this.datepipe.transform(new Date(year, 1, 1), 'yyyy-MM-dd');
                this.dtEndDate = this.datepipe.transform(new Date(year, 2, 0), 'yyyy-MM-dd');
                break;
            case '11': // tháng 3
                this.dtBeginDate = this.datepipe.transform(new Date(year, 2, 1), 'yyyy-MM-dd');
                this.dtEndDate = this.datepipe.transform(new Date(year, 3, 0), 'yyyy-MM-dd');
                break;
            case '12': // tháng 4
                this.dtBeginDate = this.datepipe.transform(new Date(year, 3, 1), 'yyyy-MM-dd');
                this.dtEndDate = this.datepipe.transform(new Date(year, 4, 0), 'yyyy-MM-dd');
                break;
            case '13': // tháng 5
                this.dtBeginDate = this.datepipe.transform(new Date(year, 4, 1), 'yyyy-MM-dd');
                this.dtEndDate = this.datepipe.transform(new Date(year, 5, 0), 'yyyy-MM-dd');
                break;
            case '14': // tháng 6
                this.dtBeginDate = this.datepipe.transform(new Date(year, 5, 1), 'yyyy-MM-dd');
                this.dtEndDate = this.datepipe.transform(new Date(year, 6, 0), 'yyyy-MM-dd');
                break;
            case '15': // tháng 7
                this.dtBeginDate = this.datepipe.transform(new Date(year, 6, 1), 'yyyy-MM-dd');
                this.dtEndDate = this.datepipe.transform(new Date(year, 7, 0), 'yyyy-MM-dd');
                break;
            case '16': // tháng 8
                this.dtBeginDate = this.datepipe.transform(new Date(year, 7, 1), 'yyyy-MM-dd');
                this.dtEndDate = this.datepipe.transform(new Date(year, 8, 0), 'yyyy-MM-dd');
                break;
            case '17': // tháng 9
                this.dtBeginDate = this.datepipe.transform(new Date(year, 8, 1), 'yyyy-MM-dd');
                this.dtEndDate = this.datepipe.transform(new Date(year, 9, 0), 'yyyy-MM-dd');
                break;
            case '18': // tháng 10
                this.dtBeginDate = this.datepipe.transform(new Date(year, 9, 1), 'yyyy-MM-dd');
                this.dtEndDate = this.datepipe.transform(new Date(year, 10, 0), 'yyyy-MM-dd');
                break;
            case '19': // tháng 11
                this.dtBeginDate = this.datepipe.transform(new Date(year, 10, 1), 'yyyy-MM-dd');
                this.dtEndDate = this.datepipe.transform(new Date(year, 11, 0), 'yyyy-MM-dd');
                break;
            case '20': // tháng 12
                this.dtBeginDate = this.datepipe.transform(new Date(year, 11, 1), 'yyyy-MM-dd');
                this.dtEndDate = this.datepipe.transform(new Date(year, 12, 0), 'yyyy-MM-dd');
                break;
            case '21': // quý I
                this.dtBeginDate = this.datepipe.transform(new Date(year, 0, 1), 'yyyy-MM-dd');
                this.dtEndDate = this.datepipe.transform(new Date(year, 3, 0), 'yyyy-MM-dd');
                break;
            case '22': // quý II
                this.dtBeginDate = this.datepipe.transform(new Date(year, 3, 1), 'yyyy-MM-dd');
                this.dtEndDate = this.datepipe.transform(new Date(year, 6, 0), 'yyyy-MM-dd');
                break;
            case '23': // quý III
                this.dtBeginDate = this.datepipe.transform(new Date(year, 6, 1), 'yyyy-MM-dd');
                this.dtEndDate = this.datepipe.transform(new Date(year, 9, 0), 'yyyy-MM-dd');
                break;
            case '24': // quý IV
                this.dtBeginDate = this.datepipe.transform(new Date(year, 9, 1), 'yyyy-MM-dd');
                this.dtEndDate = this.datepipe.transform(new Date(year, 12, 0), 'yyyy-MM-dd');
                break;
            case '25': // tuần trước
                this.dtBeginDate = this.datepipe.transform(
                    this.dtBeginTemp.setDate(this.dtBeginTemp.getDate() - (dayOfWeek - 1) - 7),
                    'yyyy-MM-dd'
                );
                this.dtEndDate = this.datepipe.transform(
                    this.dtEndTemp.setDate(this.dtEndTemp.getDate() - (dayOfWeek - 1) - 1),
                    'yyyy-MM-dd'
                );
                break;
            case '26': // tháng trước
                let lastMonth;
                let lastYear;
                if (month === 0) {
                    lastMonth = 12;
                    lastYear = year - 1;
                } else {
                    lastMonth = month - 1;
                    lastYear = year;
                }
                this.dtBeginDate = this.datepipe.transform(new Date(lastYear, lastMonth, 1), 'yyyy-MM-dd');
                this.dtEndDate = this.datepipe.transform(new Date(lastYear, lastMonth + 1, 0), 'yyyy-MM-dd');
                break;
            case '27': // quý trước
                // quý I
                if (month >= 0 && month <= 2) {
                    this.dtBeginDate = this.datepipe.transform(new Date(year - 1, 9, 1), 'yyyy-MM-dd');
                    this.dtEndDate = this.datepipe.transform(new Date(year - 1, 12, 0), 'yyyy-MM-dd');
                } else if (month >= 3 && month <= 5) {
                    // quý II
                    this.dtBeginDate = this.datepipe.transform(new Date(year, 0, 1), 'yyyy-MM-dd');
                    this.dtEndDate = this.datepipe.transform(new Date(year, 3, 0), 'yyyy-MM-dd');
                } else if (month >= 6 && month <= 8) {
                    // quý III
                    this.dtBeginDate = this.datepipe.transform(new Date(year, 3, 1), 'yyyy-MM-dd');
                    this.dtEndDate = this.datepipe.transform(new Date(year, 6, 0), 'yyyy-MM-dd');
                } else if (month >= 9 && month <= 11) {
                    // quý IV
                    this.dtBeginDate = this.datepipe.transform(new Date(year, 6, 1), 'yyyy-MM-dd');
                    this.dtEndDate = this.datepipe.transform(new Date(year, 9, 0), 'yyyy-MM-dd');
                } else {
                    this.toastr.error(
                        this.translate.instant('ebwebApp.receiveBill.errorWhenSelectTimeLine'),
                        this.translate.instant('ebwebApp.receiveBill.message')
                    );
                }
                break;
            case '28': // năm trước
                this.dtBeginDate = this.datepipe.transform(new Date(year - 1, 0, 1), 'yyyy-MM-dd');
                this.dtEndDate = this.datepipe.transform(new Date(year - 1, 12, 0), 'yyyy-MM-dd');
                break;
            case '29': // tuần sau
                this.dtBeginDate = this.datepipe.transform(
                    this.dtBeginTemp.setDate(this.dtBeginTemp.getDate() + (alpha + 1)),
                    'yyyy-MM-dd'
                );
                this.dtEndDate = this.datepipe.transform(this.dtBeginTemp.setDate(this.dtBeginTemp.getDate() + 6), 'yyyy-MM-dd');
                break;
            case '30': // bốn tuần tới
                this.dtBeginDate = this.datepipe.transform(
                    this.dtBeginTemp.setDate(this.dtBeginTemp.getDate() + (alpha + 1)),
                    'yyyy-MM-dd'
                );
                this.dtEndDate = this.datepipe.transform(this.dtBeginTemp.setDate(this.dtBeginTemp.getDate() + 27), 'yyyy-MM-dd');
                break;
            case '31': // tháng sau
                let afterMonth;
                let afterYear;
                if (month === 11) {
                    afterMonth = 0;
                    afterYear = year + 1;
                } else {
                    afterMonth = month + 1;
                    afterYear = year;
                }
                this.dtBeginDate = this.datepipe.transform(new Date(afterYear, afterMonth, 1), 'yyyy-MM-dd');
                this.dtEndDate = this.datepipe.transform(new Date(afterYear, afterMonth + 1, 0), 'yyyy-MM-dd');
                break;
            case '32': // quý sau
                // quý I
                if (month >= 0 && month <= 2) {
                    this.dtBeginDate = this.datepipe.transform(new Date(year, 3, 1), 'yyyy-MM-dd');
                    this.dtEndDate = this.datepipe.transform(new Date(year, 6, 0), 'yyyy-MM-dd');
                } else if (month >= 3 && month <= 5) {
                    // quý II
                    this.dtBeginDate = this.datepipe.transform(new Date(year, 6, 1), 'yyyy-MM-dd');
                    this.dtEndDate = this.datepipe.transform(new Date(year, 9, 0), 'yyyy-MM-dd');
                } else if (month >= 6 && month <= 8) {
                    // quý III
                    this.dtBeginDate = this.datepipe.transform(new Date(year, 9, 1), 'yyyy-MM-dd');
                    this.dtEndDate = this.datepipe.transform(new Date(year, 12, 0), 'yyyy-MM-dd');
                } else if (month >= 9 && month <= 11) {
                    // quý IV
                    this.dtBeginDate = this.datepipe.transform(new Date(year + 1, 0, 1), 'yyyy-MM-dd');
                    this.dtEndDate = this.datepipe.transform(new Date(year + 1, 3, 0), 'yyyy-MM-dd');
                } else {
                    this.toastr.error(
                        this.translate.instant('ebwebApp.receiveBill.errorWhenSelectTimeLine'),
                        this.translate.instant('ebwebApp.receiveBill.message')
                    );
                }
                break;
            case '33': // năm sau
                this.dtBeginDate = this.datepipe.transform(new Date(year + 1, 0, 1), 'yyyy-MM-dd');
                this.dtEndDate = this.datepipe.transform(new Date(year + 1, 12, 0), 'yyyy-MM-dd');
                break;
            case '34': // Tùy chọn
                this.dtBeginDate = this.datepipe.transform(new Date(), 'yyyy-MM-dd');
                this.dtEndDate = this.datepipe.transform(new Date(), 'yyyy-MM-dd');
                break;
        }
        this.objTimeLine.dtBeginDate = this.dtBeginDate;
        this.objTimeLine.dtEndDate = this.dtEndDate;
        return this.objTimeLine;
    }

    ngayHachToan(ac): Moment {
        const now = moment(new Date(), DATE_FORMAT);
        let dateCheck;
        if (ac) {
            const ngayHachToanDefault = moment(ac.systemOption.find(x => x.code === NGAY_HACH_TOAN).defaultData, DATE_FORMAT);
            const dd = ngayHachToanDefault.toDate();
            const ngayHachToanData = moment(ac.systemOption.find(x => x.code === NGAY_HACH_TOAN).data, DATE_FORMAT);
            const currentDate = new Date();
            if (!ac.yearWork) {
                dateCheck = moment(new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate()), DATE_FORMAT);
            } else {
                if (ngayHachToanDefault.day() === now.day()) {
                    dateCheck = ngayHachToanData;
                } else {
                    dateCheck = moment(new Date(ac.yearWork, currentDate.getMonth(), currentDate.getDate()), DATE_FORMAT);
                }
            }
            if (this.datepipe.transform(ngayHachToanDefault, 'yyyy-MM-dd') === this.datepipe.transform(dateCheck, 'yyyy-MM-dd')) {
                return ngayHachToanData
                    .add(now.hour(), 'hours')
                    .add(now.minute(), 'minutes')
                    .add(now.second(), 'seconds');
            } else {
                return dateCheck
                    .add(now.hour(), 'hours')
                    .add(now.minute(), 'minutes')
                    .add(now.second(), 'seconds');
            }
        } else {
            return now;
        }
    }

    getCbbTimeLine2() {
        return new Promise(resolve => {
            this.translate
                .get([
                    'ebwebApp.receiveBill.timeLine.today',
                    'ebwebApp.receiveBill.timeLine.thisWeek',
                    'ebwebApp.receiveBill.timeLine.earlyWeekToNow',
                    'ebwebApp.receiveBill.timeLine.thisMonth',
                    'ebwebApp.receiveBill.timeLine.earlyMonthToNow',
                    'ebwebApp.receiveBill.timeLine.thisQuarter',
                    'ebwebApp.receiveBill.timeLine.earlyQuarterToNow',
                    'ebwebApp.receiveBill.timeLine.thisYear',
                    'ebwebApp.receiveBill.timeLine.earlyYearToNow',
                    'ebwebApp.receiveBill.timeLine.january',
                    'ebwebApp.receiveBill.timeLine.february',
                    'ebwebApp.receiveBill.timeLine.march',
                    'ebwebApp.receiveBill.timeLine.april',
                    'ebwebApp.receiveBill.timeLine.may',
                    'ebwebApp.receiveBill.timeLine.june',
                    'ebwebApp.receiveBill.timeLine.july',
                    'ebwebApp.receiveBill.timeLine.august',
                    'ebwebApp.receiveBill.timeLine.september',
                    'ebwebApp.receiveBill.timeLine.october',
                    'ebwebApp.receiveBill.timeLine.november',
                    'ebwebApp.receiveBill.timeLine.december',
                    'ebwebApp.receiveBill.timeLine.quarterOne',
                    'ebwebApp.receiveBill.timeLine.quarterTwo',
                    'ebwebApp.receiveBill.timeLine.quarterThree',
                    'ebwebApp.receiveBill.timeLine.quarterFour',
                    'ebwebApp.receiveBill.timeLine.lastWeek',
                    'ebwebApp.receiveBill.timeLine.lastMonth',
                    'ebwebApp.receiveBill.timeLine.lastQuarter',
                    'ebwebApp.receiveBill.timeLine.lastYear',
                    'ebwebApp.receiveBill.timeLine.nextWeek',
                    'ebwebApp.receiveBill.timeLine.theNextFourWeeks',
                    'ebwebApp.receiveBill.timeLine.nextMonth',
                    'ebwebApp.receiveBill.timeLine.nextQuarter',
                    'ebwebApp.receiveBill.timeLine.nextYear',
                    'ebwebApp.receiveBill.timeLine.optional'
                ])
                .subscribe(res => {
                    const listTimeLine = [
                        { value: '0', display: res['ebwebApp.receiveBill.timeLine.today'] },
                        { value: '1', display: res['ebwebApp.receiveBill.timeLine.thisWeek'] },
                        { value: '2', display: res['ebwebApp.receiveBill.timeLine.earlyWeekToNow'] },
                        { value: '3', display: res['ebwebApp.receiveBill.timeLine.thisMonth'] },
                        { value: '4', display: res['ebwebApp.receiveBill.timeLine.earlyMonthToNow'] },
                        { value: '5', display: res['ebwebApp.receiveBill.timeLine.thisQuarter'] },
                        { value: '6', display: res['ebwebApp.receiveBill.timeLine.earlyQuarterToNow'] },
                        { value: '7', display: res['ebwebApp.receiveBill.timeLine.thisYear'] },
                        { value: '8', display: res['ebwebApp.receiveBill.timeLine.earlyYearToNow'] },
                        { value: '9', display: res['ebwebApp.receiveBill.timeLine.january'] },
                        { value: '10', display: res['ebwebApp.receiveBill.timeLine.february'] },
                        { value: '11', display: res['ebwebApp.receiveBill.timeLine.march'] },
                        { value: '12', display: res['ebwebApp.receiveBill.timeLine.april'] },
                        { value: '13', display: res['ebwebApp.receiveBill.timeLine.may'] },
                        { value: '14', display: res['ebwebApp.receiveBill.timeLine.june'] },
                        { value: '15', display: res['ebwebApp.receiveBill.timeLine.july'] },
                        { value: '16', display: res['ebwebApp.receiveBill.timeLine.august'] },
                        { value: '17', display: res['ebwebApp.receiveBill.timeLine.september'] },
                        { value: '18', display: res['ebwebApp.receiveBill.timeLine.october'] },
                        { value: '19', display: res['ebwebApp.receiveBill.timeLine.november'] },
                        { value: '20', display: res['ebwebApp.receiveBill.timeLine.december'] },
                        { value: '21', display: res['ebwebApp.receiveBill.timeLine.quarterOne'] },
                        { value: '22', display: res['ebwebApp.receiveBill.timeLine.quarterTwo'] },
                        { value: '23', display: res['ebwebApp.receiveBill.timeLine.quarterThree'] },
                        { value: '24', display: res['ebwebApp.receiveBill.timeLine.quarterFour'] },
                        { value: '25', display: res['ebwebApp.receiveBill.timeLine.lastWeek'] },
                        { value: '26', display: res['ebwebApp.receiveBill.timeLine.lastMonth'] },
                        { value: '27', display: res['ebwebApp.receiveBill.timeLine.lastQuarter'] },
                        { value: '28', display: res['ebwebApp.receiveBill.timeLine.lastYear'] },
                        { value: '29', display: res['ebwebApp.receiveBill.timeLine.nextWeek'] },
                        { value: '30', display: res['ebwebApp.receiveBill.timeLine.theNextFourWeeks'] },
                        { value: '31', display: res['ebwebApp.receiveBill.timeLine.nextMonth'] },
                        { value: '32', display: res['ebwebApp.receiveBill.timeLine.nextQuarter'] },
                        { value: '33', display: res['ebwebApp.receiveBill.timeLine.nextYear'] },
                        { value: '34', display: res['ebwebApp.receiveBill.timeLine.optional'] }
                    ];
                    resolve(listTimeLine);
                });
        });
        // return listTimeLine;
    }

    public getSortArray(myArr: any[]) {
        if (myArr !== undefined && myArr !== null) {
            return myArr.sort((a1, a2) => {
                return a1.orderPriority - a2.orderPriority;
            });
        } else {
        }
    }

    viewRefVoucher(voucher, account?) {
        let url = '';
        if (!account) {
            switch (voucher.typeGroupID) {
                // Hàng bán trả lại
                case 33:
                    url = `/#/hang-ban/tra-lai/${voucher.refID2}/edit/from-ref`;
                    break;
                // Giảm giá hàng bán
                case 34:
                    url = `/#/hang-ban/giam-gia/${voucher.refID2}/edit/from-ref`;
                    break;
                // Xuất hóa đơn
                case 35:
                    url = `/#/xuat-hoa-don/${voucher.refID2}/edit/from-ref`;
                    break;
                case 22:
                    url = `/#/hang-mua/tra-lai/${voucher.refID2}/from-ref`;
                    break;
                case 23:
                    url = `/#/hang-mua/giam-gia/${voucher.refID2}/from-ref`;
                    break;
                case 10:
                    url = `/#/phieu-thu/${voucher.refID2}/edit/from-ref`;
                    break;
                case 15:
                    url = `/#/chuyen-tien-noi-bo/${voucher.refID2}/edit/from-ref`;
                    break;
                case 16:
                    url = `/#/bao-co/${voucher.refID2}/edit/from-ref`;
                    break;
                case 17:
                    url = `/#/the-tin-dung/${voucher.refID2}/edit/from-ref`;
                    break;
                case 70:
                    if (voucher.typeID && voucher.typeID === 701) {
                        url = `/#/ket-chuyen-chi-phi/${voucher.refID2}/edit/from-ref`;
                        break;
                    } else if (voucher.typeID && voucher.typeID === 702) {
                        url = `/#/ket-chuyen-lai-lo/${voucher.refID2}/edit/from-ref`;
                        break;
                    } else {
                        url = `/#/chung-tu-nghiep-vu-khac/${voucher.refID2}/edit/from-ref`;
                        break;
                    }
                case 11:
                    url = `/#/phieu-chi/${voucher.refID2}/edit/from-ref`;
                    break;
                case 31:
                    url = `/#/don-dat-hang/${voucher.refID2}/edit/from-ref`;
                    break;
                case 24:
                    url = `/#/mua-dich-vu/${voucher.refID2}/edit/from-ref`;
                    break;
                case 40:
                    url = `/#/nhap-kho/${voucher.refID2}/edit/from-ref`;
                    break;
                case 20:
                    url = `/#/don-mua-hang/${voucher.refID2}/edit/from-ref`;
                    break;
                case 41:
                    url = `/#/xuat-kho/${voucher.refID2}/edit/from-ref`;
                    break;
                case 42:
                    url = `/#/chuyen-kho/${voucher.refID2}/edit/from-ref`;
                    break;
                case 21:
                    url = `/#/chung-tu-mua-hang/${voucher.refID2}/edit/from-ref`;
                    break;
                case 18:
                    url = `/#/kiem-ke-quy/${voucher.refID2}/edit/from-ref`;
                    break;
                case 32:
                    url = `/#/chung-tu-ban-hang/${voucher.refID2}/edit/from-ref`;
                    break;
                case 30:
                    url = `/#/bao-gia/${voucher.refID2}/edit/from-ref`;
                    break;
                case 12:
                case 13:
                case 14:
                    url = `/#/bao-no/${voucher.refID2}/edit/from-ref`;
                    break;
                case 56:
                    url = `/#/phan-bo-ccdc/${voucher.refID2}/edit/from-ref`;
                    break;
                case 52:
                    url = `/#/ghi-tang-ccdc/${voucher.refID2}/edit/from-ref`;
                    break;
                case 53:
                    url = `/#/ghi-giam-ccdc/${voucher.refID2}/edit/from-ref`;
                    break;
                case 55:
                    url = `/#/dieu-chinh-ccdc/${voucher.refID2}/edit/from-ref`;
                    break;
                case 54:
                    url = `/#/dieu-chuyen-ccdc/${voucher.refID2}/edit/from-ref`;
                    break;
                case 57:
                    url = `/#/kiem-ke-ccdc/${voucher.refID2}/edit/from-ref`;
                    break;
                case 66:
                    url = `/#/tinh-khau-hao-tscd/${voucher.refID2}/edit/from-ref`;
                    break;
                case 62:
                    url = `/#/ghi-tang-tscd/${voucher.refID2}/edit/from-ref`;
                    break;
                case 63:
                    url = `/#/ghi-giam-tscd/${voucher.refID2}/edit/from-ref`;
                    break;
                case 65:
                    url = `/#/dieu-chinh-tscd/${voucher.refID2}/edit/from-ref`;
                    break;
                case 64:
                    url = `/#/dieu-chuyen-tscd/${voucher.refID2}/edit/from-ref`;
                    break;
                case 67:
                    url = `/#/kiem-ke-tscd/${voucher.refID2}/edit/from-ref`;
                    break;
                case 71:
                    url = `/#/chung-tu-ghi-so/${voucher.refID2}/edit/from-ref`;
                    break;
                case 72:
                    url = `/#/hop-dong-mua/${voucher.refID2}/edit/from-ref`;
                    break;
                case 73:
                    url = `/#/hop-dong-ban/${voucher.refID2}/edit/from-ref`;
                    break;
                case 25:
                    url = `/#/hoa-don-dau-vao/${voucher.refID2}/edit/from-ref`;
                    break;
            }
        } else {
            switch (voucher.typeGroupID) {
                // Hàng bán trả lại
                case 33:
                    if (this.hasAuthority(ROLE.HangBanTraLai_Xem, account)) {
                        url = `/#/hang-ban/tra-lai/${voucher.refID2}/edit/from-ref`;
                    }
                    break;
                // Giảm giá hàng bán
                case 34:
                    if (this.hasAuthority(ROLE.HangBanGiamGia_Xem, account)) {
                        url = `/#/hang-ban/giam-gia/${voucher.refID2}/edit/from-ref`;
                    }
                    break;
                // Xuất hóa đơn
                case 35:
                    if (this.hasAuthority(ROLE.XuatHoaDon_Xem, account)) {
                        url = `/#/xuat-hoa-don/${voucher.refID2}/edit/from-ref`;
                    }
                    break;
                case 22:
                    if (this.hasAuthority(ROLE.HangMuaTraLai_Xem, account)) {
                        url = `/#/hang-mua/tra-lai/${voucher.refID2}/from-ref`;
                    }
                    break;
                case 23:
                    if (this.hasAuthority(ROLE.HangMuaGiamGia_Xem, account)) {
                        url = `/#/hang-mua/giam-gia/${voucher.refID2}/from-ref`;
                    }
                    break;
                case 10:
                    if (this.hasAuthority(ROLE.PhieuThu_Xem, account)) {
                        url = `/#/phieu-thu/${voucher.refID2}/edit/from-ref`;
                    }
                    break;
                case 15:
                    if (this.hasAuthority(ROLE.ChuyenTienNoiBo_Xem, account)) {
                        url = `/#/chuyen-tien-noi-bo/${voucher.refID2}/edit/from-ref`;
                    }
                    break;
                case 16:
                    if (this.hasAuthority(ROLE.BaoCo_Xem, account)) {
                        url = `/#/bao-co/${voucher.refID2}/edit/from-ref`;
                    }
                    break;
                case 17:
                    if (this.hasAuthority(ROLE.TheTinDung_Xem, account)) {
                        url = `/#/the-tin-dung/${voucher.refID2}/edit/from-ref`;
                    }
                    break;
                case 70:
                    if (voucher.typeID && voucher.typeID === 701) {
                        if (this.hasAuthority(ROLE.KetChuyenChiPhi_Xem, account)) {
                            url = `/#/ket-chuyen-chi-phi/${voucher.refID2}/edit/from-ref`;
                        }
                    } else if (voucher.typeID && voucher.typeID === 702) {
                        url = `/#/ket-chuyen-lai-lo/${voucher.refID2}/edit/from-ref`;
                        break;
                    } else {
                        if (this.hasAuthority(ROLE.ChungTuNghiepVuKhac_Xem, account)) {
                            url = `/#/chung-tu-nghiep-vu-khac/${voucher.refID2}/edit/from-ref`;
                        }
                    }
                    break;
                case 11:
                    if (this.hasAuthority(ROLE.PhieuChi_Xem, account)) {
                        url = `/#/phieu-chi/${voucher.refID2}/edit/from-ref`;
                    }
                    break;
                case 31:
                    if (this.hasAuthority(ROLE.DonDatHang_Xem, account)) {
                        url = `/#/don-dat-hang/${voucher.refID2}/edit/from-ref`;
                    }
                    break;
                case 24:
                    if (this.hasAuthority(ROLE.MuaDichVu_Xem, account)) {
                        url = `/#/mua-dich-vu/${voucher.refID2}/edit/from-ref`;
                    }
                    break;
                case 40:
                    if (this.hasAuthority(ROLE.NhapKho_XEM, account)) {
                        url = `/#/nhap-kho/${voucher.refID2}/edit/from-ref`;
                    }
                    break;
                case 20:
                    if (this.hasAuthority(ROLE.DonMuaHang_XEM, account)) {
                        url = `/#/don-mua-hang/${voucher.refID2}/edit/from-ref`;
                    }
                    break;
                case 41:
                    if (this.hasAuthority(ROLE.XuatKho_XEM, account)) {
                        url = `/#/xuat-kho/${voucher.refID2}/edit/from-ref`;
                    }
                    break;
                case 42:
                    if (this.hasAuthority(ROLE.ChuyenKho_XEM, account)) {
                        url = `/#/chuyen-kho/${voucher.refID2}/edit/from-ref`;
                    }
                    break;
                case 21:
                    if (this.hasAuthority(ROLE.MuaHangQuaKho_Xem, account)) {
                        url = `/#/chung-tu-mua-hang/${voucher.refID2}/edit/from-ref`;
                    }
                    break;
                case 18:
                    if (this.hasAuthority(ROLE.KiemKeQuy_Xem, account)) {
                        url = `/#/kiem-ke-quy/${voucher.refID2}/edit/from-ref`;
                    }
                    break;
                case 32:
                    if (this.hasAuthority(ROLE.ChungTuBanHang_Xem, account)) {
                        url = `/#/chung-tu-ban-hang/${voucher.refID2}/edit/from-ref`;
                    }
                    break;
                case 30:
                    if (this.hasAuthority(ROLE.BaoGia_Xem, account)) {
                        url = `/#/bao-gia/${voucher.refID2}/edit/from-ref`;
                    }
                    break;
                case 12:
                case 13:
                case 14:
                    if (this.hasAuthority(ROLE.BaoNo_Xem, account)) {
                        url = `/#/bao-no/${voucher.refID2}/edit/from-ref`;
                    }
                    break;
                case 56:
                    if (this.hasAuthority(ROLE.PhanBoCCDC_Xem, account)) {
                        url = `/#/phan-bo-ccdc/${voucher.refID2}/edit/from-ref`;
                    }
                    break;
                case 52:
                    if (this.hasAuthority(ROLE.GhiTangCCDC_Xem, account)) {
                        url = `/#/ghi-tang-ccdc/${voucher.refID2}/edit/from-ref`;
                    }
                    break;
                case 53:
                    if (this.hasAuthority(ROLE.GhiGiamCCDC_Xem, account)) {
                        url = `/#/ghi-giam-ccdc/${voucher.refID2}/edit/from-ref`;
                    }
                    break;
                case 55:
                    if (this.hasAuthority(ROLE.DieuChinhCCDC_Xem, account)) {
                        url = `/#/dieu-chinh-ccdc/${voucher.refID2}/edit/from-ref`;
                    }
                    break;
                case 54:
                    if (this.hasAuthority(ROLE.DieuChuyenCCDC_Xem, account)) {
                        url = `/#/dieu-chuyen-ccdc/${voucher.refID2}/edit/from-ref`;
                    }
                    break;
                case 57:
                    if (this.hasAuthority(ROLE.KiemKeCCDC_Xem, account)) {
                        url = `/#/kiem-ke-ccdc/${voucher.refID2}/edit/from-ref`;
                    }
                    break;
                case 71:
                    if (this.hasAuthority(ROLE.ChungTuGhiSo_Xem, account)) {
                        url = `/#/chung-tu-ghi-so/${voucher.refID2}/edit/from-ref`;
                    }
                    break;
                case 72:
                    if (this.hasAuthority(ROLE.HopDongMua_Xem, account)) {
                        url = `/#/hop-dong-mua/${voucher.refID2}/edit/from-ref`;
                    }
                    break;
                case 73:
                    if (this.hasAuthority(ROLE.HopDongBan_Xem, account)) {
                        url = `/#/hop-dong-ban/${voucher.refID2}/edit/from-ref`;
                    }
                    break;
                case 66:
                    url = `/#/tinh-khau-hao-tscd/${voucher.refID2}/edit/from-ref`;
                    break;
                case 62:
                    url = `/#/ghi-tang-tscd/${voucher.refID2}/edit/from-ref`;
                    break;
                case 63:
                    url = `/#/ghi-giam-tscd/${voucher.refID2}/edit/from-ref`;
                    break;
                case 65:
                    url = `/#/dieu-chinh-tscd/${voucher.refID2}/edit/from-ref`;
                    break;
                case 64:
                    url = `/#/dieu-chuyen-tscd/${voucher.refID2}/edit/from-ref`;
                    break;
                case 67:
                    url = `/#/kiem-ke-tscd/${voucher.refID2}/edit/from-ref`;
                    break;
                case 43:
                    if (this.hasAuthority(ROLE.LLRTD_XEM, account)) {
                        url = `/#/lenh-lap-rap-thao-do/${voucher.refID2}/edit/from-ref`;
                    }
                    break;
                case 25:
                    if (this.hasAuthority(ROLE.LLRTD_XEM, account)) {
                        url = `/#/hoa-don-dau-vao/${voucher.refID2}/edit/from-ref`;
                    }
                    break;
                case 90:
                    if (this.hasAuthority(ROLE.Thue01GTGTKhauTru_XEM, account)) {
                        url = `/#/thue/to-khai-thue-gtgt-01/${voucher.refID2}/edit/from-ref`;
                    }
                    break;
                case 91:
                    if (this.hasAuthority(ROLE.Thue01GTGTTrucTiep_XEM, account)) {
                        url = `/#/thue/to-khai-thue-gtgt-04/${voucher.refID2}/edit/from-ref`;
                    }
                    break;
                case 92:
                    if (this.hasAuthority(ROLE.Thue01GTGTDauTu_XEM, account)) {
                        url = `/#/thue/to-khai-thue-gtgt-02/${voucher.refID2}/edit/from-ref`;
                    }
                    break;
                case 93:
                    if (this.hasAuthority(ROLE.ThueQuyetToanTNDNNam_XEM, account)) {
                        url = `/#/thue/quyet-toan-thue-tncn-03-tndn/${voucher.refID2}/edit/from-ref`;
                    }
                    break;
                case 94:
                    if (this.hasAuthority(ROLE.ThueKhaiThueTaiNguyen_XEM, account)) {
                        url = `/#/thue/to-khai-thue-tai-nguyen-01/${voucher.refID2}/edit/from-ref`;
                    }
                    break;
                case 95:
                    if (this.hasAuthority(ROLE.ThueQuyetToanThueTaiNguyen_XEM, account)) {
                        url = `/#/thue/quyet-toan-thue-tai-nguyen-02/${voucher.refID2}/edit/from-ref`;
                    }
                    break;
            }
        }
        if (voucher.typeID === TypeID.XUAT_HOA_DON && this.hasAuthority(ROLE.XuatHoaDon_Xem, account)) {
            url = `/#/xuat-hoa-don/${voucher.refID2}/edit/from-ref`;
        }
        if (voucher.typeID === TypeID.KHAI_BAO_TSCD_DAU_KY) {
            url = `/#/khai-bao-tscd/${voucher.refID2}/edit/from-ref`;
        }
        if (url) {
            window.open(url, '_blank');
        }
    }

    hasAuthority(auth, account): boolean {
        if (account.authorities.includes(ROLE.ROLE_ADMIN) || account.authorities.includes(auth)) {
            return true;
        }
        this.toastr.error(this.translate.instant('ebwebApp.quyTrinh.notHasAccess'));
        return false;
    }

    getConfigCurrencyWithAcc(currentAccount, type?, noUseAllowNegative?) {
        let prec = 0;
        switch (type) {
            case DDSo_TienVND:
                prec = currentAccount.systemOption.find(x => x.code === DDSo_TienVND && x.data).data;
                break;
            case DDSo_NgoaiTe:
                prec = currentAccount.systemOption.find(x => x.code === DDSo_NgoaiTe && x.data).data;
                break;
            case DDSo_SoLuong:
                prec = currentAccount.systemOption.find(x => x.code === DDSo_SoLuong && x.data).data;
                break;
            case DDSo_DonGia:
                prec = currentAccount.systemOption.find(x => x.code === DDSo_DonGia && x.data).data;
                break;
            case DDSo_DonGiaNT:
                prec = currentAccount.systemOption.find(x => x.code === DDSo_DonGiaNT && x.data).data;
                break;
            case DDSo_TyGia:
                prec = currentAccount.systemOption.find(x => x.code === DDSo_TyGia && x.data).data;
                break;
            case DDSo_TyLe:
                prec = currentAccount.systemOption.find(x => x.code === DDSo_TyLe && x.data).data;
                break;
            case DDSo_TyLePBo:
                prec = currentAccount.systemOption.find(x => x.code === DDSo_TyLePBo && x.data).data;
                break;
        }

        if (prec) {
            prec = Number(prec);
        } else {
            prec = 0;
        }
        const thousands = currentAccount.systemOption.find(x => x.code === DDSo_NCachHangNghin && x.data).data;
        const decimal = currentAccount.systemOption.find(x => x.code === DDSo_NCachHangDVi && x.data).data;
        const negative = currentAccount.systemOption.find(x => x.code === DDSo_SoAm && x.data).data === '0';
        if (noUseAllowNegative) {
            return this.getConfigCurrency(prec, thousands, decimal, false, negative);
        } else {
            return this.getConfigCurrency(prec, thousands, decimal, true, negative);
        }
    }

    round(value: number, systemOption: any, type: number): number {
        let precision = 0;
        for (const item of systemOption) {
            if (item.code === this.getStringFromType(type)) {
                precision = parseInt(item.data, 10);
                break;
            }
        }
        return Number(Math.round(parseFloat(`${value}e${precision}`)) + 'e-' + precision);
    }

    getStringFromType(index: number) {
        const typeArray = [
            DDSo_DonGia,
            DDSo_DonGiaNT,
            DDSo_SoLuong,
            DDSo_TyGia,
            DDSo_TyLe,
            DDSo_TyLePBo,
            DDSo_TienVND,
            DDSo_NgoaiTe,
            SO_NGUYEN
        ];
        if (index < 0 || index > typeArray.length) {
            return typeArray[0];
        }
        return typeArray[index - 1];
    }

    getConfigCurrencyAll(currentAccount, noUseAllowNegative?): CurrencyMaskModel {
        let precDDSo_TienVND = currentAccount.systemOption.find(x => x.code === DDSo_TienVND && x.data).data;
        let precDDSo_NgoaiTe = currentAccount.systemOption.find(x => x.code === DDSo_NgoaiTe && x.data).data;
        let precDDSo_SoLuong = currentAccount.systemOption.find(x => x.code === DDSo_SoLuong && x.data).data;
        let precDDSo_DonGia = currentAccount.systemOption.find(x => x.code === DDSo_DonGia && x.data).data;
        let precDDSo_DonGiaNT = currentAccount.systemOption.find(x => x.code === DDSo_DonGiaNT && x.data).data;
        let precDDSo_TyGia = currentAccount.systemOption.find(x => x.code === DDSo_TyGia && x.data).data;
        let precDDSo_TyLe = currentAccount.systemOption.find(x => x.code === DDSo_TyLe && x.data).data;
        let precDDSo_TyLePBo = currentAccount.systemOption.find(x => x.code === DDSo_TyLePBo && x.data).data;
        if (precDDSo_TienVND) {
            precDDSo_TienVND = Number(precDDSo_TienVND);
        } else {
            precDDSo_TienVND = 0;
        }
        if (precDDSo_NgoaiTe) {
            precDDSo_NgoaiTe = Number(precDDSo_NgoaiTe);
        } else {
            precDDSo_NgoaiTe = 0;
        }
        if (precDDSo_SoLuong) {
            precDDSo_SoLuong = Number(precDDSo_SoLuong);
        } else {
            precDDSo_SoLuong = 0;
        }
        if (precDDSo_DonGia) {
            precDDSo_DonGia = Number(precDDSo_DonGia);
        } else {
            precDDSo_DonGia = 0;
        }
        if (precDDSo_DonGiaNT) {
            precDDSo_DonGiaNT = Number(precDDSo_DonGiaNT);
        } else {
            precDDSo_DonGiaNT = 0;
        }
        if (precDDSo_TyGia) {
            precDDSo_TyGia = Number(precDDSo_TyGia);
        } else {
            precDDSo_TyGia = 0;
        }
        if (precDDSo_TyLe) {
            precDDSo_TyLe = Number(precDDSo_TyLe);
        } else {
            precDDSo_TyLe = 0;
        }
        if (precDDSo_TyLePBo) {
            precDDSo_TyLePBo = Number(precDDSo_TyLePBo);
        } else {
            precDDSo_TyLePBo = 0;
        }
        const thousands = currentAccount.systemOption.find(x => x.code === DDSo_NCachHangNghin && x.data).data;
        const decimal = currentAccount.systemOption.find(x => x.code === DDSo_NCachHangDVi && x.data).data;
        const negative = currentAccount.systemOption.find(x => x.code === DDSo_SoAm && x.data).data === '0';
        if (noUseAllowNegative) {
            const rs: CurrencyMaskModel = {};
            rs.DDSo_TienVND = this.getConfigCurrency(precDDSo_TienVND, thousands, decimal, false, negative);
            rs.DDSo_NgoaiTe = this.getConfigCurrency(precDDSo_NgoaiTe, thousands, decimal, false, negative);
            rs.DDSo_SoLuong = this.getConfigCurrency(precDDSo_SoLuong, thousands, decimal, false, negative);
            rs.DDSo_DonGia = this.getConfigCurrency(precDDSo_DonGia, thousands, decimal, false, negative);
            rs.DDSo_DonGiaNT = this.getConfigCurrency(precDDSo_DonGiaNT, thousands, decimal, false, negative);
            rs.DDSo_TyGia = this.getConfigCurrency(precDDSo_TyGia, thousands, decimal, false, negative);
            rs.DDSo_TyLe = this.getConfigCurrency(precDDSo_TyLe, thousands, decimal, false, negative);
            rs.DDSo_TyLePBo = this.getConfigCurrency(precDDSo_TyLePBo, thousands, decimal, false, negative);
            return rs;
        } else {
            const rs: CurrencyMaskModel = {};
            rs.DDSo_TienVND = this.getConfigCurrency(precDDSo_TienVND, thousands, decimal, true, negative);
            rs.DDSo_NgoaiTe = this.getConfigCurrency(precDDSo_NgoaiTe, thousands, decimal, true, negative);
            rs.DDSo_SoLuong = this.getConfigCurrency(precDDSo_SoLuong, thousands, decimal, true, negative);
            rs.DDSo_DonGia = this.getConfigCurrency(precDDSo_DonGia, thousands, decimal, true, negative);
            rs.DDSo_DonGiaNT = this.getConfigCurrency(precDDSo_DonGiaNT, thousands, decimal, true, negative);
            rs.DDSo_TyGia = this.getConfigCurrency(precDDSo_TyGia, thousands, decimal, true, negative);
            rs.DDSo_TyLe = this.getConfigCurrency(precDDSo_TyLe, thousands, decimal, true, negative);
            rs.DDSo_TyLePBo = this.getConfigCurrency(precDDSo_TyLePBo, thousands, decimal, true, negative);
            return rs;
        }
    }

    checkAccoutWithDetailType(
        debitAccounts: IAccountList[],
        creditAccounts: IAccountList[],
        listDetail: object[],
        accountingObjects: IAccountingObject[],
        costSets: ICostSet[],
        eMContracts: IEMContract[],
        materialGoods: IMaterialGoods[],
        bankAccountDetails: IBankAccountDetails[],
        departments: IOrganizationUnit[],
        expenseItems: IExpenseItem[],
        budgetItems: IBudgetItem[],
        statisticsCodes: IStatisticsCode[],
        voucher?: object,
        option?: boolean
    ) {
        let result = null;
        const tkNo = 'debitAccount';
        const tkCo = 'creditAccount';
        const dtT = 'detailType';
        let continueForEach = true;
        listDetail.forEach((dt: object) => {
            if (!option) {
                if (continueForEach) {
                    if (dt.hasOwnProperty(tkNo)) {
                        const acc_f = debitAccounts.find(n => n.accountNumber === dt[tkNo]);
                        result = this.getResult(
                            acc_f,
                            dt,
                            accountingObjects,
                            costSets,
                            eMContracts,
                            materialGoods,
                            bankAccountDetails,
                            departments,
                            expenseItems,
                            budgetItems,
                            statisticsCodes,
                            voucher
                        );
                        if (result) {
                            continueForEach = false;
                        }
                    }
                }
                if (continueForEach) {
                    if (dt.hasOwnProperty(tkCo)) {
                        const acc_f = creditAccounts.find(n => n.accountNumber === dt[tkCo]);
                        result = this.getResult(
                            acc_f,
                            dt,
                            accountingObjects,
                            costSets,
                            eMContracts,
                            materialGoods,
                            bankAccountDetails,
                            departments,
                            expenseItems,
                            budgetItems,
                            statisticsCodes,
                            voucher
                        );
                        if (result) {
                            continueForEach = false;
                        }
                    }
                }
            } else {
                if (continueForEach) {
                    if (dt.hasOwnProperty(tkNo)) {
                        const acc_f = debitAccounts.find(n => n.accountNumber === dt[tkNo]);
                        result = this.getResult(
                            acc_f,
                            dt,
                            accountingObjects,
                            costSets,
                            eMContracts,
                            materialGoods,
                            bankAccountDetails,
                            departments,
                            expenseItems,
                            budgetItems,
                            statisticsCodes,
                            voucher,
                            1
                        );
                        if (result) {
                            continueForEach = false;
                        }
                    }
                }
                if (continueForEach) {
                    if (dt.hasOwnProperty(tkCo)) {
                        const acc_f = creditAccounts.find(n => n.accountNumber === dt[tkCo]);
                        result = this.getResult(
                            acc_f,
                            dt,
                            accountingObjects,
                            costSets,
                            eMContracts,
                            materialGoods,
                            bankAccountDetails,
                            departments,
                            expenseItems,
                            budgetItems,
                            statisticsCodes,
                            voucher,
                            2
                        );
                        if (result) {
                            continueForEach = false;
                        }
                    }
                }
            }
        });
        return result;
    }

    findByRowNum(req?: any): Observable<EntityResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<object>(SERVER_API_URL + 'api/voucher/findByRowNum', {
                params: options,
                observe: 'response'
            })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    private convertDateFromServer(res: EntityResponseType): EntityResponseType {
        res.body['date'] = res.body['date'] != null ? moment(res.body['date']) : null;
        res.body['postedDate'] = res.body['postedDate'] != null ? moment(res.body['postedDate']) : null;
        res.body['issuedate'] = res.body['issuedate'] != null ? moment(res.body['issuedate']) : null;
        res.body['matchdate'] = res.body['matchdate'] != null ? moment(res.body['matchdate']) : null;
        return res;
    }

    getGenCodeVoucher(req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.get(SERVER_API_URL + 'api/voucher/genCodeVoucher', {
            params: options,
            observe: 'response',
            responseType: 'text'
        });
    }

    checkNoBook(noFBook: string, account?: any): boolean {
        const regExp = /\d/;
        if (noFBook.length > 25) {
            this.toastr.error(
                this.translate.instant('ebwebApp.mBTellerPaper.error.noLengthGreater25'),
                this.translate.instant('ebwebApp.mBTellerPaper.error.error')
            );
            return false;
        } else if (!regExp.test(noFBook)) {
            this.toastr.error(
                this.translate.instant('ebwebApp.mBTellerPaper.error.noExistNumber'),
                this.translate.instant('ebwebApp.mBTellerPaper.error.error')
            );
            return false;
        }
        if (account) {
            const isLeftToRight = account.systemOption.some(x => x.code === TangSoChungTu && x.data === '0');
            const no: any[] = noFBook.match(/^\d+|\d+\b|\d+(?=\w)/g);
            if (isLeftToRight) {
                if (Number(no[0]) > 2147483647) {
                    this.toastr.error(
                        this.translate.instant('ebwebApp.mBTellerPaper.error.noValidate'),
                        this.translate.instant('ebwebApp.mBTellerPaper.error.error')
                    );
                    return false;
                }
            } else {
                if (Number(no[no.length - 1]) > 2147483647) {
                    this.toastr.error(
                        this.translate.instant('ebwebApp.mBTellerPaper.error.noValidate'),
                        this.translate.instant('ebwebApp.mBTellerPaper.error.error')
                    );
                    return false;
                }
            }
        }
        return true;
        /*const regExp = /^\D*\d{3,}.*$/;
        if (account) {
            const typeGenVocherNo = account.systemOption.find(x => x.code === TangSoChungTu && x.data).data;
            if (typeGenVocherNo === '1') {
                const no = Array.from(noFBook)
                    .reverse()
                    .join('');
                if (no.length > 25) {
                    this.toastr.error(
                        this.translate.instant('ebwebApp.mBTellerPaper.error.noLengthGreater25'),
                        this.translate.instant('ebwebApp.mBTellerPaper.error.error')
                    );
                    return false;
                } else if (!regExp.test(no)) {
                    this.toastr.error(
                        this.translate.instant('ebwebApp.mBTellerPaper.error.noValidateDefine'),
                        this.translate.instant('ebwebApp.mBTellerPaper.error.error')
                    );
                    return false;
                } else {
                    return true;
                }
            } else {
                if (noFBook.length > 25) {
                    this.toastr.error(
                        this.translate.instant('ebwebApp.mBTellerPaper.error.noLengthGreater25'),
                        this.translate.instant('ebwebApp.mBTellerPaper.error.error')
                    );
                    return false;
                } else if (!regExp.test(noFBook)) {
                    this.toastr.error(
                        this.translate.instant('ebwebApp.mBTellerPaper.error.noValidateDefine'),
                        this.translate.instant('ebwebApp.mBTellerPaper.error.error')
                    );
                    return false;
                } else {
                    return true;
                }
            }
        } else {
            if (noFBook.length > 25) {
                this.toastr.error(
                    this.translate.instant('ebwebApp.mBTellerPaper.error.noLengthGreater25'),
                    this.translate.instant('ebwebApp.mBTellerPaper.error.error')
                );
                return false;
            } else if (!regExp.test(noFBook)) {
                this.toastr.error(
                    this.translate.instant('ebwebApp.mBTellerPaper.error.noValidateDefine'),
                    this.translate.instant('ebwebApp.mBTellerPaper.error.error')
                );
                return false;
            } else {
                return true;
            }
        }*/
    }

    getIndexRow(req?: any): Observable<EntityResponseType> {
        const options = createRequestOption(req);
        return this.http.get<object>(SERVER_API_URL + 'api/voucher/getIndexVoucher', {
            params: options,
            observe: 'response'
        });
    }

    private getResult(
        acc_f: IAccountList,
        dt: object,
        accountingObjects: IAccountingObject[],
        costSets: ICostSet[],
        eMContracts: IEMContract[],
        materialGoods: IMaterialGoods[],
        bankAccountDetails: IBankAccountDetails[],
        departments: IOrganizationUnit[],
        expenseItems: IExpenseItem[],
        budgetItems: IBudgetItem[],
        statisticsCodes: IStatisticsCode[],
        voucher?: object,
        accType?: number
    ) {
        let result = null;
        if (acc_f) {
            if (acc_f.detailType) {
                const lstacc_Dtt = acc_f.detailType.split(';');
                lstacc_Dtt.forEach((detailType: string) => {
                    if (detailType === '0') {
                        if (accountingObjects) {
                            if (accType === 1) {
                                const acc_Object = accountingObjects.find(n => n.id === dt['debitAccountingObjectID']);
                                if (!acc_Object) {
                                    result =
                                        'Đối tượng bị trống!\n' +
                                        'Tài khoản ' +
                                        acc_f.accountNumber +
                                        ' chi tiết theo Đối tượng nợ Nhà cung cấp';
                                } else if (acc_Object.objectType !== 0 && acc_Object.objectType !== 2) {
                                    result =
                                        'Đối tượng bị trống!\n' +
                                        'Tài khoản ' +
                                        acc_f.accountNumber +
                                        ' chi tiết theo Đối tượng nợ Nhà cung cấp';
                                }
                            } else if (accType === 2) {
                                const acc_Object = accountingObjects.find(n => n.id === dt['creditAccountingObjectID']);
                                if (!acc_Object) {
                                    result =
                                        'Đối tượng bị trống!\n' +
                                        'Tài khoản ' +
                                        acc_f.accountNumber +
                                        ' chi tiết theo Đối tượng có Nhà cung cấp';
                                } else if (acc_Object.objectType !== 0 && acc_Object.objectType !== 2) {
                                    result =
                                        'Đối tượng bị trống!\n' +
                                        'Tài khoản ' +
                                        acc_f.accountNumber +
                                        ' chi tiết theo Đối tượng có Nhà cung cấp';
                                }
                            } else {
                                if (dt.hasOwnProperty('accountingObject')) {
                                    if (!dt['accountingObject']) {
                                        result =
                                            'Đối tượng bị trống!\n' +
                                            'Tài khoản ' +
                                            acc_f.accountNumber +
                                            ' chi tiết theo Đối tượng Nhà cung cấp';
                                    } else {
                                        const acc_Object = accountingObjects.find(n => n.id === dt['accountingObject']['id']);
                                        if (!acc_Object) {
                                            result =
                                                'Đối tượng bị trống!\n' +
                                                'Tài khoản ' +
                                                acc_f.accountNumber +
                                                ' chi tiết theo Đối tượng Nhà cung cấp';
                                        } else if (acc_Object.objectType !== 0 && acc_Object.objectType !== 2) {
                                            result =
                                                'Đối tượng bị trống!\n' +
                                                'Tài khoản ' +
                                                acc_f.accountNumber +
                                                ' chi tiết theo Đối tượng Nhà cung cấp';
                                        }
                                    }
                                } else {
                                    const acc_Object = accountingObjects.find(
                                        n =>
                                            n.id === dt['accountingObjectID'] ||
                                            n.id === dt['accountingObjectId'] ||
                                            n.id === dt['postedObjectId']
                                    );
                                    if (!acc_Object) {
                                        result =
                                            'Đối tượng bị trống!\n' +
                                            'Tài khoản ' +
                                            acc_f.accountNumber +
                                            ' chi tiết theo Đối tượng Nhà cung cấp';
                                    } else if (acc_Object.objectType !== 0 && acc_Object.objectType !== 2) {
                                        result =
                                            'Đối tượng bị trống!\n' +
                                            'Tài khoản ' +
                                            acc_f.accountNumber +
                                            ' chi tiết theo Đối tượng Nhà cung cấp';
                                    }
                                }
                            }
                        }
                    } else if (detailType === '1') {
                        if (accountingObjects) {
                            if (accType === 1) {
                                const acc_Object = accountingObjects.find(n => n.id === dt['debitAccountingObjectID']);
                                if (!acc_Object) {
                                    result =
                                        'Đối tượng bị trống!\n' +
                                        'Tài khoản ' +
                                        acc_f.accountNumber +
                                        ' chi tiết theo Đối tượng nợ Khách hàng';
                                } else if (acc_Object.objectType !== 1 && acc_Object.objectType !== 2) {
                                    result =
                                        'Đối tượng bị trống!\n' +
                                        'Tài khoản ' +
                                        acc_f.accountNumber +
                                        ' chi tiết theo Đối tượng nợ Khách hàng';
                                }
                            } else if (accType === 2) {
                                const acc_Object = accountingObjects.find(n => n.id === dt['creditAccountingObjectID']);
                                if (!acc_Object) {
                                    result =
                                        'Đối tượng bị trống!\n' +
                                        'Tài khoản ' +
                                        acc_f.accountNumber +
                                        ' chi tiết theo Đối tượng có Khách hàng';
                                } else if (acc_Object.objectType !== 1 && acc_Object.objectType !== 2) {
                                    result =
                                        'Đối tượng bị trống!\n' +
                                        'Tài khoản ' +
                                        acc_f.accountNumber +
                                        ' chi tiết theo Đối tượng có Khách hàng';
                                }
                            } else {
                                if (dt.hasOwnProperty('accountingObject')) {
                                    if (!dt['accountingObject']) {
                                        result =
                                            'Đối tượng bị trống!\n' +
                                            'Tài khoản ' +
                                            acc_f.accountNumber +
                                            ' chi tiết theo Đối tượng Khách hàng';
                                    } else {
                                        const acc_Object = accountingObjects.find(n => n.id === dt['accountingObject']['id']);
                                        if (!acc_Object) {
                                            result =
                                                'Đối tượng bị trống!\n' +
                                                'Tài khoản ' +
                                                acc_f.accountNumber +
                                                ' chi tiết theo Đối tượng Khách hàng';
                                        } else if (acc_Object.objectType !== 1 && acc_Object.objectType !== 2) {
                                            result =
                                                'Đối tượng bị trống!\n' +
                                                'Tài khoản ' +
                                                acc_f.accountNumber +
                                                ' chi tiết theo Đối tượng Khách hàng';
                                        }
                                    }
                                } else {
                                    const acc_Object = accountingObjects.find(
                                        n => n.id === dt['accountingObjectID'] || n.id === dt['accountingObjectId']
                                    );
                                    if (!acc_Object) {
                                        result =
                                            'Đối tượng bị trống!\n' +
                                            'Tài khoản ' +
                                            acc_f.accountNumber +
                                            ' chi tiết theo Đối tượng Khách hàng';
                                    } else if (acc_Object.objectType !== 1 && acc_Object.objectType !== 2) {
                                        result =
                                            'Đối tượng bị trống!\n' +
                                            'Tài khoản ' +
                                            acc_f.accountNumber +
                                            ' chi tiết theo Đối tượng Khách hàng';
                                    }
                                }
                            }
                        }
                    } else if (detailType === '2') {
                        if (accountingObjects) {
                            if (accType === 1) {
                                const acc_Object = accountingObjects.find(n => n.id === dt['debitAccountingObjectID']);
                                if (!acc_Object) {
                                    result =
                                        'Đối tượng bị trống!\n' +
                                        'Tài khoản ' +
                                        acc_f.accountNumber +
                                        ' chi tiết theo Đối tượng nợ Nhân viên';
                                } else if (!acc_Object.isEmployee) {
                                    result =
                                        'Đối tượng bị trống!\n' +
                                        'Tài khoản ' +
                                        acc_f.accountNumber +
                                        ' chi tiết theo Đối tượng nợ Nhân viên';
                                }
                            } else if (accType === 2) {
                                const acc_Object = accountingObjects.find(n => n.id === dt['creditAccountingObjectID']);
                                if (!acc_Object) {
                                    result =
                                        'Đối tượng bị trống!\n' +
                                        'Tài khoản ' +
                                        acc_f.accountNumber +
                                        ' chi tiết theo Đối tượng có Nhân viên';
                                } else if (!acc_Object.isEmployee) {
                                    result =
                                        'Đối tượng bị trống!\n' +
                                        'Tài khoản ' +
                                        acc_f.accountNumber +
                                        ' chi tiết theo Đối tượng có Nhân viên';
                                }
                            } else {
                                if (dt.hasOwnProperty('accountingObject')) {
                                    if (!dt['accountingObject']) {
                                        result =
                                            'Đối tượng bị trống!\n' +
                                            'Tài khoản ' +
                                            acc_f.accountNumber +
                                            ' chi tiết theo Đối tượng Nhân viên';
                                    } else {
                                        const acc_Object = accountingObjects.find(n => n.id === dt['accountingObject']['id']);
                                        if (!acc_Object) {
                                            result =
                                                'Đối tượng bị trống!\n' +
                                                'Tài khoản ' +
                                                acc_f.accountNumber +
                                                ' chi tiết theo Đối tượng Nhân viên';
                                        } else if (!acc_Object.isEmployee) {
                                            result =
                                                'Đối tượng bị trống!\n' +
                                                'Tài khoản ' +
                                                acc_f.accountNumber +
                                                ' chi tiết theo Đối tượng Nhân viên';
                                        }
                                    }
                                } else {
                                    const acc_Object = accountingObjects.find(
                                        n =>
                                            n.id === dt['accountingObjectID'] ||
                                            n.id === dt['accountingObjectId'] ||
                                            n.id === dt['postedObjectId']
                                    );
                                    if (!acc_Object) {
                                        result =
                                            'Đối tượng bị trống!\n' +
                                            'Tài khoản ' +
                                            acc_f.accountNumber +
                                            ' chi tiết theo Đối tượng Nhân viên';
                                    } else if (!acc_Object.isEmployee) {
                                        result =
                                            'Đối tượng bị trống!\n' +
                                            'Tài khoản ' +
                                            acc_f.accountNumber +
                                            ' chi tiết theo Đối tượng Nhân viên';
                                    }
                                }
                            }
                        }
                    } else if (detailType === '3') {
                        if (costSets) {
                            if (dt.hasOwnProperty('costSet')) {
                                if (!dt['costSet']) {
                                    result =
                                        'Đối tượng tập hợp chi phí bị trống!\n' +
                                        'Tài khoản ' +
                                        acc_f.accountNumber +
                                        ' chi tiết theo đối tượng tập hợp chi phí';
                                } else {
                                    const costSet = costSets.find(n => n.id === dt['costSet']['id']);
                                    if (!costSet) {
                                        result =
                                            'Đối tượng tập hợp chi phí bị trống!\n' +
                                            'Tài khoản ' +
                                            acc_f.accountNumber +
                                            ' chi tiết theo đối tượng tập hợp chi phí';
                                    }
                                }
                            } else {
                                const costSet = costSets.find(n => n.id === (dt['costSetID'] ? dt['costSetID'] : dt['costSetId']));
                                if (!costSet) {
                                    result =
                                        'Đối tượng tập hợp chi phí bị trống!\n' +
                                        'Tài khoản ' +
                                        acc_f.accountNumber +
                                        ' chi tiết theo đối tượng tập hợp chi phí';
                                }
                            }
                        }
                    } else if (detailType === '4') {
                        if (eMContracts) {
                            if (dt.hasOwnProperty('eMContract')) {
                                if (!dt['eMContract']) {
                                    result = 'Hợp đồng bị trống!\n' + 'Tài khoản ' + acc_f.accountNumber + ' chi tiết theo hợp đồng';
                                } else {
                                    const eMContract = eMContracts.find(n => n.id === dt['eMContract']['id']);
                                    if (!eMContract) {
                                        result = 'Hợp đồng bị trống!\n' + 'Tài khoản ' + acc_f.accountNumber + ' chi tiết theo hợp đồng';
                                    }
                                }
                            } else {
                                const eMContract = eMContracts.find(n => n.id === dt['contractID']);
                                if (!eMContract) {
                                    result = 'Hợp đồng bị trống!\n' + 'Tài khoản ' + acc_f.accountNumber + ' chi tiết theo hợp đồng';
                                }
                            }
                        }
                    } else if (detailType === '5') {
                        if (materialGoods) {
                            if (dt.hasOwnProperty('materialGood')) {
                                if (!dt['materialGood']) {
                                    result =
                                        'Vật tư hàng hóa công cụ dụng cụ bị trống!\n' +
                                        'Tài khoản ' +
                                        acc_f.accountNumber +
                                        ' chi tiết theo vật tư hàng hóa công cụ dụng cụ';
                                } else {
                                    const materialGood = materialGoods.find(n => n.id === dt['materialGood']['id']);
                                    if (!materialGood) {
                                        result =
                                            'Vật tư hàng hóa công cụ dụng cụ bị trống!\n' +
                                            'Tài khoản ' +
                                            acc_f.accountNumber +
                                            ' chi tiết theo vật tư hàng hóa công cụ dụng cụ';
                                    }
                                }
                            } else if (dt.hasOwnProperty('materialGoods')) {
                                if (!dt['materialGoods']) {
                                    result =
                                        'Vật tư hàng hóa công cụ dụng cụ bị trống!\n' +
                                        'Tài khoản ' +
                                        acc_f.accountNumber +
                                        ' chi tiết theo vật tư hàng hóa công cụ dụng cụ';
                                } else {
                                    const materialGood = materialGoods.find(n => n.id === dt['materialGoods']['id']);
                                    if (!materialGood) {
                                        result =
                                            'Vật tư hàng hóa công cụ dụng cụ bị trống!\n' +
                                            'Tài khoản ' +
                                            acc_f.accountNumber +
                                            ' chi tiết theo vật tư hàng hóa công cụ dụng cụ';
                                    }
                                }
                            } else {
                                const materialGood = materialGoods.find(n => n.id === dt['materialGoodID']);
                                if (!materialGood) {
                                    result =
                                        'Vật tư hàng hóa công cụ dụng cụ bị trống!\n' +
                                        'Tài khoản ' +
                                        acc_f.accountNumber +
                                        ' chi tiết theo vật tư hàng hóa công cụ dụng cụ';
                                }
                            }
                        }
                    } else if (detailType === '6') {
                        if (bankAccountDetails) {
                            if (dt.hasOwnProperty('bankAccountDetails')) {
                                if (!dt['bankAccountDetails']) {
                                    result =
                                        'Tài khoản ngân hàng bị trống!\n' +
                                        'Tài khoản ' +
                                        acc_f.accountNumber +
                                        ' chi tiết theo tài khoản ngân hàng';
                                } else {
                                    const bankAccountDetail = bankAccountDetails.find(n => n.id === dt['bankAccountDetails']['id']);
                                    if (!bankAccountDetail) {
                                        result =
                                            'Tài khoản ngân hàng bị trống!\n' +
                                            'Tài khoản ' +
                                            acc_f.accountNumber +
                                            ' chi tiết theo tài khoản ngân hàng';
                                    }
                                }
                            } else if (voucher) {
                                const bankAccountDetail = bankAccountDetails.find(
                                    n =>
                                        n.id ===
                                        (voucher['bankAccountDetailID']
                                            ? voucher['bankAccountDetailID']
                                            : voucher['accountPaymentId']
                                                ? voucher['accountPaymentId']
                                                : voucher['creditCardItem'] ? voucher['creditCardItem']['id'] : null)
                                );
                                if (!bankAccountDetail) {
                                    result =
                                        'Tài khoản ngân hàng bị trống!\n' +
                                        'Tài khoản ' +
                                        acc_f.accountNumber +
                                        ' chi tiết theo tài khoản ngân hàng';
                                }
                            } else {
                                const bankAccountDetail = bankAccountDetails.find(n => n.id === dt['bankAccountDetailID']);
                                if (!bankAccountDetail) {
                                    result =
                                        'Tài khoản ngân hàng bị trống!\n' +
                                        'Tài khoản ' +
                                        acc_f.accountNumber +
                                        ' chi tiết theo tài khoản ngân hàng';
                                }
                            }
                        }
                    } else if (detailType === '7') {
                        if (departments) {
                            if (dt.hasOwnProperty('organizationUnit')) {
                                if (!dt['organizationUnit']) {
                                    result = 'Phòng ban bị trống!\n' + 'Tài khoản ' + acc_f.accountNumber + ' chi tiết theo phòng ban';
                                } else {
                                    const organizationUnit = departments.find(n => n.id === dt['organizationUnit']['id']);
                                    if (!organizationUnit) {
                                        result = 'Phòng ban bị trống!\n' + 'Tài khoản ' + acc_f.accountNumber + ' chi tiết theo phòng ban';
                                    }
                                }
                            } else if (dt.hasOwnProperty('department')) {
                                if (!dt['department']) {
                                    result = 'Phòng ban bị trống!\n' + 'Tài khoản ' + acc_f.accountNumber + ' chi tiết theo phòng ban';
                                } else {
                                    const department = departments.find(n => n.id === dt['department']['id']);
                                    if (!department) {
                                        result = 'Phòng ban bị trống!\n' + 'Tài khoản ' + acc_f.accountNumber + ' chi tiết theo phòng ban';
                                    }
                                }
                            } else {
                                const organizationUnit = departments.find(
                                    n =>
                                        n.id === dt['organizationUnitID'] ||
                                        n.id === (dt['departmentID'] ? dt['departmentID'] : dt['departmentId'])
                                );
                                if (!organizationUnit) {
                                    result = 'Phòng ban bị trống!\n' + 'Tài khoản ' + acc_f.accountNumber + ' chi tiết theo phòng ban';
                                }
                            }
                        }
                    } else if (detailType === '8') {
                        if (expenseItems) {
                            if (dt.hasOwnProperty('expenseItem')) {
                                if (!dt['expenseItem']) {
                                    result =
                                        'Khoản mục chi phí bị trống!\n' +
                                        'Tài khoản ' +
                                        acc_f.accountNumber +
                                        ' chi tiết theo khoản mục chi phí';
                                } else {
                                    const expenseItem = expenseItems.find(n => n.id === dt['expenseItem']['id']);
                                    if (!expenseItem) {
                                        result =
                                            'Khoản mục chi phí bị trống!\n' +
                                            'Tài khoản ' +
                                            acc_f.accountNumber +
                                            ' chi tiết theo khoản mục chi phí';
                                    }
                                }
                            } else {
                                const expenseItem = expenseItems.find(
                                    n => n.id === (dt['expenseItemID'] ? dt['expenseItemID'] : dt['expenseItemId'])
                                );
                                if (!expenseItem) {
                                    result =
                                        'Khoản mục chi phí bị trống!\n' +
                                        'Tài khoản ' +
                                        acc_f.accountNumber +
                                        ' chi tiết theo khoản mục chi phí';
                                }
                            }
                        }
                    } else if (detailType === '9') {
                        if (budgetItems) {
                            if (dt.hasOwnProperty('budgetItem')) {
                                if (!dt['budgetItem']) {
                                    result = 'Mục thu chi bị trống!\n' + 'Tài khoản ' + acc_f.accountNumber + ' chi tiết theo mục thu chi';
                                } else {
                                    const budgetItem = budgetItems.find(n => n.id === dt['budgetItem']['id']);
                                    if (!budgetItem) {
                                        result =
                                            'Mục thu chi bị trống!\n' + 'Tài khoản ' + acc_f.accountNumber + ' chi tiết theo mục thu chi';
                                    }
                                }
                            } else {
                                const budgetItem = budgetItems.find(
                                    n => n.id === (dt['budgetItemID'] ? dt['budgetItemID'] : dt['budgetItemId'])
                                );
                                if (!budgetItem) {
                                    result = 'Mục thu chi bị trống!\n' + 'Tài khoản ' + acc_f.accountNumber + ' chi tiết theo mục thu chi';
                                }
                            }
                        }
                    } else if (detailType === '10') {
                        if (statisticsCodes) {
                            if (dt.hasOwnProperty('statisticsCode') && !dt.hasOwnProperty('postedObjectName')) {
                                if (!dt['statisticsCode']) {
                                    result = 'Mã thống kê bị trống!\n' + 'Tài khoản ' + acc_f.accountNumber + ' chi tiết theo mã thống kê';
                                } else {
                                    const statisticsCode = statisticsCodes.find(n => n.id === dt['statisticsCode']['id']);
                                    if (!statisticsCode) {
                                        result =
                                            'Mã thống kê bị trống!\n' + 'Tài khoản ' + acc_f.accountNumber + ' chi tiết theo mã thống kê';
                                    }
                                }
                            } else {
                                const statisticsCode = statisticsCodes.find(
                                    n =>
                                        n.id ===
                                            (dt['statisticsCodeID']
                                                ? dt['statisticsCodeID']
                                                : dt['statisticsId'] ? dt['statisticsId'] : dt['statisticsCodeId']) ||
                                        n.id === dt['statisticCodeID']
                                );
                                if (!statisticsCode) {
                                    result = 'Mã thống kê trống!\n' + 'Tài khoản ' + acc_f.accountNumber + ' chi tiết theo mã thống kê';
                                }
                            }
                        }
                    }
                    if (result) {
                        return result;
                    }
                });
            }
        }
        return result;
    }

    navigateToOtherSite(routerLink: any, typeModule?): void {
        let location = '';
        if (!typeModule) {
            location = window.location.origin + `/#/` + routerLink;
            location = location.replace(window.location.port, '9000');
        } else {
            location = window.location.origin + `/#/` + routerLink;
        }
        window.location.href = location;
    }
}
