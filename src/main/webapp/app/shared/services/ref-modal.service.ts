import { Injectable } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import {EbRefModalComponent} from 'app/shared/ref/ref.component';

@Injectable({ providedIn: 'root' })
export class RefModalService {
    private isOpen = false;

    constructor(private modalService: NgbModal) {}

    open(
        data,
        refModal?,
        modalData?,
        isSizeSm?: boolean,
        typeID?,
        disableWindowClass?,
        currencyID?,
        objectId?,
        accountingObjectId?,
        force?,
        strCodePrint?,
        codePrint?,
        sizeOverridePopup?
    ): NgbModalRef {
        if (!force) {
            if (this.isOpen) {
                return;
            }
        }
        this.isOpen = true;
        let modalRef;
        if (sizeOverridePopup) {
            modalRef = this.modalService.open(refModal ? refModal : EbRefModalComponent, {
                size: 'lg',
                backdrop: 'static',
                windowClass: 'width-80 width-60'
            });
        } else {
            modalRef = this.modalService.open(refModal ? refModal : EbRefModalComponent, {
                size: isSizeSm ? 'sm' : 'lg',
                windowClass: disableWindowClass ? disableWindowClass : codePrint ? 'width-25' : 'width-80',
                backdrop: 'static'
            });
        }
        modalRef.componentInstance.data = data;
        if (modalData) {
            modalRef.componentInstance.modalData = modalData;
        }
        if (typeID) {
            modalRef.componentInstance.typeID = typeID;
        }
        if (currencyID) {
            modalRef.componentInstance.currencyID = currencyID;
        }
        if (objectId) {
            modalRef.componentInstance.objectId = objectId;
        }
        if (accountingObjectId) {
            modalRef.componentInstance.accountingObjectId = accountingObjectId;
        }
        if (strCodePrint) {
            modalRef.componentInstance.strCodePrint = strCodePrint;
        }
        modalRef.result.then(
            result => {
                this.isOpen = false;
            },
            reason => {
                this.isOpen = false;
            }
        );
        return modalRef;
    }
}
