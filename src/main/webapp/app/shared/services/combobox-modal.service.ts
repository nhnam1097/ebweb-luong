import { Injectable } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { CategoryName } from 'app/app.constants';
import {UtilsService} from 'app/shared/UtilsService/Utils.service';
import {IAccountingObject} from 'app/shared/models/accounting-object.model';

@Injectable({ providedIn: 'root' })
export class ComboboxModalService {
    public isOpen = false;

    constructor(private modalService: NgbModal, private utilsService: UtilsService) {}

    open(data, typeObject?, isCostSet?, isPopupOverride?): NgbModalRef {
        if (this.isOpen) {
            return;
        }
        // this.isOpen = true;
        // let modalRef;
        // if (data === CategoryName.DOI_TUONG) {
        //     modalRef = this.modalService.open(EbAccountingObjectComboboxComponent, {
        //         size: 'lg',
        //         windowClass: 'width-80 height-90',
        //         backdrop: 'static'
        //     });
        //     const acc: IAccountingObject = {};
        //     acc.objectType = 1;
        //     const type = 1;
        //     modalRef.componentInstance.type = type;
        //     modalRef.componentInstance.data = acc;
        //     modalRef.result.then(
        //         result => {
        //             this.isOpen = false;
        //         },
        //         reason => {
        //             this.isOpen = false;
        //         }
        //     );
        // } else if (data === CategoryName.KHACH_HANG || data === CategoryName.KHAC) {
        //     modalRef = this.modalService.open(EbAccountingObjectComboboxComponent, {
        //         size: 'lg',
        //         windowClass: 'width-80 height-90',
        //         backdrop: 'static'
        //     });
        //     const acc: IAccountingObject = {};
        //     if (data === CategoryName.KHACH_HANG) {
        //         acc.objectType = 1;
        //         const type = 1;
        //         modalRef.componentInstance.data = acc;
        //         modalRef.componentInstance.type = type;
        //         modalRef.result.then(
        //             result => {
        //                 this.isOpen = false;
        //             },
        //             reason => {
        //                 this.isOpen = false;
        //             }
        //         );
        //     } else {
        //         acc.objectType = 3;
        //         const type = 3;
        //         modalRef.componentInstance.data = acc;
        //         modalRef.componentInstance.type = type;
        //         modalRef.result.then(
        //             result => {
        //                 this.isOpen = false;
        //             },
        //             reason => {
        //                 this.isOpen = false;
        //             }
        //         );
        //     }
        // } else if (data === CategoryName.NHA_CUNG_CAP) {
        //     modalRef = this.modalService.open(EbAccountingObjectComboboxComponent, {
        //         size: 'lg',
        //         windowClass: 'width-80 height-90',
        //         backdrop: 'static'
        //     });
        //     const acc: IAccountingObject = {};
        //     acc.objectType = 0;
        //     const type = 0;
        //     modalRef.componentInstance.data = acc;
        //     modalRef.componentInstance.type = type;
        //     modalRef.result.then(
        //         result => {
        //             this.isOpen = false;
        //         },
        //         reason => {
        //             this.isOpen = false;
        //         }
        //     );
        // } else if (data === CategoryName.NHAN_VIEN) {
        //     modalRef = this.modalService.open(EbEmployeeComboboxComponent, {
        //         size: 'lg',
        //         windowClass: 'width-80',
        //         backdrop: 'static'
        //     });
        //     if (typeObject) {
        //         modalRef.componentInstance.typeObject = typeObject;
        //     }
        //     modalRef.componentInstance.data = data;
        //     modalRef.result.then(
        //         result => {
        //             this.isOpen = false;
        //         },
        //         reason => {
        //             this.isOpen = false;
        //         }
        //     );
        // } else if (data === CategoryName.DON_VI_TINH) {
        //     modalRef = this.modalService.open(UnitComboboxComponent, {
        //         size: 'lg',
        //         windowClass: 'width-80',
        //         backdrop: 'static'
        //     });
        //     modalRef.componentInstance.data = data;
        //     if (isPopupOverride) {
        //         this.utilsService.setPopupOverride(true);
        //     }
        //     modalRef.result.then(
        //         result => {
        //             this.isOpen = false;
        //         },
        //         reason => {
        //             this.isOpen = false;
        //         }
        //     );
        // } else if (data === CategoryName.KHO) {
        //     modalRef = this.modalService.open(RepositoryComboboxComponent, {
        //         size: 'lg',
        //         windowClass: 'width-80',
        //         backdrop: 'static'
        //     });
        //     modalRef.componentInstance.data = data;
        //     if (isPopupOverride) {
        //         this.utilsService.setPopupOverride(true);
        //     }
        //     modalRef.result.then(
        //         result => {
        //             this.isOpen = false;
        //         },
        //         reason => {
        //             this.isOpen = false;
        //         }
        //     );
        // } else if (data === CategoryName.DINH_KHOAN_TU_DONG) {
        //     modalRef = this.modalService.open(AutoPrincipleComboboxComponent, {
        //         size: 'lg',
        //         windowClass: 'width-80',
        //         backdrop: 'static'
        //     });
        //     modalRef.componentInstance.data = data;
        //     modalRef.result.then(
        //         result => {
        //             this.isOpen = false;
        //         },
        //         reason => {
        //             this.isOpen = false;
        //         }
        //     );
        // } else if (data === CategoryName.TAI_KHOAN_NGAN_HANG) {
        //     modalRef = this.modalService.open(EbBankAccountDetailComboboxComponent, {
        //         size: 'lg',
        //         windowClass: 'height-30',
        //         backdrop: 'static'
        //     });
        //     modalRef.componentInstance.data = data;
        //     modalRef.result.then(
        //         result => {
        //             this.isOpen = false;
        //         },
        //         reason => {
        //             this.isOpen = false;
        //         }
        //     );
        // } else if (data === CategoryName.MA_THONG_KE) {
        //     modalRef = this.modalService.open(EbStatisticsCodeComboboxComponent, {
        //         size: 'lg',
        //         windowClass: 'height-30',
        //         backdrop: 'static'
        //     });
        //     modalRef.componentInstance.data = data;
        //     modalRef.result.then(
        //         result => {
        //             this.isOpen = false;
        //         },
        //         reason => {
        //             this.isOpen = false;
        //         }
        //     );
        // } else if (data === CategoryName.HOP_DONG) {
        //     modalRef = this.modalService.open(EbEMContractComboboxComponent, {
        //         size: 'lg',
        //         windowClass: 'height-30',
        //         backdrop: 'static'
        //     });
        //     modalRef.componentInstance.data = data;
        //     modalRef.result.then(
        //         result => {
        //             this.isOpen = false;
        //         },
        //         reason => {
        //             this.isOpen = false;
        //         }
        //     );
        // } else if (data === CategoryName.THE_TIN_DUNG) {
        //     modalRef = this.modalService.open(CreditCardComboboxComponent, {
        //         size: 'lg',
        //         windowClass: 'height-30',
        //         backdrop: 'static'
        //     });
        //     modalRef.componentInstance.data = data;
        //     modalRef.result.then(
        //         result => {
        //             this.isOpen = false;
        //         },
        //         reason => {
        //             this.isOpen = false;
        //         }
        //     );
        // } else if (data === CategoryName.KHOAN_MUC_CHI_PHI) {
        //     modalRef = this.modalService.open(KhoanMucChiPhiListComboboxComponent, {
        //         size: 'lg',
        //         windowClass: 'height-30',
        //         backdrop: 'static'
        //     });
        //     modalRef.componentInstance.data = data;
        //     modalRef.result.then(
        //         result => {
        //             this.isOpen = false;
        //         },
        //         reason => {
        //             this.isOpen = false;
        //         }
        //     );
        // } else if (data === CategoryName.LOAI_VAT_TU_HANG_HOA) {
        //     modalRef = this.modalService.open(MaterialGoodsCategoryComboboxComponent, {
        //         size: 'lg',
        //         windowClass: 'height-30',
        //         backdrop: 'static'
        //     });
        //     modalRef.componentInstance.data = data;
        //     if (isPopupOverride) {
        //         this.utilsService.setPopupOverride(true);
        //     }
        //     modalRef.result.then(
        //         result => {
        //             this.isOpen = false;
        //         },
        //         reason => {
        //             this.isOpen = false;
        //         }
        //     );
        // } else if (data === CategoryName.VAT_TU_HANG_HOA) {
        //     modalRef = this.modalService.open(MaterialGoodsComboboxComponent, {
        //         size: 'lg',
        //         windowClass: 'height-30',
        //         backdrop: 'static'
        //     });
        //     modalRef.componentInstance.isCostSet = isCostSet;
        //     modalRef.result.then(
        //         result => {
        //             this.isOpen = false;
        //         },
        //         reason => {
        //             this.isOpen = false;
        //         }
        //     );
        // } else if (data === CategoryName.VAT_TU_HANG_HOA_PP_SERVICE) {
        //     modalRef = this.modalService.open(MaterialGoodsComboboxComponent, {
        //         size: 'lg',
        //         windowClass: 'height-30',
        //         backdrop: 'static'
        //     });
        //     modalRef.componentInstance.isPPService = true;
        //     modalRef.result.then(
        //         result => {
        //             this.isOpen = false;
        //         },
        //         reason => {
        //             this.isOpen = false;
        //         }
        //     );
        // } else if (data === CategoryName.LOAI_TIEN) {
        //     modalRef = this.modalService.open(CurrencyComboboxComponent, {
        //         size: 'lg',
        //         windowClass: 'height-30',
        //         backdrop: 'static'
        //     });
        //     modalRef.componentInstance.data = data;
        //     if (isPopupOverride) {
        //         this.utilsService.setPopupOverride(true);
        //     }
        //     modalRef.result.then(
        //         result => {
        //             this.isOpen = false;
        //         },
        //         reason => {
        //             this.isOpen = false;
        //         }
        //     );
        // } else if (data === CategoryName.DOI_TUONG_TAP_HOP_CHI_PHI) {
        //     modalRef = this.modalService.open(CostSetComboboxComponent, {
        //         size: 'lg',
        //         windowClass: 'height-30',
        //         backdrop: 'static'
        //     });
        //     // modalRef.componentInstance.data = data;
        //     modalRef.result.then(
        //         result => {
        //             this.isOpen = false;
        //         },
        //         reason => {
        //             this.isOpen = false;
        //         }
        //     );
        // } else if (data === CategoryName.NGAN_HANG) {
        //     modalRef = this.modalService.open(BankComboboxComponent, {
        //         size: 'lg',
        //         windowClass: 'height-30',
        //         backdrop: 'static'
        //     });
        //     modalRef.componentInstance.data = data;
        //     modalRef.result.then(
        //         result => {
        //             this.isOpen = false;
        //         },
        //         reason => {
        //             this.isOpen = false;
        //         }
        //     );
        // } else if (data === CategoryName.NHOM_HHDV_CHIU_THUE_TTĐB) {
        //     modalRef = this.modalService.open(MaterialGoodsSpecialTaxGroupCoboboxComponent, {
        //         size: 'lg',
        //         windowClass: 'height-30',
        //         backdrop: 'static'
        //     });
        //     modalRef.componentInstance.data = data;
        //     modalRef.result.then(
        //         result => {
        //             this.isOpen = false;
        //         },
        //         reason => {
        //             this.isOpen = false;
        //         }
        //     );
        // } else if (data === CategoryName.MUC_THU_CHI) {
        //     modalRef = this.modalService.open(EbBudgetItemComboboxComponent, {
        //         size: 'lg',
        //         windowClass: 'height-30',
        //         backdrop: 'static'
        //     });
        //     modalRef.componentInstance.data = data;
        //     modalRef.result.then(
        //         result => {
        //             this.isOpen = false;
        //         },
        //         reason => {
        //             this.isOpen = false;
        //         }
        //     );
        // } else if (data === CategoryName.PHONG_BAN) {
        //     modalRef = this.modalService.open(EbPhongBanComboboxComponent, {
        //         size: 'lg',
        //         windowClass: 'height-30',
        //         backdrop: 'static'
        //     });
        //     modalRef.componentInstance.data = data;
        //     if (isPopupOverride) {
        //         this.utilsService.setPopupOverride(true);
        //     }
        //     modalRef.result.then(
        //         result => {
        //             this.isOpen = false;
        //         },
        //         reason => {
        //             this.isOpen = false;
        //         }
        //     );
        // } else if (data === CategoryName.PHUONG_THUC_VAN_CHUYEN) {
        //     modalRef = this.modalService.open(EbTransportMethodComboboxComponent, {
        //         size: 'lg',
        //         windowClass: 'height-30',
        //         backdrop: 'static'
        //     });
        //     modalRef.componentInstance.data = data;
        //     modalRef.result.then(
        //         result => {
        //             this.isOpen = false;
        //         },
        //         reason => {
        //             this.isOpen = false;
        //         }
        //     );
        // } else if (data === CategoryName.LOAI_TSCD) {
        //     modalRef = this.modalService.open(LoaiTaiSanCoDinhComboboxComponent, {
        //         size: 'lg',
        //         windowClass: 'height-30',
        //         backdrop: 'static'
        //     });
        //     modalRef.componentInstance.data = data;
        //     modalRef.result.then(
        //         result => {
        //             this.isOpen = false;
        //         },
        //         reason => {
        //             this.isOpen = false;
        //         }
        //     );
        // } else if (data === CategoryName.CONG_CU_DUNG_CU) {
        //     modalRef = this.modalService.open(CongCuDungCuModalUpdateComponent, {
        //         size: 'lg',
        //         windowClass: 'height-30',
        //         backdrop: 'static'
        //     });
        //     modalRef.componentInstance.data = data;
        //     modalRef.result.then(
        //         result => {
        //             this.isOpen = false;
        //         },
        //         reason => {
        //             this.isOpen = false;
        //         }
        //     );
        // } else if (data === CategoryName.TAI_SAN_CO_DINH) {
        //     modalRef = this.modalService.open(FixedAssetPopupComponent, {
        //         size: 'lg',
        //         windowClass: 'height-30',
        //         backdrop: 'static'
        //     });
        //     modalRef.componentInstance.isCostSet = isCostSet;
        //     modalRef.result.then(
        //         result => {
        //             this.isOpen = false;
        //         },
        //         reason => {
        //             this.isOpen = false;
        //         }
        //     );
        // }
        return null;
    }
}
