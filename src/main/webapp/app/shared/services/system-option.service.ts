import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ISystemOption } from 'app/shared/models/system-option.model';

type EntityResponseString = HttpResponse<string>;
type EntityResponseType = HttpResponse<ISystemOption>;
type EntityArrayResponseType = HttpResponse<ISystemOption[]>;

@Injectable({ providedIn: 'root' })
export class SystemOptionService {
    private resourceUrl = SERVER_API_URL + 'api/system-options';

    constructor(private http: HttpClient) {}

    updatePostedDate(req?: string): Observable<EntityResponseString> {
        return this.http.put<string>(`${this.resourceUrl}/update-posted-date`, req, { observe: 'response' });
    }

    getSystemOptionsByCompanyID(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<ISystemOption[]>(this.resourceUrl + '/find-system-options-by-companyid', {
            params: options,
            observe: 'response'
        });
    }
}
