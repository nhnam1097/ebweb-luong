import { Injectable } from '@angular/core';
import { SERVER_API_URL } from 'app/app.constants';
import { HttpClient } from '@angular/common/http';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';
import { UtilsService } from 'app/shared/UtilsService/Utils.service';

@Injectable({ providedIn: 'root' })
export class BaoCaoService {
    private resourceUrl = SERVER_API_URL + 'api/business/report';
    private resourceUrlToolsIn = SERVER_API_URL;
    private instructionUrl = SERVER_API_URL + 'api/business/instruction';
    private sessionData: { type: string; data: any }[] = [];

    constructor(
        private http: HttpClient,
        private localStorage: LocalStorageService,
        private sessionStorage: SessionStorageService,
        private utilsService: UtilsService
    ) {}

    public changeCompany() {
        this.sessionData = JSON.parse(sessionStorage.getItem('searchReportSession')) || [];
        this.sessionData.forEach(item => {
            item.data.organizationUnit = null;
            item.data.dependent = false;
        });
        sessionStorage.setItem('searchReportSession', JSON.stringify(this.sessionData));
    }
}
