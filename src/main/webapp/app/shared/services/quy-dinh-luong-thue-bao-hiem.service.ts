import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';
import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import {IPSSalaryTaxInsuranceRegulation} from 'app/shared/models/ps-salary-tax-insurance-regulation.model';
import {HandlingResult} from 'app/shared/handling-result/handling-result.model';

type EntityResponseType = HttpResponse<IPSSalaryTaxInsuranceRegulation>;
type EntityArrayResponseType = HttpResponse<IPSSalaryTaxInsuranceRegulation[]>;

@Injectable({ providedIn: 'root' })
export class QuyDinhLuongThueBaoHiemService {
    private resourceUrl = SERVER_API_URL + 'api/ps-salary-tax-insurance-regulations';

    constructor(private http: HttpClient) {}

    create(pSSalaryTaxInsuranceRegulation: IPSSalaryTaxInsuranceRegulation): Observable<EntityResponseType> {
        return this.http.post<IPSSalaryTaxInsuranceRegulation>(this.resourceUrl, pSSalaryTaxInsuranceRegulation, { observe: 'response' });
    }

    update(pSSalaryTaxInsuranceRegulation: IPSSalaryTaxInsuranceRegulation): Observable<EntityResponseType> {
        return this.http.put<IPSSalaryTaxInsuranceRegulation>(this.resourceUrl, pSSalaryTaxInsuranceRegulation, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<IPSSalaryTaxInsuranceRegulation>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    deleteValidate(id: string): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}/validate`, { observe: 'response' });
    }

    deleteByListIDAcc(rq: any[]): Observable<HttpResponse<HandlingResult>> {
        return this.http.post<any>(`${this.resourceUrl}/delete-list`, rq, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IPSSalaryTaxInsuranceRegulation[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    getAll(req?: any): Observable<HttpResponse<any[]>> {
        const options = createRequestOption(req);
        return this.http.get<any[]>(SERVER_API_URL + 'api/ps-salary-tax-insurance-regulations/get-all', {
            params: options,
            observe: 'response'
        });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    getPSSalaryTaxInsuranceRegulation(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IPSSalaryTaxInsuranceRegulation[]>(SERVER_API_URL + 'api/get-ps-salary-tax-insurance-regulation', {
            params: options,
            observe: 'response'
        });
    }

    getAllPSSalaryTaxInsuranceRegulations(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IPSSalaryTaxInsuranceRegulation[]>(this.resourceUrl + '/getAllPSSalaryTaxInsuranceRegulations', {
            params: options,
            observe: 'response'
        });
    }

    multiDelete(obj: any[]): Observable<any> {
        return this.http.post<any>(`${this.resourceUrl}/multi-delete-pSSalaryTaxInsuranceRegulation`, obj, { observe: 'response' });
    }

    getAllActivePSSalaryTaxInsuranceRegulations(): Observable<EntityArrayResponseType> {
        return this.http.get<IPSSalaryTaxInsuranceRegulation[]>(this.resourceUrl + '/is-active', { observe: 'response' });
    }

    findAllActive(): Observable<EntityArrayResponseType> {
        return this.http.get<IPSSalaryTaxInsuranceRegulation[]>(`${this.resourceUrl}/active`, { observe: 'response' });
    }

    findAllByCompanyID(): Observable<EntityArrayResponseType> {
        return this.http.get<IPSSalaryTaxInsuranceRegulation[]>(`${this.resourceUrl}/find-all-by-id`, { observe: 'response' });
    }

    findAllByIDIsNull(): Observable<EntityArrayResponseType> {
        return this.http.get<IPSSalaryTaxInsuranceRegulation[]>(`${this.resourceUrl}/find-all-by-id-null`, { observe: 'response' });
    }

    pageablePSSalaryTaxInsuranceRegulation(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IPSSalaryTaxInsuranceRegulation[]>(this.resourceUrl + '/pageable-all-pSSalaryTaxInsuranceRegulation', {
            params: options,
            observe: 'response'
        });
    }

    searchAll(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IPSSalaryTaxInsuranceRegulation[]>(`${this.resourceUrl}/search-all-pSSalaryTaxInsuranceRegulation`, {
            params: options,
            observe: 'response'
        });
    }

    deleteByListIDPSSalaryTaxInsuranceRegulation(rq: any[]): Observable<HttpResponse<HandlingResult>> {
        return this.http.post<any>(`${this.resourceUrl}/delete-list-pSSalaryTaxInsuranceRegulation`, rq, { observe: 'response' });
    }

    private convertDateFromClient(pSSalaryTaxInsuranceRegulation: IPSSalaryTaxInsuranceRegulation): IPSSalaryTaxInsuranceRegulation {
        const copy: IPSSalaryTaxInsuranceRegulation = Object.assign({}, pSSalaryTaxInsuranceRegulation, {
            fromDate:
                pSSalaryTaxInsuranceRegulation.fromDate != null && moment(pSSalaryTaxInsuranceRegulation.fromDate).isValid()
                    ? moment(pSSalaryTaxInsuranceRegulation.fromDate).format(DATE_FORMAT)
                    : null,
            toDate:
                pSSalaryTaxInsuranceRegulation.toDate != null && moment(pSSalaryTaxInsuranceRegulation.toDate).isValid()
                    ? moment(pSSalaryTaxInsuranceRegulation.toDate).format(DATE_FORMAT)
                    : null
        });
        return copy;
    }

    private convertDateFromServer(res: EntityResponseType): EntityResponseType {
        res.body.fromDate = res.body.fromDate != null ? moment(res.body.fromDate) : null;
        res.body.toDate = res.body.toDate != null ? moment(res.body.toDate) : null;
        return res;
    }

    private convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        res.body.forEach((pSSalaryTaxInsuranceRegulation: IPSSalaryTaxInsuranceRegulation) => {
            pSSalaryTaxInsuranceRegulation.fromDate =
                pSSalaryTaxInsuranceRegulation.fromDate != null ? moment(pSSalaryTaxInsuranceRegulation.fromDate) : null;
            pSSalaryTaxInsuranceRegulation.toDate =
                pSSalaryTaxInsuranceRegulation.toDate != null ? moment(pSSalaryTaxInsuranceRegulation.toDate) : null;
        });
        return res;
    }

    getDataByCompanyID(req?: any): Observable<HttpResponse<IPSSalaryTaxInsuranceRegulation>> {
        const options = createRequestOption(req);
        return this.http.get<IPSSalaryTaxInsuranceRegulation>(this.resourceUrl + '/get-data-by-company-id', {
            params: options,
            observe: 'response'
        });
    }
}
