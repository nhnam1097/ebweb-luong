import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import * as XLSX from 'xlsx';
import * as FileSaver from 'file-saver';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IGOtherVoucher } from 'app/shared/models/g-other-voucher.model';
import { IInsurancePayment } from 'app/shared/models/insurancePayment.model';
import { ISalaryPayment } from 'app/shared/models/salaryPayment.model';
import { IHistorySalaryPayment } from 'app/shared/models/historySalaryPayment.model';
import { IGOtherVoucherDetails } from 'app/shared/models/g-other-voucher-details.model';
import { ViewVoucherNo } from 'app/shared/models/view-voucher-no.model';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

type EntityResponseType = HttpResponse<IGOtherVoucher>;
type EntityResponseTypeAny = HttpResponse<any>;
type EntityArrayResponseType = HttpResponse<IGOtherVoucher[]>;
type EntityArrayResponseTypeInsurancePayment = HttpResponse<IInsurancePayment[]>;
type EntityArrayResponseTypeSalaryPayment = HttpResponse<ISalaryPayment[]>;
type EntityArrayResponseHistorySalaryPayment = HttpResponse<IHistorySalaryPayment[]>;

@Injectable({ providedIn: 'root' })
export class GOtherVoucherService {
    private resourceUrl = SERVER_API_URL + 'api/g-other-vouchers';

    constructor(private http: HttpClient) {}

    create(gOtherVoucher: IGOtherVoucher): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(gOtherVoucher);
        return this.http
            .post<IGOtherVoucher>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    update(gOtherVoucher: IGOtherVoucher): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(gOtherVoucher);
        return this.http
            .put<IGOtherVoucher>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    find(id: string): Observable<EntityResponseTypeAny> {
        return this.http
            .get<IGOtherVoucher>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseTypeAny) => this.convertDateFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IGOtherVoucher[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    delete(id: string): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    private convertDateFromClient(gOtherVoucher: IGOtherVoucher): IGOtherVoucher {
        const copy: IGOtherVoucher = Object.assign({}, gOtherVoucher, {
            date: gOtherVoucher.date != null && gOtherVoucher.date.isValid() ? gOtherVoucher.date.format(DATE_FORMAT) : null,
            postedDate:
                gOtherVoucher.postedDate != null && gOtherVoucher.postedDate.isValid() ? gOtherVoucher.postedDate.format(DATE_FORMAT) : null
        });
        return copy;
    }

    private convertDateFromClientInsurancePayment(date: any): IGOtherVoucher {
        return date != null && date.isValid() ? date.for(DATE_FORMAT) : null;
    }

    private convertDateFromServer(res: EntityResponseType): EntityResponseType {
        res.body.date = res.body.date != null ? moment(res.body.date) : null;
        res.body.postedDate = res.body.postedDate != null ? moment(res.body.postedDate) : null;
        return res;
    }

    private convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        res.body.forEach((gOtherVoucher: IGOtherVoucher) => {
            gOtherVoucher.date = gOtherVoucher.date != null ? moment(gOtherVoucher.date) : null;
            gOtherVoucher.postedDate = gOtherVoucher.postedDate != null ? moment(gOtherVoucher.postedDate) : null;
        });
        return res;
    }

    searchAll(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IGOtherVoucher[]>(`${this.resourceUrl}/search-all`, {
                params: options,
                observe: 'response'
            })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    public exportAsExcelFile(json: any[], excelFileName: string): void {
        const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
        const workbook: XLSX.WorkBook = { Sheets: { 'Báo có': worksheet }, SheetNames: ['Báo có'] };
        const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
        this.saveAsExcelFile(excelBuffer, excelFileName);
    }

    private saveAsExcelFile(buffer: any, fileName: string): void {
        const data: Blob = new Blob([buffer], { type: EXCEL_TYPE });
        FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    }

    getCustomerReport(req?: any): Observable<HttpResponse<any>> {
        const options = createRequestOption(req);
        let headers = new HttpHeaders();
        headers = headers.set('Accept', 'application/pdf');
        return this.http.get(SERVER_API_URL + 'api/report/pdf', {
            params: options,
            observe: 'response',
            headers,
            responseType: 'blob'
        });
    }

    mutipleDelete(listID: string[]): Observable<HttpResponse<string[]>> {
        return this.http.put<string[]>(`${this.resourceUrl}/mutiple-delete`, listID, { observe: 'response' });
    }

    findByRowNum(req?: any): Observable<EntityResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IGOtherVoucher>(`${this.resourceUrl}/findByRowNum`, { params: options, observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    getIndexRow(req?: any): Observable<EntityResponseType> {
        const options = createRequestOption(req);
        return this.http.get<any>(`${this.resourceUrl}/getIndexRow`, { params: options, observe: 'response' });
    }

    exportPDF(req?: any): Observable<any> {
        const options = createRequestOption(req);
        let headers = new HttpHeaders();
        headers = headers.set('Accept', 'application/x-excel');
        return this.http.get(this.resourceUrl + '/export/pdf', { params: options, observe: 'response', headers, responseType: 'blob' });
    }

    exportExcel(req?: any): Observable<any> {
        const options = createRequestOption(req);
        let headers = new HttpHeaders();
        headers = headers.set('Accept', 'application/x-excel');
        return this.http.get(this.resourceUrl + '/export/excel', { params: options, observe: 'response', headers, responseType: 'blob' });
    }

    multiDelete(obj: IGOtherVoucher[]): Observable<HttpResponse<any>> {
        const copy = this.convertDateFromClientArr(obj);
        return this.http
            .post<any>(`${this.resourceUrl}/multi-delete-g-other-vouchers`, copy, { observe: 'response' })
            .pipe(map((res: any) => this.convertDateArrayFromServerForMultiAction(res)));
    }

    multiUnRecord(obj: IGOtherVoucher[]): Observable<HttpResponse<any>> {
        const options = this.convertDateFromClientArr(obj);
        return this.http
            .post<any>(`${this.resourceUrl}/multi-un-record-g-other-vouchers`, options, { observe: 'response' })
            .pipe(map((res: any) => this.convertDateArrayFromServerForMultiAction(res)));
    }

    multiDeleteKCLL(obj: any[]): Observable<HttpResponse<any>> {
        return this.http.post<any>(`${this.resourceUrl}/multi-delete-g-other-vouchers`, obj, { observe: 'response' });
    }

    multiUnrecordKCLL(obj: any[]): Observable<HttpResponse<any>> {
        return this.http.post<any>(`${this.resourceUrl}/multi-un-record-g-other-vouchers`, obj, { observe: 'response' });
    }

    private convertDateFromClientArr(gOtherVouchers: IGOtherVoucher[]): IGOtherVoucher[] {
        const goArr = [];
        gOtherVouchers.forEach(n => {
            const copy: IGOtherVoucher = Object.assign({}, n, {
                date: n.date != null && n.date.isValid() ? n.date.format(DATE_FORMAT) : null,
                postedDate: n.postedDate != null && n.postedDate.isValid() ? n.postedDate.format(DATE_FORMAT) : null
            });
            goArr.push(copy);
        });
        return goArr;
    }

    private convertDateArrayFromServerForMultiAction(res: any): EntityArrayResponseType {
        res.body.listFail.forEach((xuLyChungTuModel: ViewVoucherNo) => {
            xuLyChungTuModel.date = xuLyChungTuModel.date != null ? moment(xuLyChungTuModel.date) : null;
            xuLyChungTuModel.postedDate = xuLyChungTuModel.postedDate != null ? moment(xuLyChungTuModel.postedDate) : null;
        });
        return res;
    }

    getDetailsHTCPL(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IGOtherVoucherDetails[]>(this.resourceUrl + '/get-details-by-ps-salary-sheet-id', {
            params: options,
            observe: 'response'
        });
    }

    getInsurancePayments(req?: any): Observable<EntityArrayResponseTypeInsurancePayment> {
        const options = createRequestOption(req);
        return this.http.get<IInsurancePayment[]>(this.resourceUrl + '/get-insurance-payments', {
            params: options,
            observe: 'response'
        });
    }

    getSalaryPayments(req?: any): Observable<EntityArrayResponseTypeSalaryPayment> {
        const options = createRequestOption(req);
        return this.http.get<IHistorySalaryPayment[]>(this.resourceUrl + '/get-salary-payments', {
            params: options,
            observe: 'response'
        });
    }

    getHistorySalaries(req?: any): Observable<EntityArrayResponseTypeSalaryPayment> {
        const options = createRequestOption(req);
        return this.http.get<IHistorySalaryPayment[]>(this.resourceUrl + '/get-history-salary-payments', {
            params: options,
            observe: 'response'
        });
    }

    createInsurancePayment(req?: any): Observable<any> {
        return this.http.post<any>(this.resourceUrl + '/create-insurance-payments', req, {
            observe: 'response'
        });
    }

    createSalaryPayment(req?: any): Observable<any> {
        return this.http.post<any>(this.resourceUrl + '/create-salary-payments', req, {
            observe: 'response'
        });
    }

    findAllHTCPL(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IGOtherVoucher[]>(`${this.resourceUrl}/find-all-htcpl`, {
                params: options,
                observe: 'response'
            })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    findAllPaymentSalaries(req?: any): Observable<EntityArrayResponseTypeSalaryPayment> {
        const options = createRequestOption(req);
        return this.http.get<ISalaryPayment[]>(this.resourceUrl + '/get-list-salary-payments', {
            params: options,
            observe: 'response'
        });
    }

    private convertRequestFromClient(request: any): any {
        // console.log("date format 1: ",request);
        const copy: any = Object.assign({}, request, {
            fromDate: request.fromDate != null ? request.fromDate : null,
            toDate: request.toDate != null ? request.toDate : null
        });
        // console.log("date format 2: ",copy);
        return copy;
    }

    exportDetail(req?: any): Observable<any> {
        const options = createRequestOption(req);
        let headers = new HttpHeaders();
        headers = headers.set('Accept', 'application/x-excel');
        return this.http.get(this.resourceUrl + '/export-detail', { params: options, observe: 'response', headers, responseType: 'blob' });
    }

    getIndexRowGOVTherByTypeId(req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.get<any>(`${this.resourceUrl}/getIndexRow-GOVTher`, { params: options, observe: 'response' });
    }
}
