import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import {IPSTimeSheetDetails} from 'app/shared/models/ps-time-sheet-details.model';
import {IPSTimeSheet} from 'app/shared/models/ps-time-sheet.model';

type EntityResponseType = HttpResponse<IPSTimeSheetDetails>;
type EntityArrayResponseType = HttpResponse<IPSTimeSheetDetails[]>;

@Injectable({ providedIn: 'root' })
export class PSTimeSheetDetailsService {
    private resourceUrl = SERVER_API_URL + 'api/p-s-time-sheet-details';

    constructor(private http: HttpClient) {}

    create(pSTimeSheetDetails: IPSTimeSheetDetails): Observable<EntityResponseType> {
        return this.http.post<IPSTimeSheetDetails>(this.resourceUrl, pSTimeSheetDetails, { observe: 'response' });
    }

    update(pSTimeSheetDetails: IPSTimeSheetDetails): Observable<EntityResponseType> {
        return this.http.put<IPSTimeSheetDetails>(this.resourceUrl, pSTimeSheetDetails, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IPSTimeSheetDetails>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IPSTimeSheetDetails[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: string): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    getNewPSTimeSheets(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IPSTimeSheet[]>(this.resourceUrl + '/get-new-ps-time-sheet', {
            params: options,
            observe: 'response'
        });
    }

    getNewPSTimeSheetsByExistPSTS(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IPSTimeSheet[]>(this.resourceUrl + '/get-new-ps-time-sheet-by-exist-psts', {
            params: options,
            observe: 'response'
        });
    }

    exportExcel(req: any): Observable<any> {
        let headers = new HttpHeaders();
        headers = headers.set('Accept', 'application/x-excel');
        return this.http.post(this.resourceUrl + '/export-excel', req, {
            observe: 'response',
            headers,
            responseType: 'blob'
        });
    }
}
