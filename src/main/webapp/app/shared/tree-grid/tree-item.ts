export class TreeAccountListItem {
    parent: any;
    index?: number;
    select?: boolean;
    check?: boolean;
    children?: any[];
    isHidden?: boolean;
    grade?: number;
}
