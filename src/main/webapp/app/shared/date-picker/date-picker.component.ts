import { Component, ElementRef, EventEmitter, forwardRef, Input, OnInit, Output, ViewChild } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import * as moment from 'moment';
import {DATE_FORMAT_SLASH, DATE_TIME_FORMAT, DATE_TIME_SECOND_FORMAT} from 'app/shared';

const noop = () => {};

export const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any = {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => DatePickerComponent),
    multi: true
};

@Component({
    selector: 'eb-date-picker',
    templateUrl: './date-picker.component.html',
    styleUrls: ['./date-picker.component.css'],
    providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR]
})
export class DatePickerComponent implements OnInit, ControlValueAccessor {
    private innerValue: any = '';
    @Input() mask: string;
    @Input() format: string;
    @Input() disabled: boolean;
    @Input() isInTable: boolean;
    @Input() isMultiTabs: boolean;
    @Input() minDate: any;
    @Input() maxDate: any;
    @Input() isRequired: boolean;
    @Input() isTime: boolean;
    @Input() isSecond: boolean;
    @Input() tabindex: boolean;
    @Input() backgroundInherit: boolean;
    @Input() idIp: string;
    @Input() nameIp: string;
    @Input() isTax: boolean;
    @Input() showToptoggleDropDown: boolean;
    @Input() isHiddenCalendar: boolean;
    @Input() timeValue: any;
    @Input() haveTime: any;
    @Input() select: any;
    @Input() dateSearch: any;
    @Output() focusInput = new EventEmitter<any>();
    @Output() checkSame = new EventEmitter<any>();
    @Input() checkRefDateTime: boolean;
    @Input() noSetDisabled: boolean;
    @Input() autocomplete: string;
    @Input() fullLenghtOfDate: boolean;
    @Input() isInTableRequired: boolean;
    date: string;
    dateHolder: any;
    showTime: boolean;
    time: any;
    timeValueCopy: any;
    dem: any = 0;

    private onTouchedCallback: () => void = noop;
    private onChangeCallback: (_: any) => void = noop;
    overFlow: boolean;
    isOverBottomPage: boolean;
    bottom100: boolean;
    isOutTable: boolean;
    zIndexForDropdown: boolean;
    top100: boolean;
    checkIn: boolean;
    @ViewChild('nameOfInputCombobox') nameField: ElementRef; // sử dụng kick chuột trong vùng div mà không có dữ liệu

    // get accessor
    get value(): any {
        return this.innerValue;
    }

    // set accessor including call the onchange callback
    set value(v: any) {
        if (!v) {
            this.innerValue = null;
        }
        let date1;
        let time1;
        if (this.haveTime) {
            date1 = moment(this.dateSearch, 'DD/MM/YYYY').format(DATE_FORMAT_SLASH);
            time1 = moment(v, 'hhmmss');
            this.time.hour = time1.hour();
            this.time.minute = time1.minute();
            this.time.second = time1.second();
            this.innerValue = moment(`${date1} ${this.time.hour}:${this.time.minute}:${this.time.second}`, 'DD/MM/YYYY HH:mm:ss').format(
                this.format
            );
        }
        let value = v;
        let oldInnerValue = this.innerValue;
        if (this.format) {
            if (this.haveTime) {
                value = moment(`${date1} ${this.time.hour}:${this.time.minute}:${this.time.second}`, 'DD/MM/YYYY HH:mm:ss').format(
                    this.format
                );
            } else {
                value = moment(v, 'DD/MM/YYYY').format(this.format);
            }
            if (oldInnerValue) {
                if (this.haveTime) {
                    // oldInnerValue = moment(this.innerValue, 'HH:mm:ss').format(this.format);
                } else {
                    oldInnerValue = moment(this.innerValue, 'DD/MM/YYYY').format(this.format);
                }
            }
        } else {
            if (this.isTime) {
                if (this.isSecond) {
                    value = moment(`${v} ${this.time.hour}:${this.time.minute}:${this.time.second}`, 'DD/MM/YYYY HH:mm:ss').format(
                        DATE_TIME_SECOND_FORMAT
                    );
                    if (oldInnerValue) {
                        oldInnerValue = moment(
                            `${this.innerValue} ${this.time.hour}:${this.time.minute}:${this.time.second}`,
                            'DD/MM/YYYY HH:mm:ss'
                        ).format(DATE_TIME_SECOND_FORMAT);
                    }
                } else {
                    value = moment(`${v} ${this.time.hour}:${this.time.minute}`, 'DD/MM/YYYY HH:mm').format(DATE_TIME_FORMAT);
                    if (oldInnerValue) {
                        oldInnerValue = moment(`${this.innerValue} ${this.time.hour}:${this.time.minute}`, 'DD/MM/YYYY HH:mm').format(
                            DATE_TIME_FORMAT
                        );
                    }
                }
            } else {
                value = moment(v, 'DD/MM/YYYY').format(DATE_FORMAT_SLASH);
                if (oldInnerValue) {
                    oldInnerValue = moment(this.innerValue, 'DD/MM/YYYY').format(DATE_FORMAT_SLASH);
                }
            }
        }
        if (value !== oldInnerValue) {
            if (v && (v.length === 10 || v.length === 8)) {
                try {
                    if (this.format) {
                        if (this.haveTime) {
                            this.innerValue = moment(v, 'HH:mm:ss').format(this.format);
                        } else {
                            this.innerValue = moment(v, 'DD/MM/YYYY').format(this.format);
                        }
                    } else {
                        if (this.isTime) {
                            if (this.isSecond) {
                                this.innerValue = moment(
                                    `${v} ${this.time.hour}:${this.time.minute}:${this.time.second}`,
                                    'DD/MM/YYYY HH:mm:ss'
                                );
                            } else {
                                this.innerValue = moment(`${v} ${this.time.hour}:${this.time.minute}`, 'DD/MM/YYYY HH:mm');
                            }
                        } else {
                            this.innerValue = moment(v, 'DD/MM/YYYY');
                        }
                    }
                } catch (e) {
                    this.innerValue = null;
                }
            } else {
                this.innerValue = null;
                this.dateHolder = null;
            }

            this.onChangeCallback(this.innerValue);
        }
        if (this.haveTime) {
            this.date = moment(this.innerValue, this.format).format('HH:mm:ss');
            this.onChangeCallback(this.innerValue);
        }
    }

    ngOnInit(): void {
        if (this.haveTime) {
            this.mask = this.mask ? this.mask : '00:00:00';
        } else {
            this.mask = this.mask ? this.mask : '00/00/0000';
        }
        if (this.haveTime) {
            if (this.isTime && this.timeValue) {
                this.timeValueCopy = this.timeValue.slice(0);
                const date = moment(this.timeValue, DATE_TIME_SECOND_FORMAT);
                if (!this.isSecond) {
                    this.time = { hour: date.hour(), minute: date.minute() };
                } else {
                    this.time = { hour: date.hour(), minute: date.minute(), second: date.second() };
                }
            }
        } else {
            if (this.isTime) {
                const date = moment();
                if (!this.isSecond) {
                    this.time = { hour: date.hour(), minute: date.minute() };
                } else {
                    this.time = { hour: date.hour(), minute: date.minute(), second: date.second() };
                }
            }
        }
    }

    registerOnChange(fn: any): void {
        this.onChangeCallback = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouchedCallback = fn;
    }

    setDisabledState(isDisabled: boolean): void {
        this.disabled = isDisabled;
    }

    writeValue(value: any): void {
        if (this.isMultiTabs && this.date === '' && value !== '') {
            if (this.isTime || this.isSecond) {
                this.time = { hour: moment(value).hour(), minute: moment(value).minute(), second: moment(value).second() };
            }
            this.date = value;
            return;
        }
        this.dateHolder = null;
        if (value !== this.innerValue) {
            this.innerValue = value;
            if (this.innerValue instanceof moment) {
                if (this.isTime || this.isSecond) {
                    this.time = { hour: this.innerValue.hour(), minute: this.innerValue.minute(), second: this.innerValue.second() };
                }
                this.date = value.format('DD/MM/YYYY');
                if (this.format) {
                    this.innerValue = value.format(this.format);
                }
            } else {
                if (this.format) {
                    const date = moment(value, this.format);
                    if (date.isValid()) {
                        if (this.haveTime) {
                            this.innerValue = this.date = moment(value, this.format).format('HH:mm:ss');
                            this.time = { hour: moment(value).hour(), minute: moment(value).minute(), second: moment(value).second() };
                        } else {
                            this.innerValue = this.date = moment(value, this.format).format('DD/MM/YYYY');
                        }
                    } else if (!this.innerValue) {
                        this.date = '';
                    }
                } else {
                    if (this.isTime || this.isSecond) {
                        this.time = { hour: moment(value).hour(), minute: moment(value).minute(), second: moment(value).second() };
                    }
                    this.date = this.innerValue = value;
                }
            }
        } else {
            if (this.isTime || this.isSecond) {
                this.time = { hour: moment(value).hour(), minute: moment(value).minute(), second: moment(value).second() };
            }
            this.dateHolder = value;
        }
    }

    update() {
        // this.innerValue = this.dateHolder.format('DD/MM/YYYY');
        this.time = { hour: moment().hour(), minute: moment().minute(), second: moment().second() };
        this.date = this.dateHolder.format('DD/MM/YYYY');
        this.value = this.date;
    }

    parseDate(event?: any) {
        if (this.isTime && !this.time) {
            this.time = { hour: 0, minute: 0, second: 0 };
        }
        let beforeParse;
        if (this.haveTime) {
            this.date = this.formatTime();
            beforeParse = this.date.replace(new RegExp(':', 'g'), '');
        } else {
            beforeParse = this.date.replace(new RegExp('/', 'g'), '');
        }
        if (beforeParse.length === 8) {
            try {
                const data = moment(beforeParse, 'DDMMYYYY').format('DD/MM/YYYY');
                if (data !== 'Invalid date') {
                    this.date = data;
                }
            } catch (e) {
                this.date = '';
            }
        } else if (beforeParse.length === 6 && !this.fullLenghtOfDate) {
            try {
                const data = moment(beforeParse, 'hhmmss').format('HH:mm:ss');
                if (data !== 'Invalid date') {
                    this.date = data;
                }
            } catch (e) {
                const data = moment(this.timeValueCopy, this.format).format('HH:mm:ss');
                if (data !== 'Invalid date') {
                    this.date = data;
                } else {
                    this.date = '';
                }
            }
        }
        if (this.isMultiTabs && this.date === '' && this.value !== '') {
            this.date = this.value;
        }
        this.value = this.date;
        if (this.haveTime) {
            this.checkSame.emit(event);
        }
    }

    checkDate(event?: any) {
        if (this.haveTime) {
            if (this.date) {
                this.date = moment(this.date, 'hhmmss').format('HH:mm:ss');
            }
            if (!this.date || this.date.length !== 8) {
                const data = moment(this.timeValueCopy, this.format).format('HH:mm:ss');
                if (data !== 'Invalid date') {
                    this.date = data;
                } else {
                    this.date = '';
                }
            } else {
                const beforeParse = this.date.replace(new RegExp(':', 'g'), '');
                const data = moment(beforeParse, 'hhmmss').format('HH:mm:ss');
                const date = moment(beforeParse, 'hhmmss');
                if (data === 'Invalid date' || !date.isValid()) {
                    const data1 = moment(this.timeValueCopy, this.format).format('HH:mm:ss');
                    if (data1 !== 'Invalid date') {
                        this.date = data1;
                    } else {
                        this.date = '';
                    }
                } else {
                    this.value = this.date;
                }
            }
            this.checkSame.emit(event);
        } else {
            if (!this.date || this.date.length !== 10) {
                this.date = null;
            } else {
                const beforeParse = this.date.replace(new RegExp('/', 'g'), '');
                const data = moment(beforeParse, 'DDMMYYYY').format('DD/MM/YYYY');
                const date = moment(beforeParse, 'DDMMYYYY');
                if (data === 'Invalid date' || !date.isValid() || date.year() < 1920) {
                    this.date = null;
                }
            }
        }
    }

    toggleDropDown(event): void {
        this.showTime = !this.showTime;
        // prevent overflow page
        if (event.clientX > 1200) {
            this.overFlow = true;
        }
        if (event.clientY > 260) {
            this.isOverBottomPage = true;
        }
        this.setLocationDropDown(this.nameField, event);
    }

    setLocationDropDown(nameField, event) {
        let nF = nameField.nativeElement.parentElement;
        let checkInsideTable;
        for (let i = 0; i < 10; i++) {
            nF = nF.parentElement;
            if (nF.nodeName.includes('TABLE')) {
                checkInsideTable = true;
                break;
            }
        }
        if (checkInsideTable) {
            // const pTb = nF.getBoundingClientRect();
            const pTb_Div = nF.parentElement.getBoundingClientRect();
            const long = pTb_Div.y + pTb_Div.height - event.clientY;
            if (this.haveTime) {
                if (long < 128) {
                    this.bottom100 = true;
                    this.top100 = false;
                } else {
                    this.bottom100 = false;
                    this.top100 = true;
                }
            } else {
                if (long < 180) {
                    this.bottom100 = true;
                    this.isOutTable = true;
                } else {
                    this.bottom100 = false;
                    this.zIndexForDropdown = this.isOutTable;
                }
            }
        }
    }

    focus(event) {
        this.focusInput.emit(event);
    }

    formatTime(): string {
        let result;
        result = this.time.hour < 10 ? '0' + this.time.hour + ':' : this.time.hour + ':';
        result += this.time.minute < 10 ? '0' + this.time.minute + ':' : this.time.minute + ':';
        result += this.time.second < 10 ? '0' + this.time.second : this.time.second;
        return result;
    }

    checkMouseLeave() {
        this.checkIn = false;
    }

    checkMouseOver() {
        this.checkIn = true;
    }

    checkFocusout() {
        if (!this.checkIn) {
            this.showTime = false;
        }
    }
}
