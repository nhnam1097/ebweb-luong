import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { NgbActiveModal, NgbDateAdapter } from '@ng-bootstrap/ng-bootstrap';

import { NgbDateMomentAdapter } from './util/datepicker-adapter';
import { TestluongSharedLibsModule, TestluongSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';
import { HasSomeAuthorityDirective } from 'app/shared/auth/has-some-authority.directive';
import { HasSomeAuthorityAndConditionDirective } from 'app/shared/auth/has-some-authority-and-condition.directive';
import { HasSomeAuthorityOrConditionDirective } from 'app/shared/auth/has-some-authority-or-condition.directive';
import { TreeViewComponent, TreeViewItemComponent } from 'app/shared/tree-combo-box/tree-combo-box.component';
import { DatePickerComponent } from 'app/shared/date-picker/date-picker.component';
import { NgxMaskModule } from 'ngx-mask';
import { CheckService } from 'app/shared/tree-combo-box/check-service';
import { CheckPasswordModalComponent } from 'app/shared/modals/check-password/check-password.modal.component';
import { ComboBoxComponent } from 'app/shared/combobox/combo-box.component';
import { CurrencyMaskDirective } from 'app/shared/directive/currency-input/currency.directive';
import { EbCurrencyPipe } from 'app/shared/directive/currency-input/currency.pipe';
import { UppercaseInputDirective } from 'app/shared/directive/uppercase-input/uppercase-input.directive';
import { CurrencyMaskModule } from 'app/shared/directive/ng2-currency-mask/currency-mask.module';
import { ToastrModule } from 'ngx-toastr';
import { ComboBoxPipe } from 'app/shared/combobox/combo-box.pipe';
import { EbRefModalComponent } from 'app/shared/ref/ref.component';
import { EbReportPdfPopupComponent } from 'app/shared/show-pdf/eb-report-pdf-popup.component';
import { HandlingResultComponent } from 'app/shared/handling-result/handling-result.component';
import { InChungTuHangLoatComponent } from 'app/shared/in-chung-tu-hang-loat/in-chung-tu-hang-loat.component';
import { CodePrintComponent } from 'app/shared/code-print/code-print.component';
import { ConfirmLeaveComponent } from 'app/shared/can-deactive-guard/confirm-leave.component';
import { NopTienBaoHiemComponent } from 'app/luong/nop_bao_hiem/nop-tien-bao-hiem.component';
import { ThanhToanLuongComponent } from 'app/luong/thanh_toan_luong/thanh-toan-luong.component';
import { CanDeactiveGuardService } from 'app/shared/can-deactive-guard/can-deactive-guard.service';
import { EbContextMenuComponent } from 'app/shared/context-menu';
import { EbContextMenuModule } from 'app/shared/context-menu/contex-menu.module';
import { TreeGridComponent, TreeGridItemComponent } from 'app/shared/tree-grid/tree-grid.component';
import { ThongTinDichVuComponent } from 'app/shared/thong-tin-dich-vu';

@NgModule({
    imports: [
        TestluongSharedLibsModule,
        TestluongSharedCommonModule,
        NgxMaskModule.forRoot(),
        CurrencyMaskModule,
        ToastrModule.forRoot(),
        EbContextMenuModule
    ],
    declarations: [
        JhiLoginModalComponent,
        HasAnyAuthorityDirective,
        HasAnyAuthorityDirective,
        HasSomeAuthorityDirective,
        HasSomeAuthorityAndConditionDirective,
        HasSomeAuthorityOrConditionDirective,
        DatePickerComponent,
        TreeViewComponent,
        TreeViewItemComponent,
        CheckPasswordModalComponent,
        ComboBoxComponent,
        CurrencyMaskDirective,
        EbCurrencyPipe,
        UppercaseInputDirective,
        ComboBoxPipe,
        EbRefModalComponent,
        EbReportPdfPopupComponent,
        HandlingResultComponent,
        InChungTuHangLoatComponent,
        CodePrintComponent,
        ConfirmLeaveComponent,
        NopTienBaoHiemComponent,
        ThanhToanLuongComponent,
        TreeGridComponent,
        TreeGridItemComponent,
        ThongTinDichVuComponent
    ],
    providers: [{ provide: NgbDateAdapter, useClass: NgbDateMomentAdapter }, CheckService, NgbActiveModal, CanDeactiveGuardService],
    entryComponents: [
        JhiLoginModalComponent,
        CheckPasswordModalComponent,
        ComboBoxComponent,
        EbRefModalComponent,
        EbReportPdfPopupComponent,
        HandlingResultComponent,
        InChungTuHangLoatComponent,
        CodePrintComponent,
        ConfirmLeaveComponent,
        NopTienBaoHiemComponent,
        ThanhToanLuongComponent,
        TreeGridComponent,
        TreeGridItemComponent,
        ThongTinDichVuComponent
    ],
    exports: [
        TestluongSharedCommonModule,
        JhiLoginModalComponent,
        HasAnyAuthorityDirective,
        HasAnyAuthorityDirective,
        HasSomeAuthorityDirective,
        HasSomeAuthorityAndConditionDirective,
        HasSomeAuthorityOrConditionDirective,
        DatePickerComponent,
        TreeViewComponent,
        TreeViewItemComponent,
        CheckPasswordModalComponent,
        ComboBoxComponent,
        CurrencyMaskDirective,
        EbCurrencyPipe,
        UppercaseInputDirective,
        ComboBoxPipe,
        EbRefModalComponent,
        EbReportPdfPopupComponent,
        HandlingResultComponent,
        InChungTuHangLoatComponent,
        CodePrintComponent,
        ConfirmLeaveComponent,
        NopTienBaoHiemComponent,
        ThanhToanLuongComponent,
        EbContextMenuModule,
        TreeGridComponent,
        TreeGridItemComponent,
        ThongTinDichVuComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TestluongSharedModule {}
