import { Injectable } from '@angular/core';
import { SERVER_API_URL } from 'app/app.constants';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { createRequestOption, DATE_FORMAT } from 'app/shared';
import { catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';
import * as moment from 'moment';
import { ToastrService } from 'ngx-toastr';
import {RequestReport} from 'app/shared/models/reqest-report.model';
import {ViewVoucherNo} from 'app/shared/models/view-voucher-no.model';

@Injectable({ providedIn: 'root' })
export class InChungTuHangLoatService {
    private resourceUrl = SERVER_API_URL + '/report-tool/multi-print-template';
    private sessionData: { type: string; data: any }[] = [];
    constructor(
        private http: HttpClient,
        private localStorage: LocalStorageService,
        private sessionStorage: SessionStorageService,
        private toastrService: ToastrService
    ) {}

    public reportPDFInChungTuHangLoat(requestReport: RequestReport) {
        const token = this.localStorage.retrieve('authenticationToken') || this.sessionStorage.retrieve('authenticationToken');
        requestReport.token = 'Bearer ' + token;
        const copy = this.convertDateFromClient(requestReport);
        let headers = new HttpHeaders();
        headers = headers.set('Accept', 'application/pdf');
        return this.http
            .post(this.resourceUrl, copy, {
                observe: 'response',
                headers,
                responseType: 'blob'
            })
            .pipe(catchError(this.parseErrorBlob));
    }

    private parseErrorBlob(err: HttpErrorResponse): Observable<any> {
        const reader: FileReader = new FileReader();

        const obs = Observable.create((observer: any) => {
            reader.onloadend = () => {
                observer.error(JSON.parse(reader.result));
                observer.complete();
            };
        });
        reader.readAsText(err.error);
        return obs;
    }

    private convertDateFromClient(requestReport: RequestReport): ViewVoucherNo {
        const copy: ViewVoucherNo = Object.assign({}, requestReport, {
            fromDate: requestReport.fromDate != null ? requestReport.fromDate.format(DATE_FORMAT) : null,
            toDate: requestReport.toDate != null ? requestReport.toDate.format(DATE_FORMAT) : null
        });
        return copy;
    }

    inChungTuHangLoat(requestReport: any, noBlankPage?) {
        return new Promise(resolve => {
            this.reportPDFInChungTuHangLoat(requestReport).subscribe(
                response => {
                    // this.showReport(response)
                    const contentDispositionHeader = response.headers.get('Content-Disposition');
                    let result2 = '';
                    if (contentDispositionHeader) {
                        const lengthRS2 = contentDispositionHeader.indexOf('=') + 2;
                        result2 = contentDispositionHeader.substring(lengthRS2, lengthRS2 + 5);
                    }
                    if (result2 === 'error') {
                        const indexEquals1 = contentDispositionHeader.indexOf('=');
                        const indexEquals2 = contentDispositionHeader.indexOf('=', indexEquals1 + 1);
                        const result3 = contentDispositionHeader.substring(indexEquals2 + 2, contentDispositionHeader.length - 1);

                        noBlankPage = result3;
                        resolve(result3);
                    } else {
                        const file = new Blob([response.body], { type: 'application/pdf' });
                        const fileURL = window.URL.createObjectURL(file);
                        const isDownload = false; // mặc định open new tab
                        if (isDownload) {
                            const link = document.createElement('a');
                            document.body.appendChild(link);
                            link.download = fileURL;
                            link.setAttribute('style', 'display: none');
                            const name = 'ten_bao_cao.pdf';
                            link.setAttribute('download', name);
                            link.href = fileURL;
                            link.click();
                        } else {
                            const contentDispositionHeader2 = response.headers.get('Content-Disposition');
                            let result = '';
                            if (contentDispositionHeader2) {
                                result = contentDispositionHeader2
                                    .split(';')[1]
                                    .trim()
                                    .split('=')[1];
                            }
                            if (!noBlankPage) {
                                const newWin = window.open(fileURL, '_blank');
                                // add a load listener to the window so that the title gets changed on page load
                                newWin.addEventListener('load', () => {
                                    newWin.document.title = result.replace(/'/g, '');
                                });
                            } else {
                                const newWin = window.open(fileURL, '_self');
                                // add a load listener to the window so that the title gets changed on page load
                                newWin.addEventListener('load', () => {
                                    newWin.document.title = result.replace(/'/g, '');
                                });
                            }
                        }
                    }
                },
                response => {}
            );
        });
        // this.reportPDFInChungTuHangLoat(requestReport).subscribe(response => {
        //     // this.showReport(response);
        //     const file = new Blob([response.body], { type: 'application/pdf' });
        //     const fileURL = window.URL.createObjectURL(file);
        //     const isDownload = false; // mặc định open new tab
        //     if (isDownload) {
        //         const link = document.createElement('a');
        //         document.body.appendChild(link);
        //         link.download = fileURL;
        //         link.setAttribute('style', 'display: none');
        //         const name = 'ten_bao_cao.pdf';
        //         link.setAttribute('download', name);
        //         link.href = fileURL;
        //         link.click();
        //     } else {
        //         const contentDispositionHeader = response.headers.get('Content-Disposition');
        //         const result = contentDispositionHeader
        //             .split(';')[1]
        //             .trim()
        //             .split('=')[1];
        //         const newWin = window.open(fileURL, '_blank');
        //         // add a load listener to the window so that the title gets changed on page load
        //         newWin.addEventListener('load', () => {
        //             newWin.document.title = result.replace(/'/g, '');
        //         });
        //     }
        // });
    }

    inChungTuMauDon(requestReport: any, message?: string, title?: string, noBlankPage?) {
        return new Promise(resolve => {
            this.reportPDFInChungTuHangLoat(requestReport).subscribe(
                response => {
                    this.toastrService.success(message, title);
                    // this.showReport(response)
                    const contentDispositionHeader = response.headers.get('Content-Disposition');
                    let result2 = '';
                    if (contentDispositionHeader) {
                        const lengthRS2 = contentDispositionHeader.indexOf('=') + 2;
                        result2 = contentDispositionHeader.substring(lengthRS2, lengthRS2 + 5);
                    }
                    if (result2 === 'error') {
                        const indexEquals1 = contentDispositionHeader.indexOf('=');
                        const indexEquals2 = contentDispositionHeader.indexOf('=', indexEquals1 + 1);
                        const result3 = contentDispositionHeader.substring(indexEquals2 + 2, contentDispositionHeader.length - 1);

                        noBlankPage = result3;
                        resolve(result3);
                    } else {
                        const file = new Blob([response.body], { type: 'application/pdf' });
                        const fileURL = window.URL.createObjectURL(file);
                        const isDownload = false; // mặc định open new tab
                        if (isDownload) {
                            const link = document.createElement('a');
                            document.body.appendChild(link);
                            link.download = fileURL;
                            link.setAttribute('style', 'display: none');
                            const name = 'ten_bao_cao.pdf';
                            link.setAttribute('download', name);
                            link.href = fileURL;
                            link.click();
                        } else {
                            const contentDispositionHeader2 = response.headers.get('Content-Disposition');
                            const result = contentDispositionHeader2
                                .split(';')[1]
                                .trim()
                                .split('=')[1];
                            if (!noBlankPage) {
                                const newWin = window.open(fileURL, '_blank');
                                // add a load listener to the window so that the title gets changed on page load
                                newWin.addEventListener('load', () => {
                                    newWin.document.title = result.replace(/'/g, '');
                                });
                            } else {
                                const newWin = window.open(fileURL, '_self');
                                // add a load listener to the window so that the title gets changed on page load
                                newWin.addEventListener('load', () => {
                                    newWin.document.title = result.replace(/'/g, '');
                                });
                            }
                        }
                    }
                },
                response => {}
            );
        });
    }

    exportPdfToolInBaoNo(typeReport: number, typeGroupID: number, id: string, message?: string, title?: string) {
        const requestReport = {
            typeGroupID,
            typeReport,
            listID: id ? [id] : [],
            token: ''
        };

        return this.inChungTuMauDon(requestReport, message, title);
    }

    // return false không hiện, true hiện (*ngIf)
    hidePrintButtonTypeID(selectRow: any, selectRows: any[], typeIDs: number[], prop: string, notExistTypeId?: boolean): boolean {
        if (!typeIDs || typeIDs.length < 1) {
            return true;
        }
        if (selectRows.length < 1) {
            if (selectRow) {
                return typeIDs.includes(selectRow[prop]);
            }
            return false;
        } else {
            return !(notExistTypeId
                ? selectRows.some(item => typeIDs.includes(item[prop]))
                : selectRows.some(item => !typeIDs.includes(item[prop])));
        }
    }

    exportPdfToolInSelectedRows(typeReport: number, typeGroupID: number, ids: string[], message?: string, title?: string) {
        const requestReport = {
            typeGroupID,
            typeReport,
            listID: ids,
            token: '',
            printSelectedRows: true
        };

        return this.inChungTuMauDon(requestReport, message, title);
    }

    selectIdAndSort(
        arrayTemp: any[],
        selectRows: any[],
        typeIDs: number[],
        id: string,
        propId: string,
        propTypeId: string,
        notExistTypeId?: boolean
    ): string[] {
        let temp;
        const listSort = [];
        arrayTemp.forEach(item => {
            temp = selectRows.find(x => x[id] === item[id]);
            if (temp) {
                listSort.push(temp);
            }
        });
        if (typeIDs && propTypeId && typeIDs.length > 0) {
            if (notExistTypeId) {
                return listSort.filter(item => !typeIDs.includes(item[propTypeId])).map(item => item[propId]);
            } else {
                return listSort.filter(item => typeIDs.includes(item[propTypeId])).map(item => item[propId]);
            }
        }

        return listSort.map(item => item[propId]);
    }
}
