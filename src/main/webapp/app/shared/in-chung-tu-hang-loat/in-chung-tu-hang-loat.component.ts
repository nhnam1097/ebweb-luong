import {Component, ElementRef, OnInit, Renderer} from '@angular/core';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {Router} from '@angular/router';
import {JhiEventManager, JhiParseLinks} from 'ng-jhipster';

import {LoginService} from 'app/core/login/login.service';
import {StateStorageService} from 'app/core/auth/state-storage.service';
import * as moment from 'moment';
import {DATE_FORMAT, ITEMS_PER_PAGE} from 'app/shared';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {Principal} from 'app/core';
import {ROLE} from 'app/role.constants';
import {TYPEGROUP, TypeMultiPrintTemplate, UpdateDataMessages} from 'app/app.constants';
import {InChungTuHangLoatService} from 'app/shared/in-chung-tu-hang-loat/in-chung-tu-hang-loat.service';
import {RefModalService} from 'app/shared/services/ref-modal.service';
import {UtilsService} from 'app/shared/UtilsService/Utils.service';
import {TypeGroupService} from 'app/shared/ref/type-group.service';
import {ViewVoucherService} from 'app/shared/ref/view-voucher.service';
import {IType} from 'app/shared/models/type.model';
import {CodePrintComponent} from 'app/shared/code-print/code-print.component';
import {TypeService} from 'app/shared/services/type.service';

@Component({
    selector: 'eb-in-chung-tu-hang-loat',
    templateUrl: './in-chung-tu-hang-loat.component.html',
    styleUrls: ['./in-chung-tu-hang-loat.component.css']
})
export class InChungTuHangLoatComponent implements OnInit {
    data: any[];
    recorded: any;
    status: any;
    fromDate;
    any;
    toDate: any;
    typeGroups: any;
    typeGroup: any;
    typeSearch: number;
    viewVouchers: any[];
    no: any;
    invoiceNo: any;
    totalItems: number;
    page: number;
    itemsPerPage = ITEMS_PER_PAGE;
    links: any;
    queryCount: any;
    newList: any[];
    timeLineVoucher: any;
    listTimeLine: any;
    objTimeLine: { dtBeginDate?: string; dtEndDate?: string };
    account: any;
    typePrintForm: number;
    listTypePrintForm: any[];
    listType: any[];
    valueVoucher: any;
    TYPE_GROUP_PHIEU_THU = 10;
    listOnSelect: any[];
    paramCheckAll: boolean;
    types: IType[];
    strCodePrint: any;
    modalRef: NgbModalRef;
    ROLE = ROLE;
    ROLE_InChungTuHangLoat_XEM = ROLE.InChungTuHangLoat_Xem;
    ROLE_InChungTuHangLoat_IN = ROLE.InChungTuHangLoat_In;

    constructor(
        private eventManager: JhiEventManager,
        private loginService: LoginService,
        private stateStorageService: StateStorageService,
        private elementRef: ElementRef,
        private renderer: Renderer,
        private router: Router,
        private activeModal: NgbActiveModal,
        private viewVoucherService: ViewVoucherService,
        private typeGroupService: TypeGroupService,
        private parseLinks: JhiParseLinks,
        public utilsService: UtilsService,
        private toastrService: ToastrService,
        private translateService: TranslateService,
        private principal: Principal,
        private translate: TranslateService,
        private inChungTuHangLoatService: InChungTuHangLoatService,
        private typeService: TypeService,
        private refModalService: RefModalService
    ) {
        this.typeSearch = 1;
        this.viewVouchers = [];
        this.principal.identity().then(account => {
            this.account = account;
            this.listTimeLine = this.utilsService.getCbbTimeLine();
            this.timeLineVoucher = this.listTimeLine[4].value;
            this.selectChangeBeginDateAndEndDate(this.timeLineVoucher);
        });
    }

    ngOnInit(): void {
        this.strCodePrint = '';
        this.viewVouchers = [];
        this.listOnSelect = [];
        this.paramCheckAll = false;
        this.utilsService.getCbbTimeLine2().then(data => {
            this.listTimeLine = data;
            this.timeLineVoucher = this.listTimeLine[4].value;
            this.selectChangeBeginDateAndEndDate(this.timeLineVoucher);
            this.translate
                .get([
                    'multiPrintVoucher.phieuThuMau01TTA5',
                    'multiPrintVoucher.phieuThuMau01TT',
                    'multiPrintVoucher.phieuThuMau01TT2Lien',
                    'multiPrintVoucher.phieuChi02TTA5',
                    'multiPrintVoucher.phieuChi02TT',
                    'multiPrintVoucher.phieuChi02TT2Lien',
                    'multiPrintVoucher.chungTuKeToanMauThuong',
                    'multiPrintVoucher.chungTuKeToanMauQuyDoi',
                    'multiPrintVoucher.giayBaoNo',
                    'multiPrintVoucher.giayBaoCo',
                    'multiPrintVoucher.phieuNhapKho',
                    'multiPrintVoucher.phieuNhapKhoSoLoHanDung',
                    'multiPrintVoucher.phieuXuatKho',
                    'multiPrintVoucher.phieuXuatKhoSoLoHanDung',
                    'multiPrintVoucher.phieuNhapKho',
                    'multiPrintVoucher.phieuXuatKho',
                    'multiPrintVoucher.phieuXuatKhoKiemVCNB',
                    'multiPrintVoucher.lenhLapRap',
                    'multiPrintVoucher.lenhThapDo',
                    'multiPrintVoucher.phieuXuatKhoMaQuyCach',
                    'multiPrintVoucher.hoaDonGTGT',
                    'multiPrintVoucher.hoaDonBanHang',
                    'multiPrintVoucher.bangKeHangHoaDichVu',
                    'multiPrintVoucher.phieuNhapKhoA5',
                    'multiPrintVoucher.phieuNhapKhoTheoMaQuyCach',
                    'multiPrintVoucher.donMuaHang',
                    'multiPrintVoucher.bangKiemKeQuy',
                    'multiPrintVoucher.bangChamCongTheoGio',
                    'multiPrintVoucher.bangChamCongTheoNgay',
                    'multiPrintVoucher.bangLuongTheoBuoi',
                    'multiPrintVoucher.bangLuongTheoGio',
                    'multiPrintVoucher.bangLuongCoBanCoDinh',
                    'multiPrintVoucher.bangLuongTamUng',
                    'multiPrintVoucher.bangTongHopChamCongTheoNgay',
                    'multiPrintVoucher.bangTongHopChamCongTheoGio',
                    'multiPrintVoucher.chungTuGhiSoKhongCongGop',
                    'multiPrintVoucher.chungTuGhiSoCongGopCungChungTu',
                    'multiPrintVoucher.chungTuGhiSoCongGopToanBo',
                    'multiPrintVoucher.phieuXuatKhoA5',
                    'multiPrintVoucher.bangTinhKhauHaoTSCD',
                    'multiPrintVoucher.bienBanKiemKeTSCD',
                    'multiPrintVoucher.bienBanDanhGiaLaiTSCD',
                    'multiPrintVoucher.chungTuDieuChuyenTSCD',
                    'multiPrintVoucher.chungTuGhiGiamCCDC',
                    'multiPrintVoucher.chungTuPhanBoCCDC',
                    'multiPrintVoucher.bienBanKiemKeCCDC',
                    'multiPrintVoucher.chungTuDieuChinhCCDC',
                    'multiPrintVoucher.chungTuDieuChuyenCCDC',
                    'multiPrintVoucher.lenhSanXuat',
                    'multiPrintVoucher.lenhThaoDo',
                    'multiPrintVoucher.bangKeHHDV',
                    'multiPrintVoucher.donDatHang',
                    'multiPrintVoucher.baoGiaMauCoBan',
                    'multiPrintVoucher.baoGiaMauDayDu',
                    'multiPrintVoucher.chungTuGhiGiamTSCD',
                    'multiPrintVoucher.phieuXuatKhoBanHang',
                    'multiPrintVoucher.bangTinhBaoHiem'
                ])
                .subscribe(res => {
                    this.listType = [
                        {
                            value: TypeMultiPrintTemplate.CHUNG_TU_KE_TOAN_THUONG,
                            name: res['multiPrintVoucher.chungTuKeToanMauThuong'],
                            typeGroup: TYPEGROUP.PHIEU_THU
                        },
                        {
                            value: TypeMultiPrintTemplate.CHUNG_TU_KE_TOAN_QUY_DOI,
                            name: res['multiPrintVoucher.chungTuKeToanMauQuyDoi'],
                            typeGroup: TYPEGROUP.PHIEU_THU
                        },
                        {
                            value: TypeMultiPrintTemplate.PHIEU_THU_A5,
                            name: res['multiPrintVoucher.phieuThuMau01TTA5'],
                            typeGroup: TYPEGROUP.PHIEU_THU
                        },
                        {
                            value: TypeMultiPrintTemplate.PHIEU_THU,
                            name: res['multiPrintVoucher.phieuThuMau01TT'],
                            typeGroup: TYPEGROUP.PHIEU_THU
                        },
                        {
                            value: TypeMultiPrintTemplate.PHIEU_THU_2_LIEN,
                            name: res['multiPrintVoucher.phieuThuMau01TT2Lien'],
                            typeGroup: TYPEGROUP.PHIEU_THU
                        },
                        {
                            value: TypeMultiPrintTemplate.CHUNG_TU_KE_TOAN_THUONG,
                            name: res['multiPrintVoucher.chungTuKeToanMauThuong'],
                            typeGroup: TYPEGROUP.PHIEU_CHI
                        },
                        {
                            value: TypeMultiPrintTemplate.CHUNG_TU_KE_TOAN_QUY_DOI,
                            name: res['multiPrintVoucher.chungTuKeToanMauQuyDoi'],
                            typeGroup: TYPEGROUP.PHIEU_CHI
                        },
                        {
                            value: TypeMultiPrintTemplate.PHIEU_CHI_A5,
                            name: res['multiPrintVoucher.phieuChi02TTA5'],
                            typeGroup: TYPEGROUP.PHIEU_CHI
                        },
                        {
                            value: TypeMultiPrintTemplate.PHIEU_CHI,
                            name: res['multiPrintVoucher.phieuChi02TT'],
                            typeGroup: TYPEGROUP.PHIEU_CHI
                        },
                        {
                            value: TypeMultiPrintTemplate.PHIEU_CHI_2_LIEN,
                            name: res['multiPrintVoucher.phieuChi02TT2Lien'],
                            typeGroup: TYPEGROUP.PHIEU_CHI
                        },
                        {
                            value: TypeMultiPrintTemplate.CHUNG_TU_KE_TOAN_THUONG,
                            name: res['multiPrintVoucher.chungTuKeToanMauThuong'],
                            typeGroup: TYPEGROUP.UY_NHIEM_CHI
                        },
                        {
                            value: TypeMultiPrintTemplate.CHUNG_TU_KE_TOAN_QUY_DOI,
                            name: res['multiPrintVoucher.chungTuKeToanMauQuyDoi'],
                            typeGroup: TYPEGROUP.UY_NHIEM_CHI
                        },
                        {
                            value: TypeMultiPrintTemplate.GIAY_BAO_NO,
                            name: res['multiPrintVoucher.giayBaoNo'],
                            typeGroup: TYPEGROUP.UY_NHIEM_CHI
                        },
                        {
                            value: TypeMultiPrintTemplate.CHUNG_TU_KE_TOAN_THUONG,
                            name: res['multiPrintVoucher.chungTuKeToanMauThuong'],
                            typeGroup: TYPEGROUP.SEC_CHUYEN_KHOAN
                        },
                        {
                            value: TypeMultiPrintTemplate.CHUNG_TU_KE_TOAN_QUY_DOI,
                            name: res['multiPrintVoucher.chungTuKeToanMauQuyDoi'],
                            typeGroup: TYPEGROUP.SEC_CHUYEN_KHOAN
                        },
                        {
                            value: TypeMultiPrintTemplate.GIAY_BAO_NO,
                            name: res['multiPrintVoucher.giayBaoNo'],
                            typeGroup: TYPEGROUP.SEC_CHUYEN_KHOAN
                        },
                        {
                            value: TypeMultiPrintTemplate.CHUNG_TU_KE_TOAN_THUONG,
                            name: res['multiPrintVoucher.chungTuKeToanMauThuong'],
                            typeGroup: TYPEGROUP.SEC_TIEN_MAT
                        },
                        {
                            value: TypeMultiPrintTemplate.CHUNG_TU_KE_TOAN_QUY_DOI,
                            name: res['multiPrintVoucher.chungTuKeToanMauQuyDoi'],
                            typeGroup: TYPEGROUP.SEC_TIEN_MAT
                        },
                        {
                            value: TypeMultiPrintTemplate.GIAY_BAO_NO,
                            name: res['multiPrintVoucher.giayBaoNo'],
                            typeGroup: TYPEGROUP.SEC_TIEN_MAT
                        },
                        {
                            value: TypeMultiPrintTemplate.CHUNG_TU_KE_TOAN_THUONG,
                            name: res['multiPrintVoucher.chungTuKeToanMauThuong'],
                            typeGroup: TYPEGROUP.CHUYEN_TIEN_NOI_BO
                        },
                        {
                            value: TypeMultiPrintTemplate.CHUNG_TU_KE_TOAN_QUY_DOI,
                            name: res['multiPrintVoucher.chungTuKeToanMauQuyDoi'],
                            typeGroup: TYPEGROUP.CHUYEN_TIEN_NOI_BO
                        },
                        {
                            value: TypeMultiPrintTemplate.GIAY_BAO_NO,
                            name: res['multiPrintVoucher.giayBaoNo'],
                            typeGroup: TYPEGROUP.CHUYEN_TIEN_NOI_BO
                        },
                        {
                            value: TypeMultiPrintTemplate.GIAY_BAO_CO,
                            name: res['multiPrintVoucher.giayBaoCo'],
                            typeGroup: TYPEGROUP.CHUYEN_TIEN_NOI_BO
                        },
                        {
                            value: TypeMultiPrintTemplate.CHUNG_TU_KE_TOAN_THUONG,
                            name: res['multiPrintVoucher.chungTuKeToanMauThuong'],
                            typeGroup: TYPEGROUP.BAO_CO
                        },
                        {
                            value: TypeMultiPrintTemplate.CHUNG_TU_KE_TOAN_QUY_DOI,
                            name: res['multiPrintVoucher.chungTuKeToanMauQuyDoi'],
                            typeGroup: TYPEGROUP.BAO_CO
                        },
                        {
                            value: TypeMultiPrintTemplate.GIAY_BAO_CO,
                            name: res['multiPrintVoucher.giayBaoCo'],
                            typeGroup: TYPEGROUP.BAO_CO
                        },
                        {
                            value: TypeMultiPrintTemplate.CHUNG_TU_KE_TOAN_THUONG,
                            name: res['multiPrintVoucher.chungTuKeToanMauThuong'],
                            typeGroup: TYPEGROUP.THE_TIN_DUNG
                        },
                        {
                            value: TypeMultiPrintTemplate.CHUNG_TU_KE_TOAN_QUY_DOI,
                            name: res['multiPrintVoucher.chungTuKeToanMauQuyDoi'],
                            typeGroup: TYPEGROUP.THE_TIN_DUNG
                        },
                        {
                            value: TypeMultiPrintTemplate.BANG_KIEM_KE_QUY,
                            name: res['multiPrintVoucher.bangKiemKeQuy'],
                            typeGroup: TYPEGROUP.KIEM_KE_QUY
                        },
                        {
                            value: TypeMultiPrintTemplate.DON_MUA_HANG,
                            name: res['multiPrintVoucher.donMuaHang'],
                            typeGroup: TYPEGROUP.DON_MUA_HANG
                        },
                        {
                            value: TypeMultiPrintTemplate.CHUNG_TU_KE_TOAN_THUONG,
                            name: res['multiPrintVoucher.chungTuKeToanMauThuong'],
                            typeGroup: TYPEGROUP.MUA_HANG
                        },
                        {
                            value: TypeMultiPrintTemplate.CHUNG_TU_KE_TOAN_QUY_DOI,
                            name: res['multiPrintVoucher.chungTuKeToanMauQuyDoi'],
                            typeGroup: TYPEGROUP.MUA_HANG
                        },
                        // {
                        //     value: TypeMultiPrintTemplate.PHIEU_NHAP_KHO,
                        //     name: res['multiPrintVoucher.phieuNhapKho'],
                        //     typeGroup: TYPEGROUP.MUA_HANG
                        // },
                        // {
                        //     value: TypeMultiPrintTemplate.PHIEU_CHI,
                        //     name: res['multiPrintVoucher.phieuChi02TT'],
                        //     typeGroup: TYPEGROUP.MUA_HANG
                        // },
                        // {
                        //     value: TypeMultiPrintTemplate.PHIEU_CHI_A5,
                        //     name: res['multiPrintVoucher.phieuChi02TTA5'],
                        //     typeGroup: TYPEGROUP.MUA_HANG
                        // },
                        // {
                        //     value: TypeMultiPrintTemplate.PHIEU_CHI_2_LIEN,
                        //     name: res['multiPrintVoucher.phieuChi02TT2Lien'],
                        //     typeGroup: TYPEGROUP.MUA_HANG
                        // },
                        // {
                        //     value: TypeMultiPrintTemplate.PHIEU_NHAP_KHO_A5,
                        //     name: res['multiPrintVoucher.phieuNhapKhoA5'],
                        //     typeGroup: TYPEGROUP.MUA_HANG
                        // },
                        // {
                        //     value: TypeMultiPrintTemplate.GIAY_BAO_NO,
                        //     name: res['multiPrintVoucher.giayBaoNo'],
                        //     typeGroup: TYPEGROUP.MUA_HANG
                        // },
                        // {
                        //     value: TypeMultiPrintTemplate.PHIEU_NHAP_KHO_THEO_MA_QUY_CACH,
                        //     name: res['multiPrintVoucher.phieuNhapKhoTheoMaQuyCach'],
                        //     typeGroup: TYPEGROUP.MUA_HANG
                        // },
                        // {
                        //     value: TypeMultiPrintTemplate.PHIEU_NHAP_KHO_SO_LO_HAN_DUNG,
                        //     name: res['multiPrintVoucher.phieuNhapKhoSoLoHanDung'],
                        //     typeGroup: TYPEGROUP.MUA_HANG
                        // },
                        {
                            value: TypeMultiPrintTemplate.CHUNG_TU_KE_TOAN_THUONG,
                            name: res['multiPrintVoucher.chungTuKeToanMauThuong'],
                            typeGroup: TYPEGROUP.HANG_MUA_TRA_LAI
                        },
                        {
                            value: TypeMultiPrintTemplate.CHUNG_TU_KE_TOAN_QUY_DOI,
                            name: res['multiPrintVoucher.chungTuKeToanMauQuyDoi'],
                            typeGroup: TYPEGROUP.HANG_MUA_TRA_LAI
                        },
                        // {
                        //     value: TypeMultiPrintTemplate.PHIEU_XUAT_KHO,
                        //     name: res['multiPrintVoucher.phieuXuatKho'],
                        //     typeGroup: TYPEGROUP.HANG_MUA_TRA_LAI
                        // },
                        // {
                        //     value: TypeMultiPrintTemplate.PHIEU_THU,
                        //     name: res['multiPrintVoucher.phieuChi02TT'],
                        //     typeGroup: TYPEGROUP.HANG_MUA_TRA_LAI
                        // },
                        // {
                        //     value: TypeMultiPrintTemplate.GIAY_BAO_CO,
                        //     name: res['multiPrintVoucher.giayBaoCo'],
                        //     typeGroup: TYPEGROUP.HANG_MUA_TRA_LAI
                        // },
                        // {
                        //     value: TypeMultiPrintTemplate.PHIEU_THU_A5,
                        //     name: res['multiPrintVoucher.phieuThuMau01TTA5'],
                        //     typeGroup: TYPEGROUP.HANG_MUA_TRA_LAI
                        // },
                        // {
                        //     value: TypeMultiPrintTemplate.PHIEU_NHAP_KHO_A5,
                        //     name: res['multiPrintVoucher.phieuNhapKhoA5'],
                        //     typeGroup: TYPEGROUP.HANG_MUA_TRA_LAI
                        // },
                        // {
                        //     value: TypeMultiPrintTemplate.PHIEU_CHI,
                        //     name: res['multiPrintVoucher.phieuThuMau01TT2Lien'],
                        //     typeGroup: TYPEGROUP.HANG_MUA_TRA_LAI
                        // },
                        // {
                        //     value: TypeMultiPrintTemplate.PHIEU_XUAT_KHO_SO_LO_HAN_DUNG,
                        //     name: res['multiPrintVoucher.phieuXuatKhoSoLoHanDung'],
                        //     typeGroup: TYPEGROUP.HANG_MUA_TRA_LAI
                        // },
                        // {
                        //     value: TypeMultiPrintTemplate.HOA_DON_GTGT,
                        //     name: res['multiPrintVoucher.hoaDonGTGT'],
                        //     typeGroup: TYPEGROUP.HANG_MUA_TRA_LAI
                        // },
                        // {
                        //     value: TypeMultiPrintTemplate.PHIEU_XUAT_KHO_MA_QUY_CACH,
                        //     name: res['multiPrintVoucher.phieuXuatKhoMaQuyCach'],
                        //     typeGroup: TYPEGROUP.HANG_MUA_TRA_LAI
                        // },
                        // {
                        //     value: TypeMultiPrintTemplate.HOA_DON_BAN_HANG,
                        //     name: res['multiPrintVoucher.hoaDonBanHang'],
                        //     typeGroup: TYPEGROUP.HANG_MUA_TRA_LAI
                        // },
                        // {
                        //     value: TypeMultiPrintTemplate.BANG_KE_HHDV,
                        //     name: res['multiPrintVoucher.bangKeHangHoaDichVu'],
                        //     typeGroup: TYPEGROUP.HANG_MUA_TRA_LAI
                        // },
                        {
                            value: TypeMultiPrintTemplate.CHUNG_TU_KE_TOAN_THUONG,
                            name: res['multiPrintVoucher.chungTuKeToanMauThuong'],
                            typeGroup: TYPEGROUP.HANG_MUA_GIAM_GIA
                        },
                        {
                            value: TypeMultiPrintTemplate.CHUNG_TU_KE_TOAN_QUY_DOI,
                            name: res['multiPrintVoucher.chungTuKeToanMauQuyDoi'],
                            typeGroup: TYPEGROUP.HANG_MUA_GIAM_GIA
                        },
                        // {
                        //     value: TypeMultiPrintTemplate.PHIEU_THU,
                        //     name: res['multiPrintVoucher.phieuChi02TT'],
                        //     typeGroup: TYPEGROUP.HANG_MUA_GIAM_GIA
                        // },
                        // {
                        //     value: TypeMultiPrintTemplate.PHIEU_THU_2_LIEN,
                        //     name: res['multiPrintVoucher.phieuThuMau01TT2Lien'],
                        //     typeGroup: TYPEGROUP.HANG_MUA_GIAM_GIA
                        // },
                        // {
                        //     value: TypeMultiPrintTemplate.PHIEU_CHI_A5,
                        //     name: res['multiPrintVoucher.phieuChi02TTA5'],
                        //     typeGroup: TYPEGROUP.HANG_MUA_GIAM_GIA
                        // },
                        // {
                        //     value: TypeMultiPrintTemplate.GIAY_BAO_CO,
                        //     name: res['multiPrintVoucher.giayBaoCo'],
                        //     typeGroup: TYPEGROUP.HANG_MUA_GIAM_GIA
                        // },
                        {
                            value: TypeMultiPrintTemplate.CHUNG_TU_KE_TOAN_THUONG,
                            name: res['multiPrintVoucher.chungTuKeToanMauThuong'],
                            typeGroup: TYPEGROUP.MUA_DICH_VU
                        },
                        {
                            value: TypeMultiPrintTemplate.CHUNG_TU_KE_TOAN_QUY_DOI,
                            name: res['multiPrintVoucher.chungTuKeToanMauQuyDoi'],
                            typeGroup: TYPEGROUP.MUA_DICH_VU
                        },
                        // {
                        //     value: TypeMultiPrintTemplate.PHIEU_CHI,
                        //     name: res['multiPrintVoucher.phieuChi02TT'],
                        //     typeGroup: TYPEGROUP.MUA_DICH_VU
                        // },
                        // {
                        //     value: TypeMultiPrintTemplate.PHIEU_CHI_A5,
                        //     name: res['multiPrintVoucher.phieuChi02TTA5'],
                        //     typeGroup: TYPEGROUP.MUA_DICH_VU
                        // },
                        // {
                        //     value: TypeMultiPrintTemplate.PHIEU_CHI_2_LIEN,
                        //     name: res['multiPrintVoucher.phieuChi02TT2Lien'],
                        //     typeGroup: TYPEGROUP.MUA_DICH_VU
                        // },
                        // {
                        //     value: TypeMultiPrintTemplate.GIAY_BAO_NO,
                        //     name: res['multiPrintVoucher.giayBaoNo'],
                        //     typeGroup: TYPEGROUP.MUA_DICH_VU
                        // },
                        {
                            value: TypeMultiPrintTemplate.HOA_DON_GTGT,
                            name: res['multiPrintVoucher.hoaDonGTGT'],
                            typeGroup: TYPEGROUP.HOA_DON_DAU_VAO
                        },
                        {
                            value: TypeMultiPrintTemplate.HOA_DON_BAN_HANG,
                            name: res['multiPrintVoucher.hoaDonBanHang'],
                            typeGroup: TYPEGROUP.HOA_DON_DAU_VAO
                        },
                        {
                            value: TypeMultiPrintTemplate.CHUNG_TU_KE_TOAN_THUONG,
                            name: res['multiPrintVoucher.chungTuKeToanMauThuong'],
                            typeGroup: TYPEGROUP.BAN_HANG
                        },
                        {
                            value: TypeMultiPrintTemplate.CHUNG_TU_KE_TOAN_QUY_DOI,
                            name: res['multiPrintVoucher.chungTuKeToanMauQuyDoi'],
                            typeGroup: TYPEGROUP.BAN_HANG
                        },
                        {
                            value: TypeMultiPrintTemplate.PHIEU_XUAT_KHO_BAN_HANG,
                            name: res['multiPrintVoucher.phieuXuatKhoBanHang'],
                            typeGroup: TYPEGROUP.BAN_HANG
                        },
                        {
                            value: TypeMultiPrintTemplate.CHUNG_TU_KE_TOAN_THUONG,
                            name: res['multiPrintVoucher.chungTuKeToanMauThuong'],
                            typeGroup: TYPEGROUP.HANG_BAN_TRA_LAI
                        },
                        {
                            value: TypeMultiPrintTemplate.CHUNG_TU_KE_TOAN_QUY_DOI,
                            name: res['multiPrintVoucher.chungTuKeToanMauQuyDoi'],
                            typeGroup: TYPEGROUP.HANG_BAN_TRA_LAI
                        },
                        {
                            value: TypeMultiPrintTemplate.CHUNG_TU_KE_TOAN_THUONG,
                            name: res['multiPrintVoucher.chungTuKeToanMauThuong'],
                            typeGroup: TYPEGROUP.HANG_BAN_GIAM_GIA
                        },
                        {
                            value: TypeMultiPrintTemplate.CHUNG_TU_KE_TOAN_QUY_DOI,
                            name: res['multiPrintVoucher.chungTuKeToanMauQuyDoi'],
                            typeGroup: TYPEGROUP.HANG_BAN_GIAM_GIA
                        },
                        {
                            value: TypeMultiPrintTemplate.CHUNG_TU_KE_TOAN_THUONG,
                            name: res['multiPrintVoucher.chungTuKeToanMauThuong'],
                            typeGroup: TYPEGROUP.NHAP_KHO
                        },
                        {
                            value: TypeMultiPrintTemplate.PHIEU_NHAP_KHO,
                            name: res['multiPrintVoucher.phieuNhapKho'],
                            typeGroup: TYPEGROUP.NHAP_KHO
                        },
                        {
                            value: TypeMultiPrintTemplate.PHIEU_NHAP_KHO_A5,
                            name: res['multiPrintVoucher.phieuNhapKhoA5'],
                            typeGroup: TYPEGROUP.NHAP_KHO
                        },
                        {
                            value: TypeMultiPrintTemplate.PHIEU_NHAP_KHO_SO_LO_HAN_DUNG,
                            name: res['multiPrintVoucher.phieuNhapKhoSoLoHanDung'],
                            typeGroup: TYPEGROUP.NHAP_KHO
                        },
                        {
                            value: TypeMultiPrintTemplate.PHIEU_NHAP_KHO_THEO_MA_QUY_CACH,
                            name: res['multiPrintVoucher.phieuNhapKhoTheoMaQuyCach'],
                            typeGroup: TYPEGROUP.NHAP_KHO
                        },
                        {
                            value: TypeMultiPrintTemplate.CHUNG_TU_KE_TOAN_THUONG,
                            name: res['multiPrintVoucher.chungTuKeToanMauThuong'],
                            typeGroup: TYPEGROUP.XUAT_KHO
                        },
                        {
                            value: TypeMultiPrintTemplate.PHIEU_XUAT_KHO,
                            name: res['multiPrintVoucher.phieuXuatKho'],
                            typeGroup: TYPEGROUP.XUAT_KHO
                        },
                        {
                            value: TypeMultiPrintTemplate.PHIEU_XUAT_KHO_A5,
                            name: res['multiPrintVoucher.phieuXuatKhoA5'],
                            typeGroup: TYPEGROUP.XUAT_KHO
                        },
                        {
                            value: TypeMultiPrintTemplate.PHIEU_XUAT_KHO_SO_LO_HAN_DUNG,
                            name: res['multiPrintVoucher.phieuXuatKhoSoLoHanDung'],
                            typeGroup: TYPEGROUP.XUAT_KHO
                        },
                        {
                            value: TypeMultiPrintTemplate.PHIEU_XUAT_KHO_MA_QUY_CACH,
                            name: res['multiPrintVoucher.phieuXuatKhoMaQuyCach'],
                            typeGroup: TYPEGROUP.XUAT_KHO
                        },
                        {
                            value: TypeMultiPrintTemplate.CHUNG_TU_KE_TOAN_THUONG,
                            name: res['multiPrintVoucher.chungTuKeToanMauThuong'],
                            typeGroup: TYPEGROUP.CHUYEN_KHO
                        },
                        {
                            value: TypeMultiPrintTemplate.PHIEU_XUAT_KHO,
                            name: res['multiPrintVoucher.phieuXuatKho'],
                            typeGroup: TYPEGROUP.CHUYEN_KHO
                        },
                        {
                            value: TypeMultiPrintTemplate.PHIEU_XUAT_KHO_A5,
                            name: res['multiPrintVoucher.phieuXuatKhoA5'],
                            typeGroup: TYPEGROUP.CHUYEN_KHO
                        },
                        {
                            value: TypeMultiPrintTemplate.PHIEU_XUAT_KHO_SO_LO_HAN_DUNG,
                            name: res['multiPrintVoucher.phieuXuatKhoSoLoHanDung'],
                            typeGroup: TYPEGROUP.CHUYEN_KHO
                        },
                        {
                            value: TypeMultiPrintTemplate.PHIEU_XUAT_KHO_MA_QUY_CACH,
                            name: res['multiPrintVoucher.phieuXuatKhoMaQuyCach'],
                            typeGroup: TYPEGROUP.CHUYEN_KHO
                        },
                        {
                            value: TypeMultiPrintTemplate.PHIEU_NHAP_KHO,
                            name: res['multiPrintVoucher.phieuNhapKho'],
                            typeGroup: TYPEGROUP.CHUYEN_KHO
                        },
                        {
                            value: TypeMultiPrintTemplate.PHIEU_NHAP_KHO_A5,
                            name: res['multiPrintVoucher.phieuNhapKhoA5'],
                            typeGroup: TYPEGROUP.CHUYEN_KHO
                        },
                        {
                            value: TypeMultiPrintTemplate.PHIEU_NHAP_KHO_SO_LO_HAN_DUNG,
                            name: res['multiPrintVoucher.phieuNhapKhoSoLoHanDung'],
                            typeGroup: TYPEGROUP.CHUYEN_KHO
                        },
                        {
                            value: TypeMultiPrintTemplate.PHIEU_NHAP_KHO_THEO_MA_QUY_CACH,
                            name: res['multiPrintVoucher.phieuNhapKhoTheoMaQuyCach'],
                            typeGroup: TYPEGROUP.CHUYEN_KHO
                        },
                        {
                            value: TypeMultiPrintTemplate.CHUNG_TU_GHI_GIAM_CCDC,
                            name: res['multiPrintVoucher.chungTuGhiGiamCCDC'],
                            typeGroup: TYPEGROUP.GHI_GIAM_CCDC
                        },
                        {
                            value: TypeMultiPrintTemplate.CHUNG_TU_KE_TOAN_THUONG,
                            name: res['multiPrintVoucher.chungTuKeToanMauThuong'],
                            typeGroup: TYPEGROUP.CHUNG_TU_NGHIEP_VU_KHAC
                        },
                        {
                            value: TypeMultiPrintTemplate.CHUNG_TU_KE_TOAN_QUY_DOI,
                            name: res['multiPrintVoucher.chungTuKeToanMauQuyDoi'],
                            typeGroup: TYPEGROUP.CHUNG_TU_NGHIEP_VU_KHAC
                        },
                        {
                            value: TypeMultiPrintTemplate.LENH_SAN_XUAT,
                            name: res['multiPrintVoucher.lenhSanXuat'],
                            typeGroup: TYPEGROUP.LENH_SAN_XUAT
                        },
                        {
                            value: TypeMultiPrintTemplate.BANG_CHAM_CONG_THEO_GIO,
                            name: res['multiPrintVoucher.bangChamCongTheoGio'],
                            typeGroup: TYPEGROUP.BANG_CHAM_CONG
                        },
                        {
                            value: TypeMultiPrintTemplate.BANG_CHAM_CONG_THEO_NGAY,
                            name: res['multiPrintVoucher.bangChamCongTheoNgay'],
                            typeGroup: TYPEGROUP.BANG_CHAM_CONG
                        },
                        {
                            value: TypeMultiPrintTemplate.BANG_TONG_HOP_CHAM_CONG_THEO_NGAY,
                            name: res['multiPrintVoucher.bangTongHopChamCongTheoNgay'],
                            typeGroup: TYPEGROUP.BANG_TONG_HOP_CHAM_CONG
                        },
                        {
                            value: TypeMultiPrintTemplate.BANG_TONG_HOP_CHAM_CONG_THEO_GIO,
                            name: res['multiPrintVoucher.bangTongHopChamCongTheoGio'],
                            typeGroup: TYPEGROUP.BANG_TONG_HOP_CHAM_CONG
                        },
                        {
                            value: TypeMultiPrintTemplate.BANG_LUONG_THEO_BUOI,
                            name: res['multiPrintVoucher.bangLuongTheoBuoi'],
                            typeGroup: TYPEGROUP.BANG_LUONG
                        },
                        {
                            value: TypeMultiPrintTemplate.BANG_LUONG_THEO_GIO,
                            name: res['multiPrintVoucher.bangLuongTheoGio'],
                            typeGroup: TYPEGROUP.BANG_LUONG
                        },
                        {
                            value: TypeMultiPrintTemplate.BANG_LUONG_CO_BAN_CO_DINH,
                            name: res['multiPrintVoucher.bangLuongCoBanCoDinh'],
                            typeGroup: TYPEGROUP.BANG_LUONG
                        },
                        {
                            value: TypeMultiPrintTemplate.BANG_TINH_BAO_HIEM,
                            name: res['multiPrintVoucher.bangTinhBaoHiem'],
                            typeGroup: TYPEGROUP.BANG_LUONG
                        },
                        // {
                        //     value: TypeMultiPrintTemplate.BANG_LUONG_TAM_UNG,
                        //     name: res['multiPrintVoucher.bangLuongTamUng'],
                        //     typeGroup: TYPEGROUP.BANG_LUONG
                        // },
                        {
                            value: TypeMultiPrintTemplate.CHUNG_TU_GHI_SO_KHONG_CONG_GOP,
                            name: res['multiPrintVoucher.chungTuGhiSoKhongCongGop'],
                            typeGroup: TYPEGROUP.CHUNG_TU_GHI_SO
                        },
                        {
                            value: TypeMultiPrintTemplate.CHUNG_TU_GHI_SO_CONG_GOP_CUNG_CHUNG_TU,
                            name: res['multiPrintVoucher.chungTuGhiSoCongGopCungChungTu'],
                            typeGroup: TYPEGROUP.CHUNG_TU_GHI_SO
                        },
                        {
                            value: TypeMultiPrintTemplate.CHUNG_TU_GHI_SO_CONG_GOP_TOAN_BO,
                            name: res['multiPrintVoucher.chungTuGhiSoCongGopToanBo'],
                            typeGroup: TYPEGROUP.CHUNG_TU_GHI_SO
                        },
                        {
                            value: TypeMultiPrintTemplate.CHUNG_TU_KE_TOAN_THUONG,
                            name: res['multiPrintVoucher.chungTuKeToanMauThuong'],
                            typeGroup: TYPEGROUP.TINH_KHAU_HAO_TSCD
                        },
                        {
                            value: TypeMultiPrintTemplate.BANG_TINH_KHAU_HAO_TSCD,
                            name: res['multiPrintVoucher.bangTinhKhauHaoTSCD'],
                            typeGroup: TYPEGROUP.TINH_KHAU_HAO_TSCD
                        },
                        {
                            value: TypeMultiPrintTemplate.BIEN_BAN_KIEM_KE_TSCD,
                            name: res['multiPrintVoucher.bienBanKiemKeTSCD'],
                            typeGroup: TYPEGROUP.KIEM_KE_TSCD
                        },
                        {
                            value: TypeMultiPrintTemplate.CHUNG_TU_KE_TOAN_THUONG,
                            name: res['multiPrintVoucher.chungTuKeToanMauThuong'],
                            typeGroup: TYPEGROUP.DIEU_CHINH_TSCD
                        },
                        {
                            value: TypeMultiPrintTemplate.BIEN_BAN_DANH_GIA_LAI_TSCD,
                            name: res['multiPrintVoucher.bienBanDanhGiaLaiTSCD'],
                            typeGroup: TYPEGROUP.DIEU_CHINH_TSCD
                        },
                        {
                            value: TypeMultiPrintTemplate.CHUNG_TU_DIEU_CHUYEN_TSCD,
                            name: res['multiPrintVoucher.chungTuDieuChuyenTSCD'],
                            typeGroup: TYPEGROUP.DIEU_CHUYEN_TSCD
                        },
                        {
                            value: TypeMultiPrintTemplate.CHUNG_TU_KE_TOAN_THUONG,
                            name: res['multiPrintVoucher.chungTuKeToanMauThuong'],
                            typeGroup: TYPEGROUP.PHAN_BO_CCDC
                        },
                        {
                            value: TypeMultiPrintTemplate.CHUNG_TU_PHAN_BO_CCDC,
                            name: res['multiPrintVoucher.chungTuPhanBoCCDC'],
                            typeGroup: TYPEGROUP.PHAN_BO_CCDC
                        },
                        {
                            value: TypeMultiPrintTemplate.BIEN_BAN_KIEM_KE_CCDC,
                            name: res['multiPrintVoucher.bienBanKiemKeCCDC'],
                            typeGroup: TYPEGROUP.KIEM_KE_CCDC
                        },
                        {
                            value: TypeMultiPrintTemplate.CHUNG_TU_DIEU_CHINH_CCDC,
                            name: res['multiPrintVoucher.chungTuDieuChinhCCDC'],
                            typeGroup: TYPEGROUP.DIEU_CHINH_CCDC
                        },
                        {
                            value: TypeMultiPrintTemplate.CHUNG_TU_DIEU_CHUYEN_CCDC,
                            name: res['multiPrintVoucher.chungTuDieuChuyenCCDC'],
                            typeGroup: TYPEGROUP.DIEU_CHUYEN_CCDC
                        },
                        {
                            value: TypeMultiPrintTemplate.LENH_LAP_RAP,
                            name: res['multiPrintVoucher.lenhLapRap'],
                            typeGroup: TYPEGROUP.LENH_LAP_RAP
                        },
                        {
                            value: TypeMultiPrintTemplate.LENH_THAO_DO,
                            name: res['multiPrintVoucher.lenhThaoDo'],
                            typeGroup: TYPEGROUP.LENH_LAP_RAP
                        },
                        {
                            value: TypeMultiPrintTemplate.HOA_DON_GTGT,
                            name: res['multiPrintVoucher.hoaDonGTGT'],
                            typeGroup: TYPEGROUP.XUAT_HOA_DON
                        },
                        {
                            value: TypeMultiPrintTemplate.HOA_DON_BAN_HANG,
                            name: res['multiPrintVoucher.hoaDonBanHang'],
                            typeGroup: TYPEGROUP.XUAT_HOA_DON
                        },
                        {
                            value: TypeMultiPrintTemplate.BANG_KE_HHDV,
                            name: res['multiPrintVoucher.bangKeHHDV'],
                            typeGroup: TYPEGROUP.XUAT_HOA_DON
                        },
                        {
                            value: TypeMultiPrintTemplate.DON_DAT_HANG,
                            name: res['multiPrintVoucher.donDatHang'],
                            typeGroup: TYPEGROUP.DON_DAT_HANG
                        },
                        {
                            value: TypeMultiPrintTemplate.BAO_GIA_MAU_CO_BAN,
                            name: res['multiPrintVoucher.baoGiaMauCoBan'],
                            typeGroup: TYPEGROUP.BAO_GIA
                        },
                        {
                            value: TypeMultiPrintTemplate.BAO_GIA_MAU_DAY_DU,
                            name: res['multiPrintVoucher.baoGiaMauDayDu'],
                            typeGroup: TYPEGROUP.BAO_GIA
                        },
                        {
                            value: TypeMultiPrintTemplate.CHUNG_TU_KE_TOAN_THUONG,
                            name: res['multiPrintVoucher.chungTuKeToanMauThuong'],
                            typeGroup: TYPEGROUP.GHI_GIAM_TSCD
                        },
                        {
                            value: TypeMultiPrintTemplate.CHUNG_TU_GHI_GIAM_TSCD,
                            name: res['multiPrintVoucher.chungTuGhiGiamTSCD'],
                            typeGroup: TYPEGROUP.GHI_GIAM_TSCD
                        }
                    ];
                    this.listTypePrintForm = [];
                });
        });
        this.typeSearch = 1;
        this.recorded = null;
        this.typeGroupService.queryForPrint().subscribe(res => {
            this.typeGroups = res.body;
            // console.table(this.typeGroups);
        });
        this.typeService.getTypes().subscribe(res => {
            this.types = res.body;
        });
        this.page = 1;
        if (this.data) {
            this.newList = [];
            this.newList.push(...this.data);
        }
    }

    search() {
        this.paramCheckAll = false;
        if (!this.typeGroup) {
            this.toastrService.error(this.translateService.instant('multiPrintVoucher.typeGroupNotNull'));
            return;
        }
        if (!this.fromDate) {
            this.toastrService.error(this.translateService.instant('ebwebApp.muaHang.muaDichVu.toastr.fromDateNull'));
            return;
        }
        if (!this.toDate) {
            this.toastrService.error(this.translateService.instant('ebwebApp.muaHang.muaDichVu.toastr.toDateNull'));
            return;
        }
        if (this.fromDate > this.toDate) {
            this.toastrService.error(this.translateService.instant('ebwebApp.mCAudit.errorDate'));
            return;
        }
        let isQuyDoi = false;
        if (this.valueVoucher === TypeMultiPrintTemplate.CHUNG_TU_KE_TOAN_QUY_DOI) {
            isQuyDoi = true;
        }
        this.viewVoucherService
            .query({
                page: this.page - 1,
                size: this.itemsPerPage,
                typeSearch: this.typeSearch ? this.typeSearch : '',
                status: this.status !== null && this.status !== undefined ? this.status : '',
                fromDate: this.fromDate ? this.fromDate : '',
                toDate: this.toDate ? this.toDate : '',
                typeGroup: this.typeGroup && this.typeGroup ? this.typeGroup : '',
                recorded: this.recorded !== null && this.recorded !== undefined ? this.recorded : '',
                no: this.no ? this.no : '',
                invoiceNo: this.invoiceNo ? this.invoiceNo : '',
                isThamChieu: false,
                isQuyDoi,
                typeTemplate: this.valueVoucher ? this.valueVoucher : 0
            })
            .subscribe(res => {
                this.viewVouchers = res.body;
                // if (this.newList) {
                //     this.viewVouchers.forEach(item => {
                //         item.checked = this.newList.some(data => data.refID2 === item.refID2);
                //     });
                // }
                this.links = this.parseLinks.parse(res.headers.get('link'));
                this.totalItems = parseInt(res.headers.get('X-Total-Count'), 10);
                this.queryCount = this.totalItems;
                if (this.paramCheckAll) {
                    this.viewVouchers.forEach(n => {
                        if (this.listOnSelect.some(m => m.refID2 === n.refID2)) {
                            n.checked = false;
                        } else {
                            n.checked = true;
                        }
                    });
                } else {
                    this.viewVouchers.forEach(n => {
                        if (this.listOnSelect.some(m => m.refID2 === n.refID2)) {
                            n.checked = true;
                        } else {
                            n.checked = false;
                        }
                    });
                }
            });
    }

    autoSearch() {
        this.paramCheckAll = false;
        if (!this.typeGroup) {
            return;
        }
        if (!this.fromDate) {
            return;
        }
        if (!this.toDate) {
            return;
        }
        if (this.fromDate > this.toDate) {
            return;
        }
        let isQuyDoi = false;
        if (this.valueVoucher === TypeMultiPrintTemplate.CHUNG_TU_KE_TOAN_QUY_DOI) {
            isQuyDoi = true;
        }
        this.viewVoucherService
            .query({
                page: this.page - 1,
                size: this.itemsPerPage,
                typeSearch: this.typeSearch ? this.typeSearch : '',
                status: this.status !== null && this.status !== undefined ? this.status : '',
                fromDate: this.fromDate ? this.fromDate : '',
                toDate: this.toDate ? this.toDate : '',
                typeGroup: this.typeGroup && this.typeGroup ? this.typeGroup : '',
                recorded: this.recorded !== null && this.recorded !== undefined ? this.recorded : '',
                no: this.no ? this.no : '',
                invoiceNo: this.invoiceNo ? this.invoiceNo : '',
                isThamChieu: false,
                isQuyDoi,
                typeTemplate: this.valueVoucher ? this.valueVoucher : 0
            })
            .subscribe(res => {
                this.viewVouchers = res.body;
                // if (this.newList) {
                //     this.viewVouchers.forEach(item => {
                //         item.checked = this.newList.some(data => data.refID2 === item.refID2);
                //     });
                // }
                this.links = this.parseLinks.parse(res.headers.get('link'));
                this.totalItems = parseInt(res.headers.get('X-Total-Count'), 10);
                this.queryCount = this.totalItems;
                if (this.paramCheckAll) {
                    this.viewVouchers.forEach(n => {
                        if (this.listOnSelect.some(m => m.refID2 === n.refID2)) {
                            n.checked = false;
                        } else {
                            n.checked = true;
                        }
                    });
                } else {
                    this.viewVouchers.forEach(n => {
                        if (this.listOnSelect.some(m => m.refID2 === n.refID2)) {
                            n.checked = true;
                        } else {
                            n.checked = false;
                        }
                    });
                }
            });
    }

    async apply() {
        if (!this.typeGroup) {
            this.toastrService.error(this.translateService.instant('multiPrintVoucher.typeGroupNotNull'));
            return;
        }
        if (this.valueVoucher === null || this.valueVoucher === undefined) {
            this.toastrService.error(this.translateService.instant('multiPrintVoucher.valueVoucherNotNull'));
            return;
        }
        const listID = this.listOnSelect.map(n => n.refID2);
        if (!listID || (listID.length === 0 && !this.paramCheckAll) || (this.paramCheckAll && listID.length === this.totalItems)) {
            return this.toastrService.error(this.translate.instant('multiPrintVoucher.listIDNotNull'));
        }
        this.strCodePrint = await this.inChungTuHangLoatService.inChungTuHangLoat({
            typeGroupID: this.typeGroup,
            typeReport: this.valueVoucher,
            listID,
            fromDate: moment(this.fromDate, DATE_FORMAT),
            toDate: moment(this.toDate, DATE_FORMAT),
            paramCheckAll: this.paramCheckAll
        });
        this.modalRef = this.refModalService.open(
            null,
            CodePrintComponent,
            null,
            true,
            null,
            false,
            null,
            null,
            null,
            true,
            this.strCodePrint,
            true
        );
    }

    close() {
        this.activeModal.dismiss(false);
    }

    getCurrentDate() {
        const _date = moment();
        return { year: _date.year(), month: _date.month() + 1, day: _date.date() };
    }

    // nếu là loại chứng từ thì chỉ cho chọn 1 giá trị có cùng type group, bỏ các giá trị khác
    changeTypeSearch(viewVoucher = null) {
        // if (this.viewVouchers && this.viewVouchers.length) {
        //     if (viewVoucher.typeGroupID !== this.viewVouchers[0].typeGroupID) {
        //         this.viewVouchers.forEach(item => {
        //             item.checked = !(!viewVoucher || item.no !== viewVoucher.no);
        //         });
        //     }
        // }

        if (this.newList && this.newList.length) {
            if (viewVoucher && viewVoucher.typeGroupID !== this.newList[0].typeGroupID) {
                this.newList = [];
                this.viewVouchers.forEach(item => {
                    item.checked = !(!viewVoucher || item.no !== viewVoucher.no);
                });
            }
        }
    }

    selectChangeBeginDateAndEndDate(intTimeLine: String) {
        if (intTimeLine) {
            this.objTimeLine = this.utilsService.getTimeLine(intTimeLine, this.account);
            this.fromDate = moment(this.objTimeLine.dtBeginDate).format('YYYYMMDD');
            this.toDate = moment(this.objTimeLine.dtEndDate).format('YYYYMMDD');
        }
    }

    view(voucher, $event) {
        $event.stopPropagation();
        let url = '';
        // console.log(voucher.typeGroupID);
        switch (voucher.typeGroupID) {
            // Hàng bán trả lại
            case 33:
                if (this.hasAuthority(ROLE.HangBanTraLai_Xem)) {
                    url = `/#/hang-ban/tra-lai/${voucher.refID2}/edit/from-ref`;
                }
                break;
            // Giảm giá hàng bán
            case 34:
                if (this.hasAuthority(ROLE.HangBanGiamGia_Xem)) {
                    url = `/#/hang-ban/giam-gia/${voucher.refID2}/edit/from-ref`;
                }
                break;
            // Xuất hóa đơn
            case 35:
                if (this.hasAuthority(ROLE.XuatHoaDon_Xem)) {
                    url = `/#/xuat-hoa-don/${voucher.refID2}/edit/from-ref`;
                }
                break;
            case 22:
                if (this.hasAuthority(ROLE.HangMuaTraLai_Xem)) {
                    url = `/#/hang-mua/tra-lai/${voucher.refID2}/view`;
                }
                break;
            case 23:
                if (this.hasAuthority(ROLE.HangMuaGiamGia_Xem)) {
                    url = `/#/hang-mua/giam-gia/${voucher.refID2}/view`;
                }
                break;
            case 10:
                if (this.hasAuthority(ROLE.PhieuThu_Xem)) {
                    url = `/#/mc-receipt/${voucher.refID2}/edit/from-ref`;
                }
                break;
            case 16:
                if (this.hasAuthority(ROLE.BaoCo_Xem)) {
                    url = `/#/bao-co/${voucher.refID2}/edit/from-ref`;
                }
                break;
            case 17:
                if (this.hasAuthority(ROLE.TheTinDung_Xem)) {
                    url = `/#/the-tin-dung/${voucher.refID2}/edit/from-ref`;
                }
                break;
            case 70:
                if (this.hasAuthority(ROLE.ChungTuNghiepVuKhac_Xem)) {
                    url = `/#/chung-tu-nghiep-vu-khac/${voucher.refID2}/edit/from-ref`;
                }
                break;
            case 11:
                if (this.hasAuthority(ROLE.PhieuChi_Xem)) {
                    url = `/#/mc-payment/${voucher.refID2}/edit/from-ref`;
                }
                break;
            case 31:
                if (this.hasAuthority(ROLE.DonDatHang_Xem)) {
                    url = `/#/don-dat-hang/${voucher.refID2}/edit/from-ref`;
                }
                break;
            case 24:
                if (this.hasAuthority(ROLE.MuaDichVu_Xem)) {
                    url = `/#/mua-dich-vu/${voucher.refID2}/edit/from-ref`;
                }
                break;
            case 40:
                if (this.hasAuthority(ROLE.NhapKho_XEM)) {
                    url = `/#/nhap-kho/${voucher.refID2}/edit/from-ref`;
                }
                break;
            case 20:
                if (this.hasAuthority(ROLE.DonMuaHang_XEM)) {
                    url = `/#/don-mua-hang/${voucher.refID2}/edit/from-ref`;
                }
                break;
            case 41:
                if (this.hasAuthority(ROLE.XuatKho_XEM)) {
                    url = `/#/xuat-kho/${voucher.refID2}/edit/from-ref`;
                }
                break;
            case 21:
                this.viewVoucherService.checkViaStockPPInvoice({ id: voucher.refID2 }).subscribe(
                    (res: HttpResponse<any>) => {
                        if (res.body.message === UpdateDataMessages.STOCK_TRUE) {
                            if (this.hasAuthority(ROLE.MuaHangQuaKho_Xem)) {
                                url = `/#/mua-hang/qua-kho/${voucher.refID2}/edit/1`;
                                window.open(url, '_blank');
                            }
                        } else if (res.body.message === UpdateDataMessages.STOCK_FALSE) {
                            if (this.hasAuthority(ROLE.MuaHangQuaKho_Xem)) {
                                url = `/#/mua-hang/khong-qua-kho/${voucher.refID2}/edit/1`;
                                window.open(url, '_blank');
                            }
                        } else {
                            // else if (res.body.message === UpdateDataMessages.STOCK_FALSE) {
                            //     if (this.hasAuthority(ROLE.MuaHangKhongQuaKho_Xem)) {
                            //         url = `/#/mua-hang/khong-qua-kho/${voucher.refID2}/edit/1`;
                            //         window.open(url, '_blank');
                            //     }
                            // }
                            this.toastrService.error(this.translateService.instant('ebwebApp.pPInvoice.error.default'));
                        }
                        return;
                    },
                    (res: HttpErrorResponse) => {
                        this.toastrService.error(this.translateService.instant('ebwebApp.pPInvoice.error.default'));
                    }
                );
                return;
            case 18:
                if (this.hasAuthority(ROLE.KiemKeQuy_Xem)) {
                    url = `/#/mc-audit/${voucher.refID2}/edit/from-ref`;
                }
                break;
            case 32:
                if (this.hasAuthority(ROLE.ChungTuBanHang_Xem)) {
                    url = `/#/chung-tu-ban-hang/${voucher.refID2}/edit/from-ref`;
                }
                break;
            case 30:
                if (this.hasAuthority(ROLE.BaoGia_Xem)) {
                    url = `/#/sa-quote/${voucher.refID2}/edit/from-ref`;
                }
                break;
            case 12:
            case 13:
            case 14:
                if (this.hasAuthority(ROLE.BaoNo_Xem)) {
                    url = `/#/bao-no/${voucher.refID2}/edit/from-ref`;
                }
                break;
            case 42:
                if (this.hasAuthority(ROLE.ChuyenKho_XEM)) {
                    url = `/#/chuyen-kho/${voucher.refID2}/edit/from-ref`;
                }
                break;
        }
        if (url) {
            window.open(url, '_blank');
        }
    }

    private hasAuthority(auth): boolean {
        if (this.account.authorities.includes(ROLE.ROLE_ADMIN) || this.account.authorities.includes(auth)) {
            return true;
        }
        this.toastrService.error(this.translateService.instant('ebwebApp.quyTrinh.notHasAccess'));
        return false;
    }

    newArr(length: number): any[] {
        if (length > 0) {
            return new Array(length);
        } else {
            return new Array(0);
        }
    }

    selectChangeTypeGroup() {
        this.listOnSelect = [];
        this.paramCheckAll = false;
        if (this.listType) {
            if (!this.typeGroup) {
                this.viewVouchers = [];
            }
            this.listTypePrintForm = this.listType.filter(a => a.typeGroup === this.typeGroup);
        }
        this.valueVoucher = null;
        this.autoSearch();
    }

    selectChangePrintForm() {
        this.listOnSelect = [];
        this.autoSearch();
    }

    check(viewVoucher: any) {
        viewVoucher.checked = !viewVoucher.checked;
        if (this.paramCheckAll) {
            if (!viewVoucher.checked) {
                this.listOnSelect.push(viewVoucher);
            } else {
                for (let i = 0; i < this.listOnSelect.length; i++) {
                    if (this.listOnSelect[i].refID2 === viewVoucher.refID2) {
                        this.listOnSelect.splice(i, 1);
                        i--;
                    }
                }
            }
        } else {
            if (viewVoucher.checked) {
                this.listOnSelect.push(viewVoucher);
            } else {
                for (let i = 0; i < this.listOnSelect.length; i++) {
                    if (this.listOnSelect[i].refID2 === viewVoucher.refID2) {
                        this.listOnSelect.splice(i, 1);
                        i--;
                    }
                }
            }
        }
        if (this.listOnSelect.length === this.totalItems && !this.paramCheckAll) {
            this.paramCheckAll = true;
            this.listOnSelect = [];
        }
    }

    getNumberSelect() {
        if (this.paramCheckAll) {
            return this.totalItems - this.listOnSelect.length;
        } else {
            return this.listOnSelect.length;
        }
    }

    checkAll() {
        if (this.paramCheckAll && this.listOnSelect.length > 0) {
            this.paramCheckAll = true;
        } else {
            this.paramCheckAll = !this.paramCheckAll;
        }
        if (this.paramCheckAll) {
            // this.totalSelect = this.totalItems;
            this.viewVouchers.forEach(n => (n.checked = true));
            this.listOnSelect = [];
        } else {
            // this.totalSelect = 0;
            this.viewVouchers.forEach(n => (n.checked = false));
            this.listOnSelect = [];
        }
    }

    isCheckAll() {
        if (this.viewVouchers) {
            return this.paramCheckAll && this.totalItems - this.listOnSelect.length === this.totalItems;
        } else {
            return false;
        }
    }

    getTypeName(id) {
        if (this.types) {
            const type = this.types.find(a => a.id === id);
            if (type) {
                return type.typeName;
            } else {
                return '';
            }
        }
    }

    test() {}
}
