import { Directive, ElementRef, HostListener, Input } from '@angular/core';
@Directive({
    selector: '[ebUpperCase]'
})
export class UppercaseInputDirective {
    @Input() options: any;
    lastValue: string;

    constructor(public ref: ElementRef) {}

    @HostListener('input', ['$event'])
    onInput($event) {
        const start = $event.target.selectionStart;
        const end = $event.target.selectionEnd;
        if (this.options === 1) {
            if ($event.target.maxLength >= this.slugify($event.target.value).length) {
                $event.target.value = this.slugify($event.target.value).toUpperCase();
            }
        } else {
            $event.target.value = $event.target.value.toUpperCase();
        }
        $event.target.setSelectionRange(start, end);
        $event.preventDefault();

        if (!this.lastValue || (this.lastValue && $event.target.value.length > 0 && this.lastValue !== $event.target.value)) {
            this.lastValue = this.ref.nativeElement.value = $event.target.value;
            // Propagation
            const evt = document.createEvent('HTMLEvents');
            evt.initEvent('input', false, true);
            event.target.dispatchEvent(evt);
        }
    }

    private slugify(str): string {
        const map = {
            '-': ' ',
            a: 'á|à|ã|À|Á|Ã',
            aa: 'â|Â',
            e: 'é|è|ê|É|È|Ê',
            i: 'í|ì|î|Í|Ì|Î',
            o: 'ó|ò|õ|Ó|Ò|Ô|Õ',
            oo: 'ô',
            ow: 'ơ',
            u: 'ú|ù|û|ü|Ú|Ù|Û|Ü|Ư',
            c: 'ç|Ç',
            n: 'ñ|Ñ'
        };
        // for (const pattern in Object.keys(map)) {
        //     str = str.replace(new RegExp(map[pattern], 'g'), pattern);
        // }
        Object.keys(map).forEach(pattern => {
            str = str.replace(new RegExp(map[pattern], 'g'), pattern);
        });
        return str;
    }
}
