import { Irecord } from 'app/shared/models/record';

export class RequestRecordListDtoModel {
    constructor(public records?: Irecord[], public typeIDMain?: number, public kho?: boolean) {}
}
