import { Moment } from 'moment';

export interface IHistorySalaryPayment {
    refID2?: string;
    employeeID?: string;
    postedDate?: Moment;
    date?: Moment;
    no?: string;
    reason?: string;
    accumAmount?: number;
    payAmount?: number;
    remainingAmount?: number;
    typeID?: number;
    typeGroupID?: number;
}

export class HistorySalaryPayment implements IHistorySalaryPayment {
    constructor(
        public refID2?: string,
        public employeeID?: string,
        public postedDate?: Moment,
        public date?: Moment,
        public no?: string,
        public reason?: string,
        public accumAmount?: number,
        public payAmount?: number,
        public remainingAmount?: number,
        public typeID?: number,
        public typeGroupID?: number
    ) {}
}
