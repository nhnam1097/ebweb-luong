import { Moment } from 'moment';
import { IGOtherVoucherDetails } from 'app/shared/models/g-other-voucher-details.model';
import { IGOtherVoucherDetailTax } from 'app/shared/models/g-other-voucher-detail-tax.model';
import { IGOtherVoucherDetailExpense } from 'app/shared/models/g-other-voucher-detail-expense.model';
import { IGOtherVoucherDetailExpenseAllocation } from 'app/shared/models/g-other-voucher-detail-expense-allocation.model';
import { IViewVoucher } from 'app/shared/models/view-voucher.model';
import { IRefVoucher } from 'app/shared/models/ref-voucher.model';

export interface IGOtherVoucher {
    id?: string;
    companyID?: string;
    branchID?: string;
    typeID?: number;
    currencyID?: string;
    exchangeRate?: number;
    typeLedger?: number;
    reason?: string;
    noMBook?: string;
    noFBook?: string;
    date?: Moment;
    postedDate?: Moment;
    totalAmount?: number;
    totalAmountOriginal?: number;
    recorded?: boolean;
    gOtherVoucherDetails?: IGOtherVoucherDetails[];
    gOtherVoucherDetailTax?: IGOtherVoucherDetailTax[];
    gOtherVoucherDetailExpenses?: IGOtherVoucherDetailExpense[];
    gOtherVoucherDetailExpenseAllocations?: IGOtherVoucherDetailExpenseAllocation[];
    gOtherVoucherDetailDebtPayments?: any[];
    gOtherVoucherDetailDebtPaymentsDTO?: any[];
    gOtherVoucherDetailForeignCurrencies?: any[];
    gOtherVoucherDetailForeignCurrenciesDTO?: any[];
    gOtherVoucherDetailExcepts?: any[];
    gOtherVoucherDetailExceptsDTO?: any[];
    viewVouchers?: IViewVoucher[];
    templateID?: string;
    no?: string;
    typeGroup?: number;
    refVouchers?: IRefVoucher[];
    currentBook?: string;
    noBook?: any;
    total?: number;
    sumAmount?: any;
    cPPeriodID?: any;
    sumTotalAmount?: number;
    pSSalarySheetID?: string;
    exceptVouchers?: any;
    customField1?: string;
    customField2?: string;
    customField3?: string;
    customField4?: string;
    customField5?: string;
    billReceived?: boolean;
}

export class GOtherVoucher implements IGOtherVoucher {
    constructor(
        public id?: string,
        public companyID?: string,
        public branchID?: string,
        public typeID?: number,
        public currencyID?: string,
        public exchangeRate?: number,
        public typeLedger?: number,
        public reason?: string,
        public noMBook?: string,
        public noFBook?: string,
        public date?: Moment,
        public postedDate?: Moment,
        public totalAmount?: number,
        public totalAmountOriginal?: number,
        public recorded?: boolean,
        public gOtherVoucherDetails?: IGOtherVoucherDetails[],
        public gOtherVoucherDetailTax?: IGOtherVoucherDetailTax[],
        public gOtherVoucherDetailExpenses?: IGOtherVoucherDetailExpense[],
        public gOtherVoucherDetailExpenseAllocations?: IGOtherVoucherDetailExpenseAllocation[],
        public gOtherVoucherDetailDebtPayments?: any[],
        public gOtherVoucherDetailDebtPaymentsDTO?: any[],
        public gOtherVoucherDetailForeignCurrencies?: any[],
        public gOtherVoucherDetailForeignCurrenciesDTO?: any[],
        public gOtherVoucherDetailExcepts?: any[],
        public gOtherVoucherDetailExceptsDTO?: any[],
        public viewVouchers?: IViewVoucher[],
        public templateID?: string,
        public no?: string,
        public typeGroup?: number,
        public refVouchers?: IRefVoucher[],
        public noBook?: any,
        public total?: number,
        public sumAmount?: any,
        public cPPeriodID?: any,
        public sumTotalAmount?: number,
        public pSSalarySheetID?: string,
        public exceptVouchers?: any,
        public customField1?: string,
        public customField2?: string,
        public customField3?: string,
        public customField4?: string,
        public customField5?: string,
        public billReceived?: boolean
    ) {
        this.recorded = this.recorded || false;
    }
}
