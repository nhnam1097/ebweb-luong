import { IAccountingObject } from 'app/shared/models/accounting-object.model';
import { IOrganizationUnit } from 'app/shared/models/organization-unit.model';

export interface IPSTimeSheetSummaryDetails {
    id?: string;
    pSTimeSheetSummaryID?: string;
    employeeID?: string;
    accountingObjectName?: string;
    accountingObjectTitle?: string;
    departmentID?: string;
    workAllDay?: number;
    workHalfAday?: number;
    totalOverTime?: number;
    total?: number;
    orderPriority?: number;
    listEmployeeID?: IAccountingObject[];
    stt?: number;
}

export class PSTimeSheetSummaryDetails implements IPSTimeSheetSummaryDetails {
    constructor(
        public id?: string,
        public pSTimeSheetSummaryID?: string,
        public employeeID?: string,
        public accountingObjectName?: string,
        public accountingObjectTitle?: string,
        public departmentID?: string,
        public workAllDay?: number,
        public workHalfAday?: number,
        public totalOverTime?: number,
        public total?: number,
        public orderPriority?: number,
        public listEmployeeID?: IAccountingObject[],
        public stt?: number
    ) {}
}

export class TongHopChamCongExportExcel {
    private stt: number;
    private department: string;
    private employeeCode: string;
    private employeeName: string;
    private workAllDay: number;
    private workHalfAday: number;
    private totalOverTime: number;
    private total: number;

    constructor() {}

    public setAllFieldsFromPSTimeSummarySheetDetails(
        psTimeSummarySheetDetails: PSTimeSheetSummaryDetails,
        organizationUnits: IOrganizationUnit[]
    ): void {
        let department = '';
        const departmentId: string = psTimeSummarySheetDetails.departmentID ? psTimeSummarySheetDetails.departmentID : '';
        for (let i = 0; i < organizationUnits.length; i++) {
            if (departmentId != null && departmentId === organizationUnits[i].id) {
                department = organizationUnits[i].organizationUnitCode;
                break;
            }
        }
        let employeeCode = '';
        const employeeID: string = psTimeSummarySheetDetails.employeeID ? psTimeSummarySheetDetails.employeeID : '';
        for (let i = 0; i < psTimeSummarySheetDetails.listEmployeeID.length; i++) {
            if (employeeID != null && employeeID === psTimeSummarySheetDetails.listEmployeeID[i].id) {
                employeeCode = psTimeSummarySheetDetails.listEmployeeID[i].accountingObjectCode;
                break;
            }
        }
        this.stt = psTimeSummarySheetDetails.stt + 1;
        this.employeeCode = employeeCode;
        this.employeeName = psTimeSummarySheetDetails.accountingObjectName ? psTimeSummarySheetDetails.accountingObjectName : '';
        this.department = department;
        this.workHalfAday = psTimeSummarySheetDetails.workHalfAday ? psTimeSummarySheetDetails.workHalfAday : 0;
        this.workAllDay = psTimeSummarySheetDetails.workAllDay ? psTimeSummarySheetDetails.workAllDay : 0;
        this.totalOverTime = psTimeSummarySheetDetails.totalOverTime ? psTimeSummarySheetDetails.totalOverTime : 0;
        this.total = psTimeSummarySheetDetails.total ? psTimeSummarySheetDetails.total : 0;
    }
}
