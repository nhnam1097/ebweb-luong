import { Moment } from 'moment';

export interface IRequestReport {
    typeReport?: string;
    bill?: boolean;
    fromDate?: Moment; // từ ngày
    toDate?: Moment; // đến ngày
    monthAndQuarter?: any;
    month?: any;
    quarter?: any;
    year?: any;
    groupTheSameItem?: boolean; // Cộng gộp các bút toán giống nhau
    showAccumAmount?: boolean;
    similarSum?: boolean;
    onlyDisplayDifferent?: boolean;
    getINTranferHaveDebitLikeCredit?: boolean;
    companyID?: string;
    id?: string;
    option?: boolean;
    unitType?: number;
    repositoryID?: string;
    grade?: number;
    listMaterialGoods?: any[];
    listMaterialGoodsNew?: any[];
    listRSProductionOrderID?: any[];
    listCostSets?: any[];
    listEMContracts?: any[];
    eMContractList?: any[];
    listIDEMContracts?: any[];
    listCostSetID?: any[];
    listExpenseItems?: any[];
    listGvoucherList?: any[];
    typeGroupID?: string;
    totalItems?: number;
    pageCount?: number;
    previousPage?: any;
    accountingObjects?: any[];
    currencyID?: string;
    accountNumber?: string;
    accountName?: string;
    bankAccountDetail?: string;
    accountList?: any[];
    accountListObj?: any[];
    listAccounts?: any[];
    typeGroupList?: any[];
    voucherList?: any[];
    createDate?: string;
    bankAccountDetailID?: string;
    materialGoodsCategoryID?: string;
    timeLineVoucher?: string;
    employeeID?: string;
    fileName?: string;
    tools?: any[];
    departments?: any[];
    typeShowCurrency?: boolean;
    getAmountOriginal?: boolean;
    cPPeriodID?: string;
    typeMethod?: number;
    statisticsCodes?: any[];
    expenseItems?: any[];
    typeGroups?: any[];
    materialGoodsID?: string;
    listRepository?: any[];
    status?: number;
    isCheckAll?: boolean;
    checkALL?: boolean;
    groupID?: any;
    acNameFilter?: any;
    acCodeFilter?: any;
    acAddressFilter?: any;
    objectType?: boolean;
    departmentID?: any;
    fixedAssetCategoryID?: any;
    mCodeFilter?: string;
    mNameFilter?: string;
    rCodeFilter?: string;
    rNameFilter?: string;
    empCodeFilter?: string;
    empNameFilter?: string;
    departmentCodeFilter?: string;
    token?: any;
    currentBook?: any;
    optionXML?: any;
    mDateFilter?: string;
    mNoFilter?: string;
    fromDateFilter?: string;
    toDateFilter?: string;
    listID?: string[];
    mauGop?: boolean;
    searchDate?: string;
    checkCbb?: number;
    typeGroupBy?: any;
    repositoryName?: string;
    isCustomForm?: boolean;
    isOnlyGetData?: boolean;
    orgs?: any;
    listQuantums?: any;
    isNewDTO?: any;
}

export class RequestReport implements IRequestReport {
    constructor(
        public typeReport?: string,
        public bill?: boolean,
        public fromDate?: Moment, // từ ngày
        public toDate?: Moment, // đến ngày
        public monthAndQuarter?: any,
        public month?: any,
        public quarter?: any,
        public year?: any,
        public groupTheSameItem?: boolean, // Cộng gộp các bút toán giống nhau
        public showAccumAmount?: boolean,
        public similarSum?: boolean,
        public dependent?: boolean,
        public onlyDisplayDifferent?: boolean,
        public getINTranferHaveDebitLikeCredit?: boolean,
        public companyID?: string,
        public id?: string,
        public option?: boolean,
        public unitType?: number,
        public status?: number,
        public repositoryID?: string,
        public grade?: number,
        public listMaterialGoods?: any[],
        public listMaterialGoodsNew?: any[],
        public listRSProductionOrderID?: any[],
        public listRepository?: any[],
        public listEMContracts?: any[],
        public eMContractList?: any[],
        public listIDEMContracts?: any[],
        public listCostSetID?: any[],
        public listCostSets?: any[],
        public listExpenseItems?: any[],
        public listGvoucherList?: any[],
        public typeGroupID?: string,
        public totalItems?: number,
        public pageCount?: number,
        public previousPage?: any,
        public accountingObjects?: any[],
        public currencyID?: string,
        public accountNumber?: string,
        public accountName?: string,
        public bankAccountDetail?: string,
        public accountList?: any[],
        public accountListObj?: any[],
        public listAccounts?: any[],
        public typeGroupList?: any[],
        public voucherList?: any[],
        public createDate?: string,
        public bankAccountDetailID?: string,
        public materialGoodsCategoryID?: string,
        public timeLineVoucher?: string,
        public employeeID?: string,
        public fileName?: string,
        public tools?: any[],
        public departments?: any[],
        public typeShowCurrency?: boolean,
        public getAmountOriginal?: boolean,
        public cPPeriodID?: string,
        public typeMethod?: number,
        public statisticsCodes?: any[],
        public expenseItems?: any[],
        public isCheckAll?: boolean,
        public materialGoodsID?: string,
        public checkALL?: boolean,
        public groupID?: any,
        public typeGroupNameFilter?: string,
        public debitAccountFilter?: string,
        public creditAccountFilter?: string,
        public acNameFilter?: any,
        public acCodeFilter?: any,
        public acAddressFilter?: any,
        public objectType?: boolean,
        public typeGroups?: any[],
        public departmentID?: any,
        public fixedAssetCategoryID?: any,
        public mCodeFilter?: string,
        public mNameFilter?: string,
        public rCodeFilter?: string,
        public rNameFilter?: string,
        public empCodeFilter?: string,
        public empNameFilter?: string,
        public no?: string,
        public date?: string,
        public description?: string,
        public departmentCodeFilter?: string,
        public token?: any,
        public currentBook?: any,
        public optionXML?: any,
        public listID?: string[],
        public accountingGroupID?: string,
        public mDateFilter?: string,
        public mNoFilter?: string,
        public fromDateFilter?: string,
        public toDateFilter?: string,
        public mauGop?: boolean,
        public searchDate?: string,
        public checkCbb?: number,
        public typeGroupBy?: any,
        public repositoryName?: string,
        public isCustomForm?: boolean,
        public typeStatistical?: number,
        public isOnlyGetData?: boolean,
        public orgs?: any,
        public listQuantums?: any,
        public isNewDTO?: any
    ) {}
}
