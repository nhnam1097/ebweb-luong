import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Principal, UserService } from 'app/core';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { JhiEventManager } from 'ng-jhipster';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { HttpResponse } from '@angular/common/http';
import { UtilsService } from 'app/shared/UtilsService/Utils.service';
import { OrganizationUnitService } from 'app/shared/services/organization-unit.service';
import { NotificationService } from 'app/shared/notification';

@Component({
    selector: 'eb-thong-tin-dich-vu',
    templateUrl: './thong-tin-dich-vu.component.html',
    styleUrls: ['thong-tin-dich-vu.component.css']
})
export class ThongTinDichVuComponent implements OnInit, OnDestroy {
    @ViewChild('content') public modalCloseComponent: NgbModalRef;
    @ViewChild('popupAlert') public modalAlertComponent: NgbModalRef;
    modalRef: NgbModalRef;
    modalConfirm: NgbModalRef;
    orgUnits: any[];
    currentAccount: any;
    packageCode: any;
    packageName: any;
    payment: any;
    expiredDate: any;
    remainDays: any;
    isPayed: boolean;
    isPackageNoLimitTime: boolean;
    isPackageNoLimitVoucher: boolean;
    user: any;
    isPackageNoLimitUser: boolean;
    countUser: number;
    contracts: any[];
    createDate: any;

    constructor(
        public activeModal: NgbActiveModal,
        private principal: Principal,
        private toastr: ToastrService,
        public translate: TranslateService,
        public eventManager: JhiEventManager,
        public utilsService: UtilsService,
        private modalService: NgbModal,
        private organizationUnitService: OrganizationUnitService,
        private userService: UserService,
        private router: Router,
        private notificationService: NotificationService
    ) {}

    ngOnInit(): void {
        sessionStorage.setItem('noUseTabEnter', 'noUseTabEnter');
        this.contracts = [];
        this.principal.identity().then(account => {
            this.currentAccount = account;
            this.loadInfo();
            // this.loadContracts();
            this.loadAll();
        });
    }

    loadContracts() {
        this.userService.getContracts().subscribe(res => {
            if (res.status === 0) {
                this.toastr.error(this.translate.instant('thongTinDichVu.error.getContractFailed'));
            } else if (res.status === 1) {
                this.contracts = res.data;
            }
        });
    }

    loadInfo() {
        this.packageCode = this.currentAccount.ebPackage.packageCode;
        this.packageName = this.currentAccount.ebPackage.description;
        this.createDate = moment(this.currentAccount.ebUserPackage.createdDate).format('DD/MM/YYYY');
        this.isPayed =
            this.currentAccount.ebUserPackage.packCount > 0 && !this.currentAccount.ebPackage.packageCode.toLowerCase().includes('demo');
        this.translate.get(['thongTinDichVu.home.payed', 'thongTinDichVu.home.notPayed']).subscribe(res => {
            this.payment = this.isPayed
                ? this.translate.instant('thongTinDichVu.home.payed', { param: this.currentAccount.ebUserPackage.packCount })
                : this.translate.instant('thongTinDichVu.home.notPayed');
        });
        this.isPackageNoLimitTime =
            this.currentAccount.ebUserPackage.expriredDate === -1 ||
            !this.currentAccount.ebUserPackage.expriredDate ||
            this.currentAccount.ebPackage.packageCode.toLowerCase().includes('demo');
        this.expiredDate = this.isPackageNoLimitTime
            ? 'Không giới hạn thời gian'
            : 'Từ ' +
              moment(this.currentAccount.ebUserPackage.activedDate).format('DD/MM/YYYY') +
              ' đến ' +
              moment(this.currentAccount.ebUserPackage.expriredDate).format('DD/MM/YYYY');
        if (this.currentAccount.ebUserPackage.expriredDate) {
            const endD = moment(this.currentAccount.ebUserPackage.expriredDate);
            const nowD = moment(Date.now());
            if (nowD.isSameOrBefore(endD, 'day')) {
                const expiredDate: any = !this.isPackageNoLimitTime
                    ? moment(this.currentAccount.ebUserPackage.expriredDate).toDate()
                    : moment(Date.now()).toDate();
                const now: any = moment(Date.now()).toDate();
                now.setHours(0);
                now.setMinutes(0);
                now.setSeconds(0);
                const oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
                const diffDays = Math.round(Math.abs((expiredDate - now) / oneDay));
                this.remainDays = !this.isPackageNoLimitTime ? diffDays + 1 : null;
            } else {
                this.remainDays = 0;
            }
        } else {
            this.remainDays = 0;
        }
        this.isPackageNoLimitVoucher =
            this.currentAccount.ebPackage.limitedVoucher === -1 ||
            !this.currentAccount.ebPackage.limitedVoucher ||
            this.currentAccount.ebPackage.packageCode.toLowerCase().includes('demo');
        this.isPackageNoLimitUser =
            this.currentAccount.ebPackage.limitedUser === -1 ||
            !this.currentAccount.ebPackage.limitedUser ||
            this.currentAccount.ebPackage.packageCode.toLowerCase().includes('demo');
        this.userService.countUserOfAdmin().subscribe(res => {
            this.countUser = res;
            this.translate.get(['thongTinDichVu.home.userLimit', 'thongTinDichVu.home.userNoLimit']).subscribe(resD => {
                this.user = !this.isPackageNoLimitUser
                    ? this.translate.instant('thongTinDichVu.home.userLimit', {
                          count: this.countUser,
                          total: this.currentAccount.ebPackage.limitedUser
                      })
                    : this.translate.instant('thongTinDichVu.home.userNoLimit');
            });
        });
    }

    loadAll() {
        this.organizationUnitService.getOrganizationUnitInfoTrees().subscribe(res => {
            this.orgUnits = res.body;
        });
    }

    newArr(length: number): any[] {
        if (length > 0) {
            return new Array(length);
        } else {
            return new Array(0);
        }
    }

    getUnitType(unitType: number): string {
        if (unitType === 0) {
            return 'Tổng công ty/Công ty';
        } else if (unitType === 1) {
            return 'Chi nhánh';
        }
    }

    ngOnDestroy() {
        // this.routeData.unsubscribe();
        sessionStorage.removeItem('noUseTabEnter');
    }

    apply() {
        this.userService.sendNotifyCRM(this.currentAccount.ebUserPackage).subscribe(
            res => {
                if (res.body) {
                    this.modalConfirm = this.modalService.open(this.modalAlertComponent, {
                        backdrop: 'static',
                        size: 'lg'
                    });
                } else {
                    this.toastr.error(this.translate.instant('thongTinDichVu.error.sendNotifyFailed'));
                }
            },
            error => {
                this.toastr.error(this.translate.instant('thongTinDichVu.error.sendNotifyFailed'));
            }
        );
        this.modalRef.close();
    }

    closeModal() {
        this.modalRef.close();
    }

    close() {
        this.modalConfirm.close();
    }

    requestConfirm() {
        this.modalRef = this.modalService.open(this.modalCloseComponent, { backdrop: 'static' });
    }

    sum(prop) {
        let total = 0;
        if (this.orgUnits) {
            this.orgUnits.forEach(org => {
                total += org[prop];
                if (org.children && org.children.length > 0) {
                    org.children.forEach(child => {
                        total += child[prop];
                    });
                }
            });
        }
        return isNaN(total) ? 0 : total;
    }

    navigateHelp() {
        this.notificationService.findHelpLink({ id: 'TTDV' }).subscribe((res: HttpResponse<any>) => {
            window.open(res.body, '_blank');
        });
    }
}
