import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import {INotification} from 'app/shared/models/notification.model';

type EntityResponseType = HttpResponse<INotification>;
type EntityArrayResponseType = HttpResponse<INotification[]>;

@Injectable({ providedIn: 'root' })
export class NotificationService {
    private resourceUrl = SERVER_API_URL + 'api/notifications';

    constructor(private http: HttpClient) {}

    create(notification: INotification): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(notification);
        return this.http
            .post<INotification>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    update(notification: INotification): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(notification);
        return this.http
            .put<INotification>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<INotification>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<INotification[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    private convertDateFromClient(notification: INotification): INotification {
        const copy: INotification = Object.assign({}, notification, {
            startDate:
                notification.startDate != null && notification.startDate.isValid() ? notification.startDate.format(DATE_FORMAT) : null,
            endDate: notification.endDate != null && notification.endDate.isValid() ? notification.endDate.format(DATE_FORMAT) : null
        });
        return copy;
    }

    private convertDateFromServer(res: EntityResponseType): EntityResponseType {
        res.body.startDate = res.body.startDate != null ? moment(res.body.startDate) : null;
        res.body.endDate = res.body.endDate != null ? moment(res.body.endDate) : null;
        return res;
    }

    private convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        res.body.forEach((notification: INotification) => {
            notification.startDate = notification.startDate != null ? moment(notification.startDate) : null;
            notification.endDate = notification.endDate != null ? moment(notification.endDate) : null;
        });
        return res;
    }

    getNotificationsByUser(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<INotification[]>(this.resourceUrl + '/find-all-notification-by-user', {
            observe: 'response',
            params: options
        });
    }

    updateStatusNoti(req?: any): Observable<HttpResponse<any>> {
        const options = createRequestOption(req);
        let headers = new HttpHeaders();
        headers = headers.set('Accept', 'image/png');
        return this.http.get(this.resourceUrl + '/update-status-noti', {
            observe: 'response',
            params: options,
            headers,
            responseType: 'blob'
        });
    }

    getHelpTable(): Observable<EntityArrayResponseType> {
        return this.http.get<any[]>(this.resourceUrl + '/get-help-table', { observe: 'response' });
    }

    findHelpLink(req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/find-link-help', { params: options, observe: 'response', responseType: 'text' });
    }
}
