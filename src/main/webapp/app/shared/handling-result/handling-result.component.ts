import * as moment from 'moment';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Principal } from 'app/core';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { JhiEventManager } from 'ng-jhipster';
import { DBDateClosed, DBDateClosedOld, MSGERROR, SO_LAM_VIEC, TypeID } from 'app/app.constants';
import { DATE_FORMAT } from 'app/shared';
import { Moment } from 'moment';
import { Component, OnInit } from '@angular/core';
import {HandlingResult} from 'app/shared/handling-result/handling-result.model';

@Component({
    selector: 'eb-handling-result',
    templateUrl: 'handling-result.component.html',
    styleUrls: ['handling-result.component.css']
})
export class HandlingResultComponent implements OnInit {
    data: HandlingResult;
    modalData: any[];
    typeID: number;
    listIDRecorded: string[];
    listIDUnrecorded: string[];
    countTotalVouchers: number;
    countSuccessVouchers: number;
    countFailVouchers: number;
    dateLockNow: Moment;
    dateLockNew: Moment;
    account: any;
    isSoTaiChinh: boolean;
    XUAT_HOA_DON = TypeID.XUAT_HOA_DON;
    BANG_LUONG = TypeID.BANG_LUONG;
    KET_QUA_TINH_GIA_THANH = TypeID.KET_QUA_TINH_GIA_THANH;
    HD_BAN = TypeID.HOP_DONG_BAN;
    HD_MUA = TypeID.HOP_DONG_MUA;
    HOA_DON_DAU_VAO = TypeID.HOA_DON_DAU_VAO;
    constructor(
        public activeModal: NgbActiveModal,
        private principal: Principal,
        private toastr: ToastrService,
        public translate: TranslateService,
        public eventManager: JhiEventManager
    ) {}

    ngOnInit(): void {
        this.principal.identity().then(account => {
            this.account = account;
            if (account) {
                const dbDateClosed = this.account.systemOption.find(x => x.code === DBDateClosed).data;
                const dbDateClosedOld = this.account.systemOption.find(x => x.code === DBDateClosedOld).data;
                this.dateLockNow = dbDateClosed ? moment(dbDateClosed, DATE_FORMAT) : null;
                this.dateLockNew = dbDateClosedOld ? moment(dbDateClosedOld, DATE_FORMAT) : null;
                this.isSoTaiChinh = this.account.systemOption.some(x => x.code === SO_LAM_VIEC && x.data === '0');
            }
        });
        if (this.data) {
            this.countTotalVouchers = this.data.countTotalVouchers;
            this.countSuccessVouchers = this.data.countSuccessVouchers;
            this.countFailVouchers = this.data.countFailVouchers;
        }
    }

    trackId(index: number, item: any) {
        return item.id;
    }

    newArr(lenght: number): any[] {
        if (lenght > 0) {
            return new Array(lenght);
        } else {
            return new Array(0);
        }
    }

    getReasonFail(reasonFail) {
        switch (reasonFail) {
            case MSGERROR.XUAT_QUA_TON_QUY_QT:
            case MSGERROR.XUAT_QUA_TON_QUY_TC:
            case MSGERROR.XUAT_QUA_TON_QUY:
                return this.translate.instant('global.messages.error.quaTonQuy');
            case MSGERROR.KC_DU_LIEU_AM:
                return this.translate.instant('global.messages.error.kcDataError');
            default:
                return reasonFail;
        }
    }
}
