import {ViewVoucherNo} from 'app/shared/models/view-voucher-no.model';

export class HandlingResult {
    constructor(
        public countTotalVouchers?: number,
        public countSuccessVouchers?: number,
        public countFailVouchers?: number,
        public listIDFail?: string[],
        public listFail?: ViewVoucherNo[]
    ) {}
}
