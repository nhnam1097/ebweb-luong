import {
    Component,
    ElementRef,
    EventEmitter,
    forwardRef,
    Input,
    OnChanges,
    OnDestroy,
    OnInit,
    Output,
    SimpleChanges,
    ViewChild
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { ComboBoxPipe } from './combo-box.pipe';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { isNumber } from 'util';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';
import { DATE_FORMAT, ITEMS_PER_PAGE } from 'app/shared';
import { HttpResponse } from '@angular/common/http';
import { CategoryName } from 'app/app.constants';
import { JhiEventManager, JhiParseLinks } from 'ng-jhipster';
import { Subscription } from 'rxjs';
import {UtilsService} from 'app/shared/UtilsService/Utils.service';
import {IAccountingObject} from 'app/shared/models/accounting-object.model';
import {ComboboxModalService} from 'app/shared/services/combobox-modal.service';

const noop = () => {};

export const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any = {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => ComboBoxComponent),
    multi: true
};

export enum KEY_CODE {
    UP_ARROW = 38,
    ENTER = 13,
    DOWN_ARROW = 40,
    TAB_KEY = 9
}

// @ts-ignore: Unreachable code error
@Component({
    selector: 'combo-box',
    templateUrl: './combo-box.component.html',
    styleUrls: ['./combo-box.component.css'],
    providers: [ComboBoxPipe, CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR]
})
// author : hautv
// -----
export class ComboBoxComponent implements OnInit, ControlValueAccessor, OnChanges, OnDestroy {
    // The internal data model
    private innerValue: any = '';
    @Input() dataList: object[];
    @Input() displayMember: string;
    @Input() idIp: string;
    @Input() nameIp: string;
    @Input() listColumns: string[];
    @Input() headerColumns: string[];
    @Input() headerColumnWidth: string[];
    @Input() isObject: boolean;
    @Input() isSelectDummyList: boolean;
    @Input() valueName: string;
    @Input() isReadOnly: boolean;
    @Input() hiddenHeader: boolean;
    @Input() onSelectedCallback: boolean;
    @Input() isRequired: boolean;
    @Input() isRequiredDuplicate: boolean;
    @Input() hideRequiredWhenValueTrue: boolean;
    @Input() checkCoincide: boolean;
    // css for z-index
    @Input() isMaxWidth: boolean;
    @Input() isCheckVirtualScrollCbb: boolean;
    @Input() isOutTable: boolean;
    @Input() valueIsNumber: boolean;
    @Input() getValueDisplay: boolean;
    @Input() systemOptions: any;
    @Input() options: any;
    @Input() account: any;
    @Input() type: any;
    @Input() allowNegative: boolean;
    @Input() nameCategory: string;
    @Input() showIconPlus: boolean;
    @Input() isCheckEmpty: boolean;
    @Input() tabindex: boolean;
    @Input() autoFillFistMatch: boolean;
    @Input() backgroundInherit: boolean;
    @Input() noEdit: boolean;
    @Input() inPutCurrency: string;
    @Input() hasBoder: boolean;
    @Input() isCostSet: boolean;
    @Input() noGetValueParentNode: boolean;
    @Input() isPopupOverride: boolean;
    @Input() setOpenLeft: boolean;
    // @Output() dataIsNotExist = new EventEmitter<boolean>();
    @Input() isRateType: boolean; // Sử dụng cho các ô %
    @Input() showAsInput: boolean; // hiển thị như ô input không phải cbb
    @Input() stypeForHome: boolean;
    @Input() typeObject: number; // phân biệt cbb đối tượng và nhân viên bên TMNH 0- combobox nhân viên, 1 - combobox đối tượng bất kỳ
    @Output() focusInput = new EventEmitter<any>();
    @Output() afterDataChange = new EventEmitter<any>(); // Add data for combo in display display main
    @Output() blur = new EventEmitter<any>(); // Add data for combo in display display main
    @Input() useVirtualScroll: boolean;
    @Input() option: any;
    // với các màn ccdc và tscd load combobox cần date nên truyền thêm 1 biến date vào combobox
    @Input() date: any;
    @Input() checkGetAll: any;
    @Input() objectHidden: any;
    // Check xem có hiển thị dòng dữ liệu có theo dõi hay không
    @Input() checkActive: any;
    @Input() fromTax: boolean;
    @Input() noSetDisabled: boolean;
    @Input() isReadOnlyForInput: boolean;
    @Input() notSetDisable: boolean;
    @Input() typeCbb: number;
    @Input() unitType: number;
    @Input() defaultValue: any;
    @Input() bold: any;
    @ViewChild('nameOfInputCombobox') nameField: ElementRef; // sử dụng kick chuột trong vùng div mà không có dữ liệu
    @ViewChild('nameOfInputComboboxNumber') nameField_Number: ElementRef; // sử dụng kick chuột trong vùng div mà không có dữ liệu
    @ViewChild('nameOfInputComboboxNumberIsRateType') nameField_NumberIsRateType: ElementRef; // sử dụng kick chuột trong vùng div mà không có dữ liệu

    @Input() textCenter: boolean;
    @Input() textLeft: boolean;
    @Input() textRight: boolean;
    lstLoadStream: string[] = [
        CategoryName.KHACH_HANG,
        CategoryName.DOI_TUONG,
        CategoryName.NHA_CUNG_CAP,
        CategoryName.NHAN_VIEN,
        CategoryName.KHAC,
        CategoryName.VAT_TU_HANG_HOA,
        CategoryName.TAI_KHOAN_NGAN_HANG,
        CategoryName.KHOAN_MUC_CHI_PHI,
        CategoryName.MUC_THU_CHI,
        CategoryName.PHONG_BAN,
        CategoryName.DOI_TUONG_TAP_HOP_CHI_PHI,
        CategoryName.HOP_DONG,
        CategoryName.MA_THONG_KE,
        CategoryName.PHONG_BAN_DOI_TUONG_TAP_HOP_CHI_PHI,
        CategoryName.KHO,
        CategoryName.DINH_MUC_NVL,
        CategoryName.TAI_SAN_CO_DINH,
        CategoryName.CONG_CU_DUNG_CU,
        CategoryName.BIEU_THUE_TAI_NGUYEN,
        CategoryName.CBB_CCTC,
        CategoryName.LENH_SAN_XUAT
    ];

    idFocus: string;
    dummyDataList: any[];
    showDropDown: boolean;
    counter: number;
    textToSort: string;
    selectRow: any;
    inSide: boolean;
    overFlow: boolean;
    required: boolean;
    bottom100: boolean;
    zindexForDropdown: boolean;
    isOverBottomPage: boolean;
    nonCheckZeroValue: boolean;
    modalRef: NgbModalRef;
    clientX: number;
    clientY: number;

    itemsPerPage: number;
    links: any;
    page: number;
    predicate: string;
    ascending: boolean;
    eventSubscriber: Subscription;
    fromChangeDataList: boolean;
    eventForTextChange: any;
    isLoaded: boolean;
    // Placeholders for the callbacks which are later provided
    // by the Control Value Accessor
    private onTouchedCallback: () => void = noop;
    private onChangeCallback: (_: any) => void = noop;

    // get accessor
    get value(): any {
        return this.innerValue;
    }

    // set accessor including call the onchange callback
    set value(v: any) {
        if (v !== this.innerValue || this.onSelectedCallback) {
            this.innerValue = v;
            this.onChangeCallback(v);
        }
    }

    // Set touched on blur
    onBlur() {
        this.onTouchedCallback();
    }

    focus() {
        const element = document.activeElement;
        if (element) {
            this.idFocus = element.id;
        }
    }

    // From ControlValueAccessor interface
    writeValue(value: any) {
        if (this.getValueDisplay) {
            this.textToSort = value;
            if (this.dummyDataList.some(x => x[this.valueName] === value)) {
                this.selectRow = this.dummyDataList.find(x => x[this.valueName] === value);
            }
            this.value = value;
            if (this.isRequired) {
                this.required = !this.textToSort;
            }
        } else {
            if (value !== this.innerValue) {
                this.innerValue = value;
                if (this.dataList) {
                    if (this.value || this.value === 0 || this.value === false) {
                        if (this.isObject) {
                            this.selectRow = this.value;
                            if (this.displayMember) {
                                this.textToSort = this.value[this.displayMember];
                            }
                        } else {
                            for (const r of this.dataList) {
                                if (r[this.valueName] || r[this.valueName] === 0 || r[this.valueName] === false) {
                                    if (this.valueIsNumber) {
                                        if (r[this.valueName] === this.value) {
                                            this.selectRow = r;
                                            break;
                                        }
                                    } else {
                                        if (String(r[this.valueName]).toUpperCase() === String(this.value).toUpperCase()) {
                                            this.selectRow = r;
                                            break;
                                        }
                                    }
                                }
                            }
                            if (this.selectRow) {
                                if (this.displayMember) {
                                    this.textToSort = this.selectRow[this.displayMember];
                                }
                            } else {
                                this.textToSort = value;
                            }
                        }
                    } else {
                        this.textToSort = null;
                        this.required = false;
                    }
                }
            }
        }
    }

    // From ControlValueAccessor interface
    registerOnChange(fn: any) {
        this.onChangeCallback = fn;
    }

    // From ControlValueAccessor interface
    registerOnTouched(fn: any) {
        this.onTouchedCallback = fn;
    }

    onFocusEventAction(event): void {
        this.counter = -1;
        if (this.nameField) {
            const nF = this.nameField.nativeElement.getBoundingClientRect();
            this.clientX = nF.x;
            this.clientY = nF.y;
        }
        this.focusInput.emit(event);
    }

    onBlurEventAction(event): void {
        this.showDropDown = false;
        this.blur.emit(event);
    }

    mouseOverTable() {
        this.inSide = true;
    }

    mouseLeaveTable() {
        this.inSide = false;
    }

    // Hautv edit check trường hợp dữ liệu không tồn tại trong danh mục
    outFocus() {
        if (!this.inSide) {
            this.showDropDown = false;
            if (!this.getValueDisplay) {
                if (this.textToSort) {
                    if (!this.value && !this.nonCheckZeroValue && (this.value === undefined || this.value === null)) {
                        if (this.autoFillFistMatch && this.dummyDataList && this.dummyDataList.length > 0) {
                            this.updateTextBox(this.dummyDataList[0]);
                            return;
                        }
                        if (this.dummyDataList) {
                            if (!this.isObject) {
                                if (this.dummyDataList.find(n => n[this.valueName] === this.value)) {
                                    return;
                                }
                            }
                        }
                        if (this.useVirtualScroll && this.lstLoadStream.includes(this.nameCategory)) {
                            this.resetPage();
                        }
                        this.toastr.warning(
                            this.translate.instant('global.combobox.dataNotExist'),
                            this.translate.instant('global.combobox.error')
                        );
                        this.required = true;
                        // giờ để mặc định xóa luôn text
                        this.textToSort = '';
                        this.required = this.isRequired;
                    }
                }
            }
            this.blur.emit(event);
        } else {
            if (this.getValueDisplay && this.valueIsNumber) {
                if (this.isRateType) {
                    this.nameField_NumberIsRateType.nativeElement.focus(); // fix lỗi không đóng combobox
                } else {
                    this.nameField_Number.nativeElement.focus(); // fix lỗi không đóng combobox
                }
            } else {
                this.nameField.nativeElement.focus(); // fix lỗi không đóng combobox
            }
        }
    }

    resetPage() {
        this.page = 0;
        this.dummyDataList = [];
        if (this.value) {
            this.value = null;
            this.textToSort = '';
        }
    }

    onKeyCodeAction(event: KeyboardEvent): void {
        if (!this.showDropDown) {
            return;
        }
        if (event.keyCode === KEY_CODE.UP_ARROW) {
            this.counter = this.dummyDataList && this.dummyDataList.length > 0 ? this.dummyDataList.indexOf(this.selectRow) - 1 : 0;
        } else if (event.keyCode === KEY_CODE.DOWN_ARROW) {
            this.counter = this.dummyDataList && this.dummyDataList.length > 0 ? this.dummyDataList.indexOf(this.selectRow) + 1 : 0;
        } else if (event.keyCode === KEY_CODE.ENTER) {
            this.counter = this.dummyDataList && this.dummyDataList.length > 0 ? this.dummyDataList.indexOf(this.selectRow) : 0;
        }
        if (this.displayMember && this.counter >= 0 && this.counter < this.dummyDataList.length) {
            if (this.getValueDisplay) {
                this.textToSort = this.dummyDataList[this.counter][this.displayMember];
                this.value = this.dummyDataList[this.counter][this.displayMember];
                if (this.isRequired) {
                    this.required = !this.value;
                }
            } else {
                this.textToSort = this.dummyDataList[this.counter][this.displayMember];
                if (this.isObject) {
                    this.value = this.dummyDataList[this.counter];
                } else {
                    this.value = this.dummyDataList[this.counter][this.valueName];
                }
                this.selectRow = this.dummyDataList[this.counter];
                if (this.isRequired) {
                    this.required = !this.value;
                }
            }
        }
        if (event.keyCode === KEY_CODE.ENTER) {
            this.showDropDown = false;
        }
        this.scrollItemSelected(event);
    }

    scrollItemSelected($event: KeyboardEvent, scroll?: string) {
        // class table
        const parentContainer = document.getElementsByClassName('cbb-table')[0];
        // class item selected
        const element = document.getElementsByClassName('selected')[0];
        // class header
        const headerTable = document.getElementsByClassName('header-cbb-table')[0];
        if (parentContainer === undefined || element === undefined || headerTable === undefined) {
            return false;
        }
        const elRect = element.getBoundingClientRect(),
            parRect = parentContainer.getBoundingClientRect(),
            headerRect = headerTable.getBoundingClientRect();
        const elementHeight = elRect.height;
        if (scroll && scroll === 'UP') {
            if (elRect.top <= parRect.top + elementHeight) {
                parentContainer.scrollTop = parentContainer.scrollTop - (parRect.top + elementHeight - elRect.top) - headerRect.height;
            }
        }
        if (scroll && scroll === 'DOWN') {
            if (!(elRect.top >= parRect.top && elRect.bottom <= parRect.bottom && elRect.bottom + elementHeight <= parRect.bottom)) {
                parentContainer.scrollTop = parentContainer.scrollTop + elRect.height + (elRect.bottom - parRect.bottom);
            }
        }
        if ($event && $event.key === 'ArrowDown') {
            // down arrow
            if (!(elRect.top >= parRect.top && elRect.bottom <= parRect.bottom && elRect.bottom + elementHeight <= parRect.bottom)) {
                parentContainer.scrollTop = parentContainer.scrollTop + elRect.height + (elRect.bottom - parRect.bottom);
            }
        } else {
            // up arrow
            if (elRect.top <= parRect.top + elementHeight) {
                parentContainer.scrollTop = parentContainer.scrollTop - (parRect.top + elementHeight - elRect.top) - headerRect.height;
            }
        }
    }

    checkHighlight(currentItem): boolean {
        return this.counter === currentItem;
    }

    constructor(
        private comboBoxPipe: ComboBoxPipe,
        private utilsService: UtilsService,
        private toastr: ToastrService,
        private translate: TranslateService,
        private comboboxModalService: ComboboxModalService,
        protected parseLinks: JhiParseLinks,
        public eventManager: JhiEventManager
    ) {
        this.reset();
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 0;
        this.links = {
            last: 0
        };
        this.registerSelectComboboxCategory();
    }

    ngOnInit() {
        this.required = false;
        this.reset();
        this.zindexForDropdown = this.isOutTable;
        if (this.dataList) {
            if (this.dataList.length > 0) {
                if (this.dataList[0].hasOwnProperty('isParentNode')) {
                    this.configComboboxWithParentNode(this.dataList.filter(n => n['isParentNode'] && n['grade'] === 1), 1);
                }
            }
        }
    }

    // Add by Hautv
    configComboboxWithParentNode(dataPerent: any[], i): void {
        const dataParent: any[] = dataPerent.filter(n => n['isParentNode'] && n['grade'] === i);
        dataParent.forEach(n => {
            n.space = i;
            const children = this.dataList.filter(m => m['parentID'] === m['id']);
            if (children) {
                this.configComboboxWithParentNode(children, i++);
            }
        });
    }

    ngOnChanges(changes: SimpleChanges): void {
        const name = changes['nameCategory'];
        if (name && this.useVirtualScroll) {
            if (name.currentValue !== name.previousValue) {
                this.resetPage();
            }
        }
        if (this.getValueDisplay) {
            if (this.isRequired) {
                this.required = !this.textToSort;
            }
        } else {
            if (this.useVirtualScroll && this.fromChangeDataList) {
                this.fromChangeDataList = false;
                return;
            }
            if (this.value || this.value === 0 || this.value === false) {
                if (this.isObject) {
                    this.selectRow = this.value;
                    if (this.displayMember) {
                        this.textToSort = this.value[this.displayMember];
                    }
                } else {
                    if (this.dataList) {
                        for (const r of this.dataList) {
                            if (r[this.valueName] || r[this.valueName] === 0 || r[this.valueName] === false) {
                                if (this.valueIsNumber) {
                                    if (r[this.valueName] === this.value) {
                                        this.selectRow = r;
                                        break;
                                    }
                                } else {
                                    if (String(r[this.valueName]).toUpperCase() === String(this.value).toUpperCase()) {
                                        this.selectRow = r;
                                        break;
                                    }
                                }
                            }
                        }
                        if (this.selectRow) {
                            if (this.displayMember) {
                                this.textToSort = this.selectRow[this.displayMember];
                            }
                        } else {
                            this.textToSort = this.value;
                        }
                    }
                }
            } else {
                this.textToSort = null;
                this.required = false;
            }
        }
    }

    focusText(event) {
        this.eventForTextChange = event;
    }

    async toogleDropDown(event, noShowDropDown?) {
        if (this.useVirtualScroll && this.lstLoadStream.includes(this.nameCategory)) {
            if (!this.isLoaded) {
                this.dummyDataList = [];
            }
            if (!this.dummyDataList || this.dummyDataList.length === 0) {
                this.dummyDataList = await this.loadData(this.textToSort ? this.textToSort : '');
                this.dataChange();
            } else {
                if (this.value && this.textToSort) {
                    if (!this.dummyDataList.map(n => n[this.displayMember]).includes(this.textToSort)) {
                        this.dummyDataList = await this.loadData(this.textToSort ? this.textToSort : '');
                    } else {
                        if (!this.dummyDataList || this.dummyDataList.length === 0) {
                            this.dummyDataList = await this.loadData(this.textToSort ? this.textToSort : '');
                        }
                    }
                }
                this.dataChange();
            }
        } else {
            this.dummyDataList = this.dataList;
        }
        if (!this.value && this.dummyDataList && this.dummyDataList.length > 0) {
            this.selectRow = this.dummyDataList[0];
        }
        if (noShowDropDown) {
            return;
        }
        this.showDropDown = !this.showDropDown;
        // prevent overflow page
        if (window.innerWidth - event.clientX < 400) {
            this.overFlow = true;
        }
        if (event.clientY > 260) {
            this.isOverBottomPage = true;
        }
        /*Add by hautv*/
        if (this.nameField) {
            this.setLocaitonDropDown(this.nameField, event);
        } else if (this.nameField_Number) {
            this.setLocaitonDropDown(this.nameField_Number, event);
        } else if (this.nameField_NumberIsRateType) {
            this.setLocaitonDropDown(this.nameField, event);
        }
        if (this.showDropDown && this.dataList) {
            setTimeout(() => this.onScrollToItem(), 100);
        }
    }

    onScrollToItem() {
        // class table
        const parentContainer = document.getElementsByClassName('dropdown-element')[0];
        if (parentContainer === undefined) {
            return;
        }
        // class item selected
        const element = parentContainer.getElementsByClassName('selected')[0];
        if (element === undefined) {
            return;
        }
        const elRect = element.getBoundingClientRect(),
            parRect = parentContainer.getBoundingClientRect();
        const elementHeight = elRect.height;
        if (!(elRect.top >= parRect.top && elRect.bottom <= parRect.bottom && elRect.bottom + elementHeight <= parRect.bottom)) {
            parentContainer.scrollTop = parentContainer.scrollTop + elRect.height + (elRect.bottom - parRect.bottom);
        }
    }

    /*Add by hautv*/
    setLocaitonDropDown(nameField, event) {
        let nF = nameField.nativeElement.parentElement;
        let checkInsideTable;
        for (let i = 0; i < 10; i++) {
            nF = nF.parentElement;
            if (nF.nodeName.includes('TABLE')) {
                checkInsideTable = true;
                break;
            }
        }
        if (checkInsideTable) {
            const pTb_Div = nF.parentElement.getBoundingClientRect();
            const long =
                pTb_Div.y +
                pTb_Div.height -
                (event && event.clientY ? event.clientY : this.eventForTextChange.clientY ? this.eventForTextChange.clientY : 0);
            if (long < 135) {
                this.bottom100 = true;
            } else {
                this.bottom100 = false;
                this.zindexForDropdown = this.isOutTable;
            }
        }
    }

    reset(): void {
        if (this.dataList === undefined) {
            if (!this.useVirtualScroll) {
                this.dataList = [];
            }
        }
        this.showDropDown = false;
        this.dummyDataList = this.dataList;
    }

    async textChange(value) {
        this.page = 0;
        // this.dummyDataList = [];
        if (this.getValueDisplay) {
            if (value || value === 0) {
                if (this.dummyDataList.some(x => x[this.valueName] === value[this.valueName])) {
                    this.selectRow = value;
                }
                this.value = value;
                this.dummyDataList = await this.seachWithText(value);
                this.dataChange();
                this.required = false;
            } else {
                if (this.isRequired) {
                    this.required = true;
                }
                this.value = null;
            }
        } else {
            if (value.length > 0) {
                this.dummyDataList = await this.seachWithText(value);
                this.dataChange();
                if (this.dummyDataList) {
                    if (this.dummyDataList.length > 0) {
                        this.showDropDown = true;
                        if (this.isObject) {
                            if (this.dummyDataList[0][this.displayMember]) {
                                if (this.valueIsNumber) {
                                    if (isNumber(value)) {
                                        if (this.dummyDataList[0][this.displayMember] === value) {
                                            this.value = this.dummyDataList[0];
                                            this.selectRow = this.dummyDataList[0];
                                            this.textToSort = this.selectRow[this.displayMember];
                                            this.required = false;
                                        } else {
                                            this.value = null;
                                            this.selectRow = null;
                                            if (this.textToSort) {
                                                this.required = true;
                                            }
                                        }
                                    } else {
                                        this.value = null;
                                        this.selectRow = null;
                                        if (this.textToSort) {
                                            this.required = true;
                                        }
                                    }
                                } else {
                                    for (let i = 0; i < this.dummyDataList.length; i++) {
                                        if (
                                            String(this.dummyDataList[i][this.displayMember]).toUpperCase() ===
                                            value.toString().toUpperCase()
                                        ) {
                                            this.value = this.dummyDataList[i];
                                            this.selectRow = this.dummyDataList[i];
                                            this.textToSort = this.selectRow[this.displayMember];
                                            this.required = false;
                                            break;
                                        } else {
                                            this.value = null;
                                            this.selectRow = null;
                                            if (this.textToSort) {
                                                this.required = true;
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            for (let i = 0; i < this.dummyDataList.length; i++) {
                                if (this.dummyDataList[i][this.displayMember]) {
                                    if (this.valueIsNumber) {
                                        if (isNumber(value)) {
                                            if (this.dummyDataList[i][this.displayMember] === value) {
                                                this.value = this.dummyDataList[i][this.valueName];
                                                this.selectRow = this.dummyDataList[i];
                                                this.textToSort = this.selectRow[this.displayMember];
                                                this.required = false;
                                                break;
                                            } else {
                                                this.value = null;
                                                this.selectRow = null;
                                                if (this.textToSort) {
                                                    this.required = true;
                                                }
                                            }
                                        } else {
                                            this.value = null;
                                            this.selectRow = null;
                                            if (this.textToSort) {
                                                this.required = true;
                                            }
                                        }
                                    } else {
                                        if (
                                            String(this.dummyDataList[i][this.displayMember]).toUpperCase() ===
                                            value.toString().toUpperCase()
                                        ) {
                                            this.value = this.dummyDataList[i][this.valueName];
                                            this.selectRow = this.dummyDataList[i];
                                            this.textToSort = this.selectRow[this.displayMember];
                                            this.required = false;
                                            break;
                                        } else {
                                            this.value = null;
                                            this.selectRow = null;
                                            if (this.textToSort) {
                                                this.required = true;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        this.value = null;
                    }
                }
            } else {
                this.value = null;
                this.required = false;
                if (this.useVirtualScroll && this.lstLoadStream.includes(this.nameCategory)) {
                    this.dataList = [];
                }
                // this.reset();
            }
        }
        if (this.clientY && this.clientX) {
            // prevent overflow page
            if (window.innerWidth - this.clientX < 400) {
                this.overFlow = true;
            }
            if (this.clientY > 260) {
                this.isOverBottomPage = true;
            }
            /*Add by hautv*/
            if (this.nameField) {
                this.setLocaitonDropDown(this.nameField, event);
            } else if (this.nameField_Number) {
                this.setLocaitonDropDown(this.nameField_Number, event);
            } else if (this.nameField_NumberIsRateType) {
                this.setLocaitonDropDown(this.nameField, event);
            }
        }
    }

    async seachWithText(text: string, noload?) {
        if (!noload && this.useVirtualScroll && this.lstLoadStream.includes(this.nameCategory)) {
            return await this.loadData(text);
        } else {
            let listSeach: any[];
            listSeach = [];
            for (let i = 0; i < this.dataList.length; i++) {
                for (let j = 0; j < this.listColumns.length; j++) {
                    if (text) {
                        if (
                            String(this.dataList[i][this.listColumns[j]] === null ? '' : this.dataList[i][this.listColumns[j]])
                                .toString()
                                .trim()
                                .toUpperCase()
                                .includes(
                                    text
                                        .toString()
                                        .trim()
                                        .toUpperCase()
                                )
                        ) {
                            listSeach.push(this.dataList[i]);
                            break;
                        }
                    } else {
                        listSeach.push(this.dataList[i]);
                        break;
                    }
                }
            }
            return listSeach;
        }
    }

    searchDrop(text): any[] {
        let listSeach: any[];
        listSeach = [];
        for (let i = 0; i < this.dummyDataList.length; i++) {
            for (let j = 0; j < this.listColumns.length; j++) {
                if (text) {
                    if (
                        String(this.dummyDataList[i][this.listColumns[j]] === null ? '' : this.dummyDataList[i][this.listColumns[j]])
                            .toString()
                            .toUpperCase()
                            .includes(text.toString().toUpperCase())
                    ) {
                        listSeach.push(this.dummyDataList[i]);
                        break;
                    }
                } else {
                    listSeach.push(this.dummyDataList[i]);
                    break;
                }
            }
        }
        return listSeach;
    }

    updateTextBox(valueSelected) {
        if (this.noGetValueParentNode && valueSelected['isParentNode']) {
            this.value = null;
            this.textToSort = '';
            if (this.fromTax) {
                this.toastr.warning('Không chọn vào chỉ tiêu cha');
            } else {
                this.toastr.warning('Không hạch toán vào chỉ tiêu cha');
            }
            return;
        }
        if (this.getValueDisplay) {
            if (this.displayMember) {
                if (this.dummyDataList.some(x => x[this.valueName] === valueSelected[this.valueName])) {
                    this.selectRow = valueSelected;
                }
                if (this.isRateType) {
                    this.value = valueSelected[this.valueName];
                    this.textToSort = valueSelected[this.displayMember];
                } else {
                    this.value = valueSelected[this.displayMember];
                    this.textToSort = valueSelected[this.displayMember];
                }
                if (this.isRequired) {
                    this.required = !this.textToSort;
                }
                this.showDropDown = false;
                this.inSide = false;
            }
        } else {
            this.selectRow = valueSelected;
            if (this.displayMember) {
                this.textToSort = valueSelected[this.displayMember];
            }
            if (this.isObject) {
                this.value = valueSelected;
            } else {
                this.value = valueSelected[this.valueName];
            }
            this.inSide = false;
            this.showDropDown = false;
            this.required = false;
        }
    }

    onSelect(select: any) {
        this.selectRow = select;
    }

    Add() {
        this.comboboxModalService.isOpen = false;
        this.toogleDropDown(null, true); // xử lý khi chưa có dữ liệu mà thêm tại dấu + bị ra id
        this.utilsService.setShowPopup(true);
        this.modalRef = this.comboboxModalService.open(this.nameCategory, this.typeObject, this.isCostSet, this.isPopupOverride);
    }

    getDate(date) {
        if (date) {
            return moment(date, DATE_FORMAT).format('DD/MM/YYYY');
        }
    }

    getData(nameColumn, value) {
        if (nameColumn.toLowerCase().includes('date')) {
            return this.getDate(value[nameColumn]);
        } else {
            return value[nameColumn];
        }
    }

    clickInside() {
        try {
            this.nameField.nativeElement.focus();
        } catch (e) {}
    }

    tab(event: KeyboardEvent) {
        if (
            this.showDropDown &&
            (event.keyCode === KEY_CODE.UP_ARROW || event.keyCode === KEY_CODE.DOWN_ARROW || event.keyCode === KEY_CODE.ENTER)
        ) {
        } else {
            this.inSide = false;
            this.showDropDown = false;
        }
    }

    async loadPage(page) {
        this.page = page;
        const data = await this.loadData(this.textToSort);
        // data.forEach(item => {
        //     if (this.dummyDataList.indexOf(item) < 0) {
        //         this.dummyDataList.push(item);
        //     }
        // });
        this.dummyDataList.push(...data);
        this.dataChange();
    }

    loadData(text: string): any {
        return new Promise(resolve => {
            if (this.useVirtualScroll && this.lstLoadStream.includes(this.nameCategory)) {
                this.utilsService
                    .getAllForComboboxWithpage({
                        page: this.page,
                        textSearch: text ? text : '',
                        nameCategory: this.nameCategory,
                        materialsGoodsTypes: this.option && this.option.materialsGoodsTypes ? this.option.materialsGoodsTypes : '',
                        typeID: this.option && this.option.typeID ? this.option.typeID : '',
                        getAll: this.checkGetAll || false,
                        typeObjects: this.option && this.option.typeObjects ? this.option.typeObjects : '',
                        date: this.date ? this.date.format(DATE_FORMAT) : '',
                        typeCbb: this.typeCbb != null ? this.typeCbb : 999,
                        unitType: this.unitType != null ? this.unitType : 999
                    })
                    .subscribe((res: HttpResponse<IAccountingObject[]>) => {
                        const headersLink = res.headers.get('link');
                        if (headersLink) {
                            this.links = this.parseLinks.parse(headersLink ? headersLink : '');
                        }
                        this.isLoaded = true;
                        resolve(res.body);
                    });
            }
        });
    }

    registerSelectComboboxCategory() {
        this.eventSubscriber = this.eventManager.subscribe('afterSelectCategoryForCombobox', response => {
            switch (response.content.nameCategory) {
                case this.utilsService.NHA_CUNG_CAP:
                case this.utilsService.NHAN_VIEN:
                case this.utilsService.DOI_TUONG:
                    if (this.dummyDataList) {
                        if (!this.dummyDataList.some(n => n.id === response.content.dataSelect.id)) {
                            if (this.isOutTable === false) {
                                this.dummyDataList.push(response.content.dataSelect);
                            }
                        }
                    }
                    break;
            }
        });
    }

    ngOnDestroy(): void {
        this.eventSubscriber.unsubscribe();
    }

    dataChange() {
        if (this.useVirtualScroll) {
            this.afterDataChange.emit({
                data: this.dummyDataList,
                nameCategory: this.nameCategory,
                typeObject: this.typeObject
            });
            this.fromChangeDataList = true;
        }
    }
}
