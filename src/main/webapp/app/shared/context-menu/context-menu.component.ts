import { Component, Input, OnInit } from '@angular/core';
import { JhiEventManager } from 'ng-jhipster';
import { ContextMenu } from 'app/shared/models/context-menu.model';

@Component({
    selector: 'eb-context-menu',
    templateUrl: './context-menu.component.html',
    styleUrls: ['./context-menu.component.css']
})
export class EbContextMenuComponent implements OnInit {
    @Input() contextMenu: ContextMenu;
    @Input() eventHandlingHere: boolean;
    @Input() useCopyRow: boolean;
    @Input() isMaterialGoodsSpecification: boolean;
    @Input() isMaterialGoodsSpecificationSelect: boolean;
    @Input() isDichDanh: boolean;
    @Input() isModal: boolean;
    @Input() isModalAccounting: boolean;
    @Input() isModalSmall: boolean;

    constructor(private eventManager: JhiEventManager) {}

    ngOnInit() {
        this.contextMenu = this.contextMenu ? this.contextMenu : {};
    }

    addNewRow() {
        if (!this.eventHandlingHere) {
            this.contextMenu.data.push({});
        }
        this.contextMenu.isShow = false;
        // sau khi them 1 dong
        this.eventManager.broadcast({
            name: 'afterAddNewRow',
            content: this.contextMenu.selectedData
        });
    }

    deleteRow() {
        if (!this.eventHandlingHere) {
            this.contextMenu.data.splice(this.contextMenu.data.indexOf(this.contextMenu.selectedData), 1);
        }
        this.contextMenu.isShow = false;
        // sau khi xoa 1 dong
        this.eventManager.broadcast({
            name: 'afterDeleteRow',
            content: this.contextMenu.selectedData
        });
    }

    copyRow() {
        if (!this.eventHandlingHere) {
            const copy = Object.assign({}, this.contextMenu.selectedData ? this.contextMenu.selectedData : {});
            copy.id = null;
            copy.orderPriority = null;
            this.contextMenu.data.push(copy);
        }
        this.contextMenu.isShow = false;
        // sau khi them 1 dong
        this.eventManager.broadcast({
            name: 'afterCopyRow',
            content: this.contextMenu.selectedData
        });
    }

    copyAnnotationVertical() {
        this.contextMenu.isShow = false;
        // sao chép ký hiệu theo chiều dọc
        this.eventManager.broadcast({
            name: 'afterCopyAnnotationVertical',
            content: this.contextMenu.selectedData
        });
    }

    copyAnnotationHorizontal() {
        this.contextMenu.isShow = false;
        // sao chép ký hiệu theo chiều dọc
        this.eventManager.broadcast({
            name: 'afterCopyAnnotationHorizontal',
            content: this.contextMenu.selectedData
        });
    }

    addNewRowModal() {
        if (!this.eventHandlingHere) {
            this.contextMenu.data.push({});
        }
        this.contextMenu.isShow = false;
        // sau khi them 1 dong
        this.eventManager.broadcast({
            name: 'afterAddNewRowModal',
            content: this.contextMenu.selectedData
        });
    }

    deleteRowModal() {
        if (!this.eventHandlingHere) {
            this.contextMenu.data.splice(this.contextMenu.data.indexOf(this.contextMenu.selectedData), 1);
        }
        this.contextMenu.isShow = false;
        // sau khi xoa 1 dong
        this.eventManager.broadcast({
            name: 'afterDeleteRowModal',
            content: this.contextMenu.selectedData
        });
    }

    copyRowModal() {
        if (!this.eventHandlingHere) {
            const copy = Object.assign({}, this.contextMenu.selectedData ? this.contextMenu.selectedData : {});
            copy.id = null;
            copy.orderPriority = null;
            this.contextMenu.data.push(copy);
        }
        this.contextMenu.isShow = false;
        // sau khi them 1 dong
        this.eventManager.broadcast({
            name: 'afterCopyRowModal',
            content: this.contextMenu.selectedData
        });
    }

    selectMaterialGoodsSpecification() {
        this.contextMenu.isShow = false;
        this.eventManager.broadcast({
            name: 'selectMaterialGoodsSpecification',
            content: this.contextMenu.selectedData
        });
    }

    selectMaterialGoodsDichDanh() {
        this.contextMenu.isShow = false;
        this.eventManager.broadcast({
            name: 'selectMaterialGoodsDichDanh',
            content: this.contextMenu.selectedData
        });
    }

    getYOffset() {
        this.contextMenu.y = this.contextMenu && this.contextMenu.event ? this.contextMenu.event.pageY : 0;
        return this.contextMenu.y;
    }

    getXOffset() {
        if (this.contextMenu && this.contextMenu.event) {
            if (this.contextMenu.event.pageX > 1290) {
                return this.contextMenu.event.pageX - 200;
            } else if (this.contextMenu.event.pageX < 1290) {
                return this.contextMenu.event.pageX;
            }
        }
        return 0;
    }

    getYOffsetModal() {
        return this.contextMenu.event.pageY - 100;
    }

    getXOffsetModal() {
        if (!this.isModalSmall) {
            return this.contextMenu.event.pageX - 200;
        } else {
            return this.contextMenu.event.pageX - 500;
        }
    }

    getYOffsetModalAccounting() {
        return this.contextMenu.event.pageY - 40;
    }

    getXOffsetModalAccounting() {
        return this.contextMenu.event.pageX - 210;
    }
}
