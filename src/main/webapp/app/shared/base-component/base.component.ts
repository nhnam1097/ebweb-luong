import { HostListener, Injectable, OnDestroy } from '@angular/core';
import { UtilsService } from 'app/shared/UtilsService/Utils.service';
import { Subscription } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import { CategoryName, DBDateClosed } from 'app/app.constants';
import * as moment from 'moment';
import { DATE_FORMAT, DATE_FORMAT_SLASH } from 'app/shared';
// @Component({
//     template: ``,
//     selector: 'eb-base'
// })
@Injectable()
export abstract class BaseComponent implements OnDestroy {
    isShowPopup: boolean;
    isEnter: any;
    utilsService: UtilsService;
    selectedRows = [];
    selectedRow = {};
    objects: any[];
    lstDataForSetDefault: any[];
    object: any;
    eventSubscribers: Subscription[] = [];
    inPopup: any;
    isSearching: any;
    typeCbb: number;
    accountingObjects = [];
    employees = [];
    bankAccountDetails = [];
    budgetItems = [];
    costSets = [];
    eMContracts = [];
    organizationUnits = [];
    expenseItems = [];
    statisticCodes = [];
    materialGoodss = [];
    materialGoodsInStock = [];
    repositories = [];
    objectsMaterialQuantum = [];
    prepaidExpenseCodeDTOs = [];
    materialGoodsResourceTaxGroups = [];
    allOrganizationUnits = [];
    rsProductionOrders = [];
    isUseToolIn: any;
    isEdit: boolean;
    isCbbSaveAndNew: any;
    currentRow: number;
    isEditPage: boolean;
    toolbarClass: string;
    isSpecialForm: boolean;
    accountingObjectID: any;
    accountingObjectId: any;
    employeeID: any;
    details: any[];
    parent: any;
    noUseEnter: any;
    color: any;
    accountingObjectBankAccounts: any;

    loadDataDefaultForOpenVoucherSync(lstDataDetailForSetDefault: any[]) {
        return new Promise(resolve => {
            let lstID_AccountingObject = [];
            let lstID_Employee = [];
            let lstID_BankAccountDetail = [];
            let lstID_BudgetItem = [];
            let lstID_CostSet = [];
            let lstID_Contract = [];
            let lstID_Department = [];
            let lstID_ExpenseItem = [];
            let lstID_StatisticsCode = [];
            let lstID_MaterialGoods = [];
            let lstID_Repositories = [];
            let lstID_DepartmentCostSet = [];
            let lstID_DepartmentCCTC = [];
            let lstID_ObjectMaterialQuantums = [];
            let lstID_RSProductionOrder = [];
            if (lstDataDetailForSetDefault && lstDataDetailForSetDefault.length > 0) {
                lstDataDetailForSetDefault.forEach(detail => {
                    if (detail && detail.length > 0) {
                        if (detail[0].hasOwnProperty('accountingObject')) {
                            lstID_AccountingObject.push(
                                ...Array.from(new Set(detail.map(item => item['accountingObject'] && item['accountingObject']['id'])))
                            );
                        } else if (detail[0].hasOwnProperty('accountingObjectID')) {
                            lstID_AccountingObject.push(...Array.from(new Set(detail.map(item => item['accountingObjectID']))));
                        } else if (detail[0].hasOwnProperty('accountingObjectId')) {
                            lstID_AccountingObject.push(...Array.from(new Set(detail.map(item => item['accountingObjectId']))));
                        } else if (detail[0].hasOwnProperty('postedObjectId')) {
                            lstID_AccountingObject.push(...Array.from(new Set(detail.map(item => item['postedObjectId']))));
                        }

                        if (detail[0].hasOwnProperty('creditAccountingObjectID')) {
                            lstID_AccountingObject.push(...Array.from(new Set(detail.map(item => item['creditAccountingObjectID']))));
                        }
                        if (detail[0].hasOwnProperty('debitAccountingObjectID')) {
                            lstID_AccountingObject.push(...Array.from(new Set(detail.map(item => item['debitAccountingObjectID']))));
                        }
                        if (detail[0].hasOwnProperty('objectID')) {
                            lstID_ObjectMaterialQuantums.push(...Array.from(new Set(detail.map(item => item['objectID']))));
                            lstID_DepartmentCostSet.push(...Array.from(new Set(detail.map(item => item['objectID']))));
                        } else if (detail[0].hasOwnProperty('objectID')) {
                            lstID_ObjectMaterialQuantums.push(...Array.from(new Set(detail.map(item => item['objectID']))));
                            lstID_DepartmentCostSet.push(...Array.from(new Set(detail.map(item => item['objectID']))));
                        }

                        if (detail[0].hasOwnProperty('employeeID')) {
                            lstID_Employee.push(...Array.from(new Set(detail.map(item => item['employeeID']))));
                        }
                        if (detail[0].hasOwnProperty('employeeId')) {
                            lstID_Employee.push(...Array.from(new Set(detail.map(item => item['employeeId']))));
                        }

                        if (detail[0].hasOwnProperty('bankAccountDetails')) {
                            lstID_BankAccountDetail.push(
                                ...Array.from(new Set(detail.map(item => item['bankAccountDetails'] && item['bankAccountDetails']['id'])))
                            );
                        } else if (detail[0].hasOwnProperty('bankAccountDetailID')) {
                            lstID_BankAccountDetail.push(...Array.from(new Set(detail.map(item => item['bankAccountDetailID']))));
                        }

                        if (detail[0].hasOwnProperty('budgetItem')) {
                            lstID_BudgetItem.push(
                                ...Array.from(new Set(detail.map(item => item['budgetItem'] && item['budgetItem']['id'])))
                            );
                        } else if (detail[0].hasOwnProperty('budgetItemID')) {
                            lstID_BudgetItem.push(...Array.from(new Set(detail.map(item => item['budgetItemID']))));
                        } else if (detail[0].hasOwnProperty('budgetItemId')) {
                            lstID_BudgetItem.push(...Array.from(new Set(detail.map(item => item['budgetItemId']))));
                        }

                        if (detail[0].hasOwnProperty('costSet')) {
                            lstID_CostSet.push(...Array.from(new Set(detail.map(item => item['costSet'] && item['costSet']['id']))));
                        } else if (detail[0].hasOwnProperty('costSetID')) {
                            lstID_CostSet.push(...Array.from(new Set(detail.map(item => item['costSetID']))));
                        } else if (detail[0].hasOwnProperty('costSetId')) {
                            lstID_CostSet.push(...Array.from(new Set(detail.map(item => item['costSetId']))));
                        }

                        if (detail[0].hasOwnProperty('contractID')) {
                            lstID_Contract.push(...Array.from(new Set(detail.map(item => item['contractID']))));
                        } else if (detail[0].hasOwnProperty('contractId')) {
                            lstID_Contract.push(...Array.from(new Set(detail.map(item => item['contractId']))));
                        }

                        if (detail[0].hasOwnProperty('organizationUnit')) {
                            lstID_Department.push(
                                ...Array.from(new Set(detail.map(item => item['organizationUnit'] && item['organizationUnit']['id'])))
                            );
                        } else if (detail[0].hasOwnProperty('departmentID')) {
                            lstID_Department.push(...Array.from(new Set(detail.map(item => item['departmentID']))));
                        } else if (detail[0].hasOwnProperty('departmentId')) {
                            lstID_Department.push(...Array.from(new Set(detail.map(item => item['departmentId']))));
                        } else if (detail[0].hasOwnProperty('toDepartmentId')) {
                            lstID_Department.push(...Array.from(new Set(detail.map(item => item['toDepartmentId']))));
                            lstID_Department.push(...Array.from(new Set(detail.map(item => item['fromDepartmentID']))));
                        } else if (detail[0].hasOwnProperty('fromDepartmentID')) {
                            lstID_Department.push(...Array.from(new Set(detail.map(item => item['toDepartmentId']))));
                            lstID_Department.push(...Array.from(new Set(detail.map(item => item['fromDepartmentID']))));
                        }

                        if (detail[0].hasOwnProperty('expenseItemID')) {
                            lstID_ExpenseItem.push(...Array.from(new Set(detail.map(item => item['expenseItemID']))));
                        } else if (detail[0].hasOwnProperty('expenseItemId')) {
                            lstID_ExpenseItem.push(...Array.from(new Set(detail.map(item => item['expenseItemId']))));
                        } else if (detail[0].hasOwnProperty('expenseItem')) {
                            lstID_ExpenseItem.push(
                                ...Array.from(new Set(detail.map(item => item['expenseItem'] && item['expenseItem']['id'])))
                            );
                        }

                        if (detail[0].hasOwnProperty('statisticsCodeID')) {
                            lstID_StatisticsCode.push(...Array.from(new Set(detail.map(item => item['statisticsCodeID']))));
                        } else if (detail[0].hasOwnProperty('statisticsCodeId')) {
                            lstID_StatisticsCode.push(...Array.from(new Set(detail.map(item => item['statisticsCodeId']))));
                        } else if (detail[0].hasOwnProperty('statisticCodeId')) {
                            lstID_StatisticsCode.push(...Array.from(new Set(detail.map(item => item['statisticCodeId']))));
                        } else if (detail[0].hasOwnProperty('statisticCodeID')) {
                            lstID_StatisticsCode.push(...Array.from(new Set(detail.map(item => item['statisticCodeID']))));
                        } else if (detail[0].hasOwnProperty('statisticsCode')) {
                            lstID_StatisticsCode.push(
                                ...Array.from(new Set(detail.map(item => item['statisticsCode'] && item['statisticsCode']['id'])))
                            );
                        }

                        if (detail[0].hasOwnProperty('materialGoodsID')) {
                            lstID_MaterialGoods.push(...Array.from(new Set(detail.map(item => item['materialGoodsID']))));
                        } else if (detail[0].hasOwnProperty('materialGoodsId')) {
                            lstID_MaterialGoods.push(...Array.from(new Set(detail.map(item => item['materialGoodsId']))));
                        } else if (detail[0].hasOwnProperty('materialGoods')) {
                            lstID_MaterialGoods.push(
                                ...Array.from(new Set(detail.map(item => item['materialGoods'] && item['materialGoods']['id'])))
                            );
                        } else if (detail[0].hasOwnProperty('materialGood')) {
                            lstID_MaterialGoods.push(...Array.from(new Set(detail.map(item => item['materialGood']['id']))));
                        }

                        if (detail[0].hasOwnProperty('materialAssemblyID')) {
                            lstID_MaterialGoods.push(...Array.from(new Set(detail.map(item => item['materialAssemblyID']))));
                        } else if (detail[0].hasOwnProperty('materialGoodsId')) {
                            lstID_MaterialGoods.push(...Array.from(new Set(detail.map(item => item['materialGoodsId']))));
                        }

                        if (detail[0].hasOwnProperty('repositoryID')) {
                            lstID_Repositories.push(...Array.from(new Set(detail.map(item => item['repositoryID']))));
                        } else if (detail[0].hasOwnProperty('repositoryId')) {
                            lstID_Repositories.push(...Array.from(new Set(detail.map(item => item['repositoryId']))));
                        }
                        if (detail[0].hasOwnProperty('objectID') && detail[0].hasOwnProperty('objectType')) {
                            lstID_DepartmentCostSet.push(...Array.from(new Set(detail.map(item => item['objectID']))));
                        }
                        if (detail[0].hasOwnProperty('rsProductionOrderQuantumID') && detail[0].hasOwnProperty('id')) {
                            lstID_RSProductionOrder.push(...Array.from(new Set(detail.map(item => item['id']))));
                        }
                        // if (detail[0].hasOwnProperty('materialGood')) {
                        //     lstID_Repositories.push(...Array.from(new Set(detail.map(item => item['materialGood']['repositoryID']))));
                        // }
                    }
                });
            }
            /* if (this.objectsDetailsSalary && this.objectsDetailsSalary.length > 0){
                if (this.objectsDetailsSalary[0].hasOwnProperty('accountingObject')) {
                    lstID_AccountingObject = Array.from(
                        new Set(this.objectsDetailsSalary.map(item => item['accountingObject'] && item['accountingObject']['id']))
                    );
                } else if (this.objectsDetailsSalary[0].hasOwnProperty('accountingObjectID')) {
                    lstID_AccountingObject = Array.from(new Set(this.objectsDetailsSalary.map(item => item['accountingObjectID'])));
                }
                lstID_Employee = Array.from(new Set(this.objectsDetailsSalary.map(item => item['employeeID'])));
                if (this.objectsDetailsSalary[0].hasOwnProperty('organizationUnit')) {
                    lstID_Department = Array.from(
                        new Set(this.objectsDetailsSalary.map(item => item['organizationUnit'] && item['organizationUnit']['id']))
                    );
                } else if (this.objectsDetailsSalary[0].hasOwnProperty('departmentID')) {
                    lstID_Department = Array.from(new Set(this.objectsDetailsSalary.map(item => item['departmentID'])));
                }
            }

            if (this.objectsDetailsInsurance && this.objectsDetailsInsurance.length > 0){
                if (this.objectsDetailsInsurance[0].hasOwnProperty('accountingObject')) {
                    lstID_AccountingObject = Array.from(
                        new Set(this.objectsDetailsInsurance.map(item => item['accountingObject'] && item['accountingObject']['id']))
                    );
                } else if (this.objectsDetailsInsurance[0].hasOwnProperty('accountingObjectID')) {
                    lstID_AccountingObject = Array.from(new Set(this.objectsDetailsInsurance.map(item => item['accountingObjectID'])));
                }
                lstID_Employee = Array.from(new Set(this.objectsDetailsInsurance.map(item => item['employeeID'])));
                if (this.objectsDetailsInsurance[0].hasOwnProperty('organizationUnit')) {
                    lstID_Department = Array.from(
                        new Set(this.objectsDetailsInsurance.map(item => item['organizationUnit'] && item['organizationUnit']['id']))
                    );
                } else if (this.objectsDetailsInsurance[0].hasOwnProperty('departmentID')) {
                    lstID_Department = Array.from(new Set(this.objectsDetailsInsurance.map(item => item['departmentID'])));
                }
            }*/

            if (this.object) {
                if (this.object.hasOwnProperty('accountingObjectID')) {
                    lstID_AccountingObject.push(this.object['accountingObjectID']);
                } else if (this.object.hasOwnProperty('accountingObjectId')) {
                    lstID_AccountingObject.push(this.object['accountingObjectId']);
                }
                if (this.object.hasOwnProperty('employeeID')) {
                    lstID_Employee.push(this.object['employeeID']);
                } else if (this.object.hasOwnProperty('employeeId')) {
                    lstID_Employee.push(this.object['employeeId']);
                }
                if (this.object.hasOwnProperty('bankAccountDetailID')) {
                    lstID_BankAccountDetail.push(this.object['bankAccountDetailID']);
                } else if (this.object.hasOwnProperty('bankAccountDetailId')) {
                    lstID_BankAccountDetail.push(this.object['bankAccountDetailId']);
                } else if (this.object.hasOwnProperty('accountPaymentId')) {
                    lstID_BankAccountDetail.push(this.object['accountPaymentId']);
                }
                if (this.object.hasOwnProperty('departmentID')) {
                    lstID_Department.push(this.object['departmentID']);
                }
                if (this.object.hasOwnProperty('toDepartmentID')) {
                    lstID_Department.push(this.object['toDepartmentID']);
                    lstID_Department.push(this.object['fromDepartmentID']);
                } else if (this.object.hasOwnProperty('fromDepartmentID')) {
                    lstID_Department.push(this.object['toDepartmentID']);
                    lstID_Department.push(this.object['fromDepartmentID']);
                }
                if (this.object.hasOwnProperty('costSetID')) {
                    lstID_CostSet.push(this.object['costSetID']);
                }
                if (this.object.hasOwnProperty('budgetItemID')) {
                    lstID_BudgetItem.push(this.object['budgetItemID']);
                }
                if (this.object.hasOwnProperty('statisticsCodeID')) {
                    lstID_StatisticsCode.push(this.object['statisticsCodeID']);
                }
                if (this.object.hasOwnProperty('materialGoodsID')) {
                    lstID_MaterialGoods.push(this.object['materialGoodsID']);
                }
                if (this.object.hasOwnProperty('materialGood')) {
                    lstID_MaterialGoods.push(this.object['materialGood']['id']);
                }
                if (this.object.hasOwnProperty('materialGood')) {
                    lstID_Repositories.push(this.object['materialGood']['repositoryID']);
                }
                if (this.object.hasOwnProperty('objectID')) {
                    lstID_ObjectMaterialQuantums.push(this.object['objectID']);
                }
                if (this.object.hasOwnProperty('parentID')) {
                    lstID_DepartmentCCTC.push(this.object['parentID']);
                }
            }
            // distin
            lstID_AccountingObject = Array.from(new Set(lstID_AccountingObject.map(item => item)));
            lstID_Employee = Array.from(new Set(lstID_Employee.map(item => item)));
            lstID_BankAccountDetail = Array.from(new Set(lstID_BankAccountDetail.map(item => item)));
            lstID_BudgetItem = Array.from(new Set(lstID_BudgetItem.map(item => item)));
            lstID_CostSet = Array.from(new Set(lstID_CostSet.map(item => item)));
            lstID_Contract = Array.from(new Set(lstID_Contract.map(item => item)));
            lstID_Department = Array.from(new Set(lstID_Department.map(item => item)));
            lstID_ExpenseItem = Array.from(new Set(lstID_ExpenseItem.map(item => item)));
            lstID_StatisticsCode = Array.from(new Set(lstID_StatisticsCode.map(item => item)));
            lstID_MaterialGoods = Array.from(new Set(lstID_MaterialGoods.map(item => item)));
            lstID_Repositories = Array.from(new Set(lstID_Repositories.map(item => item)));
            lstID_DepartmentCostSet = Array.from(new Set(lstID_DepartmentCostSet.map(item => item)));
            lstID_DepartmentCCTC = Array.from(new Set(lstID_DepartmentCCTC.map(item => item)));
            lstID_ObjectMaterialQuantums = Array.from(new Set(lstID_ObjectMaterialQuantums.map(item => item)));
            lstID_RSProductionOrder = Array.from(new Set(lstID_RSProductionOrder.map(item => item)));

            this.utilsService
                .getDataDefaultForOpenVoucher({
                    lstIDAccountingObject: lstID_AccountingObject,
                    lstIDEmployee: lstID_Employee,
                    lstIDBankAccountDetail: lstID_BankAccountDetail,
                    lstIDBudgetItem: lstID_BudgetItem,
                    lstIDCostSet: lstID_CostSet,
                    lstIDContract: lstID_Contract,
                    lstIDDepartment: lstID_Department,
                    lstIDExpenseItem: lstID_ExpenseItem,
                    lstIDStatisticsCode: lstID_StatisticsCode,
                    lstIDMaterialGoods: lstID_MaterialGoods,
                    lstIDRepositories: lstID_Repositories,
                    lstObjectsMaterialQuantum: lstID_ObjectMaterialQuantums,
                    lstIDDepartmentCostSet: lstID_DepartmentCostSet,
                    lstIDDepartmentCCTC: lstID_DepartmentCCTC,
                    typeCbbCCTC: this.typeCbb,
                    lstIDRSProductionOrder: lstID_RSProductionOrder
                })
                .subscribe((res: HttpResponse<any>) => {
                    this.accountingObjects = res.body.listAccountingObjectDTOS;
                    this.employees = res.body.listEmployees;
                    this.bankAccountDetails = res.body.listBankAccountDetailDTOS;
                    this.budgetItems = res.body.listBudgetItems;
                    this.costSets = res.body.listCostSets;
                    this.eMContracts = res.body.listEMEmContracts;
                    this.organizationUnits = res.body.listDepartment;
                    this.expenseItems = res.body.listExpenseItem;
                    this.statisticCodes = res.body.listStatisticsCodes;
                    this.materialGoodss = res.body.listMaterialGoods;
                    this.materialGoodsInStock = res.body.listMaterialGoods;
                    this.repositories = res.body.listRepositories;
                    this.objectsMaterialQuantum = res.body.lstObjectsMaterialQuantum;
                    this.prepaidExpenseCodeDTOs = res.body.listPrepaidExpenseCodeDTOS;
                    this.materialGoodsResourceTaxGroups = res.body.listMaterialGoodsResourceTaxGroups;
                    this.allOrganizationUnits = res.body.lstIDDepartmentCCTC;
                    this.rsProductionOrders = res.body.listRSProductionOrders;
                    resolve(res.body);
                });
        });
    }

    registerComboboxSave(response) {
        // if (response.content.name === CategoryName.TAI_KHOAN_NGAN_HANG) {
        //     const newBankAccountDetail = response.content.data;
        //     if (newBankAccountDetail.isActive) {
        //         this.bankAccountDetails.push(newBankAccountDetail);
        //         this.bankAccountDetails = this.bankAccountDetails.sort((a, b) => a.bankAccount.localeCompare(b.bankAccount));
        //         this.bankAccountDetailsCCTC = this.bankAccountDetailsCCTC ? this.bankAccountDetailsCCTC : [];
        //         this.bankAccountDetailsCCTC.push(newBankAccountDetail);
        //         this.bankAccountDetailsCCTC = this.bankAccountDetailsCCTC.sort((a, b) => a.bankAccount.localeCompare(b.bankAccount));
        //         if (!this.isCbbSaveAndNew) {
        //             if (this.currentRow !== null && this.currentRow !== undefined) {
        //                 this.details[this.currentRow].bankAccountDetailID = newBankAccountDetail.id;
        //                 this.details[this.currentRow].bankAccountDetails = newBankAccountDetail;
        //                 this.details[this.currentRow].bankAccountDetailsCCTC = newBankAccountDetail;
        //                 if (this.checkBank) {
        //                     this.details[this.currentRow].fromBankAccountDetailID = newBankAccountDetail.id;
        //                 } else {
        //                     this.details[this.currentRow].toBankAccountDetailID = newBankAccountDetail.id;
        //                 }
        //             } else if (this.parent) {
        //                 this.parent.bankAccountDetailID = newBankAccountDetail.id;
        //                 this.parent.accountPaymentId = newBankAccountDetail.id;
        //                 this.parent.bankName = newBankAccountDetail.bankName;
        //                 this.parent.accountPayment = newBankAccountDetail;
        //                 this.onChangeAccountPayment();
        //             } else {
        //                 this.taiKhoanNganHangID = newBankAccountDetail.id;
        //                 this.taiKhoanNganHang = newBankAccountDetail;
        //             }
        //         }
        //     }
        // }
        // if (response.content.name === CategoryName.NGAN_HANG) {
        //     const newBank = response.content.data;
        //     if (newBank.bank.isActive) {
        //         if (!this.bankPopup) {
        //             this.bankPopup = [];
        //         }
        //         this.bankPopup.push(newBank.bank);
        //         this.bankPopup = this.bankPopup.sort((a, b) => a.bankCode.localeCompare(b.bankCode));
        //         if (!this.isCbbSaveAndNew) {
        //             if (this.parent) {
        //                 this.parent.id = newBank.bank.id;
        //                 this.parent.bankIDIssueCard = newBank.bank.id;
        //             } else {
        //                 this.nganHangID = newBank.id;
        //             }
        //         } else {
        //             this.parent.id = newBank.bank.id;
        //             this.parent.bankIDIssueCard = newBank.bank.id;
        //         }
        //     }
        // }
        // if (response.content.name === CategoryName.THE_TIN_DUNG) {
        //     const newCreditCard = response.content.data;
        //     if (newCreditCard.isActive) {
        //         if (this.creditCards) {
        //             this.creditCards.push(newCreditCard);
        //             this.creditCards = this.creditCards.sort((a, b) => a.creditCardNumber.localeCompare(b.creditCardNumber));
        //         } else {
        //             if (!this.bankAccountDetails) {
        //                 this.bankAccountDetails = [];
        //             }
        //             this.bankAccountDetails.push(newCreditCard);
        //             this.bankAccountDetails = this.bankAccountDetails.sort((a, b) => a.bankName.localeCompare(b.bankName));
        //         }
        //         if (!this.isCbbSaveAndNew) {
        //             if (this.parent) {
        //                 this.parent.creditCardNumber = newCreditCard.creditCardNumber;
        //                 this.parent.creditCardID = newCreditCard.id;
        //                 this.parent.creditCardItem = newCreditCard;
        //                 this.ownerCard = newCreditCard.ownerCard;
        //                 this.creditCardType = newCreditCard.creditCardType;
        //                 this.onChangeCreditCard();
        //             } else {
        //                 this.theTinDung = newCreditCard;
        //             }
        //         }
        //     }
        // }
        // if (response.content.name === CategoryName.PHONG_BAN) {
        //     const newOrganizationUnit = response.content.data;
        //     if (newOrganizationUnit.isActive) {
        //         this.organizationUnits.push(newOrganizationUnit);
        //         this.organizationUnits = this.organizationUnits.sort((a, b) =>
        //             a.organizationUnitCode.localeCompare(b.organizationUnitCode)
        //         );
        //         if (!this.isCbbSaveAndNew) {
        //             if (this.currentRow !== null && this.currentRow !== undefined) {
        //                 if (!this.details[this.currentRow].hasOwnProperty('faIncrementID')) {
        //                     this.details[this.currentRow].departmentID = newOrganizationUnit.id;
        //                 }
        //                 this.details[this.currentRow].departmentId = newOrganizationUnit.id;
        //                 // this.details[this.currentRow].fromDepartmentID = newOrganizationUnit.id;
        //                 // this.details[this.currentRow].fromDepartmentId = newOrganizationUnit.id;
        //                 this.details[this.currentRow].toDepartmentID = newOrganizationUnit.id;
        //                 this.details[this.currentRow].toDepartmentId = newOrganizationUnit.id;
        //                 this.details[this.currentRow].department = newOrganizationUnit;
        //                 this.details[this.currentRow].organizationUnit = newOrganizationUnit;
        //                 this.details[this.currentRow].organizationUnitItem = newOrganizationUnit;
        //                 this.selectChangeOrganizationUnitItem(this.details[this.currentRow]);
        //             } else {
        //                 this.parent.department = newOrganizationUnit;
        //                 this.parent.departmentId = newOrganizationUnit.id;
        //                 this.parent.departmentID = newOrganizationUnit.id;
        //                 this.parent.organizationUnit = newOrganizationUnit;
        //                 this.parent.organizationUnitID = newOrganizationUnit.id;
        //             }
        //         } else {
        //             this.parent.departmentId = newOrganizationUnit.id;
        //             this.parent.department = newOrganizationUnit;
        //             this.parent.organizationUnit = newOrganizationUnit;
        //         }
        //     }
        // }
        // if (response.content.name === CategoryName.MA_THONG_KE) {
        //     const newStatisticsCode = response.content.data;
        //     if (newStatisticsCode.isActive) {
        //         this.statisticCodes.push(newStatisticsCode);
        //         this.statisticCodes = this.statisticCodes.sort((a, b) => a.statisticsCode.localeCompare(b.statisticsCode));
        //         if (!this.isCbbSaveAndNew) {
        //             if (this.currentRow !== null && this.currentRow !== undefined) {
        //                 this.details[this.currentRow].statisticsCodeID = newStatisticsCode.id;
        //                 this.details[this.currentRow].statisticCodeID = newStatisticsCode.id;
        //                 this.details[this.currentRow].statisticCodeId = newStatisticsCode.id;
        //                 this.details[this.currentRow].statisticsId = newStatisticsCode.id;
        //                 this.details[this.currentRow].statisticCode = newStatisticsCode;
        //                 this.details[this.currentRow].statisticsCode = newStatisticsCode;
        //                 this.details[this.currentRow].statisticsCodeItem = newStatisticsCode;
        //                 this.selectChangeStatisticsCodeItem(this.details[this.currentRow]);
        //             } else {
        //                 this.parent.statisticsCode = newStatisticsCode;
        //                 this.parent.statisticsCodeID = newStatisticsCode.id;
        //             }
        //         }
        //     }
        // }
        // if (response.content.name === CategoryName.DOI_TUONG_TAP_HOP_CHI_PHI) {
        //     if (this.selected === 2) {
        //         const newCostSet = response.content.data;
        //         if (newCostSet.isActive) {
        //             if (this.costSets) {
        //                 this.costSets.push(newCostSet);
        //                 this.costSets = this.costSets.sort((a, b) => a.costSetCode.localeCompare(b.costSetCode));
        //             }
        //             if (!this.isCbbSaveAndNew) {
        //                 if (this.currentRow1 !== null && this.currentRow1 !== undefined) {
        //                     this.details1[this.currentRow1].costSetID = newCostSet.id;
        //                     this.details1[this.currentRow1].costSetId = newCostSet.id;
        //                     this.details1[this.currentRow1].costSet = newCostSet;
        //                     this.details1[this.currentRow1].costSetItem = newCostSet;
        //                     this.selectChangeCostSetItem(this.details1[this.currentRow1]);
        //                 } else {
        //                     this.parent.costSet = newCostSet;
        //                     this.parent.costSetID = newCostSet.id;
        //                 }
        //             }
        //         }
        //     } else {
        //         const newCostSet = response.content.data;
        //         if (newCostSet.isActive) {
        //             if (this.costSetList) {
        //                 this.costSetList.push(newCostSet);
        //                 this.costSetList = this.costSetList.sort((a, b) => a.costSetCode.localeCompare(b.costSetCode));
        //             }
        //             if (this.costSets) {
        //                 this.costSets.push(newCostSet);
        //                 this.costSets = this.costSets.sort((a, b) => a.costSetCode.localeCompare(b.costSetCode));
        //             }
        //             if (!this.isCbbSaveAndNew) {
        //                 if (this.currentRow !== null && this.currentRow !== undefined) {
        //                     this.details[this.currentRow].costSetID = newCostSet.id;
        //                     this.details[this.currentRow].costSetId = newCostSet.id;
        //                     this.details[this.currentRow].costSet = newCostSet;
        //                     this.details[this.currentRow].costSetItem = newCostSet;
        //                     this.selectChangeCostSetItem(this.details[this.currentRow]);
        //                 } else {
        //                     this.parent.costSet = newCostSet;
        //                     this.parent.costSetID = newCostSet.id;
        //                 }
        //             }
        //         }
        //     }
        // }
        // if (response.content.name === CategoryName.MUC_THU_CHI) {
        //     const newBudgetItem = response.content.data;
        //     if (newBudgetItem.isActive && newBudgetItem.id) {
        //         if (this.budgetItemList) {
        //             this.budgetItemList.push(newBudgetItem);
        //             this.budgetItemList = this.budgetItemList.sort((a, b) => a.budgetItemCode.localeCompare(b.budgetItemCode));
        //         }
        //         if (this.budgetItems) {
        //             this.budgetItems.push(newBudgetItem);
        //             this.budgetItems = this.budgetItems.sort((a, b) => a.budgetItemCode.localeCompare(b.budgetItemCode));
        //         }
        //         if (!this.isCbbSaveAndNew) {
        //             if (this.currentRow !== null && this.currentRow !== undefined) {
        //                 this.details[this.currentRow].budgetItemID = newBudgetItem.id;
        //                 this.details[this.currentRow].budgetItemId = newBudgetItem.id;
        //                 this.details[this.currentRow].budgetItem = newBudgetItem;
        //                 this.selectChangeBudgetItem(this.details[this.currentRow]);
        //             } else {
        //                 this.parent.budgetItem = newBudgetItem;
        //                 this.parent.budgetItemID = newBudgetItem.id;
        //             }
        //         }
        //     }
        // }
        //
        // if (response.content.name === CategoryName.KHOAN_MUC_CHI_PHI) {
        //     if (this.selected === 2) {
        //         const newExpenseItem = response.content.data;
        //         if (newExpenseItem.isActive && newExpenseItem.id) {
        //             if (this.expenseItems) {
        //                 this.expenseItems.push(newExpenseItem);
        //                 this.expenseItems = this.expenseItems.sort((a, b) => a.expenseItemCode.localeCompare(b.expenseItemCode));
        //             }
        //             if (!this.isCbbSaveAndNew) {
        //                 if (this.currentRow1 !== null && this.currentRow1 !== undefined) {
        //                     if (this.details1) {
        //                         this.details1[this.currentRow1].expenseItemID = newExpenseItem.id;
        //                         this.details1[this.currentRow1].expenseItemId = newExpenseItem.id;
        //                         this.details1[this.currentRow1].expenseItem = newExpenseItem;
        //                         this.selectChangeExpenseItem(this.details1[this.currentRow1]);
        //                     } else if (this.details2nd) {
        //                         this.details2nd[this.currentRow1].expenseItemID = newExpenseItem.id;
        //                         this.details2nd[this.currentRow1].expenseItemId = newExpenseItem.id;
        //                         this.details2nd[this.currentRow1].expenseItem = newExpenseItem;
        //                     }
        //                 } else {
        //                 }
        //             }
        //         }
        //     } else {
        //         const newExpenseItem = response.content.data;
        //         if (newExpenseItem.isActive && newExpenseItem.id) {
        //             if (this.expenseItemList) {
        //                 this.expenseItemList.push(newExpenseItem);
        //                 this.expenseItemList = this.expenseItemList.sort((a, b) => a.expenseItemCode.localeCompare(b.expenseItemCode));
        //             }
        //             if (this.expenseItems) {
        //                 this.expenseItems.push(newExpenseItem);
        //                 this.expenseItems = this.expenseItems.sort((a, b) => a.expenseItemCode.localeCompare(b.expenseItemCode));
        //             }
        //             if (!this.isCbbSaveAndNew) {
        //                 if (this.currentRow !== null && this.currentRow !== undefined) {
        //                     if (this.details) {
        //                         this.details[this.currentRow].expenseItemID = newExpenseItem.id;
        //                         this.details[this.currentRow].expenseItemId = newExpenseItem.id;
        //                         this.details[this.currentRow].expenseItem = newExpenseItem;
        //                         this.selectChangeExpenseItem(this.details[this.currentRow]);
        //                     } else if (this.details2nd) {
        //                         this.details2nd[this.currentRow].expenseItemID = newExpenseItem.id;
        //                         this.details2nd[this.currentRow].expenseItemId = newExpenseItem.id;
        //                         this.details2nd[this.currentRow].expenseItem = newExpenseItem;
        //                     }
        //                 } else {
        //                 }
        //             }
        //         }
        //     }
        // }
        //
        // if (
        //     response.content.name === CategoryName.KHACH_HANG ||
        //     response.content.name === CategoryName.NHA_CUNG_CAP ||
        //     response.content.name === CategoryName.DOI_TUONG ||
        //     response.content.name === CategoryName.KHAC ||
        //     (response.content.name === CategoryName.NHAN_VIEN && response.content.typeObject)
        // ) {
        //     const newAccountingObject = response.content.data;
        //     if (newAccountingObject.isActive && newAccountingObject.id) {
        //         if (this.accounting && this.accounting.length > 0) {
        //             this.accounting.push(newAccountingObject);
        //             this.accounting = this.accounting.sort((a, b) => a.accountingObjectCode.localeCompare(b.accountingObjectCode));
        //         }
        //         if (this.parent && this.parent.accountingObjectType !== undefined && this.parent.accountingObjectType !== null) {
        //             if (
        //                 newAccountingObject.objectType === this.parent.accountingObjectType ||
        //                 newAccountingObject.objectType === 2 ||
        //                 newAccountingObject.isEmployee
        //             ) {
        //                 this.accountingObjects.push(newAccountingObject);
        //                 this.accountingObjects = this.accountingObjects.sort((a, b) =>
        //                     a.accountingObjectCode.localeCompare(b.accountingObjectCode)
        //                 );
        //                 this.accountingObjectBankAccounts = newAccountingObject.accountingObjectBankAccounts;
        //                 // if (!this.isCbbSaveAndNew) {
        //                 if (this.currentRow !== null && this.currentRow !== undefined) {
        //                     this.details[this.currentRow].accountingObjectID = newAccountingObject.id;
        //                     this.details[this.currentRow].accountingObject = newAccountingObject;
        //                 } else {
        //                     this.parent.accountingObject = newAccountingObject;
        //                     this.parent.accountingObjectID = newAccountingObject.id;
        //                     this.parent.accountingObjectId = newAccountingObject.id;
        //                     this.parent.accountingObjectName = newAccountingObject.accountingObjectName;
        //                     this.parent.accountingObjectAddress = newAccountingObject.accountingObjectAddress;
        //                     this.parent.taxCode = newAccountingObject.taxCode;
        //                 }
        //                 // }
        //             }
        //         } else {
        //             this.accountingObjects.push(newAccountingObject);
        //             if (this.accounting && this.accounting.length > 0) {
        //                 this.accounting.push(newAccountingObject);
        //                 this.accounting = this.accounting.sort((a, b) => a.accountingObjectCode.localeCompare(b.accountingObjectCode));
        //             }
        //             this.accountingObjectBankAccounts = newAccountingObject.accountingObjectBankAccounts;
        //             if (!this.isCbbSaveAndNew) {
        //                 if (this.currentRow !== null && this.currentRow !== undefined) {
        //                     this.details[this.currentRow].accountingObjectID = newAccountingObject.id;
        //                     this.details[this.currentRow].accountingObject = newAccountingObject;
        //                     if (this.constructor.name === 'TabInsertUpdateReceiptMuaDichVuComponent') {
        //                         this.details[this.currentRow].postedObjectId = newAccountingObject.id;
        //                     }
        //                 } else {
        //                     // if (this.parent.accountingObjectID) {
        //                     this.parent.accountingObjectID = newAccountingObject.id;
        //                     // }
        //                     this.parent.accountingObject = newAccountingObject;
        //                     this.parent.accountingObjectID = newAccountingObject.id;
        //                     this.parent.accountingObjectId = newAccountingObject.id;
        //                     this.parent.accountingObjectName = newAccountingObject.accountingObjectName;
        //                     this.parent.accountingObjectAddress = newAccountingObject.accountingObjectAddress;
        //                     this.parent.accountingObject.accountingObjectName = newAccountingObject.accountingObjectName;
        //                     this.parent.accountingObject.accountingObjectAddress = newAccountingObject.accountingObjectAddress;
        //                     this.selectAccountingObjects();
        //                     this.selectAccountingObject();
        //                     this.onSelectAccountingObject();
        //                 }
        //             } else {
        //                 this.parent.accountingObject = newAccountingObject;
        //                 this.details = undefined;
        //             }
        //         }
        //     }
        // }
        // if (response.content.name === CategoryName.LOAI_VAT_TU_HANG_HOA) {
        //     const newMaterialGoodsCategories = response.content.data;
        //     if (newMaterialGoodsCategories.isActive && newMaterialGoodsCategories.id) {
        //         this.materialGoodsCategoryPopup.push(newMaterialGoodsCategories);
        //         this.materialGoodsCategoryPopup = this.materialGoodsCategoryPopup.sort((a, b) =>
        //             a.materialGoodsCategoryCode.localeCompare(b.materialGoodsCategoryCode)
        //         );
        //         if (!this.isCbbSaveAndNew) {
        //             if (this.currentRow !== null && this.currentRow !== undefined) {
        //                 this.details[this.currentRow].materialGoodsCategoryID = newMaterialGoodsCategories.id;
        //             } else {
        //                 this.parent.materialGoodsCategoryID = newMaterialGoodsCategories.id;
        //             }
        //         }
        //     }
        // }
        // if (response.content.name === CategoryName.VAT_TU_HANG_HOA) {
        //     if (this.selected === 2) {
        //         const newMaterialGoods = response.content.data;
        //         if (newMaterialGoods.isActive && newMaterialGoods.id) {
        //             if (this.materialGoodss) {
        //                 this.materialGoodss.push(newMaterialGoods);
        //             }
        //             if (!this.isCbbSaveAndNew) {
        //                 if (this.currentRow1 !== null && this.currentRow1 !== undefined) {
        //                     this.details1[this.currentRow1].materialGoodsID = newMaterialGoods.id;
        //                     this.details1[this.currentRow1].mgForPPOderItem = newMaterialGoods;
        //                     this.details1[this.currentRow1].description = newMaterialGoods.materialGoodsName;
        //                     this.details1[this.currentRow1].materialGoodsName = newMaterialGoods.materialGoodsName;
        //                     this.details1[this.currentRow1].materialGoodsCode = newMaterialGoods.materialGoodsCode;
        //                     this.selectChangeMaterialGoods(this.details1[this.currentRow1]);
        //                     this.selectedMaterialGoods(this.currentRow1);
        //                     this.getListUnits(true);
        //                 } else if (this.parent && this.costSet) {
        //                     this.parent.materialGoodsID = newMaterialGoods.id;
        //                     this.parent.materialGoodsName = newMaterialGoods.materialGoodsName;
        //                     this.parent.materialGoodsCode = newMaterialGoods.materialGoodsCode;
        //                     this.costSet.materialGoods = newMaterialGoods;
        //                     this.selectOnChangeMaterialGoods();
        //                 }
        //             } else {
        //                 if (this.currentRow1 !== null && this.currentRow1 !== undefined) {
        //                     this.details1[this.currentRow1].materialGoods = newMaterialGoods;
        //                     this.details1[this.currentRow1].description = newMaterialGoods.materialGoodsName;
        //                 }
        //             }
        //         }
        //     } else {
        //         const newMaterialGoods = response.content.data;
        //         if (
        //             newMaterialGoods.isActive &&
        //             newMaterialGoods.id &&
        //             (this.checkType !== 'Mua dịch vụ' || newMaterialGoods.materialGoodsType === 2)
        //         ) {
        //             if (this.materialGoodsInStock && this.materialGoodsInStock.length > 0) {
        //                 this.materialGoodsInStock.push(newMaterialGoods);
        //             } else {
        //                 if (this.materialGoodss) {
        //                     this.materialGoodss.push(newMaterialGoods);
        //                 }
        //             }
        //             if (!this.isCbbSaveAndNew) {
        //                 if (this.currentRow !== null && this.currentRow !== undefined) {
        //                     this.details[this.currentRow].materialGoods = newMaterialGoods;
        //                     this.details[this.currentRow].materialGoodsId = newMaterialGoods.id;
        //                     this.details[this.currentRow].materialGoodsID = newMaterialGoods.id;
        //                     this.details[this.currentRow].mgForPPOderItem = newMaterialGoods;
        //                     this.details[this.currentRow].description = newMaterialGoods.materialGoodsName;
        //                     this.details[this.currentRow].materialGoodsName = newMaterialGoods.materialGoodsName;
        //                     this.details[this.currentRow].materialGoodsCode = newMaterialGoods.materialGoodsCode;
        //                     this.details[this.currentRow].materialGood = newMaterialGoods;
        //                     this.selectChangeMaterialGoods(this.details[this.currentRow]);
        //                     this.selectedMaterialGoods(this.currentRow);
        //                     this.getListUnits(true);
        //                 } else if (this.parent && this.costSet) {
        //                     this.parent.materialGoodsID = newMaterialGoods.id;
        //                     this.parent.materialGoodsName = newMaterialGoods.materialGoodsName;
        //                     this.parent.materialGoodsCode = newMaterialGoods.materialGoodsCode;
        //                     this.costSet.materialGoods = newMaterialGoods;
        //                     this.selectOnChangeMaterialGoods();
        //                 } else {
        //                     this.parent.materialGoodsID = newMaterialGoods.id;
        //                     this.parent.materialGoodsName = newMaterialGoods.materialGoodsName;
        //                     this.parent.materialGoodsCode = newMaterialGoods.materialGoodsCode;
        //                     this.selectOnChangeMaterialGoods();
        //                     this.selectedMaterialGoods(null, true);
        //                 }
        //             } else {
        //                 if (this.currentRow !== null && this.currentRow !== undefined) {
        //                     this.details[this.currentRow].materialGoods = newMaterialGoods;
        //                     this.details[this.currentRow].description = newMaterialGoods.materialGoodsName;
        //                 }
        //             }
        //         }
        //     }
        // }
        //
        // if (response.content.name === CategoryName.KHO) {
        //     const newRepository = response.content.data;
        //     if (newRepository.isActive && newRepository.id) {
        //         if (this.repositorys) {
        //             this.repositorys.push(newRepository);
        //             this.repositorys = this.repositorys.sort((a, b) => a.repositoryCode.localeCompare(b.repositoryCode));
        //         }
        //         if (this.repositories) {
        //             this.repositories.push(newRepository);
        //             this.repositories = this.repositories.sort((a, b) => a.repositoryCode.localeCompare(b.repositoryCode));
        //         } else {
        //             this.repositoryPopup.push(newRepository);
        //             this.repositoryPopup = this.repositoryPopup.sort((a, b) => a.repositoryCode.localeCompare(b.repositoryCode));
        //         }
        //         if (!this.isCbbSaveAndNew) {
        //             if (this.currentRow !== null && this.currentRow !== undefined) {
        //                 this.details[this.currentRow].repositoryID = newRepository.id;
        //                 this.details[this.currentRow].repositoryId = newRepository.id;
        //                 this.details[this.currentRow].repository = newRepository;
        //                 if (this.checkRepository) {
        //                     this.details[this.currentRow].fromRepositoryID = newRepository.id;
        //                 } else {
        //                     this.details[this.currentRow].toRepositoryID = newRepository.id;
        //                 }
        //                 this.onSelectRepository(this.details[this.currentRow]);
        //             } else {
        //                 this.parent.repositoryID = newRepository.id;
        //             }
        //         } else {
        //             this.parent.repositoryID = newRepository.id;
        //         }
        //     }
        // }
        // if (response.content.name === CategoryName.DON_VI_TINH) {
        //     const newUnit = response.content.data;
        //     if (newUnit.isActive && newUnit.id) {
        //         if (this.units) {
        //             this.units.push(newUnit);
        //             this.units = this.units.sort((a, b) => a.unitName.localeCompare(b.unitName));
        //         }
        //         if (!this.isCbbSaveAndNew) {
        //             if (this.currentRow !== null && this.currentRow !== undefined) {
        //                 this.details[this.currentRow].unitID = newUnit.id;
        //                 this.details[this.currentRow].unitId = newUnit.id;
        //                 this.details[this.currentRow].unit = newUnit;
        //             } else {
        //                 if (this.parent) {
        //                     this.parent.unitID = newUnit.id;
        //                 }
        //             }
        //         } else {
        //             this.parent.unitID = newUnit.id;
        //         }
        //     }
        // }
        // if (response.content.name === CategoryName.NHAN_VIEN && !response.content.typeObject) {
        //     const newEmployee = response.content.data;
        //     if (newEmployee.isActive && newEmployee.id) {
        //         this.employees.push(newEmployee);
        //         this.employees = this.employees.sort((a, b) => a.accountingObjectCode.localeCompare(b.accountingObjectCode));
        //         if (this.accounting && this.accounting.length > 0) {
        //             this.accounting.push(newEmployee);
        //             this.accounting = this.accounting.sort((a, b) => a.accountingObjectCode.localeCompare(b.accountingObjectCode));
        //         }
        //         if (!this.isCbbSaveAndNew) {
        //             if (this.currentRow !== null && this.currentRow !== undefined) {
        //                 this.details[this.currentRow].employeeID = newEmployee.id;
        //                 this.details[this.currentRow].employeeId = newEmployee.id;
        //                 this.details[this.currentRow].accountingObjectID = newEmployee.id;
        //                 this.details[this.currentRow].accountingObject = newEmployee;
        //                 this.selectEmployeeMC(this.details[this.currentRow]);
        //             } else if (this.parent) {
        //                 this.parent.employeeID = newEmployee.id;
        //                 this.parent.employeeId = newEmployee.id;
        //                 this.parent.accountingObjectIsEmployee = newEmployee;
        //                 this.parent.accountingObjectEmployee = newEmployee;
        //                 this.parent.employee = newEmployee;
        //                 this.selectEmployee();
        //             } else {
        //                 this.nhanVienID = newEmployee.id;
        //             }
        //         }
        //     }
        // }
        // if (response.content.name === CategoryName.PHUONG_THUC_VAN_CHUYEN) {
        //     const newTransportMethod = response.content.data;
        //     if (newTransportMethod.isActive && newTransportMethod.id) {
        //         if (this.transportMethods) {
        //             this.transportMethods.push(newTransportMethod);
        //         }
        //         if (!this.isCbbSaveAndNew) {
        //             if (this.currentRow !== null && this.currentRow !== undefined) {
        //             } else {
        //                 if (this.parent) {
        //                     this.parent.transportMethodID = newTransportMethod.id;
        //                 }
        //             }
        //         }
        //     }
        // }
        // if (response.content.name === CategoryName.LOAI_TSCD) {
        //     const newFixedAssetCategory = response.content.data;
        //     if (newFixedAssetCategory.isActive && newFixedAssetCategory.id) {
        //         if (this.fixedassetcategories) {
        //             this.fixedassetcategories.push(newFixedAssetCategory);
        //         }
        //         if (!this.isCbbSaveAndNew) {
        //             if (this.currentRow !== null && this.currentRow !== undefined) {
        //             } else {
        //                 if (this.parent) {
        //                     this.parent.fixedAssetCategoryID = newFixedAssetCategory.id;
        //                     this.parent.usedTime = newFixedAssetCategory.usedTime;
        //                     this.parent.originalPriceAccount = newFixedAssetCategory.originalPriceAccount;
        //                     this.parent.expenditureAccount = newFixedAssetCategory.expenditureAccount;
        //                     this.parent.depreciationAccount = newFixedAssetCategory.depreciationAccount;
        //                     // this.parent.fixedAssetCategoryID = newFixedAssetCategory;
        //                 }
        //             }
        //         }
        //     }
        // }
        // if (response.content.name === CategoryName.TAI_SAN_CO_DINH) {
        //     const newFixedAsset = response.content.data;
        //     if (newFixedAsset.isActive && newFixedAsset.id) {
        //         if (this.fixedAssets) {
        //             this.fixedAssets.push(newFixedAsset);
        //         }
        //         if (!this.isCbbSaveAndNew) {
        //             if (this.currentRow !== null && this.currentRow !== undefined) {
        //                 this.details[this.currentRow].fixedAssetItem = newFixedAsset;
        //             } else {
        //                 if (this.parent) {
        //                 } else {
        //                 }
        //             }
        //         }
        //     }
        // }
        // if (response.content.name === CategoryName.CONG_CU_DUNG_CU) {
        //     const newtools = response.content.data;
        //     if (newtools.isActive && newtools.id) {
        //         if (this.tools) {
        //             this.tools.push(newtools);
        //         }
        //         if (!this.isCbbSaveAndNew) {
        //             if (this.currentRow !== null && this.currentRow !== undefined) {
        //                 newtools.toolsCode = newtools.toolCode;
        //                 newtools.toolsName = newtools.toolName;
        //                 this.details[this.currentRow].toolsID = newtools.id;
        //                 this.details[this.currentRow].toolsItem = newtools;
        //             } else {
        //                 if (this.parent) {
        //                     this.parent.toolsID = newtools.id;
        //                     // this.parent.tools = newtools;
        //                     // this.changTools(this.parent.tools, this.currentRow);
        //                 }
        //             }
        //         }
        //     }
        // }
        // this.addDataToDetail();
        // this.addDataToDetail2nd();
    }

    getSelectionText() {
        let selectedText = '';
        if (window.getSelection) {
            selectedText = window.getSelection().toString();
        } else if (document.getSelection) {
            selectedText = document.getSelection().toString();
        }
        return selectedText;
    }

    disableInput() {
        if (window.location.href.includes('new') || window.location.href.includes('edit') || this.isEditPage || this.isSpecialForm) {
            const inputs = document.getElementsByTagName('input');
            const buttons = document.getElementsByTagName('button');
            const selects = document.getElementsByTagName('select');
            const toolbar = document.getElementsByClassName(this.toolbarClass ? this.toolbarClass : 'gr-tool-bar2')[0];

            const toolbarButton = toolbar ? toolbar.getElementsByTagName('button') : [];

            const tfoot = document.getElementsByTagName('tfoot');
            for (let i = 0; i < inputs.length; i++) {
                if (
                    !inputs[i].className.includes('noSetDisable') &&
                    !inputs[i].className.includes('noSetDisableCustom') &&
                    !inputs[i].className.includes('modal-body') &&
                    !inputs[i].parentElement.className.includes('modal-body')
                ) {
                    inputs[i].disabled = !this.isEdit;
                    if (!inputs[i].className.includes('noEdit') && !inputs[i].className.includes('noEditOPN')) {
                        if (!window.location.href.includes('thue')) {
                            if (inputs[i].disabled) {
                                if (inputs[i].className.includes('input-Cell')) {
                                    inputs[i].classList.remove('bg-white');
                                    inputs[i].classList.add('bg-inherit');
                                } else {
                                    inputs[i].classList.add('bg-white');
                                }
                            }
                        }
                    } else {
                        inputs[i].disabled = true;
                        if (
                            !inputs[i].parentElement.className.includes('countRow') &&
                            !inputs[i].parentElement.className.includes('color-tfoot') &&
                            !window.location.href.includes('thue') &&
                            !inputs[i].className.includes('noEditOPN')
                        ) {
                            inputs[i].classList.add('bg-white');
                        }
                    }
                } else if (inputs[i].className.includes('noSetDisableCustom')) {
                    inputs[i].disabled = false;
                }
                // xóa background trắng trong input nếu nớ ở otrng combobox và trong table
                if (inputs[i].closest('.div-combobox-div') && inputs[i].closest('td')) {
                    if (inputs[i].classList.contains('bg-white')) {
                        inputs[i].classList.remove('bg-white');
                    }
                }
            }
            for (let i = 0; i < buttons.length; i++) {
                if (
                    !buttons[i].parentElement.className.includes('modal-footer') &&
                    !buttons[i].parentElement.className.includes('modal-body') &&
                    !buttons[i].className.includes('noSetDisable') &&
                    !buttons[i].className.includes('noSetDisableCustom')
                ) {
                    let check = true;
                    for (let j = 0; j < toolbarButton.length; j++) {
                        if (buttons[i] === toolbarButton[j]) {
                            check = false;
                        }
                    }
                    if (check) {
                        buttons[i].disabled = !this.isEdit;
                    }
                    if (buttons[i].className.includes('noEdit')) {
                        buttons[i].disabled = true;
                    }
                    if (buttons[i].className.includes('navbar-toggler')) {
                        buttons[i].disabled = false;
                    }
                } else if (buttons[i].className.includes('noSetDisableCustom')) {
                    buttons[i].disabled = false;
                }
            }
            for (let i = 0; i < selects.length; i++) {
                if (!selects[i].className.includes('noSetDisable')) {
                    selects[i].disabled = !this.isEdit;
                    if (selects[i].className.includes('noEdit')) {
                        selects[i].disabled = true;
                    }
                    if (selects[i].disabled) {
                        selects[i].classList.add('bg-white');
                    }
                }
            }
            if (tfoot) {
                for (let i = 0; i < tfoot.length; i++) {
                    const tfootInput = tfoot[i].getElementsByTagName('input');
                    for (let j = 0; j < tfootInput.length; j++) {
                        tfootInput[j].disabled = true;
                    }
                }
            }
        }
    }

    addDataToDetail() {}

    addDataToDetail2nd() {}

    ngOnDestroy(): void {}

    selectMultiRow(object, evt, objects: any[], isPPBill?) {
        if (evt) {
            if (!isPPBill) {
                evt.preventDefault();
            }
            if (evt.shiftKey) {
                this.selectedRows = [];
                if (this.selectedRow) {
                    const previousIndex = objects.indexOf(this.selectedRow);
                    const thisIndex = objects.indexOf(object);

                    // Nếu lần trước và lần này cùng chọn 1 vị trí, add hoặc remove object tùy theo object không thuộc
                    // hoặc thuộc list

                    if (previousIndex === thisIndex) {
                        const curIndex = this.selectedRows.indexOf(object);
                        if (curIndex !== -1) {
                            this.selectedRows.splice(curIndex, 1);
                        } else {
                            this.selectedRows.push(object);
                        }
                    } else {
                        // Nếu 2 vị trí chọn khác nhau,
                        // sắp xếp vị trí trước sau để chạy vòng for
                        // Nếu lần chọn 2 đã thuộc list selected thì loại bỏ tất cả khỏi list selected
                        // Nếu lần chọn 2 không thuộc list selected thì thêm tất cả vào list selected
                        let start = 0;
                        let end = 0;
                        if (thisIndex >= previousIndex) {
                            start = previousIndex;
                            end = thisIndex;
                        } else {
                            start = thisIndex;
                            end = previousIndex;
                        }
                        const curIndex = this.selectedRows.indexOf(object);

                        for (let i = start; i <= end; i++) {
                            if (curIndex === -1) {
                                this.selectedRows.push(objects[i]);
                            } else {
                                for (let j = 0; j < this.selectedRows.length; j++) {
                                    if (this.selectedRows[j].id === objects[i].id) {
                                        this.selectedRows.splice(j, 1);
                                        j--;
                                    }
                                }
                            }
                        }
                    }
                    // phuonghv: Chỉ select từ phần tử đầu tiên khi nhấm shift
                    // this.selectedRow = object;
                } else {
                    this.selectedRows = [];
                    this.selectedRows.push(object);
                    this.selectedRow = object;
                }
            } else if (evt.ctrlKey || evt.metaKey) {
                const _index = this.selectedRows.indexOf(object);
                if (_index !== -1) {
                    this.selectedRows.splice(_index, 1);
                } else {
                    this.selectedRows.push(object);
                }
                this.selectedRow = object;
            } else {
                this.selectedRows = [];
                this.selectedRows.push(object);
                this.selectedRow = object;
            }
        } else {
            this.selectedRows = [];
            this.selectedRows.push(object);
            this.selectedRow = object;
        }
        this.onSelect(object);
    }

    onSelect(object?, any?) {}

    moveArrowDown($event, count) {
        if (document.getElementsByClassName('cbb-table')[0]) {
            return;
        }
        const inputs = document.getElementsByTagName('input');
        const selects = document.getElementsByTagName('select');
        const is = document.getElementsByTagName('i');
        const curIndex = $event.path['0'].tabIndex;
        let nextIndex = curIndex;
        let j = 0;
        let isFound = false;
        let next = false;
        while (j < 15) {
            j++;
            nextIndex = curIndex + (next ? count * j : count);
            for (let i = 0; i < inputs.length; i++) {
                // loop over the tabs.
                if (inputs[i].tabIndex === nextIndex) {
                    // is this our tab?
                    if (inputs[i].disabled || inputs[i].className.includes('disable')) {
                        next = true;
                        break;
                    } else {
                        inputs[i].focus(); // Focus and leave.
                        inputs[i].select(); // Focus and leave.
                        isFound = true;
                        break;
                    }
                }
            }
            for (let i = 0; i < selects.length; i++) {
                // loop over the tabs.
                if (selects[i].tabIndex === nextIndex && !selects[i].disabled) {
                    // is this our tab?
                    selects[i].focus(); // Focus and leave.
                    selects[i].select(); // Focus and leave.
                    isFound = true;
                    break;
                }
            }
            for (let k = 0; k < is.length; k++) {
                // loop over the tabs.
                if (is[k].tabIndex === nextIndex) {
                    // is this our tab?
                    is[k].focus(); // Focus and leave.
                    isFound = true;
                    break;
                }
            }
            if (isFound) {
                break;
            }
        }
    }

    moveArrowUp($event, count) {
        if (document.getElementsByClassName('cbb-table')[0]) {
            return;
        }
        const inputs = document.getElementsByTagName('input');
        const selects = document.getElementsByTagName('select');
        const is = document.getElementsByTagName('i');
        const curIndex = $event.path['0'].tabIndex;
        let nextIndex = curIndex;
        let j = 0;
        let isFound = false;
        let next = false;
        while (j < 15) {
            j++;
            nextIndex = curIndex - (next ? count * j : count);
            for (let i = 0; i < inputs.length; i++) {
                // loop over the tabs.
                if (inputs[i].tabIndex === nextIndex) {
                    // is this our tab?
                    if (inputs[i].disabled || inputs[i].className.includes('disable')) {
                        next = true;
                        break;
                    } else {
                        inputs[i].focus(); // Focus and leave.
                        inputs[i].select(); // Focus and leave.
                        isFound = true;
                        break;
                    }
                }
            }
            for (let i = 0; i < selects.length; i++) {
                // loop over the tabs.
                if (selects[i].tabIndex === nextIndex && !selects[i].disabled) {
                    // is this our tab?
                    selects[i].focus(); // Focus and leave.
                    selects[i].select(); // Focus and leave.
                    isFound = true;
                    break;
                }
            }
            for (let k = 0; k < is.length; k++) {
                // loop over the tabs.
                if (is[k].tabIndex === nextIndex) {
                    // is this our tab?
                    is[k].focus(); // Focus and leave.
                    isFound = true;
                    break;
                }
            }
            if (isFound) {
                break;
            }
        }
    }

    updateScroll(scrollHeader, scrollBody, scrollFooter) {
        // do logic and set
        if (scrollHeader && scrollBody && scrollFooter) {
            scrollHeader.scrollLeft = scrollBody.scrollLeft;
            scrollFooter.scrollLeft = scrollBody.scrollLeft;
        }
    }

    afterDataChange(event) {
        switch (event.nameCategory) {
            case CategoryName.DOI_TUONG:
            case CategoryName.KHACH_HANG:
            case CategoryName.NHA_CUNG_CAP:
            case CategoryName.KHAC:
                if (!this.accountingObjects) {
                    this.accountingObjects = event.data;
                } else {
                    this.accountingObjects.push(...event.data);
                }
                this.accountingObjects = Array.from(new Set(this.accountingObjects.map(item => item)));
                break;
            case CategoryName.NHAN_VIEN:
                if (event.typeObject) {
                    if (!this.accountingObjects) {
                        this.accountingObjects = event.data;
                    } else {
                        this.accountingObjects.push(...event.data);
                    }
                    this.accountingObjects = Array.from(new Set(this.accountingObjects.map(item => item)));
                } else {
                    if (!this.employees) {
                        this.employees = event.data;
                    } else {
                        this.employees.push(...event.data);
                    }
                    this.employees = Array.from(new Set(this.employees.map(item => item)));
                }
                break;
            case CategoryName.VAT_TU_HANG_HOA:
                if (!this.materialGoodss) {
                    this.materialGoodss = event.data;
                } else {
                    this.materialGoodss.push(...event.data);
                }
                this.materialGoodss = Array.from(new Set(this.materialGoodss.map(item => item)));
                break;
            case CategoryName.KHO:
                if (!this.repositories) {
                    this.repositories = event.data;
                } else {
                    this.repositories.push(...event.data);
                }
                this.repositories = Array.from(new Set(this.repositories.map(item => item)));
                break;
            case CategoryName.TAI_KHOAN_NGAN_HANG:
                if (!this.bankAccountDetails) {
                    this.bankAccountDetails = event.data;
                } else {
                    this.bankAccountDetails.push(...event.data);
                }
                this.bankAccountDetails = Array.from(new Set(this.bankAccountDetails.map(item => item)));
                break;
            case CategoryName.KHOAN_MUC_CHI_PHI:
                if (!this.expenseItems) {
                    this.expenseItems = event.data;
                } else {
                    this.expenseItems.push(...event.data);
                }
                this.expenseItems = Array.from(new Set(this.expenseItems.map(item => item)));
                break;
            case CategoryName.MUC_THU_CHI:
                if (!this.budgetItems) {
                    this.budgetItems = event.data;
                } else {
                    this.budgetItems.push(...event.data);
                }
                this.budgetItems = Array.from(new Set(this.budgetItems.map(item => item)));
                break;
            case CategoryName.PHONG_BAN:
                if (!this.organizationUnits) {
                    this.organizationUnits = event.data;
                } else {
                    this.organizationUnits.push(...event.data);
                }
                this.organizationUnits = Array.from(new Set(this.organizationUnits.map(item => item)));
                break;
            case CategoryName.DOI_TUONG_TAP_HOP_CHI_PHI:
                if (!this.costSets) {
                    this.costSets = event.data;
                } else {
                    this.costSets.push(...event.data);
                }
                this.costSets = Array.from(new Set(this.costSets.map(item => item)));
                break;
            case CategoryName.HOP_DONG:
                if (!this.eMContracts) {
                    this.eMContracts = event.data;
                } else {
                    this.eMContracts.push(...event.data);
                }
                this.eMContracts = Array.from(new Set(this.eMContracts.map(item => item)));
                break;
            case CategoryName.LENH_SAN_XUAT:
                if (!this.rsProductionOrders) {
                    this.rsProductionOrders = event.data;
                } else {
                    this.rsProductionOrders.push(...event.data);
                }
                this.rsProductionOrders = Array.from(new Set(this.rsProductionOrders.map(item => item)));
                break;
            case CategoryName.MA_THONG_KE:
                if (!this.statisticCodes) {
                    this.statisticCodes = event.data;
                } else {
                    this.statisticCodes.push(...event.data);
                }
                this.statisticCodes = Array.from(new Set(this.statisticCodes.map(item => item)));
                break;
            // case this.utilsService.PHONG_BAN_DOI_TUONG_TAP_HOP_CHI_PHI:
            //     this.prepaidExpenseCodeDTOs = event.data;
            //     break;
            case CategoryName.DINH_MUC_NVL:
                if (!this.objectsMaterialQuantum) {
                    this.objectsMaterialQuantum = event.data;
                } else {
                    this.objectsMaterialQuantum.push(...event.data);
                }
                this.objectsMaterialQuantum = Array.from(new Set(this.objectsMaterialQuantum.map(item => item)));
                break;
            // case CategoryName.TAI_SAN_CO_DINH:
            //     if (!this.fixedAssets) {
            //         this.fixedAssets = event.data;
            //     } else {
            //         this.fixedAssets.push(...event.data);
            //     }
            //     this.fixedAssets = Array.from(new Set(this.objectsMaterialQuantum.map(item => item)));
            //     break;
            // case CategoryName.CONG_CU_DUNG_CU:
            //     if (!this.tools) {
            //         this.tools = event.data;
            //     } else {
            //         this.tools.push(...event.data);
            //     }
            //     this.tools = Array.from(new Set(this.tools.map(item => item)));
            //     break;
            case CategoryName.BIEU_THUE_TAI_NGUYEN:
                if (!this.materialGoodsResourceTaxGroups) {
                    this.materialGoodsResourceTaxGroups = event.data;
                } else {
                    this.materialGoodsResourceTaxGroups.push(...event.data);
                }
                this.materialGoodsResourceTaxGroups = Array.from(new Set(this.materialGoodsResourceTaxGroups.map(item => item)));
                break;
            case CategoryName.CBB_CCTC:
                this.allOrganizationUnits = event.data;
                this.allOrganizationUnits = Array.from(new Set(this.allOrganizationUnits.map(item => item)));
                break;
        }
    }

    loadDataDefaultForOpenVoucher(lstDataDetailForSetDefault: any[]) {
        /*if (!this.objectsDetails || !this.object) {
            return;
        }*/
        let lstID_AccountingObject = [];
        let lstID_Employee = [];
        let lstID_BankAccountDetail = [];
        let lstID_BudgetItem = [];
        let lstID_CostSet = [];
        let lstID_Contract = [];
        let lstID_RSProductionOrder = [];
        let lstID_Department = [];
        let lstID_ExpenseItem = [];
        let lstID_StatisticsCode = [];
        let lstID_MaterialGoods = [];
        let lstID_DepartmentCostSet = [];
        let lstID_Repositories = [];
        let lstID_ObjectMaterialQuantums = [];
        let lstID_FixedAssets = [];
        let lstID_Tools = [];
        let lstID_MaterialGoodsResourceTaxGroups = [];
        let lstID_DepartmentCCTC = [];
        let lstID_FixedAssetCategories = [];
        if (lstDataDetailForSetDefault && lstDataDetailForSetDefault.length > 0) {
            lstDataDetailForSetDefault.forEach(detail => {
                if (detail && detail.length > 0) {
                    if (detail[0].hasOwnProperty('accountingObjectID')) {
                        lstID_AccountingObject.push(...Array.from(new Set(detail.map(item => item['accountingObjectID']))));
                    } else if (detail[0].hasOwnProperty('accountingObjectId')) {
                        lstID_AccountingObject.push(...Array.from(new Set(detail.map(item => item['accountingObjectId']))));
                    } else if (detail[0].hasOwnProperty('accountingObject')) {
                        lstID_AccountingObject.push(
                            ...Array.from(new Set(detail.map(item => item['accountingObject'] && item['accountingObject']['id'])))
                        );
                    } else if (detail[0].hasOwnProperty('postedObjectId')) {
                        lstID_AccountingObject.push(...Array.from(new Set(detail.map(item => item['postedObjectId']))));
                    }

                    if (detail[0].hasOwnProperty('creditAccountingObjectID')) {
                        lstID_AccountingObject.push(...Array.from(new Set(detail.map(item => item['creditAccountingObjectID']))));
                    }
                    if (detail[0].hasOwnProperty('debitAccountingObjectID')) {
                        lstID_AccountingObject.push(...Array.from(new Set(detail.map(item => item['debitAccountingObjectID']))));
                    }

                    lstID_Employee.push(...Array.from(new Set(detail.map(item => item['employeeID']))));

                    if (detail[0].hasOwnProperty('accountingObjectID') && detail[0].hasOwnProperty('role')) {
                        lstID_Employee.push(...Array.from(new Set(detail.map(item => item['accountingObjectID']))));
                    }

                    if (detail[0].hasOwnProperty('bankAccountDetails')) {
                        lstID_BankAccountDetail.push(
                            ...Array.from(new Set(detail.map(item => item['bankAccountDetails'] && item['bankAccountDetails']['id'])))
                        );
                    } else if (detail[0].hasOwnProperty('bankAccountDetailID')) {
                        lstID_BankAccountDetail.push(...Array.from(new Set(detail.map(item => item['bankAccountDetailID']))));
                    } else if (detail[0].hasOwnProperty('bankAccountDetailId')) {
                        lstID_BankAccountDetail.push(...Array.from(new Set(detail.map(item => item['bankAccountDetailId']))));
                    }
                    if (detail[0].hasOwnProperty('fromBankAccountDetailID')) {
                        lstID_BankAccountDetail.push(...Array.from(new Set(detail.map(item => item['fromBankAccountDetailID']))));
                    }
                    if (detail[0].hasOwnProperty('toBankAccountDetailID')) {
                        lstID_BankAccountDetail.push(...Array.from(new Set(detail.map(item => item['toBankAccountDetailID']))));
                    }

                    if (detail[0].hasOwnProperty('budgetItem')) {
                        lstID_BudgetItem.push(...Array.from(new Set(detail.map(item => item['budgetItem'] && item['budgetItem']['id']))));
                    } else if (detail[0].hasOwnProperty('budgetItemID')) {
                        lstID_BudgetItem.push(...Array.from(new Set(detail.map(item => item['budgetItemID']))));
                    } else if (detail[0].hasOwnProperty('budgetItemId')) {
                        lstID_BudgetItem.push(...Array.from(new Set(detail.map(item => item['budgetItemId']))));
                    }

                    if (detail[0].hasOwnProperty('costSet')) {
                        lstID_CostSet.push(...Array.from(new Set(detail.map(item => item['costSet'] && item['costSet']['id']))));
                    } else if (detail[0].hasOwnProperty('costSetID')) {
                        lstID_CostSet.push(...Array.from(new Set(detail.map(item => item['costSetID']))));
                    } else if (detail[0].hasOwnProperty('costSetId')) {
                        lstID_CostSet.push(...Array.from(new Set(detail.map(item => item['costSetId']))));
                    }

                    if (detail[0].hasOwnProperty('contractID')) {
                        lstID_Contract.push(...Array.from(new Set(detail.map(item => item['contractID']))));
                    } else if (detail[0].hasOwnProperty('contractId')) {
                        lstID_Contract.push(...Array.from(new Set(detail.map(item => item['contractId']))));
                    }

                    if (detail[0].hasOwnProperty('organizationUnit')) {
                        lstID_Department.push(
                            ...Array.from(new Set(detail.map(item => item['organizationUnit'] && item['organizationUnit']['id'])))
                        );
                    } else if (detail[0].hasOwnProperty('departmentID')) {
                        lstID_Department.push(...Array.from(new Set(detail.map(item => item['departmentID']))));
                    } else if (detail[0].hasOwnProperty('departmentId')) {
                        lstID_Department.push(...Array.from(new Set(detail.map(item => item['departmentId']))));
                    } else if (detail[0].hasOwnProperty('toDepartmentID')) {
                        lstID_Department.push(...Array.from(new Set(detail.map(item => item['toDepartmentID']))));
                        lstID_Department.push(...Array.from(new Set(detail.map(item => item['fromDepartmentID']))));
                    } else if (detail[0].hasOwnProperty('fromDepartmentID')) {
                        lstID_Department.push(...Array.from(new Set(detail.map(item => item['toDepartmentID']))));
                        lstID_Department.push(...Array.from(new Set(detail.map(item => item['fromDepartmentID']))));
                    } else if (detail[0].hasOwnProperty('fromDepartmentId')) {
                        lstID_Department.push(...Array.from(new Set(detail.map(item => item['toDepartmentId']))));
                        lstID_Department.push(...Array.from(new Set(detail.map(item => item['fromDepartmentId']))));
                    } else if (detail[0].hasOwnProperty('toDepartmentId')) {
                        lstID_Department.push(...Array.from(new Set(detail.map(item => item['toDepartmentId']))));
                        lstID_Department.push(...Array.from(new Set(detail.map(item => item['fromDepartmentId']))));
                    }

                    if (detail[0].hasOwnProperty('companyID')) {
                        lstID_Department.push(...Array.from(new Set(detail.map(item => item['companyID']))));
                    }

                    if (detail[0].hasOwnProperty('expenseItemID')) {
                        lstID_ExpenseItem.push(...Array.from(new Set(detail.map(item => item['expenseItemID']))));
                    } else if (detail[0].hasOwnProperty('expenseItemId')) {
                        lstID_ExpenseItem.push(...Array.from(new Set(detail.map(item => item['expenseItemId']))));
                    } else if (detail[0].hasOwnProperty('expenseItem')) {
                        lstID_ExpenseItem.push(
                            ...Array.from(new Set(detail.map(item => item['expenseItem'] && item['expenseItem']['id'])))
                        );
                    }

                    if (detail[0].hasOwnProperty('statisticsId')) {
                        lstID_StatisticsCode.push(...Array.from(new Set(detail.map(item => item['statisticsId']))));
                    }
                    if (detail[0].hasOwnProperty('statisticsCodeID')) {
                        lstID_StatisticsCode.push(...Array.from(new Set(detail.map(item => item['statisticsCodeID']))));
                    } else if (detail[0].hasOwnProperty('statisticsCodeId')) {
                        lstID_StatisticsCode.push(...Array.from(new Set(detail.map(item => item['statisticsCodeId']))));
                    } else if (detail[0].hasOwnProperty('statisticCodeId')) {
                        lstID_StatisticsCode.push(...Array.from(new Set(detail.map(item => item['statisticCodeId']))));
                    } else if (detail[0].hasOwnProperty('statisticCodeID')) {
                        lstID_StatisticsCode.push(...Array.from(new Set(detail.map(item => item['statisticCodeID']))));
                    } else if (detail[0].hasOwnProperty('statisticsCode')) {
                        lstID_StatisticsCode.push(
                            ...Array.from(new Set(detail.map(item => item['statisticsCode'] && item['statisticsCode']['id'])))
                        );
                    }

                    if (detail[0].hasOwnProperty('materialGoodsID')) {
                        lstID_MaterialGoods.push(...Array.from(new Set(detail.map(item => item['materialGoodsID']))));
                    } else if (detail[0].hasOwnProperty('materialGoodsId')) {
                        lstID_MaterialGoods.push(...Array.from(new Set(detail.map(item => item['materialGoodsId']))));
                    } else if (detail[0].hasOwnProperty('materialGoods')) {
                        lstID_MaterialGoods.push(
                            ...Array.from(new Set(detail.map(item => item['materialGoods'] && item['materialGoods']['id'])))
                        );
                    } else if (detail[0].hasOwnProperty('materialGood')) {
                        lstID_MaterialGoods.push(...Array.from(new Set(detail.map(item => item['materialGood']['id']))));
                    }

                    if (detail[0].hasOwnProperty('materialAssemblyID')) {
                        lstID_MaterialGoods.push(...Array.from(new Set(detail.map(item => item['materialAssemblyID']))));
                    } else if (detail[0].hasOwnProperty('materialGoodsId')) {
                        lstID_MaterialGoods.push(...Array.from(new Set(detail.map(item => item['materialGoodsId']))));
                    }

                    if (detail[0].hasOwnProperty('repositoryID')) {
                        lstID_Repositories.push(...Array.from(new Set(detail.map(item => item['repositoryID']))));
                    } else if (detail[0].hasOwnProperty('repositoryId')) {
                        lstID_Repositories.push(...Array.from(new Set(detail.map(item => item['repositoryId']))));
                    }

                    if (detail[0].hasOwnProperty('fixedAssetID')) {
                        lstID_FixedAssets.push(...Array.from(new Set(detail.map(item => item['fixedAssetID']))));
                    } else if (detail[0].hasOwnProperty('fixedAssetId')) {
                        lstID_FixedAssets.push(...Array.from(new Set(detail.map(item => item['fixedAssetId']))));
                    } else if (detail[0].hasOwnProperty('fixAssetID')) {
                        lstID_FixedAssets.push(...Array.from(new Set(detail.map(item => item['fixAssetID']))));
                    } else if (detail[0].hasOwnProperty('fixAssetId')) {
                        lstID_FixedAssets.push(...Array.from(new Set(detail.map(item => item['fixAssetId']))));
                    } else if (detail[0].hasOwnProperty('fixedAssetItem')) {
                        lstID_FixedAssets.push(
                            ...Array.from(new Set(detail.map(item => item['fixedAssetItem'] && item['fixedAssetItem']['id'])))
                        );
                    }
                    if (detail[0].hasOwnProperty('toolsID')) {
                        lstID_Tools.push(...Array.from(new Set(detail.map(item => item['toolsID']))));
                    } else if (detail[0].hasOwnProperty('toolsId')) {
                        lstID_Tools.push(...Array.from(new Set(detail.map(item => item['toolsID']))));
                    } else if (detail[0].hasOwnProperty('toolsItem')) {
                        lstID_Tools.push(...Array.from(new Set(detail.map(item => item['toolsItem'] && item['toolsItem']['id']))));
                    }

                    if (detail[0].hasOwnProperty('objectID')) {
                        lstID_ObjectMaterialQuantums.push(...Array.from(new Set(detail.map(item => item['objectID']))));
                        lstID_DepartmentCostSet.push(...Array.from(new Set(detail.map(item => item['objectID']))));
                    } else if (detail[0].hasOwnProperty('objectID')) {
                        lstID_ObjectMaterialQuantums.push(...Array.from(new Set(detail.map(item => item['objectID']))));
                        lstID_DepartmentCostSet.push(...Array.from(new Set(detail.map(item => item['objectID']))));
                    }
                    if (detail[0].hasOwnProperty('allocationObjectID')) {
                        lstID_DepartmentCostSet.push(...Array.from(new Set(detail.map(item => item['allocationObjectID']))));
                    }

                    if (
                        detail[0].hasOwnProperty('objectID') &&
                        detail[0].hasOwnProperty('objectType') &&
                        (detail[0].hasOwnProperty('toolsID') ||
                            detail[0].hasOwnProperty('faInitID') ||
                            detail[0].hasOwnProperty('fixedassetID'))
                    ) {
                        lstID_DepartmentCostSet.push(...Array.from(new Set(detail.map(item => item['objectID']))));
                    }
                    if (detail[0].hasOwnProperty('fromRepositoryID') && detail[0].hasOwnProperty('toRepositoryID')) {
                        lstID_Repositories.push(...Array.from(new Set(detail.map(item => item['fromRepositoryID']))));
                        lstID_Repositories.push(...Array.from(new Set(detail.map(item => item['toRepositoryID']))));
                    }
                    if (detail[0].hasOwnProperty('repository')) {
                        lstID_Repositories.push(
                            ...Array.from(new Set(detail.filter(item => item['repository']).map(item => item['repository']['id'])))
                        );
                    }

                    if (detail[0].hasOwnProperty('materialGoodsResourceTaxGroupID')) {
                        lstID_MaterialGoodsResourceTaxGroups.push(
                            ...Array.from(new Set(detail.map(item => item['materialGoodsResourceTaxGroupID'])))
                        );
                    }
                    if (detail[0].hasOwnProperty('rsProductionOrderID')) {
                        lstID_RSProductionOrder.push(...Array.from(new Set(detail.map(item => item['rsProductionOrderID']))));
                    }
                }
            });
        }
        /* if (this.objectsDetailsSalary && this.objectsDetailsSalary.length > 0){
            if (this.objectsDetailsSalary[0].hasOwnProperty('accountingObject')) {
                lstID_AccountingObject = Array.from(
                    new Set(this.objectsDetailsSalary.map(item => item['accountingObject'] && item['accountingObject']['id']))
                );
            } else if (this.objectsDetailsSalary[0].hasOwnProperty('accountingObjectID')) {
                lstID_AccountingObject = Array.from(new Set(this.objectsDetailsSalary.map(item => item['accountingObjectID'])));
            }
            lstID_Employee = Array.from(new Set(this.objectsDetailsSalary.map(item => item['employeeID'])));
            if (this.objectsDetailsSalary[0].hasOwnProperty('organizationUnit')) {
                lstID_Department = Array.from(
                    new Set(this.objectsDetailsSalary.map(item => item['organizationUnit'] && item['organizationUnit']['id']))
                );
            } else if (this.objectsDetailsSalary[0].hasOwnProperty('departmentID')) {
                lstID_Department = Array.from(new Set(this.objectsDetailsSalary.map(item => item['departmentID'])));
            }
        }

            if (this.objectsDetailsInsurance && this.objectsDetailsInsurance.length > 0){
                if (this.objectsDetailsInsurance[0].hasOwnProperty('accountingObject')) {
                    lstID_AccountingObject = Array.from(
                        new Set(this.objectsDetailsInsurance.map(item => item['accountingObject'] && item['accountingObject']['id']))
                    );
                } else if (this.objectsDetailsInsurance[0].hasOwnProperty('accountingObjectID')) {
                    lstID_AccountingObject = Array.from(new Set(this.objectsDetailsInsurance.map(item => item['accountingObjectID'])));
                }
                lstID_Employee = Array.from(new Set(this.objectsDetailsInsurance.map(item => item['employeeID'])));
                if (this.objectsDetailsInsurance[0].hasOwnProperty('organizationUnit')) {
                    lstID_Department = Array.from(
                        new Set(this.objectsDetailsInsurance.map(item => item['organizationUnit'] && item['organizationUnit']['id']))
                    );
                } else if (this.objectsDetailsInsurance[0].hasOwnProperty('departmentID')) {
                    lstID_Department = Array.from(new Set(this.objectsDetailsInsurance.map(item => item['departmentID'])));
                }
            }*/

        if (this.object) {
            if (this.object.hasOwnProperty('accountingObjectID')) {
                lstID_AccountingObject.push(this.object['accountingObjectID']);
            } else if (this.object.hasOwnProperty('accountingObjectId')) {
                lstID_AccountingObject.push(this.object['accountingObjectId']);
            }
            if (this.object.hasOwnProperty('employeeID')) {
                lstID_Employee.push(this.object['employeeID']);
            } else if (this.object.hasOwnProperty('employeeId')) {
                lstID_Employee.push(this.object['employeeId']);
            }
            if (this.object.hasOwnProperty('bankAccountDetailID')) {
                lstID_BankAccountDetail.push(this.object['bankAccountDetailID']);
            } else if (this.object.hasOwnProperty('bankAccountDetailId')) {
                lstID_BankAccountDetail.push(this.object['bankAccountDetailId']);
            } else if (this.object.hasOwnProperty('accountPaymentId')) {
                lstID_BankAccountDetail.push(this.object['accountPaymentId']);
            }
            if (this.object.hasOwnProperty('departmentID')) {
                lstID_Department.push(this.object['departmentID']);
            }
            if (this.object.hasOwnProperty('todepartmentID')) {
                lstID_Department.push(this.object['todepartmentID']);
                lstID_Department.push(this.object['fromDepartmentID']);
            } else if (this.object.hasOwnProperty('fromDepartmentID')) {
                lstID_Department.push(this.object['todepartmentID']);
                lstID_Department.push(this.object['fromDepartmentID']);
            }
            if (this.object.hasOwnProperty('departmentId')) {
                lstID_Department.push(this.object['departmentId']);
            }
            if (this.object.hasOwnProperty('costSetID')) {
                lstID_CostSet.push(this.object['costSetID']);
            }
            if (this.object.hasOwnProperty('budgetItemID')) {
                lstID_BudgetItem.push(this.object['budgetItemID']);
            }
            if (this.object.hasOwnProperty('statisticsCodeID')) {
                lstID_StatisticsCode.push(this.object['statisticsCodeID']);
            }
            if (this.object.hasOwnProperty('objectID')) {
                lstID_ObjectMaterialQuantums.push(this.object['objectID']);
            }
            if (this.object.hasOwnProperty('materialGoods')) {
                lstID_MaterialGoods.push(this.object['materialGoods']);
            }
            if (this.object.hasOwnProperty('materialGoodsID')) {
                lstID_MaterialGoods.push(this.object['materialGoodsID']);
            }

            if (this.object.hasOwnProperty('repositoryID')) {
                lstID_Repositories.push(this.object['repositoryID']);
            }

            if (this.object.hasOwnProperty('parentID')) {
                lstID_DepartmentCCTC.push(this.object['parentID']);
            }

            if (this.object.hasOwnProperty('fixedAssetCategoryID')) {
                lstID_FixedAssetCategories.push(this.object['fixedAssetCategoryID']);
            }

            // if (this.object.hasOwnProperty('fixedAssetID')) {
            //     lstID_FixedAssets.push(...Array.from(new Set(this.object.map(item => item['fixedAssetID']))));
            // } else if (this.object.hasOwnProperty('fixedAssetId')) {
            //     lstID_FixedAssets.push(...Array.from(new Set(this.object.map(item => item['fixedAssetId']))));
            // } else if (this.object.hasOwnProperty('fixedAssetItem')) {
            //     lstID_FixedAssets.push(
            //         ...Array.from(new Set(this.object.map(item => item['fixedAssetItem'] && item['fixedAssetItem']['id'])))
            //     );
            // }
        }
        if (this.accountingObjectID) {
            lstID_AccountingObject.push(this.accountingObjectID);
        } else if (this.accountingObjectId) {
            lstID_AccountingObject.push(this.accountingObjectId);
        }
        if (this.employeeID) {
            lstID_Employee.push(this.employeeID);
        }
        // distin
        lstID_AccountingObject = Array.from(new Set(lstID_AccountingObject.map(item => item)));
        lstID_Employee = Array.from(new Set(lstID_Employee.map(item => item)));
        lstID_BankAccountDetail = Array.from(new Set(lstID_BankAccountDetail.map(item => item)));
        lstID_BudgetItem = Array.from(new Set(lstID_BudgetItem.map(item => item)));
        lstID_CostSet = Array.from(new Set(lstID_CostSet.map(item => item)));
        lstID_Contract = Array.from(new Set(lstID_Contract.map(item => item)));
        lstID_RSProductionOrder = Array.from(new Set(lstID_RSProductionOrder.map(item => item)));
        lstID_Department = Array.from(new Set(lstID_Department.map(item => item)));
        lstID_ExpenseItem = Array.from(new Set(lstID_ExpenseItem.map(item => item)));
        lstID_StatisticsCode = Array.from(new Set(lstID_StatisticsCode.map(item => item)));
        lstID_MaterialGoods = Array.from(new Set(lstID_MaterialGoods.map(item => item)));
        lstID_DepartmentCostSet = Array.from(new Set(lstID_DepartmentCostSet.map(item => item)));
        lstID_Repositories = Array.from(new Set(lstID_Repositories.map(item => item)));
        lstID_ObjectMaterialQuantums = Array.from(new Set(lstID_ObjectMaterialQuantums.map(item => item)));
        lstID_FixedAssets = Array.from(new Set(lstID_FixedAssets.map(item => item)));
        lstID_Tools = Array.from(new Set(lstID_Tools.map(item => item)));
        lstID_MaterialGoodsResourceTaxGroups = Array.from(new Set(lstID_MaterialGoodsResourceTaxGroups.map(item => item)));
        lstID_DepartmentCCTC = Array.from(new Set(lstID_DepartmentCCTC.map(item => item)));
        lstID_FixedAssetCategories = Array.from(new Set(lstID_FixedAssetCategories.map(item => item)));

        this.utilsService
            .getDataDefaultForOpenVoucher({
                lstIDAccountingObject: lstID_AccountingObject,
                lstIDEmployee: lstID_Employee,
                lstIDBankAccountDetail: lstID_BankAccountDetail,
                lstIDBudgetItem: lstID_BudgetItem,
                lstIDCostSet: lstID_CostSet,
                lstIDContract: lstID_Contract,
                lstIDRSProductionOrder: lstID_RSProductionOrder,
                lstIDDepartment: lstID_Department,
                lstIDExpenseItem: lstID_ExpenseItem,
                lstIDStatisticsCode: lstID_StatisticsCode,
                lstIDMaterialGoods: lstID_MaterialGoods,
                lstIDDepartmentCostSet: lstID_DepartmentCostSet,
                lstIDRepositories: lstID_Repositories,
                lstObjectsMaterialQuantum: lstID_ObjectMaterialQuantums,
                lstIDFixedAssets: lstID_FixedAssets,
                lstIDTools: lstID_Tools,
                lstMaterialGoodsResourceTaxGroups: lstID_MaterialGoodsResourceTaxGroups,
                lstIDDepartmentCCTC: lstID_DepartmentCCTC,
                typeCbbCCTC: this.typeCbb,
                lstIDFixedAssetCategory: lstID_FixedAssetCategories
            })
            .subscribe((res: HttpResponse<any>) => {
                this.accountingObjects = res.body.listAccountingObjectDTOS;
                this.employees = res.body.listEmployees;
                this.bankAccountDetails = res.body.listBankAccountDetailDTOS;
                this.budgetItems = res.body.listBudgetItems;
                this.costSets = res.body.listCostSets;
                this.eMContracts = res.body.listEMEmContracts;
                this.rsProductionOrders = res.body.listRSProductionOrders;
                if (res.body.listDepartment.length > 0) {
                    this.organizationUnits = res.body.listDepartment;
                }
                this.expenseItems = res.body.listExpenseItem;
                this.statisticCodes = res.body.listStatisticsCodes;
                this.materialGoodss = res.body.listMaterialGoods;
                this.materialGoodsInStock = res.body.listMaterialGoods;
                this.prepaidExpenseCodeDTOs = res.body.listPrepaidExpenseCodeDTOS;
                this.repositories = res.body.listRepositories;
                this.objectsMaterialQuantum = res.body.lstObjectsMaterialQuantum;
                // this.fixedAssets = res.body.lstFixedAssets;
                // this.tools = res.body.lstTools;
                // this.allOrganizationUnits = res.body.lstIDDepartmentCCTC;
                // // this.fixedassetcategories = res.body.lstFixedAssetCategory;
                // if (this.fixedAssets) {
                //     this.getFixedAssets();
                // }
                // if (this.tools) {
                //     this.getTools();
                // }
                // this.materialGoodsResourceTaxGroups = res.body.lstMaterialGoodsResourceTaxGroups;
                // if (this.prepaidExpenseCodeDTOs) {
                //     this.getPrepaidExpenseCodeDTOs();
                // }
                // if (this.materialGoodss) {
                //     this.getMaterialGoods();
                // }
                if (this.employees) {
                    this.getEmployees();
                }
            });
    }

    getEmployees() {}

    focusFirstInput() {
        const inputs = document.getElementsByTagName('input');
        if (
            window.location.href.includes('supplier') ||
            window.location.href.includes('accounting-object') ||
            window.location.href.includes('repository') ||
            window.location.href.includes('ky-hieu-cham-cong') ||
            window.location.href.includes('hang-ban/tra-lai') ||
            window.location.href.includes('hang-ban/giam-gia') ||
            window.location.href.includes('kho')
        ) {
            inputs[1].focus();
        } else if (
            window.location.href.includes('mua-hang/qua-kho') ||
            window.location.href.includes('mua-hang/khong-qua-kho') ||
            window.location.href.includes('mua-dich-vu') ||
            window.location.href.includes('chung-tu-ban-hang') ||
            window.location.href.includes('xuat-hoa-don') ||
            window.location.href.includes('thu-tien-khach-hang')
        ) {
            inputs[2].focus();
        } else {
            inputs[0].focus();
        }
        if (
            window.location.href.includes('he-thong-tai-khoan/new') ||
            window.location.href.includes('tai-khoan-ket-chuyen') ||
            window.location.href.includes('dinh-khoan-tu-dong')
        ) {
            inputs[0].focus();
        }
        if (window.location.href.includes('he-thong-tai-khoan') && !window.location.href.includes('he-thong-tai-khoan/new')) {
            inputs[1].focus();
        }
    }

    checkCloseBook(account, postedDate: any, selectMulti?: boolean): Boolean {
        if (selectMulti && this.selectedRows.length > 1) {
            return false;
        }

        if (postedDate && !(postedDate instanceof moment) && postedDate.toString().includes('/')) {
            postedDate = moment(postedDate, DATE_FORMAT_SLASH);
        }
        if (account && postedDate) {
            if (!(postedDate instanceof moment)) {
                postedDate = moment(postedDate);
            }
            const dbDateClosed = account.systemOption.find(x => x.code === DBDateClosed).data;
            if (dbDateClosed && postedDate) {
                const dateClose = dbDateClosed ? moment(dbDateClosed, DATE_FORMAT) : null;
                const postedDateCheck = moment(postedDate.format(DATE_FORMAT), DATE_FORMAT);
                if (dateClose) {
                    if (postedDateCheck.isAfter(dateClose)) {
                        return false;
                    } else {
                        return true;
                    }
                } else {
                    return false;
                }
            }
        }
    }

    getStyle(record?: boolean) {
        return !record
            ? {
                  input: { color: this.color },
                  color: this.color
              }
            : {};
    }

    canDeactive() {}

    // Add by Hautv
    @HostListener('document:keydown.control.a')
    selectAll() {
        if (this.objects && !window.location.href.includes('/edit')) {
            event.preventDefault();
            if (window.location.href.includes('/thue')) {
                return;
            }
            this.selectedRows = [];
            this.objects.forEach(n => {
                this.selectedRows.push(n);
            });
            this.checkSameOrg();
            this.selectAllCCtrA();
        }
    }

    // ẩn hiện nút in khi khác type id. khi nhấn ctrl a
    selectAllCCtrA() {}

    checkSameOrg() {}
}
