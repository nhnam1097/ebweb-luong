import { Component, ElementRef, OnInit, Renderer } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { JhiEventManager, JhiParseLinks } from 'ng-jhipster';

import { LoginService } from 'app/core/login/login.service';
import { StateStorageService } from 'app/core/auth/state-storage.service';
import * as moment from 'moment';
import { ITEMS_PER_PAGE } from 'app/shared';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { Principal } from 'app/core';
import { ROLE } from 'app/role.constants';
import {ViewVoucherService} from 'app/shared/ref/view-voucher.service';
import {TypeGroupService} from 'app/shared/ref/type-group.service';
import {UtilsService} from 'app/shared/UtilsService/Utils.service';

@Component({
    selector: 'eb-ref-modal',
    templateUrl: './ref.component.html'
})
export class EbRefModalComponent implements OnInit {
    data: any[];
    recorded: any;
    status: any;
    fromDate;
    any;
    toDate: any;
    typeGroups: any;
    typeGroup: any;
    typeSearch: number;
    viewVouchers: any[];
    viewVouchersAll: any[];
    no: any;
    invoiceNo: any;
    totalItems: number;
    page: number;
    itemsPerPage = ITEMS_PER_PAGE;
    links: any;
    queryCount: any;
    newList: any[];
    timeLineVoucher: any;
    listTimeLine: any[];
    objTimeLine: { dtBeginDate?: string; dtEndDate?: string };
    account: any;
    paramCheckAll: boolean;

    constructor(
        private eventManager: JhiEventManager,
        private loginService: LoginService,
        private stateStorageService: StateStorageService,
        private elementRef: ElementRef,
        private renderer: Renderer,
        private router: Router,
        private activeModal: NgbActiveModal,
        private viewVoucherService: ViewVoucherService,
        private typeGroupService: TypeGroupService,
        private parseLinks: JhiParseLinks,
        public utilsService: UtilsService,
        private toastrService: ToastrService,
        private translateService: TranslateService,
        private principal: Principal
    ) {
        this.principal.identity().then(account => {
            this.account = account;
            this.typeSearch = 1;
            this.viewVouchers = [];
            this.listTimeLine = this.utilsService.getCbbTimeLine();
            this.timeLineVoucher = this.listTimeLine[4].value;
            this.selectChangeBeginDateAndEndDate(this.timeLineVoucher);
        });
    }

    ngOnInit(): void {
        // this.search();
        this.typeGroupService.queryForPopup().subscribe(res => {
            this.typeGroups = res.body.filter(a => a.id !== 51 && a.id !== 61);
        });

        this.page = 1;
        this.newList = [];
        if (this.data) {
            this.newList.push(...this.data);
        }
        if (this.newList.length === 0) {
            this.totalItems = 0;
        }
    }

    search(isSearch?: any) {
        if (isSearch) {
            this.paramCheckAll = false;
        }
        if (
            (!this.typeGroup && this.typeSearch === 1) ||
            (!this.no && this.typeSearch === 2) ||
            (!this.invoiceNo && this.typeSearch === 3)
        ) {
            this.toastrService.error(this.translateService.instant('ebwebApp.muaHang.muaDichVu.toastr.typeGroup'));
            return;
        }
        if (!this.fromDate) {
            this.toastrService.error(this.translateService.instant('ebwebApp.muaHang.muaDichVu.toastr.fromDateNull'));
            return;
        }
        if (!this.toDate) {
            this.toastrService.error(this.translateService.instant('ebwebApp.muaHang.muaDichVu.toastr.toDateNull'));
            return;
        }
        if (this.fromDate > this.toDate) {
            this.toastrService.error(this.translateService.instant('ebwebApp.mCAudit.errorDate'));
            return;
        }
        this.viewVoucherService
            .query({
                page: this.page - 1,
                size: this.itemsPerPage,
                typeSearch: this.typeSearch ? this.typeSearch : '',
                status: this.status !== null && this.status !== undefined ? this.status : '',
                fromDate: this.fromDate ? this.fromDate : '',
                toDate: this.toDate ? this.toDate : '',
                typeGroup: this.typeGroup && this.typeGroup.id ? this.typeGroup.id : '',
                recorded: this.recorded !== null && this.recorded !== undefined ? this.recorded : '',
                no: this.no ? this.no : '',
                invoiceNo: this.invoiceNo ? this.invoiceNo : '',
                isThamChieu: true
            })
            .subscribe(res => {
                this.viewVouchers = res.body;
                this.links = this.parseLinks.parse(res.headers.get('link'));
                this.totalItems = parseInt(res.headers.get('X-Total-Count'), 10);
                this.queryCount = this.totalItems;
                if (this.paramCheckAll) {
                    this.viewVouchers.forEach(n => {
                        if (this.newList.some(m => m.refID2 === n.refID2)) {
                            n.checked = false;
                        } else {
                            n.checked = true;
                        }
                    });
                } else {
                    this.viewVouchers.forEach(n => {
                        if (this.newList.some(m => m.refID2 === n.refID2)) {
                            n.checked = true;
                        } else {
                            n.checked = false;
                        }
                    });
                }
                this.viewVoucherService
                    .queryAll({
                        typeSearch: this.typeSearch ? this.typeSearch : '',
                        status: this.status !== null && this.status !== undefined ? this.status : '',
                        fromDate: this.fromDate ? this.fromDate : '',
                        toDate: this.toDate ? this.toDate : '',
                        typeGroup: this.typeGroup && this.typeGroup.id ? this.typeGroup.id : '',
                        recorded: this.recorded !== null && this.recorded !== undefined ? this.recorded : '',
                        no: this.no ? this.no : '',
                        invoiceNo: this.invoiceNo ? this.invoiceNo : ''
                    })
                    .subscribe(res2 => {
                        this.viewVouchersAll = res2.body;
                    });
            });
    }

    apply() {
        let listVoucher = [];
        if (this.paramCheckAll) {
            listVoucher = this.viewVouchersAll;
            if (this.newList) {
                this.newList.forEach(item => {
                    listVoucher.splice(listVoucher.indexOf(item), 1);
                });
            }
        } else {
            listVoucher = this.newList;
        }
        this.eventManager.broadcast({
            name: 'selectViewVoucher',
            content: listVoucher
        });
        this.activeModal.dismiss(true);
    }

    close() {
        this.activeModal.dismiss(false);
    }

    getCurrentDate() {
        const _date = moment();
        return { year: _date.year(), month: _date.month() + 1, day: _date.date() };
    }

    isCheckAll() {
        if (!this.totalItems) {
            return false;
        }
        if (this.totalItems - this.newList.length === 0) {
            this.newList = [];
            this.paramCheckAll = true;
        }
        if (this.viewVouchers) {
            return this.paramCheckAll && this.totalItems - this.newList.length === this.totalItems;
        } else {
            return false;
        }
    }

    checkAll() {
        if (this.paramCheckAll && this.newList.length > 0) {
            this.paramCheckAll = true;
        } else {
            this.paramCheckAll = !this.paramCheckAll;
        }
        if (this.paramCheckAll) {
            this.viewVouchers.forEach(n => (n.checked = true));
            this.newList = [];
        } else {
            this.viewVouchers.forEach(n => (n.checked = false));
            this.newList = [];
        }
    }

    check(viewVoucher) {
        viewVoucher.checked = !viewVoucher.checked;
        if (this.paramCheckAll) {
            if (!viewVoucher.checked) {
                this.changeTypeSearch(viewVoucher);
                this.newList.push(viewVoucher);
            } else {
                for (let i = 0; i < this.newList.length; i++) {
                    if (this.newList[i].refID2 === viewVoucher.refID2) {
                        this.newList.splice(i, 1);
                        i--;
                    }
                }
            }
        } else {
            if (viewVoucher.checked) {
                this.changeTypeSearch(viewVoucher);
                this.newList.push(viewVoucher);
            } else {
                for (let i = 0; i < this.newList.length; i++) {
                    if (this.newList[i].refID2 === viewVoucher.refID2) {
                        this.newList.splice(i, 1);
                        i--;
                    }
                }
            }
        }
    }

    // nếu là loại chứng từ thì chỉ cho chọn 1 giá trị có cùng type group, bỏ các giá trị khác
    changeTypeSearch(viewVoucher = null) {
        /*if (this.newList && this.newList.length) {
            if (viewVoucher && viewVoucher.typeGroupID !== this.newList[0].typeGroupID) {
                this.newList = [];
                this.viewVouchers.forEach(item => {
                    item.checked = !(!viewVoucher || item.no !== viewVoucher.no);
                });
            }
        }*/
    }

    selectChangeBeginDateAndEndDate(intTimeLine: String) {
        if (intTimeLine) {
            this.objTimeLine = this.utilsService.getTimeLine(intTimeLine, this.account);
            this.fromDate = moment(this.objTimeLine.dtBeginDate).format('YYYYMMDD');
            this.toDate = moment(this.objTimeLine.dtEndDate).format('YYYYMMDD');
        }
    }

    view(voucher, $event) {
        $event.stopPropagation();
        let url = '';
        switch (voucher.typeGroupID) {
            // Hàng bán trả lại
            case 33:
                if (this.hasAuthority(ROLE.HangBanTraLai_Xem)) {
                    url = `/#/hang-ban/tra-lai/${voucher.refID2}/edit/from-ref`;
                }
                break;
            // Giảm giá hàng bán
            case 34:
                if (this.hasAuthority(ROLE.HangBanGiamGia_Xem)) {
                    url = `/#/hang-ban/giam-gia/${voucher.refID2}/edit/from-ref`;
                }
                break;
            // Xuất hóa đơn
            case 35:
                if (this.hasAuthority(ROLE.XuatHoaDon_Xem)) {
                    url = `/#/xuat-hoa-don/${voucher.refID2}/edit/from-ref`;
                }
                break;
            case 22:
                if (this.hasAuthority(ROLE.HangMuaTraLai_Xem)) {
                    url = `/#/hang-mua/tra-lai/${voucher.refID2}/view`;
                }
                break;
            case 23:
                if (this.hasAuthority(ROLE.HangMuaGiamGia_Xem)) {
                    url = `/#/hang-mua/giam-gia/${voucher.refID2}/view`;
                }
                break;
            case 10:
                if (this.hasAuthority(ROLE.PhieuThu_Xem)) {
                    url = `/#/phieu-thu/${voucher.refID2}/edit/from-ref`;
                }
                break;
            case 16:
                if (this.hasAuthority(ROLE.BaoCo_Xem)) {
                    url = `/#/bao-co/${voucher.refID2}/edit/from-ref`;
                }
                break;
            case 17:
                if (this.hasAuthority(ROLE.TheTinDung_Xem)) {
                    url = `/#/the-tin-dung/${voucher.refID2}/edit/from-ref`;
                }
                break;
            case 70:
                if (this.hasAuthority(ROLE.ChungTuNghiepVuKhac_Xem)) {
                    url = `/#/chung-tu-nghiep-vu-khac/${voucher.refID2}/edit/from-ref`;
                }
                break;
            case 11:
                if (this.hasAuthority(ROLE.PhieuChi_Xem)) {
                    url = `/#/phieu-chi/${voucher.refID2}/edit/from-ref`;
                }
                break;
            case 31:
                if (this.hasAuthority(ROLE.DonDatHang_Xem)) {
                    url = `/#/don-dat-hang/${voucher.refID2}/edit/from-ref`;
                }
                break;
            case 24:
                if (this.hasAuthority(ROLE.MuaDichVu_Xem)) {
                    url = `/#/mua-dich-vu/${voucher.refID2}/edit/from-ref`;
                }
                break;
            case 40:
                if (this.hasAuthority(ROLE.NhapKho_XEM)) {
                    url = `/#/nhap-kho/${voucher.refID2}/edit/from-ref`;
                }
                break;
            case 20:
                if (this.hasAuthority(ROLE.DonMuaHang_XEM)) {
                    url = `/#/don-mua-hang/${voucher.refID2}/edit/from-ref`;
                }
                break;
            case 41:
                if (this.hasAuthority(ROLE.XuatKho_XEM)) {
                    url = `/#/xuat-kho/${voucher.refID2}/edit/from-ref`;
                }
                break;
            case 21:
                if (this.hasAuthority(ROLE.MuaHangQuaKho_Xem)) {
                    url = `/#/chung-tu-mua-hang/${voucher.refID2}/edit/from-ref`;
                }
                break;
            case 18:
                if (this.hasAuthority(ROLE.KiemKeQuy_Xem)) {
                    url = `/#/mc-audit/${voucher.refID2}/edit/from-ref`;
                }
                break;
            case 32:
                if (this.hasAuthority(ROLE.ChungTuBanHang_Xem)) {
                    url = `/#/chung-tu-ban-hang/${voucher.refID2}/edit/from-ref`;
                }
                break;
            case 30:
                if (this.hasAuthority(ROLE.BaoGia_Xem)) {
                    url = `/#/sa-quote/${voucher.refID2}/edit/from-ref`;
                }
                break;
            case 12:
            case 13:
            case 14:
                if (this.hasAuthority(ROLE.BaoNo_Xem)) {
                    url = `/#/bao-no/${voucher.refID2}/edit/from-ref`;
                }
                break;
            case 42:
                if (this.hasAuthority(ROLE.ChuyenKho_XEM)) {
                    url = `/#/chuyen-kho/${voucher.refID2}/edit/from-ref`;
                }
                break;
            case 72:
                if (this.hasAuthority(ROLE.HopDongMua_Xem)) {
                    url = `/#/hop-dong-mua/${voucher.refID2}/edit/from-ref`;
                }
                break;
            case 73:
                if (this.hasAuthority(ROLE.HopDongBan_Xem)) {
                    url = `/#/hop-dong-mua/${voucher.refID2}/edit/from-ref`;
                }
                break;
        }
        if (url) {
            window.open(url, '_blank');
        }
    }

    private hasAuthority(auth): boolean {
        if (this.account.authorities.includes(ROLE.ROLE_ADMIN) || this.account.authorities.includes(auth)) {
            return true;
        }
        this.toastrService.error(this.translateService.instant('ebwebApp.quyTrinh.notHasAccess'));
        return false;
    }

    newArr(length: number): any[] {
        if (length > 0) {
            return new Array(length);
        } else {
            return new Array(1);
        }
    }

    getNumberSelect() {
        if (this.paramCheckAll) {
            return this.totalItems - this.newList.length;
        } else {
            return this.newList.length;
        }
    }
}
