// import { AfterViewChecked, Component, OnInit, ViewChild } from '@angular/core';
// import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
// import { Principal } from 'app/core';
// import { ToastrService } from 'ngx-toastr';
// import { TranslateService } from '@ngx-translate/core';
// import { CALCULATE_OW, PP_TINH_TY_GIA_XUAT_QUY } from 'app/app.constants';
// import { JhiEventManager } from 'ng-jhipster';
// import * as moment from 'moment';
// import { Moment } from 'moment';
// import { DATE_FORMAT, DATE_FORMAT_SEARCH } from 'app/shared';
// import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
// import { Router } from '@angular/router';
//
// @Component({
//     selector: 'eb-calculate-ow-repository',
//     templateUrl: './tinh-ty-gia-xuat-quy.component.html',
//     styleUrls: ['./tinh-ty-gia-xuat-quy.component.css']
// })
// export class CalculateExchangeRateComponent extends BaseComponent implements OnInit, AfterViewChecked {
//     @ViewChild('question') question;
//     phuongPhapTinh: string;
//     isAll: number;
//     fromDate: Moment;
//     toDate: Moment;
//     listTimeLine: any[];
//     timeLineVoucher: any;
//     objTimeLine: { dtBeginDate?: string; dtEndDate?: string };
//     account: any;
//     modalRef: NgbModalRef;
//     currencies: ICurrency[];
//     currenciesAll: ICurrency[];
//     calculationMethod: any;
//     setEdit: boolean;
//     isLoading: boolean;
//     currencyCode: string;
//     viewNo: string;
//     gOtherVoucher: any;
//
//     constructor(
//         public activeModal: NgbActiveModal,
//         private principal: Principal,
//         public translate: TranslateService,
//         public eventManager: JhiEventManager,
//         public utilsService: UtilsService,
//         private modalService: NgbModal,
//         private refModalService: RefModalService,
//         private toastr: ToastrService,
//         private router: Router,
//         private currencyService: CurrencyService,
//         private glService: GeneralLedgerService
//     ) {
//         super();
//     }
//
//     ngOnInit() {
//         this.isLoading = false;
//         this.principal.identity().then(account => {
//             this.account = account;
//             this.currencyCode = account.organizationUnit.currencyID;
//             this.setEdit = this.isEdit;
//             this.isEdit = true;
//             this.calculationMethod = this.account.systemOption.find(x => x.code === PP_TINH_TY_GIA_XUAT_QUY).data;
//             if (this.calculationMethod === CALCULATE_OW.BINH_QUAN_CUOI_KY_CODE) {
//                 this.phuongPhapTinh = CALCULATE_OW.BINH_QUAN_CUOI_KY;
//             } else if (this.calculationMethod === CALCULATE_OW.BINH_QUAN_TUC_THOI_CODE) {
//                 this.phuongPhapTinh = CALCULATE_OW.BINH_QUAN_TUC_THOI;
//             }
//             this.listTimeLine = this.utilsService.getCbbTimeLine();
//             this.timeLineVoucher = this.listTimeLine[4].value;
//             this.selectChangeBeginDateAndEndDate(this.timeLineVoucher);
//             this.isAll = 1;
//             this.currencies = [];
//             this.currencyService.findAllActive().subscribe((res: HttpResponse<ICurrency[]>) => {
//                 this.currenciesAll = res.body.filter(x => x.currencyCode !== this.currencyCode);
//             });
//         });
//         this.registerRef();
//     }
//
//     calculate() {
//         if (this.isAll === 1) {
//             this.currencies = this.currenciesAll;
//         }
//         this.closeModal();
//         if (this.checkErr()) {
//             this.isLoading = true;
//             this.toastr.info(this.translate.instant('global.messages.calculatingEx'));
//             const calculateOWDTO = {
//                 calculationMethod: this.calculationMethod ? this.calculationMethod : '',
//                 currencies: this.currencies.map(x => x.currencyCode),
//                 fromDate: this.fromDate ? moment(this.fromDate).format(DATE_FORMAT) : '',
//                 toDate: this.toDate ? moment(this.toDate).format(DATE_FORMAT) : '',
//                 gOtherVoucherID: this.gOtherVoucher ? this.gOtherVoucher.id : ''
//             };
//             this.glService.calculateExchangeRate(calculateOWDTO).subscribe(
//                 res => {
//                     this.isLoading = false;
//                     if (res.body.status === 1) {
//                         this.toastr.success(this.translate.instant('global.messages.calculateExSuccess1'));
//                     } else if (res.body.status === 4) {
//                         this.toastr.success(this.translate.instant('global.messages.calculateExSuccess2'));
//                     } else if (res.body.status === 2 && res.body.gOtherVoucher) {
//                         sessionStorage.setItem('gOtherVoucher', JSON.stringify(res.body.gOtherVoucher));
//                         this.router.navigate(['chung-tu-nghiep-vu-khac/', 'new']);
//                         this.closeForm();
//                     } else if (res.body.status === 3 && res.body.gOtherVoucher) {
//                         this.gOtherVoucher = res.body.gOtherVoucher;
//                         this.viewNo = res.body.gOtherVoucher.noFBook ? res.body.gOtherVoucher.noFBook : res.body.gOtherVoucher.noMBook;
//                         this.modalRef = this.modalService.open(this.question, { backdrop: 'static' });
//                     } else if (res.body.voucherResultDTOs) {
//                         this.modalRef = this.refModalService.open(
//                             res.body.voucherResultDTOs,
//                             ErrorVouchersComponent,
//                             null,
//                             false,
//                             null,
//                             null,
//                             null,
//                             null,
//                             null,
//                             true
//                         );
//                     } else {
//                         this.toastr.error(
//                             this.translate.instant('global.messages.error.calculateExError'),
//                             this.translate.instant('ebwebApp.mCReceipt.error.error')
//                         );
//                     }
//                 },
//                 (res: HttpErrorResponse) => {
//                     this.isLoading = false;
//                     this.toastr.error(
//                         this.translate.instant('global.messages.error.calculateExError'),
//                         this.translate.instant('ebwebApp.mCReceipt.error.error')
//                     );
//                 }
//             );
//         }
//     }
//
//     closeForm() {
//         this.isEdit = this.setEdit;
//         this.activeModal.dismiss('closed');
//     }
//
//     closeModal() {
//         if (this.modalRef) {
//             this.modalRef.close();
//         }
//     }
//
//     viewRelateVoucher() {
//         this.closeModal();
//         this.router.navigate(['./chung-tu-nghiep-vu-khac', this.gOtherVoucher.id, 'edit']);
//         this.closeForm();
//     }
//
//     checkErr() {
//         if (this.currenciesAll.length === 0) {
//             this.toastr.error(
//                 this.translate.instant('global.messages.error.nullCurrenciesAll'),
//                 this.translate.instant('ebwebApp.mCReceipt.error.error')
//             );
//             return false;
//         }
//         if (this.currencies.length === 0) {
//             this.toastr.error(
//                 this.translate.instant('global.messages.error.nullCurrencies'),
//                 this.translate.instant('ebwebApp.mCReceipt.error.error')
//             );
//             return false;
//         }
//         if (!this.fromDate) {
//             this.toastr.error(
//                 this.translate.instant('ebwebApp.mCReceipt.error.nullFromDate'),
//                 this.translate.instant('ebwebApp.mCReceipt.error.error')
//             );
//             return false;
//         }
//         if (!this.toDate) {
//             this.toastr.error(
//                 this.translate.instant('ebwebApp.mCReceipt.error.nullToDate'),
//                 this.translate.instant('ebwebApp.mCReceipt.error.error')
//             );
//             return false;
//         }
//         if (this.toDate && this.fromDate) {
//             if (moment(this.toDate, DATE_FORMAT_SEARCH) < moment(this.fromDate, DATE_FORMAT_SEARCH)) {
//                 this.toastr.error(
//                     this.translate.instant('ebwebApp.mCReceipt.error.fromDateGreaterToDate'),
//                     this.translate.instant('ebwebApp.mCReceipt.error.error')
//                 );
//                 return false;
//             }
//         }
//         return true;
//     }
//
//     registerRef() {
//         this.eventManager.subscribe('selectCurrency', ref => {
//             this.currencies = ref.content;
//         });
//     }
//
//     chooseCurrency() {
//         this.modalRef = this.refModalService.open(this.currencies, EbChooseCurrencyModalComponent, null, false, null, 'width-80 width-50');
//     }
//
//     selectChangeBeginDateAndEndDate(intTimeLine: String) {
//         if (intTimeLine) {
//             this.objTimeLine = this.utilsService.getTimeLine(intTimeLine, this.account);
//             this.fromDate = moment(this.objTimeLine.dtBeginDate);
//             this.toDate = moment(this.objTimeLine.dtEndDate);
//         }
//     }
//
//     getCurrentDate(): { year; month; day } {
//         const _date = moment();
//         return { year: _date.year(), month: _date.month() + 1, day: _date.date() };
//     }
//
//     ngAfterViewChecked(): void {
//         this.disableInput();
//     }
// }
