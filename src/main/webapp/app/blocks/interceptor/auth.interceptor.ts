import { Observable } from 'rxjs';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';

import { SERVER_API_URL } from 'app/app.constants';
import { CookieService } from 'ngx-cookie';

export class AuthInterceptor implements HttpInterceptor {
    constructor(
        private localStorage: LocalStorageService,
        private sessionStorage: SessionStorageService,
        private _cookieService: CookieService
    ) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (!request || !request.url || (/^http/.test(request.url) && !(SERVER_API_URL && request.url.startsWith(SERVER_API_URL)))) {
            return next.handle(request);
        }

        let token =
            this.localStorage.retrieve('authenticationtoken') ||
            this.sessionStorage.retrieve('authenticationtoken') ||
            this._cookieService.get('authenticationToken');
        if (token && token.length > 7000) {
            token = '';
        }
        if (!!token) {
            request = request.clone({
                setHeaders: {
                    Authorization: 'Bearer ' + token,
                    ip: sessionStorage.getItem('ipComputer') ? JSON.parse(sessionStorage.getItem('ipComputer')).ip : ''
                }
            });
        }
        return next.handle(request);
    }
}
