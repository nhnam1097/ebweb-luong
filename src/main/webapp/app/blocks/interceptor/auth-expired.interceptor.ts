import { Injector } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { LoginService } from 'app/core/login/login.service';
import { ToastrService } from 'ngx-toastr';

export class AuthExpiredInterceptor implements HttpInterceptor {
    constructor(private injector: Injector, private toastr: ToastrService) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(
            tap(
                (event: HttpEvent<any>) => {},
                (err: any) => {
                    if (err instanceof HttpErrorResponse) {
                        if (err.status === 401) {
                            if (err.error && err.error.text) {
                                this.toastr.error(err.error.text);
                            }
                            const loginService: LoginService = this.injector.get(LoginService);
                            loginService.logout();
                        }
                    }
                }
            )
        );
    }
}
