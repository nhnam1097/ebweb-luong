// role, authorities
export const ROLE = {
    ROLE_MGT: 'ROLE_MGT',
    ROLE_PERMISSION: 'ROLE_PERMISSION',
    ROLE_ADMIN_KH: 'ROLE_ADMIN_KH',
    ROLE_REPORT: 'ROLE_REPORT',
    ROLE_ADMIN: 'ROLE_ADMIN',
    // 1.Tien Mat Ngan Hang
    TienMatNganHang: '01',

    // Phieu thu
    PhieuThu: '0101',
    PhieuThu_Xem: '010101',
    PhieuThu_Them: '010102',
    PhieuThu_Sua: '010103',
    PhieuThu_Xoa: '010104',
    PhieuThu_GhiSo: '010105',
    PhieuThu_In: '010106',
    PhieuThu_KetXuat: '010107',
    PhieuThu_Import: '010108',

    // Phieu chi
    PhieuChi: '0102',
    PhieuChi_Xem: '010201',
    PhieuChi_Them: '010202',
    PhieuChi_Sua: '010203',
    PhieuChi_Xoa: '010204',
    PhieuChi_GhiSo: '010205',
    PhieuChi_In: '010206',
    PhieuChi_KetXuat: '010207',
    PhieuChi_Import: '010208',

    // Bao No
    BaoNo: '0103',
    BaoNo_Xem: '010301',
    BaoNo_Them: '010302',
    BaoNo_Sua: '010303',
    BaoNo_Xoa: '010304',
    BaoNo_GhiSo: '010305',
    BaoNo_In: '010306',
    BaoNo_KetXuat: '010307',
    BaoNo_Upload: '010308',

    // Bao Co
    BaoCo: '0104',
    BaoCo_Xem: '010401',
    BaoCo_Them: '010402',
    BaoCo_Sua: '010403',
    BaoCo_Xoa: '010404',
    BaoCo_GhiSo: '010405',
    BaoCo_In: '010406',
    BaoCo_KetXuat: '010407',
    BaoCo_Import: '010408',
    BaoCo_QuanTriMau: '010409',

    // The Tin Dung
    TheTinDung: '0105',
    TheTinDung_Xem: '010501',
    TheTinDung_Them: '010502',
    TheTinDung_Sua: '010503',
    TheTinDung_Xoa: '010504',
    TheTinDung_GhiSo: '010505',
    TheTinDung_In: '010506',
    TheTinDung_KetXuat: '010507',
    TheTinDung_Import: '010508',

    // Kiem Ke Quy
    KiemKeQuy: '0106',
    KiemKeQuy_Xem: '010601',
    KiemKeQuy_Them: '010602',
    KiemKeQuy_Sua: '010603',
    KiemKeQuy_Xoa: '010604',
    KiemKeQuy_In: '010606',
    KiemKeQuy_KetXuat: '010607',

    // chuyen tien noi bo
    ChuyenTienNoiBo: '0107',
    ChuyenTienNoiBo_Xem: '010701',
    ChuyenTienNoiBo_Them: '010702',
    ChuyenTienNoiBo_Sua: '010703',
    ChuyenTienNoiBo_Xoa: '010704',
    ChuyenTienNoiBo_GhiSo: '010705',
    ChuyenTienNoiBo_In: '010706',
    ChuyenTienNoiBo_KetXuat: '010707',
    ChuyenTienNoiBo_Import: '010708',

    // Đối chiếu ngân hàng
    DoiChieuNganHang: '0108',
    DoiChieuNganHang_Xem: '010801',
    DoiChieuNganHang_Them: '010802',
    DoiChieuNganHang_Sua: '010803',

    // 2.Mua Hang
    MuaHang: '02',

    // Đơn mua hàng
    DonMuaHang: '0201',
    DonMuaHang_XEM: '020101',
    DonMuaHang_THEM: '020102',
    DonMuaHang_SUA: '020103',
    DonMuaHang_XOA: '020104',
    DonMuaHang_IN: '020106',
    DonMuaHang_KET_XUAT: '020107',
    DonMuaHang_Import: '020108',

    // Nhan Hoa Don
    NhanHoaDon: '0204',
    NhanHoaDon_Xem: '020401',
    NhanHoaDon_Them: '020402',
    NhanHoaDon_Sua: '020403',
    NhanHoaDon_Xoa: '020404',
    NhanHoaDon_In: '020406',
    NhanHoaDon_KetXuat: '020407',

    // Tra Tien Nha Cung Cap
    TraTienNhaCungCap: '0208',
    TraTienNhaCungCap_Xem: '020801',

    // Doi Tru Chung Tu
    DoiTruChungTuMH: '0210',
    DoiTruChungTuMH_Xem: '021001',

    // Hàng mua trả lại
    HangMuaTraLai: '0206',
    HangMuaTraLai_Xem: '020601',
    HangMuaTraLai_Them: '020602',
    HangMuaTraLai_Sua: '020603',
    HangMuaTraLai_Xoa: '020604',
    HangMuaTraLai_GhiSo: '020605',
    HangMuaTraLai_In: '020606',
    HangMuaTraLai_KetXuat: '020607',
    HangMuaTraLai_Upload: '020608',

    // Hàng mua Qua Kho
    MuaHangQuaKho: '0202',
    MuaHangQuaKho_Xem: '020201',
    MuaHangQuaKho_Them: '020202',
    MuaHangQuaKho_Sua: '020203',
    MuaHangQuaKho_Xoa: '020204',
    MuaHangQuaKho_GhiSo: '020205',
    MuaHangQuaKho_In: '020206',
    MuaHangQuaKho_KetXuat: '020207',
    MuaHangQuaKho_Import: '020208',
    MuaHangQuaKho_QuanTriMau: '020209',

    // Hàng mua giảm giá
    HangMuaGiamGia: '0207',
    HangMuaGiamGia_Xem: '020701',
    HangMuaGiamGia_Them: '020702',
    HangMuaGiamGia_Sua: '020703',
    HangMuaGiamGia_Xoa: '020704',
    HangMuaGiamGia_GhiSo: '020705',
    HangMuaGiamGia_In: '020706',
    HangMuaGiamGia_KetXuat: '020707',
    HangMuaGiamGia_Upload: '020708',

    MuaDichVu_Xem: '020501',
    MuaDichVu_Them: '020502',
    MuaDichVu_Sua: '020503',
    MuaDichVu_Xoa: '020504',
    MuaDichVu_GhiSo: '020505',
    MuaDichVu_In: '020506',
    MuaDichVu_KetXuat: '020507',
    MuaDichVu_Upload: '020508',
    MuaDichVu_QuanTriMau: '020509',

    // Hoa don dau vao
    HoaDonDauVao: '0209',
    HoaDonDauVao_Xem: '020901',
    HoaDonDauVao_Them: '020902',
    HoaDonDauVao_Sua: '020903',
    HoaDonDauVao_Xoa: '020904',
    HoaDonDauVao_In: '020906',
    HoaDonDauVao_ketXuat: '020907',
    HoaDonDauVao_Upload: '020908',

    // 3.Ban Hang
    BanHang: '03',
    // Bao Gia
    BaoGia: '0301',
    BaoGia_Xem: '030101',
    BaoGia_Them: '030102',
    BaoGia_Sua: '030103',
    BaoGia_Xoa: '030104',
    BaoGia_In: '030106',
    BaoGia_KetXuat: '030107',
    BaoGia_Upload: '030108',

    // Don dat hang
    DonDatHang: '0302',
    DonDatHang_Xem: '030201',
    DonDatHang_Them: '030202',
    DonDatHang_Sua: '030203',
    DonDatHang_Xoa: '030204',
    DonDatHang_In: '030206',
    DonDatHang_KetXuat: '030207',
    DonDatHang_Import: '030208',

    // Ban Hang Chua Thu Tien
    ChungTuBanHang: '0303',
    ChungTuBanHang_Xem: '030301',
    ChungTuBanHang_Them: '030302',
    ChungTuBanHang_Sua: '030303',
    ChungTuBanHang_Xoa: '030304',
    ChungTuBanHang_GhiSo: '030305',
    ChungTuBanHang_In: '030306',
    ChungTuBanHang_KetXuat: '030307',
    ChungTuBanHang_Import: '030308',
    MauChungTuBanHang: '030309',

    // Xuat Hoa Don
    XuatHoaDon: '0305',
    XuatHoaDon_Xem: '030501',
    XuatHoaDon_Them: '030502',
    XuatHoaDon_Sua: '030503',
    XuatHoaDon_Xoa: '030504',
    XuatHoaDon_In: '030506',
    XuatHoaDon_KetXuat: '030507',
    XuatHoaDon_Upload: '030508',
    XuatHoaDon_QuanLyMau: '030509',

    // Hang Ban Tra Lai
    HangBanTraLai: '0306',
    HangBanTraLai_Xem: '030601',
    HangBanTraLai_Them: '030602',
    HangBanTraLai_Sua: '030603',
    HangBanTraLai_Xoa: '030604',
    HangBanTraLai_GhiSo: '030605',
    HangBanTraLai_In: '030606',
    HangBanTraLai_KetXuat: '030607',
    HangBanTraLai_Upload: '030608',
    HangBanTraLai_QuanLyMau: '030609',

    // Hang Ban Giam Gia
    HangBanGiamGia: '0307',
    HangBanGiamGia_Xem: '030701',
    HangBanGiamGia_Them: '030702',
    HangBanGiamGia_Sua: '030703',
    HangBanGiamGia_Xoa: '030704',
    HangBanGiamGia_GhiSo: '030705',
    HangBanGiamGia_In: '030706',
    HangBanGiamGia_KetXuat: '030707',
    HangBanGiamGia_Upload: '030708',
    HangBanGiamGia_QuanLyMau: '030709',

    // Thu Tien Khach Hang
    ThuTienKhachHang: '0308',
    ThuTienKhachHang_Xem: '030801',

    // Tinh Gia Ban
    TinhGiaBan: '0311',
    TinhGiaBan_Xem: '031101',

    // Thiet lap chinh sach gia
    ThietLapChinhSachGia: '0309',
    ThietLapChinhSachGia_Xem: '030901',
    ThietLapChinhSachGia_Them: '030902',
    ThietLapChinhSachGia_Sua: '030903',
    ThietLapChinhSachGia_Xoa: '030904',
    ThietLapChinhSachGia_In: '030906',
    ThietLapChinhSachGia_KetXuat: '030907',

    // Doi Tru Chung Tu
    DoiTruChungTuBH: '0310',
    DoiTruChungTuBH_Xem: '031001',

    // 4.Kho
    Kho: '04',

    // Nhập kho
    NhapKho: '0401',
    NhapKho_XEM: '040101',
    NhapKho_THEM: '040102',
    NhapKho_SUA: '040103',
    NhapKho_XOA: '040104',
    NhapKho_GHI_SO: '040105',
    NhapKho_IN: '040106',
    NhapKho_KET_XUAT: '040107',
    NhapKho_IMPORT: '040108',

    // Tinh Gia Xuat Kho
    TinhGiaXuatKho: '0408',
    TinhGiaXuatKho_Xem: '040801',

    // cap nhat gia kho thanh pham
    CapNhatGiaKhoThanhPham: '0410',
    CapNhatGiaKhoThanhPham_Xem: '041001',

    // Xuất kho
    XuatKho: '0402',
    XuatKho_XEM: '040201',
    XuatKho_THEM: '040202',
    XuatKho_SUA: '040203',
    XuatKho_XOA: '040204',
    XuatKho_GHI_SO: '040205',
    XuatKho_IN: '040206',
    XuatKho_KET_XUAT: '040207',
    XuatKho_IMPORT: '040208',

    // Chuyển kho
    ChuyenKho: '0403',
    ChuyenKho_XEM: '040301',
    ChuyenKho_THEM: '040302',
    ChuyenKho_SUA: '040303',
    ChuyenKho_XOA: '040304',
    ChuyenKho_GHI_SO: '040305',
    ChuyenKho_IN: '040306',
    ChuyenKho_KET_XUAT: '040307',
    ChuyenKho_UP_LOAD: '040308',

    // Lệnh sản xuất
    LenhSanXuat: '0407',
    LenhSanXuat_Xem: '040701',
    LenhSanXuat_Them: '040702',
    LenhSanXuat_Sua: '040703',
    LenhSanXuat_Xoa: '040704',
    LenhSanXuat_In: '040706',
    LenhSanXuat_KetXuat: '040707',
    LenhSanXuat_QuanTriMau: '040709',

    // lệnh lắp ráp tháo dỡ
    LLRTD: '0404',
    LLRTD_XEM: '040401',
    LLRTD_THEM: '040402',
    LLRTD_SUA: '040403',
    LLRTD_XOA: '040404',
    LLRTD_IN: '040406',
    LLRTD_KET_XUAT: '040407',
    LLRTD_QUAN_TRI_MAU: '040409',

    // Sắp xếp thứ tự nhập xuất
    SapXepThuTuNhapXuat_Xem: '041101',

    // Lệnh lắp ráp. tháo dỡ
    LapRapThaoDo: '0404',
    LapRapThaoDo_IN: '040406',

    // Kiểm kê kho
    KiemKeKho: '0409',
    KiemKeKho_XEM: '040901',

    // 5.Gia Thanh
    GiaThanh: '05',

    // Dinh muc gia thanh thanh pham
    DinhMucGiaThanhThanhPham: '0502',
    DinhMucGiaThanhThanhPham_Xem: '050201',
    DinhMucGiaThanhThanhPham_Sua: '050203',
    DinhMucGiaThanhThanhPham_In: '050206',
    DinhMucGiaThanhThanhPham_KetXuat: '050207',
    DinhMucGiaThanhThanhPham_Upload: '050208',

    // Dinh muc phan bo chi phi
    DinhMucPhanBoChiPhi: '0503',
    DinhMucPhanBoChiPhi_Xem: '050301',
    DinhMucPhanBoChiPhi_Sua: '050303',
    DinhMucPhanBoChiPhi_In: '050306',
    DinhMucPhanBoChiPhi_Export: '050307',
    DinhMucPhanBoChiPhi_Upload: '050308',

    // Chi phi do dang dau ky
    ChiPhiDoDangDauKy: '0504',
    ChiPhiDoDangDauKy_Xem: '050401',
    ChiPhiDoDangDauKy_Sua: '050403',
    ChiPhiDoDangDauKy_In: '050406',
    ChiPhiDoDangDauKy_Export: '050407',
    ChiPhiDoDangDauKy_Upload: '050408',

    // Phuong phap gian don
    PhuongPhapGianDon: '0505',
    PhuongPhapGianDon_Xem: '050501',
    PhuongPhapGianDon_Them: '050502',
    PhuongPhapGianDon_Sua: '050503',
    PhuongPhapGianDon_Xoa: '050504',
    PhuongPhapGianDon_In: '050506',
    PhuongPhapGianDon_KetXuat: '050507',

    // Phuong phap he so
    PhuongPhapHeSo: '0506',
    PhuongPhapHeSo_Xem: '050601',
    PhuongPhapHeSo_Them: '050602',
    PhuongPhapHeSo_Sua: '050603',
    PhuongPhapHeSo_Xoa: '050604',
    PhuongPhapHeSo_In: '050606',
    PhuongPhapHeSo_KetXuat: '050607',

    // Phuong phap ty le
    PhuongPhapTyLe: '0507',
    PhuongPhapTyLe_Xem: '050701',
    PhuongPhapTyLe_Them: '050702',
    PhuongPhapTyLe_Sua: '050703',
    PhuongPhapTyLe_Xoa: '050704',
    PhuongPhapTyLe_In: '050706',
    PhuongPhapTyLe_KetXuat: '050707',

    // Gia thanh theo cong trinh, vu viec
    GiaThanhTheoCTVV: '0508',
    GiaThanhTheoCTVV_Xem: '050801',
    GiaThanhTheoCTVV_Them: '050802',
    GiaThanhTheoCTVV_Sua: '050803',
    GiaThanhTheoCTVV_Xoa: '050804',
    GiaThanhTheoCTVV_In: '050806',
    GiaThanhTheoCTVV_KetXuat: '050807',

    // Gia thanh theo don hang
    GiaThanhTheoDonHang: '0509',
    GiaThanhTheoDonHang_Xem: '050901',
    GiaThanhTheoDonHang_Them: '050902',
    GiaThanhTheoDonHang_Sua: '050903',
    GiaThanhTheoDonHang_Xoa: '050904',
    GiaThanhTheoDonHang_In: '050906',
    GiaThanhTheoDonHang_KetXuat: '050907',

    // Gia thanh theo hop dong
    GiaThanhTheoHopDong: '0510',
    GiaThanhTheoHopDong_Xem: '051001',
    GiaThanhTheoHopDong_Them: '051002',
    GiaThanhTheoHopDong_Sua: '051003',
    GiaThanhTheoHopDong_Xoa: '051004',
    GiaThanhTheoHopDong_In: '051006',
    GiaThanhTheoHopDong_KetXuat: '051007',

    // Ket chuyen chi phi
    KetChuyenChiPhi: '0511',
    KetChuyenChiPhi_Xem: '051101',
    KetChuyenChiPhi_Them: '051102',
    KetChuyenChiPhi_Sua: '051103',
    KetChuyenChiPhi_Xoa: '051104',
    KetChuyenChiPhi_GhiSo: '051105',
    KetChuyenChiPhi_In: '051106',
    KetChuyenChiPhi_KetXuat: '051107',

    // 6. Tong Hop
    TongHop: '06',
    // Chung Tu Nghiep Vu Khac
    ChungTuNghiepVuKhac: '0601',
    ChungTuNghiepVuKhac_Xem: '060101',
    ChungTuNghiepVuKhac_Them: '060102',
    ChungTuNghiepVuKhac_Sua: '060103',
    ChungTuNghiepVuKhac_Xoa: '060104',
    ChungTuNghiepVuKhac_GhiSo: '060105',
    ChungTuNghiepVuKhac_In: '060106',
    ChungTuNghiepVuKhac_KetXuat: '060107',
    ChungTuNghiepVuKhac_Upload: '060108',
    ChungTuNghiepVuKhac_QuanTriMau: '060109',

    // Kết chuyển lãi lỗ
    KetChuyenLaiLo: '0602',
    KetChuyenLaiLo_Xem: '060201',
    KetChuyenLaiLo_Them: '060202',
    KetChuyenLaiLo_Sua: '060203',
    KetChuyenLaiLo_Xoa: '060204',
    KetChuyenLaiLo_GhiSo: '060205',
    KetChuyenLaiLo_In: '060206',
    KetChuyenLaiLo_KetXuat: '060207',

    // Chứng từ ghi sổ
    ChungTuGhiSo: '0603',
    ChungTuGhiSo_Xem: '060301',
    ChungTuGhiSo_Them: '060302',
    ChungTuGhiSo_Sua: '060303',
    ChungTuGhiSo_Xoa: '060304',
    ChungTuGhiSo_GhiSo: '060305',
    ChungTuGhiSo_In: '060306',
    ChungTuGhiSo_KetXuat: '060307',
    ChungTuGhiSo_QuanLyMau: '060309',

    // Khóa sổ kỳ kế toán
    KhoaSoKyKeToan: '0609',
    KhoaSo: '060901',

    // Bỏ khóa sổ kỳ kế toán
    BoKhoaSoKyKeToan: '0610',
    BoKhoaSo: '061001',

    // Tính tỷ giá xuất quỹ
    TinhTyGiaXuatQuy: '0607',
    TinhTyGiaXuatQuy_Xem: '060701',

    // Đánh giá lại tài khaonr ngoại tệ
    DanhGiaLaiTaiKhoanNgoaiTe: '0608',
    DanhGiaLaiTaiKhoanNgoaiTe_Xem: '060801',

    // Lập báo cáo tài chính
    LapBaoCaoTaiChinh: '0611',
    LapBaoCaoTaiChinh_Xem: '061101',
    LapBaoCaoTaiChinh_Them: '061102',
    LapBaoCaoTaiChinh_Sua: '061103',
    LapBaoCaoTaiChinh_Xoa: '061104',
    LapBaoCaoTaiChinh_In: '061106',
    LapBaoCaoTaiChinh_KetXuat: '061107',

    // 7. Quản lý hóa đơn
    QLHD: '07',

    // Khởi tạo mẫu hóa đơn
    KTHD: '0701',
    KTHD_XEM: '070101',
    KTHD_THEM: '070102',
    KTHD_SUA: '070103',
    KTHD_XOA: '070104',
    KTHD_KET_XUAT: '070107',

    // Đăng ký sử dụng hóa đơn
    DKSD: '0702',
    DKSD_XEM: '070201',
    DKSD_THEM: '070202',
    DKSD_SUA: '070203',
    DKSD_XOA: '070204',
    DKSD_KET_XUAT: '070207',

    // Thông báo phát hành hóa đơn
    TBPH: '0703',
    TBPH_XEM: '070301',
    TBPH_THEM: '070302',
    TBPH_SUA: '070303',
    TBPH_XOA: '070304',
    TBPH_KET_XUAT: '070307',
    TBPH_IN: '070306',

    // Hủy hóa Đơn
    HuyHoaDon: '0704',
    HuyHoaDon_XEM: '070401',
    HuyHoaDon_THEM: '070402',
    HuyHoaDon_SUA: '070403',
    HuyHoaDon_XOA: '070404',
    HuyHoaDon_IN: '070406',
    HuyHoaDon_KET_XUAT: '070407',

    // Mất cháy hỏng hoá đơn
    MCH: '0705',
    MCH_XEM: '070501',
    MCH_THEM: '070502',
    MCH_SUA: '070503',
    MCH_XOA: '070504',
    MCH_KET_XUAT: '070507',
    MCH_IN: '070506',

    // Xóa hóa đơn
    XHD: '0706',
    XHD_XEM: '070601',
    XHD_THEM: '070602',
    XHD_SUA: '070603',
    XHD_XOA: '070604',
    XHD_IN: '070606',
    XHD_KET_XUAT: '070607',

    // 8. Hóa đơn điện tử
    HDDT: '08',

    // Danh sách hóa đơn
    DSHD: '0801',
    DSHD_XEM: '080101',
    DSHD_PHAT_HANH: '080110',
    DSHD_THAY_THE_HOA_DON: '080111',
    DSHD_DIEU_CHINH_HOA_DON: '080112',
    DSHD_HUY_BO_HOA_DON: '080113',
    DSHD_GUI_EMAIL: '080114',
    DSHD_TAO_CHO: '080115',

    // Danh sách hóa đơn chờ ký
    DSHD_CHO_KY: '0802',
    DSHD_CHO_KY_XEM: '080201',
    DSHD_CHO_KY_PHAT_HANH: '080210',

    // 10. Tài sản cố định
    TSCD: '10',

    // Khai báo tscd
    KhaiBao: '1001',
    KhaiBao_XEM: '100101',
    KhaiBao_THEM: '100102',
    KhaiBao_SUA: '100103',
    KhaiBao_XOA: '100104',
    KhaiBao_KET_XUAT: '100107',
    KhaiBao_Import: '100108',

    // Hóa đơn thay thế
    HD_THAY_THE: '0803',
    HD_THAY_THE_XEM: '080301',

    // Hóa đơn điều chỉnh
    HD_DIEU_CHINH: '0804',
    HD_DIEU_CHINH_XEM: '080401',

    // Hóa đơn hủy
    HD_HUY: '0805',
    HD_HUY_XEM: '080501',

    // Chuyển đổi hóa đơn
    CDHD: '0806',
    CDHD_XEM: '080601',
    CDHD_CHUNG_MINH_NGUON_GOC: '080616',
    CDHD_LUU_TRU: '080617',

    // Kết nối hóa đơn
    KNHD: '0807',
    KNHD_KET_NOI_HD: '080718',

    // 11. Lương
    Luong: '11',

    // Chấm công
    ChamCong: '1101',
    ChamCong_XEM: '110101',
    ChamCong_Them: '110102',
    ChamCong_Sua: '110103',
    ChamCong_Xoa: '110104',
    ChamCong_In: '110106',
    ChamCong_KetXuat: '110107',

    // Tổng hợp chấm công
    TongHopChamCong: '1102',
    TongHopChamCong_XEM: '110201',
    TongHopChamCong_Them: '110202',
    TongHopChamCong_Sua: '110203',
    TongHopChamCong_Xoa: '110204',
    TongHopChamCong_In: '110206',
    TongHopChamCong_KetXuat: '110207',

    // Bảng lương
    BangLuong: '1103',
    BangLuong_XEM: '110301',
    BangLuong_Them: '110302',
    BangLuong_Sua: '110303',
    BangLuong_Xoa: '110304',
    BangLuong_In: '110306',
    BangLuong_KetXuat: '110307',

    // Hạch toán chi phí lương
    HachToanChiPhiLuong: '1104',
    HachToanChiPhiLuong_XEM: '110401',
    HachToanChiPhiLuong_Them: '110402',
    HachToanChiPhiLuong_Sua: '110403',
    HachToanChiPhiLuong_Xoa: '110404',
    HachToanChiPhiLuong_GhiSo: '110405',
    HachToanChiPhiLuong_In: '110406',
    HachToanChiPhiLuong_KetXuat: '110407',

    // Nộp tiền bảo hiểm
    NopTienBaoHiem: '1105',
    NopTienBaoHiem_XEM: '110501',
    NopTienBaoHiem_Them: '110502',

    // Thanh toán lương
    ThanhToanLuong: '1106',
    ThanhToanLuong_XEM: '110601',
    ThanhToanLuong_Them: '110602',

    Thue: '13',
    // Tờ khai thuế GTGT khấu trừ
    Thue01GTGTKhauTru: '1301',
    Thue01GTGTKhauTru_XEM: '130101',
    Thue01GTGTKhauTru_Them: '130102',
    Thue01GTGTKhauTru_Sua: '130103',
    Thue01GTGTKhauTru_Xoa: '130104',
    Thue01GTGTKhauTru_In: '130106',
    Thue01GTGTKhauTru_KetXuat: '130107',
    // Tờ khai thuế GTGT trực tiếp
    Thue01GTGTTrucTiep: '1302',
    Thue01GTGTTrucTiep_XEM: '130201',
    Thue01GTGTTrucTiep_Them: '130202',
    Thue01GTGTTrucTiep_Sua: '130203',
    Thue01GTGTTrucTiep_Xoa: '130204',
    Thue01GTGTTrucTiep_In: '130206',
    Thue01GTGTTrucTiep_KetXuat: '130207',
    // Tờ khai thuế GTGT cho dự án đầu tư
    Thue01GTGTDauTu: '1303',
    Thue01GTGTDauTu_XEM: '130301',
    Thue01GTGTDauTu_Them: '130302',
    Thue01GTGTDauTu_Sua: '130303',
    Thue01GTGTDauTu_Xoa: '130304',
    Thue01GTGTDauTu_In: '130306',
    Thue01GTGTDauTu_KetXuat: '130307',
    // Quyết toán thuế TNDN năm
    ThueQuyetToanTNDNNam: '1304',
    ThueQuyetToanTNDNNam_XEM: '130401',
    ThueQuyetToanTNDNNam_Them: '130402',
    ThueQuyetToanTNDNNam_Sua: '130403',
    ThueQuyetToanTNDNNam_Xoa: '130404',
    ThueQuyetToanTNDNNam_In: '130406',
    ThueQuyetToanTNDNNam_KetXuat: '130407',
    // Tờ khai thuế tài nguyên
    ThueKhaiThueTaiNguyen: '1305',
    ThueKhaiThueTaiNguyen_XEM: '130501',
    ThueKhaiThueTaiNguyen_Them: '130502',
    ThueKhaiThueTaiNguyen_Sua: '130503',
    ThueKhaiThueTaiNguyen_Xoa: '130504',
    ThueKhaiThueTaiNguyen_In: '130506',
    ThueKhaiThueTaiNguyen_KetXuat: '130507',
    // Quyết toán thuế tài nguyên
    ThueQuyetToanThueTaiNguyen: '1306',
    ThueQuyetToanThueTaiNguyen_XEM: '130601',
    ThueQuyetToanThueTaiNguyen_Them: '130602',
    ThueQuyetToanThueTaiNguyen_Sua: '130603',
    ThueQuyetToanThueTaiNguyen_Xoa: '130604',
    ThueQuyetToanThueTaiNguyen_In: '130606',
    ThueQuyetToanThueTaiNguyen_KetXuat: '130607',
    // Khấu trừ thuế GTGT
    ThueKhauTruThueGTGT: '1307',
    ThueKhauTruThueGTGT_XEM: '130701',
    // Nộp thuế
    ThueNopThue: '1308',
    ThueNopThue_XEM: '130801',

    HeThong: '14',
    // Ngay hach toan
    NgayHachToan_Xem: '140101',
    NgayHachToan_Sua: '140103',

    // TuyChon
    TuyChon_Xem: '140201',
    TuyChon_Sua: '140203',

    // TuyChon
    NhatKyTruyCap_Xem: '140301',
    NhatKyTruyCap_KetXuat: '140307',

    // 15.Danh Muc
    DanhMuc: '15',

    // Co cau to chuc
    CoCauToChuc: '1501',
    CoCauToChuc_Xem: '150101',
    CoCauToChuc_Them: '150102',
    CoCauToChuc_Sua: '150103',
    CoCauToChuc_Xoa: '150104',

    // He thong tai khoan
    HeThongTaiKhoan: '1502',
    HeThongTaiKhoan_Xem: '150201',
    HeThongTaiKhoan_Them: '150202',
    HeThongTaiKhoan_Sua: '150203',
    HeThongTaiKhoan_Xoa: '150204',

    // Tai khoan ket chuyen
    TaiKhoanKetChuyen: '1503',
    TaiKhoanKetChuyen_Xem: '150301',
    TaiKhoanKetChuyen_Them: '150302',
    TaiKhoanKetChuyen_Sua: '150303',
    TaiKhoanKetChuyen_Xoa: '150304',

    // Tai khoan ngam dinh
    TaiKhoanNgamDinh: '1504',
    TaiKhoanNgamDinh_Xem: '150401',
    TaiKhoanNgamDinh_Sua: '150403',

    // Danh Muc Khach Hang
    DanhMucKhachHang: '1506',
    DanhMucKhachHang_Xem: '150601',
    DanhMucKhachHang_Them: '150602',
    DanhMucKhachHang_Sua: '150603',
    DanhMucKhachHang_Xoa: '150604',
    DanhMucKhachHang_Upload: '150608',
    DanhMucKhachHang_KetXuat: '150607',

    // Danh Muc Khach Hang
    DanhMucNhaCungCap: '1507',
    DanhMucNhaCungCap_Xem: '150701',
    DanhMucNhaCungCap_Them: '150702',
    DanhMucNhaCungCap_Sua: '150703',
    DanhMucNhaCungCap_Xoa: '150704',
    DanhMucNhaCungCap_Upload: '150708',
    DanhMucNhaCungCap_KetXuat: '150707',

    // Danh Muc Khach Hang
    DanhMucNhanVien: '1508',
    DanhMucNhanVien_Xem: '150801',
    DanhMucNhanVien_Them: '150802',
    DanhMucNhanVien_Sua: '150803',
    DanhMucNhanVien_Xoa: '150804',
    DanhMucNhanVien_Upload: '150808',
    DanhMucNhanVien_Export: '150807',

    // Danh Muc Khach Hang va Nha Cung Cap
    DanhMucKhachHangVaNhaCungCap: '1509',
    DanhMucKhachHangVaNhaCungCap_Xem: '150901',
    DanhMucKhachHangVaNhaCungCap_Them: '150902',
    DanhMucKhachHangVaNhaCungCap_Sua: '150903',
    DanhMucKhachHangVaNhaCungCap_Xoa: '150904',
    DanhMucKhachHangVaNhaCungCap_KetXuat: '150907',
    DanhMucKhachHangVaNhaCungCap_Upload: '150908',

    // Danh Muc Kho
    DanhMucKho: '1511',
    DanhMucKho_Xem: '151101',
    DanhMucKho_Them: '151102',
    DanhMucKho_Sua: '151103',
    DanhMucKho_Xoa: '151104',
    DanhMucKho_Upload: '151108',
    DanhMucKho_KetXuat: '151107',

    // Danh Muc Don Vi Tinh
    DanhMucDonViTinh: '1512',
    DanhMucDonViTinh_Xem: '151201',
    DanhMucDonViTinh_Them: '151202',
    DanhMucDonViTinh_Sua: '151203',
    DanhMucDonViTinh_Xoa: '151204',

    // Danh Muc Loai Vat Tu Hang Hoa
    DanhMucLoaiVatTuHangHoa: '1513',
    DanhMucLoaiVatTuHangHoa_Xem: '151301',
    DanhMucLoaiVatTuHangHoa_Them: '151302',
    DanhMucLoaiVatTuHangHoa_Sua: '151303',
    DanhMucLoaiVatTuHangHoa_Xoa: '151304',
    DanhMucLoaiVatTuHangHoa_KetXuat: '151307',
    DanhMucLoaiVatTuHangHoa_Upload: '151308',

    // Danh Muc Bieu Thue Tai Nguyen
    DanhMucBieuThueTaiNguyen: '1518',
    DanhMucBieuThueTaiNguyen_Xem: '151801',
    DanhMucBieuThueTaiNguyen_Them: '151802',
    DanhMucBieuThueTaiNguyen_Sua: '151803',
    DanhMucBieuThueTaiNguyen_Xoa: '151804',

    // Danh Muc Tai Khoan Ngan Hang
    DanhMucTaiKhoanNganHang: '1521',
    DanhMucTaiKhoanNganHang_Xem: '152101',
    DanhMucTaiKhoanNganHang_Them: '152102',
    DanhMucTaiKhoanNganHang_Sua: '152103',
    DanhMucTaiKhoanNganHang_Xoa: '152104',
    DanhMucTaiKhoanNganHang_KetXuat: '152107',
    DanhMucTaiKhoanNganHang_Upload: '152108',

    // Danh Muc dinh khoan tu dong
    DanhMucDinhKhoanTuDong: '1505',
    DanhMucDinhKhoanTuDong_Xem: '150501',
    DanhMucDinhKhoanTuDong_Them: '150502',
    DanhMucDinhKhoanTuDong_Sua: '150503',
    DanhMucDinhKhoanTuDong_Xoa: '150504',

    // Danh Muc cong cu dung cu
    DanhMucCongCuDungCu: '1514',
    DanhMucCongCuDungCu_Xem: '151401',
    DanhMucCongCuDungCu_Them: '151402',
    DanhMucCongCuDungCu_Sua: '151403',
    DanhMucCongCuDungCu_Xoa: '151404',
    DanhMucCongCuDungCu_Export: '151407',
    DanhMucCongCuDungCu_Upload: '151408',

    // Danh Muc vat tu hang hoa
    DanhMucVatTuHangHoa: '1510',
    DanhMucVatTuHangHoa_Xem: '151001',
    DanhMucVatTuHangHoa_Them: '151002',
    DanhMucVatTuHangHoa_Sua: '151003',
    DanhMucVatTuHangHoa_Xoa: '151004',
    DanhMucVatTuHangHoa_Upload: '151008',

    // Danh Muc ngan hang
    DanhMucNganHang: '1520',
    DanhMucNganHang_Xem: '152001',
    DanhMucNganHang_Them: '152002',
    DanhMucNganHang_Sua: '152003',
    DanhMucNganHang_Xoa: '152004',

    // Danh muc ký hiệu chấm công
    DanhMucKyHieuChamCong: '1523',
    DanhMucKyHieuChamCong_Xem: '152301',
    DanhMucKyHieuChamCong_Them: '152302',
    DanhMucKyHieuChamCong_Sua: '152303',
    DanhMucKyHieuChamCong_Xoa: '152304',

    // Danh Muc the tin dung
    DanhMucTheTinDung: '1522',
    DanhMucTheTinDung_Xem: '152201',
    DanhMucTheTinDung_Them: '152202',
    DanhMucTheTinDung_Sua: '152203',
    DanhMucTheTinDung_Xoa: '152204',
    DanhMucTheTinDung_KetXuat: '152207',
    DanhMucTheTinDung_Upload: '152208',

    // Danh Muc Quy Dinh Luong Thue Bao Hiem
    QuyDinhLuongThueBaoHiem: '1525',
    QuyDinhLuongThueBaoHiem_Xem: '152501',
    QuyDinhLuongThueBaoHiem_Them: '152502',
    QuyDinhLuongThueBaoHiem_Sua: '152503',
    QuyDinhLuongThueBaoHiem_Xoa: '152504',

    // Danh Muc Loai Tien
    DanhMucLoaiTien: '1530',
    DanhMucLoaiTien_Xem: '153001',
    DanhMucLoaiTien_Them: '153002',
    DanhMucLoaiTien_Sua: '153003',
    DanhMucLoaiTien_Xoa: '153004',

    // Danh Muc Thu Chi
    DanhMucThuChi: '1529',
    DanhMucThuChi_Xem: '152901',
    DanhMucThuChi_Them: '152902',
    DanhMucThuChi_Sua: '152903',
    DanhMucThuChi_Xoa: '152904',
    DanhMucThuChi_KetXuat: '152907',
    DanhMucThuChi_Import: '152908',

    // Danh Muc Phuong Thuc Van Chuyen
    DanhMucPhuongThucVanChuyen: '1532',
    DanhMucPhuongThucVanChuyen_Xem: '153201',
    DanhMucPhuongThucVanChuyen_Them: '153202',
    DanhMucPhuongThucVanChuyen_Sua: '153203',
    DanhMucPhuongThucVanChuyen_Xoa: '153204',
    DanhMucPhuongThucVanChuyen_Upload: '153208',
    DanhMucPhuongThucVanChuyen_Export: '153207',

    // Danh Muc Dieu khoan thanh toan
    DanhMucDieuKhoanThanhToan: '1531',
    DanhMucDieuKhoanThanhToan_Xem: '153101',
    DanhMucDieuKhoanThanhToan_Them: '153102',
    DanhMucDieuKhoanThanhToan_Sua: '153103',
    DanhMucDieuKhoanThanhToan_Xoa: '153104',
    DanhMucDieuKhoanThanhToan_KetXuat: '153107',
    DanhMucDieuKhoanThanhToan_Upload: '153108',

    // Danh Muc Ma Thong Ke
    DanhMucMaThongKe: '1528',
    DanhMucMaThongKe_Xem: '152801',
    DanhMucMaThongKe_Them: '152802',
    DanhMucMaThongKe_Sua: '152803',
    DanhMucMaThongKe_Xoa: '152804',
    DanhMucMaThongKe_KetXuat: '152807',
    DanhMucMaThongKe_Import: '152808',

    // Khoan Muc Chi Phi
    DanhMucKhoanMucChiPhi: '1527',
    DanhMucKhoanMucChiPhi_Xem: '152701',
    DanhMucKhoanMucChiPhi_Them: '152702',
    DanhMucKhoanMucChiPhi_Sua: '152703',
    DanhMucKhoanMucChiPhi_Xoa: '152704',
    DanhMucKhoanMucChiPhi_Export: '152707',
    DanhMucKhoanMucChiPhi_Upload: '152708',

    // Danh Muc nhom HHDV chiu theu TTDB
    DanhMucHHDVChiuThueTTDB: '1517',
    DanhMucHHDVChiuThueTTDB_Xem: '151701',
    DanhMucHHDVChiuThueTTDB_Them: '151702',
    DanhMucHHDVChiuThueTTDB_Sua: '151703',
    DanhMucHHDVChiuThueTTDB_Xoa: '151704',

    // Dinh muc nguyen vat lieu
    DinhMucNguyenVatLieu: '1519',
    DinhMucNguyenVatLieu_Xem: '151901',
    DinhMucNguyenVatLieu_Them: '151902',
    DinhMucNguyenVatLieu_Sua: '151903',
    DinhMucNguyenVatLieu_Xoa: '151904',
    DinhMucNguyenVatLieu_In: '151906',
    DinhMucNguyenVatLieu_Upload: '151908',
    DinhMucNguyenVatLieu_KetXuat: '151907',

    // Danh Muc Doi Tuong Tap Hop Chi Phi
    DanhMucDTTHCP: '1526',
    DanhMucDTTHCP_Xem: '152601',
    DanhMucDTTHCP_Them: '152602',
    DanhMucDTTHCP_Sua: '152603',
    DanhMucDTTHCP_Xoa: '152604',
    DanhMucDTTHCP_KetXuat: '152607',
    DanhMucDTTHCP_Upload: '152608',

    // Danh Muc Loai Tai San Co Dinh
    DanhMucLoaiTSCD: '1516',
    DanhMucLoaiTSCD_Xem: '151601',
    DanhMucLoaiTSCD_Them: '151602',
    DanhMucLoaiTSCD_Sua: '151603',
    DanhMucLoaiTSCD_Xoa: '151604',

    // Danh Muc Bieu Thue Thu Nhap ca Nhan
    DanhMucBieuThueTNCN: '1524',
    DanhMucBieuThueTNCN_Xem: '152401',
    DanhMucBieuThueTNCN_Them: '152402',
    DanhMucBieuThueTNCN_Sua: '152403',
    DanhMucBieuThueTNCN_Xoa: '152404',

    // Danh Muc Tai San Co Dinh
    DanhMucTSCD: '1515',
    DanhMucTSCD_Xem: '151501',
    DanhMucTSCD_Them: '151502',
    DanhMucTSCD_Sua: '151503',
    DanhMucTSCD_Xoa: '151504',
    DanhMucTSCD_Export: '151507',
    DanhMucTSCD_Import: '151508',

    // Danh Muc Nhom Gia Ban
    DanhMucNhomGiaBan: '1533',
    DanhMucNhomGiaBan_Xem: '153301',
    DanhMucNhomGiaBan_Them: '153302',
    DanhMucNhomGiaBan_Sua: '153303',
    DanhMucNhomGiaBan_Xoa: '153304',
    DanhMucNhomGiaBan_Upload: '153308',
    DanhMucNhomGiaBan_KetXuat: '153307',

    // 16. Tiện ích
    TienIch: '16',

    // Số dư đầu kỳ
    SoDuDauKy: '1601',
    SoDuDauKy_Xem: '160101',
    SoDuDauKy_Sua: '160103',
    SoDuDauKy_KetXuat: '160107',
    SoDuDauKy_NhapTuExcel: '160108',

    // Dữ liệu
    DuLieu: '16',

    // Số dư đầu kỳ
    QLDuLieu: '1602',
    QLDuLieu_Xem: '160201',
    QLDuLieu_Xoa: '160204',
    QLDuLieu_SaoLuuDuLieu: '160219',
    QLDuLieu_KhoiPhucDuLieu: '160220',

    // Đánh lại số chứng từ
    DanhLaiSoChungTu: '1603',
    DanhLaiSoChungTu_Xem: '160301',
    DanhLaiSoChungTu_Sua: '160303',

    // Tìm kiếm chứng từ
    TimKiemChungTu: '1604',
    TimKiemChungTu_Xem: '160401',

    // In chứng từ hàng loạt
    InChungTuHangLoat: '1605',
    InChungTuHangLoat_Xem: '160501',
    InChungTuHangLoat_In: '160502',

    // Cấu hình giao diện nhập liệu
    CauHinhGiaoDienNhapLieu: '1607',
    CauHinhGiaoDienNhapLieu_Xem: '160701',
    CauHinhGiaoDienNhapLieu_Them: '160702',
    CauHinhGiaoDienNhapLieu_Sua: '160703',
    CauHinhGiaoDienNhapLieu_Xoa: '160704',

    // Thiết lập công thức báo cáo tài chính
    ThietLapCongThucBaoCaoTaiChinh: '1606',
    ThietLapCongThucBaoCaoTaiChinh_Xem: '160601',

    // phân bổ chi phí trả trước
    PhanBoChiPhiTRaTruoc: '0605',
    PhanBoChiPhiTRaTruoc_Xem: '060501',
    PhanBoChiPhiTRaTruoc_Them: '060502',
    PhanBoChiPhiTRaTruoc_Sua: '060503',
    PhanBoChiPhiTRaTruoc_Xoa: '060504',
    PhanBoChiPhiTRaTruoc_GhiSo: '060505',
    PhanBoChiPhiTRaTruoc_In: '060506',
    PhanBoChiPhiTRaTruoc_KetXuat: '060507',

    // bừ trừ công nọ
    BuTruCongNo: '0606',
    BuTruCongNo_Xem: '060601',

    // Chi phí trả trước
    ChiPhiTRaTruoc: '0604',
    ChiPhiTRaTruoc_Xem: '060401',
    ChiPhiTRaTruoc_Them: '060402',
    ChiPhiTRaTruoc_Sua: '060403',
    ChiPhiTRaTruoc_Xoa: '060404',
    ChiPhiTRaTruoc_KetXuat: '060407',
    ChiPhiTRaTruoc_Upload: '060408',

    // Ghi tăng CCDC
    CongCuDungCu: '09',
    GhiTangCCDC: '0902',
    GhiTangCCDC_Xem: '090201',
    GhiTangCCDC_Them: '090202',
    GhiTangCCDC_Sua: '090203',
    GhiTangCCDC_Xoa: '090204',
    GhiTangCCDC_GhiSo: '090205',
    GhiTangCCDC_KetXuat: '090207',
    GhiTangCCDC_QuanTriMau: '090209',

    // Phân bổ CCDC
    PhanBoCCDC: '0903',
    PhanBoCCDC_Xem: '090301',
    PhanBoCCDC_Them: '090302',
    PhanBoCCDC_Sua: '090303',
    PhanBoCCDC_Xoa: '090304',
    PhanBoCCDC_GhiSo: '090305',
    PhanBoCCDC_In: '090306',
    PhanBoCCDC_KetXuat: '090307',

    // Kiểm kê CCDC
    KiemKeCCDC: '0904',
    KiemKeCCDC_Xem: '090401',
    KiemKeCCDC_Them: '090402',
    KiemKeCCDC_Sua: '090403',
    KiemKeCCDC_Xoa: '090404',
    KiemKeCCDC_In: '090406',
    KiemKeCCDC_KetXuat: '090407',

    // Ghi giảm CCDC
    GhiGiamCCDC: '0905',
    GhiGiamCCDC_Xem: '090501',
    GhiGiamCCDC_Them: '090502',
    GhiGiamCCDC_Sua: '090503',
    GhiGiamCCDC_Xoa: '090504',
    GhiGiamCCDC_GhiSo: '090505',
    GhiGiamCCDC_In: '090506',
    GhiGiamCCDC_KetXuat: '090507',

    // Điều chỉnh CCDC
    DieuChinhCCDC: '0906',
    DieuChinhCCDC_Xem: '090601',
    DieuChinhCCDC_Them: '090602',
    DieuChinhCCDC_Sua: '090603',
    DieuChinhCCDC_Xoa: '090604',
    DieuChinhCCDC_GhiSo: '090605',
    DieuChinhCCDC_In: '090606',
    DieuChinhCCDC_KetXuat: '090607',
    // Điều chuyển CCDC
    DieuChuyenCCDC: '0907',
    DieuChuyenCCDC_Xem: '090701',
    DieuChuyenCCDC_Them: '090702',
    DieuChuyenCCDC_Sua: '090703',
    DieuChuyenCCDC_Xoa: '090704',
    DieuChuyenCCDC_GhiSo: '090705',
    DieuChuyenCCDC_In: '090706',
    DieuChuyenCCDC_KetXuat: '090707',

    // Ghi tăng TSCD
    TaiSanCoDinh: '10',
    GhiTangTSCD: '1002',
    GhiTangTSCD_Xem: '100201',
    GhiTangTSCD_Them: '100202',
    GhiTangTSCD_Sua: '100203',
    GhiTangTSCD_Xoa: '100204',
    GhiTangTSCD_GhiSo: '100205',
    GhiTangTSCD_In: '100206',
    GhiTangTSCD_KetXuat: '100207',

    // Khấu hao TSCD
    KhauHaoTSCD: '1003',
    KhauHaoTSCD_Xem: '100301',
    KhauHaoTSCD_Them: '100302',
    KhauHaoTSCD_Sua: '100303',
    KhauHaoTSCD_Xoa: '100304',
    KhauHaoTSCD_GhiSo: '100305',
    KhauHaoTSCD_In: '100306',
    KhauHaoTSCD_KetXuat: '100307',
    KhauHaoTSCD_QuanTriMau: '100309',

    // Kiểm kê TSCD
    KiemKeTSCD: '1004',
    KiemKeTSCD_Xem: '100401',
    KiemKeTSCD_Them: '100402',
    KiemKeTSCD_Sua: '100403',
    KiemKeTSCD_Xoa: '100404',
    KiemKeTSCD_In: '100406',
    KiemKeTSCD_KetXuat: '100407',

    // Ghi giảm TSCD
    GhiGiamTSCD: '1005',
    GhiGiamTSCD_Xem: '100501',
    GhiGiamTSCD_Them: '100502',
    GhiGiamTSCD_Sua: '100503',
    GhiGiamTSCD_Xoa: '100504',
    GhiGiamTSCD_GhiSo: '100505',
    GhiGiamTSCD_In: '100506',
    GhiGiamTSCD_KetXuat: '100507',

    // Điều chỉnh TSCD
    DieuChinhTSCD: '1006',
    DieuChinhTSCD_Xem: '100601',
    DieuChinhTSCD_Them: '100602',
    DieuChinhTSCD_Sua: '100603',
    DieuChinhTSCD_Xoa: '100604',
    DieuChinhTSCD_GhiSo: '100605',
    DieuChinhTSCD_In: '100606',
    DieuChinhTSCD_KetXuat: '100607',
    // Điều chuyển TSCD
    DieuChuyenTSCD: '1007',
    DieuChuyenTSCD_Xem: '100701',
    DieuChuyenTSCD_Them: '100702',
    DieuChuyenTSCD_Sua: '100703',
    DieuChuyenTSCD_Xoa: '100704',
    DieuChuyenTSCD_GhiSo: '100705',
    DieuChuyenTSCD_In: '100706',
    DieuChuyenTSCD_KetXuat: '100707',

    // Báo cáo
    BaoCao: '17',
    BangCanDoiSoPhatSinh: '1701',
    BangCanDoiSoPhatSinh_Xem: '170101',
    BangCanDoiSoPhatSinh_KetXuat: '170107',
    BangCanDoiKeToan: '1702',
    BangCanDoiKeToan_Xem: '170201',
    BangCanDoiKeToan_KetXuat: '170207',
    BaoCaoKetQuaHoatDongKinhDoanh: '1703',
    BaoCaoKetQuaHoatDongKinhDoanh_Xem: '170301',
    BaoCaoKetQuaHoatDongKinhDoanh_KetXuat: '170307',
    BaoCaoLuuChuyenTienTeTT: '1704',
    BaoCaoLuuChuyenTienTeTT_Xem: '170401',
    BaoCaoLuuChuyenTienTeTT_KetXuat: '170407',
    BaoCaoLuuChuyenTienTeGT: '1705',
    BaoCaoLuuChuyenTienTeGT_Xem: '170501',
    BaoCaoLuuChuyenTienTeGT_KetXuat: '170507',
    BanThuyetMinhBaoCaoTaiChinh: '1706',
    BanThuyetMinhBaoCaoTaiChinh_Xem: '170601',
    BanThuyetMinhBaoCaoTaiChinh_KetXuat: '170607',
    SoChiTietThanhToanVoiNguoiMuaNguoiBan: '1759',
    SoChiTietThanhToanVoiNguoiMuaNguoiBan_Xem: '175901',
    SoChiTietThanhToanVoiNguoiMuaNguoiBan_KetXuat: '175907',
    SoChiTietThanhToanVoiNguoiMuaNguoiBanBangNgoaiTe: '1760',
    SoChiTietThanhToanVoiNguoiMuaNguoiBanBangNgoaiTe_Xem: '176001',
    SoChiTietThanhToanVoiNguoiMuaNguoiBanBangNgoaiTe_KetXuat: '176007',
    SoQuyTienMat: '1707',
    SoQuyTienMat_Xem: '170701',
    SoQuyTienMat_KetXuat: '170707',
    SoKeToanChiTietQuyTienMat: '1708',
    SoKeToanChiTietQuyTienMat_Xem: '170801',
    SoKeToanChiTietQuyTienMat_KetXuat: '170807',
    SoTienGuiNganHang: '1709',
    SoTienGuiNganHang_Xem: '170901',
    SoTienGuiNganHang_KetXuat: '170907',
    BangKeSoDuNganHang: '1710',
    BangKeSoDuNganHang_Xem: '171001',
    BangKeSoDuNganHang_KetXuat: '171007',
    SoChiTietVatLieu: '1719',
    SoChiTietVatLieu_Xem: '171901',
    SoChiTietVatLieu_KetXuat: '171907',
    BangTongHopChiTietVatLieu: '1720',
    BangTongHopChiTietVatLieu_Xem: '172001',
    BangTongHopChiTietVatLieu_KetXuat: '172007',
    TheKho: '1721',
    TheKho_Xem: '172101',
    TheKho_KetXuat: '172107',
    TongHopTonKho: '1722',
    TongHopTonKho_Xem: '172201',
    TongHopTonKho_KetXuat: '172207',
    TongHopTonKhoSoLoHanDung: '1761',
    TongHopTonKhoSoLoHanDung_Xem: '176101',
    TongHopTonKhoSoLoHanDung_KetXuat: '176107',
    SoChiTietCongNoNhanVien: '1744',
    SoChiTietCongNoNhanVien_Xem: '174401',
    SoChiTietCongNoNhanVien_KetXuat: '174407',
    TongHopTonKhoHanDungHangHoa: '1765',
    TongHopTonKhoHanDungHangHoa_Xem: '176501',
    TongHopTonKhoHanDungHangHoa_KetXuat: '176507',
    TongHopTonKhoTheoMaQuyCach: '1769',
    TongHopTonKhoTheoMaQuyCach_Xem: '176901',
    TongHopTonKhoTheoMaQuyCach_KetXuat: '176907',
    BaoCaoTienDoSanXuat: '1804',
    BaoCaoTienDoSanXuat_Xem: '180401',
    BaoCaoTienDoSanXuat_KetXuat: '180407',
    TongHopTonKhoTrenNhieuKho_Xem: '181001',
    TongHopTonKhoTrenNhieuKho_KetXuat: '181007',

    SoDangKyChungTuGhiSo: '1766',
    SoDangKyChungTuGhiSo_Xem: '176601',
    SoDangKyChungTuGhiSo_KetXuat: '176607',

    // Bang ke chung tu chua co chung tu ghi so
    BangKeChungTuChuaLapChungTuGhiSo: '1767',
    BangKeChungTuChuaLapChungTuGhiSo_Xem: '176701',
    BangKeChungTuChuaLapChungTuGhiSo_KetXuat: '176707',

    SoTheoDoiMaThongKeTheoTaiKhoan: '1736',
    SoTheoDoiMaThongKeTheoTaiKhoan_Xem: '173601',
    SoTheoDoiMaThongKeTheoTaiKhoan_KetXuat: '173607',

    SoTheoDoiMaThongKeTheoKhoanMucChiPhi: '1737',
    SoTheoDoiMaThongKeTheoKhoanMucChiPhi_Xem: '173701',
    SoTheoDoiMaThongKeTheoKhoanMucChiPhi_KetXuat: '173707',

    BaoCaoSoNhatKyChung_Xem: '172301',
    BaoCaoSoNhatKyChung_KetXuat: '172307',

    BaoCaoSoCaiNhatKyChung_Xem: '172401',
    BaoCaoSoCaiNhatKyChung_KetXuat: '172407',

    BaoCaoSoChiTietCacTaiKhoan_Xem: '172701',
    BaoCaoSoChiTietCacTaiKhoan_KetXuat: '172707',

    BaoCaoSoTheoDoiThanhToanBangNgoaiTe_Xem: '174001',
    BaoCaoSoTheoDoiThanhToanBangNgoaiTe_KetXuat: '174007',

    BaoCaoSoTheoDoiLaiLoTheoMatHang_Xem: '174101',
    BaoCaoSoTheoDoiLaiLoTheoMatHang_KetXuat: '174107',

    SoTheoDoiCongNoPhaiThuTheoMatHang_Xem: '175501',
    SoTheoDoiCongNoPhaiThuTheoMatHang_KetXuat: '175507',

    SoNhatKyThuTien_Xem: '172801',
    SoNhatKyThuTien_KetXuat: '172807',
    SoNhatKyChiTien_Xem: '172901',
    SoNhatKyChiTien_KetXuat: '172907',

    BangKeHDChungTuVTHHMuaVao_Xem: '173001',
    BangKeHDChungTuVTHHMuaVao_KetXuat: '173007',
    BangKeHDChungTuVTHHMuaVaoQuanTri_Xem: '173101',
    BangKeHDChungTuVTHHMuaVaoQuanTri_KetXuat: '173107',
    BangKeHDChungTuVTHHBanRa_Xem: '173201',
    BangKeHDChungTuVTHHBanRa_KetXuat: '173207',
    BangKeHDChungTuVTHHBanRaQuanTri_Xem: '173301',
    BangKeHDChungTuVTHHBanRaQuanTri_KetXuat: '173307',
    CacKhoanPhaiNopNSNN_Xem: '179901',
    CacKhoanPhaiNopNSNN_KetXuat: '179907',

    SoChiTietMuaHang_Xem: '171101',
    SoChiTietMuaHang_KetXuat: '171107',

    SoNhatKyMuaHang_Xem: '171201',
    SoNhatKyMuaHang_KetXuat: '171207',

    SoChiTietBanHang_Xem: '171501',
    SoChiTietBanHang_KetXuat: '171507',

    SoNhatKyBanHang_Xem: '171601',
    SoNhatKyBanHang_KetXuat: '171607',

    TongHopCongNoPhaiTra_Xem: '171401',
    TongHopCongNoPhaiTra_KetXuat: '171407',
    ChiTietCongNoPhaiTra_Xem: '171301',
    ChiTietCongNoPhaiTra_KetXuat: '171307',

    ChiTietCongNoPhaiTraTheoHoaDon_Xem: '180501',
    ChiTietCongNoPhaiTraTheoHoaDon_KetXuat: '180507',

    TongHopCongNoPhaiThu_Xem: '171801',
    TongHopCongNoPhaiThu_KetXuat: '171807',
    ChiTietCongNoPhaiThu_Xem: '171701',
    ChiTietCongNoPhaiThu_KetXuat: '171707',

    BangPhanBoChiPhiTraTruoc_Xem: '172601',
    BangPhanBoChiPhiTraTruoc_KetXuat: '172607',

    SoTongHopTamUngTheoNV_Xem: '176401',
    SoTongHopTamUngTheoNV_KetXuat: '176407',
    // Khai báo ccdc đầu kì
    KhaiBaoCCDCDauKy_Xem: '090101',
    KhaiBaoCCDCDauKy_Them: '090102',
    KhaiBaoCCDCDauKy_Sua: '090103',
    KhaiBaoCCDCDauKy_Xoa: '090104',
    KhaiBaoCCDCDauKy_KetXuat: '090107',
    KhaiBaoCCDCDauKy_Upload: '090108',

    SoChiPhiSanXuatKinhDoanh_Xem: '174701',
    SoChiPhiSanXuatKinhDoanh_KetXuat: '174707',

    TongHopChiPhiTheoKhoanMucChiPhi_Xem: '174801',
    TongHopChiPhiTheoKhoanMucChiPhi_KetXuat: '174807',

    TheTinhGiaThanh_Xem: '174901',
    TheTinhGiaThanh_KetXuat: '174907',

    SoTheoDoiDoiTuongTHCPTheoTaiKhoan_Xem: '173801',
    SoTheoDoiDoiTuongTHCPTheoTaiKhoan_KetXuat: '173807',

    SoTheoDoiDoiTuongTHCPTheoKhoanMucChiPhi_Xem: '173901',
    SoTheoDoiDoiTuongTHCPTheoKhoanMucChiPhi_KetXuat: '173907',

    SoCongCuDungCu_Xem: '173401',
    SoCongCuDungCu_KetXuat: '173407',

    SoTheoDoiCCDCTaiNoiSD_Xem: '173501',
    SoTheoDoiCCDCTaiNoiSD_KetXuat: '173507',

    SoTheoDoiLaiLoTheoHoaDon_Xem: '174201',
    SoTheoDoiLaiLoTheoHoaDon_KetXuat: '174207',

    SoTheoDoiCongNoPhaiThuTheoHoaDon_Xem: '174301',
    SoTheoDoiCongNoPhaiThuTheoHoaDon_KetXuat: '174307',

    SoChiTietTienVay_Xem: '174501',
    SoChiTietTienVay_KetXuat: '174507',

    // SoTheoDoiChiTietTamUngNhanVien: '1754',
    SoTheoDoiChiTietTamUngNhanVien_Xem: '175401',
    SoTheoDoiChiTietTamUngNhanVien_KetXuat: '175407',

    // SoTongHopThueTNCNTheoNhanVien: '1756',
    SoTongHopThueTNCNTheoNhanVien_Xem: '175601',
    SoTongHopThueTNCNTheoNhanVien_KetXuat: '175607',

    // SoTheoDoiTSCDTaiNoiSuDung: '1771',
    SoTheoDoiTSCDTaiNoiSuDung_Xem: '177101',
    SoTheoDoiTSCDTaiNoiSuDung_KetXuat: '177107',

    // TheTaiSanCoDinh: '1772',
    TheTaiSanCoDinh_Xem: '177201',
    TheTaiSanCoDinh_KetXuat: '177207',

    // SoTaiSanCoDinh: '1773',
    SoTaiSanCoDinh_Xem: '177301',
    SoTaiSanCoDinh_KetXuat: '177307',

    // Đối chiếu công nợ phải thu và chứng từ thanh toán
    DoiChieuCongNoPhaiThu_Xem: '178101',
    DoiChieuCongNoPhaiThu_KetXuat: '178107',

    // Đối chiếu công nợ phải trả và chứng từ thanh toán
    DoiChieuCongNoPhaiTra_Xem: '178001',
    DoiChieuCongNoPhaiTra_KetXuat: '178007',

    // Đối chiếu chi Phí trả trước và Sổ cái
    DoiChieuChiPhiTraTruocVaSoCai_Xem: '178201',
    DoiChieuChiPhiTraTruocVaSoCai_KetXuat: '178207',

    // Đối chiếu Sổ theo dõi CCDC và Sổ cái
    DoiChieuSoTheoDoiCCDCVaSoCai_Xem: '178301',
    DoiChieuSoTheoDoiCCDCVaSoCai_KetXuat: '178307',

    // Đối chiếu Bảng tính giá thành và Giá trị nhập kho
    DoiChieuBangTinhGiaThanhGiaTriNhapKho_Xem: '178501',
    DoiChieuBangTinhGiaThanhGiaTriNhapKho_KetXuat: '178507',

    // Báo cáo đối chiếu số hóa đơn trên chứng từ mua hàng và bảng kê hàng hóa dịch vụ mua vào
    DoiChieuSoHDTrenCTMuaHangVaBangKeHHDVMuaVao_Xem: '180201',
    DoiChieuSoHDTrenCTMuaHangVaBangKeHHDVMuaVao_KetXuat: '180207',

    // Báo cáo tình hình sử dụng hóa đơn
    BaoCaoTinhHinhSuDungHoaDon_Xem: '178601',
    BaoCaoTinhHinhSuDungHoaDon_KetXuat: '178607',

    // Báo cáo đối chiếu chi phí mua giữa chứng từ mua hàng và chứng từ chi phí
    BCDCChiPhiMuaGiuaCTMuaHangVaChiPhi_Xem: '178801',
    BCDCChiPhiMuaGiuaCTMuaHangVaChiPhi_KetXuat: '178807',

    // Tổng hợp nhập xuất kho theo đối tượng THCP
    TongHopNhapXuatKhoTheoDTTHCP_Xem: '179001',
    TongHopNhapXuatKhoTheoDTTHCP_KetXuat: '179007',

    // Báo cáo đối chiếu kho và sổ cái
    DoiChieuKhoVaSoCai_Xem: '179101',
    DoiChieuKhoVaSoCai_KetXuat: '179107',

    // Bảng tính phân bổ CCDC
    BangTinhPhanBoCCDC_Xem: '180901',
    BangTinhPhanBoCCDC_KetXuat: '180907',

    TongHopXuatKhoTheoLenhSanXuat_Xem: '180301',
    TongHopXuatKhoTheoLenhSanXuat_KetXuat: '180307',

    // Báo cáo đối chiếu bảng kê thuế và sổ cái
    BCDCBangKeThueVaSoCai_Xem: '181201',
    BCDCBangKeThueVaSoCai_KetXuat: '181207',

    // Hợp đồng
    HopDong: '12',

    HopDongMua: '1201',
    HopDongMua_Xem: '120101',
    HopDongMua_Them: '120102',
    HopDongMua_Sua: '120103',
    HopDongMua_Xoa: '120104',
    HopDongMua_KetXuat: '120107',
    HopDongMua_Import: '120108',
    HopDongMua_QuanLyMau: '120109',

    HopDongBan: '1202',
    HopDongBan_Xem: '120201',
    HopDongBan_Them: '120202',
    HopDongBan_Sua: '120203',
    HopDongBan_Xoa: '120204',
    HopDongBan_KetXuat: '120207',
    HopDongBan_Import: '120208',
    HopDongBan_QuanLyMau: '120209',

    TongHopCongNoNhanVien: '1746',
    TongHopCongNoNhanVien_Xem: '174601',
    TongHopCongNoNhanVien_KetXuat: '174607',

    SoChiTietDoanhThuTheoNhanVien: '1752',
    SoChiTietDoanhThuTheoNhanVien_Xem: '175201',
    SoChiTietDoanhThuTheoNhanVien_KetXuat: '175207',

    SoTongHopDoanhThuNhanVien: '1753',
    SoTongHopDoanhThuNhanVien_Xem: '175301',
    SoTongHopDoanhThuNhanVien_KetXuat: '175307',

    TinhHinhThucHienHopDongMua_Xem: '175001',
    TinhHinhThucHienHopDongMua_KetXuat: '175007',

    TinhHinhThucHienHopDongBan_Xem: '175101',
    TinhHinhThucHienHopDongBan_KetXuat: '175107',

    SoTongHopLuongNhanVien: '1768',
    SoTongHopLuongNhanVien_Xem: '176801',
    SoTongHopLuongNhanVien_KetXuat: '176807',

    SoTheoDoiCongNoTheoHopDong: '1762',
    SoTheoDoiCongNoTheoHopDong_Xem: '176201',
    SoTheoDoiCongNoTheoHopDong_KetXuat: '176207',

    SoTheoDoiLaiLoTheoHopDong: '1763',
    SoTheoDoiLaiLoTheoHopDong_Xem: '176301',
    SoTheoDoiLaiLoTheoHopDong_KetXuat: '176307',

    TinhHinhThucHienDonDatHang: '1801',
    TinhHinhThucHienDonDatHang_Xem: '180101',
    TinhHinhThucHienDonDatHang_KetXuat: '180107',

    SoTongHopCacKhoanBHTheoNhanVien: '1770',
    SoTongHopCacKhoanBHTheoNhanVien_Xem: '177001',
    SoTongHopCacKhoanBHTheoNhanVien_KetXuat: '177007',

    DoiChieuTaiSanVaSoCai: '1784',
    DoiChieuTaiSanVaSoCai_Xem: '178401',
    DoiChieuTaiSanVaSoCai_KetXuat: '178407',

    BangCanDoiKeToanHN: '1774',
    BangCanDoiKeToanHN_Xem: '177401',
    BangCanDoiKeToanHN_KetXuat: '177407',

    BangCanDoiKeToanGND_DD: '1775',
    BangCanDoiKeToanGND_DD_Xem: '177501',
    BangCanDoiKeToanGND_DD_KetXuat: '177507',

    BangCanDoiKeToanTL: '1776',
    BangCanDoiKeToanTL_Xem: '177601',
    BangCanDoiKeToanTL_KetXuat: '177607',

    KetQuaHoatDongKinhDoanhHN: '1777',
    KetQuaHoatDongKinhDoanhHN_Xem: '177701',
    KetQuaHoatDongKinhDoanhHN_KetXuat: '177407',

    KetQuaHoatDongKinhDoanhGND_DD: '1778',
    KetQuaHoatDongKinhDoanhGND_DD_Xem: '177801',
    KetQuaHoatDongKinhDoanhGND_DD_KetXuat: '177807',

    KetQuaHoatDongKinhDoanh_TL: '1779',
    KetQuaHoatDongKinhDoanh_TL_Xem: '177901',
    KetQuaHoatDongKinhDoanh_TL_KetXuat: '177907',

    NhatKySoCai: '1789',
    NhatKySoCai_Xem: '178901',
    NhatKySoCai_KetXuat: '178907',

    BaoCaoLuuChuyenTienTeTT_HN: '1792',
    BaoCaoLuuChuyenTienTeTT_HN_Xem: '179201',
    BaoCaoLuuChuyenTienTeTT_HN_KetXuat: '179207',

    BaoCaoLuuChuyenTienTeTT_GND: '1793',
    BaoCaoLuuChuyenTienTeTT_GND_Xem: '179301',
    BaoCaoLuuChuyenTienTeTT_GND_KetXuat: '179307',

    BaoCaoLuuChuyenTienTeGT_HN: '1794',
    BaoCaoLuuChuyenTienTeGT_HN_Xem: '179401',
    BaoCaoLuuChuyenTienTeGT_HN_KetXuat: '179407',

    BaoCaoLuuChuyenTienTeGT_GND: '1795',
    BaoCaoLuuChuyenTienTeGT_GND_Xem: '179501',
    BaoCaoLuuChuyenTienTeGT_GND_KetXuat: '179507',

    BaoCaoLuuChuyenTienTe_TL: '1796',
    BaoCaoLuuChuyenTienTe_TL_Xem: '179601',
    BaoCaoLuuChuyenTienTe_TL_KetXuat: '179607',

    BanThuyetMinhBaoCaoTaiChinh_HN: '1797',
    BanThuyetMinhBaoCaoTaiChinh_HN_Xem: '179701',
    BanThuyetMinhBaoCaoTaiChinh_HN_KetXuat: '179707',

    BangKePhieuNhapPhieuXuatTheoDoiTuongTHCP: '1808',
    BangKePhieuNhapPhieuXuatTheoDoiTuongTHCP_Xem: '180801',
    BangKePhieuNhapPhieuXuatTheoDoiTuongTHCP_KetXuat: '180807',

    BanThuyetMinhBaoCaoTaiChinh_TL: '1798',
    BanThuyetMinhBaoCaoTaiChinh_TL_Xem: '179801',
    BanThuyetMinhBaoCaoTaiChinh_TL_KetXuat: '179807',

    BaoCaoSoChiTietCacTaiKhoanTheoDoiTuong: '1807',
    BaoCaoSoChiTietCacTaiKhoanTheoDoiTuong_Xem: '180701',
    BaoCaoSoChiTietCacTaiKhoanTheoDoiTuong_KetXuat: '180707',

    BaoCaoSoCaiChungTuGhiSo: '1787',
    BaoCaoSoCaiChungTuGhiSo_Xem: '178701',
    BaoCaoSoCaiChungTuGhiSo_KetXuat: '178707',

    DoiChieuNhapXuatKhoVoiLapRapThaoDo: '1806',
    DoiChieuNhapXuatKhoVoiLapRapThaoDo_Xem: '180601',
    DoiChieuNhapXuatKhoVoiLapRapThaoDo_KetXuat: '180607'
};
