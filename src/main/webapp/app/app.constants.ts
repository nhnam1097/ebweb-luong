// These constants are injected via webpack environment variables.
// You can add more variables in webpack.common.js or in profile specific webpack.<dev|prod>.js files.
// If you change the values in the webpack config files, you need to re run webpack to update the application

export const VERSION = process.env.VERSION;
export const DEBUG_INFO_ENABLED: boolean = !!process.env.DEBUG_INFO_ENABLED;
export const SERVER_API_URL = process.env.SERVER_API_URL;
export const BUILD_TIMESTAMP = process.env.BUILD_TIMESTAMP;

// role, authorities
export const ROLE_ADMIN = 'ROLE_ADMIN';
export const ROLE_USER = 'ROLE_USER';

// System option
export const TK_LAI = 'TCKHAC_TKCLechLai';
export const TK_LO = 'TCKHAC_TKCLechLo';
export const MAU_BO_GHI_SO = 'TCKHAC_MauCTuChuaGS';
export const SO_LAM_VIEC = 'PHIEN_SoLamViec';
export const TCKHAC_Enter = 'TCKHAC_Enter';
export const TCKHAC_MauCTuChuaGS = 'TCKHAC_MauCTuChuaGS';
export const VTHH_NHAP_DON_GIA_VON = 'VTHH_NhapDonGiaVon';
export const SU_DUNG_DM_KHO = 'TCKHAC_SDDMKho';
export const SU_DUNG_DM_TK_NGan_Hang = 'TCKHAC_SDDMTKNH';
export const SU_DUNG_DM_DOI_TUONG = 'TCKHAC_SDDMDoiTuong';
export const SU_DUNG_DM_VTHH = 'TCKHAC_SDDMVTHH';
export const SU_DUNG_DM_THE_TIN_DUNG = 'TCKHAC_SDDMTheTD';
export const DBDateClosed = 'DBDateClosed';
export const HH_XUATQUASLTON = 'VTHH_XuatQuaSLTon';
export const DBDateClosedOld = 'DBDateClosedOld';
export const CANH_BAO_NHAN_VIEN = 'TCKHAC_CBKoChonNVBH';
export const HMTL_KIEM_PXUAT = 'VTHH_HMTLKiemPXuat';
export const HMTL_KIEM_HOA_DON = 'VTHH_HMTLKiemHD';
export const TangSoChungTu = 'TangSoChungTu';
export const TinhLaiThanhTien = 'VTHH_TinhLaiThanhTien';
export const TCKHAC_MauSoAm = 'TCKHAC_MauSoAm';

/*Hóa đơn điện tử*/
export const EI_Path = 'EI_Path';
export const EI_IDNhaCungCapDichVu = 'EI_IDNhaCungCapDichVu';
export const EI_TenDangNhap = 'EI_TenDangNhap';
export const EI_MatKhau = 'EI_MatKhau';
export const TCKHAC_SDTichHopHDDT = 'TCKHAC_SDTichHopHDDT';
export const CheckHDCD = 'CheckHDCD';
export const SignType = 'SignType';
/*HDDT*/
export const SD_SO_QUAN_TRI = 'TCKHAC_SDSoQuanTri';
export const NHAP_DON_GIA = 'VTHH_NhapDonGiaVon';
export const LAP_KEM_HOA_DON = 'VTHH_HBGGKiemHD';
export const KIEM_PHIEU_NHAP_KHO = 'VTHH_HBTLKiemPNhap';
export const NGAY_HACH_TOAN = 'NgayHachToan';
export const TCKHAC_SDSOQUANTRI = 'TCKHAC_SDSoQuanTri';
export const TCKHAC_HANCHETK = 'TCKHAC_HanCheTK';
export const BH_KIEM_HD = 'VTHH_BHKiemHD';
export const BH_KIEM_PX = 'VTHH_BHKiemPXuat';
export const NHAP_DON_GIA_VON = 'VTHH_NhapDonGiaVon';
export const PP_TINH_GIA_XUAT_KHO = 'VTHH_PPTinhGiaXKho';
export const PP_TINH_TY_GIA_XUAT_QUY = 'TCKHAC_PPTinhGia';
export const CHI_QUA_TON_QUY = 'TCKHAC_ChiQuaTonQuy';
/**
 * Currency directive setting
 */
export const DECIMAL = ',';
export const THOUSANDS = '.';
export const SUFFIX = '';

export const HINH_THUC_HOA_DON_HOA_DON_DIEN_TU = 0;
export const DANH_SACH_HOA_DON = 0;
export const DANH_SACH_HOA_DON_CHO_KY = 1;
export const DANH_SACH_HOA_DON_THAY_THE = 2;
export const DANH_SACH_HOA_DON_DIEU_CHINH = 3;
export const DANH_SACH_HOA_DON_HUY = 4;
export const DANH_SACH_HOA_CHUYEN_DOI = 5;

export const TCKHAC_SDSoQuanTri = 'TCKHAC_SDSoQuanTri'; // check có hiển thị trường vào sổ hay ko
export const TCKHAC_GhiSo = 'TCKHAC_GhiSo'; // check khi lưu có đồng thời ghi sổ hay ko
export const SU_DUNG_SO_QUAN_TRI = 'TCKHAC_SDSoQuanTri';

/**
 * Constant phần dấu thập phân
 */
export const DDSo_DonGia = 'DDSo_DonGia'; // Đơn giá - 1
export const DDSo_DonGiaNT = 'DDSo_DonGiaNT'; // Đơn giá ngoại tệ - 2
export const DDSo_SoLuong = 'DDSo_SoLuong'; // Số lượng - 3
export const DDSo_TyGia = 'DDSo_TyGia'; // Tỷ giá - 4
export const DDSo_TyLe = 'DDSo_TyLe'; // Hệ số, tỷ lệ - 5
export const DDSo_TyLePBo = 'DDSo_TyLePBo'; // Tỷ lệ phân bổ - 6
export const DDSo_TienVND = 'DDSo_TienVND'; // Tiền Việt Nam Đồng - 7
export const DDSo_NgoaiTe = 'DDSo_NgoaiTe'; // Tiền ngoại tệ - 8
export const SO_NGUYEN = 9; // Chỉ nhập số nguyên - 9
export const DDSo_SoAm = 'DDSo_SoAm'; // Hiển thị số âm
export const DDSo_DocTienLe = 'DDSo_DocTienLe'; // Cách đọc số tiền lẻ
export const DDSo_NCachHangNghin = 'DDSo_NCachHangNghin'; // Ngăn cách hàng nghìn
export const DDSo_NCachHangDVi = 'DDSo_NCachHangDVi'; // Ngăn cách hàng đơn vị
export const CentimeterToPixel = 37.795275591; // Quy đổi 1cm sang pixel
export const PicaPointToPixel = 1.3281472327365; // Quy đổi 1pt sang pixel
export const StandardPercent = 185; // Tỉ lệ scale khi in báo cáo
export const MinStandardPercent = 120; // Tỉ lệ scale khi in báo cáo
export const percent = 1.2; // Tỉ lệ scale khi in báo cáo
export const percentLandScape = 1.215; // Tỉ lệ scale khi in báo cáo
export const percentPortrait = 1.265; // Tỉ lệ scale khi in báo cáo
export const rowsOfPerPage = 30;

export enum Enum {}

export enum AccountType {
    TK_DON_GIA = 'CostAccount',
    TK_KHO = 'RepositoryAccount',
    TK_CHIET_KHAU = 'DiscountAccount',
    TK_NO = 'DebitAccount',
    TK_CO = 'CreditAccount',
    TK_THUE_GTGT = 'VATAccount',
    TKDU_THUE_GTGT = 'DeductionDebitAccount',
    TK_THUE_NK = 'ImportTaxAccount',
    TK_THUE_TTDB = 'SpecialConsumeTaxAccount',
    TK_THUE_XK = 'ExportTaxAccount',
    TK_DU_THUE_XK = 'ExportTaxAccountCorresponding'
}

export enum CategoryName {
    DOI_TUONG = 'AccountingObjects', // Combobox đối tượng
    NHAN_VIEN = 'Employees', // Combobox nhân viên
    TAI_KHOAN_NGAN_HANG = 'BankAccountDetails', // Combobox tài khoản ngân hàng
    KHO = 'Repositories', // Combobox kho
    DON_VI_TINH = 'Unit', // Combobox đơn vị tính
    MA_THONG_KE = 'StatisticsCode',
    PHONG_BAN_DOI_TUONG_TAP_HOP_CHI_PHI = 'PHONG_BAN_DOI_TUONG_TAP_HOP_CHI_PHI',
    KHOAN_MUC_CHI_PHI = 'ExpenseItem',
    HOP_DONG = 'EMContracts',
    MUC_THU_CHI = 'BudgetItems',
    KHACH_HANG = 'Customer',
    NHA_CUNG_CAP = 'Supplier',
    KHAC = 'Customer/Supplier',
    THE_TIN_DUNG = 'CreditCard',
    NHOM_HHDV_CHIU_THUE_TTĐB = 'MaterialGoodsSpecialTaxGroup',
    LOAI_VAT_TU_HANG_HOA = 'MaterialGoodsCategory',
    DINH_KHOAN_TU_DONG = 'autoPrinciple',
    NGAN_HANG = 'bank',
    LOAI_TIEN = 'currency',
    VAT_TU_HANG_HOA = 'MaterialGoods',
    VAT_TU_HANG_HOA_PP_SERVICE = 'MaterialGoods_PPService',
    DOI_TUONG_TAP_HOP_CHI_PHI = 'CostSet',
    PHONG_BAN = 'OrganizationUnit',
    CBB_CCTC = 'CbbCCTC',
    DINH_MUC_NVL = 'MaterialQuantum',
    PHUONG_THUC_VAN_CHUYEN = 'TransportMethod',
    LOAI_TSCD = 'FixedAssetCategory',
    TAI_SAN_CO_DINH = 'FixedAsset',
    CONG_CU_DUNG_CU = 'Tools',
    BIEU_THUE_TAI_NGUYEN = 'MaterialGoodsResourceTaxGroup',
    NHOM_GIA_BAN = 'SalePriceGroup',
    LENH_SAN_XUAT = 'RSProductionOrder'
}

export enum UnitTypeByOrganizationUnit {
    TONG_CONG_TY = 0,
    CHI_NHANH = 1,
    VAN_PHONG_DAI_DIEN = 2,
    DIA_DIEM_KINH_DOANH = 3,
    PHONG_BAN = 4,
    KHAC = 5
}

export enum PPDanhGiaDoDang {
    SP_HOAN_THANH_TUONG_DUONG = 0,
    NVLTT = 1,
    DINH_MUC = 2
}

export enum IDConfigTab {
    HANG_TIEN = 0,
    CHI_TIET = 1,
    THUE = 2,
    THONG_TIN = 3,
    THONG_KE = 4,
    PHAN_BO = 5,
    HACH_TOAN = 6,
    DANH_SACH_CHUNG_TU_GOC = 7,
    THONG_TIN_BO_SUNG = 8,
    DINH_MUC_NVL = 9
}

export enum TieuThucPhanBo {
    NVLTT = 0,
    NHAN_CONG_TT = 1,
    CHI_PHI_TT = 2,
    DINH_MUC = 3,
    DOANH_THU = 3
}

export enum ObjectTypeByCPOPN {
    SAN_PHAM = '0',
    PHAN_XUONG_PHONG_BAN = '1',
    CONG_TRINH_VU_VIEC = '2',
    DON_HANG = '3',
    HOP_DONG = '4'
}

export enum EINVOICE {
    HOA_DON_MOI_TAO_LAP = 0,
    HOA_DON_CO_CHU_KY_SO = 1,
    HOA_DON_BI_THAY_THE = 3,
    HOA_DON_BI_DIEU_CHINH = 4,
    HOA_DON_HUY = 5,
    HOA_DON_MOI_TAO_LAP_THAY_THE = 7,
    HOA_DON_MOI_TAO_LAP_DIEU_CHINH = 8,
    TYPE_HANG_MUA_TRA_LAI = 220,
    TYPE_BAN_HANG_CHUA_THU_TIEN = 320,
    TYPE_BAN_HANG_THU_TIEN_NGAY_TM = 321,
    TYPE_BAN_HANG_THU_TIEN_NGAY_CK = 322,
    TYPE_HANG_BAN_TRA_LAI = 330
}

export enum TypeID {
    MUA_HANG = 210,
    HANG_BAN_TRA_LAI = 330,
    HANG_GIAM_GIA = 340,
    XUAT_HOA_DON = 350,
    KHAI_BAO_TSCD_DAU_KY = 600,
    HOA_DON_DAU_VAO = 250,
    XUAT_HOA_DON_BH = 351,
    XUAT_HOA_DON_HBGG = 353,
    BAN_HANG_CHUA_THU_TIEN = 320,
    BAN_HANG_THU_TIEN_NGAY_TM = 321,
    BAN_HANG_THU_TIEN_NGAY_CK = 322,
    BANG_KE_HANG_HOA_DICH_VU = 323,
    PHIEU_THU_TU_BAN_HANG = 102,
    NOP_TIEN_TU_BAN_HANG = 162,
    NHAP_KHO = 400,
    NHAP_KHO_TU_DIEU_CHINH = 401,
    NHAP_KHO_TU_MUA_HANG = 402,
    NHAP_KHO_TU_BAN_HANG_TRA_LAI = 403,
    XUAT_KHO = 410,
    XUAT_KHO_TU_BAN_HANG = 411,
    XUAT_KHO_TU_MUA_HANG = 412,
    XUAT_KHO_TU_DIEU_CHINH = 413,
    CHUNG_TU_GHI_SO = 6868,
    MUA_HANG_TRA_LAI = 220,
    MUA_HANG_GIAM_GIA = 230,
    PHIEU_THU_TIEN_KHACH_HANG = 101,
    NOP_TIEN_TU_KHACH_HANG = 161,
    PHAN_BO_CHI_PHI_TRA_TRUOC = 709,
    DANH_GIA_LAI_TK_NGOAI_TE = 707,
    XU_LY_CHENH_LECH_TU_TY_GIA_XUAT_QUY = 706,
    CHUNG_TU_NGHIEP_VU_KHAC = 700,
    XU_LY_CHENH_LECH_TY_GIA = 7011,
    KET_CHUYEN_LAI_LO = 702,
    DANG_KY_SU_DUNG_HOA_DON = 810,
    Thong_Bao_Phat_Hanh_Hoa_Don = 820,
    HUY_HOA_DON = 830,
    BAO_MAT_CHAY_HONG_HOA_DON = 840,
    XOA_HOA_DON = 850,
    PHIEU_THU = 100,
    PHIEU_CHI = 110,
    PHIEU_CHI_NHA_CUNG_CAP = 116,
    PHIEU_CHI_MUA_DICH_VU = 114,
    PHIEU_CHI_MUA_QUA_KHO_VA_KHONG_QUA_KHO = 115,
    UY_NHIEM_CHI = 120,
    SEC_CHUYEN_KHOAN = 130,
    SEC_TIEN_MAT = 140,
    UY_NHIEM_CHI_MUA_HANG = 125,
    SEC_CHUYEN_KHOAN_MUA_HANG = 135,
    SEC_TIEN_MAT_MUA_HANG = 145,
    UY_NHIEM_CHI_MUA_DV = 124,
    SEC_CHUYEN_KHOAN_MUA_DV = 134,
    SEC_TIEN_MAT_MUA_DV = 144,
    CHUYEN_TIEN_NOI_BO = 150,
    NOP_TIEN_TK = 160,
    THE_TIN_DUNG = 170,
    BAO_GIA = 300,
    DON_DAT_HANG = 310,
    CHUYEN_KHO = 420,
    CHAM_CONG = 760,
    CHUYEN_KHO_KIEM_VAN_CHUYEN = 420,
    CHUYEN_KHO_GUI_DAI_LY = 421,
    CHUYEN_KHO_NOI_BO = 422,
    HOP_DONG_BAN = 730,
    BANG_LUONG = 770,
    HOP_DONG_MUA = 720,
    MUA_DICH_VU = 240,
    OPN_DOI_TUONG = 742,
    DON_MUA_HANG = 200,
    TM01GTGT = 900,
    LENH_LAP_RAP = 430,
    LENH_THAO_DO = 431,

    GHI_GIAM_CCDC = 530,
    DIEU_CHINH_CCDC = 550,
    DIEU_CHUYEN_CCDC = 540,

    PHAN_BO_CCDC = 560,
    KIEM_KE_CCDC = 570,
    GHI_TANG_CCDC = 520,

    Khai_bao = 600,

    LENH_SAN_XUAT = 440,

    // tự đặt không có trong database
    CHI_PHI_TRA_TRUOC = 1111,
    TSCD_GHI_TANG = 620,
    DINH_MUC_GIA_THANH = 1112,
    TYPE_CHUNG_TU_GHI_SO = 710,
    TSCD_TINH_KHAU_HAO = 660,
    TSCD_GHI_GIAM = 630,
    TSCD_DIEU_CHINH = 650,
    TSCD_DIEU_CHUYEN = 640,
    TSCD_KIEM_KE = 670,
    CHI_PHI_DO_DANG_DAU_KY = 7099,
    DINH_MUC_PHAN_BO_CP = 7098,
    KET_QUA_TINH_GIA_THANH = 7116,
    KET_QUA_TINH_GIA_THANH_HESO = 7117,
    KET_QUA_TINH_GIA_THANH_TYLE = 7118,
    TO_KHAI_THUE_GTGT_CHO_DU_AN_DAU_TU = 920,
    DINH_MUC_NGUYEN_VAT_LIEU = 13245,
    THUE_TAI_NGUYEN = 6090,

    // Kho
    LENH_LAPRAP_THAODO = 7000,
    CAP_NHAT_GIA_NHAP_KHO_THANH_PHAM = 3333
}

export enum EbPackage {
    CO_PACKAGE = 1,
    HET_PACKAGE = 0,
    NULL_PACKAGE = 2
}

export enum statusOfUser {
    CHUA_DUNG = 0,
    DANG_DUNG = 1,
    HET_HAN = 2,
    TAM_DUNG = 3
}

export enum GROUP_TYPEID {
    GROUP_SAReturn = 33,
    GROUP_RSOUTWARD = 41,
    GROUP_RSINWARD = 40,
    GROUP_RSTRANFER = 42,
    GROUP_MCRECEIPT = 10,
    GROUP_MBDEPOSIT = 16,
    GROUP_SAQUOTE = 30,
    GROUP_SAORDER = 31,
    GROUP_SARETURN = 34,
    GROUP_SAINVOICE = 32,
    GROUP_SASABILL = 35,
    GROUP_PPBILL = 25,
    GROUP_PPINVOICE = 21,
    GROUP_PPDISCOUNTRETURN = 22,
    GROUP_PPDISCOUNTPURCHASE = 23,
    GROUP_GOTHERVOUCHER = 70,
    GROUP_PPSERVICE = 24,
    GROUP_PPORDER = 20,
    GROUP_TSCD_GHI_TANG = 62,
    GROUP_TSCD_TINH_KHAU_HAO = 66,
    GROUP_TSCD_GHI_GIAM = 63,
    GROUP_TSCD_DIEU_CHINH = 65,
    GROUP_TSCD_DIEU_CHUYEN = 64,
    GROUP_TSCD_KIEM_KE = 67,
    GROUP_HD_BAN = 73,
    GROUP_HD_MUA = 72,
    GROUP_CT_GHI_SO = 71,
    GROUP_LAP_RAP_THAO_DO = 43,
    GROUP_LENH_SAN_XUAT = 44
}

export const PPINVOICE_TYPE = {
    GROUP_CHUNG_TU_MUA_HANG: 21,
    GROUP_PHIEU_NHAP_KHO: 40,
    GROUP_PHIEU_CHI: 11,
    GROUP_UY_NHIEM_CHI: 12,
    GROUP_SEC_CHUYEN_KHOAN: 13,
    GROUP_SEC_TIEN_MAT: 14,
    GROUP_THE_TIN_DUNG: 17,

    TYPE_ID_CHUA_THANH_TOAN: 210,
    TYPE_ID_TIEN_MAT: 211,
    TYPE_ID_UY_NHIEM_CHI: 212,
    TYPE_ID_SEC_CK: 213,
    TYPE_ID_SEC_TIEN_MAT: 215,
    TYPE_ID_THE_TIN_DUNG: 214,

    TYPE_REPORT_NHAP_KHO: 1,
    TYPE_REPORT_NHAP_KHO_A5: 6,
    TYPE_REPORT_CHUNG_TU: 2,
    TYPE_REPORT_CHUNG_TU_QD: 3,
    TYPE_REPORT_BAO_NO: 4,
    TYPE_REPORT_PHIEU_CHI: 5,
    TYPE_REPORT_PHIEU_CHI_A5: 7,
    TYPE_REPORT_PHIEU_CHI_2: 8,
    TYPE_REPORT_NHAP_KHO_SO_LO_HAN_DUNG: 6996
};

export const RSOUTWARD_TYPE = {
    TYPE_REPORT_XUAT_KHO: 9,
    TYPE_REPORT_XUAT_KHO_A5: 14,
    TYPE_REPORT_XUAT_KHO_SO_LO_HAN_DUNG: 9669,
    TYPE_REPORT_ChungTuKeToan: 1,
    TYPE_REPORT_ChungTuKeToanQuyDoi: 3,
    TYPE_REPORT_HOA_DON_HONG: 840,
    NHAP_KHO_QUY_CACH: 1215,
    HHD: 123456
};

export const IADestructionInvoice_TYPE = {
    HHD: 123456
};

export const IALOSTINVOICE_TYPE = {
    TYPE_REPORT_HOA_DON_HONG: 840
};

export const FAADJUSTMENT_TYPE = {
    TYPE_REPORT_ChungTuKeToanTSCDDC: 117,
    TYPE_REPORT_DieuChinhTSCD: 119,
    TYPE_REPORT_BienBanDanhGiaLaiTSCD: 118
};

export const TM01GTGT_TYPE = {
    TYPE_REPORT_BangKeMuaVao: 903,
    TYPE_REPORT_GiaiTrinhKeKhai: 904,
    TYPE_REPORT_ToKhaiGTGTKhauTruMau01GTGT: 901,
    TYPE_REPORT_BangKeBanRa: 902
};

export const RSTRANSFER_TYPE = {
    TYPE_REPORT_XUAT_KHO: 2,
    TYPE_REPORT_XUAT_KHO_A5: 3,
    TYPE_REPORT_ChungTuKeToan: 1,
    TYPE_REPORT_PhieuNhapKho: 4,
    TYPE_REPORT_PhieuNhapKhoA5: 5,
    TYPE_REPORT_PhieuNhapKhoQuyCach: 6,
    TYPE_REPORT_PhieuXuatKhoQuyCach: 7,
    TYPE_REPORT_XUAT_KHO_SO_LO_HAN_DUNG: 9669,
    TYPE_REPORT_PhieuNhapKhoSoLoHanDung: 6996
};

export const PPINVOICE_COMPONENT_TYPE = {
    MUA_HANG_QUA_KHO: 1,
    MUA_HANG_KHONG_QUA_KHO: 2,
    REF_QUA_KHO: 3,
    REF_KHONG_QUA_KHO: 4
};

export const MATERIALQUANTUM_TYPE = {
    TYPE_REPORT_MATERIAL_QUANTUM: 1
};

export const ACCOUNT_DETAIL_TYPE = {
    ACCOUNT_DEBIT: '6',
    ACCOUNT_SUPPLIER: '0',
    ACCOUNT_CUSTOMER: '1',
    ACCOUNT_EMPLOYEE: '2',
    ACCOUNT_BANK: '5'
};

export const MATERIAL_GOODS_TYPE = {
    SERVICE: 2,
    DIFF: 4
};

export const CURRENCY_ID = 'VND';

export const ACCOUNTING_TYPE_ID = {
    NORMAL_ACCOUNT: 740,
    MATERIAL_GOODS_TYPE: 741,
    ACCOUNTING_OBJECT: 742
};

export const ACCOUNTING_TYPE = {
    MT: 'VTHH',
    AO: 'DoiTuong',
    NH: 'NganHang'
};

export const EINVOICE_STATUS = {
    SUCCESS: 2,
    UNSUSSCESS: 1
};

export const INVOICE_FORM = {
    E: 'E',
    P: 'P',
    T: 'T',
    HD_DIEN_TU: 0,
    HD_DAT_IN: 1,
    HD_TU_IN: 2
};

export const KET_CHUYEN = {
    ACCOUNT_SPECIAL: '413',
    NO: 0,
    CO: 1,
    NO_CO: 2,
    CO_NO: 3,
    ACCOUNT_911: '911'
};

export const FORMULA = {
    DIVIDE: '/',
    MULTIPLY: '*'
};

export enum EXPENSE_ITEM {
    NGUYEN_VAT_LIEU_TRUC_TIEP = 0,
    NHAN_CONG_TRUC_TIEP = 1,
    CHI_PHI_NHAN_CONG = 2,
    CHI_PHI_NGUYEN_VAT_LIEU = 3,
    CHI_PHI_DUNG_CU_SAN_XUAT = 4,
    CHI_PHI_KHAU_HAO_MAY = 5,
    CHI_PHI_DICH_VU_MUA_NGOAI = 6,
    CHI_PHI_BANG_TIEN_KHAC = 7,
    PHI_PHI_NHAN_VIEN_PHAN_XUONG = 8,
    CHI_PHI_NGUYEN_VAT_LIEU_CHUNG = 9,
    CHI_PHI_DUNG_CU_SAN_XUAT_CHUNG = 10,
    CHI_PHI_KHAU_HAO_MAY_CHUNG = 11,
    CHI_PHI_DICH_VU_MUA_NGOAI_CHUNG,
    CHI_PHI_BANG_TIEN_MAT_KHAC,
    GIA_TRI_TRONG
}

export const CURRENCY = {
    VND: 'VND'
};

export const ClOSE_BOOK = {
    RECORD: 1,
    CHANG_POSTEDDATE: 2,
    DELETE: 3
};

export const CALCULATE_OW = {
    BINH_QUAN_CUOI_KY: 'Bình quân cuối kỳ',
    BINH_QUAN_TUC_THOI: 'Bình quân tức thời',
    NHAP_TRUOC_XUAT_TRUOC: 'Nhập trước xuất trước',
    DICH_DANH: 'Đích danh',
    GIA_TRI: 'Giá trị',
    BINH_QUAN_CUOI_KY_CODE: '0',
    BINH_QUAN_TUC_THOI_CODE: '1',
    NHAP_TRUOC_XUAT_TRUOC_CODE: '2',
    DICH_DANH_CODE: '3',
    GIA_TRI_CODE: '4'
};

export const REPORT = {
    ChungTuKeToan: 1,
    ChungTuKeToanChuyenTien: 153,
    GiayBaoCoChuyenTienNoiBo: 151,
    GiayBaoNoChuyenTienNoiBo: 152,
    ChungTuKeToanCCDC: 112,
    ChungTuPhanBoCCDC: 113,
    BienBanKiemKeCCDC: 111,
    BieuMau: 2,
    ChungTuKeToanQuyDoi: 3,
    PhieuChi: 4,
    PhieuChiTT2Lien: 41,
    PhieuChiA5: 42,
    GiayBaoNo: 5,
    PhieuNhapKho: 6,
    PhieuNhapKhoSoLoHanDung: 6996,
    PhieuXuatKhoSoLoHanDung: 9669,
    PhieuThu: 7,
    PhieuThuTT2Lien: 71,
    PhieuThuA5: 72,
    GiayBaoCo: 8,
    HoadonGTGT: 10,
    HoadonBanHang: 11,
    PhieuXuatKho: 9,
    PhieuXuatKhoA5: 14,
    PhieuNhapKhoA5: 12,
    PhieuNhapKhoQuyCach: 1214,
    BangKeHangHoaDichVu: 13,
    GhiGiamCCDC: 973,
    DieuChinhCCDC: 974,
    DieuChuyenCCDC: 975,
    ChungTuKeToanTSCD: 1,
    ChungTuGhiGiamTSCD: 2,
    ChungTuDieuChuyenTSCD: 1,
    ChiPhiDoDangDauKy: 703,
    Mau031ATNDN: 304,
    Mau03TNDN: 403,
    Mau032ATNDN: 305,
    BienBanTinhKhauHaoTSCD: 115,
    BienBanKiemKeTSCD: 114
};

export const NameReport = {
    Luong: {
        SO_TONG_HOP_CAC_KHOAN_BH_THEO_NHAN_VIEN: 'SỔ TỔNG HỢP CÁC KHOẢN BẢO HIỂM THEO NHÂN VIÊN',
        SO_THEO_DOI_CHI_TIET_TAM_UNG_NHAN_VIEN: 'SỔ THEO DÕI CHI TIẾT TẠM ỨNG NHÂN VIÊN',
        SO_TONG_HOP_THUE_TNCN_THEO_NHAN_VIEN: 'SỔ TỔNG HỢP THẾ TNCN THEO NHÂN VIÊN',
        SO_TONG_HOP_TAM_UNG_THEO_NHAN_VIEN: 'SỔ TỔNG HỢP TẠM ỨNG THEO NHÂN VIÊN'
    },
    Ngan_Hang: {
        BANG_KE_SO_DU_NGAN_HANG: 'BẢNG KÊ SỐ DƯ NGÂN HÀNG',
        SO_TIEN_GUI_NGAN_HANG: 'SỔ TIỀN GỬI NGÂN HÀNG'
    },

    Quy: {
        SO_QUY_TIEN_MAT: 'SỔ QUỸ TIỀN MẶT'
    },
    TongHop: {
        SO_CHI_TIET_THANH_TOAN_VOI_NGUOI_MUA_NGUOI_BAN: 'SỔ CHI TIẾT THANH TOÁN NGƯỜI MUA (NGƯỜI BÁN)',
        SO_CHI_TIET_THANH_TOAN_VOI_NGUOI_MUA_NGUOI_BAN_BANG_NGOAI_TE: 'SỔ CHI TIẾT THANH TOÁN NGƯỜI MUA( NGƯỜI BÁN ) BẰNG NGOẠI TỆ',
        SO_TONG_HOP_CONG_NO_NHAN_VIEN: 'SỔ TỔNG HỢP CÔNG NỢ NHÂN VIÊN',
        SO_CHI_TIET_CONG_NO_NHAN_VIEN: 'SỔ CHI TIẾT CÔNG NỢ NHÂN VIÊN',
        NHAT_KY_SO_CAI: 'NHẬT KÝ - SỔ CÁI',
        SO_THEO_DOI_THANH_TOAN_BANG_NGOAI_TE: 'SỔ THEO DÕI THANH TOÁN BẰNG NGOẠI TỆ',
        SO_CHI_TIET_TIEN_VAY: 'SỔ CHI TIẾT TIỀN VAY',
        SO_NHAT_KY_CHUNG: 'SỔ NHẬT KÝ CHUNG',
        BANG_KE_CHUNG_TU_CHUA_LAP_CHUNG_TU_GHI_SO: 'BẢNG KÊ CHỨNG TỪ CHƯA LẬP CHỨNG TỪ GHI SỔ'
    },
    GiaThanh: {
        TONG_HOP_CHI_PHI_THEO_KHOAN_MUC_CHI_PHI: 'TỔNG HỢP CHI PHÍ THEO KHOẢN MỤC CHI PHÍ',
        SO_THEO_DOI_MA_THONG_KE_THEO_TAI_KHOAN: 'SỔ THEO DÕI MÃ THỐNG KÊ THEO TÀI KHOẢN',
        SO_THEO_DOI_MA_THONG_KE_THEO_KHOAN_MUC_CHI_PHI: 'SỔ THEO DÕI MÃ THỐNG KÊ THEO KHOẢN MỤC CHI PHÍ',
        TONG_HOP_NHAP_XUAT_KHO_THEO_DTTHCP: 'TỔNG HỢP NHẬP XUẤT KHO THEO ĐỐI TƯỢNG TẬP HỢP CHI PHÍ',
        SO_THEO_DOI_DOI_TUONG_THCP_THEO_TAI_KHOAN: 'SỔ THEO DÕI ĐỐI TƯỢNG TẬP HỢP CHI PHÍ (THEO TÀI KHOẢN)',
        SO_THEO_DOI_DOI_TUONG_THCP_THEO_KHOAN_MUC_CHI_PHI: 'SỔ THEO DÕI ĐỐI TƯỢNG TẬP HỢP CHI PHÍ (THEO KHOẢN MỤC CHI PHÍ)'
    },
    TSCD: {
        THE_TAI_SAN_CO_DINH: 'THẺ TÀI SẢN CỐ ĐỊNH',
        SO_THEO_DOI_TAI_SAN_CO_DINH_TAI_NOI_SU_DUNG: 'SỔ THEO DÕI TÀI SẢN CỐ ĐỊNH TẠI NƠI SỬ DỤNG',
        SO_TAI_SAN_CO_DINH: 'SỔ TÀI SẢN CỐ ĐỊNH'
    },
    BaoCaoDoiChieu: {
        BCDC_CHI_PHI_MUA_GIUA_CT_MUA_HANG_VA_CHI_PHI: 'BÁO CÁO ĐỐI CHIẾU CHI PHÍ MUA TRÊN CHỨNG TỪ MUA HÀNG VÀ CHỨNG TỪ CHI PHÍ',
        DOI_CHIEU_CONG_NO_PHAI_TRA: 'ĐỐI CHIẾU  CHỨNG TỪ CÔNG NỢ PHẢI TRẢ VÀ CHỨNG TỪ THANH TOÁN',
        DOI_CHIEU_CONG_NO_PHAI_THU: 'ĐỐI CHIẾU CHỨNG TỪ CÔNG NỢ PHẢI THU VÀ CHỨNG TỪ THANH TOÁN'
    },
    Kho: {
        BAO_CAO_TIEN_DO_SAN_XUAT: 'BÁO CÁO TIẾN ĐỘ SẢN XUẤT',
        SO_CHI_TIET_VAT_LIEU: 'SỔ CHI TIẾT VẬT LIỆU',
        THE_KHO: 'THẺ KHO (SỔ KHO)',
        BANG_TONG_HOP_CHI_TIET_VAT_LIEU: 'BẢNG TỔNG HỢP CHI TIẾT VẬT LIỆU',
        TONG_HOP_TON_KHO_TREN_NHIEU_KHO: 'TỔNG HỢP TỒN KHO TRÊN NHIỀU KHO'
    },
    HopDong: {
        TINH_HINH_THUC_HIEN_HOP_DONG_MUA: 'TÌNH HÌNH THỰC HIỆN HĐ MUA',
        TINH_HINH_THUC_HIEN_HOP_DONG_BAN: 'TÌNH HÌNH THỰC HIỆN HĐ BÁN'
    },
    BanHang: {
        TINH_HINH_THUC_HIEN_DON_DAT_HANG: 'TÌNH HÌNH THỰC HIỆN ĐƠN ĐẶT HÀNG',
        SO_THEO_DOI_CONG_NO_PHAI_THU_THEO_MAT_HANG: 'SỔ THEO DÕI CÔNG NỢ PHẢI THU THEO MẶT HÀNG',
        SO_THEO_DOI_LAI_LO_THEO_HOA_DON: 'SỔ THEO DÕI LÃI LỖ THEO HÓA ĐƠN',
        SO_THEO_DOI_CONG_NO_PHAI_THU_THEO_HOA_DON: 'SỔ THEO DÕI CÔNG NỢ PHẢI THU THEO HÓA ĐƠN'
    },
    Thue: {
        BANG_KE_HHDV_MUA_VAO: 'BẢNG KÊ HĐ, CHỨNG TỪ HHDV MUA VÀO',
        BANG_KE_HHDV_MUA_VAO_MAU_QUAN_TRI: 'BẢNG KÊ HĐ, CHỨNG TỪ HHDV MUA VÀO (MẪU QUẢN TRỊ)',
        TONG_HOP_CAC_KHOAN_PHAI_NOP_NGAN_SACH_NHA_NUOC: 'TỔNG HỢP CÁC KHOẢN PHẢI NỘP NGÂN SÁCH NHÀ NƯỚC'
    }
};

export const BaoCaoDong = {
    TYPE_TONG_HOP: 0,
    SO_QUY_TIEN_MAT: 7,
    SO_KE_TOAN_CHI_TIET_QUY_TIEN_MAT: 8,
    SO_TIEN_GUI_NGAN_HANG: 9,
    SO_THEO_DOI_LAI_LO_THEO_HOA_DON: 19,
    SO_THEO_DOI_CONG_NO_PHAI_THU_THEO_HOA_DON: 20,
    SO_THEO_DOI_LAI_LO_THEO_MAT_HANG: 21,
    SO_CHI_TIET_DOANH_THU_THEO_NHAN_VIEN: 25,
    SO_TONG_HOP_CAC_KHOAN_BH_THEO_NHAN_VIEN: 66,
    SO_TONG_HOP_DOANH_THU_THEO_NHAN_VIEN: 26,
    SO_TONG_HOP_CONG_NO_NHAN_VIEN: 44,
    SO_TONG_HOP_LUONG_NHAN_VIEN: 65,
    SO_CHI_TIET_MUA_HANG: 11,
    TONG_HOP_CHI_PHI_THEO_KHOAN_MUC_CHI_PHI: 55,
    SO_THEO_DOI_MA_THONG_KE_THEO_TAI_KHOAN: 58,
    SO_THEO_DOI_MA_THONG_KE_THEO_KHOAN_MUC_CHI_PHI: 59,
    SO_THEO_DOI_TAI_SAN_CO_DINH_TAI_NOI_SU_DUNG: 64,
    BCDC_CHI_PHI_MUA_GIUA_CT_MUA_HANG_VA_CHI_PHI: 72,
    THE_TAI_SAN_CO_DINH: 86,
    BAO_CAO_TINH_HINH_SU_DUNG_HOA_DON: 93,
    BANG_KE_PHIEU_NHAP_PHIEU_XUAT_THEO_DOI_TUONG_THCP: 91,
    BCDC_BANG_KE_THUE_VA_SO_CAI: 96,
    BANG_KE_PHIEU_NHAP_PHIEU_XUAT_THEO_HOP_DONG: 100,
    BANG_KE_HANG_HOA_BAN_RA: 50,
    BANG_KE_HANG_HOA_BAN_RA_QT: 51,
    TONG_HOP_XUAT_KHO_THEO_LENH_SAN_XUAT: 97,
    BAO_CAO_TIEN_DO_SAN_XUAT: 82,
    SO_THEO_DOI_CHI_TIET_TAM_UNG_NHAN_VIEN: 67,
    SO_TONG_HOP_THUE_TNCN_THEO_NHAN_VIEN: 68,
    SO_TONG_HOP_TAM_UNG_THEO_NHAN_VIEN: 69,
    TINH_HINH_THUC_HIEN_HOP_DONG_MUA: 70,
    TINH_HINH_THUC_HIEN_HOP_DONG_BAN: 71,
    NHAT_KY_SO_CAI: 94,
    TINH_HINH_THUC_HIEN_DON_DAT_HANG: 90,
    TONG_HOP_TON_KHO_TREN_NHIEU_KHO: 98,
    BANG_KE_SO_DU_NGAN_HANG: 10,
    SO_DANG_KY_CHUNG_TU_GHI_SO: 46,
    SO_THEO_DOI_LAI_LO_THEO_HOP_DONG: 23,
    SO_THEO_DOI_CONG_NO_THEO_HOP_DONG: 24,
    TONG_HOP_CONG_NO_PHAI_TRA: 14,
    BANG_KE_HHDV_MUA_VAO: 48,
    BANG_KE_HHDV_MUA_VAO_MAU_QUAN_TRI: 49,
    SO_CHI_TIET_BAN_HANG: 15,
    SO_NHAT_KY_BAN_HANG: 16,
    SO_NHAT_KY_MUA_HANG: 12,
    SO_CHI_TIET_THANH_TOAN_VOI_NGUOI_MUA_NGUOI_BAN: 40,
    SO_CHI_TIET_THANH_TOAN_VOI_NGUOI_MUA_NGUOI_BAN_BANG_NGOAI_TE: 41,
    TONG_HOP_CAC_KHOAN_PHAI_NOP_NGAN_SACH_NHA_NUOC: 52,
    SO_THEO_DOI_THANH_TOAN_BANG_NGOAI_TE: 42,
    SO_CHI_TIET_TIEN_VAY: 43,
    SO_CHI_TIET_TIEN_CONG_NO_PHAI_TRA_THEO_HOA_DON: 101,
    SO_THEO_DOI_DOI_TUONG_THCP_THEO_TAI_KHOAN: 53,
    SO_THEO_DOI_DOI_TUONG_THCP_THEO_KHOAN_MUC_CHI_PHI: 56,
    TONG_HOP_NHAP_XUAT_KHO_THEO_DOI_TUONG_THCP: 88,
    TONG_HOP_NHAP_XUAT_KHO_THEO_HOP_DONG: 99,
    DOI_CHIEU_CONG_NO_PHAI_TRA: 73,
    DOI_CHIEU_CONG_NO_PHAI_THU: 74,
    SO_CHI_TIET_VAT_LIEU: 27,
    TONG_HOP_TON_KHO: 30,
    SO_CHI_TIET_CAC_TAI_KHOAN: 38,
    PHAN_BO_CHI_PHI_TRA_TRUOC: 39,
    SO_CCDC: 60,
    SO_THEO_DOI_CCDC_TAI_NOI_SU_DUNG: 61,
    BANG_TINH_PHAN_BO_CCDC: 85,
    DOI_CHIEU_SO_THEO_DOI_CCDC_VA_SO_CAI: 78,
    DOI_CHIEU_CHI_PHI_TRA_TRUOC_VA_SO_CAI: 79,
    THE_KHO: 29,
    DOI_CHIEU_SO_HD_CHUNG_TU_MUA_HANG_VA_BANG_KE_HHDV_MUA_VAO: 81,
    SO_NHAT_KY_CHUNG: 34,
    CHI_TIET_CONG_NO_PHAI_THU: 17,
    TONG_HOP_CONG_NO_PHAI_THU: 18,
    SO_TAI_SAN_CO_DINH: 62,
    SO_CHI_TIET_CONG_NO_NHAN_VIEN: 45,
    SO_THEO_DOI_CONG_NO_PHAI_THU_THEO_MAT_HANG: 22,
    BANG_TONG_HOP_CHI_TIET_VAT_LIEU: 28,
    BANG_KE_CHUNG_TU_CHUA_LAP_CHUNG_TU_GHI_SO: 47,
    CHI_TIET_CONG_NO_PHAI_TRA: 13,
    Doi_Chieu_Xuat_Nhap_Kho_Va_Lenh_Lap_Rap_Thao_Do: 76,
    DOI_CHIEU_SO_TAI_SAN_VA_SO_CAI: 77,
    SO_CAI_CHUNG_TU_GHI_SO: 95,
    THE_TINH_GIA_THANH: 83
};

export const BaoCao = {
    TongHop: {
        SO_NHAT_KY_CHUNG: 'SO_NHAT_KY_CHUNG',
        SO_NHAT_KY_CHUNG_XLS: 'so_nhat_ky_chung.xlsx',
        SO_NHAT_KY_CHUNG_GOP_XLS: 'so_nhat_ky_chung_gop.xlsx',
        SO_CAI_HT_NHAT_KY_CHUNG: 'SO_CAI_HT_NHAT_KY_CHUNG',
        SO_CAI_HT_NHAT_KY_CHUNG_XLS: 'so_cai_nhat_ky_chung.xlsx',
        PHAN_BO_CHI_PHI_TRA_TRUOC: 'PHAN_BO_CHI_PHI_TRA_TRUOC',
        PHAN_BO_CHI_PHI_TRA_TRUOC_XLS: 'bang_phan_bo_chi_phi_tra_truoc.xlsx',
        SO_CHI_TIET_CAC_TAI_KHOAN: 'SO_CHI_TIET_CAC_TAI_KHOAN',
        SO_CHI_TIET_CAC_TAI_KHOAN_XLS: 'so_chi_tiet_cac_tai_khoan.xlsx',
        SO_CHI_TIET_TIEN_VAY: 'SO_CHI_TIET_TIEN_VAY',
        SO_CHI_TIET_TIEN_VAY_XLS: 'So_Chi_Tiet_Tien_Vay.xlsx',
        SO_THEO_DOI_CHI_TIET_TAM_UNG_NHAN_VIEN: 'SO_THEO_DOI_CHI_TIET_TAM_UNG_NHAN_VIEN',
        SO_THEO_DOI_CHI_TIET_TAM_UNG_NHAN_VIEN_XLS: 'So_theo_doi_chi_tiet_tam_ung_nhan_vien.xlsx',
        SO_TONG_HOP_THUE_TNCN_THEO_NHAN_VIEN: 'SO_TONG_HOP_THUE_TNCN_THEO_NHAN_VIEN',
        SO_TONG_HOP_THUE_TNCN_THEO_NHAN_VIEN_XLS: 'So_tong_hop_thue_tncn_theo_nhan_vien.xlsx',
        SO_THEO_DOI_THANH_TOAN_BANG_NGOAI_TE: 'SO_THEO_DOI_THANH_TOAN_BANG_NGOAI_TE',
        SO_THEO_DOI_THANH_TOAN_BANG_NGOAI_TE_XLS: 'So_theo_doi_thanh_toan_bang_ngoai_te.xlsx',
        SO_CHI_TIET_THANH_TOAN_VOI_NGUOI_MUA_NGUOI_BAN: 'SO_CHI_TIET_THANH_TOAN_VOI_NGUOI_MUA_NGUOI_BAN',
        SO_CHI_TIET_THANH_TOAN_VOI_NGUOI_MUA_NGUOI_BAN_XLS: 'So_Chi_Tiet_Thanh_Toan_Nguoi_Mua_Nguoi_ban.xlsx',
        SO_CHI_TIET_THANH_TOAN_VOI_NGUOI_MUA_NGUOI_BAN_BANG_NGOAI_TE: 'SO_CHI_TIET_THANH_TOAN_VOI_NGUOI_MUA_NGUOI_BAN_BANG_NGOAI_TE',
        SO_CHI_TIET_THANH_TOAN_VOI_NGUOI_MUA_NGUOI_BAN_BANG_NGOAI_TE_XLS: 'So-chi-tiet-thanh-toan-nguoi-mua-nguoi-ban-bang-ngoai-te.xlsx',
        SO_TONG_HOP_CONG_NHAN_VIEN: 'SO_TONG_HOP_CONG_NO_NHAN_VIEN',
        SO_DANG_KY_CHUNG_TU_GHI_SO: 'SO_DANG_KY_CHUNG_TU_GHI_SO',
        SO_DANG_KY_CHUNG_TU_GHI_SO_XLS: 'SO_DANG_KY_CHUNG_TU_GHI_SO.xlsx',
        SO_TONG_HOP_CONG_NHAN_VIEN_XLS: 'SoTongHopCongNoNhanVien.xlsx',
        SO_TONG_HOP_TAM_UNG_THEO_NHAN_VIEN: 'SO_TONG_HOP_TAM_UNG_THEO_NHAN_VIEN',
        SO_TONG_HOP_TAM_UNG_THEO_NHAN_VIEN_XLS: 'SoTongHopTamUngTheoNhanVien.xlsx',
        BANG_KE_CHUNG_TU_CHUA_LAP_CHUNG_TU_GHI_SO: 'BANG_KE_CHUNG_TU_CHUA_LAP_CHUNG_TU_GHI_SO',
        BANG_KE_CHUNG_TU_CHUA_LAP_CHUNG_TU_GHI_SO_XLS: 'BangKeChungTuChuaLapChungTuGhiSo.xlsx',
        SO_NHAT_KY_THU_TIEN: 'SO_NHAT_KY_THU_TIEN',
        SO_NHAT_KY_CHI_TIEN: 'SO_NHAT_KY_CHI_TIEN',
        SO_CAI_CHUNG_TU_GHI_SO: 'SO_CAI_CHUNG_TU_GHI_SO',
        SO_CAI_CHUNG_TU_GHI_SO_XLS: 'So_Cai_Chung_Tu_Ghi_So.xlsx',
        SO_CHI_TIET_TAI_KHOAN_THEO_DOI_TUONG: 'SO_CHI_TIET_TAI_KHOAN_THEO_DOI_TUONG',
        SO_CHI_TIET_TAI_KHOAN_THEO_DOI_TUONG_XLSX: 'so_chi_tiet_tai_khoan_theo_doi_tuong.xlsx',
        NHAT_KY_SO_CAI: 'NHAT_KY_SO_CAI',
        NHAT_KY_SO_CAI_XSL: 'NHAT_KY_SO_CAI.xlsx'
    },
    BaoCaoTaiChinh: {
        BANG_CAN_DOI_KE_TOAN: 'BANG_CAN_DOI_KE_TOAN',
        BANG_CAN_DOI_KE_TOAN_HOP_NHAT: 'BANG_CAN_DOI_KE_TOAN_HOP_NHAT',
        BANG_CAN_DOI_KE_TOAN_GIUA_NIEN_DO_DD: 'BANG_CAN_DOI_KE_TOAN_GIUA_NIEN_DO_DD',
        BANG_CAN_DOI_KE_TOAN_GIUA_NIEN_DO_TL: 'BANG_CAN_DOI_KE_TOAN_GIUA_NIEN_DO_TL',
        BANG_CAN_DOI_KE_TOAN_XLS: 'BangCanDoiKeToan.xlsx',
        BANG_CAN_DOI_KE_TOAN_GND_XLS: 'BangCanDoiKeToanGND.xlsx',
        BANG_CAN_DOI_KE_TOAN_TL_XLS: 'BangCanDoiKeToanTL.xlsx',
        BANG_CAN_DOI_TAI_KHOAN: 'BANG_CAN_DOI_TAI_KHOAN',
        BANG_CAN_DOI_TAI_KHOAN_XLS: 'BanCanDoiSoPhatSinh.xlsx',
        KET_QUA_HOAT_DONG_KINH_DOANH: 'KET_QUA_HOAT_DONG_KINH_DOANH',
        KET_QUA_HOAT_DONG_KINH_DOANH_HN: 'KET_QUA_HOAT_DONG_KINH_DOANH_HN',
        KET_QUA_HOAT_DONG_KINH_DOANH_GND_DD: 'KET_QUA_HOAT_DONG_KINH_DOANH_GND_DD',
        KET_QUA_HOAT_DONG_KINH_DOANH_GND_TL: 'KET_QUA_HOAT_DONG_KINH_DOANH_GND_TL',
        KET_QUA_HOAT_DONG_KINH_DOANH_XLS: 'BaoCaoKetQuaHoatDongKinhDoanh.xlsx',
        KET_QUA_HOAT_DONG_KINH_DOANH_GND_XLS: 'BaoCaoKetQuaHoatDongKinhDoanhGND.xlsx',
        KET_QUA_HOAT_DONG_KINH_DOANH_TL_XLS: 'BaoCaoKetQuaHoatDongKinhDoanhTL.xlsx',
        LUU_CHUYEN_TIEN_TE_TT: 'LUU_CHUYEN_TIEN_TE_TT',
        LUU_CHUYEN_TIEN_TE_TT_GND_DD: 'LUU_CHUYEN_TIEN_TE_TT_GND_DD',
        LUU_CHUYEN_TIEN_TE_TT_HN: 'LUU_CHUYEN_TIEN_TE_TT_HN',
        LUU_CHUYEN_TIEN_TE_TT_XLS: 'BaoCaoLuuChuyenTienTeTT.xlsx',
        LUU_CHUYEN_TIEN_TE_TT_GND_XLS: 'BaoCaoLuuChuyenTienTeTTGND.xlsx',
        LUU_CHUYEN_TIEN_TE_GT: 'LUU_CHUYEN_TIEN_TE_GT',
        LUU_CHUYEN_TIEN_TE_GT_GND_DD: 'LUU_CHUYEN_TIEN_TE_GT_GND_DD',
        LUU_CHUYEN_TIEN_TE_GT_HN: 'LUU_CHUYEN_TIEN_TE_GT_HN',
        LUU_CHUYEN_TIEN_TE_GT_XLS: 'BaoCaoLuuChuyenTienTeGT.xlsx',
        LUU_CHUYEN_TIEN_TE_GT_GND_XLS: 'BaoCaoLuuChuyenTienTeGTGND.xlsx',
        LUU_CHUYEN_TIEN_TE_GND_TL: 'LUU_CHUYEN_TIEN_TE_GND_TL',
        LUU_CHUYEN_TIEN_TE_GND_TL_XLS: 'BaoCaoLuuChuyenTienTeTL.xlsx',
        THUYET_MINH_BAO_CAO_TAI_CHINH: 'THUYET_MINH_BAO_CAO_TAI_CHINH',
        THUYET_MINH_BAO_CAO_TAI_CHINH_CL: 'THUYET_MINH_BAO_CAO_TAI_CHINH_CL',
        THUYET_MINH_BAO_CAO_TAI_CHINH_HN: 'THUYET_MINH_BAO_CAO_TAI_CHINH_HN',
        THUYET_MINH_BAO_CAO_TAI_CHINH_XLS: 'ThuyetMinhBaoCaoTaiChinh.xlsx',
        THUYET_MINH_BAO_CAO_TAI_CHINH_CL_XLS: 'ThuyetMinhBaoCaoTaiChinhCL.xlsx',

        TONG_HOP_CONG_NO_PHAI_THU_XLS: 'TongHopCongNoPhaiThu.xlsx',
        CHI_TIET_CONG_NO_PHAI_THU_XLS: 'ChiTietCongNoPhaiThu.xlsx',

        canDoiKeToan: 0,
        canDoiKeToanHopNhat: 1,
        canDoiKeToanGiuaNienDoDD: 2,
        canDoiKeToanGiuaNienDoTL: 3
    },
    BanHang: {
        SO_NHAT_KY_BAN_HANG: 'SO_NHAT_KY_BAN_HANG',
        TONG_HOP_CONG_NO_PHAI_THU: 'TONG_HOP_CONG_NO_PHAI_THU',
        TONG_HOP_CONG_NO_PHAI_THU_FROM_BCDSPS: 'TONG_HOP_CONG_NO_PHAI_THU_FROM_BCDSPS',
        CHI_TIET_CONG_NO_PHAI_THU: 'CHI_TIET_CONG_NO_PHAI_THU',
        SO_CHI_TIET_BAN_HANG: 'SO_CHI_TIET_BAN_HANG',
        SO_NHAT_KY_BAN_HANG_XLS: 'SoNhatKiBanHang.xlsx',
        SO_CHI_TIET_BAN_HANG_XLS: 'SoChiTietBanHang.xlsx',
        SO_THEO_DOI_LAI_LO_THEO_HOA_DON: 'SO_THEO_DOI_LAI_LO_THEO_HOA_DON',
        SO_THEO_DOI_CONG_NO_PHAI_THU_THEO_HOA_DON: 'SO_THEO_DOI_CONG_NO_PHAI_THU_THEO_HOA_DON',
        SO_THEO_DOI_LAI_LO_THEO_HOA_DON_XLS: 'SoTheoDoiLaiLoTheoHoaDon.xlsx',
        SO_THEO_DOI_CONG_NO_PHAI_THU_THEO_HOA_DON_XLS: 'SoTheoDoiCongNoPhaiThuTheoHoaDon.xlsx',
        SO_THEO_DOI_CONG_NO_PHAI_THU_THEO_MAT_HANG: 'SO_THEO_DOI_CONG_NO_PHAI_THU_THEO_MAT_HANG',
        SO_THEO_DOI_CONG_NO_PHAI_THU_THEO_MAT_HANG_XLS: 'so_theo_doi_cong_no_phai_thu_theo_mat_hang.xlsx',
        SO_THEO_DOI_LAI_LO_THEO_MAT_HANG: 'SO_THEO_DOI_LAI_LO_THEO_MAT_HANG',
        SO_THEO_DOI_LAI_LO_THEO_MAT_HANG_XLS: 'so_theo_doi_lai_lo_theo_mat_hang.xlsx',
        SO_CHI_TIET_CONG_NO_NHAN_VIEN: 'SO_CHI_TIET_CONG_NO_NHAN_VIEN',
        SO_CHI_TIET_CONG_NO_NHAN_VIEN_XLS: 'SoChiTietCongNoNhanVien.xlsx',
        SO_TONG_HOP_DOANH_THU_NHAN_VIEN: 'SO_TONG_HOP_DOANH_THU_NHAN_VIEN',
        SO_TONG_HOP_DOANH_THU_NHAN_VIEN_XLS: 'SoTongHopDoanhThuNhanVien.xlsx',
        SO_CHI_TIET_DOANH_THU_THEO_NHAN_VIEN: 'SO_CHI_TIET_DOANH_THU_THEO_NHAN_VIEN',
        SO_CHI_TIET_DOANH_THU_THEO_NHAN_VIEN_XLS: 'SoChiTietDoanhThuTheoNhanVien.xlsx',
        TINH_HINH_THUC_HIEN_DON_DAT_HANG: 'TINH_HINH_THUC_HIEN_DON_DAT_HANG',
        TINH_HINH_THUC_HIEN_DON_DAT_HANG_XLS: 'bao_cao_tinh_hinh_thuc_hien_don_dat_hang.xlsx',
        SO_THEO_DOI_CONG_NO_THEO_HOP_DONG: 'SO_THEO_DOI_CONG_NO_THEO_HOP_DONG',
        SO_THEO_DOI_CONG_NO_THEO_HOP_DONG_XLS: 'SoTheoDoiCongNoTheoHopDong.xlsx',
        SO_THEO_DOI_LAI_LO_THEO_HOP_DONG: 'SO_THEO_DOI_LAI_LO_THEO_HOP_DONG',
        SO_THEO_DOI_LAI_LO_THEO_HOP_DONG_XLS: 'SoTheoDoiLaiLoTheoHopDong.xlsx'
    },
    Kho: {
        SO_CHI_TIET_VAT_LIEU: 'SO_CHI_TIET_VAT_LIEU',
        SO_CHI_TIET_VAT_LIEU_XLS: 'So_chi_tiet_vat_lieu_dung_cu.xlsx',
        THE_KHO: 'THE_KHO',
        THE_KHO_XLS: 'the_kho.xlsx',
        TONG_HOP_TON_KHO: 'TONG_HOP_TON_KHO',
        TONG_HOP_TON_KHO_XLS: 'tong_hop_ton_kho.xlsx',
        TONG_HOP_CHI_TIET_VAT_LIEU: 'TONG_HOP_CHI_TIET_VAT_LIEU',
        TONG_HOP_CHI_TIET_VAT_LIEU_XLS: 'bang_tong_hop_chi_tiet_vat_dung.xlsx',
        TONG_HOP_TON_KHO_SO_LO_HAN_DUNG: 'TONG_HOP_TON_KHO_SO_LO_HAN_DUNG',
        TONG_HOP_TON_KHO_SO_LO_HAN_DUNG_XLS: 'tong_hop_ton_kho_so_lo_han_dung.xlsx',
        TONG_HOP_TON_KHO_HAN_DUNG_HANG_HOA: 'TONG_HOP_TON_KHO_HAN_DUNG_HANG_HOA',
        TONG_HOP_XUAT_KHO_THEO_LENH_SAN_XUAT: 'TONG_HOP_XUAT_KHO_THEO_LENH_SAN_XUAT',
        TONG_HOP_XUAT_KHO_THEO_LENH_SAN_XUAT_XLS: 'tong_hop_xuat_kho_theo_lenh_san_xuat.xlsx',
        TONG_HOP_TON_KHO_HAN_DUNG_HANG_HOA_XLS: 'tong_hop_ton_kho_han_dung_hang_hoa.xlsx',
        TONG_HOP_TON_KHO_MA_QUY_CACH: 'TONG_HOP_TON_KHO_MA_QUY_CACH',
        TONG_HOP_TON_KHO_MA_QUY_CACH_XLS: 'tong_hop_ton_kho_ma_quy_cach.xlsx',
        BAO_CAO_TIEN_DO_SAN_XUAT_XLSX: 'BAO_CAO_TIEN_DO_SAN_XUAT.xlsx',
        BAO_CAO_TIEN_DO_SAN_XUAT: 'BAO_CAO_TIEN_DO_SAN_XUAT',
        DOI_CHIEU_KHO_VA_SO_CAI: 'DOI_CHIEU_KHO_VA_SO_CAI',
        DOI_CHIEU_KHO_VA_SO_CAI_XLS: 'doi_chieu_kho_va_so_cai.xlsx',
        TONG_HOP_TON_KHO_TREN_NHIEU_KHO: 'TONG_HOP_TON_KHO_TREN_NHIEU_KHO',
        TONG_HOP_TON_KHO_TREN_NHIEU_KHO_XLS: 'tong_hop_ton_kho_tren_nhieu_kho.xlsx'
    },
    Quy: {
        SO_KE_TOAN_CHI_TIET_QUY_TIEN_MAT: 'SO_KE_TOAN_CHI_TIET_QUY_TIEN_MAT',
        SO_KE_TOAN_CHI_TIET_QUY_TIEN_MAT_XLS: 'SoKeToanChiTietQuyTienMat.xlsx',
        SO_QUY_TIEN_MAT: 'SO_QUY_TIEN_MAT',
        SO_QUY_TIEN_MAT_XLS: 'SoQuyTienMat.xlsx'
    },
    GiaThanh: {
        THE_TINH_GIA_THANH: 'THE_TINH_GIA_THANH',
        THE_TINH_GIA_THANH_XLS: 'The_Tinh_Gia_Thanh.xlsx',
        TONG_HOP_CHI_PHI_THEO_KHOAN_MUC_CHI_PHI: 'TONG_HOP_CHI_PHI_THEO_KHOAN_MUC_CHI_PHI',
        TONG_HOP_CHI_PHI_THEO_KHOAN_MUC_CHI_PHI_XLS: 'Tong_Hop_Chi_Phi_Theo_Khoan_Muc_Chi_Phi.xlsx',
        SO_THEO_DOI_MA_THONG_KE_THEO_TAI_KHOAN: 'SO_THEO_DOI_MA_THONG_KE_THEO_TAI_KHOAN',
        SO_THEO_DOI_MA_THONG_KE_THEO_TAI_KHOAN_XLS: 'SoTheoDoiMaThongKeTheoTaiKhoan.xlsx',
        SO_THEO_DOI_MA_THONG_KE_THEO_KHOAN_MUC_CHI_PHI: 'SO_THEO_DOI_MA_THONG_KE_THEO_KHOAN_MUC_CHI_PHI',
        SO_THEO_DOI_MA_THONG_KE_THEO_KHOAN_MUC_CHI_PHI_XLS: 'SoTheoDoiMaThongKeTheoKhoanMucChiPhi.xlsx',
        BANG_KE_PHIEU_NHAP_PHIEU_XUAT_THEO_DOI_TUONG_THCP: 'BANG_KE_PHIEU_NHAP_PHIEU_XUAT_THEO_DOI_TUONG_THCP',
        BANG_KE_PHIEU_NHAP_PHIEU_XUAT_THEO_DOI_TUONG_THCP_XLS: 'bangKePhieuNhapPhieuXuatTheoDoiTuongTHCP.xlsx',
        BANG_KE_PHIEU_NHAP_PHIEU_XUAT_THEO_HOP_DONG_XLS: 'bangKePhieuNhapPhieuXuatTheoHopDong.xlsx',
        TONG_HOP_NHAP_XUAT_KHO_THEO_DOI_TUONG_THCP: 'TONG_HOP_NHAP_XUAT_KHO_THEO_DOI_TUONG_THCP',
        TONG_HOP_NHAP_XUAT_KHO_THEO_DOI_TUONG_THCP_XLS: 'Tong_Hop_Nhap_Xuat_Kho_Theo_Doi_Tuong_THCP.xlsx',
        TONG_HOP_NHAP_XUAT_KHO_THEO_HOP_DONG_XLS: 'Tong_Hop_Nhap_Xuat_Kho_Theo_Hop_Dong.xlsx'
    },
    BaoCaoDoiChieu: {
        DOI_CHIEU_CONG_NO_PHAI_THU: 'DOI_CHIEU_CONG_NO_PHAI_THU',
        DOI_CHIEU_CONG_NO_PHAI_THU_XLS: 'Doi_Chieu_Cong_No_Phai_Thu_Va_Chung_Tu_Thanh_Toan.xlsx',
        DOI_CHIEU_CONG_NO_PHAI_TRA: 'DOI_CHIEU_CONG_NO_PHAI_TRA',
        DOI_CHIEU_CONG_NO_PHAI_TRA_XLS: 'Doi_Chieu_Cong_No_Phai_Tra_Va_Chung_Tu_Thanh_Toan.xlsx',
        DOI_CHIEU_BANG_TINH_GIA_THANH_VA_GIA_TRI_NHAP_KHO: 'DOI_CHIEU_BANG_TINH_GIA_THANH_VA_GIA_TRI_NHAP_KHO',
        DOI_CHIEU_BANG_TINH_GIA_THANH_VA_GIA_TRI_NHAP_KHO_XLS: 'Doi_Chieu_Bang_Tinh_Gia_Thanh_Va_Gia_Tri_Nhap_Kho.xlsx',
        DOI_CHIEU_XUAT_NHAP_KHO_VA_LENH_LAP_RAP_THAO_DO: 'DOI_CHIEU_XUAT_NHAP_KHO_VA_LENH_LAP_RAP_THAO_DO',
        Doi_Chieu_Xuat_Nhap_Kho_Va_Lenh_Lap_Rap_Thao_Do: 'Doi-Chieu-Xuat-Kho-Nhap-Kho-Va-Lenh-Lap-Rap-Thao-Do.xlsx',
        DOI_CHIEU_CHI_PHI_TRA_TRUOC_VA_SO_CAI: 'DOI_CHIEU_CHI_PHI_TRA_TRUOC_VA_SO_CAI',
        DOI_CHIEU_CHI_PHI_TRA_TRUOC_VA_SO_CAI_XLS: 'DOI_CHIEU_CHI_PHI_TRA_TRUOC_VA_SO_CAI.xlsx',
        DOI_CHIEU_SO_THEO_DOI_CCDC_VA_SO_CAI: 'DOI_CHIEU_SO_THEO_DOI_CCDC_VA_SO_CAI',
        DOI_CHIEU_SO_THEO_DOI_CCDC_VA_SO_CAI_XLS: 'DOI_CHIEU_SO_THEO_DOI_CCDC_VA_SO_CAI.xlsx',
        DOI_CHIEU_SO_HD_CHUNG_TU_MUA_HANG_VA_BANG_KE_HHDV_MUA_VAO: 'DOI_CHIEU_SO_HOA_DON_TREN_CHUNG_TU_MUA_HANG_VA_BANG_KE_HHDV_MUA_VAO',
        DOI_CHIEU_SO_HD_CHUNG_TU_MUA_HANG_VA_BANG_KE_HHDV_MUA_VAO_XLS:
            'Doi_Chieu_So_Hoa_Don_Tren_Chung_Tu_Mua_Hang_Va_Bang_Ke_HHDV_Mua_Vao.xlsx',
        CHI_PHI_MUA_GIUA_CT_MUA_HANG_VA_CHI_PHI: 'CHI_PHI_MUA_GIUA_CT_MUA_HANG_VA_CHI_PHI',
        CHI_PHI_MUA_GIUA_CT_MUA_HANG_VA_CHI_PHI_XLS: 'BCDCChiPhiMuaGiuaCTMuaHangVaChiPhi.xlsx',
        BANG_KE_THUE_VA_SO_CAI: 'BANG_KE_THUE_VA_SO_CAI',
        BANG_KE_THUE_VA_SO_CAI_XLS: 'BCDC_Bang_Ke_Thue_Va_So_Cai.xlsx'
    },
    SO_NHAT_KY_THU_TIEN: 'SO_NHAT_KY_THU_TIEN',
    SO_NHAT_KY_THU_TIEN_XLS: 'SO_NHAT_KY_THU_TIEN.xlsx',
    SO_NHAT_KY_CHI_TIEN: 'SO_NHAT_KY_CHI_TIEN',
    SO_NHAT_KY_CHI_TIEN_XLS: 'SO_NHAT_KY_CHI_TIEN.xlsx',
    BANG_KE_BAN_RA: 'BANG_KE_BAN_RA',
    BANG_KE_BAN_RA_XLS: 'BangKe_HH_DV_BanRa.xlsx',
    BANG_KE_BAN_RA_QUAN_TRI_XLS: 'BangKe_HH_DV_BanRa_QT.xlsx',
    BANG_KE_MUA_VAO: 'BANG_KE_MUA_VAO',
    BANG_KE_MUA_VAO_XLS: 'BangKe_HH_DV_MuaVao.xlsx',
    BANG_KE_MUA_VAO_QUAN_TRI_XLS: 'BangKe_HH_DV_MuaVao_QT.xlsx',
    CAC_KHOAN_PHAI_NOP_NSNN: 'CAC_KHOAN_PHAI_NOP_NSNN',
    CAC_KHOAN_PHAI_NOP_NSNN_XLS: 'TongHopCacKhoanPhaiNopNganSachNhaNuoc.xlsx',
    MuaHang: {
        SO_NHAT_KY_MUA_HANG: 'SO_NHAT_KY_MUA_HANG',
        TONG_HOP_CONG_NO_PHAI_TRA: 'TONG_HOP_CONG_NO_PHAI_TRA',
        TONG_HOP_CONG_NO_PHAI_TRA_FROM_BCDSPS: 'TONG_HOP_CONG_NO_PHAI_TRA_FROM_BCDSPS',
        TONG_HOP_CONG_NO_PHAI_TRA_XLS: 'TongHopCongNoPhaiTra.xlsx',
        CHI_TIET_CONG_NO_PHAI_TRA: 'CHI_TIET_CONG_NO_PHAI_TRA',
        CHI_TIET_CONG_NO_PHAI_TRA_XLS: 'ChiTietCongNoPhaiTra.xlsx',
        SO_CHI_TIET_MUA_HANG: 'SO_CHI_TIET_MUA_HANG',
        SO_CHI_TIET_MUA_HANG_XLS: 'SoChiTietMuaHang.xlsx',
        SO_NHAT_KY_MUA_HANG_XLS: 'SoNhatKiMuaHang.xlsx',
        SO_THEO_DOI_CHI_TIET_CONG_NO_PHAI_TRA_THEO_HOA_DON: 'SO_THEO_DOI_CHI_TIET_CONG_NO_PHAI_TRA_THEO_HOA_DON',
        SO_THEO_DOI_CHI_TIET_CONG_NO_PHAI_TRA_THEO_HOA_DON_XLS: 'SoTheoDoiChiTietCongNoPhaiTraTheoHoaDon.xlsx'
    },
    Ngan_Hang: {
        SO_TIEN_GUI_NGAN_HANG: 'SO_TIEN_GUI_NGAN_HANG',
        BANG_KE_SO_DU_NGAN_HANG: 'BANG_KE_SO_DU_NGAN_HANG',
        SO_TIEN_GUI_NGAN_HANG_XLS: 'TienGuiNganHang.xlsx',
        BANG_KE_SO_DU_NGAN_HANG_XLS: 'BangKeSoDuNganHang.xlsx'
    },
    Luong: {
        SO_TONG_HOP_LUONG_NHAN_VIEN: 'SO_TONG_HOP_LUONG_NHAN_VIEN',
        SO_TONG_HOP_LUONG_NHAN_VIEN_XLS: 'SoTongHopLuongNhanVien.xlsx',
        SO_TONG_HOP_CAC_KHOAN_BH_THEO_NHAN_VIEN: 'SO_TONG_HOP_CAC_KHOAN_BH_THEO_NHAN_VIEN',
        SO_TONG_HOP_CAC_KHOAN_BH_THEO_NHAN_VIEN_XLS: 'SO_TONG_HOP_CAC_KHOAN_BH_THEO_NHAN_VIEN.xlsx',
        SO_TONG_HOP_TAM_UNG_THEO_NHAN_VIEN: 'SO_TONG_HOP_TAM_UNG_THEO_NHAN_VIEN',
        SO_TONG_HOP_TAM_UNG_THEO_NHAN_VIEN_XLS: 'SoTongHopTamUngTheoNhanVien.xlsx',
        SO_THEO_DOI_CHI_TIET_TAM_UNG_NHAN_VIEN: 'SO_THEO_DOI_CHI_TIET_TAM_UNG_NHAN_VIEN',
        SO_THEO_DOI_CHI_TIET_TAM_UNG_NHAN_VIEN_XLS: 'So_theo_doi_chi_tiet_tam_ung_nhan_vien.xlsx',
        SO_TONG_HOP_THUE_TNCN_THEO_NHAN_VIEN: 'SO_TONG_HOP_THUE_TNCN_THEO_NHAN_VIEN',
        SO_TONG_HOP_THUE_TNCN_THEO_NHAN_VIEN_XLS: 'So_tong_hop_thue_tncn_theo_nhan_vien.xlsx'
    },
    HDDT: {
        BAO_CAO_TINH_HINH_SU_DUNG_HD: 'BAO_CAO_TINH_HINH_SU_DUNG_HD',
        BANG_KE_HD_HHDV: 'BANG_KE_HD_HHDV',
        BAO_CAO_DOANH_THU_THEO_SP: 'BAO_CAO_DOANH_THU_THEO_SP',
        BAO_CAO_DOANH_THU_THEO_BEN_MUA: 'BAO_CAO_DOANH_THU_THEO_BEN_MUA'
    },
    Gia_Thanh: {
        SO_CHI_PHI_SAN_XUAT_KINH_DOANH: 'SO_CHI_PHI_SAN_XUAT_KINH_DOANH',
        SO_CHI_PHI_SAN_XUAT_KINH_DOANH_XLS: 'SO_CHI_PHI_SAN_XUAT_KINH_DOANH.xlsx',
        SO_THEO_DOI_DOI_TUONG_THCP_THEO_TAI_KHOAN: 'SO_THEO_DOI_DOI_TUONG_THCP_THEO_TAI_KHOAN',
        SO_THEO_DOI_DOI_TUONG_THCP_THEO_TAI_KHOAN_XSL: 'So_theo_doi_doi_tuong_THCP_theo_tai_khoan.xlsx',
        SO_THEO_DOI_DOI_TUONG_THCP_THEO_TAI_KHOAN_V2_XSL: 'So_theo_doi_doi_tuong_THCP_theo_tai_khoan_V2.xlsx',
        SO_THEO_DOI_DOI_TUONG_THCP_THEO_KHOAN_MUC_CHI_PHI: 'SO_THEO_DOI_DOI_TUONG_THCP_THEO_KHOAN_MUC_CHI_PHI',
        SO_THEO_DOI_DOI_TUONG_THCP_THEO_KHOAN_MUC_CHI_PHI_XSL: 'So_theo_doi_doi_tuong_THCP_theo_khoan_muc_chi_phi.xlsx'
    },
    CCDC: {
        SO_CONG_CU_DUNG_CU: 'SO_CONG_CU_DUNG_CU',
        SO_CONG_CU_DUNG_CU_XLS: 'SoCongCuDungCu.xlsx',
        SO_THEO_DOI_CCDC_TAI_NOI_SU_DUNG: 'SO_THEO_DOI_CCDC_TAI_NOI_SU_DUNG',
        SO_THEO_DOI_CCDC_TAI_NOI_SU_DUNG_XLS: 'SoTheoDoiCCDCTaiNoiSuDung.xlsx',
        BANG_TINH_PHAN_BO_CCDC: 'BANG_TINH_PHAN_BO_CONG_CU_DUNG_CU',
        BANG_TINH_PHAN_BO_CCDC_XLS: 'BangTinhPhanBoCongCuDungCu.xlsx'
    },
    TSCD: {
        SO_TAI_SAN_CO_DINH: 'SO_TAI_SAN_CO_DINH',
        SO_TAI_SAN_CO_DINH_XLS: 'SoTSCD.xlsx',
        THE_TAI_SAN_CO_DINH: 'THE_TAI_SAN_CO_DINH',
        THE_TAI_SAN_CO_DINH_XLS: 'TheTaiSanCoDinh.xlsx',
        SO_THEO_DOI_TAI_SAN_CO_DINH_TAI_NOI_SU_DUNG: 'SO_THEO_DOI_TAI_SAN_CO_DINH_TAI_NOI_SU_DUNG',
        SO_THEO_DOI_TAI_SAN_CO_DINH_TAI_NOI_SU_DUNG_XLS: 'SoTheoDoiTSCDTaiNoiSuDung.xlsx'
    },
    HopDong: {
        TINH_HINH_THUC_HIEN_HOP_DONG_MUA: 'TINH_HINH_THUC_HIEN_HOP_DONG_MUA',
        TINH_HINH_THUC_HIEN_HOP_DONG_MUA_XSL: 'Tinh_Hinh_Thuc_Hien_Hop_Dong_Mua.xlsx',
        TINH_HINH_THUC_HIEN_HOP_DONG_BAN: 'TINH_HINH_THUC_HIEN_HOP_DONG_BAN',
        TINH_HINH_THUC_HIEN_HOP_DONG_BAN_XSL: 'Tinh_Hinh_Thuc_Hien_Hop_Dong_Ban.xlsx'
    },
    DoiChieu: {
        BAO_CAO_DOI_CHIEU_TAI_SAN_VA_SO_CAI: 'BAO_CAO_DOI_CHIEU_TAI_SAN_VA_SO_CAI',
        Bao_Cao_Doi_Chieu_Tai_San_Va_So_Cai: 'Doi-Chieu-So-Tai-San-Va-So-Cai.xlsx'
    },
    HoaDon: {
        BAO_CAO_TINH_HINH_SU_DUNG_HOA_DON: 'BAO_CAO_TINH_HINH_SU_DUNG_HOA_DON',
        BAO_CAO_TINH_HINH_SU_DUNG_HOA_DON_XSL: 'BaoCaoTinhHinhSuDungHoaDon.xlsx'
    }
};

export const TypeBCTC = {
    B01_DN: 1,
    B02_DN: 2,
    B03_DN: 3,
    B03_DN_GT: 4,
    B09_DN: 5,
    B01a_DN: 6,
    B01b_DN: 7,
    B02a_DN: 8,
    B02b_DN: 9,
    B03a_DN: 10,
    B03b_DN: 11,
    B03a_DN_GT: 12,
    B01_DN_HN: 13,
    B02_DN_HN: 14,
    B03_DN_HN: 15,
    B03_DN_GT_HN: 16
};

export const B09Tab = {
    B09Tab1: 1,
    B09Tab2: 2,
    B09Tab3: 3,
    B09Tab4: 4,
    B09Tab5: 5,
    B09Tab6: 6,
    B09Tab7: 7,
    B09Tab8: 8,
    B09Tab9: 9,
    B09Tab10: 10,
    B09Tab11: 11,
    B09Tab12: 12,
    B09Tab13: 13,
    B09Tab14: 14,
    B09Tab15: 15,
    B09Tab16: 16,
    B09Tab17: 17,
    B09Tab18: 18,
    B09Tab19: 19
};

export const TOOLTIPS = {
    DELETE: 'ebwebApp.mBDeposit.toolTip.delete',
    ADD: 'ebwebApp.mBDeposit.toolTip.add',
    EDIT: 'ebwebApp.mBDeposit.toolTip.edit',
    SAVE: 'ebwebApp.mBDeposit.toolTip.save',
    RECORD: 'ebwebApp.mBDeposit.toolTip.record',
    UNRECORD: 'ebwebApp.mBDeposit.toolTip.unrecord',
    EXPORT: 'ebwebApp.mBDeposit.toolTip.export',
    PRINT: 'ebwebApp.mBDeposit.toolTip.print',
    SEARCH: 'ebwebApp.mBDeposit.toolTip.search',
    SAVE_AND_NEW: 'ebwebApp.mBDeposit.toolTip.saveAndNew',
    COPY_AND_NEW: 'ebwebApp.mBDeposit.toolTip.copyAndNew',
    CLOSE_FORM: 'ebwebApp.mBDeposit.toolTip.closeForm',
    INSERT_LINE: 'ebwebApp.mBDeposit.toolTip.insertLine',
    DELETE_LINE: 'ebwebApp.mBDeposit.toolTip.deleteLine'
};

export const WarningPackage = {
    limitedNo: 1,
    expiredTime: 1
};

export const MSGERROR = {
    XUAT_QUA_SO_TON: 'xuat qua so ton',
    XUAT_QUA_TON_QUY: 'xuat qua ton quy',
    DA_PHAT_SINH_CHUNG_TU_GHI_SO: 'DA_PHAT_SINH_CHUNG_TU_GHI_SO',
    XUAT_QUA_TON_QUY_QT: 'xuat qua ton quy so quan tri',
    XUAT_QUA_TON_QUY_TC: 'xuat qua ton quy so tai chinh',
    NGAY_CHUNG_TU_NHO_HON_NGAY_BAT_DAU: 'ngay chung tu nho hon ngay bat dau',
    KC_DU_LIEU_AM: 'kcDataError'
};

export const MultiAction = {
    GHI_SO: 'GHI_SO',
    BO_GHI_SO: 'BO_GHI_SO',
    XOA: 'XOA'
};

export const SingleAction = {
    GHI_SO: 'Single_GHI_SO',
    BO_GHI_SO: 'Single_BO_GHI_SO',
    XOA: 'Single_XOA'
};

export const BROADCAST_EVENT = {
    DISABLE_USER_SELECT: 'DISABLE_USER_SELECT'
};

export const GIA_THANH = {
    PHUONG_PHAP_TY_LE: 2,
    PHUONG_PHAP_GIAN_DON: 0,
    PHUONG_PHAP_HE_SO: 1,
    PHUONG_PHAP_CONG_TRINH_VU_VIEC: 3,
    PHUONG_PHAP_DON_HANG: 4,
    PHUONG_PHAP_HOP_DONG: 5
};

export const DTTHCP = {
    CONG_TRINH_VU_VIEC: 1
};

export const NCCDV_EINVOICE = {
    SDS: 'SDS',
    SIV: 'SIV',
    MIV: 'MIV',
    VNPT: 'VNPT',
    BKAV: 'BKAV',
    CYBER: 'CYBER'
};

export const EINVOICE_RESPONE = {
    Success: 2,
    InvalidData: 4,
    Fail: 1,
    errorCode: 'SUCCESS'
};

export const ImportExcel = {
    SELECT_SHEET: 'SELECT_SHEET'
};

export const DanhMucType = {
    KH: 'KH',
    NV: 'NV',
    NCC: 'NCC',
    VTHH: 'VTHH',
    DONMUAHANG: 'DONMUAHANG',
    CCDC: 'CCDC',
    CCDCDK: 'CCDCDK',
    DKTT: 'DKTT',
    PTVC: 'PTVC',
    KMCP: 'KMCP',
    LVTHH: 'LVTHH',
    TTD: 'TTD',
    CPDDDK: 'CPDDDK',
    KHNCC: 'KHNCC',
    TKNH: 'TKNH',
    DTTHCP: 'DTTHCP',
    NGB: 'NGB',
    KHO: 'KHO',
    GTTP: 'GTTP',
    PBCP: 'PBCP',
    DM_NVL: 'DM_NVL',
    TSCDDK: 'TSCDDK',
    TSCD: 'TSCD',
    MTC: 'MTC',
    MTK: 'MTK',
    CP_TRA_TRUOC: 'CP_TRA_TRUOC'
};

export const SDSoQuanTri = {
    USE: '1',
    NOT_USE: '0'
};

export const validExcel = {
    MAXIMUM_SIZE_UPLOADED: 2097152
};

export const BanHangType = {
    HBTL: 'HBTL',
    HBGG: 'HBGG',
    CTBH: 'CTBH'
};

export const TypeGvoucherList = {
    CHUNG_TU_GHI_SO_KHONG_CONG_GOP: 6869,
    CHUNG_TU_GHI_SO_CONG_GOP_CUNG_CHUNG_TU: 6870,
    CHUNG_TU_GHI_SO_CONG_GOP_TOAN_BO_CHUNG_TU: 6871
};

export const MuaHangType = {
    HMTL: 'HMTL',
    HMGG: 'HMGG'
};

export const HandlingResult = {
    Success: 0,
    Fail: 1
};

export const NameCategory = {
    Khoi_Tao_Mau_Hoa_Don: 'QLHĐ/Khởi tạo mẫu hóa đơn',
    Dang_Ky_Su_Dung_Hoa_Don: 'QLHĐ/Đăng ký sử dụng hóa đơn',
    Thong_Bao_Phat_Hanh_Hoa_Don: 'QLHĐ/Thông báo phát hành hóa đơn',
    LOAI_TAI_SAN_CO_DINH: 'Danh mục/Loại tài sản cố định',
    Don_Mua_Hang: 'MH/Đơn Mua Hàng',
    Lenh_Lap_Rap_Thao_Do: 'Kho/Lệnh lắp ráp, tháo dỡ',
    Chuyen_Kho: 'Kho/Chuyển kho',
    Phuong_Thuc_Van_Chuyen: 'Danh mục/Phương thức vận chuyển',
    He_Thong_Tai_Khoan: 'Danh mục/Hệ thống tài khoản',
    CO_CAU_TO_CHUC: 'Danh mục/Cơ cấu tổ chức',
    NHAN_HOA_DON: 'MH/Nhận hóa đơn',
    Tai_Khoan_Ket_Chuyen: 'Danh mục/Tài khoản kết chuyển',
    CHAM_CONG: 'Lương/Chấm công',
    TONG_HOP_CHAM_CONG: 'Lương/Tổng hợp chấm công',
    BANG_LUONG: 'Lương/Bảng lương',
    Chung_Tu_Nghiep_Vu_Khac: 'Tổng hợp/Chứng từ nghiệp vụ khác',
    Bao_Co: 'TMNH/Báo có',
    Bao_No: 'TMNH/Báo nợ',
    TMNH_The_Tin_Dung: 'TMNH/Thẻ tín dụng',
    Tai_Khoan_Ngam_Dinh: 'Danh mục/Tài khoản ngầm định',
    Lenh_San_Xuat: 'Lệnh sản xuất ',
    Dinh_Muc_Nguyen_Vat_Lieu: 'Danh mục/Định mức nguyên vật liệu',
    Dinh_Khoan_Tu_Dong: 'Danh mục/Định khoản tự động',
    Nhap_Kho: 'Kho/Nhập kho',
    Xuat_Kho: 'Kho/Xuất kho',
    Xuat_Kho_HMTL: 'Kho/Xuất kho từ hàng mua trả lại',
    Xuat_Kho_BH: 'Kho/Xuất kho từ bán hàng',
    Nhap_Kho_MH: 'Kho/Nhập kho từ mua hàng',
    Nhap_Kho_HBTL: 'Kho/Xuất kho từ hàng bán trả lại',
    Khai_Bao_TSCD: 'TSCD/Khai báo tài sản cố định',
    Tinh_Gia_Ban: 'Tính giá bán',
    Tai_San_Co_Dinh: 'Tài sản cố định',
    Xoa_hoa_don: 'QLHD/Xóa hóa đơn',
    Cong_Cu_Dung_Cu: 'Công cụ dụng cụ',
    Khai_bao_cong_cu_dung_cu_DK: 'CCDC/Khai báo CCDC đầu kỳ',
    Kiem_Ke_Quy: 'Kiểm kê quỹ',
    Chung_Tu_Mua_Hang: 'Mua hàng/Chứng từ mua hàng',
    Chung_Tu_Ban_Hang: 'Bán hàng/Chứng từ bán hàng',
    Xuat_Hoa_Don: 'Bán hàng/Xuất hóa đơn',
    Hang_Ban_TL: 'Bán hàng/Hàng bán trả lại',
    Hang_Ban_GG: 'Bán hàng/Hàng bán giảm giá',
    Ket_Chuyen_Lai_Lo: 'Tổng hợp/Kết chuyển lãi lỗ',
    Nop_Bao_Hiem: 'Lương/Nộp bảo hiểm',
    Thanh_Toan_Luong: 'Lương/Thanh toán lương',
    Phieu_Chi: 'TMNH/Phiếu Chi',
    Gia_Thanh_Gian_Don: 'Giá thành/Giá thành giản đơn',
    Gia_Thanh_Ty_Le: 'Giá thành/Giá thành tỷ lệ',
    Gia_Thanh_He_So: 'Giá thành/Giá thành hệ số',
    Gia_Thanh_CTVV: 'Giá thành/Giá thành công trình vụ việc',
    Gia_Thanh_Don_Hang: 'Giá thành/Giá thành đơn hàng',
    Gia_Thanh_Hop_Dong: 'Giá thành/Giá thành hợp đồng',
    Sao_Luu_Khoi_Phuc: 'Tiện ích/Sao lưu và khôi phục dữ liệu',
    Dinh_Muc_Gia_Thanh_Thanh_Pham: 'Giá thành/Định mức giá thành thành phẩm',
    Dinh_Muc_Phan_Bo_Chi_Phi: 'Giá thành/Định mức phân bổ chi phí',
    Chi_Phi_Do_Dang_Dau_Ky: 'Giá thành/Chi phí dở dang đầu kỳ',
    Ket_Chuyen_Chi_Phi: 'Giá thành/Kết chuyển chi phí',
    Chung_Tu_Ghi_So: 'Chứng từ ghi sổ',
    Huy_Hoa_Don: 'Hủy hóa đơn',
    Phieu_Thu: 'TMNH/Phiếu thu',
    Chuyen_Tien_Noi_Bo: 'Chuyển tiền nội bộ',
    Don_Dat_Hang: 'Bán hàng/Đơn đặt hàng',
    Thue_01GTGT: 'Thuế/Tờ khai thuế GTGT khấu trừ (Mẫu 01/GTGT)',
    Thue_02GTGT: 'Thuế/Tờ khai thuế GTGT cho dự án đầu tư (Mẫu 02/GTGT)',
    Thue_03TNDN: 'Thuế/Quyết toán thuế TNDN năm (Mẫu 03/TNDN)',
    Thue_01TAIN: 'Thuế/Tờ khai thuế tài nguyên (Mẫu 01/TAIN)',
    Thue_02TAIN: 'Thuế/Quyết toán thuế tài nguyên (Mẫu 02/TAIN)',
    Thue_04GTGT: 'Thuế/Tờ khai thuế GTGT (Mẫu 04/GTGT)',
    Thue_KhauTru: 'Thuế/Khấu trừ thuế GTGT',
    Thue_NopThue: 'Thuế/Nộp thuế',
    Kiem_Ke_Kho: 'Kho/Kiểm kê kho',
    Nhap_So_Du_Dau_Ky: 'Nhập số dư cho tài khoản'
};

export const TypeMultiPrintTemplate = {
    PHIEU_THU: 0,
    PHIEU_THU_A5: 1,
    PHIEU_THU_2_LIEN: 2,
    PHIEU_CHI: 3,
    PHIEU_CHI_A5: 4,
    PHIEU_CHI_2_LIEN: 5,
    CHUNG_TU_KE_TOAN_THUONG: 6,
    GIAY_BAO_CO: 7,
    GIAY_BAO_NO: 8,
    PHIEU_NHAP_KHO: 9,
    PHIEU_NHAP_KHO_SO_LO_HAN_DUNG: 10,
    PHIEU_XUAT_KHO: 11,
    PHIEU_XUAT_KHO_SO_LO_HAN_DUNG: 12,
    PHIEU_XUAT_KHO_KIEM_VCNB: 13,
    LENH_LAP_RAP: 14,
    LENH_THAO_DO: 15,
    CHUNG_TU_KE_TOAN_QUY_DOI: 16,
    PHIEU_NHAP_KHO_A5: 17,
    HOA_DON_GTGT: 18,
    PHIEU_XUAT_KHO_MA_QUY_CACH: 19,
    HOA_DON_BAN_HANG: 20,
    BANG_KE_HHDV: 21,
    PHIEU_NHAP_KHO_THEO_MA_QUY_CACH: 22,
    DON_MUA_HANG: 23,
    BANG_KIEM_KE_QUY: 24,
    BANG_CHAM_CONG_THEO_GIO: 25,
    BANG_CHAM_CONG_THEO_NGAY: 26,
    BANG_TONG_HOP_CHAM_CONG_THEO_NGAY: 27,
    BANG_TONG_HOP_CHAM_CONG_THEO_GIO: 28,
    BANG_LUONG_THEO_BUOI: 29,
    BANG_LUONG_THEO_GIO: 30,
    BANG_LUONG_CO_BAN_CO_DINH: 31,
    BANG_TINH_BAO_HIEM: 32,
    CHUNG_TU_GHI_SO_KHONG_CONG_GOP: 33,
    CHUNG_TU_GHI_SO_CONG_GOP_CUNG_CHUNG_TU: 34,
    CHUNG_TU_GHI_SO_CONG_GOP_TOAN_BO: 35,
    PHIEU_XUAT_KHO_A5: 36,
    BANG_TINH_KHAU_HAO_TSCD: 37,
    BIEN_BAN_KIEM_KE_TSCD: 38,
    BIEN_BAN_DANH_GIA_LAI_TSCD: 39,
    CHUNG_TU_DIEU_CHUYEN_TSCD: 40,
    CHUNG_TU_GHI_GIAM_CCDC: 41,
    CHUNG_TU_PHAN_BO_CCDC: 42,
    BIEN_BAN_KIEM_KE_CCDC: 43,
    CHUNG_TU_DIEU_CHINH_CCDC: 44,
    CHUNG_TU_DIEU_CHUYEN_CCDC: 45,
    LENH_SAN_XUAT: 46,
    DON_DAT_HANG: 47,
    BAO_GIA_MAU_CO_BAN: 48,
    BAO_GIA_MAU_DAY_DU: 49,
    BAO_GIA_MAU_EXTEND: 54,
    DON_DAT_HANG_EXTEND: 55,
    CHUNG_TU_GHI_GIAM_TSCD: 50,
    PHIEU_XUAT_KHO_BAN_HANG: 51,
    HUY_HOA_DON: 52,
    MAT_CHAY_HONG_HOA_DON: 53
};

export const TYPEGROUP = {
    PHIEU_THU: 10,
    PHIEU_CHI: 11,
    UY_NHIEM_CHI: 12,
    SEC_CHUYEN_KHOAN: 13,
    SEC_TIEN_MAT: 14,
    CHUYEN_TIEN_NOI_BO: 15,
    BAO_CO: 16,
    THE_TIN_DUNG: 17,
    KIEM_KE_QUY: 18,
    DON_MUA_HANG: 20,
    MUA_HANG: 21,
    HANG_MUA_TRA_LAI: 22,
    HANG_MUA_GIAM_GIA: 23,
    MUA_DICH_VU: 24,
    HOA_DON_DAU_VAO: 25,
    BAO_GIA: 30,
    DON_DAT_HANG: 31,
    BAN_HANG: 32,
    HANG_BAN_TRA_LAI: 33,
    HANG_BAN_GIAM_GIA: 34,
    XUAT_HOA_DON: 35,
    NHAP_KHO: 40,
    XUAT_KHO: 41,
    CHUYEN_KHO: 42,
    LENH_LAP_RAP: 43,
    LENH_SAN_XUAT: 44,
    KHAI_BAO_CCDC_DAU_KY: 50,
    MUA_VA_GHI_TANG_CCDC: 51,
    GHI_TANG_CCDC: 52,
    GHI_GIAM_CCDC: 53,
    DIEU_CHUYEN_CCDC: 54,
    DIEU_CHINH_CCDC: 55,
    PHAN_BO_CCDC: 56,
    KIEM_KE_CCDC: 57,
    KHAI_BAO_TSCD_DAU_KY: 60,
    MUA_VA_GHI_TANG_TSCD: 61,
    GHI_TANG_TSCD: 62,
    GHI_GIAM_TSCD: 63,
    DIEU_CHUYEN_TSCD: 64,
    DIEU_CHINH_TSCD: 65,
    TINH_KHAU_HAO_TSCD: 66,
    KIEM_KE_TSCD: 67,
    CHUNG_TU_NGHIEP_VU_KHAC: 70,
    CHUNG_TU_GHI_SO: 71,
    HOP_DONG_MUA: 72,
    HOP_DONG_BAN: 73,
    SO_DU_DAU_KY: 74,
    BANG_CHAM_CONG: 75,
    BANG_TONG_HOP_CHAM_CONG: 76,
    BANG_LUONG: 77,
    KHOI_TAO_MAU_HOA_DON: 80,
    DANG_KY_SU_DUNG_HOA_DON: 81,
    THONG_BAO_PHAT_HANH_HOA_DON: 82,
    HUY_HOA_DON: 83,
    MAT_CHAY_HONG_HOA_DON: 84,
    XOA_HOA_DON: 85,
    DIEU_CHINH_THONG_BAO_PHAT_HANH: 86,
    TO_KHAI_THUE_GTGT_KHAU_TU_01_GTGT: 90,
    TO_KHAI_THUE_GTGT_KHAU_TU_04_GTGT: 91,
    TO_KHAI_THUE_GTGT_CHO_DU_AN_DAU_TU: 92,
    QUYET_TOAN_THUE_TNCN_DN_NAM: 93,
    TO_KHAI_THUE_TAI_NGUYEN: 94,
    QUYET_TOAN_THUE_TAI_NGUYEN: 95
};

export const QLHD = {
    ThongTu39: 39,
    ThongTu68: 68
};

export const ACTION = {
    MultiRecord: 'multi_record',
    MultiUnRecord: 'multi_un_record',
    MultiDelete: 'multi_delete'
};

export const TYPEPDFMAUDON = {
    // tự đánh số
    DINHMUCNGUYENVATLIEU: 1000,
    DINHMUCGIATHANH: 1001,
    CHIPHIDODANG: 1002,
    DINHMUCPHANBO: 1003,
    THONGBAOPHATHANHHDDT: 1004,
    THONGBAOPHATHANHHDTUIN: 1005
};

export const TypeLedger = {
    SoTaiChinh: 0,
    SoQuanTri: 1,
    Ca2So: 2
};

export const HOST = {
    TEST_200: '14.225.3.184:8081',
    TEST_133: '14.225.17.177:8082',
    APP_200: 'app.easybooks.vn',
    APP_133: 'app133.easybooks.vn',
    DAO_TAO_200: 'daotao.easybooks.vn',
    DAO_TAO_133: 'daotao133.easybooks.vn',
    LOCALHOST: 'localhost:9000'
};

export const ViTri = {
    TenCCTC: 'headerOrgFont',
    TenThemCCTC: 'headerAddInfoOrgFont',
    TieuDeChinh: 'titleFont',
    TieuDePhu: 'subTitleFont',
    TieuDeCot: 'headerFont',
    NoiDungKieuChu: 'bodyTextFont',
    NoiDungKieuSo: 'bodyNumberFont',
    NoiDungTongCong: 'bodySumFont',
    CuoiTrangKieuChu: 'footerTextFont',
    CuoiTrangTongCong: 'footerSumFont',
    NgayThang: 'monthDayFont',
    ChucDanh: 'positionFont',
    LoaiKy: 'typeSignedFont',
    ChuKy: 'signedFont'
};

export const UpdateDataMessages = {
    SAVE_SUCCESS: 'SAVE_SUCCESS',
    UPDATE_SUCCESS: 'UPDATE_SUCCESS',
    EXPENSIVE_ITEM_HAS_CHILD: 'EXPENSIVE_ITEM_HAS_CHILD',
    CANNOT_UPDATE_OTHER_NO_BOOK: 'CANNOT_UPDATE_OTHER_NO_BOOK',
    DUPLICATE_OTHER_NO_BOOK: 'DUPLICATE_OTHER_NO_BOOK',
    CANNOT_UPDATE_NO_BOOK: 'CANNOT_UPDATE_NO_BOOK',
    DUPLICATE_NO_BOOK: 'DUPLICATE_NO_BOOK',
    DUPLICATE_NO_BOOK_RS: 'DUPLICATE_NO_BOOK_RS',
    CANNOT_UPDATE_NO_BOOK_RS: 'CANNOT_UPDATE_NO_BOOK_RS',
    CURRENT_USER_IS_NOT_PRESENT: 'CURRENT_USER_IS_NOT_PRESENT',
    DATE_NULL: 'DATE_NULL',
    PP_SERVICE_IS_NOT_PRESET: 'PP_SERVICE_IS_NOT_PRESET',
    MC_PAYMENT_IS_NOT_PRESET: 'MC_PAYMENT_IS_NOT_PRESET',
    MB_TELLER_PAPER_IS_NOT_PRESET: 'MB_TELLER_PAPER_IS_NOT_PRESET',
    MB_CREDIT_CARD_IS_NOT_PRESET: 'MB_CREDIT_CARD_IS_NOT_PRESET',
    DELETE_SUCCESS: 'DELETE_SUCCESS',
    HAD_REFERENCE_AND_PAID: 'HAD_REFERENCE_AND_PAID',
    HAD_REFERENCE: 'HAD_REFERENCE',
    HAD_PAID: 'HAD_PAID',
    RSI_ERROR: 'RSI_ERROR',
    PPINVOICE_NOT_FOUND: 'PPINVOICE_NOT_FOUND',
    PPINVOICE_USED: 'PPINVOICE_USED',
    IS_FIST_ITEM: 'IS_FIST_ITEM',
    IS_LAST_ITEM: 'IS_LAST_ITEM',
    NO_BOOK_NULL: 'NO_BOOK_NULL',
    NO_BOOK_RSI_NULL: 'NO_BOOK_RSI_NULL',
    NO_BOOK_OTHER_NULL: 'NO_BOOK_OTHER_NULL',
    NOTHING: 'NOTHING',
    STOCK_TRUE: 'STOCK_TRUE',
    STOCK_FALSE: 'STOCK_FALSE',
    QUANTITY_RECEIPT_GREATER_THAN: 'QUANTITY_RECEIPT_GREATER_THAN',
    NOT_FOUND: 'NOT_FOUND',
    DUPLICATE_PP_SERVICE: 'updateReceipt24',
    DUPLICATE_MC_PAYMENT: 'updateReceipt11',
    DUPLICATE_PAYMENT_ORDER: 'updateReceipt12',
    DUPLICATE_TRANSFER_SEC: 'updateReceipt13',
    DUPLICATE_CASH_SEC: 'updateReceipt14',
    DUPLICATE_CREDIT_CARD: 'updateReceipt17',
    SUCCESS: 'SUCCESS',
    DUPLICATE: 'DUPLICATE',
    DUPLICATENO_OTHER: 'DUPLICATENO_OTHER',
    NO_VOUCHER_LIMITED: 'noVoucherLimited',
    DETAIL_CODE_INVALID: 'detailCodeInvalid',
    POSTDATE_INVALID: 'postdateInvalid',
    COST_SET_USED: 'COST_SET_USED'
};
