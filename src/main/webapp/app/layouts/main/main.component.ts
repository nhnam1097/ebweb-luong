import { AfterViewChecked, AfterViewInit, Component, HostListener, OnInit } from '@angular/core';
import { Router, ActivatedRouteSnapshot, NavigationEnd } from '@angular/router';

import { JhiLanguageHelper, LoginService, Principal } from 'app/core';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
// import { BaseComponent } from 'app/shared/base-component/base.component';
import { BROADCAST_EVENT, HOST } from 'app/app.constants';
import { HttpClient, HttpResponse } from '@angular/common/http';
// import * as $ from 'jquery';
import * as moment from 'moment';

@Component({
    selector: 'eb-main',
    templateUrl: './main.component.html'
})
export class EbMainComponent implements OnInit {
    eventSubscriberChangeSession: Subscription;
    isNavbarCollapsed: boolean;
    eventSubscriber: Subscription;
    disableUserSelect: boolean;
    isCustomLogin: any;

    constructor(
        private jhiLanguageHelper: JhiLanguageHelper,
        public router: Router,
        private principal: Principal,
        private eventManager: JhiEventManager,
        private http: HttpClient,
        private loginService: LoginService
    ) {
        // super();
    }

    private getPageTitle(routeSnapshot: ActivatedRouteSnapshot) {
        let title = routeSnapshot.data && routeSnapshot.data['pageTitle'] ? routeSnapshot.data['pageTitle'] : 'ebwebApp';
        if (routeSnapshot.firstChild) {
            title = this.getPageTitle(routeSnapshot.firstChild) || title;
        }
        return title;
    }

    ngOnInit() {
        if (window.location.href.includes('ketoantructuyen.com') && window.location.href.includes('/login')) {
            this.isCustomLogin = true;
        } else {
            this.isCustomLogin = false;
        }
        if (!sessionStorage.getItem('ipComputer')) {
            this.http.get('https://api.ipify.org?format=json').subscribe(data => {
                sessionStorage.setItem('ipComputer', JSON.stringify(data));
            });
        }
        this.registerChangeSession();
        this.router.events.subscribe(event => {
            if (event instanceof NavigationEnd) {
                this.jhiLanguageHelper.updateTitle(this.getPageTitle(this.router.routerState.snapshot.root));
            }
        });
        this.principal.identity().then(account => {
            if (!account) {
                if (this.router.url.includes('/admin/login')) {
                    this.router.navigate(['/admin/login']);
                } else {
                    this.router.navigate(['/login']);
                }
            }
            if (account && account.ebUserPackage.expriredDate) {
                const endD = moment(account.ebUserPackage.expriredDate);
                const nowD = moment(Date.now());
                if (nowD.isAfter(endD, 'day')) {
                    this.loginService.logout();
                    this.router.navigate(['/login']);
                }
            }
        });
        this.isNavbarCollapsed = true;
        this.disableUserSelect = false;
        this.eventSubscriberChangeSession = this.eventManager.subscribe(BROADCAST_EVENT.DISABLE_USER_SELECT, response => {
            this.disableUserSelect = response.content;
        });
        this.registerSystemOption();
        // this.eventSubscriberChangeSession = this.eventManager.subscribe('checkEnter', res => {
        //     this.isEnter = res.content;
        // });
    }

    registerSystemOption() {
        this.eventSubscriber = this.eventManager.subscribe('newSystemOption', response => {
            // this.isEnter = response.content.systemOption.filter(n => n.code === 'TCKHAC_Enter')[0].data === '1';
        });
    }

    registerChangeSession() {
        this.eventSubscriberChangeSession = this.eventManager.subscribe('changeSession', response => {
            this.router.navigate(['/']);
        });

        this.eventSubscriberChangeSession = this.eventManager.subscribe('toggleNavbar', response => {
            this.isNavbarCollapsed = response.content;
        });
    }

    moveNext($event) {
        const inputs = document.getElementsByTagName('input');
        const selects = document.getElementsByTagName('select');
        const is = document.getElementsByTagName('i');
        const curIndex = $event.path['0'].tabIndex;
        let nextIndex = curIndex;
        let j = 0;
        let isFound = false;
        // for (let i = 0; i < inputs.length; i++) {
        //     // loop over the tabs.
        //     if (inputs[i].tabIndex === curIndex && !inputs[i].disabled) {
        //
        //     }
        // }
        const currentPosition = $event.path['0'].selectionEnd ? $event.path['0'].selectionEnd : 0;
        const lengthPosition = $event.path['0'].value && $event.path['0'].value.length ? $event.path['0'].value.length : 0;
        if (currentPosition === lengthPosition || $event.path['0'].type === 'checkbox') {
            while (j < 15) {
                j++;
                nextIndex = curIndex + j;
                for (let i = 0; i < inputs.length; i++) {
                    // loop over the tabs.
                    if (inputs[i].tabIndex === nextIndex && !inputs[i].disabled && !inputs[i].className.includes('disable')) {
                        // is this our tab?
                        inputs[i].focus(); // Focus and leave.
                        inputs[i].setSelectionRange(0, inputs[i].value.length); // Focus and leave.
                        // if ($event.path['0'].type !== 'checkbox' && inputs[i].type !== 'number') {
                        //     inputs[i].selectionEnd = 0;
                        // }
                        isFound = true;
                        break;
                    }
                }
                for (let i = 0; i < selects.length; i++) {
                    // loop over the tabs.
                    if (selects[i].tabIndex === nextIndex && !selects[i].disabled) {
                        // is this our tab?
                        selects[i].focus(); // Focus and leave.
                        selects[i].setSelectionRange(0, selects[i].length); // Focus and leave.
                        isFound = true;
                        break;
                    }
                }
                for (let k = 0; k < is.length; k++) {
                    // loop over the tabs.
                    if (is[k].tabIndex === nextIndex) {
                        // is this our tab?
                        is[k].focus(); // Focus and leave.
                        isFound = true;
                        break;
                    }
                }
                if (isFound) {
                    break;
                }
            }
        }
    }

    movePrevious($event) {
        const inputs = document.getElementsByTagName('input');
        const selects = document.getElementsByTagName('select');
        const is = document.getElementsByTagName('i');
        const curIndex = $event.path['0'].tabIndex;
        let nextIndex = curIndex;
        let j = 0;
        let isFound = false;
        const currentPosition = $event.path['0'].selectionEnd ? $event.path['0'].selectionEnd : 0;
        if (currentPosition === 0) {
            while (j < 15) {
                j++;
                nextIndex = curIndex - j;
                for (let i = 0; i < inputs.length; i++) {
                    // loop over the tabs.
                    if (inputs[i].tabIndex === nextIndex && !inputs[i].disabled && !inputs[i].className.includes('disable')) {
                        // is this our tab?
                        inputs[i].focus(); // Focus and leave.
                        inputs[i].setSelectionRange(0, inputs[i].value.length); // Focus and leave.
                        // if (inputs[i].type !== 'number' && inputs[i].type !== 'checkbox') {
                        //     inputs[i].selectionEnd = 0;
                        // }

                        isFound = true;
                        break;
                    }
                }
                for (let i = 0; i < selects.length; i++) {
                    // loop over the tabs.
                    if (selects[i].tabIndex === nextIndex && !selects[i].disabled) {
                        // is this our tab?
                        selects[i].focus(); // Focus and leave.
                        selects[i].setSelectionRange(0, selects[i].length); // Focus and leave.
                        isFound = true;
                        break;
                    }
                }
                for (let k = 0; k < is.length; k++) {
                    // loop over the tabs.
                    if (is[k].tabIndex === nextIndex) {
                        // is this our tab?
                        is[k].focus(); // Focus and leave.
                        isFound = true;
                        break;
                    }
                }
                if (isFound) {
                    break;
                }
            }
        }
    }

    closeNotification() {
        if (!sessionStorage.getItem('isShowNotification') && !sessionStorage.getItem('readAll')) {
            this.eventManager.broadcast({
                name: 'closeNotification',
                content: 'closeNotification'
            });
        }
        sessionStorage.removeItem('isShowNotification');
        sessionStorage.removeItem('readAll');
    }
}
