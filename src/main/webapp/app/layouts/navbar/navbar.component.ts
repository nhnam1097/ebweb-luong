import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiLanguageService, JhiParseLinks } from 'ng-jhipster';

import { NGAY_HACH_TOAN, ROLE_ADMIN, SD_SO_QUAN_TRI, SO_LAM_VIEC, VERSION } from 'app/app.constants';
import { JhiLanguageHelper, LoginModalService, LoginService, Principal, UserService } from 'app/core';
import { PasswordService } from 'app/account';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
import { Moment } from 'moment';
import { DatePipe } from '@angular/common';
import { ROLE } from 'app/role.constants';
import { DomSanitizer } from '@angular/platform-browser';
import { Subscription } from 'rxjs';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { DATE_FORMAT, ITEMS_PER_PAGE } from 'app/shared';
import { UtilsService } from 'app/shared/UtilsService/Utils.service';
import { OrganizationUnitService } from 'app/shared/services/organization-unit.service';
import { ToastrService } from 'ngx-toastr';
import { GeneralLedgerService } from 'app/shared/services/general-ledger.service';
import { TreeAccountListItem } from 'app/shared/tree-grid/tree-item';
import { NotificationService } from 'app/shared/notification';
import { InChungTuHangLoatComponent } from 'app/shared/in-chung-tu-hang-loat/in-chung-tu-hang-loat.component';
import { INotification } from 'app/shared/models/notification.model';
import { TypeGroupService } from 'app/shared/ref/type-group.service';
import { ViewVoucherService } from 'app/shared/ref/view-voucher.service';
import { TreeOrganizationUnit } from 'app/shared/models/organization-unit-tree.model';
import { ThongTinDichVuComponent } from 'app/shared/thong-tin-dich-vu';
import { HomeService } from 'app/home/home.service';
import { BaoCaoService } from 'app/shared/services/bao-cao.service';
import { SystemOptionService } from 'app/shared/services/system-option.service';

import * as Pusher from 'pusher-js';
// declare const Pusher: any;
@Component({
    selector: 'eb-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['navbar.css']
})
export class NavbarComponent implements OnInit {
    inProduction: boolean;
    isNavbarCollapsed: boolean;
    languages: any[];
    swaggerEnabled: boolean;
    modalRef: NgbModalRef;
    version: string;
    currentAccount: any;
    data: any;
    org: TreeAccountListItem;
    book: string;
    isShow: boolean;
    isShowIsDependent: boolean;
    ngbModalRef: any;
    ngbModalRef2: any;
    confirmPassword: string;
    currentPassword: string;
    newPassword: string;
    postedDate: Moment;
    isAdmin: boolean;
    isAdminLoginRoute: boolean;
    logOutAll: boolean;
    listTutorial: any[];
    listTutorialNotChange: any[];
    listTHead = [];
    listKey: any[];
    listSearch: any;
    keySearch: any[];
    eventSubscriber: Subscription;
    listParentTutorial: any[];
    urlImage: any;
    // Role
    ROLE = ROLE;
    ROLE_HETHONG = ROLE.HeThong;
    ROLE_TIENICH = ROLE.TienIch;
    ROLE_BAOCAO = ROLE.BaoCao;
    ROLE_DANHMUC = ROLE.DanhMuc;
    ROLE_MGT = ROLE.ROLE_MGT;
    ROLE_DanhMucKhachHang = ROLE.DanhMucKhachHang;
    ROLE_DanhMucNhaCungCap = ROLE.DanhMucNhaCungCap;
    ROLE_DanhMucNhanVien = ROLE.DanhMucNhanVien;
    ROLE_DanhMucKhachHangVaNhaCungCap = ROLE.DanhMucKhachHangVaNhaCungCap;
    ROLE_DanhMucKho = ROLE.DanhMucKho;
    ROLE_DanhMucDonViTinh = ROLE.DanhMucDonViTinh;
    ROLE_DanhMucLoaiVatTuHangHoa = ROLE.DanhMucLoaiVatTuHangHoa;
    ROLE_DanhMucTaiKhoanNganHang = ROLE.DanhMucTaiKhoanNganHang;
    ROLE_DanhMucDinhKhoanTuDong_Xem = ROLE.DanhMucDinhKhoanTuDong_Xem;
    ROLE_DanhMucCongCuDungCu_Xem = ROLE.DanhMucCongCuDungCu_Xem;
    ROLE_DanhMucVatTuHangHoa_Xem = ROLE.DanhMucVatTuHangHoa_Xem;
    ROLE_DanhMucNganHang_Xem = ROLE.DanhMucNganHang_Xem;
    ROLE_DanhMucTheTinDung_Xem = ROLE.DanhMucTheTinDung_Xem;
    ROLE_DanhMucLoaiTien_Xem = ROLE.DanhMucLoaiTien_Xem;
    ROLE_DanhMucKhoanMucChiPhi_Xem = ROLE.DanhMucKhoanMucChiPhi_Xem;
    ROLE_DanhMucThuChi_Xem = ROLE.DanhMucThuChi_Xem;
    ROLE_DanhMucPhuongThucVanChuyen_Xem = ROLE.DanhMucPhuongThucVanChuyen_Xem;
    ROLE_DanhMucDieuKhoanThanhToan_Xem = ROLE.DanhMucDieuKhoanThanhToan_Xem;
    ROLE_DanhMucMaThongKe_Xem = ROLE.DanhMucMaThongKe_Xem;
    ROLE_DanhMucHHDVChiuThueTTDB_Xem = ROLE.DanhMucHHDVChiuThueTTDB_Xem;
    ROLE_HeThongTaiKhoan_Xem = ROLE.HeThongTaiKhoan_Xem;
    ROLE_TaiKhoanKetChuyen_Xem = ROLE.TaiKhoanKetChuyen_Xem;
    ROLE_TaiKhoanNgamDinh_Xem = ROLE.TaiKhoanNgamDinh_Xem;
    ROLE_CoCauToChuc_Xem = ROLE.CoCauToChuc_Xem;
    ROLE_NgayHachToan_Xem = ROLE.NgayHachToan_Xem;
    ROLE_NgayHachToan_Sua = ROLE.NgayHachToan_Sua;
    ROLE_TuyChon_Xem = ROLE.TuyChon_Xem;
    ROLE_NhatKyTruyCap_Xem = ROLE.NhatKyTruyCap_Xem;
    ROLE_DanhMucDTTHCP = ROLE.DanhMucDTTHCP;
    DanhMucDTTHCP_Xem = ROLE.DanhMucDTTHCP_Xem;
    ROLE_DanhMucKhachHang_Xem = ROLE.DanhMucKhachHang_Xem;
    ROLE_DanhMucNhaCungCap_Xem = ROLE.DanhMucNhaCungCap_Xem;
    ROLE_DanhMucNhanVien_Xem = ROLE.DanhMucNhanVien_Xem;
    ROLE_DanhMucKhachHangVaNhaCungCap_Xem = ROLE.DanhMucKhachHangVaNhaCungCap_Xem;
    ROLE_DanhMucKho_Xem = ROLE.DanhMucKho_Xem;
    ROLE_DanhMucDonViTinh_Xem = ROLE.DanhMucDonViTinh_Xem;
    ROLE_DanhMucLoaiVatTuHangHoa_Xem = ROLE.DanhMucLoaiVatTuHangHoa_Xem;
    ROLE_DanhMucTaiKhoanNganHang_Xem = ROLE.DanhMucTaiKhoanNganHang_Xem;
    ROLE_DanhMucLoaiTSCD_Xem = ROLE.DanhMucLoaiTSCD_Xem;
    ROLE_PSSalaryTaxInsuranceRegulation_Xem = ROLE.QuyDinhLuongThueBaoHiem_Xem;
    ROLE_DanhMucBieuThueTaiNguyen_Xem = ROLE.DanhMucBieuThueTaiNguyen_Xem;
    ROLE_DanhMucBieuThueTNCN_Xem = ROLE.DanhMucBieuThueTNCN_Xem;
    ROLE_DanhMucNhomGiaBan_Xem = ROLE.DanhMucNhomGiaBan_Xem;
    ROLE_InChungTuHangLoat_Xem = ROLE.InChungTuHangLoat_Xem;
    ROLE_InChungTuHangLoat_In = ROLE.InChungTuHangLoat_In;
    ROLE_CauHinhGiaoDienNhapLieu_Xem = ROLE.CauHinhGiaoDienNhapLieu_Xem;
    soLamViecDuocPhanQuyen: any;
    isRoleUser: boolean;
    isRecordedSysLog: boolean;
    index: number;
    url: any;
    selectedRow: any;
    isShowNotification: any;
    listNotifications: INotification[];
    countNotiNonRead: any;

    recorded: any;
    status: any;
    fromDate;
    any;
    toDate: any;
    typeGroups: any;
    typeGroup: any;
    typeSearch: number;
    viewVouchers: any[];
    no: any;
    invoiceNo: any;
    totalItems: number;
    page: number;
    itemsPerPage = ITEMS_PER_PAGE;
    links: any;
    queryCount: any;
    newList: any[];
    timeLineVoucher: any;
    listTimeLine: any;
    timeLine: any;
    objTimeLine: { dtBeginDate?: string; dtEndDate?: string };
    account: any;
    typePrintForm: number;
    listTypePrintForm: any[];
    listType: any[];
    valueVoucher: number;
    isReadAll: any;
    listYearWork: any[];
    yearWork: any;
    isDependent: boolean;
    // @ts-ignore
    private pusherClient: Pusher;
    isCustomNavbar: boolean;
    isLoadingYearWork: any;

    constructor(
        private loginService: LoginService,
        private languageService: JhiLanguageService,
        private languageHelper: JhiLanguageHelper,
        private principal: Principal,
        private loginModalService: LoginModalService,
        public router: Router,
        private eventManager: JhiEventManager,
        private modalService: NgbModal,
        private orgService: OrganizationUnitService,
        private userService: UserService,
        private passwordService: PasswordService,
        private translateService: TranslateService,
        private toastrService: ToastrService,
        private systemOptionService: SystemOptionService,
        private datepipe: DatePipe,
        private baoCaoService: BaoCaoService,
        private translate: TranslateService,
        public homeService: HomeService,
        public sanitize: DomSanitizer,
        private typeGroupService: TypeGroupService,
        private viewVoucherNoService: ViewVoucherService,
        private parseLinks: JhiParseLinks,
        public utilsService: UtilsService,
        private generalLedgerService: GeneralLedgerService,
        private notificationService: NotificationService
    ) {
        this.version = VERSION ? 'v' + VERSION : '';
        this.isNavbarCollapsed = true;
        // @ts-ignore
        this.pusherClient = new Pusher('dca14493bc4534da0c13', { cluster: 'ap1' });
        /*this.router.events.subscribe((routerEvent: Event) => {
            if (routerEvent instanceof NavigationStart) {
                this.showLoading = true;
            }

            if (routerEvent instanceof NavigationEnd) {
                this.showLoading = false;
            }
        });*/
        this.isAdminLoginRoute = window.location.href.includes('/admin/login');
    }

    ngOnInit() {
        this.isLoadingYearWork = false;
        if (window.location.href.includes('tt200.ketoantructuyen.com') || window.location.href.includes('tt133.ketoantructuyen.com')) {
            this.isCustomNavbar = true;
        } else {
            this.isCustomNavbar = false;
        }
        this.data = [];
        this.listYearWork = [];
        this.languageHelper.getAll().then(languages => {
            this.languages = languages;
        });
        // @ts-ignore
        Pusher.logToConsole = true;
        // @ts-ignore
        const pusher = new Pusher('dca14493bc4534da0c13', {
            cluster: 'ap1'
        });

        const channel = pusher.subscribe('my-channel');
        channel.bind('my-event', data => {
            // this.toastrService.success(data.message, 'Bạn có 1 thông báo mới!');
            this.countNotiNonRead = this.countNotiNonRead ? this.countNotiNonRead + 1 : 1;
        });
        this.index = 0;
        this.principal.identity().then(account => {
            this.currentAccount = account;
            if (this.currentAccount) {
                this.isShow = this.currentAccount.systemOption.some(x => x.code === SD_SO_QUAN_TRI && x.data === '1');
                const data = this.currentAccount.systemOption.find(x => x.code === NGAY_HACH_TOAN && x.data);
                if (data) {
                    // this.postedDate = moment(data.data, DATE_FORMAT_TYPE1);
                }
                this.isAdmin = this.currentAccount.authorities.includes(ROLE_ADMIN);
                this.notificationService.getNotificationsByUser({ userID: account.id }).subscribe((res: HttpResponse<INotification[]>) => {
                    this.isShowNotification = false;
                    this.listNotifications = res.body;
                    for (let i = 0; i < this.listNotifications.length; i++) {
                        this.listNotifications[i].timeNoti = this.getTimeNoti(this.listNotifications[i].startDate);
                    }
                    this.countNotiNonRead = this.listNotifications.filter(a => !a.isRead).length;
                    if (this.countNotiNonRead > 10) {
                        this.countNotiNonRead = '9+';
                    }
                });
            }
        });

        this.registerAuthenticationSuccess();
        this.registerChangeListSearch();
        this.registerSelectedRow();
        this.registerLoadData();
        this.registerCountNoti();
        this.registerCloseNotification();
        this.eventManager.subscribe('changeUserInfo', res => {
            this.currentAccount.fullName = res.content;
        });
    }

    registerAuthenticationSuccess() {
        this.eventManager.subscribe('authenticationSuccess', message => {
            this.principal.identity().then(account => {
                this.currentAccount = account;
            });
        });
    }

    changeLanguage(languageKey: string) {
        this.languageService.changeLanguage(languageKey);
    }

    collapseNavbar() {
        this.isNavbarCollapsed = true;
    }

    isAuthenticated() {
        return this.principal.isAuthenticated();
    }

    login() {
        this.modalRef = this.loginModalService.open();
    }

    logout() {
        this.collapseNavbar();
        this.recordSysLogForLogout();
        this.loginService.logout();
        // this.removeSession();
        this.utilsService.navigateToOtherSite('login');
    }

    recordSysLogForLogout() {
        this.homeService
            .recordSysLog({
                isLogin: false
            })
            .subscribe(
                (res: HttpResponse<any>) => {
                    this.isRecordedSysLog = res.body;
                },
                (res: HttpErrorResponse) => {}
            );
    }

    toggleNavbar() {
        this.isNavbarCollapsed = !this.isNavbarCollapsed;
        this.eventManager.broadcast({
            name: 'toggleNavbar',
            content: this.isNavbarCollapsed
        });
    }

    getImageUrl() {
        return this.isAuthenticated() ? this.principal.getImageUrl() : null;
    }

    showPopup(popup) {
        this.principal.identity(true).then(account => {
            this.currentAccount = account;
            this.yearWork = account.yearWork;
            this.isDependent = account.isDependent;
            this.isRoleUser = !this.currentAccount.authorities.includes(ROLE.ROLE_PERMISSION);
            if (this.currentAccount) {
                this.isShow = this.currentAccount.systemOption.some(x => x.code === SD_SO_QUAN_TRI && x.data === '1');
            }
        });
        this.orgService.getOuTree().subscribe(res => {
            this.data = res.body.orgTrees;
            this.book = res.body.book;
            this.soLamViecDuocPhanQuyen = res.body.bookUser;
            this.org = res.body.currentOrgLogin;
            this.listYearWork = [];
            this.generalLedgerService
                .findDataYearWork({ companyID: this.org.parent.id, userID: this.currentAccount.id })
                .subscribe(res2 => {
                    const listYear = res2.body.listYear;
                    this.isDependent = res2.body.isDependent;
                    this.isShowIsDependent = res2.body.isShowIsDependent;
                    this.listYearWork.push({ data: 1, year: 'Tất cả' });
                    this.yearWork = res2.body.yearWork;
                    if (!this.yearWork) {
                        this.yearWork = 1;
                    }
                    if (listYear && listYear.length > 0) {
                        for (let i = 0; i < listYear.length; i++) {
                            this.listYearWork.push({ data: listYear[i], year: listYear[i] });
                        }
                    }
                });
            if (this.ngbModalRef) {
                this.ngbModalRef.close();
            }
            this.ngbModalRef = this.modalService.open(popup, { size: 'lg', backdrop: 'static' });
        });
    }

    showChangePostedDate(popupChangePostedDate) {
        this.principal.identity(true).then(account => {
            this.currentAccount = account;
            if (this.currentAccount) {
                const defaultData = moment(account.systemOption.find(x => x.code === NGAY_HACH_TOAN).defaultData, DATE_FORMAT);
                const now = moment(new Date());
                const data = moment(account.systemOption.find(x => x.code === NGAY_HACH_TOAN).data, DATE_FORMAT);
                if (this.datepipe.transform(now, 'dd/MM/yyyy') === this.datepipe.transform(defaultData, 'dd/MM/yyyy')) {
                    this.postedDate = data;
                } else {
                    this.postedDate = now;
                }
                // this.postedDate = moment(ngayHachToan);
                // this.testPostedDate = '11/11/2019'
                this.ngbModalRef = this.modalService.open(popupChangePostedDate, { size: 'lg', backdrop: 'static' });
            }
        });
    }

    showTutorialUse(popupTutorialUse) {
        this.listTutorial = [];
        this.listTutorialNotChange = [];
        this.url = '';
        this.notificationService.getHelpTable().subscribe((res: HttpResponse<any[]>) => {
            this.listTutorialNotChange = res.body;
            this.listTutorial = this.listTutorialNotChange.map(object => ({ ...object }));
            this.listParentTutorial = [];
            this.listSearch = {
                chucNang: '',
                videoTutorials: ''
            };
            const listAccount = this.listTutorial.filter(a => a.grade === 1);
            for (let i = 0; i < listAccount.length; i++) {
                this.listParentTutorial.push(Object.assign({}));
                this.listParentTutorial[i].parent = listAccount[i];
            }
            this.tree(this.listParentTutorial, 1);
            this.setIndexTree(this.listParentTutorial, 1);

            this.keySearch = [];
            this.keySearch.push('chucNang');
            // this.keySearch.push('linkVideo');

            this.listTHead = [];
            this.listTHead.push('navbar.tutorialUse.function');
            // this.listTHead.push('navbar.tutorialUse.videoTutorials');

            this.listKey = [];
            this.listKey.push({ key: 'chucNang', type: 1 });
            // this.listKey.push({ key: 'linkVideo', type: 4 });
        });
        this.ngbModalRef = this.modalService.open(popupTutorialUse, {
            size: 'sm',
            backdrop: 'static',
            windowClass: 'width-60'
        });
    }

    showPopupChangePassword(popup) {
        this.logOutAll = false;
        this.confirmPassword = this.currentPassword = this.newPassword = '';
        this.ngbModalRef2 = this.modalService.open(popup, { size: 'lg', backdrop: 'static' });
    }

    changeOrg(org) {
        if (org) {
            this.isLoadingYearWork = true;
            this.systemOptionService.getSystemOptionsByCompanyID({ companyID: org.parent.id }).subscribe(res => {
                this.isShow = res.body.some(x => x.code === SD_SO_QUAN_TRI && x.data === '1');
                this.book = res.body.find(x => x.code === SO_LAM_VIEC).data;
                if (this.isRoleUser) {
                    this.userService
                        .getCurrentBookOfUser({
                            companyID: org.parent.id
                        })
                        .subscribe(resD => {
                            if (this.isShow) {
                                this.soLamViecDuocPhanQuyen = resD.body;
                            }
                        });
                }
            });
            this.userService
                .getYearWorkAndIsDependent({
                    companyID: org.parent.id,
                    userID: this.currentAccount.id,
                    isDependent: this.isDependent
                })
                .subscribe(res => {
                    this.yearWork = res.body.yearWork;
                    this.isDependent = res.body.isDependent;
                });
            this.generalLedgerService.findDataYearWork({ companyID: org.parent.id, userID: this.currentAccount.id }).subscribe(res => {
                this.listYearWork = [];
                const listYear = res.body.listYear;
                this.isDependent = res.body.isDependent;
                this.isShowIsDependent = res.body.isShowIsDependent;
                this.listYearWork.push({ data: 1, year: 'Tất cả' });
                this.yearWork = res.body.yearWork;
                if (!this.yearWork) {
                    this.yearWork = 1;
                }
                for (let i = 0; i < listYear.length; i++) {
                    this.listYearWork.push({ data: listYear[i], year: listYear[i] });
                }
                this.isLoadingYearWork = false;
            });
        }
    }

    afterChangePass() {
        this.baoCaoService.changeCompany();
        const book = this.currentAccount.systemOption.find(x => x.code === SO_LAM_VIEC).data;
        const companyID = this.currentAccount.organizationUnit.id;
        this.userService.updateSession({ currentBook: book, org: companyID }).subscribe(res => {
            const bearerToken = res.headers.get('Authorization');
            if (bearerToken && bearerToken.slice(0, 7) === 'Bearer ') {
                const jwt = bearerToken.slice(7, bearerToken.length);
                this.loginService.storeAuthenticationToken(jwt, false);
                this.loginService.storeAuthenticationToken(jwt, true);
            }

            this.principal.identity(true).then(account => {
                this.currentAccount = account;
                if (this.currentAccount) {
                    this.isShow = this.currentAccount.systemOption.some(x => x.code === SD_SO_QUAN_TRI && x.data === '1');
                }
                this.eventManager.broadcast({
                    name: 'changeSession',
                    content: 'Change Authentication Session Success'
                });
            });
            this.ngbModalRef.close();
        });
    }

    apply() {
        if (this.org) {
            // update dữ liệu
            // this.baoCaoService.changeCompany();
            // if (this.yearWork && this.yearWork === 1) {
            //     this.yearWork = null;
            // }
            if (!this.isLoadingYearWork) {
                this.userService
                    .updateSession({
                        currentBook: this.book,
                        org: this.org.parent.id,
                        yearWork: this.yearWork,
                        isDependent: this.isDependent
                    })
                    .subscribe(res => {
                        const bearerToken = res.headers.get('Authorization');
                        if (bearerToken && bearerToken.slice(0, 7) === 'Bearer ') {
                            const jwt = bearerToken.slice(7, bearerToken.length);
                            this.loginService.storeAuthenticationToken(jwt, false);
                            this.loginService.storeAuthenticationToken(jwt, true);
                        }
                        this.principal.identity(true).then(account => {
                            this.currentAccount = account;
                            if (this.currentAccount) {
                                this.isShow = this.currentAccount.systemOption.some(x => x.code === SD_SO_QUAN_TRI && x.data === '1');
                            }
                            this.eventManager.broadcast({
                                name: 'changeSession',
                                content: 'Change Authentication Session Success'
                            });
                        });
                        this.ngbModalRef.close();
                    });
            }
        } else {
            this.toastrService.error(this.translateService.instant('navbar.changePostedDate.error.org'));
        }
    }

    changePassword() {
        if (!this.newPassword) {
            this.toastrService.error(this.translateService.instant('navbar.password.error.newPassword'));
            return;
        }
        if (!this.confirmPassword) {
            this.toastrService.error(this.translateService.instant('navbar.password.error.newPassword'));
            return;
        }
        if (!this.currentPassword) {
            this.toastrService.error(this.translateService.instant('navbar.password.error.newPassword'));
            return;
        }
        if (this.newPassword.length < 6) {
            this.toastrService.error(
                this.translateService.instant('userManagement.error.passwordLength'),
                this.translateService.instant('ebwebApp.ebPackage.error.error')
            );
            return;
        }
        if (this.newPassword === this.currentPassword) {
            this.toastrService.error(this.translateService.instant('userManagement.error.samePassword'));
            this.newPassword = '';
            this.confirmPassword = '';
            return;
        }
        if (this.newPassword !== this.confirmPassword) {
            this.toastrService.error(this.translateService.instant('navbar.password.error.doNotMatch'));
        } else {
            this.passwordService.save(this.newPassword, this.currentPassword, this.logOutAll ? this.logOutAll : false).subscribe(
                () => {
                    this.toastrService.success(this.translateService.instant('navbar.password.success'));
                    if (this.logOutAll) {
                        window.location.reload();
                    }
                    this.ngbModalRef2.close();
                },
                error => {
                    if (error.error.type.includes('invalid-password')) {
                        this.toastrService.error(this.translateService.instant('navbar.password.error.invalidPassword'));
                    }
                }
            );
        }
    }

    updatePostedDate() {
        let data = '';
        let defaultData = '';
        const now = new Date();
        if (!this.currentAccount.yearWork) {
            defaultData = this.datepipe.transform(new Date(), 'yyyy-MM-dd');
        } else {
            defaultData = this.datepipe.transform(new Date(this.currentAccount.yearWork, now.getMonth(), now.getDate()), 'yyyy-MM-dd');
        }
        if (!this.postedDate) {
            this.toastrService.error(
                this.translateService.instant('navbar.changePostedDate.postedDateIsNotNull'),
                this.translateService.instant('navbar.changePostedDate.message')
            );
            return;
        } else {
            data = this.datepipe.transform(this.postedDate, 'yyyy-MM-dd');
        }
        const obj = { data, defaultData };
        this.systemOptionService.updatePostedDate(JSON.stringify(obj)).subscribe(
            () => {
                this.toastrService.success(this.translateService.instant('navbar.changePostedDate.success'));
                this.principal.identity(true).then(account => {});
                this.ngbModalRef.close();
            },
            () => {}
        );
    }

    navigateUser() {
        this.utilsService.navigateToOtherSite('quan-tri-nguoi-dung/thong-tin-tai-khoan');
    }

    getInstructionForUse() {
        // this.baoCaoService.getInstruction();
    }

    toolSign() {
        window.open('https://easyinvoice.vn/tai-nguyen/', '_blank');
    }

    removeSession() {
        sessionStorage.removeItem('searchBangCanDoiKeToan');
        sessionStorage.removeItem('searchBangCanDoiTaiKhoan');
        sessionStorage.removeItem('searchHoatDongKinhDoanh');
        sessionStorage.removeItem('searchTienTeTT');
        sessionStorage.removeItem('searchTienTeGT');
        sessionStorage.removeItem('searchBaoCaoTaiChinh');
        sessionStorage.removeItem('searchSoChiTietVatLieu');
        sessionStorage.removeItem('searchBangTongHopChiTietVatLieu');
        sessionStorage.removeItem('searchTheKho');
        sessionStorage.removeItem('searchTongHopTonKho');
        sessionStorage.removeItem('searchSoQuyTienMat');
        sessionStorage.removeItem('searchSoKeToanChiTietQuyTienMat');
        sessionStorage.removeItem('searchBangKeSoDuNganHang');
        sessionStorage.removeItem('searchSoTienGuiNganHang');
        sessionStorage.removeItem('searchSoChiPhiSXKD');
        sessionStorage.removeItem('searchSoTongHopLuongNhanVien');
        sessionStorage.removeItem('searchSoTheoDoiThanhToanVoiNguoiMuaNguoiBan');
        sessionStorage.removeItem('lichSuTruyCapSession');
        sessionStorage.removeItem('searchReportSession');
        sessionStorage.removeItem('searchSoTongHopCacKhoanBHTheoNhanVien');
        sessionStorage.removeItem('searchSoNhatKyBanHang');
        sessionStorage.removeItem('searchSoNhatKyMuaHang');
        sessionStorage.removeItem('searchDoiChieuLenhLapRapThaoDoVaXuatNhapKho');
        sessionStorage.removeItem('searchBangKeBanRa');
        sessionStorage.removeItem('searchBangKeBanRaQT');
        sessionStorage.removeItem('searchBangKeMuaVao');
        sessionStorage.removeItem('searchBangKeMuaVaoQT');
        sessionStorage.removeItem('searchDoiChieuTaiSanVaSoCai');
        // sessionStorage.removeItem('ipComputer');
    }

    showPopupServiceInfo() {
        const modalRef = this.modalService.open(ThongTinDichVuComponent as Component, {
            backdrop: 'static',
            windowClass: 'width-1000'
        });
    }

    showInChungTuHangLoat() {
        if (this.ROLE_InChungTuHangLoat_Xem) {
            this.ngbModalRef = this.modalService.open(InChungTuHangLoatComponent as Component, {
                size: 'lg',
                backdrop: 'static',
                windowClass: 'width-80'
            });
        }
    }

    checkChildren(treeOrganizationUnit: TreeOrganizationUnit): boolean {
        if (treeOrganizationUnit && treeOrganizationUnit.children && treeOrganizationUnit.children.length > 0) {
            for (let i = 0; i < treeOrganizationUnit.children.length; i++) {
                if (
                    treeOrganizationUnit.children[i].parent.accType === 0 &&
                    treeOrganizationUnit.children[i].parent.unitType === 1 &&
                    treeOrganizationUnit.children[i].parent.parentID === this.org.parent.id
                ) {
                    return true;
                }
            }
        }
        return false;
    }

    registerChangeListSearch() {
        this.eventSubscriber = this.eventManager.subscribe('listSearch', response => {
            this.listSearch = response.data;
            this.listParentTutorial = [];
            this.listTutorial = this.listTutorialNotChange.filter(a =>
                a.chucNang.toUpperCase().includes(this.listSearch.chucNang.toUpperCase())
            );
            this.treeforSearch();
            let grade;
            if (this.listTutorial.length) {
                grade = this.listTutorial[0].grade;
            }
            for (let i = 0; i < this.listTutorial.length; i++) {
                if (this.listTutorial[i].grade < grade) {
                    grade = this.listTutorial[i].grade;
                }
            }
            const listAccount = this.listTutorial.filter(a => a.grade === grade);
            for (let i = 0; i < listAccount.length; i++) {
                this.listParentTutorial.push(Object.assign({}));
                this.listParentTutorial[i].parent = listAccount[i];
            }
            this.tree(this.listParentTutorial, grade);
            this.setIndexTree(this.listParentTutorial, grade);
        });
    }

    tree(accountList: any[], grade) {
        for (let i = 0; i < accountList.length; i++) {
            const newList = this.listTutorial.filter(a => a.parentID === accountList[i].parent.id);
            accountList[i].children = [];
            for (let j = 0; j < newList.length; j++) {
                // if (j === 0) {
                //     accountList[i].children = [];
                // }
                accountList[i].children.push(Object.assign({}));
                accountList[i].children[j].parent = newList[j];
            }
            if (accountList[i].children && accountList[i].children.length > 0) {
                this.tree(accountList[i].children, grade + 1);
            }
        }
    }

    treeforSearch() {
        for (let i = 0; i < this.listTutorial.length; i++) {
            if (this.listTutorial[i].parentID) {
                this.getAllParent(this.listTutorial[i]);
            }
        }
    }

    setIndexTree(accountList: any[], grade) {
        for (let i = 0; i < accountList.length; i++) {
            accountList[i].index = this.index;
            this.index++;
            if (accountList[i].children && accountList[i].children.length > 0) {
                this.setIndexTree(accountList[i].children, grade + 1);
            }
        }
    }

    getAllParent(accountList: any) {
        const addAccount = this.listTutorialNotChange.find(a => a.id === accountList.parentID);
        const checkExistAccount = this.listTutorial.find(a => a.id === addAccount.id);
        if (addAccount && !checkExistAccount) {
            this.listTutorial.push(addAccount);
            if (addAccount.parentID) {
                this.getAllParent(addAccount);
            }
        }
    }

    registerSelectedRow() {
        this.eventSubscriber = this.eventManager.subscribe('selectRow', response => {
            this.selectedRow = response.data;
            this.url = this.selectedRow.parent.linkVideo;
            if (this.url) {
                this.url = this.sanitize.bypassSecurityTrustResourceUrl(this.url.replaceAll('watch?v=', 'embed/'));
            }
        });
    }

    openNotification(isShowNotification) {
        this.isShowNotification = isShowNotification;
        if (this.isShowNotification) {
            sessionStorage.setItem('isShowNotification', JSON.stringify(true));
            this.notificationService
                .getNotificationsByUser({ userID: this.currentAccount.id })
                .subscribe((res: HttpResponse<INotification[]>) => {
                    this.listNotifications = res.body;
                    for (let i = 0; i < this.listNotifications.length; i++) {
                        this.listNotifications[i].timeNoti = this.getTimeNoti(this.listNotifications[i].startDate);
                    }
                    this.countNotiNonRead = 0;
                    this.isReadAll = false;
                });
            this.notificationService
                .getNotificationsByUser({ userID: this.currentAccount.id })
                .subscribe((res: HttpResponse<INotification[]>) => {
                    this.listNotifications = res.body;
                    this.countNotiNonRead = this.listNotifications.filter(a => !a.isRead).length;
                });
        }
    }

    showDetailsNotification(detail: any, popup) {
        if (!detail.isRead) {
            this.notificationService
                .updateStatusNoti({
                    isReadAll: false,
                    userID: this.currentAccount.id,
                    image: detail.image,
                    isRead: detail.isRead
                })
                .subscribe(async (res: HttpResponse<any>) => {
                    const height = await this.renderImage(res.body);
                    detail.isRead = true;
                    this.ngbModalRef = this.modalService.open(popup, {
                        backdrop: 'static',
                        windowClass: height < 800 ? 'width-notification' : 'width-notification-custom'
                    });
                });
        }
        this.urlImage = '../../../content/images/notification/' + detail.image + '.png';
    }

    renderImage(res?) {
        return new Promise(resolve => {
            if (res) {
                const reader = new FileReader();
                reader.onload = e => {
                    this.urlImage = e.target['result'];
                    const img = new Image();
                    // @ts-ignore
                    img.src = this.urlImage;
                    img.addEventListener('load', function() {
                        resolve(img.height);
                    });
                };
                reader.readAsDataURL(new Blob([res], { type: 'image/png' }));
            }
        });
    }

    readAllNotification() {
        sessionStorage.setItem('readAll', JSON.stringify(true));
        const listNonRead = this.listNotifications.filter(a => !a.isRead);
        if (listNonRead && listNonRead.length > 0) {
            this.notificationService
                .updateStatusNoti({ isReadAll: true, userID: this.currentAccount.id, image: null, isRead: false })
                .subscribe((res: HttpResponse<any>) => {
                    for (let i = 0; i < this.listNotifications.length; i++) {
                        this.listNotifications[i].isRead = true;
                    }
                });
        }
    }

    getTimeNoti(startDate?: any) {
        const currentYear = new Date().getFullYear();
        const currentMonth = new Date().getMonth();
        const currentDay = new Date().getDate();
        const currentHours = new Date().getHours();
        const startYear = new Date(startDate).getFullYear();
        const startMonth = new Date(startDate).getMonth();
        const startDay = new Date(startDate).getDate();
        const startHours = new Date(startDate).getHours();
        if (currentYear === startYear && currentMonth === startMonth) {
            if (currentDay <= startDay + 1) {
                if (currentDay > startDay) {
                    if (currentHours - startHours < 0) {
                        return (currentHours + (24 - startHours) + ' giờ trước').toString();
                    }
                } else if (currentDay === startDay) {
                    if (currentHours === startHours) {
                        const currentTime = new Date().getMinutes();
                        const startTime = new Date(startDate).getMinutes();
                        return (currentTime - startTime + ' phút trước').toString();
                    } else {
                        return (currentHours - startHours + ' giờ trước').toString();
                    }
                }
            }
        }
        return this.datepipe.transform(startDate, 'dd/MM/yyyy').toString();
    }

    registerLoadData() {
        this.eventSubscriber = this.eventManager.subscribe('loadNotification', response => {
            this.notificationService
                .getNotificationsByUser({ userID: this.currentAccount.id })
                .subscribe((res: HttpResponse<INotification[]>) => {
                    this.listNotifications = res.body;
                    this.countNotiNonRead = this.listNotifications.filter(a => !a.isRead).length;
                    if (this.countNotiNonRead > 10) {
                        this.countNotiNonRead = '9+';
                    }
                });
        });
    }

    registerCloseNotification() {
        this.eventSubscriber = this.eventManager.subscribe('closeNotification', response => {
            this.isShowNotification = false;
        });
    }

    registerCountNoti() {
        this.eventSubscriber = this.eventManager.subscribe('countNoti', response => {
            this.countNotiNonRead = parseInt(response.data, 10);
        });
    }

    navigateBaoCao() {
        this.eventManager.broadcast({
            name: 'navigateBaoCao',
            content: 'Navigate BaoCao'
        });
        sessionStorage.removeItem('sessionBC');
        this.utilsService.navigateToOtherSite('bao-cao');
    }

    navigateHelp() {
        this.notificationService.findHelpLink({ id: 'CPLV' }).subscribe((res: HttpResponse<any>) => {
            window.open(res.body, '_blank');
        });
    }

    showCauHinhGiaoDienNhapLieu() {
        if (this.ROLE_CauHinhGiaoDienNhapLieu_Xem) {
            this.utilsService.navigateToOtherSite('cau-hinh-giao-dien-nhap-lieu');
        }
    }
}
