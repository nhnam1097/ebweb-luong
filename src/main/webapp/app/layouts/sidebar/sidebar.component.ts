import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiLanguageService } from 'ng-jhipster';

import { CheckHDCD, EI_IDNhaCungCapDichVu, NCCDV_EINVOICE, SO_LAM_VIEC, TCKHAC_SDTichHopHDDT, VERSION } from 'app/app.constants';
import { ROLE } from 'app/role.constants';
import { JhiLanguageHelper, LoginModalService, LoginService, Principal } from 'app/core';
import { Subscription } from 'rxjs';
import { ThanhToanLuongComponent } from 'app/luong/thanh_toan_luong/thanh-toan-luong.component';
import { NopTienBaoHiemComponent } from 'app/luong/nop_bao_hiem/nop-tien-bao-hiem.component';
import { UtilsService } from 'app/shared/UtilsService/Utils.service';

@Component({
    selector: 'eb-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['sidebar.css']
})
export class SidebarComponent implements OnInit {
    inProduction: boolean;
    isNavbarCollapsed: boolean;
    languages: any[];
    swaggerEnabled: boolean;
    modalRef: NgbModalRef;
    version: string;
    isHover: boolean;
    isInside: boolean;
    isTichHop: boolean; // Add by Hautv
    thuePPTT: boolean; // Add by Hautv
    useInvoiceWait: boolean; // Add by Hautv
    account: any;
    eventSubscriber: Subscription; // Add by Hautv
    // role
    ROLE = ROLE;
    ROLE_TIENMATNGANHANG = ROLE.TienMatNganHang;
    ROLE_HDDT = ROLE.HDDT;
    ROLE_TSCD = ROLE.TSCD;
    ROLE_BANHANG = ROLE.BanHang;
    ROLE_TONGHOP = ROLE.TongHop;
    ROLE_BaoNo_Xem = ROLE.BaoNo_Xem;
    ROLE_BaoCo_Xem = ROLE.BaoCo_Xem;
    ROLE_DanhMucCongCuDungCu_Xem = ROLE.DanhMucCongCuDungCu_Xem;
    ROLE_TheTinDung_Xem = ROLE.TheTinDung_Xem;
    ROLE_DonMuaHang_Xem = ROLE.DonMuaHang_XEM;
    ROLE_MuaHangQuaKho_Xem = ROLE.MuaHangQuaKho_Xem;
    ROLE_QLHD = ROLE.QLHD;
    ROLE_KTHD_XEM = ROLE.KTHD_XEM;
    ROLE_DKSD_XEM = ROLE.DKSD_XEM;
    ROLE_TBPH_XEM = ROLE.TBPH_XEM;
    ROLE_XHD_XEM = ROLE.XHD_XEM;

    ROLE_MCH_XEM = ROLE.MCH_XEM;
    ROLE_NhanHoaDon_Xem = ROLE.NhanHoaDon_Xem;
    ROLE_MuaHang = ROLE.MuaHang;
    ROLE_NhapKho = ROLE.NhapKho;
    ROLE_NhapKho_Xem = ROLE.NhapKho_XEM;
    ROLE_KiemKeKho_Xem = ROLE.KiemKeKho_XEM;
    ROLE_XuatKho = ROLE.XuatKho;
    ROLE_XuatKho_Xem = ROLE.XuatKho_XEM;
    ROLE_ChuyenKho_Xem = ROLE.ChuyenKho_XEM;
    ROLE_LLRTD_XEM = ROLE.LLRTD_XEM;
    ROLE_Kho = ROLE.Kho;
    ROLE_ChungTuGhiSo = ROLE.ChungTuGhiSo_Xem;
    ROLE_GiaThanh = ROLE.GiaThanh;
    ROLE_TheTinDung = ROLE.TheTinDung;
    ROLE_KiemKeQuy = ROLE.KiemKeQuy_Xem;
    ROLE_DoiChieuNganHang = ROLE.DoiChieuNganHang_Xem;
    ROLE_NhanHoaDon = ROLE.NhanHoaDon;
    ROLE_DonDatHang = ROLE.DonDatHang_Xem;
    ROLE_TraTienNhaCungCap = ROLE.TraTienNhaCungCap_Xem;
    ROLE_CTBanHang = ROLE.ChungTuBanHang_Xem;
    ROLE_XuatHoaDon = ROLE.XuatHoaDon_Xem;
    ROLE_HangBanTraLai = ROLE.HangBanTraLai_Xem;
    ROLE_HangBanGiamGia = ROLE.HangBanGiamGia_Xem;
    ROLE_ThuTienKhachHang = ROLE.ThuTienKhachHang_Xem;
    ROLE_ThietLapChinhSachGia = ROLE.ThietLapChinhSachGia_Xem;
    ROLE_TinhGiaXuatKho = ROLE.TinhGiaXuatKho_Xem;
    ROLE_SapXepThuTuNhapXuat = ROLE.SapXepThuTuNhapXuat_Xem;
    ROLE_CapNhatGiaNhapKhoThanhPham = ROLE.CapNhatGiaKhoThanhPham_Xem;
    ROLE_HangMuaTraLai = ROLE.HangMuaTraLai;
    ROLE_HangMuaGiamGia = ROLE.HangMuaGiamGia;
    ROLE_ChungTuNghiepVuKhac_Xem = ROLE.ChungTuNghiepVuKhac_Xem;
    ROLE_KetChuyenLaiLo_Xem = ROLE.KetChuyenLaiLo_Xem;
    ROLE_BaoGia_Xem = ROLE.BaoGia_Xem;
    ROLE_DinhMucNguyenVatLieu_Xem = ROLE.DinhMucNguyenVatLieu_Xem;
    ROLE_DinhMucGiaThanhThanhPham_Xem = ROLE.DinhMucGiaThanhThanhPham_Xem;
    ROLE_DinhMucPhanBoChiPhi_Xem = ROLE.DinhMucPhanBoChiPhi_Xem;
    ROLE_ChiPhiDoDangDauKy_Xem = ROLE.ChiPhiDoDangDauKy_Xem;
    ROLE_ChiPhiTraTruoc_Xem = ROLE.ChiPhiTRaTruoc_Xem;
    ROLE_PhanBoChiPhiTRaTruoc_Xem = ROLE.PhanBoChiPhiTRaTruoc_Xem;
    ROLE_BuTruCongNo_Xem = ROLE.BuTruCongNo_Xem;
    ROLE_PhuongPhapGianDon_Xem = ROLE.PhuongPhapGianDon_Xem;
    ROLE_PhuongPhapHeSo_Xem = ROLE.PhuongPhapHeSo_Xem;
    ROLE_PhuongPhapTyLe_Xem = ROLE.PhuongPhapTyLe_Xem;
    ROLE_GiaThanhTheoCTVV_Xem = ROLE.GiaThanhTheoCTVV_Xem;
    ROLE_GiaThanhTheoDonHang_Xem = ROLE.GiaThanhTheoDonHang_Xem;
    ROLE_GiaThanhTheoHopDong_Xem = ROLE.GiaThanhTheoHopDong_Xem;
    ROLE_KetChuyenChiPhi_Xem = ROLE.KetChuyenChiPhi_Xem;
    ROLE_KhaiBaoCCDCDauKy_Xem = ROLE.KhaiBaoCCDCDauKy_Xem;
    ROLE_KhaiBaoCCDCDauKy_Them = ROLE.KhaiBaoCCDCDauKy_Them;
    ROLE_KhaiBaoCCDCDauKy_Sua = ROLE.KhaiBaoCCDCDauKy_Sua;
    ROLE_KhaiBaoCCDCDauKy_Xoa = ROLE.KhaiBaoCCDCDauKy_Xoa;
    ROLE_KhaiBaoCCDCDauKy_KetXuat = ROLE.KhaiBaoCCDCDauKy_KetXuat;
    ROLE_HopDong = ROLE.HopDong;
    ROLE_HopDongMua_Xem = ROLE.HopDongMua_Xem;
    ROLE_HopDongBan_Xem = ROLE.HopDongBan_Xem;
    ROLE_HuyHoaDon = ROLE.HuyHoaDon;
    ROLE_HuyHoaDon_XEM = ROLE.HuyHoaDon_XEM;
    ROLE_LenhSanXuat_xem = ROLE.LenhSanXuat_Xem;
    ROLE_TinhGiaBan = ROLE.TinhGiaBan_Xem;
    ROLE_TinhGiaBan_Xem = ROLE.TinhGiaBan_Xem;
    ROLE_HoaDonDauVao_Xem = ROLE.HoaDonDauVao_Xem;
    ROLE_LapBaoCaoTaiChinh_Xem = ROLE.LapBaoCaoTaiChinh_Xem;

    NCCDV: string;
    NCCDV_EINVOICE = NCCDV_EINVOICE;

    constructor(
        private loginService: LoginService,
        private languageService: JhiLanguageService,
        private languageHelper: JhiLanguageHelper,
        private principal: Principal,
        private loginModalService: LoginModalService,
        private router: Router,
        private modalService: NgbModal,
        private eventManager: JhiEventManager,
        public utilsService: UtilsService
    ) {
        this.version = VERSION ? 'v' + VERSION : '';
        this.isNavbarCollapsed = true;
        this.loadHDDT();
        this.registerLoginSucss();
        this.registerChangeSession();
    }

    ngOnInit() {
        this.languageHelper.getAll().then(languages => {
            this.languages = languages;
        });
    }

    changeLanguage(languageKey: string) {
        this.languageService.changeLanguage(languageKey);
    }

    collapseNavbar() {
        this.isNavbarCollapsed = true;
    }

    isAuthenticated() {
        return this.principal.isAuthenticated();
    }

    login() {
        this.modalRef = this.loginModalService.open();
    }

    logout() {
        this.collapseNavbar();
        this.loginService.logout();
        this.router.navigate(['']);
    }

    toggleNavbar() {
        this.isNavbarCollapsed = !this.isNavbarCollapsed;
    }

    getImageUrl() {
        return this.isAuthenticated() ? this.principal.getImageUrl() : null;
    }

    addClass() {
        // if (this.isInside === true) {
        //     this.isHover = true;
        // }
        // alert('a!!!');
    }

    removeClass() {
        // if (this.isInside === false) {
        //     this.isHover = false;
        // }
        // alert('b!!!');
    }

    test() {
        // alert('CLICKED!!!');
    }

    inSideBar() {
        // this.isInside = true;
    }

    outSideBar() {
        // this.isInside = false;
    }

    /*
    * Chuongnv
    * */
    calculateExchangeRate() {
        // this.modalService.open(CalculateExchangeRateComponent as Component, {
        //     size: 'lg',
        //     backdrop: 'static',
        //     windowClass: 'width-80 width-50'
        // });
    }

    /*
  * Chuongnv
  * */
    calculateOWRepository() {
        // this.modalService.open(CalculateOWRepositoryComponent as Component, {
        //     size: 'lg',
        //     backdrop: 'static',
        //     windowClass: 'width-80 width-50'
        // });
    }

    /* Tiepvv
    * */
    UpdateFinishedProductPricesRepository() {
        // this.modalService.open(UpdateFinishedProductPricesRepositoryComponent as Component, {
        //     size: 'lg',
        //     backdrop: 'static',
        //     windowClass: 'width-80 width-60'
        // });
    }

    /*
    * Hautv
    * */
    ConnectEInvocie() {
        // this.modalService.open(ConnnectEInvoiceComponent as Component, {
        //     size: 'lg',
        //     backdrop: 'static',
        //     windowClass: 'width-80 width-50'
        // });
    }

    /*
    * Hautv
    * */
    khauTruThueGTGT() {
        // this.modalService.open(KhauTruThueGtgtComponent as Component, {
        //     size: 'lg',
        //     backdrop: 'static',
        //     windowClass: 'width-80 width-35'
        // });
    }

    registerLoginSucss() {
        this.eventSubscriber = this.eventManager.subscribe('authenticationSuccess', response => {
            this.loadHDDT(false);
        });
    }

    registerChangeSession() {
        this.eventSubscriber = this.eventManager.subscribe('changeSession', response => {
            this.principal.identity(true).then(account => {
                this.account = account;
                if (account) {
                    this.isTichHop = this.account.systemOption.some(x => x.code === TCKHAC_SDTichHopHDDT && x.data === '1');
                    this.useInvoiceWait = this.account.systemOption.some(x => x.code === CheckHDCD && x.data === '1');
                    this.NCCDV = this.account.systemOption.find(x => x.code === EI_IDNhaCungCapDichVu).data;
                    this.thuePPTT = this.account.organizationUnit.taxCalculationMethod === 1;
                }
            });
        });
        // this.eventSubscribers.push(this.eventSubscriber);
    }

    loadHDDT(force?) {
        if (force) {
            this.principal.identity(true).then(account => {
                this.account = account;
                if (account) {
                    this.isTichHop = this.account.systemOption.some(x => x.code === TCKHAC_SDTichHopHDDT && x.data === '1');
                    this.useInvoiceWait = this.account.systemOption.some(x => x.code === CheckHDCD && x.data === '1');
                    this.NCCDV = this.account.systemOption.find(x => x.code === EI_IDNhaCungCapDichVu).data;
                    this.thuePPTT = this.account.organizationUnit.taxCalculationMethod === 1;
                }
            });
        } else {
            this.principal.identity().then(account => {
                this.account = account;
                if (account) {
                    this.isTichHop = this.account.systemOption.some(x => x.code === TCKHAC_SDTichHopHDDT && x.data === '1');
                    this.useInvoiceWait = this.account.systemOption.some(x => x.code === CheckHDCD && x.data === '1');
                    this.NCCDV = this.account.systemOption.find(x => x.code === EI_IDNhaCungCapDichVu).data;
                    this.thuePPTT = this.account.organizationUnit.taxCalculationMethod === 1;
                }
            });
        }
    }

    /*
    * @Author Hautv
    * */
    lockBook() {
        // this.modalService.open(KhoaSoKyKeToanComponent as Component, {
        //     size: 'lg',
        //     backdrop: 'static',
        //     windowClass: 'width-80 width-40'
        // });
    }

    /*
    * @Author Hautv
    * */
    unlockBook() {
        // this.modalService.open(BoKhoaSoKyKeToanComponent as Component, {
        //     size: 'lg',
        //     backdrop: 'static',
        //     windowClass: 'width-80 width-35'
        // });
    }

    navigateTMNH() {
        this.utilsService.navigateToOtherSite('quy-trinh-tien-mat-ngan-hang');
    }

    navigateKho() {
        this.utilsService.navigateToOtherSite('quy-trinh-kho');
    }

    navigateTongHop() {
        this.utilsService.navigateToOtherSite('quy-trinh-tong-hop');
    }

    navigateMuaHang() {
        this.utilsService.navigateToOtherSite('quy-trinh-mua-hang');
    }

    navigateHoaDonDienTu() {
        this.utilsService.navigateToOtherSite('quy-trinh-hoa-don-dien-tu');
    }

    navigateQuanLyHoaDon() {
        this.utilsService.navigateToOtherSite('quy-trinh-quan-ly-hoa-don');
    }

    navigateBanHang() {
        this.utilsService.navigateToOtherSite('quy-trinh-ban-hang');
    }

    navigateGiaThanh() {
        this.utilsService.navigateToOtherSite('quy-trinh-gia-thanh');
    }

    navigateHopDong() {
        this.utilsService.navigateToOtherSite('quy-trinh-hop-dong');
    }

    navigateThue() {
        this.utilsService.navigateToOtherSite('quy-trinh-thue');
    }

    navigateLuong() {
        this.utilsService.navigateToOtherSite('quy-trinh-luong');
    }

    navigateCCDC() {
        this.utilsService.navigateToOtherSite('quy-trinh-ccdc');
    }

    navigateTSCD() {
        this.utilsService.navigateToOtherSite('quy-trinh-tscd');
    }

    showNopTienBaoHiem() {
        this.modalService.open(NopTienBaoHiemComponent as Component, {
            size: 'lg',
            backdrop: 'static',
            windowClass: 'width-thanh-toan-luong'
        });
    }

    showThanhToanLuong() {
        this.modalService.open(ThanhToanLuongComponent as Component, {
            size: 'lg',
            backdrop: 'static',
            windowClass: 'width-thanh-toan-luong'
        });
    }

    /*
    * Hautv
    * */
    nopThue() {
        // this.modalService.open(NopThueComponent as Component, {
        //     size: 'lg',
        //     backdrop: 'static',
        //     windowClass: 'width-80 width-50'
        // });
    }

    SapXepThuTuNhapXuat() {
        // const modalRef = this.modalService.open(SortOrderImportExportComponent as Component, {
        //     size: 'lg',
        //     backdrop: 'static',
        //     windowClass: 'width-80 width-60'
        // });
        // modalRef.componentInstance.page = 1;
        // modalRef.componentInstance.previousPage = 1;
    }

    openBuTruCongNo() {
        // this.modalService.open(BuTruCongNoComponent as Component, {
        //     backdrop: 'static',
        //     windowClass: 'myCustomModalClass'
        // });
    }
}
