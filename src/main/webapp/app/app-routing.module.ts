import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { errorRoute, navbarRoute } from './layouts';
import { DEBUG_INFO_ENABLED } from 'app/app.constants';
import { loginRoute } from 'app/layouts/login/login.route';
import { sidebarRoute } from 'app/layouts/sidebar/sidebar.route';

const LAYOUT_ROUTES = [navbarRoute, ...errorRoute, ...loginRoute, sidebarRoute];

@NgModule({
    imports: [
        RouterModule.forRoot(
            [
                ...LAYOUT_ROUTES,
                {
                    path: 'admin',
                    loadChildren: './admin/admin.module#TestluongAdminModule'
                },
                // {
                //     path: './luong',
                //     loadChildren: './home/home.module#TestluongHomeModule'
                // },
                {
                    path: 'cham-cong',
                    loadChildren: './luong/cham_cong/cham-cong.module#EbwebPSTimeSheetModule'
                },
                {
                    path: 'tong-hop-cham-cong',
                    loadChildren: './luong/tong_hop_cham_cong/tong-hop-cham-cong.module#EbwebPSTimeSheetSummaryModule'
                },
                {
                    path: 'bang-luong',
                    loadChildren: './luong/bang_luong/bang-luong.module#EbwebPSSalarySheetModule'
                },
                {
                    path: 'hach-toan-chi-phi-luong',
                    loadChildren: './luong/hach_toan_chi_phi_luong/hach-toan-chi-phi-luong.module#EbwebHachToanChiPhiLuongModule'
                }
            ],
            { useHash: true, enableTracing: DEBUG_INFO_ENABLED }
        )
    ],
    exports: [RouterModule]
})
export class TestluongAppRoutingModule {}
